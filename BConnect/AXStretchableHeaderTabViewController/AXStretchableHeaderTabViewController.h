//
//  AXStretchableHeaderTabViewController.h
//  Pods
//

#import <UIKit/UIKit.h>
#import "BaseViewViewController.h"
#import "AXStretchableHeaderView.h"
#import "AXTabBar.h"

@class AXStretchableHeaderTabViewController;

@protocol AXStretchableSubViewControllerViewSource <NSObject>
@optional
- (UIScrollView *)stretchableSubViewInSubViewController:(id)subViewController;
/*huydna*/
- (UIEdgeInsets)stretchableSubViewContentInset;

@end

@interface AXStretchableHeaderTabViewController : BaseViewViewController <UIScrollViewDelegate, AXTabBarDelegate>
@property (nonatomic) NSUInteger selectedIndex;
@property (readwrite, nonatomic) UIViewController *selectedViewController;
@property (copy, nonatomic) NSArray *viewControllers;

@property (weak, nonatomic) IBOutlet AXStretchableHeaderView *collapseView;
@property (readonly, nonatomic) AXTabBar *tabBar;
@property (weak, nonatomic) IBOutlet UIScrollView *containerScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerBottomConstraint;

@property (nonatomic) BOOL shouldBounceHeaderView;
@property (nonatomic) BOOL shouldFooterView;

// Layout
- (void)layoutHeaderViewAndTabBar;
- (void)layoutViewControllers;
- (void)layoutSubViewControllerToSelectedViewController;
- (UIScrollView *)scrollViewWithSubViewController:(UIViewController *)viewController;
@end

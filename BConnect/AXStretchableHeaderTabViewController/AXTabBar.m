//
//  AXTabBar.m
//  Pods
//

#import "AXTabBar.h"
#import "AXTabBarItemButton.h"

@interface AXTabBar ()
@end

@implementation AXTabBar {
  NSArray *_items;
  CALayer *_bottomSeparator;
  CALayer *_indicatorLayer;
  AXTabBarStyle _tabBarStyle;
}

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    _tabBarButtonFont = [UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:14];
    _containerView = [[UIScrollView alloc] init];
    _containerView.bounces = NO;
    _containerView.showsHorizontalScrollIndicator = NO;
    _containerView.decelerationRate = UIScrollViewDecelerationRateFast;
    [self addSubview:_containerView];
    
//    _bottomSeparator = [CALayer layer];
//    [_bottomSeparator setBackgroundColor:[[APP_BACKGROUND_COLOR colorWithAlphaComponent:1.0] CGColor]];
//    [self setTintColor:[APP_BACKGROUND_COLOR colorWithAlphaComponent:1.0]];
//    [self.layer addSublayer:_bottomSeparator];
    
    _indicatorLayer = [CALayer layer];
    [self.layer setBackgroundColor:[[UIColor whiteColor] CGColor]];
      
    _tabBarHeight = AXTABBAR_HEIGHT_NORMAL;
  }
  return self;
}

- (void)layoutSubviews
{
  [super layoutSubviews];
  [_containerView setFrame:self.bounds];
  
  if (_tabBarItemButtons.count > 0) {
    CGSize buttonSize = (CGSize){CGRectGetWidth(self.bounds) / _tabBarItemButtons.count, CGRectGetHeight(self.bounds)};
    switch (_tabBarStyle) {
      case AXTabBarStyleDefault: {
        [_tabBarItemButtons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
          [button setFrame:(CGRect){buttonSize.width * idx, 0.0, buttonSize}];
        }];
        [_containerView setContentSize:CGSizeZero];
        break;
      }
      case AXTabBarStyleVariableWidthButton: {
        __block CGFloat x = 8.0;
        [_tabBarItemButtons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
          [button sizeToFit];
          CGFloat buttonWidth = CGRectGetWidth(button.bounds);
          [button setFrame:(CGRect){x, 0.0, buttonWidth, buttonSize.height}];
          x += buttonWidth;
        }];
        [_containerView setContentSize:(CGSize){x, 0.0}];
        break;
      }
    }
    [self layoutIndicatorLayerWithButton:[_tabBarItemButtons objectAtIndex:[_items indexOfObject:_selectedItem]]];
  }
}

- (void)sizeToFit
{
  [super sizeToFit];
  [self setBounds:(CGRect){
    CGPointZero,
    CGRectGetWidth([[UIApplication sharedApplication] statusBarFrame]),
    _tabBarHeight
  }];
}

#pragma mark - Property

- (void)setImageButton:(AXTabBarItemButton*)button item:(UITabBarItem*)item {
    CGSize buttonSize = (CGSize){CGRectGetWidth(self.bounds) / self.items.count, CGRectGetHeight(self.bounds)};

    UIImageView* imgIcon = nil;
    UILabel* lbTab = nil;

    if (self.tabBarHeight == AXTABBAR_HEIGHT_NONE) {
        return;
        
    } else  if (self.tabBarHeight == AXTABBAR_HEIGHT_NORMAL) {
        
        /*
        imgIcon = [[UIImageView alloc] initWithFrame:CGRectMake(buttonSize.width / 2 - 13, buttonSize.height / 2 - 13, 26, 26)];
        lbTab = [[UILabel alloc] initWithFrame:CGRectMake(0, buttonSize.height / 2 + 5, buttonSize.width, 20)];
        [lbTab setHidden:YES];
        [lbTab setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:12]];
         */
        
        lbTab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, buttonSize.width, buttonSize.height)];
        [lbTab setHidden:NO];
        [lbTab setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Bold" size:16]];

    } else {
        imgIcon = [[UIImageView alloc] initWithFrame:CGRectMake(buttonSize.width / 2 - 11, buttonSize.height / 2 - 17, 22, 22)];
        lbTab = [[UILabel alloc] initWithFrame:CGRectMake(0, buttonSize.height / 2 + 5, buttonSize.width, 20)];
        [lbTab setHidden:NO];
        [lbTab setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Bold" size:12]];
    }
    
    [imgIcon setContentMode:UIViewContentModeScaleAspectFit];
    imgIcon.tag = 1;
    [lbTab setTextAlignment:NSTextAlignmentCenter];
    lbTab.tag = 2;
    [lbTab setTextColor:kShopViewTabBarNormalColor];
    [lbTab setBackgroundColor:[UIColor whiteColor]];
    lbTab.highlightedTextColor = APP_COLOR;
    
    if ([item.title isEqualToString:[CoreStringUtils getStringRes:@"str_tab_shop_promotion"]]) {
        imgIcon.image = nil;
        lbTab.text = item.title;
        [button addSubview:imgIcon];
        [button addSubview:lbTab];
        
    } else if ([item.title isEqualToString:[CoreStringUtils getStringRes:@"str_tab_shop_service"]]) {
        imgIcon.image = nil; //[UIImage imageNamed:@"ic_tab_booth_n"];
        lbTab.text = item.title;
        [button addSubview:imgIcon];
        [button addSubview:lbTab];

    } else if ([item.title isEqualToString:[CoreStringUtils getStringRes:@"str_tab_shop_product"]]) {
        imgIcon.image = nil; //[UIImage imageNamed:@"ic_tab_keynote_n"];
        lbTab.text = item.title;
        [button addSubview:imgIcon];
        [button addSubview:lbTab];
        
    } else if ([item.title isEqualToString:[CoreStringUtils getStringRes:@"str_tab_shop_info"]]) {
        imgIcon.image = nil; //[UIImage imageNamed:@"ic_tab_info_n"];
        lbTab.text = item.title;
        [button addSubview:imgIcon];
        [button addSubview:lbTab];
    } else if ([item.title isEqualToString:[CoreStringUtils getStringRes:@"str_Activity"]]) {
        imgIcon.image = [UIImage imageNamed:@"ic_tab_activity_n"];
        lbTab.text = item.title;
        [button addSubview:imgIcon];
        [button addSubview:lbTab];
    } else if ([item.title isEqualToString:[CoreStringUtils getStringRes:@"str_Wish_list"]]) {
        imgIcon.image = [UIImage imageNamed:@"ic_tab_wishlist_n"];
        lbTab.text = item.title;
        [button addSubview:imgIcon];
        [button addSubview:lbTab];
    }  else if ([item.title isEqualToString:[CoreStringUtils getStringRes:@"str_products"]]) {
        [button setBackgroundColor:APP_BACKGROUND_COLOR];
        UILabel* lb = [[UILabel alloc] initWithFrame:CGRectMake(7, 12, 150, 20)];
        [lb setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:16]];
        [lb setTextColor:[UIColor colorWithRed:55/255.0 green:61/255.0 blue:59/255.0 alpha:1.0]];
        lb.text = [CoreStringUtils getStringRes:@"str_products"];
        [button addSubview:lb];
    } else if ([item.title isEqualToString:[CoreStringUtils getStringRes:@"str_similar_products"]]) {
        [button setBackgroundColor:APP_BACKGROUND_COLOR];
        UILabel* lb = [[UILabel alloc] initWithFrame:CGRectMake(7, 12, 150, 20)];
        [lb setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:16]];
        [lb setTextColor:[UIColor colorWithRed:55/255.0 green:61/255.0 blue:59/255.0 alpha:1.0]];
        lb.text = [CoreStringUtils getStringRes:@"str_similar_products"];
        [button addSubview:lb];
    } else if ([item.title isEqualToString:[CoreStringUtils getStringRes:@"str_history_tab"]]) {
         lbTab.text = [CoreStringUtils getStringRes:@"str_history_tab"];
         [button addSubview:lbTab];
    } else if ([item.title isEqualToString:[CoreStringUtils getStringRes:@"str_schedule_tab"]]) {
        lbTab.text = [CoreStringUtils getStringRes:@"str_schedule_tab"];
        [button addSubview:lbTab];
    }
}

- (void)setItems:(NSArray *)items
{
  if ([_items isEqualToArray:items] == NO) {
    _items = [items copy];
    
    // TODO
    NSMutableArray *buttons = [NSMutableArray array];
    [items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
      if ([obj isKindOfClass:[UITabBarItem class]]) {
        UITabBarItem *item = obj;
        AXTabBarItemButton *button = [[AXTabBarItemButton alloc] init];
        [button.titleLabel setFont:_tabBarButtonFont];
        [button setTitle:nil forState:UIControlStateNormal];
        [button setBadgeValue:item.badgeValue];
        [button addTarget:self action:@selector(touchesButton:) forControlEvents:UIControlEventTouchDown];
        [button setTitleColor:self.tintColor forState:UIControlStateSelected];
        [button setTitleColor:self.tintColor forState:UIControlStateHighlighted];
        [self setImageButton:button item:item];
        [_containerView addSubview:button];
        [buttons addObject:button];
      }
    }];
    [_indicatorLayer setBackgroundColor:[self.tintColor CGColor]];
    [self setNeedsLayout];
    self.tabBarItemButtons = [buttons copy];
    
    [self setSelectedItem:[items firstObject]];
  }
}

- (void)setSelectedImageButton:(AXTabBarItemButton*)button selected:(BOOL)selected {
    if (self.tabBarHeight == AXTABBAR_HEIGHT_NONE)
        return;
    
    UIImageView* imgIcon = [button viewWithTag:1];
    UILabel* lbTab = [button viewWithTag:2];
    lbTab.textColor = selected ? kShopViewTabBarHighlightedColor : kShopViewTabBarNormalColor;
    
    if ([lbTab.text isEqualToString:[CoreStringUtils getStringRes:@"str_tab_event_feature"]]) {
        imgIcon.image = selected ? [UIImage imageNamed:@"ic_tab_featured_o"] : [UIImage imageNamed:@"ic_tab_featured_n"];
        
    } else if ([lbTab.text isEqualToString:[CoreStringUtils getStringRes:@"str_tab_booth_event"]]) {
        imgIcon.image = selected ? [UIImage imageNamed:@"ic_tab_booth_o"] : [UIImage imageNamed:@"ic_tab_booth_n"];
        
    } else if ([lbTab.text isEqualToString:[CoreStringUtils getStringRes:@"str_tab_keynote_event"]]) {
        imgIcon.image = selected ? [UIImage imageNamed:@"ic_tab_keynote_o"] : [UIImage imageNamed:@"ic_tab_keynote_n"];
        
    } else if ([lbTab.text isEqualToString:[CoreStringUtils getStringRes:@"str_tab_event_info"]]) {
        imgIcon.image = selected ? [UIImage imageNamed:@"ic_tab_info_o"] : [UIImage imageNamed:@"ic_tab_info_n"];
        
    } else if ([lbTab.text isEqualToString:[CoreStringUtils getStringRes:@"str_Activity"]]) {
        imgIcon.image = selected ? [UIImage imageNamed:@"ic_tab_activity_o"] : [UIImage imageNamed:@"ic_tab_activity_n"];
        
    } else if ([lbTab.text isEqualToString:[CoreStringUtils getStringRes:@"str_Wish_list"]]) {
        imgIcon.image = selected ? [UIImage imageNamed:@"ic_tab_wishlist_o"] : [UIImage imageNamed:@"ic_tab_wishlist_n"];
    }
}

- (void)setSelectedItem:(UITabBarItem *)selectedItem
{
  if (_selectedItem != selectedItem) {
    
    NSUInteger beforeIndex = [_items indexOfObject:_selectedItem];
    NSUInteger afterIndex = [_items indexOfObject:selectedItem];
    if (beforeIndex != NSNotFound) {
      [_tabBarItemButtons[beforeIndex] setSelected:NO];
      [self setSelectedImageButton:_tabBarItemButtons[beforeIndex] selected:NO];
    }
    if (afterIndex != NSNotFound) {
      AXTabBarItemButton *button = _tabBarItemButtons[afterIndex];
      _selectedItem = selectedItem;
      [button setSelected:YES];
      [self setSelectedImageButton:button selected:YES];
        
        /*huydna: auto scroll when change tab */
        if (_tabBarStyle == AXTabBarStyleVariableWidthButton) {
            CGPoint scrollOffset = self.containerView.contentOffset;
            CGPoint origin = button.frame.origin;
            CGSize buttonSize = button.frame.size;
            CGSize contentSize = self.containerView.contentSize;
            CGSize frameSize = self.containerView.frame.size;
            CGFloat newOffsetX = origin.x + buttonSize.width / 2 - frameSize.width / 2;
            if (newOffsetX < 0)
                newOffsetX = 0;
            else if (newOffsetX + frameSize.width > contentSize.width)
                newOffsetX = contentSize.width - frameSize.width;
            [self.containerView setContentOffset:CGPointMake(newOffsetX, scrollOffset.y)];
        }
        
      [self layoutIndicatorLayerWithButton:button];
    }
  }
}

- (void)layoutIndicatorLayerWithButton:(UIButton *)button
{
  [CATransaction begin];
  [CATransaction setDisableActions:YES];
  CGFloat width = CGRectGetWidth(self.bounds);
  CGFloat height = CGRectGetHeight(self.bounds);
  [_bottomSeparator setFrame:(CGRect){
    0.0, height - 1.0,
    width, 1.0
  }];
  [_indicatorLayer setFrame:(CGRect){
    CGRectGetMinX(button.frame), height - 2.0,
    CGRectGetWidth(button.frame), 2.0
  }];
  [CATransaction commit];
}

- (void)setTabBarStyle:(AXTabBarStyle)tabBarStyle
{
  if (_tabBarStyle != tabBarStyle) {
    _tabBarStyle = tabBarStyle;
    [self setNeedsLayout];
  }
}

#pragma mark - Action

- (void)touchesButton:(UIButton *)sender
{
  NSUInteger index = [_tabBarItemButtons indexOfObject:sender];
  if (index != NSNotFound) {
    UITabBarItem *selectedItem = _items[index];
    
    BOOL shouldSelectItem = YES;
    if ([_delegate respondsToSelector:@selector(tabBar:shouldSelectItem:)]) {
      shouldSelectItem = [_delegate tabBar:self shouldSelectItem:selectedItem];
    }
    if (shouldSelectItem) {
      [sender setHighlighted:YES];
      [self setSelectedItem:selectedItem];
      if ([_delegate respondsToSelector:@selector(tabBar:didSelectItem:)]) {
        [_delegate tabBar:self didSelectItem:selectedItem];
      }
    }
  }
}

@end

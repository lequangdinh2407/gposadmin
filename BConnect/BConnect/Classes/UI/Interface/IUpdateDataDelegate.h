//
//  IUpdateDataDelegate.h
//  spabees
//
//  Created by Toan Pham Thanh on 1/14/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#ifndef spabees_IUpdateDataDelegate_h
#define spabees_IUpdateDataDelegate_h

#import <Foundation/Foundation.h>


@protocol IUpdateDataDelegate <NSObject>
- (void)didInitServiceMenu;
- (void)didInitToken:(NSString*)token;
@end

#endif

//
//  IViewControllerResultDelegate.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/30/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#ifndef spabees_IViewControllerResultDelegate_h
#define spabees_IViewControllerResultDelegate_h

@protocol IViewControllerResultDelegate <NSObject>

- (void)didViewControllerResult:(NSInteger)requestCode data:(NSMutableDictionary *)data;

@end

#endif
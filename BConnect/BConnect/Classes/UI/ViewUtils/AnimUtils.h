//
//  AnimUtils.h
//  BConnect
//
//  Created by Toan Pham Thanh on 3/25/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import <pop/POP.h>
#import "ZFModalTransitionAnimator.h"

@interface AnimUtils : NSObject

+ (POPBasicAnimation *)fadeInIconAnimation:(CGFloat)duration;
+ (ZFModalTransitionAnimator*)addZDragAnimator:(UIViewController*)controller;

@end

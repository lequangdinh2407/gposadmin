//
//  AnimUtils.m
//  BConnect
//
//  Created by Toan Pham Thanh on 3/25/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import "AnimUtils.h"

@implementation AnimUtils

+ (POPBasicAnimation *)fadeInIconAnimation:(CGFloat)duration {
    
    POPBasicAnimation *animation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    
    animation.fromValue = @(0.0);
    
    animation.toValue = @(1.0);
    
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    animation.duration = duration;
    
    return animation;
}

+ (ZFModalTransitionAnimator*)addZDragAnimator:(UIViewController*)controller {
    ZFModalTransitionAnimator *animator = [[ZFModalTransitionAnimator alloc] initWithModalViewController:controller];
    animator.dragable = YES;
    animator.bounces = NO;
    animator.behindViewAlpha = 0.5f;
    animator.behindViewScale = 0.5f;
    animator.transitionDuration = 0.7f;
    animator.direction = ZFModalTransitonDirectionBottom;
    
    return animator;
}


@end

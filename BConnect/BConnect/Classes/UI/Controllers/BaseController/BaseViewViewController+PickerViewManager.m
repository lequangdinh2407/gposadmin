//
//  BaseViewViewController+PickerViewManager.m
//  bizfconnect
//
//  Created by Dragon on 8/19/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "BaseViewViewController+PickerViewManager.h"

#import "UIAlertView+Blocks.h"

#define kPickerContentHeightNormal 221
#define kPickerContentWidthNormal (IS_IPAD ? 400 : (CGRectGetWidth([UIScreen mainScreen].bounds) - 20))
#define kImageOptionPickerContentHeight (IS_IPAD ? 300 : 180)

#define kCalendarPickerContentHeight (IS_IPHONE_4_OR_LESS ? 400 : 473)
#define kCalendarPickerContentWidth (IS_IPHONE_4_OR_LESS ? 288 : 352)

@implementation BaseViewViewController (PickerViewManager)

#pragma mark -UIPickerView methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.pickerData.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"data %ld", (long)row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
}

#pragma mark - IBAction methods
- (IBAction)btnPickerCancelPressed:(id)sender {
    @try {
        if ([(BaseViewViewController*)((BaseViewViewController*)rootView).pickerDelegate respondsToSelector:@selector(pickerCancel)]) {
            [(BaseViewViewController*)((BaseViewViewController*)rootView).pickerDelegate pickerCancel];
        }else{
            [self removePickerView];
        }
        
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

- (IBAction)btnPickerDonePressed:(id)sender {
    @try {
        [(BaseViewViewController*)((BaseViewViewController*)rootView).pickerDelegate pickerDone];
        [(BaseViewViewController*)((BaseViewViewController*)rootView) removePickerView];
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

- (IBAction)btnDateTimePickerChangeValue:(id)sender {
}

- (void)showAlertPermission {
    [[[UIAlertView alloc] initWithTitle:[CoreStringUtils getStringRes:@"str_access_denied"]
                                message:[CoreStringUtils getStringRes:@"str_camera_support_warning_message"]
                       cancelButtonItem:[RIButtonItem itemWithLabel:[CoreStringUtils getStringRes:@"str_cancel"] action:^{
        
    }]
                       otherButtonItems:[RIButtonItem itemWithLabel:[CoreStringUtils getStringRes:@"str_open_setting"] action:^{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }], nil] show];
}

- (IBAction)btnCameraPressed:(id)sender {
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus ==  AVAuthorizationStatusAuthorized) {        
        ((BaseViewViewController*)rootView).fasttCameraViewController = [FasttCameraViewController new];
        ((BaseViewViewController*)rootView).fasttCameraViewController.delegate = ((BaseViewViewController*)rootView).pickerDelegate;
        [((BaseViewViewController*)rootView) presentViewController:((BaseViewViewController*)rootView).fasttCameraViewController animated:YES completion:NULL];
    } else if(authStatus == AVAuthorizationStatusDenied){
        [self showAlertPermission];
    } else if(authStatus == AVAuthorizationStatusRestricted){
        [self showAlertPermission];
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){
                ((BaseViewViewController*)rootView).fasttCameraViewController = [FasttCameraViewController new];
                ((BaseViewViewController*)rootView).fasttCameraViewController.delegate = ((BaseViewViewController*)rootView).pickerDelegate;
                [((BaseViewViewController*)rootView) presentViewController:((BaseViewViewController*)rootView).fasttCameraViewController animated:YES completion:NULL];
            } else {
                
            }
        }];
    } else {
        
    }
}

- (IBAction)btnGalleryPressed:(id)sender {
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    ((BaseViewViewController*)rootView).photosPicker = [[UIImagePickerController alloc] init];
    ((BaseViewViewController*)rootView).photosPicker.delegate = ((BaseViewViewController*)rootView).pickerDelegate;
    ((BaseViewViewController*)rootView).photosPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [((BaseViewViewController*)rootView) presentViewController:((BaseViewViewController*)rootView).photosPicker animated:YES completion:NULL];
}

- (IBAction)btnVideoGalleryPressed:(id)sender {
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    ((BaseViewViewController*)rootView).photosPicker = [[UIImagePickerController alloc] init];
    ((BaseViewViewController*)rootView).photosPicker.delegate = ((BaseViewViewController*)rootView).pickerDelegate;
    ((BaseViewViewController*)rootView).photosPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    NSArray *videoMediaTypesOnly = [mediaTypes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF contains %@)", @"movie"]];
    ((BaseViewViewController*)rootView).photosPicker.mediaTypes = videoMediaTypesOnly;
    ((BaseViewViewController*)rootView).photosPicker.videoMaximumDuration = 10;
    [((BaseViewViewController*)rootView) presentViewController:((BaseViewViewController*)rootView).photosPicker animated:YES completion:NULL];
}

- (IBAction)btnVideoPressed:(id)sender {
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        ((BaseViewViewController*)rootView).cameraPicker = [[UIImagePickerController alloc] init];
        ((BaseViewViewController*)rootView).cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        ((BaseViewViewController*)rootView).cameraPicker.delegate = ((BaseViewViewController*)rootView).pickerDelegate;
        ((BaseViewViewController*)rootView).cameraPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;

        NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        NSArray *videoMediaTypesOnly = [mediaTypes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF contains %@)", @"movie"]];

        if ([videoMediaTypesOnly count] == 0) {		//Is movie output possible?
            [UIAlertView alertViewWithTitle:@"" message:[CoreStringUtils getStringRes:@"str_camera_support_warning_message"] cancelButtonTitle:[CoreStringUtils getStringRes:@"str_ok"]];
        } else {
            //Select front facing camera if possible
            //            if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront])
            //                videoRecorder.cameraDevice = UIImagePickerControllerCameraDeviceFront;

            ((BaseViewViewController*)rootView).cameraPicker.mediaTypes = videoMediaTypesOnly;
            ((BaseViewViewController*)rootView).cameraPicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
            ((BaseViewViewController*)rootView).cameraPicker.videoMaximumDuration = 10;			//Specify in seconds (600 is default)
            [self presentViewController:((BaseViewViewController*)rootView).cameraPicker animated:YES completion:nil];
        }
    } else {
        [UIAlertView alertViewWithTitle:@"" message:[CoreStringUtils getStringRes:@"str_camera_support_warning_message"] cancelButtonTitle:[CoreStringUtils getStringRes:@"str_ok"]];
    }
}

#pragma mark - FasttCameraControllerDelegate

- (void)dismissFasttCameraController:(FasttCameraViewController*)controller {
    ((BaseViewViewController*)rootView).fasttCameraViewController.delegate = nil;
    ((BaseViewViewController*)rootView).fasttCameraViewController = nil;
}

#pragma mark - Init methods
- (void)showPickerContainer:(NSInteger)pickerID target:(id)target{
    [self.view endEditing:YES];
    
    NSInteger pickerType = [self getCurrentPickerType];
    CGFloat contentWidth = (pickerType == DatePicker) ? kCalendarPickerContentWidth : kPickerContentWidthNormal;
    CGFloat contentHeight = (pickerType == DatePicker) ? kCalendarPickerContentHeight : kPickerContentHeightNormal;
    if (pickerType == ImagePicker || pickerType == VideoPicker) {
        contentHeight = kImageOptionPickerContentHeight;
    }
    
    ((BaseViewViewController*)rootView).pickerViewWidthConstraint.constant = contentWidth;
    ((BaseViewViewController*)rootView).pickerContentHeightConstraint.constant = contentHeight;
    [((BaseViewViewController*)rootView).view layoutIfNeeded];
    
    UITapGestureRecognizer* touch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removePickerView)];
    [((BaseViewViewController*)rootView).pickerContainer addGestureRecognizer:touch];
    
    ((BaseViewViewController*)rootView).pickerID = pickerID;
    ((BaseViewViewController*)rootView).pickerDelegate = target;
    
    [self hidePickerView:NO];
}

- (void)showListPickerView:(NSInteger)pickerID target:(id)target dataSource:(NSMutableArray*)data{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView) removePickerView];
    [((BaseViewViewController*)rootView).btnPickerDone setHidden:NO];
    [((BaseViewViewController*)rootView).pickerView setHidden:NO];
    [((BaseViewViewController*)rootView).pickerContainer bringSubviewToFront:((BaseViewViewController*)rootView).pickerView];
    ((BaseViewViewController*)rootView).pickerView.delegate = nil;
    ((BaseViewViewController*)rootView).pickerView.dataSource = nil;
    ((BaseViewViewController*)rootView).pickerView.delegate = target;
    ((BaseViewViewController*)rootView).pickerView.dataSource = target;
    self.pickerData = data;
    [self showPickerContainer:pickerID target:target];
}

- (void)showDateTimePickerView:(NSInteger)pickerID pickerMode:(UIDatePickerMode)mode value:(NSDate*)value target:(id)target{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView) removePickerView];
    [((BaseViewViewController*)rootView).btnPickerDone setHidden:NO];
    [((BaseViewViewController*)rootView).dateTimePicker setHidden:NO];
    [((BaseViewViewController*)rootView).pickerContainer bringSubviewToFront:((BaseViewViewController*)rootView).dateTimePicker];
    [((BaseViewViewController*)rootView).dateTimePicker setDatePickerMode:mode];
    [((BaseViewViewController*)rootView).dateTimePicker setDate:value];
    [self showPickerContainer:pickerID target:target];
    //[((BaseViewViewController*)rootView).view bringSubviewToFront:((BaseViewViewController*)rootView).pickerContainer];

}

- (void)showCalendarPickerView:(NSInteger)pickerID value:(NSDate*)value target:(id)target{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView) removePickerView];
    [((BaseViewViewController*)rootView).btnPickerDone setHidden:NO];
    [((BaseViewViewController*)rootView).calendarView setHidden:NO];
    [((BaseViewViewController*)rootView).pickerContainer bringSubviewToFront:((BaseViewViewController*)rootView).calendarView];
    
    float containerWidth = CGRectGetWidth(((BaseViewViewController*)rootView).calendarView.bounds);
    float containerHeight = CGRectGetHeight(((BaseViewViewController*)rootView).calendarView.bounds);
    ((BaseViewViewController*)rootView).datePickerView = [[TMDatePickerViewController alloc] initWithNibName:@"TMDatePickerViewController" bundle:nil];
    [((BaseViewViewController*)rootView).datePickerView.view setFrame:CGRectMake(0, 0, containerWidth, containerHeight)];
    ((BaseViewViewController*)rootView).datePickerView.thedatedelegate = self;
    ((BaseViewViewController*)rootView).datePickerView.inputdate = value;
    
    [((BaseViewViewController*)rootView).calendarView addSubview:((BaseViewViewController*)rootView).datePickerView.view];
    [((BaseViewViewController*)rootView).calendarView addConstraint:[NSLayoutConstraint constraintWithItem:((BaseViewViewController*)rootView).datePickerView.view
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:((BaseViewViewController*)rootView).calendarView
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [((BaseViewViewController*)rootView).calendarView addConstraint:[NSLayoutConstraint constraintWithItem:((BaseViewViewController*)rootView).datePickerView.view
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:((BaseViewViewController*)rootView).calendarView
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [((BaseViewViewController*)rootView).calendarView addConstraint:[NSLayoutConstraint constraintWithItem:((BaseViewViewController*)rootView).datePickerView.view
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:((BaseViewViewController*)rootView).calendarView
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [((BaseViewViewController*)rootView).calendarView addConstraint:[NSLayoutConstraint constraintWithItem:((BaseViewViewController*)rootView).datePickerView.view
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:((BaseViewViewController*)rootView).calendarView
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [((BaseViewViewController*)rootView).calendarView layoutIfNeeded];

    [self showPickerContainer:pickerID target:target];
}

- (void)showImagePickerView:(NSInteger)pickerID target:(id)target{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView) removePickerView];
    [((BaseViewViewController*)rootView).btnPickerDone setHidden:YES];
    [((BaseViewViewController*)rootView).imageOptionPicker setHidden:NO];
    [((BaseViewViewController*)rootView).pickerContainer bringSubviewToFront:((BaseViewViewController*)rootView).imageOptionPicker];
    ((BaseViewViewController*)rootView).imageOptionPicker.tag = pickerID;
    ((BaseViewViewController*)rootView).pickerDelegate = target;
    [self showPickerContainer:pickerID target:target];
}

- (void)showVideoPickerView:(NSInteger)pickerID target:(id)target{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView) removePickerView];
    [((BaseViewViewController*)rootView).btnPickerDone setHidden:YES];
    [((BaseViewViewController*)rootView).videoOptionPicker setHidden:NO];
    [((BaseViewViewController*)rootView).pickerContainer bringSubviewToFront:((BaseViewViewController*)rootView).videoOptionPicker];
    ((BaseViewViewController*)rootView).videoOptionPicker.tag = pickerID;
    ((BaseViewViewController*)rootView).pickerDelegate = target;
    [self showPickerContainer:pickerID target:target];
}

#pragma mark - Business methods
- (void)removePickerView{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [self hidePickerView:YES];
    ((BaseViewViewController*)rootView).pickerDelegate = nil;
    ((BaseViewViewController*)rootView).pickerID = -1;
    
    //Picker
    [((BaseViewViewController*)rootView).pickerView setHidden:YES];
    ((BaseViewViewController*)rootView).pickerView.delegate = nil;
    ((BaseViewViewController*)rootView).pickerView.dataSource = nil;
    self.pickerData = nil;
    
    //Date time picker
    [((BaseViewViewController*)rootView).dateTimePicker setHidden:YES];
    
    //Calendar picker
    [((BaseViewViewController*)rootView).calendarView setHidden:YES];
    ((BaseViewViewController*)rootView).datePickerView.thedatedelegate = nil;
    ((BaseViewViewController*)rootView).datePickerView = nil;
    
    //Image picker
    [((BaseViewViewController*)rootView).imageOptionPicker setHidden:YES];
    ((BaseViewViewController*)rootView).cameraPicker.delegate = nil;
    ((BaseViewViewController*)rootView).cameraPicker = nil;
    ((BaseViewViewController*)rootView).photosPicker.delegate = nil;
    ((BaseViewViewController*)rootView).photosPicker = nil;
    ((BaseViewViewController*)rootView).fasttCameraViewController.delegate = nil;
    ((BaseViewViewController*)rootView).fasttCameraViewController = nil;
    
    //Video picker
    [((BaseViewViewController*)rootView).videoOptionPicker setHidden:YES];
}

- (void)hidePickerView:(BOOL)isHide{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [((BaseViewViewController*)rootView).pickerContainer setHidden:isHide];
        isHide ? [((BaseViewViewController*)rootView).view sendSubviewToBack:((BaseViewViewController*)rootView).pickerContainer] : [((BaseViewViewController*)rootView).view bringSubviewToFront:((BaseViewViewController*)rootView).pickerContainer];
    } completion:^(BOOL finished) {
        
    }];
    
}

- (NSDate*)getCalendarDateSelected{
    if (![((BaseViewViewController*)rootView).calendarView isHidden]){
        return [((BaseViewViewController*)rootView).datePickerView getUserSelectedDate];
    }
    return [NSDate date];
}

- (PickerType)getCurrentPickerType{
    if (![((BaseViewViewController*)rootView).pickerView isHidden]) {
        return ListPicker;
    }else if (![((BaseViewViewController*)rootView).dateTimePicker isHidden]){
        return TimePicker;
    }else if (![((BaseViewViewController*)rootView).calendarView isHidden]){
        return DatePicker;
    }else if (![((BaseViewViewController*)rootView).imageOptionPicker isHidden]){
        return ImagePicker;
    }else if (![((BaseViewViewController*)rootView).videoOptionPicker isHidden]){
        return VideoPicker;
    }
    return NonePicker;
}

- (NSInteger)getPickerID{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return -1;
    }
    return ((BaseViewViewController*)rootView).pickerID;
}

- (UIPickerView*)getCurrentPickerView{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return nil;
    }
    return ((BaseViewViewController*)rootView).pickerView;
}

- (UIDatePicker*)getCurrentDateTimePickerView{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return nil;
    }
    return ((BaseViewViewController*)rootView).dateTimePicker;
}

- (void)pickerCancel{
    
}

- (void)pickerDone{
    
}
@end

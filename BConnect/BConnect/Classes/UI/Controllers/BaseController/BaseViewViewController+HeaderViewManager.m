//
//  BaseViewViewController+HeaderViewManager.m
//  spabees
//
//  Created by vu van long on 2/4/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "BaseViewViewController+HeaderViewManager.h"
#import "CoreConstants.h"

#define item_align 5
@implementation BaseViewViewController (HeaderViewManager)

#pragma mark - Left Item methods
- (void)enableLeftItem:(BOOL)isEnable{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    isEnable ? [((BaseViewViewController*)rootView).btnLeftItem setAlpha:1.0] : [((BaseViewViewController*)rootView).btnLeftItem setAlpha:0.5];
    [((BaseViewViewController*)rootView).btnLeftItem setEnabled:isEnable];
}

- (void)setLeftItemWithImage:(UIImage*)image target:(id)target action:(SEL)action{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    image = [Helpers imageResize:image andResizeTo:CGSizeMake(20, 20) keepRatio:YES];
    [((BaseViewViewController*)rootView).btnLeftItem setImage:image forState:UIControlStateNormal];
    [((BaseViewViewController*)rootView).btnLeftItem setImage:[Helpers changeImage:image toColor:[UIColor lightGrayColor]] forState:UIControlStateHighlighted];
    [((BaseViewViewController*)rootView).btnLeftItem addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [((BaseViewViewController*)rootView).btnLeftItem setHidden:NO];
}

- (void)setLeftItemWithTitle:(NSString*)title target:(id)target action:(SEL)action{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView).btnLeftItem setTitle:title forState:UIControlStateNormal];
    [((BaseViewViewController*)rootView).btnLeftItem addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [((BaseViewViewController*)rootView).btnLeftItem setImage:nil forState:UIControlStateNormal];
    [((BaseViewViewController*)rootView).btnLeftItem setHidden:NO];
}

- (void)hideRightItem:(BOOL)isHide{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    isHide ? [((BaseViewViewController*)rootView).btnRightItem setHidden:YES] : [((BaseViewViewController*)rootView).btnRightItem setHidden:NO];
}

- (void)enableRightItem:(BOOL)isEnable{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    isEnable ? [((BaseViewViewController*)rootView).btnRightItem setAlpha:1.0] : [((BaseViewViewController*)rootView).btnRightItem setAlpha:0.5];
    [((BaseViewViewController*)rootView).btnRightItem setEnabled:isEnable];
}

- (void)setRightItemWithImage:(UIImage*)image target:(id)target action:(SEL)action{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    image = [Helpers imageResize:image andResizeTo:CGSizeMake(20, 20) keepRatio:YES];
    [((BaseViewViewController*)rootView).btnRightItem setTitle:@"" forState:UIControlStateNormal];
    [((BaseViewViewController*)rootView).btnRightItem setImage:image forState:UIControlStateNormal];
    [((BaseViewViewController*)rootView).btnLeftItem setImage:[Helpers changeImage:image toColor:[UIColor lightGrayColor]] forState:UIControlStateHighlighted];
    [((BaseViewViewController*)rootView).btnRightItem addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    ((BaseViewViewController*)rootView).rightItemWidthConstraint.constant = MAX(((BaseViewViewController*)rootView).btnRightItem.imageView.frame.size.width, kHeaderItemWidth);
    [((BaseViewViewController*)rootView).view layoutIfNeeded];
    [self updateBackButtonSize];
    [((BaseViewViewController*)rootView).btnRightItem setHidden:NO];
}

- (void)setRightItemWithTitle:(NSString*)title target:(id)target action:(SEL)action{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView).btnRightItem setTitle:title forState:UIControlStateNormal];
    [((BaseViewViewController*)rootView).btnRightItem addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    CGSize newSize = [self changeSizeToFit:((BaseViewViewController*)rootView).btnRightItem];
    ((BaseViewViewController*)rootView).rightItemWidthConstraint.constant = MAX(newSize.width+5, kHeaderItemWidth);
    [((BaseViewViewController*)rootView).view layoutIfNeeded];
    [self updateBackButtonSize];
    [((BaseViewViewController*)rootView).btnRightItem setImage:nil forState:UIControlStateNormal];
    [((BaseViewViewController*)rootView).btnRightItem setHidden:NO];
}

- (void)hideSideRightItem:(BOOL)isHide{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    isHide ? [((BaseViewViewController*)rootView).btnSideRightItem setHidden:YES] : [((BaseViewViewController*)rootView).btnSideRightItem setHidden:NO];
}

- (void)enableSideRightItem:(BOOL)isEnable{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    isEnable ? [((BaseViewViewController*)rootView).btnSideRightItem setAlpha:1.0] : [((BaseViewViewController*)rootView).btnSideRightItem setAlpha:0.5];
    [((BaseViewViewController*)rootView).btnSideRightItem setEnabled:isEnable];
}

- (void)setSideRightItemWithImage:(UIImage*)image target:(id)target action:(SEL)action{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    image = [Helpers imageResize:image andResizeTo:CGSizeMake(20, 20) keepRatio:YES];
    [((BaseViewViewController*)rootView).btnSideRightItem setTitle:@"" forState:UIControlStateNormal];
    [((BaseViewViewController*)rootView).btnSideRightItem setImage:image forState:UIControlStateNormal];
    [((BaseViewViewController*)rootView).btnLeftItem setImage:[Helpers changeImage:image toColor:[UIColor lightGrayColor]] forState:UIControlStateHighlighted];
    [((BaseViewViewController*)rootView).btnSideRightItem addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    ((BaseViewViewController*)rootView).sideRightItemWidthConstraint.constant = MAX(((BaseViewViewController*)rootView).btnSideRightItem.imageView.frame.size.width, kHeaderItemWidth);
    [((BaseViewViewController*)rootView).view layoutIfNeeded];
    [self updateBackButtonSize];
    [((BaseViewViewController*)rootView).btnSideRightItem setHidden:NO];
}

- (void)setSideRightItemWithTitle:(NSString*)title target:(id)target action:(SEL)action{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView).btnSideRightItem setTitle:title forState:UIControlStateNormal];
    [((BaseViewViewController*)rootView).btnSideRightItem addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    CGSize newSize = [self changeSizeToFit:((BaseViewViewController*)rootView).btnSideRightItem];
    ((BaseViewViewController*)rootView).sideRightItemWidthConstraint.constant = MAX(newSize.width+5, kHeaderItemWidth);
    [((BaseViewViewController*)rootView).view layoutIfNeeded];
    [self updateBackButtonSize];
    [((BaseViewViewController*)rootView).btnSideRightItem setImage:nil forState:UIControlStateNormal];
    [((BaseViewViewController*)rootView).btnSideRightItem setHidden:NO];
}

- (CGSize)changeSizeToFit:(UIButton*)button{
    [button.titleLabel sizeToFit];
    CGRect newFrame = button.titleLabel.frame;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        
        CGRect oldFrame = button.frame;
        CGRect tmpFrame = [self sizeFitFrame:button.titleLabel oldFrame:oldFrame];
        newFrame = CGRectMake(oldFrame.origin.x, oldFrame.origin.y, tmpFrame.size.width, oldFrame.size.height);
        button.frame = newFrame;
    }
    return newFrame.size;
}

- (CGRect)sizeFitFrame:(UILabel*)view oldFrame:(CGRect)oldFrame{
    UIFont *font = [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:16];
    NSDictionary * attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    CGRect tmpFrame = [view.text boundingRectWithSize:(CGSize){99999, oldFrame.size.height}
                                                                                                         options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                                                                      attributes:attributes
                                                                                                         context:nil];
    return tmpFrame;
}

#define item_align 5
- (void)setRightItemArray:(NSArray*)itemList{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    if (itemList.count == 0) {
        return;
    }
    float containerWidth = item_align*(itemList.count-1);
    float leftAlign = 0;
    for (id item in itemList) {
        if ([item isKindOfClass:[UIButton class]]) {
            UIButton*tmpButton = (UIButton*)item;
            tmpButton.frame = CGRectMake((leftAlign + item_align), 0, tmpButton.frame.size.width, tmpButton.frame.size.height);
            [((BaseViewViewController*)rootView).rightItemContainer addSubview:tmpButton];
            containerWidth += tmpButton.frame.size.width;
            leftAlign += tmpButton.frame.size.width;
        }
    }
    ((BaseViewViewController*)rootView).rightItemContainerWidthConstraint.constant = containerWidth;
    [((BaseViewViewController*)rootView).rightItemContainer setHidden:NO];
    [((BaseViewViewController*)rootView).view layoutIfNeeded];
}

- (void)btnBackPressed:(id)sender {
   
    @synchronized(self) {
        [self.view endEditing:YES];
        [self dismissProgressHub];
        id tmp = bconnectAppDelegate.rootNavigation;
        if (!tmp) {
            bconnectAppDelegate.rootNavigation = [bconnectAppDelegate getRootNavigation];
        }
        
        if ([rootView isKindOfClass:[BaseViewViewController class]]) {
            UIImage* img = ((BaseViewViewController*)rootView).icBack.image;
            ((BaseViewViewController*)rootView).icBack.image = [Helpers changeImage:img toColor:[UIColor lightGrayColor]];
        }
        
        [bconnectAppDelegate.rootNavigation popViewControllerAnimated:YES];
//        [bconnectAppDelegate.rootNavigation popViewControllerAnimated:YES onCompletion:^{
//            if ([rootView isKindOfClass:[BaseViewViewController class]]) {
//                ((BaseViewViewController*)rootView).icBack.image = icBackDefault;
//            }
//        }];
    }
}

- (void)btnBackPressedWithNoOnCompletion:(id)sender {
    @synchronized(self) {
        [self.view endEditing:YES];
        [self dismissProgressHub];
        id tmp = bconnectAppDelegate.rootNavigation;
        if (!tmp) {
            bconnectAppDelegate.rootNavigation = [bconnectAppDelegate getRootNavigation];
        }
        
        if ([rootView isKindOfClass:[BaseViewViewController class]]) {
            UIImage* img = ((BaseViewViewController*)rootView).icBack.image;
            ((BaseViewViewController*)rootView).icBack.image = [Helpers changeImage:img toColor:[UIColor lightGrayColor]];
        }
        
        [bconnectAppDelegate.rootNavigation popViewControllerAnimated:YES];
    }
}

- (void)SetBackTitle:(NSString*)title target:(id)target action:(SEL)action{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    ((BaseViewViewController*)rootView).icBack.image = icBackDefault;
    [((BaseViewViewController*)rootView).lbBackTitle setTextColor:[UIColor whiteColor]];
    float oldWidth = ((BaseViewViewController*)rootView).lbBackTitle.frame.size.width;
    ((BaseViewViewController*)rootView).lbBackTitle.text = title;
    [((BaseViewViewController*)rootView).lbBackTitle setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:17]];
    [((BaseViewViewController*)rootView).lbBackTitle sizeToFit];
    float newWidth = ((BaseViewViewController*)rootView).backViewContainerWidthConstraint.constant - oldWidth + ((BaseViewViewController*)rootView).lbBackTitle.frame.size.width + 10;
    newWidth = newWidth < kHeaderItemWidth ? kHeaderItemWidth : newWidth;
    ((BaseViewViewController*)rootView).backViewContainerWidthConstraint.constant = newWidth;
    [((BaseViewViewController*)rootView).lbBackTitle layoutIfNeeded];
    [self updateBackButtonSize];
    [((BaseViewViewController*)rootView).btnBack addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [((BaseViewViewController*)rootView).btnBack addTarget:target action:@selector(btnBackHighlighPressed) forControlEvents:UIControlEventTouchDown];
    [((BaseViewViewController*)rootView).btnBack addTarget:target action:@selector(btnBackDefault) forControlEvents:UIControlEventTouchUpOutside];
    [((BaseViewViewController*)rootView).backButtonContainer setHidden:NO];
}

- (void)btnBackDefault{
    ((BaseViewViewController*)rootView).icBack.image = icBackDefault;
    [((BaseViewViewController*)rootView).lbBackTitle setTextColor:[UIColor whiteColor]];
}

- (void)btnBackHighlighPressed{
    ((BaseViewViewController*)rootView).icBack.image = icBackHighlighted;
    [((BaseViewViewController*)rootView).lbBackTitle setTextColor:[UIColor whiteColor]];
}

- (void)updateBackButtonSize{
    UIButton* rightItem = ((BaseViewViewController*)rootView).btnRightItem;
    BOOL isRightItemHidden  = rightItem.isHidden;
    UIButton* siderightItem = ((BaseViewViewController*)rootView).btnSideRightItem;
    BOOL isSideRightItemHidden  = siderightItem.isHidden;
    float maxWidth = [UIScreen mainScreen].bounds.size.width - (isRightItemHidden ? 0 : rightItem.frame.size.width) - (isSideRightItemHidden ? 0 : siderightItem.frame.size.width) - 15;
    float currentWidth = ((BaseViewViewController*)rootView).backViewContainerWidthConstraint.constant;
    float newWidth = maxWidth > currentWidth ? currentWidth : maxWidth;
    ((BaseViewViewController*)rootView).backViewContainerWidthConstraint.constant = newWidth;
    [((BaseViewViewController*)rootView).view layoutIfNeeded];
}

- (void)SetHeaderTitle:(NSString*)title{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    ((BaseViewViewController*)rootView).lbHeaderTittle.text = title;
    [((BaseViewViewController*)rootView).lbHeaderTittle setHidden:NO];
}

-(void)setUserInfoHeaderStyle {
    
    ((BaseViewViewController*)rootView).avatarWidthConstraint.constant = 295.994 * [UIScreen mainScreen].bounds.size.width / 1242.0;
    ((BaseViewViewController*)rootView).avatarTopConstraint.constant = 228.528 * ([UIScreen mainScreen].bounds.size.width * 410.0 / 1242.0) / 410 - ((BaseViewViewController*)rootView).avatarWidthConstraint.constant/2 + ((BaseViewViewController*)rootView).topPadding;
    
    ((BaseViewViewController*)rootView).avatarImageView.layer.cornerRadius = ((BaseViewViewController*)rootView).avatarWidthConstraint.constant/2;
    ((BaseViewViewController*)rootView).avatarImageView.clipsToBounds = false;
    ((BaseViewViewController*)rootView).avatarImageView.layer.masksToBounds = true;
    
    ((BaseViewViewController*)rootView).avatarImageView.hidden = false;
    
    //[Helpers loadImage:((BaseViewViewController*)rootView).avatarImageView placeHolder:nil withUrl:[SessionManager sharedClient].userEntity.avatar];
}

- (void)SetHeaderBackgroundColor:(UIColor*)color Opacity:(float)opacity{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    ((BaseViewViewController*)rootView).headerBackground.backgroundColor = color;
    ((BaseViewViewController*)rootView).headerBackground.alpha = opacity;
    
//    ((BaseViewViewController*)rootView).headerBackground.layer.masksToBounds = NO;
//    ((BaseViewViewController*)rootView).headerBackground.layer.cornerRadius = kShadowConerRadius; // if you like rounded corners
//    ((BaseViewViewController*)rootView).headerBackground.layer.shadowOffset = CGSizeMake(kShadowOffsetWidth, kShadowOffsetHeight);
//;
//    ((BaseViewViewController*)rootView).headerBackground.layer.shadowRadius = kShadowRadius;
//    ((BaseViewViewController*)rootView).headerBackground.layer.shadowOpacity = kShadowOpacity;
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:((BaseViewViewController*)rootView).headerBackground.bounds];
    ((BaseViewViewController*)rootView).headerBackground.layer.masksToBounds = NO;
    ((BaseViewViewController*)rootView).headerBackground.layer.shadowColor = APP_SUB_TEXT_COLOR.CGColor;
    ((BaseViewViewController*)rootView).headerBackground.layer.shadowOffset = CGSizeMake(0.05, 0.5);
    ((BaseViewViewController*)rootView).headerBackground.layer.shadowOpacity = 0.4;
    ((BaseViewViewController*)rootView).headerBackground.layer.shadowRadius = 0.1;
    ((BaseViewViewController*)rootView).headerBackground.layer.shadowPath = shadowPath.CGPath;
}

+ (void)hideHeaderView:(BOOL)isHide{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    
    if (((BaseViewViewController*)rootView).headerView != nil) {
        [((BaseViewViewController*)rootView).headerView setHidden:isHide];
        ((BaseViewViewController*)rootView).containerTopConstraint.constant = isHide ? 0 : ((BaseViewViewController*)rootView).HeaderViewHeight;
    }
    
    [((BaseViewViewController*)rootView).view layoutIfNeeded];
}

- (void)lockHeaderView:(BOOL)isLock{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView).headerView setUserInteractionEnabled:!isLock];
}

- (void)hideHeaderView:(BOOL)isHide{
    [BaseViewViewController hideHeaderView:isHide];
}

- (void)resetItem:(UIButton*)item{
    [item setEnabled:YES];
    [item setAlpha:1.0];
    [item removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [item setImage:nil forState:UIControlStateNormal];
    [item setTitle:@"" forState:UIControlStateNormal];
    [item setTransform:CGAffineTransformMakeScale(1.f, 1.f)];
    [item.layer removeAllAnimations];
}

- (void)resetHeader{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView).btnLeftItem setHidden:YES];
    [((BaseViewViewController*)rootView).btnRightItem setHidden:YES];
    [((BaseViewViewController*)rootView).btnSideRightItem setHidden:YES];
    [((BaseViewViewController*)rootView).backButtonContainer setHidden:YES];
    [((BaseViewViewController*)rootView).lbHeaderTittle setHidden:YES];
    ((BaseViewViewController*)rootView).rightItemWidthConstraint.constant = 40;
    
    [self resetItem:((BaseViewViewController*)rootView).btnLeftItem];
    [self resetItem:((BaseViewViewController*)rootView).btnRightItem];
    [self resetItem:((BaseViewViewController*)rootView).btnSideRightItem];   
    ((BaseViewViewController*)rootView).lbHeaderTittle.text = @"";
    
    for (UIView* tmpView in [((BaseViewViewController*)rootView).rightItemContainer subviews]) {
        [tmpView removeFromSuperview];
    }
    ((BaseViewViewController*)rootView).rightItemContainerWidthConstraint.constant = 0;
    [((BaseViewViewController*)rootView).rightItemContainer setHidden:NO];
    [((BaseViewViewController*)rootView) resetSearchBar];
    //[((BaseViewViewController*)rootView) SetHeaderBackgroundColor:APP_COLOR Opacity:1.0];
    
    [((BaseViewViewController*)rootView) SetBackTitle:nil target:nil action:nil];
    [((BaseViewViewController*)rootView).btnBack removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    ((BaseViewViewController*)rootView).lbBackTitle.text = @"";
}

- (void)setupHeader{
    [self resetHeader];
    [self hideHeaderView:NO];
    
    
    
}

@end

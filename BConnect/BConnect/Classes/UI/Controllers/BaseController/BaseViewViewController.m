//
//  BaseViewViewController.m
//  spabees
//
//  Created by DKMobility on 30/12/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "BaseViewViewController.h"
#import "CoreConstants.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "SessionManager.h"

#import "LoginViewController.h"
//#import "MainMapViewController.h"
#import "UIAlertView+Blocks.h"

@interface BaseViewViewController ()
@end

@implementation BaseViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[self.view setBackgroundColor:APP_BACKGROUND_COLOR];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:40/255.0 green:37/255.0 blue:44/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:22/255.0 green:22/255.0 blue:22/255.0 alpha:1.0].CGColor];
    
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    
    [self dismissMessageLabel];
    //[self.headerView setBackgroundColor:APP_COLOR];
    //[self setupCart];
    icBackDefault = [UIImage imageNamed:@"ic_back"];
    icBackHighlighted = [Helpers changeImage:[UIImage imageNamed:@"ic_back"] toColor:APP_COLOR_70];
    
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        self.topPadding = window.safeAreaInsets.top/2;
        self.bottomPadding = window.safeAreaInsets.bottom;
    }
    
    
    
    self.HeaderViewHeight = 64;
    self.headerHeightConstraint.constant +=  ((BaseViewViewController*)rootView).topPadding;
    self.HeaderViewHeight += ((BaseViewViewController*)rootView).topPadding;
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self dismissProgressHub];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self removeBottomMenu];
//    isLeavingViewController = NO;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"SegueToNavigationController"]) {
        BOOL isLogin = [SharePrefUtils getBoolPreference:PREFKEY_LOGIN_FLAG];
        UINavigationController * childViewController = (UINavigationController *) [segue destinationViewController];
        isLogin = YES;
        if (isLogin) {
            //MainMapViewController* controller = [[MainMapViewController alloc] init];
             SlidingViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SlidingViewController"];
            [childViewController setViewControllers:@[controller] animated:NO];
        } else {
            
        }
        // do something with the AlertView's subviews here...
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Business methods
- (void)setupViewStyle:(UIView*)view{
    view.layer.borderColor = [normal CGColor];
    view.layer.borderWidth = 1;
}

- (void)dismissStatusMessage{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView).lbStatus setHidden:YES];
    [((BaseViewViewController*)rootView).view sendSubviewToBack:self.lbStatus];
}

- (void)showMessageLabel:(NSString*)message{
    [self.messageLabel setHidden:NO];
    self.messageLabel.text = message;
    [self.view bringSubviewToFront:self.messageLabel];
}

- (void)dismissMessageLabel{
    [self.messageLabel setHidden:YES];
}

- (void)showStatusContent:(NSString*)message statusColor:(UIColor*)color opacity:(CGFloat)opacity duration:(int)seconds{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView).lbStatus setHidden:NO];
    ((BaseViewViewController*)rootView).lbStatus.text = message;
    ((BaseViewViewController*)rootView).lbStatus.backgroundColor = color;
    ((BaseViewViewController*)rootView).lbStatus.alpha = opacity;
    [((BaseViewViewController*)rootView).view bringSubviewToFront:((BaseViewViewController*)rootView).lbStatus];
    if (seconds > 0) {
        [rootView performSelector:@selector(dismissStatusMessage) withObject:nil afterDelay:seconds];
    }
}

/*
 * While have a viewcontroller is pushing, another viewcontroller next will be rejected until animation finished.
 */
- (void)pushToViewController:(id)controller{
    if (!isLeavingViewController) {
        isLeavingViewController = YES;
        
        @synchronized(self) {
            
            if (@available(iOS 10, *)) {
                id tmp = bconnectAppDelegate.rootNavigation;
                if (!tmp) {
                    bconnectAppDelegate.rootNavigation = [bconnectAppDelegate getRootNavigation];
                }
            } else {
                bconnectAppDelegate.rootNavigation = [bconnectAppDelegate getRootNavigation];
            }
            
            [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
            
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                isLeavingViewController = NO;
            });
        }
    }
}

- (void)showProgressHub{
    [self.view endEditing:YES];
    [self hideWaitingView:NO];
    self.isProcessingRequest = YES;
}

- (BOOL)isProcessing{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return NO;
    }
    return [((BaseViewViewController*)rootView).waitingView isAnimating];
}

- (void)hideProgessHub:(BOOL)isHide{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView).waitingView setHidden:isHide];
}

- (void)dismissProgressHub{
    [SVProgressHUD dismiss];
    [self hideWaitingView:YES];
    self.isProcessingRequest = NO;
}

- (void)hideWaitingView:(BOOL)isHide{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    
    if (!((BaseViewViewController*)rootView).waitingView) {
        ((BaseViewViewController*)rootView).waitingView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePlane image:[UIImage imageNamed:@"ic_waiter"]];
        ((BaseViewViewController*)rootView).waitingView.alpha = 0.8;
        ((BaseViewViewController*)rootView).waitingView.center = rootView.view.center;
        [((BaseViewViewController*)rootView).view addSubview:((BaseViewViewController*)rootView).waitingView];
    }
    [((BaseViewViewController*)rootView).waitingView setHidden:isHide];
    isHide ? [rootView.view sendSubviewToBack:((BaseViewViewController*)rootView).waitingView] : [rootView.view bringSubviewToFront:((BaseViewViewController*)rootView).waitingView];
    isHide ? [((BaseViewViewController*)rootView).waitingView stopAnimating] : [((BaseViewViewController*)rootView).waitingView startAnimating];
}

- (void)pushSlideMenu{
    @synchronized(self) {
        [self hideHeaderView:YES];
        SlidingViewController* controller = (SlidingViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"SlidingViewController"];
        controller.delegate = self;
        [self pushToViewController:controller];
    }
}

- (void)addConstraintSubview:(UIView*)subview toSuperView:(UIView*)view{
    [view addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [view addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [view addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [view addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [view layoutIfNeeded];
}

#pragma mark - Show Popup view

- (Popup*)setupPopperWithSubView:(UIView*)subView{
    Popup* popper = [[Popup alloc] initWithViewWidth:CGRectGetWidth(subView.frame) viewHeight:CGRectGetHeight(subView.frame)];
    [popper setDelegate:self];
    [popper setBackgroundBlurType:PopupBackGroundBlurTypeDark];
    [popper setIncomingTransition:PopupIncomingTransitionTypeEaseFromCenter];
    [popper setOutgoingTransition:PopupOutgoingTransitionTypeDisappearCenter];
    [popper setRoundedCorners:YES];
    [popper setTapBackgroundToDismiss:YES];
    [[popper getPopupView] addSubview:subView];
    [popper showPopup];
    return popper;
}

#pragma mark - Show error view
- (void)showNoItemView:(NSString*)text btnTitle:(NSString*)title {
    if (self.noItemView) {
       [self.noItemView removeFromSuperview];
    }
    self.noItemView = [[NoItemView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.noItemView loadText:text btnTitle:title];
    self.noItemView.noItemDelegate = self;
    [self.view addSubview:self.noItemView];
}

- (void)showNoItemView:(NSString*)text btnTitle:(NSString*)title frame:(CGRect)frame {
    if (self.noItemView) {
        [self.noItemView removeFromSuperview];
    }
    self.noItemView = [[NoItemView alloc] initWithFrame:frame];
    [self.noItemView loadText:text btnTitle:title];
    self.noItemView.noItemDelegate = self;
    [self.view addSubview:self.noItemView];
}

- (void)showNoItemView:(NSString*)text btnTitle:(NSString*)title frame:(CGRect)frame imageName:(NSString*)imageName {
    if (self.noItemView) {
        [self.noItemView removeFromSuperview];
    }
    self.noItemView = [[NoItemView alloc] initWithFrame:frame];
    [self.noItemView loadText:text btnTitle:title];
    self.noItemView.noItemDelegate = self;
    [self.view addSubview:self.noItemView];
    self.noItemView.imgEmpty.image = [UIImage imageNamed:imageName];
}

- (void)showNetworkErrorView {
    isErrorNetwork = YES;
    [self showNoItemView:[CoreStringUtils getStringRes:@"str_network_error"] btnTitle:[CoreStringUtils getStringRes:@"str_network_error_btn"]];
    self.noItemView.imgEmpty.image = [UIImage imageNamed:@"ic_no_network"];
}

- (void)hideNoItemView {
    [self.noItemView removeFromSuperview];
    self.noItemView = nil;
    isErrorNetwork = NO;
}

- (BOOL)isHideNoItemView {
    return self.noItemView ? NO : YES;
}

#pragma mark - Lock View
- (void)lockAllView:(BOOL)isLock {
    [self lockHeaderView:isLock];
    [self.view setUserInteractionEnabled:!isLock];    
}

#pragma mark - Animation methods
- (void)presentViewController:(UIViewController*)controller WithAnimation:(NSString*)type{
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionReveal;
    transition.subtype = type;//kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self presentViewController:controller animated:NO completion:nil];
}

- (void)dismissViewControllerWithAnimation:(NSString*)type{
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionReveal;
    transition.subtype = type;//kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(CABasicAnimation *)zoomIn {
    CABasicAnimation *ZoomInAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    ZoomInAnimation.beginTime = 0.0f;
    ZoomInAnimation.fromValue = [NSNumber numberWithFloat:1.0f];
    ZoomInAnimation.toValue = [NSNumber numberWithFloat:0.5f];
    ZoomInAnimation.duration = 1.0f;
    return ZoomInAnimation;
}

-(CABasicAnimation *)zoomOut {
    CABasicAnimation *ZoomInAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    ZoomInAnimation.beginTime = 0.5f;
    ZoomInAnimation.fromValue = [NSNumber numberWithFloat:0.5f];
    ZoomInAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    ZoomInAnimation.duration = 1.0f;
    return ZoomInAnimation;
}

- (void)flashingAnimation:(UIView*)view{
    CAAnimationGroup *anim = [CAAnimationGroup animation];
    [anim setAnimations:[NSArray arrayWithObjects:[self zoomIn], [self zoomOut], nil]];
    [anim setDuration:2.0];
    [anim setRemovedOnCompletion:NO];
    [anim setFillMode:kCAFillModeForwards];
    [view.layer addAnimation:anim forKey:nil];
}



#pragma mark - Location methods


#pragma mark - XLPagerTabStripViewControllerDelegate
-(NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{
    return self.controllerTitle;
}

-(UIColor *)colorForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{
    return [UIColor whiteColor];
}



- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
}

#pragma mark - Sliding menu methods
- (void)frostedViewController:(REFrostedViewController *)frostedViewController willShowMenuViewController:(UIViewController *)menuViewController{
    [self hideProgessHub:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_WILL_SHOW_SLIDING_MENU object:nil];
}

- (void)frostedViewController:(REFrostedViewController *)frostedViewController willHideMenuViewController:(UIViewController *)menuViewController{
    if ([self isProcessing]) {
        [self hideProgessHub:NO];
    }else{
        [self dismissProgressHub];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_WILL_HIDE_SLIDING_MENU object:nil];
}

- (void)menuButtonTapped:(id)sender{
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}

- (void)styleNavBar {
    
    [bconnectAppDelegate.rootNavigation setNavigationBarHidden:YES animated:NO];
    
    // 2. create a new nav bar and style it
    bconnectAppDelegate.navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64.0)];
    [bconnectAppDelegate.navigationBar setTintColor:[UIColor whiteColor]];
    [bconnectAppDelegate.navigationBar setBarTintColor:[UIColor colorWithRed:229/255.0 green:59/255.0 blue:36/255.0 alpha:1.0]];
    [bconnectAppDelegate.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:20]}];
    
    // 3. add a new navigation item w/title to the new nav bar
    bconnectAppDelegate.navigationItem = [[UINavigationItem alloc] init];
    [bconnectAppDelegate.navigationBar setItems:@[bconnectAppDelegate.navigationItem]];
    
    // 4. add the nav bar to the main view
    [self.view addSubview:bconnectAppDelegate.navigationBar];
}

- (void)removeBottomMenu{
    
}

#pragma mark - DB Methods
- (void)insertDBLoginInfo:(DBSuccessCallback)success failure:(DBFailureCallback)failure {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:[NSNumber numberWithInteger:ACTION_INSERT_ALL_LOGIN_INFO] forKey:DB_TYPE_ACTION];
    [data setValue:[SessionManager sharedClient].userEntity forKey:DB_USER_ENTITY];
    [data setValue:[SessionManager sharedClient].socialProfile forKey:DB_SOCIAL_PROFILE];
    [data setValue:[SessionManager sharedClient].sessionKey forKey:DB_SESSION_KEY];
    [data setValue:[SessionManager sharedClient].refreshToken forKey:DB_REFRESH_TOKEN];
    [data setValue:[SessionManager sharedClient].domain forKey:DB_DOMAIN];
    [data setValue:[SessionManager sharedClient].accountEntity forKey:DB_ACCOUNT_ENTITY];
    [data setValue:[SessionManager sharedClient].socket forKey:DB_SOCKET];
    [data setValue:[SessionManager sharedClient].uploadSocket forKey:DB_UPLOADSOCKET];
    
    NSInteger flag = (FLAG_DELETE_ACCOUNT | FLAG_DELETE_SESSION | FLAG_DELETE_USER);
    [data setValue:[NSNumber numberWithInteger:flag] forKey:DB_FLAG_ACTION];
    
    [[DatabaseManager sharedClient] addToQueue:data success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"insertDBLoginInfo success" message:results];
        if (success)
            success(results, errorCode);
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"insertDBLoginInfo failure" message:results];
        if (failure)
            failure(results, errorCode);
    }];
}

@end

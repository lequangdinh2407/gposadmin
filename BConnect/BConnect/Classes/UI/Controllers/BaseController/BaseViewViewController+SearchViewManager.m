//
//  BaseViewViewController+SearchViewManager.m
//  spabees
//
//  Created by vu van long on 2/4/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "BaseViewViewController+SearchViewManager.h"

@implementation BaseViewViewController (SearchViewManager)

- (void)resetSearchBar{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    ((BaseViewViewController*)rootView).searchBar.delegate = nil;
    [((BaseViewViewController*)rootView) hiddenSearchBar];
}

- (void)setupSearchView:(id)target{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    ((BaseViewViewController*)rootView).searchBar.layer.borderWidth = 1;
//    ((BaseViewViewController*)rootView).searchBar.layer.borderColor = [APP_SECONDARY_COLOR CGColor];
//    ((BaseViewViewController*)rootView).searchBar.layer.borderColor = [APP_COLOR CGColor];
//    ((BaseViewViewController*)rootView).searchBar.layer.cornerRadius = 10;
    ((BaseViewViewController*)rootView).searchBar.layer.borderColor = [[UIColor clearColor] CGColor];
    ((BaseViewViewController*)rootView).searchBar.delegate = nil;
    ((BaseViewViewController*)rootView).searchBar.delegate = target;
    [((BaseViewViewController*)rootView).searchBar setImage:[UIImage imageNamed:@"ic_search"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [((BaseViewViewController*)rootView).searchBar setImage:[UIImage imageNamed:@"ic_close_search"] forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
    ((BaseViewViewController*)rootView).searchBar.showsCancelButton = YES;
    
    UITextField *searchTextField = [((BaseViewViewController*)rootView).searchBar valueForKey:@"_searchField"];
    if ([searchTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        [searchTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}]];
    }
    if ([searchTextField respondsToSelector:@selector(setTextColor:)]) {
        [searchTextField setTextColor:[UIColor whiteColor]];
    }
    [((BaseViewViewController*)rootView) hiddenSearchBar];
}

- (void)showSearchBar {
    /*
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    self.isSearching = YES;
    [((BaseViewViewController*)rootView).searchContainer setHidden:NO];
    [((BaseViewViewController*)rootView).searchBar becomeFirstResponder];
    [((BaseViewViewController*)rootView).view sendSubviewToBack:((BaseViewViewController*)rootView).headerView];
    [((BaseViewViewController*)rootView).view bringSubviewToFront:((BaseViewViewController*)rootView).searchContainer];
     */
}

- (void)hiddenSearchBar {
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    self.isSearching = NO;
    [((BaseViewViewController*)rootView).searchContainer setHidden:YES];
    [((BaseViewViewController*)rootView).searchBar resignFirstResponder];
    [((BaseViewViewController*)rootView).view sendSubviewToBack:((BaseViewViewController*)rootView).searchContainer];
    [((BaseViewViewController*)rootView).view bringSubviewToFront:((BaseViewViewController*)rootView).headerView];
    ((BaseViewViewController*)rootView).searchBar.text = @"";
}

- (BOOL)isSearchBarHidden{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return NO;
    }
    return [((BaseViewViewController*)rootView).searchContainer isHidden];
}

- (void)handleSearchBarScrolling{
    if (self.isSearching) {
        if (self.mainPageDelegate && [self.mainPageDelegate respondsToSelector:@selector(handleSearchBarScrolling)]) {
            [self.mainPageDelegate handleSearchBarScrolling];
        }
    }else{
        if (![CoreStringUtils isEmpty:((BaseViewViewController*)rootView).searchBar.text]) {
            [((BaseViewViewController*)rootView).searchBar resignFirstResponder];
        } else {
            [self hiddenSearchBar];
        }
    }
}

- (void)handleSearch:(UISearchBar *)searchBar{
    
}
@end

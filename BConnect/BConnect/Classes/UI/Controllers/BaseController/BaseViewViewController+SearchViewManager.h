//
//  BaseViewViewController+SearchViewManager.h
//  spabees
//
//  Created by vu van long on 2/4/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "BaseViewViewController.h"

@interface BaseViewViewController (SearchViewManager)

- (void)resetSearchBar;
- (void)setupSearchView:(id)target;
- (void)showSearchBar;
- (void)hiddenSearchBar;
//- (BOOL)isSearchBarHidden;
- (void)handleSearchBarScrolling;
- (void)handleSearch:(UISearchBar *)searchBar;

@end

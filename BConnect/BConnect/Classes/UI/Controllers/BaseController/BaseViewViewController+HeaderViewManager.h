//
//  BaseViewViewController+HeaderViewManager.h
//  spabees
//
//  Created by vu van long on 2/4/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "BaseViewViewController.h"

@interface BaseViewViewController (HeaderViewManager)

+ (void)hideHeaderView:(BOOL)isHide;
- (void)lockHeaderView:(BOOL)isLock;
- (void)hideHeaderView:(BOOL)isHide;
- (void)resetHeader;
- (void)setupHeader;

- (void)enableLeftItem:(BOOL)isEnable;
- (void)setLeftItemWithImage:(UIImage*)image target:(id)target action:(SEL)action;
- (void)setLeftItemWithTitle:(NSString*)title target:(id)target action:(SEL)action;

- (void)hideRightItem:(BOOL)isHide;
- (void)enableRightItem:(BOOL)isEnable;
- (void)setRightItemWithImage:(UIImage*)image target:(id)target action:(SEL)action;
- (void)setRightItemWithTitle:(NSString*)title target:(id)target action:(SEL)action;

- (void)hideSideRightItem:(BOOL)isHide;
- (void)enableSideRightItem:(BOOL)isEnable;
- (void)setSideRightItemWithImage:(UIImage*)image target:(id)target action:(SEL)action;
- (void)setSideRightItemWithTitle:(NSString*)title target:(id)target action:(SEL)action;

- (void)setRightItemArray:(NSArray*)itemList;

- (void)btnBackPressed:(id)sender;
- (void)btnBackPressedWithNoOnCompletion:(id)sender;
- (void)SetBackTitle:(NSString*)title target:(id)target action:(SEL)action;
- (void)SetHeaderTitle:(NSString*)title;
- (void)SetHeaderBackgroundColor:(UIColor*)color Opacity:(float)opacity;
- (void)setUserInfoHeaderStyle;

@end

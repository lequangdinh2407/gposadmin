//
//  BaseViewViewController+PickerViewManager.h
//  bizfconnect
//
//  Created by Dragon on 8/19/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "BaseViewViewController.h"

@interface BaseViewViewController (PickerViewManager) <UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

- (IBAction)btnPickerCancelPressed:(id)sender;
- (IBAction)btnPickerDonePressed:(id)sender;
- (IBAction)btnDateTimePickerChangeValue:(id)sender;
- (IBAction)btnCameraPressed:(id)sender;
- (IBAction)btnGalleryPressed:(id)sender;
- (IBAction)btnVideoPressed:(id)sender;

//Show picker methods
- (void)showListPickerView:(NSInteger)pickerID target:(id)target dataSource:(NSMutableArray*)data;
- (void)showDateTimePickerView:(NSInteger)pickerID pickerMode:(UIDatePickerMode)mode value:(NSDate*)value target:(id)target;
- (void)showCalendarPickerView:(NSInteger)pickerID value:(NSDate*)value target:(id)target;
- (void)showImagePickerView:(NSInteger)pickerID target:(id)target;
- (void)showVideoPickerView:(NSInteger)pickerID target:(id)target;
- (void)removePickerView;
- (void)hidePickerView:(BOOL)isHide;
- (void)pickerCancel;
- (void)pickerDone;

//Get picker methods
- (NSDate*)getCalendarDateSelected;
- (PickerType)getCurrentPickerType;
- (NSInteger)getPickerID;
- (UIPickerView*)getCurrentPickerView;
- (UIDatePicker*)getCurrentDateTimePickerView;
@end

//
//  BaseViewViewController+CartManger.m
//  BConnect
//
//  Created by vu van long on 12/17/15.
//  Copyright © 2015 DKMobility. All rights reserved.
//

#import "BaseViewViewController+CartManger.h"
#import "LoginViewController.h"
#import "SessionManager.h"

@implementation BaseViewViewController (CartManger)
- (void)setupCart{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    //[self hideCart:YES];
    
//    UIImage* imgHighlight = [Helpers changeImage:[Helpers imageResize:[UIImage imageNamed:@"ic_cart"] andResizeTo:CGSizeMake(18, 18) keepRatio:YES] toColor:[UIColor grayColor]];
//    UIImage* img = [Helpers imageResize:[UIImage imageNamed:@"ic_cart"] andResizeTo:CGSizeMake(18, 18) keepRatio:YES];
//    [((BaseViewViewController*)rootView).btnCart setImage:imgHighlight forState:UIControlStateHighlighted];
//    [((BaseViewViewController*)rootView).btnCart setImage:img forState:UIControlStateNormal];
    
    //Badger
    /*
    if (!((BaseViewViewController*)rootView).cartBadger) {
        ((BaseViewViewController*)rootView).cartBadger = [[JSBadgeView alloc] initWithParentView:((BaseViewViewController*)rootView).cartBadgeContainer alignment:JSBadgeViewAlignmentTopRight];
        [((BaseViewViewController*)rootView).cartBadger setBadgeTextFont:[UIFont systemFontOfSize:12]];
        [((BaseViewViewController*)rootView).cartBadger setBadgeBackgroundColor:[UIColor colorWithRed:243.0/255.0 green:68.0/255.0 blue:51.0/255.0 alpha:1.0]];
        ((BaseViewViewController*)rootView).cartBadger.badgeText = @"0";
    }
    */
    
    //self.btnCart.hidden = NO;
    //self.btnCart.layer.cornerRadius = self.cartIcon.frame.size.height/4;
//    self.cartIcon.backgroundColor = APP_COLOR;
//    self.cartIcon.layer.cornerRadius = 22.5;
//    self.btnCart.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];

    //Position
    //[((BaseViewViewController*)rootView) setCartPosition:CGPointMake(0, 0)];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self createTransition];
}

+ (void)hideCartView:(BOOL)isHide {
    /*
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView).cartContainer setHidden:isHide];
    if (isHide == NO)
        [((BaseViewViewController*)rootView).btnCart setEnabled:YES];
     */
}

- (void)hideCart:(BOOL)isHide{
    /*
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    [((BaseViewViewController*)rootView).cartContainer setHidden:isHide];
    if (isHide == NO)
        [((BaseViewViewController*)rootView).btnCart setEnabled:YES];
     */
}

- (IBAction)btnCartPressed:(id)sender {
    @synchronized(self) {
//        [((BaseViewViewController*)rootView).btnCart setEnabled:NO];
//        ShoppingCartViewController* shoppingVC = [shoppingCartStoryboard instantiateViewControllerWithIdentifier:@"ShoppingCartViewController"];
//        
//        if (![CoreStringUtils isEmpty:[SessionManager sharedClient].sessionKey]) {
//            [bconnectAppDelegate.rootNavigation pushViewController:shoppingVC animated:YES];
//        } else {
//            LoginViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//            [[LoginManager sharedClient] gotoController:shoppingVC];
//            [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
//        }
    
    }
}

- (void)setCartPosition:(CGPoint)point{
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    ((BaseViewViewController*)rootView).cartBottomConstraint.constant = point.y;
    ((BaseViewViewController*)rootView).cartRightConstraint.constant = point.x;
    [((BaseViewViewController*)rootView).cartContainer layoutIfNeeded];
}

// Initialize the transition
- (void)createTransition {
    if (![rootView isKindOfClass:[BaseViewViewController class]]) {
        return;
    }
    bconnectAppDelegate.rootNavigation.delegate = self;
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    //self.transition = [[JTMaterialTransition alloc] initWithAnimatedView:((BaseViewViewController*)rootView).btnCart];
//    if ([toVC isKindOfClass:[ShoppingCartViewController class]]) {
//        self.transition.reverse = NO;
//        return self.transition;
//    }
    
    return  nil;
}

@end

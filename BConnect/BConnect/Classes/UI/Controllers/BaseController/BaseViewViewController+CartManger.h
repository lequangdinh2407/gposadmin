//
//  BaseViewViewController+CartManger.h
//  BConnect
//
//  Created by vu van long on 12/17/15.
//  Copyright © 2015 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"

@interface BaseViewViewController (CartManger)

- (void)setupCart;
+ (void)hideCartView:(BOOL)isHide;
- (void)hideCart:(BOOL)isHide;
- (IBAction)btnCartPressed:(id)sender;
- (void)setCartPosition:(CGPoint)point;

@end

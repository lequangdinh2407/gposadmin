//
//  BaseViewViewController.h
//  spabees
//
//  Created by DKMobility on 30/12/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import "LocationEntity.h"
#import "RTSpinKitView.h"
#import "XLPagerTabStripViewController.h"

#import "JSBadgeView.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "BaseView.h"
#import "REFrostedViewController.h"
#import "TMDatePickerViewController.h"
#import "NoItemView.h"
#import "FasttCameraViewController.h"
#import <JTMaterialTransition.h>
#import "Popup.h"


#import "DatabaseManager.h"
#import "SlidingViewController.h"

#define kHeaderItemWidth 40
#define kHeaderItemHeight 40
//const static int HeaderViewHeight = 64;
#define ARC4RANDOM_MAX      0x100000000

@interface BaseViewViewController : UIViewController<BaseViewDelegate, XLPagerTabStripChildItem /*, MainPageViewDelegate*/ , REFrostedViewControllerDelegate,TMDatePickerProtocol, UINavigationControllerDelegate>{
    bool isInit;
    bool isErrorNetwork;
    bool isLeavingViewController;
    int actionType;
    UIImage* icBackDefault;
    UIImage* icBackHighlighted;
}

#pragma mark -  Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightItemWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sideRightItemWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightItemContainerWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftItemWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backViewContainerWidthConstraint;
@property (weak, nonatomic) IBOutlet UIView *backButtonContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cartBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cartRightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *avatarWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *avatarTopConstraint;


#pragma mark -  Views, containers
@property (weak, nonatomic) IBOutlet UILabel *lbStatus;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *searchContainer;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *rightItemContainer;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

#pragma mark -  Items
@property (weak, nonatomic) IBOutlet UIView *headerBackground;
@property (weak, nonatomic) IBOutlet UIButton *btnLeftItem;
@property (weak, nonatomic) IBOutlet UIButton *btnRightItem;
@property (weak, nonatomic) IBOutlet UILabel *lbHeaderTittle;
@property (weak, nonatomic) IBOutlet UILabel *lbBackTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSideRightItem;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIImageView *icBack;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;

#pragma mark -  Picker view
@property (weak, nonatomic) IBOutlet UILabel *pickerContainerTitle;
@property (weak, nonatomic) IBOutlet UIView *pickerContainer;
@property (weak, nonatomic) IBOutlet UIDatePicker *dateTimePicker;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIView *calendarView;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerDone;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickerViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickerContentHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *imageOptionPicker;
@property (weak, nonatomic) IBOutlet UIView *videoOptionPicker;
@property (nonatomic, strong) FasttCameraViewController *fasttCameraViewController;
@property (strong, nonatomic) UIImagePickerController *cameraPicker;
@property (strong, nonatomic) UIImagePickerController *photosPicker;
@property (strong, nonatomic) TMDatePickerViewController *datePickerView;
@property (strong, nonatomic) NSMutableArray* pickerData;
@property (weak, nonatomic) id pickerDelegate;
@property (assign, nonatomic) NSInteger pickerID;

#pragma mark -  Cart view
@property (weak, nonatomic) IBOutlet UIView *cartContainer;
@property (weak, nonatomic) IBOutlet UIView *cartBadgeContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnCart;
@property (strong, nonatomic) JSBadgeView *cartBadger;
@property (assign, nonatomic) BOOL isHideCart;
@property (nonatomic, strong) JTMaterialTransition *transition;
@property UIScreenEdgePanGestureRecognizer *edgePanGestureRecognizer;
@property UIPercentDrivenInteractiveTransition *percentDrivenInteractiveTransition;
@property (weak, nonatomic) IBOutlet UIImageView *cartIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerHeightConstraint;


//@property (nonatomic, strong) YPBubbleTransition *transition;

#pragma mark -  NoItem view
@property (strong, nonatomic) NoItemView *noItemView;

#pragma mark -  Properties other
@property (strong, nonatomic) RTSpinKitView * waitingView;
@property (assign, nonatomic) NSInteger tag;
@property (assign, nonatomic) NSInteger retryTimes;
@property (assign, nonatomic) BOOL isFBLogin;
@property (assign, nonatomic) BOOL isGGLogin;
@property (assign, nonatomic) BOOL isSearching;
@property (assign, nonatomic) BOOL isProcessingRequest;
@property (weak, nonatomic) id mainPageDelegate;
@property (nonatomic, assign) REFrostedViewController *slidingViewController;
@property (strong, nonatomic) NSString* controllerTitle;


@property (assign) CGFloat topPadding;
@property (assign) CGFloat bottomPadding;
@property (assign) CGFloat HeaderViewHeight;
//@property (strong, nonatomic) HomberServiceCategoryEntity* homberServiceCategoryEntity;

- (void)showStatusContent:(NSString*)message statusColor:(UIColor*)color opacity:(CGFloat)opacity duration:(int)seconds;
- (void)dismissStatusMessage;
- (void)showMessageLabel:(NSString*)message;
- (void)dismissMessageLabel;
- (void)setupViewStyle:(UIView*)view;
- (void)presentViewController:(UIViewController*)controller WithAnimation:(NSString*)type;
- (void)dismissViewControllerWithAnimation:(NSString*)type;
- (void)flashingAnimation:(UIView*)view;

- (void)pushToViewController:(id)controller;
- (void)hideWaitingView:(BOOL)isHide;
- (void)pushSlideMenu;
- (void)addConstraintSubview:(UIView*)subview toSuperView:(UIView*)view;
- (Popup*)setupPopperWithSubView:(UIView*)subView;

#pragma mark -  Waiter processing methods
- (BOOL)isProcessing;
- (void)hideProgessHub:(BOOL)isHide;
- (void)showProgressHub;
- (void)dismissProgressHub;

#pragma mark -  Location methods

#pragma mark - Show error view
- (void)showNoItemView:(NSString*)text btnTitle:(NSString*)title;
- (void)showNoItemView:(NSString*)text btnTitle:(NSString*)title frame:(CGRect)frame;
- (void)showNoItemView:(NSString*)text btnTitle:(NSString*)title frame:(CGRect)frame imageName:(NSString*)imageName;
- (void)showNetworkErrorView;
- (void)hideNoItemView;
- (BOOL)isHideNoItemView;

#pragma mark - Lock View
- (void)lockAllView:(BOOL)isLock;

#pragma mark - DB Methods
- (void)insertDBLoginInfo:(DBSuccessCallback)success failure:(DBFailureCallback)failure;

@end

//
//  CountriesViewControllerV2.h
//  spabees
//
//  Created by Dragon on 2/26/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "BaseViewViewController.h"
#import "CountryEntityV2.h"

@protocol CountriesViewControllerV2Delegate

- (void)countrySelected:(CountryEntityV2*)entity;

@end

@interface CountriesViewControllerV2 : BaseViewViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>{
    NSArray *searchResults;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray* countriesList;
@property (weak, nonatomic) id delegate;

- (void)btnBackPressed:(id)sender;

@end
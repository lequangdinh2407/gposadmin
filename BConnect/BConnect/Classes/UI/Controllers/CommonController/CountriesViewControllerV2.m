//
//  CountriesViewControllerV2.m
//  spabees
//
//  Created by Dragon on 2/26/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "CountriesViewControllerV2.h"
#import "RequestManager.h"
#import "SVProgressHUD.h"

@interface CountriesViewControllerV2 ()

@end

@implementation CountriesViewControllerV2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
   
    [self getContriesList];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@"str_country_back_title"] target:self action:@selector(btnBackPressed:)];
    [self setRightItemWithImage:[UIImage imageNamed:@"btn_search"] target:self action:@selector(showSearchBar)];
}



- (void)getContriesList{
    [self showProgressHub];
     [[RequestManager sharedClient] requestCountriesListV2Request:^(NSMutableDictionary *results, int errorCode){
         [self dismissProgressHub];
         self.countriesList = [NSArray array];
         self.countriesList = [results objectForKey:RESPONSE_COUNTRIES];
         if (self.countriesList.count == 0) {
             [self showNetworkErrorView];
         } else {
             [self.tableView reloadData];
         }
     } failure:^(NSMutableDictionary *results, int errorCode){
         [Log d:0 tag:@"requestCountriesListV2Request failure" message:results];
         [self dismissProgressHub];
         [self showNetworkErrorView];
     }];
}

#pragma mark - NoItemViewDelegate methods
- (void)btnClickedNoItemView {
    [self hideNoItemView];
    [self getContriesList];
}

#pragma mark - Searchbar method
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self hiddenSearchBar];
    [self handleSearch:searchBar];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    //Handle input text here
    [self handleSearch:searchBar];
}

- (void)handleSearch:(UISearchBar *)searchBar {
    //check what was passed as the query String and get rid of the keyboard
    searchResults = [SearchUtils filter:searchBar.text originalList:[NSMutableArray arrayWithArray:self.countriesList]];
    [self.tableView reloadData];
    if (searchResults.count > 0) {
        [self hideNoItemView];
    } else {
        [self showNoItemView:[CoreStringUtils getStringRes:@"str_search_no_results"] btnTitle:@""];
    }
}

- (void)hiddenSearchBar {
    [super hiddenSearchBar];
    [self.tableView reloadData];
}

#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isSearching) {
        return searchResults.count;
    }
    return self.countriesList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"countryTableViewCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"countryTableViewCell"];
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        UIView* endLine = [[UIView alloc] initWithFrame:CGRectMake(0, 43, tableView.frame.size.width, 1)];
        endLine.backgroundColor = [UIColor lightGrayColor];
        endLine.alpha = 0.8;
        [cell.contentView addSubview:endLine];
        [cell.textLabel setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:14.0f]];
    }
    CountryEntityV2* entity = nil;
    if (self.isSearching) {
        entity = [searchResults objectAtIndex:indexPath.row];
    } else {
        entity = [self.countriesList objectAtIndex:indexPath.row];
    }
    cell.textLabel.text = entity.name;
//    if (self.countriesList.count-1 == indexPath.row) {
//        [self dismissProgressHub];
//    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.delegate respondsToSelector:@selector(countrySelected:)]) {
        CountryEntityV2 *entity = nil;
        if (self.isSearching) {
            entity = [searchResults objectAtIndex:indexPath.row];
        } else {
            entity = [self.countriesList objectAtIndex:indexPath.row];
        }
        [self.delegate countrySelected:entity];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (![CoreStringUtils isEmpty:((BaseViewViewController*)rootView).searchBar.text]) {
        [((BaseViewViewController*)rootView).searchBar resignFirstResponder];
    } else {
        [self hiddenSearchBar];
    }
}

@end

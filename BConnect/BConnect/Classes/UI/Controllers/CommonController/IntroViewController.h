//
//  IntroViewController.h
//  BConnect
//
//  Created by Dragon on 1/13/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"

@interface IntroViewController : BaseViewViewController
@property (nonatomic, strong) UIView* headerContainer;
@property (nonatomic, strong) UIView* headerShadow;
@property (nonatomic, strong) UIView* bgHeaderView;
@end

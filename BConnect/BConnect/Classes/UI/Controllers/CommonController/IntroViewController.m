//
//  IntroViewController.m
//  BConnect
//
//  Created by Dragon on 1/13/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import "IntroViewController.h"

@interface IntroViewController ()

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
}

- (void)setupHeader{
    [super setupHeader];
    [self hideHeaderView:YES];
    if (!self.headerContainer) {
        self.headerContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), HEADER_HEIGHT)];
        self.bgHeaderView = [[UIView alloc] initWithFrame:self.headerContainer.bounds];
        [self.bgHeaderView setBackgroundColor:[UIColor clearColor]];
        UIButton* btnBack = [[UIButton alloc] initWithFrame:CGRectMake(0, 20, 40, 40)];
        [btnBack setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
        [btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.headerContainer addSubview:self.bgHeaderView];
        [self.headerContainer addSubview:btnBack];
        [self.view addSubview:self.headerContainer];
    }
}

- (void)btnBackPressed:(id)sender{
    [super btnBackPressed:sender];
}

@end

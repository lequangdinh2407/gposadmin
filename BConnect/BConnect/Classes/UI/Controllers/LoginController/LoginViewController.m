//
//  LoginViewController.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/29/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "LoginViewController.h"
#import "CoreStringUtils.h"
#import "RequestErrorCode.h"


#import "CommentManager.h"
//#import "MainMapViewController.h"
#import "CountriesViewControllerV2.h"

#define ERROR_CONTAINER_HEIGH   30
#define TAG @"LoginViewController"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    NSLog(@"End: %f", [[NSDate date] timeIntervalSince1970]);
    [super viewDidLoad];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    if (!isInit) {
        [self prepareLayout];
        isInit = true;
    }
    
    /*
    if (!self.isFBLogin && !self.isGGLogin) {
        [self prepareLayout];
        [self initData];
    }
    [self.loginView lockAllButton:NO];
    
    
    if (![self.loginView.txtUserName.text isEqualToString:@""]) {
        [self.loginView.txtUserName becomeFirstResponder];
    }
     */
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.edgesForExtendedLayout=UIRectEdgeTop;
    [self setupHeader];
    
     [Helpers giveBorderWithCornerRadious:self.btnLogin radius:20 borderColor:[UIColor clearColor] andBorderWidth:0];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupHeader{
    [self hideHeaderView:YES];
}


#pragma mark - Business methods
- (void)prepareLayout{
    [Helpers giveBorderWithCornerRadious:self.usernameContainer radius:5 borderColor:[UIColor whiteColor] andBorderWidth:1.5];
    [Helpers giveBorderWithCornerRadious:self.passwordContainer radius:5 borderColor:[UIColor whiteColor] andBorderWidth:1.5];
    
    UIColor *color = [UIColor whiteColor];
    self.txtUsername.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    self.txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
   
    
}

- (void)initData {
    NSString* tmpCountryName = [SharePrefUtils getStringPreference:PREFKEY_COUNTRY_NAME_LOGIN];
    NSString* tmpCountryPhone = [SharePrefUtils getStringPreference:PREFKEY_COUNTRY_PHONE_LOGIN];
    if ([CoreStringUtils isEmpty:tmpCountryName] && [CoreStringUtils isEmpty:tmpCountryPhone]) {
        [SharePrefUtils writeStringPreference:PREFKEY_COUNTRY_CODE_LOGIN prefValue:COUNTRY_CODE_DEFAULT];
        [SharePrefUtils writeStringPreference:PREFKEY_COUNTRY_PHONE_LOGIN prefValue:COUNTRY_PHONE_DEFAULT];
        [SharePrefUtils writeStringPreference:PREFKEY_COUNTRY_NAME_LOGIN prefValue:COUNTRY_NAME_DEFAULT];
        
    }else{
       
    }
    
    if (![CoreStringUtils isEmpty:self.countryCode]) {
        
    }
    
    
}

- (IBAction)btnLoginPressed:(id)sender {
    //[self showProgressHub];
    //[self showMessage:nil];
    //[self popToSlidingViewController];
    [self requestLogin];
}


- (void)chooseCountryCode{
    [self showMessage:nil];
    CountriesViewControllerV2* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"CountriesViewControllerV2"];
    controller.delegate = self;
    [self pushToViewController:controller];
}

- (void)btnForgetPaswordPressed {
    [self showMessage:nil];

    [self.view endEditing:YES];
}

- (void)btnSignUpPressed{
    [self showMessage:nil];


}

- (void)btnFBLoginPressed{
    [self showMessage:nil];

    LoginViewController* loginController    = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    loginController.isFBLogin = YES;
    [self pushToViewController:loginController];
}

- (void)btnGGLoginPressed{
    [self showMessage:nil];

    LoginViewController* loginController    = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    loginController.isGGLogin = YES;
    [self pushToViewController:loginController];
}

- (void)countrySelected:(CountryEntityV2 *)entity{
    self.countryCode = nil;
    [SharePrefUtils writeStringPreference:PREFKEY_COUNTRY_CODE_LOGIN prefValue:entity.isoCode];
    [SharePrefUtils writeStringPreference:PREFKEY_COUNTRY_PHONE_LOGIN prefValue:entity.phoneCode];
    [SharePrefUtils writeStringPreference:PREFKEY_COUNTRY_NAME_LOGIN prefValue:entity.name];
  
}

- (void)showMessage:(NSString*)message{
    /*
    CGRect frame = self.loginView.frame;
    CGFloat height = [self.loginView showErrorMessage:message];
    frame.size.height = height;
    self.loginView.frame = frame;
     */
}

#pragma mark - Request methods

- (void)requestLogin {
    NSString* userName = self.txtUsername.text;
    NSString* password = self.txtPassword.text;
    NSString* imei = [Helpers getUDID];
    NSString* countryCode = nil;
    if (![CoreStringUtils isEmpty:self.countryCode])
        countryCode = self.countryCode;
    else
        countryCode = [SharePrefUtils getStringPreference:PREFKEY_COUNTRY_CODE_LOGIN];
    
    params = [[NSMutableDictionary alloc] init];
    [params setValue:userName forKey:REQUEST_SENDINFO];
    [params setValue:imei forKey:REQUEST_IMEI];
    [params setValue:countryCode forKey:REQUEST_COUNTRY_CODE];
    
    [self showProgressHub];
    self.btnLogin.enabled = false;
    [[RequestManager sharedClient] requestLoginEmail:userName password:password  success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestLogin success" message:results];
        [self dismissProgressHub];
        self.btnLogin.enabled = true;
         [SharePrefUtils writeStringPreference:PREFKEY_REGISTER_INFO prefValue:@""];
         [SharePrefUtils writeStringPreference:PREFKEY_ACCOUNT_LOGIN prefValue:self.txtUsername.text];
         AccountEntity *accountEntity = [[AccountEntity alloc] init];
         accountEntity.userName = userName;
         accountEntity.password = [Helpers encryptData:password key:SECRECT_KEY];
         accountEntity.userId = [SessionManager sharedClient].userEntity.userId;
         [SessionManager sharedClient].accountEntity = accountEntity;
        
        self.txtUsername.text = @"";
        self.txtPassword.text = @"";
        
         [self insertDBLoginInfo];
         [self getDataFromDB];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        //[Log d:0 tag:[NSString stringWithFormat:@"requestLogin failure %d", errorCode] message:results];
        [self dismissProgressHub];
        self.btnLogin.enabled = true;
        
        if (errorCode == PARAMS_INVALID) {
            [Helpers showToast:@"Invalid username or password"];
        } else {
            [Helpers showToast:@"Login failed"];
        }
    }
      ];
}

- (void)insertDBLoginInfo {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:[NSNumber numberWithInteger:ACTION_INSERT_ALL_LOGIN_INFO] forKey:DB_TYPE_ACTION];
    [data setValue:[SessionManager sharedClient].userEntity forKey:DB_USER_ENTITY];
    [data setValue:[SessionManager sharedClient].sessionKey forKey:DB_SESSION_KEY];
    [data setValue:[SessionManager sharedClient].domain forKey:DB_DOMAIN];
    [data setValue:[SessionManager sharedClient].refreshToken forKey:DB_REFRESH_TOKEN];
    //[data setValue:[SessionManager sharedClient].sockets forKey:PREFKEY_SOCKETS];
    //forKey:PREFKEY_UPLOADSOCKETS];
    [data setValue:[SessionManager sharedClient].accountEntity forKey:DB_ACCOUNT_ENTITY];
    
    NSInteger flag = (FLAG_DELETE_ACCOUNT | FLAG_DELETE_SESSION | FLAG_DELETE_USER);
    [data setValue:[NSNumber numberWithInteger:flag] forKey:DB_FLAG_ACTION];
    
    [[DatabaseManager sharedClient] addToQueue:data success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"insertDBLoginInfo success" message:results];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"insertDBLoginInfo failure" message:results];
    }];
    
    [SharePrefUtils writeBoolPreference:PREFKEY_LOGIN_FLAG prefValue:YES];
}

- (void)popToSlidingViewController{
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    for (UIViewController* controller in allViewControllers) {
        if ([controller isKindOfClass:[SlidingViewController class]]) {
            NSInteger preCVIndex = [allViewControllers indexOfObject:controller] - 1;
            preCVIndex = preCVIndex < 0 ? 0 : preCVIndex;
            [bconnectAppDelegate.rootNavigation popToViewController:[allViewControllers objectAtIndex:preCVIndex] animated:NO];
            return;
        }
    }
    [self pushSlideMenu];
}


- (void)requestSubmitToken {
    NSString *token = [SharePrefUtils getStringPreference:PREFKEY_TOKEN];
    NSString *currentToken = [SharePrefUtils getStringPreference:PREFKEY_CURRENT_TOKEN];
    
    if (![CoreStringUtils isEmpty:token] && [token isEqualToString:currentToken] == FALSE) {
        [[RequestManager sharedClient] requestSubmitTokenPNS:[SessionManager sharedClient].sessionKey imei:[Helpers getUDID] token:token success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestSubmitToken success" message:results];
            [SharePrefUtils writeStringPreference:PREFKEY_CURRENT_TOKEN prefValue:token];
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestSubmitToken failure" message:results];
        }];
    }
}

- (void)getDataFromDB{
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:[NSNumber numberWithInteger:ACTION_GET_DATA_INIT_APP] forKey:DB_TYPE_ACTION];
    NSInteger flag = 0;
    [data setValue:[NSNumber numberWithInteger:flag] forKey:DB_FLAG_ACTION];
    
    [[DatabaseManager sharedClient] addToQueue:data success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"getDataFromDB success" message:results];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOGIN object:nil];
   

        [self popToSlidingViewController];

    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"getDataFromDB failure" message:results];
        
        [self pushSlideMenu];
        [self dismissProgressHub];
    }];
}

- (void)btnBackPressed:(id)sender {
    [[LoginManager sharedClient] reset];
    //[super btnBackPressed:sender];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.txtUsername]) {
        [self.txtUsername resignFirstResponder];
        [self.txtPassword becomeFirstResponder];
    }else{
        [self.view endEditing:YES];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.view endEditing:YES];
}

@end

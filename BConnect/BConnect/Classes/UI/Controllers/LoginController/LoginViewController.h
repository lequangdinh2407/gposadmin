//
//  LoginViewController.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/29/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "BaseViewViewController.h"

#import "TPKeyboardAvoidingScrollView.h"

#import "CoreConstants.h"
#import "Helpers.h"
#import "SessionManager.h"
#import "EntityManager.h"
#import "RequestManager.h"
#import "DatabaseManager.h"
#import "SVProgressHUD.h"
#import "Masonry.h"



@interface LoginViewController : BaseViewViewController {
    NSMutableDictionary *params;
    UIView *loadingView;
    BOOL isGoogleLoging;
    BOOL isFacebookLoging;
}

@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;


@property (weak, nonatomic) IBOutlet UIView *usernameContainer;
@property (weak, nonatomic) IBOutlet UIView *passwordContainer;


@property (weak, nonatomic) IBOutlet UILabel *loginHeaderTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginBack;

@property (nonatomic, assign) NSInteger registerErrorCode;
@property (nonatomic, strong) NSString *sendInfo;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *countryPhone;
@property (nonatomic, strong) NSString *countryName;


@property (assign, nonatomic) BOOL isBackFromRegisterVC;

- (void)insertDBLoginInfo;
- (void)getDataFromDB;
- (IBAction)btnBackPressed:(id)sender;
- (void)showMessage:(NSString*)message;

@end

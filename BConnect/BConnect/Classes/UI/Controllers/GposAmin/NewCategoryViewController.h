//
//  NewCategoryViewController.h
//  BConnect
//
//  Created by Inglab on 13/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "GposTextFieldView.h"
#import "PosCategoryEntity.h"
NS_ASSUME_NONNULL_BEGIN

@protocol NewCategoryViewControllerDelegate

- (void)updateCategory:(int)category name:(NSString*)name;
- (void)reloadCategory;

@end

@interface NewCategoryViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtName;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (assign, nonatomic) BOOL isAddNew;
@property (strong, nonatomic) PosCategoryEntity* entity;
@property (weak, nonatomic) id delegate;

@end

NS_ASSUME_NONNULL_END

//
//  ChangePasswordViewController.m
//  BConnect
//
//  Created by Inglab on 16/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!isInit) {
        [self prepareLayout];
        isInit = true;
    }
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Change your password"];
    
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
}

- (void)prepareLayout {
    
    [self.txtOldPassword setName:@"Old Password"];
    [self.txtNewPassword setName:@"New Password"];
    [self.txtConfirmNewPassword setName:@"Confirm New Password"];
    
    self.txtOldPassword.textField.delegate = self;
    self.txtNewPassword.textField.delegate = self;
    self.txtConfirmNewPassword.textField.delegate = self;
    
    [self.txtOldPassword setPasswordTextField];
    [self.txtNewPassword setPasswordTextField];
    [self.txtConfirmNewPassword setPasswordTextField];
    
}

- (IBAction)btnSavePressed:(id)sender {
    [self showProgressHub];
    [[RequestManager sharedClient] requestChangePassword:self.storeOwnerEntity.Id curPass:self.txtOldPassword.textField.text newPass:self.txtNewPassword.textField.text rePass:self.txtConfirmNewPassword.textField.text type:2  success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestChangePassword success" message:results];
        
        [self dismissProgressHub];
        [self btnBackPressed:nil];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestChangePassword failure" message:results];
        
        [self dismissProgressHub];
    }];
}

#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.txtOldPassword.textField]) {
        [self.txtOldPassword.textField resignFirstResponder];
        [self.txtNewPassword.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtNewPassword.textField]) {
        [self.txtNewPassword.textField resignFirstResponder];
        [self.txtConfirmNewPassword.textField becomeFirstResponder];
    }else {
        [self.view endEditing:YES];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

@end

//
//  ReportOrderViewController.m
//  BConnect
//
//  Created by Inglab on 06/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ReportOrderViewController.h"
#import "ReportOrderDetailViewController.h"
#import "ReportOrderTableViewCell.h"
#import "SaleReportsEntity.h"
#import "StatisticView.h"

@interface ReportOrderViewController ()

@end

@implementation ReportOrderViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor clearColor];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isInit) {
         [self getReportList];
         isInit = true;
    }
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Order Report"];
    
}


//data

- (void)getReportList {
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetSaleReports:self.startDate endDate:self.endDate success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSaleReports success" message:results];
        [self dismissProgressHub];
        self.saleReportEntity = [results objectForKey:RESPONSE_DATA];
        
        self.reportList = self.saleReportEntity.SaleOrders;
        
        if (self.reportList.count == 0) {
            [self showNoItemView:[CoreStringUtils getStringRes:@"str_no_data"] btnTitle:nil];
        } else {
            [self hideNoItemView];
        }
        [self.tableView reloadData];

    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSaleReports failure" message:results];
        [self dismissProgressHub];
        [self showNetworkErrorView];
    }];
}

#pragma mark - NoItemViewDelegate methods
- (void)btnClickedNoItemView {
    [self hideNoItemView];
    [self getReportList];
}

#pragma mark - tableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 70;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    //if (!self.skuDetailView) {
        CGFloat viewHeight = 70;
        StatisticView* statisticView = [[StatisticView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, viewHeight)];
    
        [statisticView lbHeader1:@"Grand Total" lbContent1:[NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, self.saleReportEntity.GrandTotal] lbHeader2:@"Tips" lbContent2:[NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, self.saleReportEntity.Tips] lbHeader3:@"Sale Tax" lbContent3:[NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, self.saleReportEntity.SaleTax]];
        [statisticView layoutIfNeeded];
    
    
    //}
    return statisticView;
}


 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.reportList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 230;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ReportOrderTableViewCell *cell = (ReportOrderTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ReportOrderTableViewCell"];
    
    if(!cell){
        cell = [[ReportOrderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReportOrderTableViewCell"];
    }
    
    SaleOrderEntity* entity = [self.reportList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    ReportOrderDetailViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"ReportOrderDetailViewController"];
    //controller.entity = [self.deviceUsersList objectAtIndex:indexPath.row];
    SaleOrderEntity* entity = [self.reportList objectAtIndex:indexPath.row];
    controller.reportList = entity.SaleOrderItems;
    controller.saleOrderEntity = entity;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    
}


@end

//
//  ModifierViewController.m
//  BConnect
//
//  Created by Inglab on 27/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ModifierViewController.h"
#import "ModifierTableViewCell.h"
#import "ShortFormPosCategoryEntity.h"

@interface ModifierViewController ()

@end

@implementation ModifierViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.allowsSelection = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    

}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (!isInit) {
        [self getModifierList];
        isInit = true;
    }
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Modifier"];
   
}

- (void)btnBackPressed:(id)sender{
    
    NSMutableArray* modifiersList = [[NSMutableArray alloc] init];
    NSMutableArray* selectedModifiersList = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.modifidersList.count; i++) {
        ModifierEntity* entity = [self.modifidersList objectAtIndex:i];
        if (entity.isSelected) {
            ShortFormPosCategoryEntity* category = [[ShortFormPosCategoryEntity alloc] init];
            category.Id = entity.Id;
            category.Name = entity.Name;
            
            NSDictionary* dictionary = [ShortFormPosCategoryEntity dictionaryWithPropertiesOfObject:category];
            
            [modifiersList addObject:dictionary];
            [selectedModifiersList addObject:entity];
        }
    }
    
    [self showProgressHub];
    [[RequestManager sharedClient] requestEditCategoryDetail:self.categoryEntity.Id Name:self.categoryEntity.Name Modifiders:modifiersList updateModifier:true success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestEditCategoryDetail success" message:results];
        
        self.categoryEntity.Modifiers = selectedModifiersList;
        
        if ([self.delegate respondsToSelector:@selector(updateCategory:)]) {
            [self.delegate updateCategory:self.categoryEntity];
        }
        
        [self dismissProgressHub];
        [super btnBackPressed:sender];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestEditCategoryDetail failure" message:results];
        [self dismissProgressHub];
        [super btnBackPressed:sender];
    }];
}

//data

- (void)getModifierList {
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetModifiers:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetModifiers success" message:results];
        [self dismissProgressHub];
        self.modifidersList = [results objectForKey:RESPONSE_DATA];
        
        for (int i = 0; i < self.modifidersList.count; i++) {
            
            ModifierEntity* entity = [self.modifidersList objectAtIndex:i];
            for (int j = 0; j < self.selectedModifidersList.count; j++) {
                ModifierEntity* modifier = [self.selectedModifidersList objectAtIndex:j];
                if (modifier.Id == entity.Id) {
                    entity.isSelected = true;
                }
            }
        }
        
        if (self.modifidersList.count == 0) {
            [self showNoItemView:[CoreStringUtils getStringRes:@"str_no_data"] btnTitle:nil];
        } else {
            [self hideNoItemView];
        }
        [self.tableView reloadData];
        
       
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetModifiers failure" message:results];
        [self dismissProgressHub];
    }];
}

#pragma mark - NoItemViewDelegate methods
- (void)btnClickedNoItemView {
    [self hideNoItemView];
    [self getModifierList];
}

#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.modifidersList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ModifierTableViewCell *cell = (ModifierTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ModifierTableViewCell"];
    
    if(!cell){
        cell = [[ModifierTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ModifierTableViewCell"];
    }
    
    ModifierEntity* entity = [self.modifidersList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}


@end

//
//  NewCategoryViewController.m
//  BConnect
//
//  Created by Inglab on 13/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "NewCategoryViewController.h"
#import "RequestManager.h"

@interface NewCategoryViewController ()

@end

@implementation NewCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!isInit) {
        isInit = true;
        [self prepareLayout];
    }
}

-(void)prepareLayout{
    [self.txtName setName:@"Name"];
    if (!self.isAddNew) {
        [self.txtName setValue:self.entity.Name];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    
    if (self.isAddNew) {
        [self SetHeaderTitle:@"New Category"];
    } else {
        [self SetHeaderTitle:@"Category"];
    }
    
    
}

- (IBAction)btnSavePressed:(id)sender {
    if (self.isAddNew) {
        [self showProgressHub];
        [[RequestManager sharedClient] requestAddCategory:self.txtName.textField.text success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestFindAddress success" message:results];
            
            if ([self.delegate respondsToSelector:@selector(reloadCategory)]){
                [self.delegate reloadCategory];
            }
            [self dismissProgressHub];
            [self btnBackPressed:nil];
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestFindAddress failure" message:results];
            [self dismissProgressHub];
        }];
    } else {
        [self showProgressHub];
        [[RequestManager sharedClient] requestEditCategoryDetail:self.entity.Id Name:self.txtName.textField.text Modifiders:nil updateModifier:false  success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestEditCategoryDetail success" message:results];
            
            if ([self.delegate respondsToSelector:@selector(updateCategory:name:)]){
                [self.delegate updateCategory:self.entity.Id  name:self.txtName.textField.text];
            }
            [self dismissProgressHub];
            [self btnBackPressed:nil];
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestEditCategoryDetail failure" message:results];
            [self dismissProgressHub];
        }];
    }
}

@end

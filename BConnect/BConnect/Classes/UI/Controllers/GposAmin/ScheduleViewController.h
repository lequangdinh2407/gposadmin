//
//  ScheduleViewController.h
//  BConnect
//
//  Created by Inglab on 28/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "Popup.h"
#import "SchedulePopupView.h"

@protocol ScheduleViewControllerDelegate

- (void)saveWorkingDays:(NSString*)WorkingDays;
@end

@interface ScheduleViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* schedulesList;
@property (weak, nonatomic) id delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbProviderName;
@property (strong, nonatomic) NSString* providerName;

@property (strong, nonatomic) NSString* workingDays;
@property (strong, nonatomic) SchedulePopupView* schedulePopUpView;
@property (strong, nonatomic) Popup *popper;
@property (weak, nonatomic) IBOutlet UIButton *btnCover;

@property (assign, nonatomic) int currentPicker;
@property (strong, nonatomic) NSIndexPath* currentSelectedIndex;
@property (strong, nonatomic) NSDate* currentFromTime;
@property (strong, nonatomic) NSDate* currentToTime;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@end


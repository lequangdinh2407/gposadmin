//
//  ReportOrderViewController.h
//  BConnect
//
//  Created by Inglab on 06/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "SaleReportsEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportOrderViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray* reportList;
@property (strong, nonatomic) SaleReportsEntity* saleReportEntity;

@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) NSString* startDate;
@property (strong, nonatomic) NSString* endDate;

@end

NS_ASSUME_NONNULL_END

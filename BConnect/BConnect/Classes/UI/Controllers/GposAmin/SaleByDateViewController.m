//
//  SaleByDateViewController.m
//  BConnect
//
//  Created by Inglab on 05/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "SaleByDateViewController.h"
#import "XJYChart.h"
#import "SaleReportsEntity.h"
#import "DaySaleReportEntity.h"

@interface SaleByDateViewController ()

@property (strong, nonatomic) NSMutableArray* nameList;
@property (strong, nonatomic) NSMutableArray* valueList;
@property (assign, nonatomic) double topNumber;
@property (assign, nonatomic) double bottomNumber;

@end

@implementation SaleByDateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.nameList = [[NSMutableArray alloc] init];
    self.valueList = [[NSMutableArray alloc] init];
}

-(void)viewDidAppear:(BOOL)animated {
    if (!isInit) {
        [self getReportSaleByDate];
        isInit = true;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    
    //[self getReportList];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Sale By Date Report"];
}



//data
- (void)getReportSaleByDate {
    
    self.bottomNumber = 0;
    self.topNumber = 0;
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetSaleReports:self.startDate endDate:self.endDate success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSaleReports success" message:results];
        [self dismissProgressHub];
        SaleReportsEntity* entity = [results objectForKey:RESPONSE_DATA];
        NSMutableArray* reportList = entity.DaySaleReports;
        for (int i = 0; i < reportList.count; i++) {
            DaySaleReportEntity* reportEntity = [reportList objectAtIndex:i];
            [self.nameList addObject: reportEntity.Date];
            [self.valueList addObject:@(reportEntity.GrandTotal)];
            if (reportEntity.GrandTotal > self.topNumber) {
                self.topNumber = reportEntity.GrandTotal;
            }
        }
        
        if (reportList.count > 0) {
            [self drawChart];
        }
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSaleReports failure" message:results];
        [self dismissProgressHub];
    }];
    
    
}

-(void)drawChart  {
    XLineChartItem* item =
    [[XLineChartItem alloc] initWithDataNumberArray:self.valueList
                                              color:[UIColor salmonColor]];
    XNormalLineChartConfiguration* configuration =
    [[XNormalLineChartConfiguration alloc] init];
    configuration.isShowShadow = NO;
    configuration.isEnableNumberAnimation = NO;
    configuration.lineMode = CurveLine;
    //    configuration.isEnableNumberLabel = YES;
    XLineChart* lineChart =
    [[XLineChart alloc] initWithFrame:CGRectMake(0, 100, [UIScreen mainScreen].bounds.size.width , 300)
                        dataItemArray:[NSMutableArray arrayWithObject:item]
                    dataDiscribeArray:self.nameList
     
     
                            topNumber:@([self generateTopNumber:self.topNumber])
                         bottomNumber:@0
                            graphMode:MutiLineGraph
                   chartConfiguration:configuration];
    lineChart.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:lineChart];
}

-(int)generateTopNumber:(double)originalNumber {
    int count = 1;
    while (originalNumber > 10) {
        originalNumber = originalNumber / 10;
        count = count*10;
    }
    
    if (originalNumber > 5) {
        return 10*count;
    } else {
        return (int)(originalNumber + 2) * count;
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  ReportProviderDetailViewController.m
//  BConnect
//
//  Created by Inglab on 09/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ReportProviderDetailViewController.h"
#import "OrderItemTableViewCell.h"
#import "StatisticView.h"

@interface ReportProviderDetailViewController ()

@end

@implementation ReportProviderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.allowsSelection = false;
    self.tableView.backgroundColor = [UIColor clearColor];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    [self.tableView reloadData];
    
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Provider Detail"];
    
}

//data

/*
- (void)getReportList {
    [[RequestManager sharedClient] requestGetProviderDetailReports:self.providerId startDate:self.startDate endDate:self.endDate success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetProviderReports success" message:results];
        self.reportList = [results objectForKey:RESPONSE_DATA];
        [self.tableView reloadData];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetProviderReports failure" message:results];
        //[Helpers showToast:@"Get salon info failed"];
    }];
}

 */

#pragma mark - tableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 100;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    //if (!self.skuDetailView) {
    
    CGFloat viewHeight = 50;
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, viewHeight + 30)];
    
    UILabel* lbOrderNumber = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
    [lbOrderNumber setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:22.0]];
    [lbOrderNumber setTextColor:[UIColor whiteColor]];
    [lbOrderNumber setBackgroundColor:[UIColor clearColor]];
    [lbOrderNumber setTextAlignment:NSTextAlignmentCenter];
    [headerView addSubview:lbOrderNumber];
    
    StatisticView* statisticView = [[StatisticView alloc] initWithFrame:CGRectMake(0, 30, [UIScreen mainScreen].bounds.size.width , viewHeight)];
    
    [statisticView lbHeader1:@"Total Sales" lbContent1:[NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, self.entity.Sales] lbHeader2:@"Total Tips" lbContent2:[NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, self.entity.Tips] lbHeader3:@"Total Commission" lbContent3:[NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, self.entity.Commission]];
    lbOrderNumber.text = self.entity.ProviderName;
    
    [statisticView layoutIfNeeded];
    
    [headerView addSubview:statisticView];
    
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(20, 100, [UIScreen mainScreen].bounds.size.width - 40 , 1)];
    line.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:line];
    //}
    return headerView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.reportList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 180;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderItemTableViewCell *cell = (OrderItemTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"OrderItemTableViewCell"];
    
    if(!cell){
        cell = [[OrderItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OrderItemTableViewCell"];
    }
    
    OrderItemEntity* entity = [self.reportList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

@end

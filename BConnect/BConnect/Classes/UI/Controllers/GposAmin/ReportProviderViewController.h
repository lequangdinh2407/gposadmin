//
//  ReportProviderViewController.h
//  BConnect
//
//  Created by Inglab on 09/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportProviderViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray* reportList;
@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) NSString* startDate;
@property (strong, nonatomic) NSString* endDate;

@end

NS_ASSUME_NONNULL_END

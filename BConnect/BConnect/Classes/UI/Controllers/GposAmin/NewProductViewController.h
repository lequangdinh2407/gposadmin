//
//  NewProductViewController.h
//  BConnect
//
//  Created by Inglab on 13/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "GposTextFieldView.h"
NS_ASSUME_NONNULL_BEGIN

@protocol NewProductViewControllerDelegate
- (void)refreshProductList;
//- (void)editProductDone:(ProductEntity*)product;
@end


@interface NewProductViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtName;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtPrice;
@property (weak, nonatomic) IBOutlet UISwitch *switchActive;
@property (strong, nonatomic) NSString* uploadedImageUrl;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

@property (assign, nonatomic) int categoryId;
@property (weak, nonatomic) id delegate;

@end

NS_ASSUME_NONNULL_END

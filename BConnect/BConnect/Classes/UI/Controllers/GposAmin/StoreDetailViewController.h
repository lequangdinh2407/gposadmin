//
//  StoreDetailViewController.h
//  BConnect
//
//  Created by Inglab on 03/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "StoreDetailView.h"
#import "ProviderEntity.h"
#import "TPKeyboardAvoidingScrollView.h"

@protocol StoreDetailViewControllerDelegate

- (void)refreshData;
- (void)editStoreDone:(GlobalStoreEntity*)store;

@end


@interface StoreDetailViewController : BaseViewViewController

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollWidthConstraint;
@property (strong, nonatomic) StoreDetailView* storeDetailView;

@property (strong, nonatomic) GlobalStoreEntity* entity;
@property (nonatomic, assign) BOOL isAddNew;

@property (nonatomic) id delegate;

@end



//
//  ModifierViewController.h
//  BConnect
//
//  Created by Inglab on 27/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "PosCategoryEntity.h"

@protocol ModifierViewControllerDelegate

- (void)updateCategory:(PosCategoryEntity*)category;

@end

@interface ModifierViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray* modifidersList;
@property (strong, nonatomic) NSArray* selectedModifidersList;
@property (strong, nonatomic) PosCategoryEntity* categoryEntity;
@property (weak, nonatomic) id delegate;
@end

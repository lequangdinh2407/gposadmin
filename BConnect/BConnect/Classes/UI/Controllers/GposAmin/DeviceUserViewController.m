//
//  DeviceUserViewController.m
//  BConnect
//
//  Created by Inglab on 22/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "DeviceUserViewController.h"
#import "DeviceUserTableViewCell.h"
#import "AdminDetailViewController.h"

@interface DeviceUserViewController ()

@end

@implementation DeviceUserViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor clearColor];
    
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        if (!weakSelf.isProcessingRequest) {
            [weakSelf.tableView.pullToRefreshView startAnimating];
            [weakSelf refreshData];
        } else {
            [weakSelf.tableView.pullToRefreshView stopAnimating];
        }
    }];
}

-(void)refreshData {
    [self getDeviceUsersList];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
   
    
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isInit) {
        [self getDeviceUsersList];
        isInit = true;
    }
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Device User"];
    [self setRightItemWithImage:[UIImage imageNamed:@"btn_add"] target:self action:@selector(addNewDeviceUser)];
}



-(void)addNewDeviceUser {
    AdminDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"AdminDetailViewController"];
    controller.isAddNew = true;
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    
}
//data

- (void)getDeviceUsersList {
    
    if (self.isProcessingRequest)
        return;
    
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetDeviceUsers:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetDeviceUsersList success" message:results];
        [self dismissProgressHub];
        self.deviceUsersList = [results objectForKey:RESPONSE_DATA];
        [self dismissProgressHub];
        
        if (self.deviceUsersList.count == 0) {
            [self showNoItemView:[CoreStringUtils getStringRes:@"str_no_data"] btnTitle:nil];
        } else {
            [self hideNoItemView];
        }
        
        [self.tableView.pullToRefreshView stopAnimating];
        
        [self.tableView reloadData];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetDeviceUsersList failure" message:results];
        [self dismissProgressHub];
        [self showNetworkErrorView];
    }];
}

#pragma mark - NoItemViewDelegate methods
- (void)btnClickedNoItemView {
    [self hideNoItemView];
    [self getDeviceUsersList];
}

#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.deviceUsersList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 175;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DeviceUserTableViewCell *cell = (DeviceUserTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"DeviceUserTableViewCell"];
    
    if(!cell){
        cell = [[DeviceUserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DeviceUserTableViewCell"];
    }
    
    DeviceUserEntity* entity = [self.deviceUsersList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    DeviceUserEntity* entity = [self.deviceUsersList objectAtIndex:indexPath.row];
    AdminDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"AdminDetailViewController"];
    controller.entity = entity;
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

- (void)deleteDeviceUser:(DeviceUserEntity*)entity {
    for (int i = 0; i < self.deviceUsersList.count; i++) {
        DeviceUserEntity* deviceUser = [self.deviceUsersList objectAtIndex:i];
        if (entity.Id == deviceUser.Id) {
            //[self.deviceUsersList replaceObjectAtIndex:i withObject:product];
            [self.deviceUsersList removeObjectAtIndex:i];
            [self.tableView reloadData];
            //[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

- (void)addDeviceUser:(DeviceUserEntity*)entity {
    [self.deviceUsersList addObject:entity];
    [self.tableView reloadData];
}

- (void)updateDeviceUser:(DeviceUserEntity*)entity {
    for (int i = 0; i < self.deviceUsersList.count; i++) {
        DeviceUserEntity* deviceUser = [self.deviceUsersList objectAtIndex:i];
        if (entity.Id == deviceUser.Id) {
            [self.deviceUsersList replaceObjectAtIndex:i withObject:entity];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

@end

//
//  DeviceUserViewController.h
//  BConnect
//
//  Created by Inglab on 22/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewViewController.h"



@interface DeviceUserViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* deviceUsersList;
@property (weak, nonatomic) id delegate;

@end



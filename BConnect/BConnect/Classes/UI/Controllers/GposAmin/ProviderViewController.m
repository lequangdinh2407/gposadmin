//
//  ProviderViewController.m
//  BConnect
//
//  Created by Inglab on 24/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ProviderViewController.h"
#import "ProviderTableViewCell.h"
#import "ProviderDetailViewController.h"

@interface ProviderViewController ()

@end

@implementation ProviderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor clearColor];
    
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        if (!weakSelf.isProcessingRequest) {
            [weakSelf.tableView.pullToRefreshView startAnimating];
            [weakSelf refreshData];
        } else {
            [weakSelf.tableView.pullToRefreshView stopAnimating];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
 
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isInit) {
        [self getProviderList];
        isInit = true;
    }
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Provider"];
    [self setRightItemWithImage:[UIImage imageNamed:@"btn_add"] target:self action:@selector(addNewProvider)];
}

- (void)addNewProvider{
    ProviderDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"ProviderDetailViewController"];
    controller.isAddNewProvider = true;
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}



//data

- (void)getProviderList {
    if (self.isProcessingRequest)
        return;
    
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetProvider:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetproviderList success" message:results];
        [self dismissProgressHub];
        self.providerList = [results objectForKey:RESPONSE_DATA];
        
        if (self.providerList.count == 0) {
            [self showNoItemView:[CoreStringUtils getStringRes:@"str_no_data"] btnTitle:nil];
        } else {
            [self hideNoItemView];
        }
        
        [self.tableView.pullToRefreshView stopAnimating];
       
        [self.tableView reloadData];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetproviderList failure" message:results];
        //[Helpers showToast:@"Get salon info failed"];
        [self dismissProgressHub];
        [self showNetworkErrorView];
    }];
}

#pragma mark - NoItemViewDelegate methods
- (void)btnClickedNoItemView {
    [self hideNoItemView];
    [self getProviderList];
}

#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.providerList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 135;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ProviderTableViewCell *cell = (ProviderTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ProviderTableViewCell"];
    
    if(!cell){
        cell = [[ProviderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ProviderTableViewCell"];
    }
    
    ProviderEntity* entity = [self.providerList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [self.tableView deselectRowAtIndexPath:indexPath animated:false];
    
    ProviderDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"ProviderDetailViewController"];
    controller.entity = [self.providerList objectAtIndex:indexPath.row];
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}


//delegate

- (void)updateProvider:(ProviderEntity*)entity {
    
    for (int i = 0; i < self.providerList.count; i++) {
        ProviderEntity* provider = [self.providerList objectAtIndex:i];
        if (provider.Id == entity.Id) {
            [self.providerList replaceObjectAtIndex:i withObject:entity];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

-(void)refreshData {
    [self getProviderList];
}

@end

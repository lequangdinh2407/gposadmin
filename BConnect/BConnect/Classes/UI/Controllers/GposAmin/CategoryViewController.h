//
//  CategoryViewController.h
//  BConnect
//
//  Created by Inglab on 24/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "PosCategoryEntity.h"


NS_ASSUME_NONNULL_BEGIN

@interface CategoryViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>

//@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UITableView* tableView;

@property (strong, nonatomic) NSArray* categoriesList;
@property (strong, nonatomic) NSMutableArray* categoryNameList;
@property (weak, nonatomic) id delegate;

@end

NS_ASSUME_NONNULL_END

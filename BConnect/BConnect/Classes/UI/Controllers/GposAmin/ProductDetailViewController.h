//
//  ProductDetailViewController.h
//  BConnect
//
//  Created by Inglab on 30/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ProductDetailView.h"
#import "ProductEntity.h"

@protocol ProductDetailViewControllerDelegate
- (void)refreshProductList;
- (void)editProductDone:(ProductEntity*)product;
@end

@interface ProductDetailViewController : BaseViewViewController<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollWidthConstraint;
@property (strong, nonatomic) ProductDetailView* productDetailView;
@property (strong, nonatomic) ProductEntity* entity;
@property (strong, nonatomic) NSMutableArray* categoryNameList;
@property (strong, nonatomic) NSString* uploadedImageUrl;
@property (weak, nonatomic) id delegate;

@end


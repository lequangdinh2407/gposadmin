//
//  CustomerListViewController.m
//  BConnect
//
//  Created by Inglab on 21/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "CustomerListViewController.h"
#import "CustomerTableViewCell.h"
#import "CustomerEntity.h"

@interface CustomerListViewController ()

@end

@implementation CustomerListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.allowsSelection = false;
    
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        if (!weakSelf.isProcessingRequest) {
            [weakSelf.tableView.pullToRefreshView startAnimating];
            [weakSelf refreshData];
        } else {
            [weakSelf.tableView.pullToRefreshView stopAnimating];
        }
    }];
}

-(void)refreshData {
    [self getCustomerList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    [self getCustomerList];
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchBar setTintColor:[UIColor whiteColor]];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Customer List"];
    //[self setRightItemWithImage:[UIImage imageNamed:@"btn_search"] target:self action:@selector(showSearchBar)];
}



//data

- (void)getCustomerList {
    
    if (self.isProcessingRequest)
        return;
    
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetCustomers:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetCustomerList success" message:results];
        [self dismissProgressHub];
        self.customersList = [results objectForKey:RESPONSE_DATA];
        
        if (self.customersList.count == 0) {
            [self showNoItemView:[CoreStringUtils getStringRes:@"str_no_data"] btnTitle:nil];
        } else {
            [self hideNoItemView];
        }
        
        [self.tableView.pullToRefreshView stopAnimating];
        
        [self.tableView reloadData];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetCustomerList failure" message:results];
        [self dismissProgressHub];
        [self showNetworkErrorView];
    }];
}

#pragma mark - NoItemViewDelegate methods
- (void)btnClickedNoItemView {
    [self hideNoItemView];
    [self getCustomerList];
}

#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    return self.customersList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 230;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CustomerTableViewCell *cell = (CustomerTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CustomerTableViewCell"];
    
    if(!cell){
        cell = [[CustomerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CustomerTableViewCell"];
    }
    
    CustomerEntity* entity = [self.customersList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //[tableView deselectRowAtIndexPath:indexPath animated:false];
    /*
    if ([self.delegate respondsToSelector:@selector(citySelected:)]) {
        CityEntity* city = nil;
        if (self.isSearching && ![CoreStringUtils isEmpty:self.searchBar.text]) {
            city = [searchResults objectAtIndex:indexPath.row];
        } else {
            city = [self.citiesList objectAtIndex:indexPath.row];
        }
        [self.delegate citySelected:city];
    }
     */
    //[self.navigationController popViewControllerAnimated:YES];
}


@end

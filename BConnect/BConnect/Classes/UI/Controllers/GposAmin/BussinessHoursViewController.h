//
//  BussinessHoursViewController.h
//  BConnect
//
//  Created by Inglab on 11/11/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "SchedulePopupView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BussinessHoursViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>{
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* bussinessHoursList;
@property (weak, nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UILabel *lbWork;
@property (weak, nonatomic) IBOutlet UILabel *lbTime;

@property (strong, nonatomic) SchedulePopupView* schedulePopUpView;
@property (weak, nonatomic) IBOutlet UIButton *btnCover;

@property (assign, nonatomic) int currentPicker;
@property (strong, nonatomic) NSIndexPath* currentSelectedIndex;
@property (strong, nonatomic) NSDate* currentFromTime;
@property (strong, nonatomic) NSDate* currentToTime;

@end

NS_ASSUME_NONNULL_END

//
//  ReportViewController.h
//  BConnect
//
//  Created by Inglab on 05/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "GposTextFieldView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportViewController : BaseViewViewController
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtFrom;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtTo;

@property (weak, nonatomic) IBOutlet UIView *saleByDateView;
@property (weak, nonatomic) IBOutlet UIView *paymentView;
@property (weak, nonatomic) IBOutlet UIView *providerView;
@property (weak, nonatomic) IBOutlet UIView *orderView;

@property (strong, nonatomic) NSDate* currentFromDate;
@property (strong, nonatomic) NSDate* currentToDate;
@property (assign, nonatomic) int currentPicker;

@end

NS_ASSUME_NONNULL_END

//
//  ReportOrderDetailViewController.m
//  BConnect
//
//  Created by Inglab on 02/11/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ReportOrderDetailViewController.h"
#import "OrderDetailTableViewCell.h"
#import "StatisticView.h"

@interface ReportOrderDetailViewController ()

@end

@implementation ReportOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor clearColor];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    
    //[self getReportList];
}


-(void)viewDidAppear:(BOOL)animated {
   [super viewDidAppear:animated];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Order Detail Report"];
}


#pragma mark - tableView methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 120;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    //if (!self.skuDetailView) {
    
    CGFloat viewHeight = 50;
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, viewHeight + 30)];
    
    UILabel* lbOrderNumber = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
    [lbOrderNumber setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:22.0]];
    [lbOrderNumber setTextColor:[UIColor whiteColor]];
    [lbOrderNumber setBackgroundColor:[UIColor clearColor]];
    [lbOrderNumber setTextAlignment:NSTextAlignmentCenter];
    [headerView addSubview:lbOrderNumber];
    
    StatisticView* statisticView = [[StatisticView alloc] initWithFrame:CGRectMake(10, 30, [UIScreen mainScreen].bounds.size.width - 20 , viewHeight)];
    
    [statisticView lbHeader1:@"Grand Total" lbContent1:[NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, self.saleOrderEntity.GrandTotal] lbHeader2:@"Tips" lbContent2:[NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, self.saleOrderEntity.Tips] lbHeader3:@"Balance Due" lbContent3:[NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, self.saleOrderEntity.BalanceDue]];
    lbOrderNumber.text = [NSString stringWithFormat:@"#%@", self.saleOrderEntity.OrderNumber];
    
    [statisticView layoutIfNeeded];
    
    [headerView addSubview:statisticView];
    //}
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.reportList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 150;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderDetailTableViewCell *cell = (OrderDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"OrderDetailTableViewCell"];
    
    if(!cell){
        cell = [[OrderDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReportProviderTableViewCell"];
    }
    
    SaleOrderItemEntity* entity = [self.reportList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*
    ReportProviderDetailViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"ReportProviderDetailViewController"];
    //controller.entity = [self.deviceUsersList objectAtIndex:indexPath.row];
    ProviderReportEntity* entity = [self.reportList objectAtIndex:indexPath.row];
    controller.reportList = entity.OrderItems;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
     */
    
}

@end

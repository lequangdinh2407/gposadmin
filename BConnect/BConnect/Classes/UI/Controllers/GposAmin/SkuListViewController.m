//
//  SkuListViewController.m
//  BConnect
//
//  Created by Inglab on 30/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "SkuListViewController.h"
#import "ProductTableViewCell.h"
#import "SkuDetailViewController.h"

@interface SkuListViewController ()

@end

@implementation SkuListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor clearColor];
    [self.tableView registerNib:[UINib nibWithNibName:@"ProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ProductTableViewCell"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
   
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isInit) {
        //[self.tableView reloadData];
        [self getListProduct];
        isInit = true;
    }
}

//data

- (void)getListProduct {
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetListProduct:self.listProductId success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetListProduct success" message:results];
        
        [self dismissProgressHub];
        ProductEntity* entity = [results objectForKey:RESPONSE_DATA];
        self.productsList = entity.Products;
        
        if (self.productsList.count == 0) {
            [self showNoItemView:[CoreStringUtils getStringRes:@"str_no_data"] btnTitle:nil];
        } else {
            [self hideNoItemView];
        }
        [self.tableView reloadData];
       
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetListProduct failure" message:results];
        [self dismissProgressHub];
        [self showNetworkErrorView];
    }];
}

#pragma mark - NoItemViewDelegate methods
- (void)btnClickedNoItemView {
    [self hideNoItemView];
    [self getListProduct];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"SKU"];
    [self setRightItemWithImage:[UIImage imageNamed:@"btn_add"] target:self action:@selector(addNewSKU)];
}

-(void)addNewSKU {
    SkuDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"SkuDetailViewController"];
    controller.isAddNew = true;
    controller.listProductId = self.listProductId;
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}


#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.productsList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ProductTableViewCell *cell = (ProductTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ProductTableViewCell"];

    
    cell.index = indexPath;
    ListProductEntity* entity = [self.productsList objectAtIndex:indexPath.row];
    [cell loadSkuCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    SkuDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"SkuDetailViewController"];
    //controller.lis = [self.deviceUsersList objectAtIndex:indexPath.row];
    
    ListProductEntity* entity = [self.productsList objectAtIndex:indexPath.row];
    controller.skuId = entity.Id;
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    
}

//
- (void)refreshSKUList {
    [self getListProduct];
}

- (void)editSKUDone:(ListProductEntity*)SKU {
    for (int i = 0; i < self.productsList.count; i++) {
        ListProductEntity*entity = [self.productsList objectAtIndex:i];
        if (entity.Id == SKU.Id) {
            [self.productsList replaceObjectAtIndex:i withObject:SKU];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

@end

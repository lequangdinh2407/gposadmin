//
//  ChangePasswordViewController.h
//  BConnect
//
//  Created by Inglab on 16/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "GposTextFieldView.h"
#import "StoreOwnerEntity.h"

@protocol  ChangePasswordViewControllerDelegate

-(void)updatePassword:(NSString*)password;

@end

@interface ChangePasswordViewController : BaseViewViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtOldPassword;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtNewPassword;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtConfirmNewPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (strong, nonatomic) StoreOwnerEntity* storeOwnerEntity;

@property (weak, nonatomic) id delegate;

@end



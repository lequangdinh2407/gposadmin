//
//  NewStoreOwnerViewController.h
//  BConnect
//
//  Created by Inglab on 09/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "NewStoreOwnerView.h"
#import "StoreOwnerEntity.h"
#import "TPKeyboardAvoidingScrollView.h"


@interface NewStoreOwnerViewController : BaseViewViewController

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollWidthConstraint;
@property (strong, nonatomic) NewStoreOwnerView* storeOwnerView;
@property (strong, nonatomic) NSMutableArray* storeIdsList; 
@property (nonatomic) id delegate;

@end



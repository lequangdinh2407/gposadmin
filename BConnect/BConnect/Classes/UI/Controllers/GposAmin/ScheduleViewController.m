//
//  ScheduleViewController.m
//  BConnect
//
//  Created by Inglab on 28/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ScheduleViewController.h"
#import "WorkingTimeTableViewCell.h"
#import "EntityManager.h"

@implementation ScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.currentFromTime = self.currentToTime = [NSDate date];
    
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
    [self initData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    self.lbProviderName.text = self.providerName;
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Schedule"];
    
}

-(void)initData {
    self.schedulesList = [[NSMutableArray alloc] init];
    NSString *strJson= self.workingDays;
    NSData *data = [strJson dataUsingEncoding:NSUTF8StringEncoding];
    NSArray* jsonOutput = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    for (int i = 0 ; i < jsonOutput.count; i++) {
        WorkingDayEntity* entity = (WorkingDayEntity*)[[EntityManager sharedClient] createItem:[jsonOutput objectAtIndex:i] type:WORKING_DAY_ENTITY];
        [self.schedulesList addObject:entity];
    }
    
    [self.tableView reloadData];
}


#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.schedulesList.count;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WorkingTimeTableViewCell *cell = (WorkingTimeTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"WorkingTimeTableViewCell"];
    
    if(!cell){
        cell = [[WorkingTimeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"WorkingTimeTableViewCell"];
    }
    
    WorkingDayEntity* entity = [self.schedulesList objectAtIndex:indexPath.row];
    [cell loadWorkingDayCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:false];
    self.currentSelectedIndex = indexPath;
    WorkingDayEntity* entity = [self.schedulesList objectAtIndex:indexPath.row];
    [self showSchedulePopup:entity.Start endTime:entity.End];
}


- (void)showTimePicker:(int)pickerId start:(NSString *)start end:(NSString *)end{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
   
    if (pickerId == 1) {
        [self showDateTimePickerView:1 pickerMode:UIDatePickerModeTime value:[dateFormatter dateFromString:start] target:nil];
    } else {
        [self showDateTimePickerView:1 pickerMode:UIDatePickerModeTime value:[dateFormatter dateFromString:end] target:nil];
    }
    
    ((BaseViewViewController*)rootView).pickerDelegate = self;
    self.currentPicker = pickerId;

}

- (void)showSchedulePopup:(NSString*)startTime endTime:(NSString*)endTime {
    @synchronized(self) {
        if (self.schedulePopUpView) {
            [self.schedulePopUpView removeFromSuperview];
            self.schedulePopUpView = nil;
        }
        
        self.schedulePopUpView = [[SchedulePopupView alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 10 , (self.view.frame.size.height - 250)/2, self.view.frame.size.width * 4/5 + 20, 250)];
        [self.schedulePopUpView setFromTitle:startTime];
        [self.schedulePopUpView setToTitle:endTime];
        
        [Helpers giveBorderWithCornerRadious:self.schedulePopUpView radius:8 borderColor:[UIColor whiteColor] andBorderWidth:2.0];
       
        self.schedulePopUpView.delegate = self;
        [self.view addSubview:self.schedulePopUpView];
        [self.btnCover setHidden:false];
        //self.popper = [self setupPopperWithSubView:self.schedulePopUpView];
    }
}

- (IBAction)btnCoverPressed:(id)sender {
    if (self.schedulePopUpView) {
        [self.schedulePopUpView removeFromSuperview];
        self.schedulePopUpView = nil;
        [self.btnCover setHidden:true];
    }
}

- (IBAction)btnSavePressed:(id)sender {
    
    /*
    NSMutableArray* dictionarySettingList = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.schedulesList.count; i++) {
        WorkingDayEntity* entity = [self.schedulesList objectAtIndex:i];
        NSDictionary* dictionary = [SettingEntity dictionaryWithPropertiesOfObject:entity];
        
        [dictionarySettingList addObject:dictionary];
    }
    */
    
    NSString* workingDays = @"[";
    for (int i = 0; i < self.schedulesList.count; i++) {
        WorkingDayEntity* entity = [self.schedulesList objectAtIndex:i];
        
        workingDays = [NSString stringWithFormat:@"%@\n{\n ", workingDays];
        workingDays = [NSString stringWithFormat:@"%@ \"Day\":\"%@\",\n", workingDays, entity.Day];
        workingDays = [NSString stringWithFormat:@"%@ \"Start\":\"%@\",\n", workingDays, entity.Start];
        workingDays = [NSString stringWithFormat:@"%@ \"End\":\"%@\"", workingDays, entity.End];
        
        if (i != self.schedulesList.count - 1) {
            workingDays = [NSString stringWithFormat:@"%@\n},", workingDays];
        } else {
            workingDays = [NSString stringWithFormat:@"%@\n}", workingDays];
        }
        
        
    }
    
    workingDays = [NSString stringWithFormat:@"%@\n]", workingDays];
    
    if ([self.delegate respondsToSelector:@selector(saveWorkingDays:)]) {
        [self.delegate saveWorkingDays:workingDays];
    }
    
    [self btnBackPressed:nil];
    
}


- (void)pickerDone{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *newDate = [dateFormatter stringFromDate:((BaseViewViewController*)rootView).dateTimePicker.date];
    
    if (self.currentPicker == 1) {
        [self.schedulePopUpView setFromTitle:newDate];
        self.schedulePopUpView.start = newDate;
        self.currentFromTime = ((BaseViewViewController*)rootView).dateTimePicker.date;
    } else {
        [self.schedulePopUpView setToTitle:newDate];
        self.schedulePopUpView.end = newDate;
        self.currentToTime = ((BaseViewViewController*)rootView).dateTimePicker.date;
    }
}

- (void)saveTime:(NSString*)start end:(NSString*)end {
    WorkingDayEntity* entity = [self.schedulesList objectAtIndex:self.currentSelectedIndex.row];
    entity.Start = start;
    entity.End = end;
    [self.tableView reloadRowsAtIndexPaths:@[self.currentSelectedIndex] withRowAnimation:UITableViewRowAnimationNone];
    
    [self btnCoverPressed:nil];
 
}

@end

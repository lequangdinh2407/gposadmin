//
//  ProviderDetailViewController.h
//  BConnect
//
//  Created by Inglab on 24/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "ProviderDetailView.h"
#import "ProviderEntity.h"
#import "TPKeyboardAvoidingScrollView.h"

@protocol ProviderDetailDelegate

- (void)refreshData;
- (void)updateProvider:(ProviderEntity*)entity;

@end


@interface ProviderDetailViewController : BaseViewViewController

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollWidthConstraint;
@property (strong, nonatomic) ProviderDetailView* providerDetailView;

@property (strong, nonatomic) ProviderEntity* entity;
@property (strong, nonatomic) NSString* workingDays;
@property (nonatomic, assign) BOOL isAddNewProvider;


@property (nonatomic) id delegate;

@end

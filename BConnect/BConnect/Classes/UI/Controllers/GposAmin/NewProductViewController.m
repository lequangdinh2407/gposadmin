//
//  NewProductViewController.m
//  BConnect
//
//  Created by Inglab on 13/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "NewProductViewController.h"

@interface NewProductViewController ()<UITextFieldDelegate>

@end

@implementation NewProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (!isInit) {
        [self initData];
        isInit = true;
    }
    
}

-(void)initData {
    
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
    
    [self.txtName setName:@"Name"];
    [self.txtPrice setName:@"Price"];
    self.txtName.textField.delegate = self;
    self.txtPrice.textField.delegate = self;
    
}

#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.txtName.textField]) {
        [self.txtName.textField resignFirstResponder];
        [self.txtPrice.textField becomeFirstResponder];
    }else {
        [self.view endEditing:YES];
    }
    
    return YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    
    [self SetHeaderTitle:@"New Product"];
    
}


- (IBAction)btnSavePressed:(id)sender {
    [self showProgressHub];
    [[RequestManager sharedClient] requestAddNewProduct:self.txtName.textField.text isActive:self.switchActive.isOn image:self.uploadedImageUrl categoryId:self.categoryId success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestAddNewProduct success" message:results];
        
        NSString* listProductId = [results objectForKey:RESPONSE_DATA];
        [[RequestManager sharedClient] requestAddNewSKU:self.txtName.textField.text DislayOrder:0 Price:[self.txtPrice.textField.text doubleValue] SupplyCost:0 Duration:@"" MinQuantity:0 MaxQuantity:0 SaleTaxRate:0 listProductId:[listProductId intValue] image:@"" success:^(NSMutableDictionary *results, int errorCode) {
           
            [Log d:0 tag:@"requestAddNewSKU success" message:results];
            
            if ([self.delegate respondsToSelector:@selector(refreshProductList)]) {
                [self.delegate refreshProductList];
            }
            
            [self dismissProgressHub];
            [self btnBackPressed:nil];
            
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestAddNewSKU failure" message:results];
            [self dismissProgressHub];
        }];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestAddNewProduct failure" message:results];
    }];
    
}

- (IBAction)btnUploadImagePressed:(id)sender {
    ((BaseViewViewController*)rootView).pickerDelegate = self;
    [self btnGalleryPressed:nil];
}

#pragma mark - UIPickerView methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //NSURL *imageURL = [info valueForKey: UIImagePickerControllerReferenceURL];
    [self removePickerView];
    
    UIImage* image = [info valueForKey: UIImagePickerControllerOriginalImage];
    
    //UIImage *image = [UIImage imageWithContentsOfFile:imageURL.absoluteString];
    NSData *data = UIImageJPEGRepresentation(image, 1.0);
    self.profileImageView.image = image;
    
    [[RequestManager sharedClient] requestUploadImage:data success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestUploadUserAvatar success" message:results];
        
        self.uploadedImageUrl = [results objectForKey:RESPONSE_DATA];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestUploadUserAvatar failure" message:results];
        
    } progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        float percent = ((double)totalBytesWritten) / totalBytesExpectedToWrite;
        if (percent > 1.0f) {
            percent = 1.0f;
        }
        
    }];
    
}


@end

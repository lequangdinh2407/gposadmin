//
//  HomeViewController.h
//  RecordLife
//
//  Created by 谢俊逸 on 17/01/2017.
//  Copyright © 2017 谢俊逸. All rights reserved.
//

#import "BaseViewViewController.h"
#import <UIKit/UIKit.h>

@interface HomeViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView* tableView;
@property (strong, nonatomic) UIButton* coverBtn;
@property (strong, nonatomic) UITableView* dropdownTableView;

@end

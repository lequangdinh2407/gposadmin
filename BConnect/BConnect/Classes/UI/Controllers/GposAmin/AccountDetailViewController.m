//
//  AccountDetailViewController.m
//  BConnect
//
//  Created by Inglab on 11/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "AccountDetailViewController.h"
#import "ChangePasswordViewController.h"

@interface AccountDetailViewController ()

@end

@implementation AccountDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!isInit) {
        [self loadData];
        isInit = true;
    }
}

-(void)loadData {
    [self.accountDetailView loadData:self.entity];
}

- (void)prepareLayout{
    self.scrollWidthConstraint.constant = CGRectGetWidth([UIScreen mainScreen].bounds);
    [self.scrollView layoutIfNeeded];
    if (!self.accountDetailView) {
        CGFloat viewHeight = 700;
        self.accountDetailView = [[AccountDetailView alloc] initWithFrame:CGRectMake(0, 0, self.scrollWidthConstraint.constant, viewHeight)];
        [self.accountDetailView layoutIfNeeded];
        [self.accountDetailView prepareLayout];
        self.accountDetailView.delegate = self;
        
        [self.scrollView addSubview:self.accountDetailView];
        [self.scrollView setContentSize:self.accountDetailView.frame.size];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    [self prepareLayout];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Account"];
}

//delegate

- (void)handleButtonSavePressed {
    [[RequestManager sharedClient] requestEditStoreOwner:self.entity.Id firstName:self.accountDetailView.firstName.textField.text lastName:self.accountDetailView.lastName.textField.text email:self.accountDetailView.txtEmail.textField.text phone:self.accountDetailView.txtPhone.textField.text password:nil active:self.entity.active storeIds:self.entity.storeIdsList success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestEditStoreOwner success" message:results];
        
        self.entity.firstName = self.accountDetailView.firstName.textField.text;
        self.entity.lastName = self.accountDetailView.lastName.textField.text;
        self.entity.email = self.accountDetailView.txtEmail.textField.text;
        self.entity.phone = self.accountDetailView.txtPhone.textField.text;
        
        if ([self.delegate respondsToSelector:@selector(updateStoreOwner:)]) {
            [self.delegate updateStoreOwner:self.entity];
        }
        
        [self dismissProgressHub];
        [self btnBackPressed:nil];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestEditStoreOwner failure" message:results];
        
        [self dismissProgressHub];
    }];
}

-(void)updatePassword:(NSString*)password {
    self.entity.password = password;
}

- (void)handleButtonChangePasswordPressed {
    ChangePasswordViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
    controller.storeOwnerEntity = self.entity;
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

@end

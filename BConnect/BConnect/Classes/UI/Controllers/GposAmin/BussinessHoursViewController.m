//
//  BussinessHoursViewController.m
//  BConnect
//
//  Created by Inglab on 11/11/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BussinessHoursViewController.h"
#import "RequestManager.h"
#import "SettingEntity.h"
#import "EntityManager.h"
#import "WorkingTimeTableViewCell.h"

@interface BussinessHoursViewController () {
    SettingEntity* businessHoursSetting;
}

@end

@implementation BussinessHoursViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.bussinessHoursList = [[NSMutableArray alloc] init];
    self.tableView.separatorColor = [UIColor clearColor];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:20 borderColor:[UIColor clearColor] andBorderWidth:0];
    
    if (!isInit) {
        [self initData];
        isInit = true;
    }
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Bussiness Hours"];
}

-(void)hideUI:(BOOL)isHidden {
    self.btnSave.hidden = isHidden;
    self.lbTime.hidden = isHidden;
    self.lbWork.hidden = isHidden;
    
}

-(void)initData {
    [self showProgressHub];
    [self hideUI:true];
    [[RequestManager sharedClient] requestGetSettings:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSettings succeeded" message:results];
        [self dismissProgressHub];
        [self hideUI:false];
        NSArray* settingList = [results objectForKey:RESPONSE_DATA];
        for (int i = 0 ; i < settingList.count; i++) {
            SettingEntity* entity = [settingList objectAtIndex:i];
            if ([entity.name isEqualToString:@"Business_Hours"]) {
                businessHoursSetting = entity;
            }
        }
        
        NSString *strJson= businessHoursSetting.value;
        NSData *data = [strJson dataUsingEncoding:NSUTF8StringEncoding];
        NSArray* jsonOutput = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        for (int i = 0 ; i < jsonOutput.count; i++) {
            BussinessHourEntity* entity = (BussinessHourEntity*)[[EntityManager sharedClient] createItem:[jsonOutput objectAtIndex:i] type:BUSSINESS_HOUR_ENTITY];
            [self.bussinessHoursList addObject:entity];
            
        }
        
        
        if (self.bussinessHoursList.count == 0) {
            //[self showNoItemView:[CoreStringUtils getStringRes:@"str_no_data"] btnTitle:nil];
        } else {
            [self hideNoItemView];
        }
        [self.tableView reloadData];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSettings failed" message:results];
        [self dismissProgressHub];
        [self showNetworkErrorView];
        [self hideUI:true];
    }];
}

#pragma mark - NoItemViewDelegate methods
- (void)btnClickedNoItemView {
    [self hideNoItemView];
    [self initData];
}

#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.bussinessHoursList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WorkingTimeTableViewCell *cell = (WorkingTimeTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"WorkingTimeTableViewCell"];
    
    if(!cell){
        cell = [[WorkingTimeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"WorkingTimeTableViewCell"];
    }
    
    cell.switchIsOpen.userInteractionEnabled = true;
    BussinessHourEntity* entity = [self.bussinessHoursList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
    
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    self.currentSelectedIndex = indexPath;
    BussinessHourEntity* entity = [self.bussinessHoursList objectAtIndex:indexPath.row];
    [self showSchedulePopup:entity.start endTime:entity.end];

}

- (IBAction)btnSavePressed:(id)sender {
    
    NSMutableArray* dictionarySettingList = [[NSMutableArray alloc] init];
    
    NSString* workingDays = @"[";
    for (int i = 0; i < self.bussinessHoursList.count; i++) {
        BussinessHourEntity* entity = [self.bussinessHoursList objectAtIndex:i];
        
        workingDays = [NSString stringWithFormat:@"%@{", workingDays];
        workingDays = [NSString stringWithFormat:@"%@\"day\":\"%@\",", workingDays, entity.day];
        workingDays = [NSString stringWithFormat:@"%@\"start\":\"%@\",", workingDays, entity.start];
        workingDays = [NSString stringWithFormat:@"%@\"end\":\"%@\",", workingDays, entity.end];
        workingDays = [NSString stringWithFormat:@"%@\"isOpen\":\"%@\"", workingDays, entity.isOpen ? @"true" : @"false"];
        
        if (i != self.bussinessHoursList.count - 1) {
            workingDays = [NSString stringWithFormat:@"%@},", workingDays];
        } else {
            workingDays = [NSString stringWithFormat:@"%@}", workingDays];
        }
    }
    
    workingDays = [NSString stringWithFormat:@"%@]", workingDays];
    
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:businessHoursSetting.name forKey:@"Name"];
    [dictionary setObject:workingDays forKey:@"Value"];
    
    [dictionarySettingList addObject:dictionary];
   
    [self showProgressHub];
    [[RequestManager sharedClient] requestSaveSettings:dictionarySettingList success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestSaveSettings success" message:results];
        [self dismissProgressHub];
        [self btnBackPressed:nil];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestSaveSettings failure" message:results];
        [self dismissProgressHub];
    }];
    
    
}

- (void)showTimePicker:(int)pickerId start:(NSString *)start end:(NSString *)end{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    
    if (pickerId == 1) {
        [self showDateTimePickerView:1 pickerMode:UIDatePickerModeTime value:[dateFormatter dateFromString:start] target:nil];
    } else {
        [self showDateTimePickerView:1 pickerMode:UIDatePickerModeTime value:[dateFormatter dateFromString:end] target:nil];
    }
    
    ((BaseViewViewController*)rootView).pickerDelegate = self;
    self.currentPicker = pickerId;
    
}

- (void)showSchedulePopup:(NSString*)startTime endTime:(NSString*)endTime {
    @synchronized(self) {
        if (self.schedulePopUpView) {
            [self.schedulePopUpView removeFromSuperview];
            self.schedulePopUpView = nil;
        }
        
        self.schedulePopUpView = [[SchedulePopupView alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 10 , (self.view.frame.size.height - 250)/2, self.view.frame.size.width * 4/5 + 20, 250)];
        [self.schedulePopUpView setFromTitle:startTime];
        [self.schedulePopUpView setToTitle:endTime];
        
        [Helpers giveBorderWithCornerRadious:self.schedulePopUpView radius:8 borderColor:[UIColor whiteColor] andBorderWidth:2.0];
        
        self.schedulePopUpView.delegate = self;
        [self.view addSubview:self.schedulePopUpView];
        [self.btnCover setHidden:false];
        //self.popper = [self setupPopperWithSubView:self.schedulePopUpView];
    }
}

- (IBAction)btnCoverPressed:(id)sender {
    if (self.schedulePopUpView) {
        [self.schedulePopUpView removeFromSuperview];
        self.schedulePopUpView = nil;
        [self.btnCover setHidden:true];
    }
}

- (void)pickerDone{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *newDate = [dateFormatter stringFromDate:((BaseViewViewController*)rootView).dateTimePicker.date];
    
    if (self.currentPicker == 1) {
        [self.schedulePopUpView setFromTitle:newDate];
        self.schedulePopUpView.start = newDate;
        self.currentFromTime = ((BaseViewViewController*)rootView).dateTimePicker.date;
    } else {
        [self.schedulePopUpView setToTitle:newDate];
        self.schedulePopUpView.end = newDate;
        self.currentToTime = ((BaseViewViewController*)rootView).dateTimePicker.date;
    }
}

- (void)saveTime:(NSString*)start end:(NSString*)end {
    BussinessHourEntity* entity = [self.bussinessHoursList objectAtIndex:self.currentSelectedIndex.row];
    entity.start = start;
    entity.end = end;
    [self.tableView reloadRowsAtIndexPaths:@[self.currentSelectedIndex] withRowAnimation:UITableViewRowAnimationNone];
    
    [self btnCoverPressed:nil];
    
}


@end

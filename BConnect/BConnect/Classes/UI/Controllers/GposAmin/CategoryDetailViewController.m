//
//  CategoryDetailViewController.m
//  BConnect
//
//  Created by Inglab on 27/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "CategoryDetailViewController.h"
#import "ProductTableViewCell.h"
#import "PosCategoryEntity.h"
#import "ProductDetailViewController.h"
#import "NewProductViewController.h"

@interface CategoryDetailViewController ()

@end

@implementation CategoryDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor clearColor];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ProductTableViewCell"];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isInit) {
        [self getCategoryDetail];
        isInit = true;
    }
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Category Detail"];
    [self setRightItemWithImage:[UIImage imageNamed:@"btn_add"] target:self action:@selector(addNewProduct)];
}

- (void)addNewProduct{
    NewProductViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"NewProductViewController"];
    controller.categoryId = self.categoryId;
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

//data

- (void)getCategoryDetail {
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetCategoryDetail:self.categoryId success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetDeviceUsersList success" message:results];
        [self dismissProgressHub];
        PosCategoryEntity* entity = [results objectForKey:RESPONSE_DATA];
        self.productsList = entity.ListProducts;
        
        if (self.productsList.count == 0) {
            [self showNoItemView:[CoreStringUtils getStringRes:@"str_no_data"] btnTitle:nil];
        } else {
            [self hideNoItemView];
        }
        [self.tableView reloadData];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetDeviceUsersList failure" message:results];
        [self dismissProgressHub];
        [self showNetworkErrorView];
    }];
}

#pragma mark - NoItemViewDelegate methods
- (void)btnClickedNoItemView {
    [self hideNoItemView];
    [self getCategoryDetail];
}


#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.productsList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ProductTableViewCell *cell = (ProductTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ProductTableViewCell"];
    cell.index = indexPath;
    ProductEntity* entity = [self.productsList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    
    ProductDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"ProductDetailViewController"];
    ProductEntity* entity = [self.productsList objectAtIndex:indexPath.row];
    controller.entity = entity;
    controller.categoryNameList = self.categoryNameList;
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    
}

- (void)refreshProductList {
    [self getCategoryDetail];
}

- (void)editProductDone:(ProductEntity*)product {
    for (int i = 0; i < self.productsList.count; i++) {
        ProductEntity*entity = [self.productsList objectAtIndex:i];
        if (entity.Id == product.Id) {
            [self.productsList replaceObjectAtIndex:i withObject:product];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

@end


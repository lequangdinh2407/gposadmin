//
//  SkuListViewController.h
//  BConnect
//
//  Created by Inglab on 30/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"


@interface SkuListViewController :  BaseViewViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>{
    NSArray *searchResults;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* productsList;
@property (assign, nonatomic) int listProductId;
@property (weak, nonatomic) id delegate;

@end


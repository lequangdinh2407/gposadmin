//
//  ReportProviderDetailViewController.h
//  BConnect
//
//  Created by Inglab on 09/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "ProviderReportEntity.h"
NS_ASSUME_NONNULL_BEGIN

@interface ReportProviderDetailViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray* reportList;
@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) ProviderReportEntity* entity;



@end

NS_ASSUME_NONNULL_END

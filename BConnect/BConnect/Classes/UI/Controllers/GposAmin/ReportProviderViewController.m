//
//  ReportProviderViewController.m
//  BConnect
//
//  Created by Inglab on 09/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ReportProviderViewController.h"
#import "ReportProviderTableViewCell.h"
#import "ReportProviderDetailViewController.h"

@interface ReportProviderViewController ()

@end

@implementation ReportProviderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor clearColor];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isInit) {
        [self getReportList];
        isInit = true;
    }
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Provider Report"];
}

//data

- (void)getReportList {
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetProviderReports:self.startDate endDate:self.endDate success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetProviderReports success" message:results];
        [self dismissProgressHub];
        self.reportList = [results objectForKey:RESPONSE_DATA];
        
        if (self.reportList.count == 0) {
            [self showNoItemView:[CoreStringUtils getStringRes:@"str_no_data"] btnTitle:nil];
        } else {
            [self hideNoItemView];
        }
        [self.tableView reloadData];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetProviderReports failure" message:results];
        [self dismissProgressHub];
        [self showNetworkErrorView];
        
    }];
}

#pragma mark - NoItemViewDelegate methods
- (void)btnClickedNoItemView {
    [self hideNoItemView];
    [self getReportList];
}

#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.reportList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 175;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ReportProviderTableViewCell *cell = (ReportProviderTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ReportProviderTableViewCell"];
    
    if(!cell){
        cell = [[ReportProviderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReportProviderTableViewCell"];
    }
    
    ProviderReportEntity* entity = [self.reportList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    ReportProviderDetailViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"ReportProviderDetailViewController"];
    
    ProviderReportEntity* entity = [self.reportList objectAtIndex:indexPath.row];

    controller.reportList = entity.OrderItems;
    controller.entity = entity;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    
}

@end

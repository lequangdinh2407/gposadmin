//
//  CategoryViewController.m
//  BConnect
//
//  Created by Inglab on 24/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "CategoryViewController.h"
#import "CategoryTableViewCell.h"
#import "ModifierViewController.h"
#import "CategoryDetailViewController.h"
#import "NewCategoryViewController.h"
#import "SelectCategoryEntity.h"

@interface CategoryViewController ()

@end

@implementation CategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - ((BaseViewViewController*)rootView).headerHeightConstraint.constant) style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.tableView];
    
    self.tableView.separatorColor = [UIColor clearColor];
    //self.view.clipsToBounds = true;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];

}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.view bringSubviewToFront:self.tableView];
    if (!isInit) {
        [self getCategoryList];
        isInit = true;
    }
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Category"];
    [self setRightItemWithImage:[UIImage imageNamed:@"btn_add"] target:self action:@selector(addNewCategory)];
}

- (void)addNewCategory{
    NewCategoryViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"NewCategoryViewController"];
    controller.isAddNew = true;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

//data

- (void)getCategoryList {
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetCategories:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestgetCategoryList success" message:results];
        [self dismissProgressHub];
        self.categoriesList = [results objectForKey:RESPONSE_DATA];
        self.categoryNameList = [[NSMutableArray alloc] init];
        for (int i = 0; i < self.categoriesList.count; i++) {
            PosCategoryEntity* entity = [self.categoriesList objectAtIndex:i];
            [self.categoryNameList addObject:[[SelectCategoryEntity alloc] initWithName:entity.Name id:entity.Id]];
        }
        
        if (self.categoriesList.count == 0) {
            [self showNoItemView:[CoreStringUtils getStringRes:@"str_no_data"] btnTitle:nil];
        } else {
            [self hideNoItemView];
        }
        
        [self.tableView reloadData];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestgetCategoryList failure" message:results];
        [self dismissProgressHub];
        [self showNetworkErrorView];
    }];
}

#pragma mark - NoItemViewDelegate methods
- (void)btnClickedNoItemView {
    [self hideNoItemView];
    [self getCategoryList];
}

#pragma mark - tableView methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 1.0f;
    
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return nil;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.categoriesList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     //CategoryTableViewCell *cell = (CategoryTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CategoryTableViewCell"];
    
    //if(!cell){
        CategoryTableViewCell *cell = [[CategoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CategoryTableViewCell"];
    //}
    
    PosCategoryEntity* entity = [self.categoriesList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select an action" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Edit Name" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NewCategoryViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"NewCategoryViewController"];
        PosCategoryEntity * entity = [self.categoriesList objectAtIndex:indexPath.row];
        controller.entity = entity;
        controller.delegate = self;
        [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Modifier" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    
        ModifierViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"ModifierViewController"];
        PosCategoryEntity * entity = [self.categoriesList objectAtIndex:indexPath.row];
        controller.selectedModifidersList = entity.Modifiers;
        controller.categoryEntity = entity;
        [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
        
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Category Detail" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        CategoryDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"CategoryDetailViewController"];
        PosCategoryEntity * entity = [self.categoriesList objectAtIndex:indexPath.row];
        controller.categoryId = entity.Id;
        controller.categoryNameList = self.categoryNameList;
        [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    if (actionSheet.popoverPresentationController != nil) {
        actionSheet.popoverPresentationController.sourceView = self.view;
        actionSheet.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    }
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footer=[[UIView alloc]initWithFrame:CGRectZero];
    return footer;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0f;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.nextResponder touchesBegan:touches withEvent:event];
    [super touchesBegan:touches withEvent:event];
}

//NewCategoryViewControllerDelegate

-(void)reloadCategory {
    [self getCategoryList];
}

- (void)updateCategory:(int)category name:(NSString*)name {
    for (int i = 0; i < self.categoryNameList.count; i++) {
        SelectCategoryEntity* entity = [self.categoryNameList objectAtIndex:i];
        if (entity.Id == category) {
            entity.Name = name;
        }
    }
    
    for (int i = 0; i < self.categoriesList.count; i++) {
        PosCategoryEntity* entity = [self.categoriesList objectAtIndex:i];
        if (entity.Id == category) {
            entity.Name = name;
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

- (void)updateCategory:(PosCategoryEntity*)category {
    for (int i = 0; i < self.categoriesList.count; i++) {
        PosCategoryEntity* entity = [self.categoriesList objectAtIndex:i];
        if (entity.Id == category.Id) {
            entity = category;
        }
    }
}

@end

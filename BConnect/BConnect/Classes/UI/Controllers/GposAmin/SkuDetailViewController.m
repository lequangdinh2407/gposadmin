//
//  SkuDetailViewController.m
//  BConnect
//
//  Created by Inglab on 03/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "SkuDetailViewController.h"
#import "SaleTaxGroupEntity.h"
#import "PECropViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "FastttCapturedImage.h"
#import "ResizeImageUtils.h"
#import <Photos/Photos.h>

@interface SkuDetailViewController ()<PECropViewControllerDelegate>

@end

@implementation SkuDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusAuthorized:
                NSLog(@"PHAuthorizationStatusAuthorized");
                break;
            case PHAuthorizationStatusDenied:
                NSLog(@"PHAuthorizationStatusDenied");
                break;
            case PHAuthorizationStatusNotDetermined:
                NSLog(@"PHAuthorizationStatusNotDetermined");
                break;
            case PHAuthorizationStatusRestricted:
                NSLog(@"PHAuthorizationStatusRestricted");
                break;
        }
    }];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    [self prepareLayout];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"SKU"];
    [self setRightItemWithImage:[UIImage imageNamed:@"ic_menu_delete"] target:self action:@selector(deleteSKU)];
}

-(void)deleteSKU {
    [self showProgressHub];
    [[RequestManager sharedClient] requestDeleteSKU:self.entity.Id success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestDeleteSKU succeeded" message:results];
        
        if ([self.delegate respondsToSelector:@selector(refreshSKUList)]) {
            [self.delegate refreshSKUList];
        }
        [self dismissProgressHub];
        [self btnBackPressed:nil];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestDeleteSKU failed" message:results];
        
    
        [Helpers showToast:@"Delete SKU failed"];
        [self dismissProgressHub];
        
        
    }];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isInit) {
        //[self.tableView reloadData];
        if (!self.isAddNew) {
            [self getSkuDetail];
        }
        [self getSaleTaxGroupList];
        isInit = true;
    }
}

//data

- (void)getSkuDetail {
    [[RequestManager sharedClient] requestGetSkuDetail:self.skuId success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSkuDetail success" message:results];
        
        self.entity = [results objectForKey:RESPONSE_DATA];
        [self loadData];
        self.uploadedImageUrl = self.entity.ImageUri;
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSkuDetail failure" message:results];
        //[Helpers showToast:@"Get salon info failed"];
    }];
}

-(void)getSaleTaxGroupList {
    [[RequestManager sharedClient] requestGetSaleTaxGroup:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSaleTaxGroup succeeded" message:results];
        self.saleTaxGroupList = [results objectForKey:RESPONSE_DATA];
        [self.selectionTableView reloadData];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSaleTaxGroup failed" message:results];
        
    }];
}

- (void)handleButtonSavePressed {
    if (!self.isAddNew) {
        [self showProgressHub];
        [[RequestManager sharedClient] requestEditSKU:self.skuId listProductId:self.entity.ListProductId firstCategoryId:0 Name:self.skuDetailView.txtName.textField.text DislayOrder:0 Price:[self.skuDetailView.txtPrice.textField.text doubleValue] SupplyCost:[self.skuDetailView.txtSupplyCost.textField.text doubleValue] Duration:self.skuDetailView.txtDuration.textField.text MinQuantity:[self.skuDetailView.txtMinQuantity.textField.text intValue] MaxQuantity:[self.skuDetailView.txtMaxQuantity.textField.text intValue] SaleTaxRate:[self.skuDetailView.txtSaleTaxRate.textField.text doubleValue] image:self.uploadedImageUrl success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestEditSKU success" message:results];
            self.entity.Name = self.skuDetailView.txtName.textField.text;
            self.entity.Price = [self.skuDetailView.txtPrice.textField.text doubleValue];
            self.entity.StandardCost = [self.skuDetailView.txtSupplyCost.textField.text doubleValue];
            self.entity.Duration = self.skuDetailView.txtDuration.textField.text;
            self.entity.MinQuantity = [self.skuDetailView.txtMinQuantity.textField.text intValue];
            self.entity.MaxQuantity = [self.skuDetailView.txtMaxQuantity.textField.text intValue];
            self.entity.SaleTaxRate = [self.skuDetailView.txtSaleTaxRate.textField.text doubleValue];
            self.entity.ImageUri = self.uploadedImageUrl;

            if ([self.delegate respondsToSelector:@selector(editSKUDone:)]) {
                [self.delegate editSKUDone:self.entity];
            }
            
            [self dismissProgressHub];
            [self btnBackPressed:nil];
            
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestEditSKU failure" message:results];
            [self dismissProgressHub];
        }];
        
    } else {
        [[RequestManager sharedClient] requestAddNewSKU:self.skuDetailView.txtName.textField.text DislayOrder:0 Price:[self.skuDetailView.txtPrice.textField.text doubleValue] SupplyCost:[self.skuDetailView.txtSupplyCost.textField.text doubleValue] Duration:self.skuDetailView.txtDuration.textField.text MinQuantity:[self.skuDetailView.txtMinQuantity.textField.text intValue] MaxQuantity:[self.skuDetailView.txtMaxQuantity.textField.text intValue] SaleTaxRate:[self.skuDetailView.txtSaleTaxRate.textField.text doubleValue] listProductId:self.listProductId image:self.uploadedImageUrl success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestAddNewSKU succeeded" message:results];
            
            if ([self.delegate respondsToSelector:@selector(refreshSKUList)]) {
                [self.delegate refreshSKUList];
            }
            
            [self dismissProgressHub];
            [self btnBackPressed:nil];
            
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestAddNewSKU failed" message:results];
            [self dismissProgressHub];
        }];
    }
    
}

-(void)loadData {
    [self.skuDetailView loadData:self.entity];
}

- (void)prepareLayout{
    
    self.scrollWidthConstraint.constant = CGRectGetWidth([UIScreen mainScreen].bounds);
    [self.scrollView layoutIfNeeded];
    if (!self.skuDetailView) {
        CGFloat viewHeight = 584;
        self.skuDetailView = [[SKUDetailView alloc] initWithFrame:CGRectMake(0, 0, self.scrollWidthConstraint.constant, viewHeight)];
        [self.skuDetailView layoutIfNeeded];
        [self.skuDetailView prepareLayout];
        self.skuDetailView.delegate = self;
        
        [self.scrollView addSubview:self.skuDetailView];
        [self.scrollView setContentSize:self.skuDetailView.frame.size];
    }
     
}

- (IBAction)btnCoverPressed:(id)sender {
    [self.selectionTableView setHidden:true];
    [self.coverBtn setHidden:true];
}

- (void)handleButtonSaleTaxRatePressed {
    [self.selectionTableView setHidden:false];
    [self.coverBtn setHidden:false];
}

#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.saleTaxGroupList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"singleTextCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"singleTextCell"];
        cell.backgroundColor = [UIColor clearColor];
        UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200 - 15, 40)];
        textLabel.font = [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:16.0];
        textLabel.textColor = [UIColor whiteColor];
        textLabel.tag = 1;
        
        [cell.contentView addSubview:textLabel];
    }
    
    //Endline
    //UIView * line = [[UIView alloc] initWithFrame:CGRectMake(15, 54, CGRectGetWidth([UIScreen mainScreen].bounds) - 30, 1)];
    //line.backgroundColor = [APP_SUB_TEXT_COLOR colorWithAlphaComponent:0.1];
    //[cell.contentView addSubview:line];
    
    SaleTaxGroupEntity* entity = [self.saleTaxGroupList objectAtIndex:indexPath.row];
    UILabel* label = [cell viewWithTag:1];
    label.text = entity.GroupName;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SaleTaxGroupEntity* entity = [self.saleTaxGroupList objectAtIndex:indexPath.row];
    [self.skuDetailView setSaleTaxRate:entity.SalesTaxRate];
    [self.selectionTableView setHidden:true];
    [self.coverBtn setHidden:true];
}

#pragma mark - UIPickerView methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //NSURL *imageURL = [info valueForKey: UIImagePickerControllerReferenceURL];
    [self removePickerView];
    
    UIImage* image = [info valueForKey: UIImagePickerControllerOriginalImage];
    
    //UIImage *image = [UIImage imageWithContentsOfFile:imageURL.absoluteString];
    NSData *data = UIImageJPEGRepresentation(image, 1.0);
    
    [self showProgressHub];
    [[RequestManager sharedClient] requestUploadImage:data success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestUploadUserAvatar success" message:results];
        
        self.uploadedImageUrl = [results objectForKey:RESPONSE_DATA];
        
        self.skuDetailView.profileImageView.image = image;
        [self dismissProgressHub];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestUploadUserAvatar failure" message:results];
        [self dismissProgressHub];
        [Helpers showToast:@"Upload image failed"];
    } progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        float percent = ((double)totalBytesWritten) / totalBytesExpectedToWrite;
        if (percent >= 1.0f) {
            percent = 1.0f;

        }
    }];

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    //    [self removePickerView];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)handleButtonUploadImagePressed {
    ((BaseViewViewController*)rootView).pickerDelegate = self;
    [self btnGalleryPressed:nil];
    
}

#pragma mark - <RMPZoomTransitionAnimating>



- (UIColor *)transitionSourceBackgroundColor
{
    return [UIColor clearColor];;
}



#pragma mark - <RMPZoomTransitionDelegate>

- (void)zoomTransitionAnimator:(RMPZoomTransitionAnimator *)animator
         didCompleteTransition:(BOOL)didComplete
      animatingSourceImageView:(UIImageView *)imageView
{
   
}


@end

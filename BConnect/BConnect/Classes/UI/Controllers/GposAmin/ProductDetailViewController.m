//
//  ProductDetailViewController.m
//  BConnect
//
//  Created by Inglab on 30/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "SkuListViewController.h"
#import "SelectCategoryViewController.h"
#import "SelectCategoryEntity.h"
#import "ShortFormPosCategoryEntity.h"


@interface ProductDetailViewController ()
@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    [self prepareLayout];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Product Detail"];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isInit) {
        [self getProductDetail];
        isInit = true;
    }
}

//data

- (void)getProductDetail {
    [[RequestManager sharedClient] requestGetListProduct:self.entity.Id success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetListProduct success" message:results];
        
        self.entity = [results objectForKey:RESPONSE_DATA];
        
        [self.productDetailView loadData:self.entity];
        self.uploadedImageUrl = self.entity.ImageUri;
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetListProduct failure" message:results];
        //[Helpers showToast:@"Get salon info failed"];
    }];
}

- (void)prepareLayout{
    
    self.scrollWidthConstraint.constant = CGRectGetWidth([UIScreen mainScreen].bounds);
    [self.scrollView layoutIfNeeded];
    if (!self.productDetailView) {
        CGFloat viewHeight = 600;
        self.productDetailView = [[ProductDetailView alloc] initWithFrame:CGRectMake(0, 0, self.scrollWidthConstraint.constant, viewHeight)];
        [self.productDetailView layoutIfNeeded];
        [self.productDetailView prepareLayout];
        self.productDetailView.delegate = self;
        
        [self.scrollView addSubview:self.productDetailView];
        [self.scrollView setContentSize:self.productDetailView.frame.size];
    }
}

//ProductDetailView delegate
-(void)handleBtnSavePressed {
    NSMutableArray* categoryList = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.categoryNameList.count; i++) {
        SelectCategoryEntity* entity = [self.categoryNameList objectAtIndex:i];
        if (entity.isSelected) {
            ShortFormPosCategoryEntity* category = [[ShortFormPosCategoryEntity alloc] init];
            category.Id = entity.Id;
            category.Name = entity.Name;
            
            NSDictionary* dictionary = [ShortFormPosCategoryEntity dictionaryWithPropertiesOfObject:category];
            
            [categoryList addObject:dictionary];
        }
    }
    
    [self showProgressHub];
    [[RequestManager sharedClient] requestEditProductDetail:self.entity.Id Name:self.productDetailView.txtName.text DislayOrder:1 ImageUri:self.uploadedImageUrl NonInventory:self.productDetailView.switchNonInventory.isOn Customizable:self.productDetailView.switchCustomizable.isOn IsGiftCard:self.productDetailView.switchIsGiftCard.isOn PrinterName:self.productDetailView.txtPrinterName.text Products:nil Categories:categoryList success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestEditProduct success" message:results];
        //self.entity = [results objectForKey:RESPONSE_DATA];
        
        self.entity.Name = self.productDetailView.txtName.text;
        
        self.entity.PrinterName = self.productDetailView.txtPrinterName.text;
        self.entity.Categories = categoryList;
        self.entity.Customizable = self.productDetailView.switchCustomizable.isOn;
        self.entity.NonInventory = self.productDetailView.switchNonInventory.isOn;
        self.entity.IsGiftCard = self.productDetailView.switchIsGiftCard.isOn;
        self.entity.ImageUri = self.uploadedImageUrl;
        
        if ([self.delegate respondsToSelector:@selector(editProductDone:)]) {
            [self.delegate editProductDone:self.entity];
        }
        
        [self dismissProgressHub];
        [self btnBackPressed:nil];
       
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestEditProduct failure" message:results];
        [self dismissProgressHub];
    }];
}

-(void)gotoSKUList {
    
     SkuListViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"SkuListViewController"];
     //controller.productsList = self.entity.Products;
    controller.listProductId = self.entity.Id;
     [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
     
}

-(void)gotoCategory {
   
    SelectCategoryViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"SelectCategoryViewController"];
    controller.categoryNameList = self.categoryNameList;
    controller.selectCategoryList = self.entity.Categories;
    controller.delegate = self;

    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

//delegate
- (void)reassignCategoryNameList:(NSMutableArray*)categoryNameList {
    self.categoryNameList = [[NSMutableArray alloc] initWithArray:categoryNameList];

}


#pragma mark - UIPickerView methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //NSURL *imageURL = [info valueForKey: UIImagePickerControllerReferenceURL];
    [self removePickerView];
    
    UIImage* image = [info valueForKey: UIImagePickerControllerOriginalImage];
    
    //UIImage *image = [UIImage imageWithContentsOfFile:imageURL.absoluteString];
    NSData *data = UIImageJPEGRepresentation(image, 1.0);
    self.productDetailView.profileImageView.image = image;
    
    [[RequestManager sharedClient] requestUploadImage:data success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestUploadUserAvatar success" message:results];
        
        self.uploadedImageUrl = [results objectForKey:RESPONSE_DATA];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestUploadUserAvatar failure" message:results];
        
    } progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        float percent = ((double)totalBytesWritten) / totalBytesExpectedToWrite;
        if (percent > 1.0f) {
            percent = 1.0f;
        }
        
    }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    //    [self removePickerView];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)handleButtonUploadImagePressed {
    ((BaseViewViewController*)rootView).pickerDelegate = self;
    [self btnGalleryPressed:nil];
    
}


@end

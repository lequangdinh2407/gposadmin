//
//  StoreDetailViewController.m
//  BConnect
//
//  Created by Inglab on 03/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "StoreDetailViewController.h"

@interface StoreDetailViewController ()

@end

@implementation StoreDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!isInit) {
        [self getStoreDetail];
        isInit = true;
    }
}

- (void)getStoreDetail {
   
    self.storeDetailView.entity = self.entity;
    [self.storeDetailView loadData];
//    [[RequestManager sharedClient] requestGetProviderDetail:self.entity.Id success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestGetProviderDetail success" message:results];
//
//        self.entity = [results objectForKey:RESPONSE_DATA];
//        self.storeDetailView.entity = self.entity;
//        [self.storeDetailView loadData];
//
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestGetProviderDetail failure" message:results];
//
//    }];
    
}

- (void)prepareLayout{
    
    self.scrollWidthConstraint.constant = CGRectGetWidth([UIScreen mainScreen].bounds);
    [self.scrollView layoutIfNeeded];
    if (!self.storeDetailView) {
        CGFloat viewHeight = 800;
        self.storeDetailView = [[StoreDetailView alloc] initWithFrame:CGRectMake(0, 0, self.scrollWidthConstraint.constant, viewHeight)];
        [self.storeDetailView layoutIfNeeded];
        [self.storeDetailView prepareLayout];
        self.storeDetailView.delegate = self;
        
        [self.scrollView addSubview:self.storeDetailView];
        [self.scrollView setContentSize:self.storeDetailView.frame.size];
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    [self prepareLayout];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    if (self.isAddNew) {
        [self SetHeaderTitle:@"New Store"];
        
    } else {
        [self SetHeaderTitle:@"Store Detail"];
        //[self setRightItemWithImage:[UIImage imageNamed:@"ic-schedule"] target:self action:@selector(showSearchBar)];
    }
    
}

- (void)handleButtonSavePressed {
    if (!self.isAddNew) {
        [self showProgressHub];
        [[RequestManager sharedClient] requestEditGlobalStore:self.entity.Id name:self.storeDetailView.txtName.textField.text shopCode:self.storeDetailView.txtShopCode.textField.text storeId:@"" password:self.storeDetailView.txtPasscode.textField.text active:self.storeDetailView.switchActive.isOn device:self.storeDetailView.txtDevice.textField.text phone:self.storeDetailView.txtPhone.textField.text coordinateLon:[self.storeDetailView.txtCoordinateLong.textField.text doubleValue] coordinateLat:[self.storeDetailView.txtCoordinateLat.textField.text doubleValue] success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestEditSKU success" message:results];
            
            self.entity.name = self.storeDetailView.txtName.textField.text;
            self.entity.shopCode = self.storeDetailView.txtShopCode.textField.text;
            self.entity.password = self.storeDetailView.txtPasscode.textField.text;
            self.entity.storeId = self.storeDetailView.txtStoreId.textField.text;
            self.entity.phone = self.storeDetailView.txtPhone.textField.text;
            self.entity.device = self.storeDetailView.txtDevice.textField.text ;
            self.entity.active = self.storeDetailView.switchActive.isOn;
            self.entity.coordinateLat = [self.storeDetailView.txtCoordinateLat.textField.text doubleValue];
            self.entity.coordinateLon = [self.storeDetailView.txtCoordinateLong.textField.text doubleValue];
      
            if ([self.delegate respondsToSelector:@selector(editStoreDone:)]) {
                [self.delegate editStoreDone:self.entity];
            }
            
            [self dismissProgressHub];
            [self btnBackPressed:nil];
            
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestEditSKU failure" message:results];
            [self dismissProgressHub];
        }];
        
    } else {
        [self showProgressHub];
        [[RequestManager sharedClient] requestAddNewGlobalStore:self.storeDetailView.txtName.textField.text shopCode:self.storeDetailView.txtShopCode.textField.text storeId:@"" password:self.storeDetailView.txtPasscode.textField.text active:self.storeDetailView.switchActive.isOn device:self.storeDetailView.txtDevice.textField.text phone:self.storeDetailView.txtPhone.textField.text coordinateLon:[self.storeDetailView.txtCoordinateLong.textField.text doubleValue] coordinateLat:[self.storeDetailView.txtCoordinateLat.textField.text doubleValue] success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestAddNewSKU succeeded" message:results];
            
//            if ([self.delegate respondsToSelector:@selector(refreshSKUList)]) {
//                [self.delegate refreshSKUList];
//            }
            
            [self dismissProgressHub];
            [self btnBackPressed:nil];
            
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestAddNewSKU failed" message:results];
            [self dismissProgressHub];
        }];
    }
    
}

@end

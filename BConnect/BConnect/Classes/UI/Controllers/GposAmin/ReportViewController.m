//
//  ReportViewController.m
//  BConnect
//
//  Created by Inglab on 05/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ReportViewController.h"
#import "ReportOrderViewController.h"
#import "ReportProviderViewController.h"
#import "ReportPaymentViewController.h"
#import "SaleByDateViewController.h"

@interface ReportViewController ()

@end

@implementation ReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.currentFromDate = self.currentToDate = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    NSString *newDate = [dateFormatter stringFromDate:[NSDate date]];
    [self.txtFrom setValue:newDate];
    [self.txtTo setValue:newDate];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isInit) {
        [self.txtFrom setName:@"From"];
        [self.txtTo setName:@"To"];
        //self.txtFrom setValue:<#(NSString *)#>
        
        [Helpers giveBorderWithCornerRadious:self.saleByDateView radius:8 borderColor:[UIColor whiteColor] andBorderWidth:2.0];
        [Helpers giveBorderWithCornerRadious:self.paymentView radius:8 borderColor:[UIColor whiteColor] andBorderWidth:2.0];
        [Helpers giveBorderWithCornerRadious:self.orderView radius:8 borderColor:[UIColor whiteColor] andBorderWidth:2.0];
        [Helpers giveBorderWithCornerRadious:self.providerView radius:8 borderColor:[UIColor whiteColor] andBorderWidth:2.0];
        isInit = true;
    }
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Reports"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnFromPressed:(id)sender {
    [self showDateTimePickerView:1 pickerMode:UIDatePickerModeDate value:self.currentFromDate target:nil];
    ((BaseViewViewController*)rootView).pickerDelegate = self;
    self.currentPicker = 1;
}

- (IBAction)btnToPressed:(id)sender {
    [self showDateTimePickerView:2 pickerMode:UIDatePickerModeDate value:self.currentToDate target:nil];
    ((BaseViewViewController*)rootView).pickerDelegate = self;
    self.currentPicker = 2;
}

- (IBAction)btnSaleByDatePressed:(id)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSString *startDate = [dateFormatter stringFromDate:self.currentFromDate];
    NSString *endDate = [dateFormatter stringFromDate:self.currentToDate];
        
    SaleByDateViewController* controller = [[SaleByDateViewController alloc] init];
    
    //[moreStoryboard instantiateViewControllerWithIdentifier:@"ReportPaymentViewController"];
    
    controller.startDate = startDate;
    controller.endDate = endDate;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    
}
- (IBAction)btnPaymentPressed:(id)sender {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSString *startDate = [dateFormatter stringFromDate:self.currentFromDate];
    NSString *endDate = [dateFormatter stringFromDate:self.currentToDate];
    
    ReportPaymentViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"ReportPaymentViewController"];
    
    controller.startDate = startDate;
    controller.endDate = endDate;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    
}

- (IBAction)btnProviderPressed:(id)sender {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSString *startDate = [dateFormatter stringFromDate:self.currentFromDate];
    NSString *endDate = [dateFormatter stringFromDate:self.currentToDate];
    
    ReportProviderViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"ReportProviderViewController"];
    controller.startDate = startDate;
    controller.endDate = endDate;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    
}

- (IBAction)btnOrderPressed:(id)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSString *startDate = [dateFormatter stringFromDate:self.currentFromDate];
    NSString *endDate = [dateFormatter stringFromDate:self.currentToDate];
    
    ReportProviderViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"ReportOrderViewController"];
    controller.startDate = startDate;
    controller.endDate = endDate;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

- (void)pickerDone{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    NSString *newDate = [dateFormatter stringFromDate:((BaseViewViewController*)rootView).dateTimePicker.date];
    
    if (self.currentPicker == 1) {
        
        [self.txtFrom setValue:newDate];
        self.currentFromDate = ((BaseViewViewController*)rootView).dateTimePicker.date;
    } else {
        [self.txtTo setValue:newDate];
        self.currentToDate = ((BaseViewViewController*)rootView).dateTimePicker.date;
    }
}


@end

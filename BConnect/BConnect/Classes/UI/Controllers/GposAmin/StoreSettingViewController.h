//
//  StoreSettingViewController.h
//  BConnect
//
//  Created by Inglab on 27/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "GposTextFieldView.h"

NS_ASSUME_NONNULL_BEGIN

@interface StoreSettingViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtFullTurnAccount;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtGratuityAutoChargeBase;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtStandardRewardPointRate;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtStandardRewardRedeemPoint;

@property (weak, nonatomic) IBOutlet UISwitch *switchIsGrauityAutoCharge;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UITableView *selectionTableView;

@property (weak, nonatomic) IBOutlet UIButton *coverBtn;

@end

NS_ASSUME_NONNULL_END

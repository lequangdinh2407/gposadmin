//
//  SelectCategoryViewController.h
//  BConnect
//
//  Created by Inglab on 12/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"

NS_ASSUME_NONNULL_BEGIN



@protocol SelectCategoryViewControllerDelegate

- (void)reassignCategoryNameList:(NSMutableArray*)categoryNameList;

@end

@interface SelectCategoryViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>{
    NSArray *searchResults;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) id delegate;
@property (strong, nonatomic) NSMutableArray* categoryNameList;
@property (strong, nonatomic) NSMutableArray* selectCategoryList;

@end



NS_ASSUME_NONNULL_END

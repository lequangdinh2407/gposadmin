//
//  AdminDetailViewController.h
//  BConnect
//
//  Created by Inglab on 25/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "GposTextFieldView.h"
#import "DeviceUserEntity.h"

@protocol AdminDetailViewControllerDelegate
- (void)deleteDeviceUser:(DeviceUserEntity*)entity;
- (void)addDeviceUser:(DeviceUserEntity*)entity;
- (void)updateDeviceUser:(DeviceUserEntity*)entity;
@end

@interface AdminDetailViewController : BaseViewViewController

@property (weak, nonatomic) IBOutlet GposTextFieldView *txtUsername;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtEmail;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtPasscode;
@property (weak, nonatomic) IBOutlet UISwitch *switchCashier;

@property (weak, nonatomic) IBOutlet UISwitch *switchSupervisor;
@property (weak, nonatomic) IBOutlet UISwitch *switchController;
@property (weak, nonatomic) IBOutlet UISwitch *switchManager;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) DeviceUserEntity* entity;
@property (nonatomic, assign) BOOL isAddNew;

@property (nonatomic, assign) BOOL isCashier;
@property (nonatomic, assign) BOOL isSupervisor;
@property (nonatomic, assign) BOOL isController;
@property (nonatomic, assign) BOOL isManager;

@end


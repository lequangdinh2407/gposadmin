//
//  SelectStoreViewController.h
//  BConnect
//
//  Created by Inglab on 09/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"

#define ACTION_EMPTY 1
#define ACTION_GET_LIST 2

#define LOAD_NEWS_COUNT 10
#define NO_ITEM_HEIGHT _tableView.frame.size.height

@protocol SelectStoreViewControllerDelegate
- (void)reassignStoreList:(NSMutableArray*)storeList;
@end

@interface SelectStoreViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>{
    int oldCurrentPage;
    int currentPage;
    BOOL isShowNoItemView;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* storesList;
@property (strong, nonatomic) NSArray* storeIdsList;
@property (weak, nonatomic) id delegate;
@property (assign, nonatomic) BOOL isHasMore;

@end


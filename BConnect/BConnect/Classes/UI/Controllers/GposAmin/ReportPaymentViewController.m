//
//  ReportPaymentViewController.m
//  BConnect
//
//  Created by Inglab on 03/11/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ReportPaymentViewController.h"
#import "XJYChart.h"
#import "GeneralPaymentReportEntity.h"
#import "PaymentReportEntity.h"
#import "PaymentEntity.h"
#import "PaymentMethodCollectionViewCell.h"
@interface ReportPaymentViewController ()<XJYChartDelegate>

@property(nonatomic, strong) XPieChart* pieChartView;
@property (strong, nonatomic) NSMutableArray* nameList;
@property (strong, nonatomic) NSMutableArray* amountList;
@property (strong, nonatomic) NSMutableArray* percentageList;
@property (strong, nonatomic) NSArray* colorArray;

@end

@implementation ReportPaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.nameList = [[NSMutableArray alloc] init];
    self.amountList = [[NSMutableArray alloc] init];
    self.percentageList = [[NSMutableArray alloc] init];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"PaymentMethodCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"PaymentMethodCollectionViewCell"];
    //[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"EmptyViewCell"];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isInit) {
        [self getReportPayment];
        isInit = true;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    
    //[self getReportList];
}


- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Payment Report"];
}


-(void)drawChart {
    self.pieChartView = [[XPieChart alloc] init];
    NSMutableArray* pieItems = [[NSMutableArray alloc] init];
    
    self.colorArray = @[
                            RGB(145, 235, 253), RGB(198, 255, 150), RGB(255, 0, 0),
                            RGB(253, 210, 147), RGB(145, 235, 253), RGB(198, 255, 150), RGB(254, 248, 150),
                            RGB(253, 210, 147)
                            ];
    
    NSArray* dataArray = self.nameList;
    
    for (int i = 0; i < self.nameList.count; i++) {
        XPieItem* item1 =
        [[XPieItem alloc] initWithDataNumber:[NSNumber numberWithDouble:[[self.percentageList objectAtIndex:i] doubleValue]]
                                       color:self.colorArray[i]
                                dataDescribe:dataArray[i]];
        [pieItems addObject:item1];
    }
    
    
    //设置dataItemArray
    self.pieChartView.dataItemArray = pieItems;
    self.pieChartView.descriptionTextColor = [UIColor black25PercentColor];
    self.pieChartView.delegate = self;
    self.pieChartView.frame = CGRectMake(0, 0, self.chartView.frame.size.width, self.chartView.frame.size.width);
    
    
    [self.chartView addSubview:self.pieChartView];
    
}

//data

- (void)getReportPayment {
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetPaymentReports:self.startDate endDate:self.endDate success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetPaymentReports success" message:results];
        GeneralPaymentReportEntity* generalPaymentReportEntity = [results objectForKey:RESPONSE_DATA];
        for (int i = 0; i < generalPaymentReportEntity.PaymentReports.count; i++) {
            PaymentReportEntity* paymentReportEntity = [generalPaymentReportEntity.PaymentReports objectAtIndex:i];
            [self.nameList addObject:paymentReportEntity.PaymentMethod];
            double total = 0;
            for (int j = 0; j < paymentReportEntity.Payments.count; j++) {
                PaymentEntity* paymentEntity = [paymentReportEntity.Payments objectAtIndex:j];
                total = total + paymentEntity.Amount;
            }
            [self.amountList addObject:@(total)];
            total = total/generalPaymentReportEntity.PaymentAmount * 100;
            [self.percentageList addObject:@(total)];
            
        }
        [self drawChart];
        [self.collectionView reloadData];
        [self.lineView setHidden:false];
        
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Total:  " attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:16], NSForegroundColorAttributeName : [UIColor whiteColor]}];
        [attributeString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"$%0.2f", generalPaymentReportEntity.PaymentAmount] attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:24], NSForegroundColorAttributeName: [UIColor greenColor]}]];
        self.lbTotal.attributedText = attributeString;
    
        [self dismissProgressHub];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetPaymentReports failure" message:results];
        [self dismissProgressHub];
    }];
}

#pragma mark - Collection methods



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.nameList.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
   
    if (self.nameList.count < 4) {
        return CGSizeMake( [UIScreen mainScreen].bounds.size.width / self.nameList.count , 100);
    }
    return CGSizeMake(100, 100);
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
        PaymentMethodCollectionViewCell * cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"PaymentMethodCollectionViewCell" forIndexPath:indexPath];
    
        double amount = [[self.amountList objectAtIndex:indexPath.row] doubleValue];
        NSString* amountString = [NSString stringWithFormat:@"$%0.2f", amount];
        [cell loadCellInfo:[self.nameList objectAtIndex:indexPath.row] amount:amountString color:[self.colorArray objectAtIndex:indexPath.row]];
        return cell;
        
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    
}
@end

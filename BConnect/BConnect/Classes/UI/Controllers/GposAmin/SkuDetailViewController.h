//
//  SkuDetailViewController.h
//  BConnect
//
//  Created by Inglab on 03/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "SKUDetailView.h"
#import "ListProductEntity.h"
#import <RSKImageCropper/RSKImageCropper.h>
#import "RMPZoomTransitionAnimator.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SkuDetailViewControllerDelegate
- (void)refreshSKUList;
- (void)editSKUDone:(ListProductEntity*)SKU;
@end

@interface SkuDetailViewController : BaseViewViewController<UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, RSKImageCropViewControllerDelegate, RMPZoomTransitionAnimating, RMPZoomTransitionDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollWidthConstraint;
@property (strong, nonatomic) SKUDetailView* skuDetailView;
@property (strong, nonatomic) ListProductEntity* entity;
@property (strong, nonatomic) NSMutableArray* saleTaxGroupList;
@property (assign, nonatomic) int skuId;
@property (assign, nonatomic) int listProductId;
@property (assign, nonatomic) BOOL isAddNew;
@property (nonatomic, strong) ALAssetsLibrary *library;
@property (strong, nonatomic) NSString* uploadedImageUrl;


@property (weak, nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UITableView *selectionTableView;

@property (weak, nonatomic) IBOutlet UIButton *coverBtn;

@end

NS_ASSUME_NONNULL_END

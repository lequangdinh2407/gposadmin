//
//  NewStoreOwnerViewController.m
//  BConnect
//
//  Created by Inglab on 09/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "NewStoreOwnerViewController.h"
#import "SelectStoreViewController.h"

@implementation NewStoreOwnerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.storeIdsList = [[NSMutableArray alloc] init];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!isInit) {
        
        isInit = true;
    }
}

- (void)prepareLayout{
    
    self.scrollWidthConstraint.constant = CGRectGetWidth([UIScreen mainScreen].bounds);
    [self.scrollView layoutIfNeeded];
    if (!self.storeOwnerView) {
        CGFloat viewHeight = 700;
        self.storeOwnerView = [[NewStoreOwnerView alloc] initWithFrame:CGRectMake(0, 0, self.scrollWidthConstraint.constant, viewHeight)];
        [self.storeOwnerView layoutIfNeeded];
        [self.storeOwnerView prepareLayout];
        self.storeOwnerView.delegate = self;
        
        [self.scrollView addSubview:self.storeOwnerView];
        [self.scrollView setContentSize:self.storeOwnerView.frame.size];
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    [self prepareLayout];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
   
    [self SetHeaderTitle:@"New Store Owner"];
    
}



- (void)handleButtonSavePressed {
   
        [self showProgressHub];
        [[RequestManager sharedClient] requestAddNewStoreOwner:self.storeOwnerView.txtFirstName.textField.text lastName:self.storeOwnerView.txtLastName.textField.text email:self.storeOwnerView.txtEmail.textField.text phone:self.storeOwnerView.txtPhone.textField.text active:self.storeOwnerView.switchActive.isOn storeIds:self.storeIdsList  success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestAddNewStoreOwner success" message:results];
            
//            if ([self.delegate respondsToSelector:@selector(refreshData)]) {
//                [self.delegate refreshData];
//            }
            
            [self dismissProgressHub];
            [self btnBackPressed:nil];
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestAddNewStoreOwner failure" message:results];
        
            
            [self dismissProgressHub];
        }];
    
}

- (void)handleButtonStorePressed {
        SelectStoreViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"SelectStoreViewController"];
        controller.delegate = self;
        controller.storeIdsList = self.storeIdsList;
        [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    
}

- (void)reassignStoreList:(NSMutableArray*)storeIdsList {
    self.storeIdsList = storeIdsList;
    NSString* storeDisplayText = @"";
    for (int i = 0; i < storeIdsList.count; i++) {
        if (i == 0) {
            storeDisplayText = [NSString stringWithFormat:@"#%@", [storeIdsList objectAtIndex:i]];
        } else {
            storeDisplayText = [NSString stringWithFormat:@"%@,#%@", storeDisplayText, (NSString*)[storeIdsList objectAtIndex:i]];
        }
    }
    self.storeOwnerView.txtStore.textField.text = storeDisplayText;
    
}

@end

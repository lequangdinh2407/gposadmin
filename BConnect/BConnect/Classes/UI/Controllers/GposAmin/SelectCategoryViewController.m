//
//  SelectCategoryViewController.m
//  BConnect
//
//  Created by Inglab on 12/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "SelectCategoryViewController.h"
#import "SelectCategoryTableViewCell.h"
#import "PosCategoryEntity.h"

@interface SelectCategoryViewController ()

@end

@implementation SelectCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.allowsSelection = false;
    
    for (int i = 0; i < self.categoryNameList.count; i++) {
        
        SelectCategoryEntity* entity = [self.categoryNameList objectAtIndex:i];
        for (int j = 0; j < self.selectCategoryList.count; j++) {
            PosCategoryEntity* category = [self.selectCategoryList objectAtIndex:j];
            if (category.Id == entity.Id) {
                entity.isSelected = true;
            }
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
  
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Select Categories"];

}

- (void)btnBackPressed:(id)sender{
    /*
    for (int i = 0; i < self.categoryNameList.count; i++) {
        SelectCategoryEntity* entity = [self.categoryNameList objectAtIndex:i];
        if (entity.isSelected == true) {
            se
        }
    }
     */

    if ([self.delegate respondsToSelector:@selector(reassignCategoryNameList:)]) {
        [self.delegate reassignCategoryNameList:self.categoryNameList];
    }
    
    [super btnBackPressed:sender];
}


#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.categoryNameList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectCategoryTableViewCell *cell = (SelectCategoryTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"SelectCategoryTableViewCell"];
    
    if(!cell){
        cell = [[SelectCategoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectCategoryTableViewCell"];
    }
    
    SelectCategoryEntity* entity = [self.categoryNameList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [self.tableView deselectRowAtIndexPath:indexPath animated:false];
    /*
    ProviderDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"ProviderDetailViewController"];
    controller.entity = [self.deviceUsersList objectAtIndex:indexPath.row];
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
     */
}

@end

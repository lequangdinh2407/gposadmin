//
//  SaleByDateViewController.h
//  BConnect
//
//  Created by Inglab on 05/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SaleByDateViewController : BaseViewViewController

@property (strong, nonatomic) NSString* startDate;
@property (strong, nonatomic) NSString* endDate;
@end

NS_ASSUME_NONNULL_END

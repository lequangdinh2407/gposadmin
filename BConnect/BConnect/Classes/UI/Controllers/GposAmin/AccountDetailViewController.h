//
//  AccountDetailViewController.h
//  BConnect
//
//  Created by Inglab on 11/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "AccountDetailView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "StoreOwnerEntity.h"

@protocol AccountDetailViewControllerDelegate

- (void)updateStoreOwner:(StoreOwnerEntity*)entity;

@end



@interface AccountDetailViewController : BaseViewViewController

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollWidthConstraint;
@property (strong, nonatomic) AccountDetailView* accountDetailView;
@property (nonatomic) id delegate;
@property (strong, nonatomic) StoreOwnerEntity* entity;

@end



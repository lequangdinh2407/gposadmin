//
//  AdminDetailViewController.m
//  BConnect
//
//  Created by Inglab on 25/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "AdminDetailViewController.h"
#import "RequestManager.h"

@interface AdminDetailViewController () <UITextFieldDelegate>

@end

@implementation AdminDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!isInit) {
        [self initData];
        isInit = true;
    }
}

-(void)initData {
    [self.txtUsername setName:@"Username"];
    [self.txtEmail setName:@"Email"];
    [self.txtPasscode setName:@"Passcode"];
    self.txtUsername.textField.delegate = self;
    self.txtEmail.textField.delegate = self;
    self.txtPasscode.textField.delegate = self;
    
    [self.txtEmail setEmailKeyboard];
    
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
    
    if (self.entity) {
        [self.txtUsername setValue:self.entity.UserName];
        [self.txtEmail setValue:self.entity.Email];
        [self.txtPasscode setValue:self.entity.PassCode];
        [self.switchCashier setOn:[self.entity.isCashier isEqualToString:@"True"] animated:false];
        [self.switchSupervisor setOn:[self.entity.isSupervisor isEqualToString:@"True"] animated:false];
        [self.switchManager setOn:[self.entity.isManager isEqualToString:@"True"] animated:false];
        [self.switchController setOn:[self.entity.isController isEqualToString:@"True"] animated:false];
        
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
   
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    if (self.isAddNew) {
        [self SetHeaderTitle:@"New User Device"];
    } else {
        [self SetHeaderTitle:@"User Device Detail"];
        [self setRightItemWithImage:[UIImage imageNamed:@"ic_menu_delete"] target:self action:@selector(deleteUserDevice)];
    }
}

-(void)deleteUserDevice {
    
    [[RequestManager sharedClient] requestDeleteDeviceUser:self.entity.UserName success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestDeleteDeviceUser succeeded" message:results];
        
        if ([self.delegate respondsToSelector:@selector(deleteDeviceUser:)]) {
            [self.delegate deleteDeviceUser:self.entity];
        }
        
        [self btnBackPressed:nil];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestDeleteDeviceUser failed" message:results];
        [self btnBackPressed:nil];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)switchCashierChangedValue:(id)sender {
    self.isCashier = self.switchCashier.isOn;
}
- (IBAction)switchSupervisorChangedValue:(id)sender {
    self.isSupervisor = self.switchSupervisor.isOn;
}

- (IBAction)switchControllerChangedValue:(id)sender {
    self.isController = self.switchController.isOn;
}
- (IBAction)switchManagerChangedValue:(id)sender {
    self.isManager = self.switchManager.isOn;
}


- (IBAction)btnSavePressed:(id)sender {
    
    if (self.isAddNew) {
        [self showProgressHub];
        [[RequestManager sharedClient] requestAddDeviceUser:self.txtUsername.textField.text Email:self.txtEmail.textField.text PassCode:self.txtPasscode.textField.text IsCashier:self.switchCashier.isOn IsSupervisor:self.switchSupervisor.isOn IsController:self.switchController.isOn IsManager:self.switchManager.isOn success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestAddDeviceUser succeeded" message:results];
            [self dismissProgressHub];
            
            DeviceUserEntity* entity = [[DeviceUserEntity alloc] init];
            entity.Id = [[results objectForKey:RESPONSE_DATA] integerValue];
            entity.UserName = self.txtUsername.textField.text;
            entity.Email = self.txtEmail.textField.text;
            entity.PassCode = self.txtPasscode.textField.text;
            entity.isCashier = self.switchCashier.isOn? @"True" : @"False";
            entity.isSupervisor = self.switchSupervisor.isOn ? @"True" : @"False";
            entity.isController = self.switchController.isOn ? @"True" : @"False";
            entity.isManager = self.switchManager.isOn ? @"True" : @"False";
            if ([self.delegate respondsToSelector:@selector(addDeviceUser:)]) {
                [self.delegate addDeviceUser:entity];
            }
            
            [self btnBackPressed:nil];
            
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestFindAddress failed" message:results];
            [self dismissProgressHub];
        }];
    } else {
        [self showProgressHub];
        
        [[RequestManager sharedClient] requestEditDeviceUser:self.entity.Id UserName :self.txtUsername.textField.text Email:self.txtEmail.textField.text PassCode:self.txtPasscode.textField.text IsCashier:self.switchCashier.isOn IsSupervisor:self.switchSupervisor.isOn IsController:self.switchController.isOn IsManager:self.switchManager.isOn success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestEditDeviceUser succeeded" message:results];
            [self dismissProgressHub];
            
            self.entity.UserName = self.txtUsername.textField.text;
            self.entity.Email = self.txtEmail.textField.text;
            self.entity.PassCode = self.txtPasscode.textField.text;
            self.entity.isCashier = self.switchCashier.isOn? @"True" : @"False";
            self.entity.isSupervisor = self.switchSupervisor.isOn ? @"True" : @"False";
            self.entity.isController = self.switchController.isOn ? @"True" : @"False";
            self.entity.isManager = self.switchManager.isOn ? @"True" : @"False";
            if ([self.delegate respondsToSelector:@selector(updateDeviceUser:)]) {
                [self.delegate updateDeviceUser:self.entity];
            }
            
            [self btnBackPressed:nil];
            
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestEditDeviceUser failed" message:results];
            [self dismissProgressHub];
        }];
    }
}

#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.txtUsername.textField]) {
        [self.txtUsername.textField resignFirstResponder];
        [self.txtEmail.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtEmail.textField]) {
        [self.txtEmail.textField resignFirstResponder];
        [self.txtPasscode.textField becomeFirstResponder];
    } else {
        [self.view endEditing:YES];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}


@end

//
//  StoreOwnerListViewController.m
//  BConnect
//
//  Created by Inglab on 06/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "StoreOwnerListViewController.h"
#import "NewStoreOwnerViewController.h"
#import "StoreOwnerDetailViewController.h"

@interface StoreOwnerListViewController ()

@end

@implementation StoreOwnerListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    currentPage = 0;
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        if (!weakSelf.isProcessingRequest) {
            [weakSelf.tableView.pullToRefreshView startAnimating];
            [weakSelf refreshData];
        } else {
            [weakSelf.tableView.pullToRefreshView stopAnimating];
        }
    }];
    
    self.storesList = [[NSMutableArray alloc] init];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!isInit) {
        isInit = YES;
        [self refreshData];
    }
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetHeaderTitle:[SharePrefUtils getStringPreference:PREFKEY_CONFIG_STORENAME]];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self setRightItemWithImage:[UIImage imageNamed:@"btn_add"] target:self action:@selector(addNewStoreOwner)];
}

-(void)addNewStoreOwner {
    NewStoreOwnerViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"NewStoreOwnerViewController"];

    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

//data

- (void)getstoresList {
    
    if (self.isProcessingRequest)
        return;
    
    currentPage++;
    isErrorNetwork = NO;
    isShowNoItemView = NO;
    
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetGlobalStoreOwnerList:currentPage count:10 success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetstoresList success" message:results];
        [self dismissProgressHub];
        
        NSArray* newsArr = [Helpers getArrayDiffNull:[results objectForKey:RESPONSE_DATA]];
        
        self.isHasMore = [Helpers getBoolDiffNull:[results objectForKey:RESPONSE_HAS_MORE]];
        
        if (self.isHasMore && currentPage == 1) {
            [self addLoadMore];
        }
        
        if (newsArr.count > 0) {
            if (currentPage == 1) {
                [self.storesList removeAllObjects];
                [self.storesList addObjectsFromArray:newsArr];
                [self.tableView reloadData];
                
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_GET_NEWS_NEW object:nil];
                
            } else {
                NSUInteger oldCount = self.storesList.count;
                [self.storesList addObjectsFromArray:newsArr];
                NSMutableArray* indexPaths = [[NSMutableArray alloc] init];
                for (NSUInteger i = oldCount; i < self.storesList.count; i++) {
                    [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                }
                [self.tableView beginUpdates];
                [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView endUpdates];
            }
        }
        
        //        if (newsArr.count == 0 && currentPage == 1) {
        //            [self.notificationList removeAllObjects];
        //            [self showNoProduct];
        //        }
        
        [self dismissProgressHub];
        [self.tableView.pullToRefreshView stopAnimating];
        [self.tableView.infiniteScrollingView stopAnimating];
        
        [self.tableView reloadData];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetstoresList failure" message:results];
        //[Helpers showToast:@"Get salon info failed"];
        [self dismissProgressHub];
        [self showNetworkErrorView];
    }];
}

- (void)addLoadMore{
    __weak typeof(self) weakSelf = self;
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        if (weakSelf.isHasMore && !weakSelf.isProcessingRequest) {
            [weakSelf loadMore];
            [weakSelf.tableView.infiniteScrollingView startAnimating];
        } else{
            [weakSelf.tableView.infiniteScrollingView stopAnimating];
        }
    }];
}

- (void)loadMore {
    oldCurrentPage = currentPage;
    [self getstoresList];
}

- (void)refreshData{
    oldCurrentPage = currentPage;
    currentPage = 0;
    [self getstoresList];
}

#pragma mark - NoItemViewDelegate methods

- (void)btnClickedNoItemView {
    switch (actionType) {
        case ACTION_EMPTY: {
            actionType = 0;
            [self hideNoItemView];
            [self refreshData];
        }
            break;
        case ACTION_GET_LIST: {
            actionType = 0;
            [self hideNoItemView];
            oldCurrentPage = currentPage;
            [self getstoresList];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.storesList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 145;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    StoreOwnerTableViewCell *cell = (StoreOwnerTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"StoreOwnerTableViewCell"];
    
    if(!cell){
        cell = [[StoreOwnerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"StoreOwnerTableViewCell"];
    }
    
    StoreOwnerEntity* entity = [self.storesList objectAtIndex:indexPath.row];
    [cell loadCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:false];
    
    StoreOwnerDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"StoreOwnerDetailViewController"];
    controller.entity = [self.storesList objectAtIndex:indexPath.row];
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

- (void)editStoreDone:(StoreOwnerEntity*)store {
    for (int i = 0; i < self.storesList.count; i++) {
        StoreOwnerEntity *entity = [self.storesList objectAtIndex:i];
        if (entity.Id == store.Id) {
            [self.storesList replaceObjectAtIndex:i withObject:store];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

- (void)updateStoreOwner:(StoreOwnerEntity*)store {
    for (int i = 0; i < self.storesList.count; i++) {
        StoreOwnerEntity *entity = [self.storesList objectAtIndex:i];
        if (entity.Id == store.Id) {
            [self.storesList replaceObjectAtIndex:i withObject:store];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

@end

//
//  ReportOrderDetailViewController.h
//  BConnect
//
//  Created by Inglab on 02/11/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "SaleOrderEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportOrderDetailViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray* reportList;
@property (strong, nonatomic) SaleOrderEntity* saleOrderEntity;

@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) NSString* startDate;
@property (strong, nonatomic) NSString* endDate;


@end

NS_ASSUME_NONNULL_END

//
//  StoreSettingViewController.m
//  BConnect
//
//  Created by Inglab on 27/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "StoreSettingViewController.h"
#import "SettingEntity.h"

@interface StoreSettingViewController ()<UITextFieldDelegate> {
    SettingEntity* fullTurmAmountSetting;
    SettingEntity* gratuityAutoChargeBaseSetting;
    SettingEntity* gratuityAutoChargeRateSetting;
    SettingEntity* standardRewardPointRateSetting;
    SettingEntity* standardRewardRedeemPointSetting;
    SettingEntity* isGratuityAutoChargeSetting;
}
@end

@implementation StoreSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self prepareLayout];
    if (!isInit) {
        [self initData];
        isInit = true;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:@"Store Setting"];
}

-(void)prepareLayout {
    [self.txtFullTurnAccount setName:@"Full Turn Account"];
    [self.txtGratuityAutoChargeBase setName:@"Gratuity Auto Charge Base"];
    [self.txtStandardRewardPointRate setName:@"Standard Reward Point Rate"];
    [self.txtStandardRewardRedeemPoint setName:@"Standard Reward Redeem Point"];
    self.txtFullTurnAccount.textField.delegate = self;
    self.txtGratuityAutoChargeBase.textField.delegate = self;
    self.txtStandardRewardPointRate.textField.delegate = self;
    
    //[self.txtFullTurnAccount setNumberKeyboard];
    //[self.txtGratuityAutoChargeBase setNumberKeyboard];
    [self.txtStandardRewardPointRate setNumberAndPunctuationKeyboard];
    [self.txtFullTurnAccount setNumberAndPunctuationKeyboard];
    [self.txtGratuityAutoChargeBase setNumberAndPunctuationKeyboard];
    
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
}

-(void)initData {
    
    [[RequestManager sharedClient] requestGetSettings:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSettings succeeded" message:results];
        NSArray* settingList = [results objectForKey:RESPONSE_DATA];
        for (int i = 0 ; i < settingList.count; i++) {
            SettingEntity* entity = [settingList objectAtIndex:i];
            if ([entity.name isEqualToString:@"Full_Turn_Amount"]) {
                [self.txtFullTurnAccount setValue:entity.value];
                fullTurmAmountSetting = entity;
            }
            
            if ([entity.name isEqualToString:@"Gratuity_Auto_Charge_Base"]) {
                [self.txtGratuityAutoChargeBase setValue:entity.value];
                gratuityAutoChargeBaseSetting = entity;
            }
            
            if ([entity.name isEqualToString:@"Gratuity_Auto_Charge_Rate"]) {
                [self.txtGratuityAutoChargeBase setValue:entity.value];
                gratuityAutoChargeRateSetting = entity;
            }
            
            if ([entity.name isEqualToString:@"Standard_Reward_Point_Rate"]) {
                [self.txtStandardRewardPointRate setValue:entity.value];
                standardRewardPointRateSetting = entity;
            }
            
            if ([entity.name isEqualToString:@"Standard_Reward_Redeem_Point"]) {
                [self.txtStandardRewardRedeemPoint setValue:entity.value];
                standardRewardRedeemPointSetting = entity;
            }
            
            if ([entity.name isEqualToString:@"Is_Gratuity_Auto_Charge"]) {
                
                if ([entity.value isEqualToString:@"false"] || [entity.value isEqualToString:@"NO"]) {
                    [self.switchIsGrauityAutoCharge setOn:false];
                } else {
                    [self.switchIsGrauityAutoCharge setOn:true];
                }
                
                isGratuityAutoChargeSetting = entity;
            }
        }
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSettings failed" message:results];
        
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSavePressed:(id)sender {
    fullTurmAmountSetting.value = self.txtFullTurnAccount.textField.text;
    gratuityAutoChargeBaseSetting.value = self.txtGratuityAutoChargeBase.textField.text;
    gratuityAutoChargeRateSetting.value = @"10.0";
    standardRewardPointRateSetting.value = self.txtStandardRewardPointRate.textField.text;
    standardRewardRedeemPointSetting.value = self.txtStandardRewardRedeemPoint.textField.text;
    isGratuityAutoChargeSetting.value = self.switchIsGrauityAutoCharge.isOn ? @"YES" : @"NO";
    
    NSMutableArray* settingList = [[NSMutableArray alloc] init];
    [settingList addObject:fullTurmAmountSetting];
    [settingList addObject:gratuityAutoChargeBaseSetting];
    //[settingList addObject:gratuityAutoChargeRateSetting];
    [settingList addObject:standardRewardPointRateSetting];
    [settingList addObject:standardRewardRedeemPointSetting];
    [settingList addObject:isGratuityAutoChargeSetting];
    
    
    NSMutableArray* dictionarySettingList = [[NSMutableArray alloc] init];
    
    //NSString* workingDays = @"[";
    for (int i = 0; i < settingList.count; i++) {
        SettingEntity* entity = [settingList objectAtIndex:i];
        
        
//        workingDays = [NSString stringWithFormat:@"%@\n\t{\n", workingDays];
//        workingDays = [NSString stringWithFormat:@"%@\t\t\"name\" : \"%@\",\n", workingDays, entity.Name];
//
//        if ([entity.Name isEqualToString:@"Is_Gratuity_Auto_Charge"]) {
//
//            workingDays = [NSString stringWithFormat:@"%@\t\t\"value\" : \"%@\"", workingDays, entity.Value];
//        } else {
//            workingDays = [NSString stringWithFormat:@"%@\t\t\"value\" : %@", workingDays, entity.Value];
//        }
//
//        if (i != settingList.count - 1) {
//            workingDays = [NSString stringWithFormat:@"%@\n\t},", workingDays];
//        } else {
//            workingDays = [NSString stringWithFormat:@"%@\n\t}", workingDays];
//        }
       
        
        NSString* workingDays = @"";
                //workingDays = [NSString stringWithFormat:@"%@{", workingDays];
                workingDays = [NSString stringWithFormat:@"%@\"Name\":\"%@\",", workingDays, entity.name];
        
                workingDays = [NSString stringWithFormat:@"%@\"Value\":\"%@\"", workingDays, entity.value];
        
        
//                if (i != settingList.count - 1) {
//                    workingDays = [NSString stringWithFormat:@"%@},", workingDays];
//                } else {
//                    workingDays = [NSString stringWithFormat:@"%@}", workingDays];
//                }
        
        
        //NSDictionary* dictionary = [SettingEntity dictionaryWithPropertiesOfObject:entity];
        //[dictionarySettingList addObject:dictionary];
        
        NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
        [dictionary setObject:entity.name forKey:@"Name"];
        [dictionary setObject:entity.value forKey:@"Value"];
        
        [dictionarySettingList addObject:dictionary];
        
    }
    
    //workingDays = [NSString stringWithFormat:@"%@]", workingDays];
    
    [self showProgressHub];
    [[RequestManager sharedClient] requestSaveSettings:dictionarySettingList success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestSaveSettings success" message:results];
        [self dismissProgressHub];
        [self btnBackPressed:nil];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestSaveSettings failure" message:results];
        [self dismissProgressHub];
    }];
}

- (IBAction)btnStandardRewardPointRatePressed:(id)sender {
    [self.selectionTableView setHidden:false];
    [self.coverBtn setHidden:false];
}

- (IBAction)btnCoverPressed:(id)sender {
    [self.selectionTableView setHidden:true];
    [self.coverBtn setHidden:true];
}

#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"singleTextCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"singleTextCell"];
        cell.backgroundColor = [UIColor clearColor];
        UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200 - 15, 40)];
        textLabel.font = [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:16.0];
        textLabel.textColor = [UIColor whiteColor];
        textLabel.tag = 1;
        
        [cell.contentView addSubview:textLabel];
    }
    
    //Endline
    //UIView * line = [[UIView alloc] initWithFrame:CGRectMake(15, 54, CGRectGetWidth([UIScreen mainScreen].bounds) - 30, 1)];
    //line.backgroundColor = [APP_SUB_TEXT_COLOR colorWithAlphaComponent:0.1];
    //[cell.contentView addSubview:line];
    
    
    int value = (indexPath.row + 1) * 500;
    UILabel* label = [cell viewWithTag:1];
    label.text = [NSString stringWithFormat:@"%d", value ];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.txtStandardRewardRedeemPoint setValue:[NSString stringWithFormat:@"%d", (indexPath.row + 1) * 500]];
    [self.selectionTableView setHidden:true];
    [self.coverBtn setHidden:true];
}

#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.txtFullTurnAccount.textField]) {
        [self.txtFullTurnAccount.textField resignFirstResponder];
        [self.txtGratuityAutoChargeBase.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtGratuityAutoChargeBase.textField]) {
        [self.txtGratuityAutoChargeBase.textField resignFirstResponder];
        [self.txtStandardRewardPointRate.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtStandardRewardPointRate.textField]) {
        [self.view endEditing:YES];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}
- (IBAction)btnBackgroundCoverPressed:(id)sender {
    [self.view endEditing:YES];
}

@end

//
//  CategoryDetailViewController.h
//  BConnect
//
//  Created by Inglab on 27/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"


@interface CategoryDetailViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>{
    NSArray *searchResults;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* productsList;
@property (assign, nonatomic) int categoryId;
@property (weak, nonatomic) id delegate;
@property (strong, nonatomic) NSMutableArray* categoryNameList;
@end



//
//  StoreListViewController.h
//  BConnect
//
//  Created by Inglab on 02/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "StoreTableViewCell.h"



#define ACTION_EMPTY 1
#define ACTION_GET_LIST 2

#define LOAD_NEWS_COUNT 10
#define NO_ITEM_HEIGHT _tableView.frame.size.height

@interface StoreListViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>{
    int oldCurrentPage;
    int currentPage;
    BOOL isShowNoItemView;
}


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* storesList;
@property (weak, nonatomic) id delegate;
@property (assign, nonatomic) BOOL isHasMore;

@end


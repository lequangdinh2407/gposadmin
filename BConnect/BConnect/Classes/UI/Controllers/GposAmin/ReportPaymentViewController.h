//
//  ReportPaymentViewController.h
//  BConnect
//
//  Created by Inglab on 03/11/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportPaymentViewController : BaseViewViewController

@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) NSString* startDate;
@property (strong, nonatomic) NSString* endDate;
@property (weak, nonatomic) IBOutlet UILabel *lbTotal;
@property (weak, nonatomic) IBOutlet UIView *chartView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@end

NS_ASSUME_NONNULL_END

//
//  HomeViewController.m
//  RecordLife
//
//  Created by 谢俊逸 on 17/01/2017.
//  Copyright © 2017 谢俊逸. All rights reserved.
//

#import "HomeViewController.h"

#import "BarChartCell.h"
#import "TopSaleTableViewCell.h"
#import <Masonry/Masonry.h>
#import "SaleEntity.h"

@interface HomeViewController () {
    NSMutableArray* monthsList;
    NSString* selectedMonth;
    NSMutableArray* topSalesList;
    NSMutableArray* valuesList;
    NSMutableArray* chartMonthsList;
    double maximumValue;
}

@property(nonatomic, strong) UIBarButtonItem* leftBarItem;
@property(nonatomic, strong) UIBarButtonItem* rightBarItem;

@end

@implementation HomeViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.automaticallyAdjustsScrollViewInsets = NO;
 
  self.navigationItem.leftBarButtonItem = self.leftBarItem;
  self.navigationItem.rightBarButtonItem = self.rightBarItem;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - ((BaseViewViewController*)rootView).headerHeightConstraint.constant) style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.allowsSelection = false;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_tableView registerClass:[BarChartCell class]
       forCellReuseIdentifier:kBarChartCell];
    
   [self.view addSubview:self.tableView];
    
    
    self.coverBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - ((BaseViewViewController*)rootView).headerHeightConstraint.constant)];
    [self.coverBtn addTarget:self action:@selector(btnCoverPressed) forControlEvents:UIControlEventTouchUpInside];
    self.coverBtn.hidden = true;
    [self.view addSubview:self.coverBtn];
    [self.view bringSubviewToFront:self.coverBtn];
    
    double dropdownTableViewWidth = [UIScreen mainScreen].bounds.size.width * 0.8;
    double dropdownTableViewHeight = ([UIScreen mainScreen].bounds.size.height - ((BaseViewViewController*)rootView).headerHeightConstraint.constant) * 0.8;
    
    self.dropdownTableView = [[UITableView alloc] initWithFrame:CGRectMake(dropdownTableViewWidth/8, dropdownTableViewHeight/8, dropdownTableViewWidth, dropdownTableViewHeight) style:UITableViewStylePlain];
    self.dropdownTableView.dataSource = self;
    self.dropdownTableView.delegate = self;
    self.dropdownTableView.hidden = true;
    [self.view addSubview:self.dropdownTableView];
    [self.view bringSubviewToFront:self.dropdownTableView];
    self.tableView.separatorColor = [UIColor clearColor];
    
    monthsList = [[NSMutableArray alloc] init];
    chartMonthsList = [[NSMutableArray alloc] init];
    valuesList = [[NSMutableArray alloc] init];
    
    NSDate* date = [NSDate date];
    NSCalendar *calendar = [[NSLocale currentLocale] objectForKey:NSLocaleCalendar];
    NSDateComponents *components = [calendar components: NSCalendarUnitMonth fromDate:date];
    
    NSInteger month = [components month];
    
    NSDateComponents *componentYear = [calendar components: NSCalendarUnitYear fromDate:date];
    NSInteger year = [componentYear year];
    
    for (int i = 0; i<12; i++) {
        
        if (month < 10) {
            [chartMonthsList addObject:[NSString stringWithFormat:@"0%d/%d", (int)month, (int)year]];
        } else {
            [chartMonthsList addObject:[NSString stringWithFormat:@"%d/%d", (int)month, (int)year]];
        }
        
        [monthsList addObject:[NSString stringWithFormat:@"%d-%d", (int)year, (int)month]];
        
        if (month == 1) {
            month = 12;
            year -= 1;
        } else {
            month -= 1;
        }
        
        [valuesList addObject:@(0)];
    }
    
    month = month + 1;
    
    selectedMonth = [monthsList objectAtIndex:0];
    chartMonthsList = [[[chartMonthsList reverseObjectEnumerator] allObjects] mutableCopy];
}

-(void)hideDropdownTableView:(BOOL)isHidden {
    self.dropdownTableView.hidden = isHidden;
    self.coverBtn.hidden = isHidden;
}

- (void)btnCoverPressed {
    [self hideDropdownTableView:true];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isInit) {
        isInit = true;
        [self getData];
    }
}

-(void)getData {
    [[RequestManager sharedClient] requestGetRecentSaleList:12 storeId:@"0" success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetStoreList succeeded" message:results];
        
        NSMutableArray* saleList = [[results objectForKey:RESPONSE_SALES] mutableCopy];
        
        for (int i = 0; i < saleList.count; i++) {
            SaleEntity* entity = [saleList objectAtIndex:i];
            NSString* startDate = [DateUtils parseTimeStampToDate:entity.startTime];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"hh:mm aa MM/dd/yyyy"];
            NSDate *date = [dateFormatter dateFromString:startDate];
            
            [dateFormatter setDateFormat:@"MM/yyyy"];
            NSString *month = [dateFormatter stringFromDate:date];
            
            for (int j = 0; j < chartMonthsList.count; j++) {
                if ([chartMonthsList[j] isEqualToString:month]) {
                    valuesList[j] = @(entity.sale);
                    if (entity.sale > maximumValue) {
                        maximumValue = entity.sale;
                    }
                }
            }
        }
        
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetStoreList failed" message:results];
        
    }];
    
    [[RequestManager sharedClient] requestGetInMonthSaleList:(int)0 success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetStoreList succeeded" message:results];
        
        topSalesList = [[results objectForKey:RESPONSE_DATA] mutableCopy];
        
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetStoreList failed" message:results];
        
    }];
    
}


- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self SetHeaderTitle:[SharePrefUtils getStringPreference:PREFKEY_CONFIG_STORENAME]];
    
}


- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}



- (UIBarButtonItem*)leftBarItem {
  if (!_leftBarItem) {
    _leftBarItem = [[UIBarButtonItem alloc]
        initWithImage:[UIImage imageNamed:@"settings-Small.png"]
                style:UIBarButtonItemStyleDone
               target:self
               action:nil];
  }
  return _leftBarItem;
}

- (UIBarButtonItem*)rightBarItem {
  if (!_rightBarItem) {
    _rightBarItem = [[UIBarButtonItem alloc]
        initWithImage:[UIImage imageNamed:@"add-Small.png"]
                style:UIBarButtonItemStyleDone
               target:self
               action:nil];
  }
  return _rightBarItem;
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    if (tableView == self.tableView) {
        return 2;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableView) {
        return 1;
    } else {
        return monthsList.count;
    }
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {

  if (tableView == self.tableView) {
      if (indexPath.section == 0) {
          BarChartCell* cell =
          [tableView dequeueReusableCellWithIdentifier:kBarChartCell
                                          forIndexPath:indexPath];
          
          cell.maximumValue = maximumValue;
          cell.chartMonthsList = chartMonthsList;
          cell.valuesList = valuesList;
          [cell loadData];
          return cell;

      } else {
          TopSaleTableViewCell *cell = (TopSaleTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"TopSaleTableViewCell"];
          
          if(!cell){
              cell = [[TopSaleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TopSaleTableViewCell"];
          }
          
          cell.delegate = self;
        
          cell.txtMonth.lbName.text = @"Month";
          cell.topSalesList = topSalesList;
          [cell.tableView reloadData];
          [Helpers giveBorderWithCornerRadious:cell.txtMonth.textField radius:5 borderColor:[UIColor whiteColor] andBorderWidth:2.0 rect:CGRectMake(cell.txtMonth.textField.bounds.origin.x, cell.txtMonth.textField.bounds.origin.y, [UIScreen mainScreen].bounds.size.width - 40, cell.txtMonth.textField.bounds.size.height)];
          
          [cell.txtMonth setValue:selectedMonth];
          return cell;
      }
  } else {
      static NSString *MyIdentifier = @"MyIdentifier";
      
      UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
      
      if (cell == nil) {
          cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
      }
      
      cell.textLabel.text = [monthsList objectAtIndex:indexPath.row];
      return cell;
  }
  
  return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.dropdownTableView == tableView) {
        [tableView deselectRowAtIndexPath:indexPath animated:false];
        
        [self hideDropdownTableView:true];
        selectedMonth = [monthsList objectAtIndex:indexPath.row];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        
        [[RequestManager sharedClient] requestGetInMonthSaleList:(int)indexPath.row success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestGetStoreList succeeded" message:results];
            
            topSalesList = [[results objectForKey:RESPONSE_DATA] mutableCopy];
            
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
            
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestGetStoreList failed" message:results];
            
        }];
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
   
    CGFloat viewHeight = 50;
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, viewHeight)];
    
    UILabel* lbHeaderName = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width, 25)];
    [lbHeaderName setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:25.0]];
    [lbHeaderName setTextColor:[UIColor whiteColor]];
    [lbHeaderName setBackgroundColor:[UIColor clearColor]];
    [lbHeaderName setTextAlignment:NSTextAlignmentCenter];
    [headerView addSubview:lbHeaderName];
    
    
    UILabel* lbDescription = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, [UIScreen mainScreen].bounds.size.width, 18)];
    [lbDescription setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:16.0]];
    [lbDescription setTextColor:[UIColor whiteColor]];
    [lbDescription setBackgroundColor:[UIColor clearColor]];
    [lbDescription setTextAlignment:NSTextAlignmentCenter];
    [headerView addSubview:lbDescription];
    
    if (section == 0)  {
        lbHeaderName.text = @"Global Sales";
        lbDescription.text = @"Last 12 months";
    } else {
        lbHeaderName.text = @"Top Sale";
        lbDescription.text = @"Monthly statistics";
    }

    return headerView;
}

#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView*)tableView
    heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    if (tableView == self.tableView) {
        return 400;
    } else {
        return 40;
    }
}

- (CGFloat)tableView:(UITableView*)tableView
    heightForHeaderInSection:(NSInteger)section {
    if (tableView == self.tableView) {
        return 70;
    } else {
        return 0;
    }
}

#pragma mark TopSaleTableViewCellDelegate
- (void)handleButtonMonthPressed {
    [self hideDropdownTableView:false];
    
    
}

@end

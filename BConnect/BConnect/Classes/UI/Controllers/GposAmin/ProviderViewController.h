//
//  ProviderViewController.h
//  BConnect
//
//  Created by Inglab on 24/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "ProviderEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProviderViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* providerList;
@property (weak, nonatomic) id delegate;
@end

NS_ASSUME_NONNULL_END

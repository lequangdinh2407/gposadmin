//
//  ProviderDetailViewController.m
//  BConnect
//
//  Created by Inglab on 24/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ProviderDetailViewController.h"
#import "ScheduleViewController.h"


@interface ProviderDetailViewController ()

@end

@implementation ProviderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!isInit) {
        if (!self.isAddNewProvider) {
            [self getProviderDetail];
        }
        isInit = true;
    }
}

- (void)prepareLayout{
    
    self.scrollWidthConstraint.constant = CGRectGetWidth([UIScreen mainScreen].bounds);
    [self.scrollView layoutIfNeeded];
    if (!self.providerDetailView) {
        CGFloat viewHeight = 700;
        self.providerDetailView = [[ProviderDetailView alloc] initWithFrame:CGRectMake(0, 0, self.scrollWidthConstraint.constant, viewHeight)];
        [self.providerDetailView layoutIfNeeded];
        [self.providerDetailView prepareLayout];
        self.providerDetailView.delegate = self;
        
        [self.scrollView addSubview:self.providerDetailView];
        [self.scrollView setContentSize:self.providerDetailView.frame.size];
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    [self prepareLayout];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    if (self.isAddNewProvider) {
        [self SetHeaderTitle:@"New Provider"];
        
    } else {
        [self SetHeaderTitle:@"Edit Provider"];
        [self setRightItemWithImage:[UIImage imageNamed:@"ic-schedule"] target:self action:@selector(showSearchBar)];
    }
    
}

- (void)getProviderDetail {
    self.providerDetailView.entity = self.entity;
    [self.providerDetailView loadData];
    
//    [[RequestManager sharedClient] requestGetProviderDetail:self.entity.Id success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestGetProviderDetail success" message:results];
//
//        self.entity = [results objectForKey:RESPONSE_DATA];
//        self.providerDetailView.entity = self.entity;
//        [self.providerDetailView loadData];
//
//
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestGetProviderDetail failure" message:results];
//        //[Helpers showToast:@"Get salon info failed"];
//    }];
    
}


- (void)handleButtonSavePressed {
    if (self.isAddNewProvider) {
        [self showProgressHub];
        [[RequestManager sharedClient] requestAddProvider:self.providerDetailView.txtName.textField.text Phone:self.providerDetailView.txtPhone.textField.text Active:self.providerDetailView.switchActive.isOn SignedIn:true LastSignedInTime:@"" JobCount:0 SaleAmount:0 CommissionRate:[self.providerDetailView.txtCommissionRate.textField.text doubleValue] PassCode:self.providerDetailView.txtPasscode.textField.text NotificationKey:self.providerDetailView.txtNotificationKey.textField.text WorkHours:0 IsTardy:false success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestAddProvider success" message:results];
            
            if ([self.delegate respondsToSelector:@selector(refreshData)]) {
                [self.delegate refreshData];
            }
            
            [self dismissProgressHub];
            [self btnBackPressed:nil];
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestAddProvider failure" message:results];
            [self dismissProgressHub];
        }];
    } else {
        [self showProgressHub];
        [[RequestManager sharedClient] requestEditProvider:self.entity.Id Name:self.providerDetailView.txtName.textField.text Phone:self.providerDetailView.txtPhone.textField.text Active:self.providerDetailView.switchActive.isOn SignedIn:true LastSignedInTime:@"" JobCount:0 SaleAmount:0 CommissionRate:[self.providerDetailView.txtCommissionRate.textField.text doubleValue] PassCode:self.providerDetailView.txtPasscode.textField.text NotificationKey:self.providerDetailView.txtNotificationKey.textField.text WorkHours:0 IsTardy:false WorkingDays:self.workingDays success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestEditProvider success" message:results];
            
            self.entity.Name = self.providerDetailView.txtName.textField.text;
            self.entity.Phone = self.providerDetailView.txtPhone.textField.text;
            self.entity.PassCode = self.providerDetailView.txtPasscode.textField.text;
            self.entity.NotificationKey = self.providerDetailView.txtNotificationKey.textField.text;
            self.entity.active = self.providerDetailView.switchActive.isOn;
            if ([self.delegate respondsToSelector:@selector(updateProvider:)]) {
                [self.delegate updateProvider:self.entity];
            }
            [self dismissProgressHub];
            [self btnBackPressed:nil];
            
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestEditProvider failure" message:results];
            [self dismissProgressHub];
        }];
        
    }
}

- (void)saveWorkingDays:(NSString*)WorkingDays {
    self.workingDays = WorkingDays;
}




@end

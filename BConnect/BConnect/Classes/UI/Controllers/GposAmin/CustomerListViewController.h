//
//  CustomerListViewController.h
//  BConnect
//
//  Created by Inglab on 21/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"

@interface CustomerListViewController : BaseViewViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>{
    NSArray *searchResults;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray* customersList;
@property (weak, nonatomic) id delegate;

@end

//
//  SelectStoreViewController.m
//  BConnect
//
//  Created by Inglab on 09/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "SelectStoreViewController.h"
#import "SelectCategoryTableViewCell.h"
//#import "GlobalStoreEntity.h"

@interface SelectStoreViewController ()

@end

@implementation SelectStoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    currentPage = 0;
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        if (!weakSelf.isProcessingRequest) {
            [weakSelf.tableView.pullToRefreshView startAnimating];
            [weakSelf refreshData];
        } else {
            [weakSelf.tableView.pullToRefreshView stopAnimating];
        }
    }];
    
    self.storesList = [[NSMutableArray alloc] init];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    //[self hideCart:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!isInit) {
        isInit = YES;
        [self refreshData];
    }
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}

- (void)setupHeader{
    [super setupHeader];
    [self SetHeaderTitle:@"Select Store"];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
   
}

- (void)btnBackPressed:(id)sender{
    NSMutableArray* selectedArray = [[NSMutableArray alloc] init];
    
     for (int i = 0; i < self.storesList.count; i++) {
     GlobalStoreEntity* entity = [self.storesList objectAtIndex:i];
        if (entity.isSelected == true) {
            [selectedArray addObject:@(entity.Id)];
        }
     }
    
    if ([self.delegate respondsToSelector:@selector(reassignStoreList:)]) {
        [self.delegate reassignStoreList:selectedArray];
    }
    
    [super btnBackPressed:sender];
}


//data

- (void)getstoresList {
    
    if (self.isProcessingRequest)
        return;
    
    currentPage++;
    isErrorNetwork = NO;
    isShowNoItemView = NO;
    
    [self showProgressHub];
    [[RequestManager sharedClient] requestGetGlobalStoreList:currentPage count:10 success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetstoresList success" message:results];
        [self dismissProgressHub];
        
        NSMutableArray* newsArr = [[Helpers getArrayDiffNull:[results objectForKey:RESPONSE_DATA]] mutableCopy];
        
        for (int i = 0; i < newsArr.count; i++) {
            GlobalStoreEntity* entity = [newsArr objectAtIndex:i];
            for (int j = 0; j < self.storeIdsList.count; j++) {
                if (entity.Id == [[self.storeIdsList objectAtIndex:j] integerValue]) {
                    entity.isSelected = true;
                }
            }
        }
        
        self.isHasMore = [Helpers getBoolDiffNull:[results objectForKey:RESPONSE_HAS_MORE]];
        
        if (self.isHasMore && currentPage == 1) {
            [self addLoadMore];
        }
        
        if (newsArr.count > 0) {
            if (currentPage == 1) {
                [self.storesList removeAllObjects];
                [self.storesList addObjectsFromArray:newsArr];
                [self.tableView reloadData];
                
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_GET_NEWS_NEW object:nil];
                
            } else {
                NSUInteger oldCount = self.storesList.count;
                
                [self.storesList addObjectsFromArray:newsArr];
                
                NSMutableArray* indexPaths = [[NSMutableArray alloc] init];
                for (NSUInteger i = oldCount; i < self.storesList.count; i++) {
                    [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                }
                [self.tableView beginUpdates];
                [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView endUpdates];
            }
        }
        
        //        if (newsArr.count == 0 && currentPage == 1) {
        //            [self.notificationList removeAllObjects];
        //            [self showNoProduct];
        //        }
        
        [self dismissProgressHub];
        [self.tableView.pullToRefreshView stopAnimating];
        [self.tableView.infiniteScrollingView stopAnimating];
        
        [self.tableView reloadData];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetstoresList failure" message:results];
        //[Helpers showToast:@"Get salon info failed"];
        [self dismissProgressHub];
        [self showNetworkErrorView];
    }];
}

- (void)addLoadMore{
    __weak typeof(self) weakSelf = self;
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        if (weakSelf.isHasMore && !weakSelf.isProcessingRequest) {
            [weakSelf loadMore];
            [weakSelf.tableView.infiniteScrollingView startAnimating];
        } else{
            [weakSelf.tableView.infiniteScrollingView stopAnimating];
        }
    }];
}

- (void)loadMore {
    oldCurrentPage = currentPage;
    [self getstoresList];
}

- (void)refreshData{
    oldCurrentPage = currentPage;
    currentPage = 0;
    [self getstoresList];
}

#pragma mark - NoItemViewDelegate methods

- (void)btnClickedNoItemView {
    switch (actionType) {
        case ACTION_EMPTY: {
            actionType = 0;
            [self hideNoItemView];
            [self refreshData];
        }
            break;
        case ACTION_GET_LIST: {
            actionType = 0;
            [self hideNoItemView];
            oldCurrentPage = currentPage;
            [self getstoresList];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.storesList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectCategoryTableViewCell *cell = (SelectCategoryTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"SelectCategoryTableViewCell"];
    
    if(!cell){
        cell = [[SelectCategoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectCategoryTableViewCell"];
    }
    
    GlobalStoreEntity* entity = [self.storesList objectAtIndex:indexPath.row];
    [cell loadStoreCellInfo:entity];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:false];
    
//    StoreDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"StoreDetailViewController"];
//    controller.entity = [self.storesList objectAtIndex:indexPath.row];
//    controller.delegate = self;
//    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

@end

//
//  RegisterUserInfoViewController.m
//  spabees
//
//  Created by vu van long on 1/7/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "RegisterUserInfoViewController.h"
#import "Helpers.h"
#import "DatabaseManager.h"
#import "RequestManager.h"
#import "SessionManager.h"
#import "Helpers.h"
#import "EntityManager.h"
#import "SVProgressHUD.h"
#import "RadioButton.h"
#import "UserEntity.h"
#import "RequestManager.h"

#define YEAR_NUMBER 99
#define DISTANCE_CURRENT_YEAR 18
#define TAG @"RegisterUserInfo"
@interface RegisterUserInfoViewController ()

@property (nonatomic) bool isUploading;
@property (nonatomic) bool isUpdatingInfo;

@end

@implementation RegisterUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.edgesForExtendedLayout=UIRectEdgeTop;
    [self setupHeader];
//    if (!self.countryEntitySelected && [SessionManager sharedClient].userEntity.countryEntity) {
//        self.countryEntitySelected = [SessionManager sharedClient].userEntity.countryEntity;
//    }
    self.scrollWidthConstraint.constant = CGRectGetWidth([UIScreen mainScreen].bounds);
    [self.scrollView layoutIfNeeded];
    
    if (!self.userInfoView) {
        int loginType = (int)[SharePrefUtils getIntPreference:PREFKEY_LOGIN_TYPE];
        //if (loginType == LOGIN_PHONE) {
        
            if (IS_IPAD) {
                self.userInfoView = [[UserInfoView alloc] initWithFrame:CGRectMake(0, 0, self.scrollWidthConstraint.constant, 800)];
            } else {
                self.userInfoView = [[UserInfoView alloc] initWithFrame:CGRectMake(0, 0, self.scrollWidthConstraint.constant, 660)];
            }
        
            self.userInfoView.delegate = self;
        
            //self.userInfoView.countryEnitySelected = self.countryEntitySelected;
            //self.userInfoView.homeTownTopConstraint.constant = 15;
            //self.userInfoView.phoneContainer.hidden = YES;
            //self.userInfoView.txtPhone.hidden = YES;
            //self.userInfoView.countryCodeContainer.hidden = YES;
            //self.userInfoView.txtCountryField.hidden = YES;
        
            [self.userInfoView prepareLayout];
            [self.scrollView addSubview:self.userInfoView];
            //self.userInfoView.kContentHeightDefault = 345;
            [self.scrollView setContentSize:self.userInfoView.frame.size];
            
        /*
        } else {
            self.userInfoView = [[UserInfoView alloc] initWithFrame:CGRectMake(0, 5, self.scrollWidthConstraint.constant, 470)];
            self.userInfoView.delegate = self;
            self.userInfoView.countryEnitySelected = self.countryEntitySelected;
            [self.userInfoView prepareLayout];
            self.userInfoView.kContentHeightDefault = 400;
            [self.scrollView addSubview:self.userInfoView];
            [self.scrollView setContentSize:self.userInfoView.frame.size];
        }
         */
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)initData{
//    if (![SessionManager sharedClient].userEntity.countryEntity && ![CoreStringUtils isEmpty:[SessionManager sharedClient].userEntity.countryCode]) {
//        [[RequestManager sharedClient] requestCountriesListV2Request:^(NSMutableDictionary *results, int errorCode){
//            NSArray* countryList = [results objectForKey:RESPONSE_COUNTRIES];
//            for (CountryEntityV2* country in countryList) {
//                if ([country.isoCode isEqualToString:[SessionManager sharedClient].userEntity.countryCode]) {
//                    [self countrySelected:country];
//                    [SessionManager sharedClient].userEntity.countryEntity = country;
//                    break;
//                }
//            }
//        } failure:^(NSMutableDictionary *results, int errorCode){
//            [Log d:0 tag:@"requestLogin failure" message:results];
//            [self dismissProgressHub];
//            switch (errorCode) {
//                case REQUEST_TIMEOUT:
//                case UNKNOWN_EXCEPTION: {
//                    [self showMessage:[CoreStringUtils getStringWithErrorCode:@"str_unknow_error" errorCode:errorCode]];
//                    break;
//                }
//                default:
//                    break;
//            }
//        }];
//    }
//    else if (!self.countryEntitySelected && ![CoreStringUtils isEmpty:[SessionManager sharedClient].userEntity.countryCode]){
//        [self countrySelected:[SessionManager sharedClient].userEntity.countryEntity];
//    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:@"" target:self action:@selector(btnBackPressed:)];
    [self setUserInfoHeaderStyle];
    
}

- (void)btnBackPressed:(id)sender{
    if (self.isEditUserInfo) {
        [super btnBackPressed:sender];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self hideHeaderView:YES];
    }
}

- (void)showMessage:(NSString *)message{
    /*
    CGFloat contentHeight = [self.userInfoView showError:message];
    CGRect frame = self.userInfoView.frame;
    frame.size.height = contentHeight;
    self.userInfoView.frame = frame;
    [self.userInfoView layoutIfNeeded];
     */
}

- (void)countrySelected:(CountryEntityV2 *)entity{
    /*
    self.countryEntitySelected = entity;
    [self.userInfoView setCountrySelected:self.countryEntitySelected];
     */
}

#pragma mark - Business methods

-(void)btnNextPressed {
    @synchronized(self) {
        NSString *phone = [self.userInfoView.txtPhonenumber.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *email = [self.userInfoView.txtEmail.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        self.params = [NSMutableDictionary dictionary];
        [self.params setValue:phone forKey:REQUEST_NATIONAL_NUMBER];
        [self.params setValue:email forKey:REQUEST_EMAIL];
        [self.params setValue:[SessionManager sharedClient].userEntity.userId forKey:REQUEST_ID];
        
        if (![Helpers isValidEmail:email]) {
            [[[UIAlertView alloc] initWithTitle:@"" message:[CoreStringUtils getStringRes:@"str_email_invalid"] delegate:self cancelButtonTitle:[CoreStringUtils getStringRes:@"str_ok"] otherButtonTitles:nil, nil] show];
            return;
        }
        
        [self showProgressHub];
        self.isUpdatingInfo = true;
        
        [[RequestManager sharedClient] requestUpdateUserInfo:self.params success:^(NSMutableDictionary *results, int errorCode){
            [Log d:0 tag:@"Update UserInfo success" message:results];
            
            UserEntity* userEntity = [results objectForKey:RESPONSE_DATA];
            
            self.isUpdatingInfo = false;
            
            if (userEntity) {
                [SessionManager sharedClient].userEntity = userEntity;
            } else {
                UserEntity* userEntity = [SessionManager sharedClient].userEntity;
                userEntity.email = email;
                userEntity.nationalNumber = phone;
                
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CHANGE_USER_INFO object:nil];
            
            //                if (![self.countryEntitySelected.isoCode isEqualToString:userEntity.countryCode]) {
            //                    userEntity.countryEntity = self.countryEntitySelected;
            //                }
            
            NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
            [data setValue:[NSNumber numberWithInteger:ACTION_UPDATE_USER] forKey:DB_TYPE_ACTION];
            [data setValue:[SessionManager sharedClient].userEntity forKey:DB_USER_ENTITY];
            
            [[DatabaseManager sharedClient] addToQueue:data success:^(NSMutableDictionary *results, int errorCode) {
                [Log d:0 tag:TAG @" Edit" message:[NSString stringWithFormat:@"Update UserInfo to DB success with result=%@",results]];
                [self dismissProgressHub];
                [self.navigationController popViewControllerAnimated:YES];
            } failure:^(NSMutableDictionary *results, int errorCode) {
                [Log d:0 tag:TAG @" Edit" message:[NSString stringWithFormat:@"Update UserInfo to DB failed with errorCode=%d",errorCode]];
                [self dismissProgressHub];
            }];
            
            [Helpers showToast:[CoreStringUtils getStringRes:@"str_update_user_info_successfull"]];
            
        } failure:^(NSMutableDictionary *results, int errorCode){
            [Log d:0 tag:@"Update UserInfo failure" message:results];
            self.isUpdatingInfo = false;
            switch (errorCode) {
                    /*
                case UNABLE_CHANGE_NAME: {
                    [self showMessage:[CoreStringUtils getStringWithErrorCode:@"str_error_unable_change_name" errorCode:errorCode]];
                    break;
                    
                }
                     */
                case REQUEST_TIMEOUT:
                case UNKNOWN_EXCEPTION: {
                    [self showMessage:[CoreStringUtils getStringWithErrorCode:@"str_unknow_error" errorCode:errorCode]];
                    break;
                }
                    /*
                case DISCOUNT_CODE_INVALID:
                    [self showMessage:[CoreStringUtils getStringWithErrorCode:@"str_error_license_code_invalid" errorCode:errorCode]];
                    break;
                    */
                default:
                    [self showMessage:[CoreStringUtils getStringWithErrorCode:@"str_error_update_user_info" errorCode:errorCode]];
                    break;
            }
            [self dismissProgressHub];
        }];
    }
}

- (void)showCountryList {
    CountriesViewControllerV2* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"CountriesViewControllerV2"];
    controller.delegate = self;
    [self pushToViewController:controller];
}

- (void)btnDonePressed {
    @synchronized(self) {
        [self showMessage:nil];
        [self.view endEditing:YES];
        [self dismissStatusMessage];
        
        NSString *firstName = [self.userInfoView.txtFirstName.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *lastName = [self.userInfoView.txtLastName.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *email = [self.userInfoView.txtEmail.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        /*
        NSString *hometown = [self.userInfoView.txtHometown.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *interestedIn = [self.userInfoView.txtInterestedIn.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *slogan = [self.userInfoView.txtSlogan.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *phone = [self.userInfoView.txtPhone.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
         */
        
        self.params = [NSMutableDictionary dictionary];
        [self.params setValue:firstName forKey:REQUEST_FIRST_NAME];
        [self.params setValue:lastName forKey:REQUEST_LAST_NAME];
        [self.params setValue:email forKey:REQUEST_EMAIL];
        
        [self.params setValue:[SessionManager sharedClient].userEntity.userId forKey:REQUEST_ID];
        int loginType = (int)[SharePrefUtils getIntPreference:PREFKEY_LOGIN_TYPE];
        
        
        /*
        if (loginType != LOGIN_PHONE) {
            [self.params setValue:phone forKey:REQUEST_PHONE];
            [self.params setValue:self.countryEntitySelected.phoneCode forKey:REQUEST_COUNTRY_CODE];
        }
        */
        
        if (self.isEditUserInfo == false) {
                      
        } else {
            if (![Helpers isValidEmail:email]) {
                [[[UIAlertView alloc] initWithTitle:@"" message:[CoreStringUtils getStringRes:@"str_email_invalid"] delegate:self cancelButtonTitle:[CoreStringUtils getStringRes:@"str_ok"] otherButtonTitles:nil, nil] show];
                return;
            }
            
            [self showProgressHub];
            self.isUpdatingInfo = true;
        
            [[RequestManager sharedClient] requestUpdateUserInfo:self.params success:^(NSMutableDictionary *results, int errorCode){
                [Log d:0 tag:@"Update UserInfo success" message:results];
                
                UserEntity* userEntity = [results objectForKey:RESPONSE_DATA];
                
                self.isUpdatingInfo = false;
                
                if (userEntity) {
                    [SessionManager sharedClient].userEntity = userEntity;
                } else {
                    UserEntity* userEntity = [SessionManager sharedClient].userEntity;
                    userEntity.firstName = firstName;
                    userEntity.lastName = lastName;
                    
                    userEntity.email = email;
                    
                    if (loginType != LOGIN_PHONE) {
                        
                        userEntity.countryCode = self.countryEntitySelected.phoneCode;
                    }
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CHANGE_USER_INFO object:nil];
                
//                if (![self.countryEntitySelected.isoCode isEqualToString:userEntity.countryCode]) {
//                    userEntity.countryEntity = self.countryEntitySelected;
//                }
                
                NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
                [data setValue:[NSNumber numberWithInteger:ACTION_UPDATE_USER] forKey:DB_TYPE_ACTION];
                [data setValue:[SessionManager sharedClient].userEntity forKey:DB_USER_ENTITY];
                
                [[DatabaseManager sharedClient] addToQueue:data success:^(NSMutableDictionary *results, int errorCode) {
                    [Log d:0 tag:TAG @" Edit" message:[NSString stringWithFormat:@"Update UserInfo to DB success with result=%@",results]];
                    [self dismissProgressHub];
                    [self.navigationController popViewControllerAnimated:YES];
                } failure:^(NSMutableDictionary *results, int errorCode) {
                    [Log d:0 tag:TAG @" Edit" message:[NSString stringWithFormat:@"Update UserInfo to DB failed with errorCode=%d",errorCode]];
                    [self dismissProgressHub];
                }];
                
                [Helpers showToast:[CoreStringUtils getStringRes:@"str_update_user_info_successfull"]];
                
            } failure:^(NSMutableDictionary *results, int errorCode){
                [Log d:0 tag:@"Update UserInfo failure" message:results];
                self.isUpdatingInfo = false;
                switch (errorCode) {
                    case UNABLE_CHANGE_NAME: {
                        [self showMessage:[CoreStringUtils getStringWithErrorCode:@"str_error_unable_change_name" errorCode:errorCode]];
                        break;

                    }
                    case REQUEST_TIMEOUT:
                    case UNKNOWN_EXCEPTION: {
                        [self showMessage:[CoreStringUtils getStringWithErrorCode:@"str_unknow_error" errorCode:errorCode]];
                        break;
                    }
                    case DISCOUNT_CODE_INVALID:
                        [self showMessage:[CoreStringUtils getStringWithErrorCode:@"str_error_license_code_invalid" errorCode:errorCode]];
                        break;
                        
                    default:
                        [self showMessage:[CoreStringUtils getStringWithErrorCode:@"str_error_update_user_info" errorCode:errorCode]];
                        break;
                }
                [self dismissProgressHub];
            }];
        }
    }
}

@end

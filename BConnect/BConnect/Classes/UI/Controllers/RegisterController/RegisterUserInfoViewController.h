//
//  RegisterUserInfoViewController.h
//  spabees
//
//  Created by vu van long on 1/7/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "BaseViewViewController.h"
#import "CountriesViewControllerV2.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "CountryEntityV2.h"
#import "UserInfoView.h"

@interface RegisterUserInfoViewController : BaseViewViewController<CountriesViewControllerV2Delegate, UserInfoViewDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollWidthConstraint;
@property (strong, nonatomic) CountryEntityV2* countryEntitySelected;
@property (strong, nonatomic) UserInfoView* userInfoView;
@property (assign, nonatomic) BOOL isEditUserInfo;
@property (strong, nonatomic) NSMutableDictionary *params;

@end

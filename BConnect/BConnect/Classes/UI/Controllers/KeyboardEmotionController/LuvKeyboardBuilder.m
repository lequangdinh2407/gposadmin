//
//  WUDemoKeyboardBuilder.m
//  WUEmoticonsKeyboardDemo
//
//  Created by YuAo on 7/20/13.
//  Copyright (c) 2013 YuAo. All rights reserved.
//

#import "LuvKeyboardBuilder.h"
#import "LuvKeyboardTextKeyCell.h"
#import "LuvKeyboardPressedCellPopupView.h"

@implementation LuvKeyboardBuilder

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIImage* tmpimage = [image copy];
    UIGraphicsBeginImageContext(newSize);
    [tmpimage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (WUEmoticonsKeyboard *)sharedEmoticonsKeyboard {
    static WUEmoticonsKeyboard *_sharedEmoticonsKeyboard;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //create a keyboard of default size
        WUEmoticonsKeyboard *keyboard = [WUEmoticonsKeyboard keyboard];
        NSMutableArray* objectGroup = [NSMutableArray array];
        
        // Icon creator
        NSDictionary* tmpStickers = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"StickerList" ofType:@"plist"]];
        NSArray * stickeGroups = @[@"Funny", @"Rabbit", @"Nailgirl", @"Pink", @"Mood"];
        for (NSString* stickerName in stickeGroups) {
            NSArray* stickers = [tmpStickers objectForKey:stickerName];
            NSMutableArray* stickerArr = [NSMutableArray array];
            for (NSString* stickerName in stickers) {
                WUEmoticonsKeyboardKeyItem *emotion = [[WUEmoticonsKeyboardKeyItem alloc] init];
                emotion.image = [UIImage imageNamed:stickerName];
                emotion.textToInput = stickerName;
                [stickerArr addObject:emotion];
            }
            
            //Icon key group
            WUEmoticonsKeyboardKeyItemGroup *imageIconsGroup = [[WUEmoticonsKeyboardKeyItemGroup alloc] init];
            imageIconsGroup.keyItems = stickerArr;
            
            NSString* imageNormalName = [NSString stringWithFormat:@"%@_normal", stickerName];
            UIImage *keyboardEmotionImage = [UIImage imageNamed:imageNormalName];
            
            NSString* imageSelectedName = [NSString stringWithFormat:@"%@_selected", stickerName];
            UIImage *keyboardEmotionSelectedImage = [UIImage imageNamed:imageSelectedName];
            
            if ([UIImage instancesRespondToSelector:@selector(imageWithRenderingMode:)]) {
                keyboardEmotionImage = [keyboardEmotionImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                keyboardEmotionSelectedImage = [keyboardEmotionSelectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            }
            imageIconsGroup.image = keyboardEmotionImage;
            imageIconsGroup.selectedImage = keyboardEmotionSelectedImage;
            [objectGroup addObject:imageIconsGroup];
        }
        
        //Objects keys
        NSDictionary* tmpEmojis = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"EmojisList" ofType:@"plist"]];
        NSArray *objectList = @[@"People"];
        
        for (NSString* objName in objectList) {
            NSArray *ObjectKeys = [tmpEmojis objectForKey:objName];
            NSMutableArray *objectKeyItems = [NSMutableArray array];
            for (NSString *object in ObjectKeys) {
                WUEmoticonsKeyboardKeyItem *keyItem = [[WUEmoticonsKeyboardKeyItem alloc] init];
                keyItem.title = object;
                keyItem.textToInput = object;
                [objectKeyItems addObject:keyItem];
            }
            
            //Objects key group
            CGFloat width = (CGRectGetWidth([UIScreen mainScreen].bounds) - (kKeyboardItemNumberInRow+2)*kKeyboardItemAlign)/kKeyboardItemNumberInRow;
            WUEmoticonsKeyboardKeysPageFlowLayout *textIconsLayout = [[WUEmoticonsKeyboardKeysPageFlowLayout alloc] init];
            textIconsLayout.itemSize = CGSizeMake(width, 65);
            textIconsLayout.itemSpacing = 4;
            textIconsLayout.lineSpacing = 4;
            textIconsLayout.pageContentInsets = UIEdgeInsetsMake(4,4,4,4);
            
            WUEmoticonsKeyboardKeyItemGroup *objectIconsGroup = [[WUEmoticonsKeyboardKeyItemGroup alloc] init];
            objectIconsGroup.keyItems = objectKeyItems;
            objectIconsGroup.keyItemsLayout = textIconsLayout;
            objectIconsGroup.keyItemCellClass = LuvKeyboardTextKeyCell.class;
            
            UIImage *keyboardTextImage = [UIImage imageNamed:@"emotion_normal"];
            UIImage *keyboardTextSelectedImage = [UIImage imageNamed:@"emotion_selected"];
            if ([UIImage instancesRespondToSelector:@selector(imageWithRenderingMode:)]) {
            keyboardTextImage = [keyboardTextImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                keyboardTextSelectedImage = [keyboardTextSelectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            }
            //objectIconsGroup.title = [ObjectKeys firstObject];
            objectIconsGroup.image = keyboardTextImage;
            objectIconsGroup.selectedImage = keyboardTextSelectedImage;
            
            //Custom text icons scroll background
            if (textIconsLayout.collectionView) {
                UIView *textGridBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [textIconsLayout collectionViewContentSize].width, [textIconsLayout collectionViewContentSize].height)];
                textGridBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
                textGridBackgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"keyboard_grid_bg"]];
                [textIconsLayout.collectionView addSubview:textGridBackgroundView];
            }
            
            [objectGroup addObject:objectIconsGroup];
        }
        
        //Set keyItemGroups
        keyboard.keyItemGroups = objectGroup;
        
        //Setup cell popup view
        [keyboard setKeyItemGroupPressedKeyCellChangedBlock:^(WUEmoticonsKeyboardKeyItemGroup *keyItemGroup, WUEmoticonsKeyboardKeyCell *fromCell, WUEmoticonsKeyboardKeyCell *toCell) {
            [LuvKeyboardBuilder sharedEmotionsKeyboardKeyItemGroup:keyItemGroup pressedKeyCellChangedFromCell:fromCell toCell:toCell];
        }];

        //Keyboard appearance
        
        //Custom utility keys
        [keyboard setImage:[UIImage imageNamed:@"keyboard"] forButton:WUEmoticonsKeyboardButtonKeyboardSwitch state:UIControlStateNormal];
        [keyboard setImage:[UIImage imageNamed:@"keyboard_delete"] forButton:WUEmoticonsKeyboardButtonBackspace state:UIControlStateNormal];
        [keyboard setImage:[UIImage imageNamed:@"keyboard_pressed"] forButton:WUEmoticonsKeyboardButtonKeyboardSwitch state:UIControlStateHighlighted];
        [keyboard setImage:[UIImage imageNamed:@"keyboard_delete_pressed"] forButton:WUEmoticonsKeyboardButtonBackspace state:UIControlStateHighlighted];
        [keyboard setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Space", @"") attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor darkGrayColor]}] forButton:WUEmoticonsKeyboardButtonSpace state:UIControlStateNormal];
        [keyboard setBackgroundImage:[[UIImage imageNamed:@"keyboard_segment_normal"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)] forButton:WUEmoticonsKeyboardButtonSpace state:UIControlStateNormal];

        //Keyboard background
        [keyboard setBackgroundImage:[[UIImage imageNamed:@"keyboard_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 1, 0, 1)]];
        
        _sharedEmoticonsKeyboard = keyboard;
    });
    return _sharedEmoticonsKeyboard;
}

+ (void)sharedEmotionsKeyboardKeyItemGroup:(WUEmoticonsKeyboardKeyItemGroup *)keyItemGroup
             pressedKeyCellChangedFromCell:(WUEmoticonsKeyboardKeyCell *)fromCell
                                    toCell:(WUEmoticonsKeyboardKeyCell *)toCell
{
    static LuvKeyboardPressedCellPopupView *pressedKeyCellPopupView;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        pressedKeyCellPopupView = [[LuvKeyboardPressedCellPopupView alloc] initWithFrame:CGRectMake(0, 0, 83, 110)];
        pressedKeyCellPopupView.hidden = YES;
        //[[self sharedEmoticonsKeyboard] addSubview:pressedKeyCellPopupView];
    });
    
    if ([[self sharedEmoticonsKeyboard].keyItemGroups indexOfObject:keyItemGroup] == 0) {
        [[self sharedEmoticonsKeyboard] bringSubviewToFront:pressedKeyCellPopupView];
        if (toCell) {
            pressedKeyCellPopupView.keyItem = toCell.keyItem;
            pressedKeyCellPopupView.hidden = NO;
            CGRect frame = [[self sharedEmoticonsKeyboard] convertRect:toCell.bounds fromView:toCell];
            pressedKeyCellPopupView.center = CGPointMake(CGRectGetMidX(frame), CGRectGetMaxY(frame)-CGRectGetHeight(pressedKeyCellPopupView.frame)/2);
        }else{
            pressedKeyCellPopupView.hidden = YES;
        }
    }
}

@end

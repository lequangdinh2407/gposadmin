//
//  WUDemoKeyboardBuilder.h
//  WUEmoticonsKeyboardDemo
//
//  Created by YuAo on 7/20/13.
//  Copyright (c) 2013 YuAo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WUEmoticonsKeyboard.h"

@interface LuvKeyboardBuilder : NSObject

+ (WUEmoticonsKeyboard *)sharedEmoticonsKeyboard;

@end

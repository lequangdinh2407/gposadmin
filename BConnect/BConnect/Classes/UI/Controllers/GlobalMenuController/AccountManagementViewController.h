//
//  AccountManagementViewController.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/29/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "BaseViewViewController.h"

@interface AccountManagementViewController : BaseViewViewController <UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate>{
    BOOL isSocialNetworkLogin;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

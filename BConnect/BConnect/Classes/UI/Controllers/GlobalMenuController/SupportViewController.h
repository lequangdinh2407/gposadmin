//
//  SupportViewController.h
//  luvkonect
//
//  Created by Dragon on 6/8/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "BaseViewViewController.h"
#import "SupportView.h"
#import "CountriesViewControllerV2.h"
#import "TPKeyboardAvoidingScrollView.h"
#import <MessageUI/MessageUI.h>

@interface SupportViewController : BaseViewViewController<UIScrollViewDelegate, SupportViewDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate, CountriesViewControllerV2Delegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollWidthConstraint;
@property (strong, nonatomic) SupportView* supportView;
@property (strong, nonatomic) NSMutableDictionary *params;
@end

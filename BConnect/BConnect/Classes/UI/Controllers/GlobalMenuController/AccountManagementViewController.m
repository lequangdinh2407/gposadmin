//
//  AccountManagementViewController.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/29/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "AccountManagementViewController.h"
#import "RequestManager.h"
#import "DatabaseManager.h"
#import "SVProgressHUD.h"
#import "Helpers.h"
#import "SessionManager.h"
#import "RegisterUserInfoViewController.h"

#import "MarqueeLabel.h"
#import "LoginViewController.h"

#define DETAIL_TEXT_COLOR   [UIColor colorWithRed:136/255.0 green:153/255.0 blue:166/255.0 alpha:1.0]

@interface AccountManagementViewController ()

@end

@implementation AccountManagementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:[CoreStringUtils getStringRes:@"str_account_management_back_title"] target:self action:@selector(btnBackPressed:)];
}

- (void)btnBackPressed:(id)sender{
    [super btnBackPressed:sender];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        
    } else if(buttonIndex == 1) {
        [self showProgressHub];
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        NSString* deviceId = [Helpers getUDID];
        
        [[RequestManager sharedClient] requestLogout:deviceId success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestLogout success" message:results];
            
            [SharePrefUtils clearAll];
            [self dismissProgressHub];
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestLogout failure" message:results];
            [SharePrefUtils clearAll];
            [self dismissProgressHub];
        }];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOGOUT object:nil];
                
        ((BaseViewViewController*)rootView).cartBadger.badgeText = @"0";        
        [[SessionManager sharedClient] logOut];        

        
//        LoginViewController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//        [self pushToViewController:controller];
    }
}

- (void)loadCellContent:(UITableViewCell*)cell atIndex:(NSIndexPath*)indexPath{
    UILabel* textLabel = [cell.contentView viewWithTag:1];
    UILabel* detailTextLabel = [cell.contentView viewWithTag:2];
    
    switch (indexPath.row) {
        case 0:
            textLabel.text = @"Tutorial";
            detailTextLabel.text = @"You will get to know what you can do";
            break;
        case 1: {
            textLabel.text = @"News Feed Preferences";
            MarqueeLabel* lbDetail = [[MarqueeLabel alloc] initWithFrame:CGRectMake(14, 37, cell.frame.size.width - 20, 12)];
            lbDetail.font = [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:12.0];
            lbDetail.textColor = DETAIL_TEXT_COLOR;
            lbDetail.text = @"Reconnect with people you unfollowed their feeds";
            lbDetail.marqueeType = MLContinuous;
            lbDetail.trailingBuffer = 20;
            CGSize expectSize = [lbDetail sizeThatFits:CGSizeMake(MAXFLOAT, CGRectGetHeight(lbDetail.frame))];
            float duration = expectSize.width / 120;
            [lbDetail setScrollDuration:duration];
            [cell.contentView addSubview:lbDetail];
        }
            break;
        case 2:
            textLabel.text = @"Update User Info";
            detailTextLabel.text = @"Update First Name, Last Name, ...";
            break;
        case 3:
            textLabel.text = @"Log out";
            detailTextLabel.text = @"Log out client";
            break;
            
        default:
            break;
    }
}

- (void)excuteActionCellAtIndex:(NSIndexPath*)indexPath{
    switch (indexPath.row) {
        case 0:
        {
            
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case 1:
        {
           
        }
            break;
        case 2:
        {
            RegisterUserInfoViewController* controller = [registerStoryboard instantiateViewControllerWithIdentifier:@"RegisterUserInfoViewController"];
            controller.isEditUserInfo = YES;
            [self pushToViewController:controller];
        }
            break;
        case 3:
        {
            [[AlertviewManager sharedClient] showTitleMessage:[CoreStringUtils getStringRes:@"str_note"] message:[CoreStringUtils getStringRes:@"str_do_you_want_to_change_account"] delegate:self cancelButton:[CoreStringUtils getStringRes:@"str_cancel"] otherButton:[CoreStringUtils getStringRes:@"str_ok"]];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - tableview methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return isSocialNetworkLogin ? 3 : 4;
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor whiteColor]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"accountItemCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"accountItemCell"];
    }
    
    //Endline
    UIView * line = [[UIView alloc] initWithFrame:CGRectMake(15, 54, CGRectGetWidth([UIScreen mainScreen].bounds) - 30, 1)];
    line.backgroundColor = [APP_SUB_TEXT_COLOR colorWithAlphaComponent:0.1];
    [cell.contentView addSubview:line];

    UILabel* textLabel = [[UILabel alloc] initWithFrame:CGRectMake(14, 10, CGRectGetWidth([UIScreen mainScreen].bounds) - 14, 20)];
    textLabel.font = [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:16.0];
    textLabel.textColor = APP_TEXT_COLOR;
    textLabel.tag = 1;
    
    UILabel* detailTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(14, 33, CGRectGetWidth([UIScreen mainScreen].bounds) - 14, 14)];
    detailTextLabel.font = [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:12.0];
    detailTextLabel.textColor = DETAIL_TEXT_COLOR;
    detailTextLabel.tag = 2;
    
    [cell.contentView addSubview:textLabel];
    [cell.contentView addSubview:detailTextLabel];
    [self loadCellContent:cell atIndex:indexPath];

    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_accessory"]];
    imageView.frame = CGRectMake(0, 0, 6, 11);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    cell.accessoryView = imageView;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self excuteActionCellAtIndex:indexPath];
}

@end

// MEMenuViewController.m
// TransitionFun
//
// Copyright (c) 2013, Michael Enriquez (http://enriquez.me)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "GlobalMenuViewController.h"
#import "AccountManagementViewController.h"
#import "UserEntity.h"
#import "SessionManager.h"
#import "SupportViewController.h"
#import "MenuItemCell.h"
#import "RegisterUserInfoViewController.h"
#import "LoginViewController.h"
#import "HelpViewController.h"
#import "UIAlertView+Blocks.h"
#import "CustomerListViewController.h"
#import "DeviceUserViewController.h"
#import "ProviderViewController.h"
#import "CategoryViewController.h"
#import "AdminDetailViewController.h"
#import "ReportViewController.h"
#import "StoreSettingViewController.h"
#import "BussinessHoursViewController.h"
#import "StoreListViewController.h"
#import "StoreOwnerListViewController.h"
#import "AccountDetailViewController.h"
#import "StoreOwnerEntity.h"
#import "HomeViewController.h"

@implementation GlobalMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:40/255.0 green:37/255.0 blue:44/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:22/255.0 green:22/255.0 blue:22/255.0 alpha:1.0].CGColor];
    
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    // Do any additional setup after loading the view.
    self.menuItems = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"menuList" ofType:@"plist"]];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES];
    self.menuItems = [self.menuItems sortedArrayUsingDescriptors:@[sort]];
    
    self.menuList = [[NSMutableArray alloc] init];
    self.itemList1 = [[NSMutableArray alloc] init];
    self.itemList2 = [[NSMutableArray alloc] init];
    self.itemList3 = [[NSMutableArray alloc] init];
    self.itemList4 = [[NSMutableArray alloc] init];
    self.itemList5 = [[NSMutableArray alloc] init];
    
    for (int i = 0; i< self.menuItems.count; i++) {
        NSDictionary* item = [self.menuItems objectAtIndex:i];
        NSString* group = [item objectForKey:@"group"];
        if ([group isEqualToString:@"1"]) {
            [self.itemList1 addObject:item];
        } else if ([group isEqualToString:@"3"]) {
            [self.itemList3 addObject:item];
        } else if ([group isEqualToString:@"5"]) {
            [self.itemList5 addObject:item];
        }
    }
    
    [self.menuList addObject:self.itemList1];
    [self.menuList addObject:self.itemList2];
    [self.menuList addObject:self.itemList3];
    [self.menuList addObject:self.itemList4];
    [self.menuList addObject:self.itemList5];
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (@available(iOS 10, *)) {
        id tmp = bconnectAppDelegate.rootNavigation;
        if (!tmp) {
            bconnectAppDelegate.rootNavigation = [bconnectAppDelegate getRootNavigation];
        }
    } else {
        bconnectAppDelegate.rootNavigation = [bconnectAppDelegate getRootNavigation];
    }
    
    //[SessionManager sharedClient].userEntity.profileType = 1;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.frostedViewController hideMenuViewController];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertDBLoginInfo {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:[NSNumber numberWithInteger:ACTION_INSERT_ALL_LOGIN_INFO] forKey:DB_TYPE_ACTION];
    [data setValue:[SessionManager sharedClient].userEntity forKey:DB_USER_ENTITY];
    [data setValue:[SessionManager sharedClient].sessionKey forKey:DB_SESSION_KEY];
    [data setValue:[SessionManager sharedClient].domain forKey:DB_DOMAIN];
    [data setValue:[SessionManager sharedClient].accountEntity forKey:DB_ACCOUNT_ENTITY];
    
    NSInteger flag = (FLAG_DELETE_ACCOUNT | FLAG_DELETE_SESSION | FLAG_DELETE_USER);
    [data setValue:[NSNumber numberWithInteger:flag] forKey:DB_FLAG_ACTION];
    
    [[DatabaseManager sharedClient] addToQueue:data success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"insertDBLoginInfo success" message:results];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"insertDBLoginInfo failure" message:results];
    }];
}

- (void)viewNoti{
    [self.tableView reloadData];
}

#pragma mark - UITableview methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([SessionManager sharedClient].userEntity.profileType == 1) {
        return 6;
    }
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0)
        return 0;
    return CATEGORY_HEADER_SECTION_HEIGHT;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0)
        return nil;
    
    UIView *headerSection = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, CATEGORY_HEADER_SECTION_HEIGHT)];
    [headerSection setBackgroundColor:[UIColor clearColor]];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, tableView.frame.size.width, CATEGORY_HEADER_SECTION_HEIGHT)];
    [btn setTag:section];
    [btn addTarget:self action:@selector(sectionTapped:) forControlEvents:UIControlEventTouchUpInside];
    [headerSection addSubview:btn];
    
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15, 20, 20)];
    if (section == 1) {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15, 28, 20)];
    }
    
    UILabel* lbCategoryName = [[UILabel alloc] initWithFrame:CGRectMake(50, CATEGORY_HEADER_SECTION_HEIGHT / 2 - 10, tableView.frame.size.width - 20, 20)];
    [lbCategoryName setFont:[UIFont fontWithName:@"iCielSamsungSharpSans-Medium" size:22.0]];
    [lbCategoryName setTextColor:[UIColor whiteColor]];
    [lbCategoryName setBackgroundColor:[UIColor clearColor]];
    [headerSection addSubview:lbCategoryName];
    [headerSection addSubview:imageView];
    
    switch (section) {
        case 1:
            lbCategoryName.text = @"People";
            imageView.image = [UIImage imageNamed:@"ic_people"];
            break;
        case 2:
            lbCategoryName.text = @"Reports";
            imageView.image = [UIImage imageNamed:@"icon-report"];
            break;
        case 3:
            lbCategoryName.text = @"POS Setup";
            imageView.image = [UIImage imageNamed:@"icon-setup"];
            break;
        case 4:
            lbCategoryName.text = @"Statictics";
            imageView.image = [UIImage imageNamed:@"icon-report"];
            break;
        case 5:
            lbCategoryName.text = @"Stores";
            imageView.image = [UIImage imageNamed:@"icon-store-menu"];
            break;
            
            
        default:
            break;
    }
    
    double width = self.view.bounds.size.width;
    
    if (width == [UIScreen mainScreen].bounds.size.width ) {
        width = width - 50;
    }
    
    UIImageView* imgArrow = [[UIImageView alloc] initWithFrame:CGRectMake(width - 25, CATEGORY_HEADER_SECTION_HEIGHT / 2 - 7.5, 15, 15)];
    [imgArrow setContentMode:UIViewContentModeScaleAspectFit];

    NSString* myImage = @"icon-down";
    NSString* iconImageName = [NSString stringWithFormat:@"%@", myImage];
    if ((section == 1 && self.isOpenSection1) || (section == 3 && self.isOpenSection3)){
        iconImageName = @"icon-next";
    }
    
    if (iconImageName && [iconImageName length]){
        imgArrow.image = [UIImage imageNamed:iconImageName ];
    }
    if (section != 2 && section != 4) {
        [headerSection addSubview:imgArrow];
    }
    
    return headerSection;
}

- (void)sectionTapped:(UIButton*)btn {
    NSInteger sectionIndexSelected = btn.tag;
    if (sectionIndexSelected == 0) {
        [self gotoUserProfile];
    } else if (sectionIndexSelected == 2) {
        [self gotoReport];
    } else if (sectionIndexSelected == 4) {
        [self gotoGlobalStatistics];
    }
    
    if (sectionIndexSelected == 1) {
        self.isOpenSection1 = !self.isOpenSection1;
    }
    
    if (sectionIndexSelected == 3) {
        self.isOpenSection3 = !self.isOpenSection3;
    }
    
    if (sectionIndexSelected == 5) {
        self.isOpenSection5 = !self.isOpenSection5;
    }
    
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            if (self.isOpenSection1) {
                return self.itemList1.count;
            } else {
                return 0;
            }
            break;
        case 2:
            return self.itemList2.count;
            break;
        case 3:
            if (self.isOpenSection3) {
                return self.itemList3.count;
            } else {
                return 0;
            }
        case 4:
            return self.itemList4.count;
            break;
        case 5:
            if (self.isOpenSection5) {
                return self.itemList5.count;
            } else {
                return 0;
            }
            
            break;
        default:
            break;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 100;
    }
    
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
        if (indexPath.section == 0) {
            MenuUserInfoCell* cell = [tableView dequeueReusableCellWithIdentifier:@"MenuUserInfoCell"];
            if (!cell) {
                cell = [[MenuUserInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MenuUserInfoCell"];
            }
            [cell loadUserInfo];
            return cell;
        } else {
            MenuItemCell* cell = [tableView dequeueReusableCellWithIdentifier:@"MenuItemCell"];
            if (!cell) {
                cell = [[MenuItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MenuItemCell"];
            }
        
            [cell loadItemData:[[self.menuList objectAtIndex:(indexPath.section - 1)] objectAtIndex:indexPath.row]];
            return cell;
        }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*
    if (tableView == self.providerTableView) {
        //[self.frostedViewController hideMenuViewController];
        
    } else {
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
        //[self.frostedViewController hideMenuViewController];
        if (indexPath.row == 0) {
            [self gotoUserProfile];
        }else{
            MenuItemCell* cell = (MenuItemCell*)[tableView cellForRowAtIndexPath:indexPath];
            [self performSelector:cell.selector withObject:nil];
        }
    }
     */
    

    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    //[self.frostedViewController hideMenuViewController];
    if (indexPath.section == 0) {
        [self gotoUserProfile];
    }else{
        MenuItemCell* cell = (MenuItemCell*)[tableView cellForRowAtIndexPath:indexPath];
        [self performSelector:cell.selector withObject:nil];
    }
}

#pragma mark - MoreViewDelegate methods

- (void)gotoStoreOwners {
    StoreOwnerListViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"StoreOwnerListViewController"];
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

- (void)gotoStores {
    StoreListViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"StoreListViewController"];
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

- (void)gotoCustomerList {
    CustomerListViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"CustomerListViewController"];
   
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    
}

- (void) gotoGlobalStatistics {
    HomeViewController* controller = [[HomeViewController alloc] init];
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    
}

- (void)gotoDeviceUser {
    DeviceUserViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"DeviceUserViewController"];
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

- (void)gotoProvider {
    ProviderViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"ProviderViewController"];
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

- (void)gotoReport {
    ReportViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"ReportViewController"];
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

- (void)gotoSetting {
    /*
    StoreSettingViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"StoreSettingViewController"];
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    */
    
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select an action" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Store Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        StoreSettingViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"StoreSettingViewController"];
        [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Bussiness Hours" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        BussinessHoursViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"BussinessHoursViewController"];
        [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
        
    }]];
    
    
    if (actionSheet.popoverPresentationController != nil) {
        actionSheet.popoverPresentationController.sourceView = self.view;
        actionSheet.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    }
     
    
    //[bconnectAppDelegate.rootNavigation presentViewController:actionSheet animated:YES completion:nil];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)gotoCategory {
    CategoryViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"CategoryViewController"];
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

- (void)gotoUserProfile {
    AccountDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"AccountDetailViewController"];
    StoreOwnerEntity* entity = [[StoreOwnerEntity alloc] init];
    entity.firstName = [SessionManager sharedClient].userEntity.firstName;
    entity.lastName = [SessionManager sharedClient].userEntity.lastName;
    entity.email = [SessionManager sharedClient].userEntity.email;
    entity.phone = [SessionManager sharedClient].userEntity.phone;
    entity.Id = (int)[[SessionManager sharedClient].userEntity.userId integerValue];
    
    controller.entity = entity;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
    
}

- (void)gotoUserInfo{
    RegisterUserInfoViewController* controller = [registerStoryboard instantiateViewControllerWithIdentifier:@"RegisterUserInfoViewController"];
    controller.isEditUserInfo = YES;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}


- (IBAction)btnLogoutPressed:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Do you really want to logout ?" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        [self logOut];
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    if (actionSheet.popoverPresentationController != nil) {
        actionSheet.popoverPresentationController.sourceView = self.view;
        actionSheet.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    }
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
    
}

-(void)logOut {
    NSString* deviceId = [Helpers getUDID];
    
    [[RequestManager sharedClient] requestLogout:deviceId success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestLogout success" message:results];
        //[self dismissProgressHub];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestLogout failure" message:results];
        //[self dismissProgressHub];
    }];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOGOUT object:nil];
    
    [[SessionManager sharedClient] logOut];
    
    [bconnectAppDelegate.rootNavigation popToRootViewControllerAnimated:NO];
    
    LoginViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)prepareForPopoverPresentation:(UIPopoverPresentationController *)popoverPresentationController {
    
}

@end

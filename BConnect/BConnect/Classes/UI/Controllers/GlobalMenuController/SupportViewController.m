//
//  SupportViewController.m
//  luvkonect
//
//  Created by Dragon on 6/8/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "SupportViewController.h"
#import "SessionManager.h"

#define kSubmitSuccessTag   1024
#define kSubmitFailTag   4201
#define kInforHeightDefault 150
#define CHARACTER_LIMITED   500

@interface SupportViewController ()

@end

@implementation SupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    [self prepareLayout];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (![CoreStringUtils isEmpty:[SessionManager sharedClient].sessionKey]) {
        [self.supportView.tvFeedback becomeFirstResponder];
    }else{
        [self.supportView.txtName becomeFirstResponder];
    }
}

- (void)prepareLayout{
    self.scrollWidthConstraint.constant = CGRectGetWidth([UIScreen mainScreen].bounds);
    [self.scrollView layoutIfNeeded];
    if (!self.supportView) {
        CGFloat supportViewHeight = (![CoreStringUtils isEmpty:[SessionManager sharedClient].sessionKey]) ? (465 - kInforHeightDefault) : 465;
        self.supportView = [[SupportView alloc] initWithFrame:CGRectMake(0, 5, self.scrollWidthConstraint.constant, supportViewHeight)];
        [self.supportView layoutIfNeeded];
        [self.supportView prepareLayout];
        self.supportView.delegate = self;
        
        CountryEntityV2 *entity = [[CountryEntityV2 alloc] init];
        entity.isoCode = COUNTRY_CODE_DEFAULT;
        entity.name = COUNTRY_NAME_DEFAULT;
        entity.phoneCode = COUNTRY_PHONE_DEFAULT;

        self.supportView.countryEnitySelected = entity;
        self.supportView.txtCountry.text = [NSString stringWithFormat:@"%@", self.supportView.countryEnitySelected.name];

        [self.scrollView addSubview:self.supportView];
        [self.scrollView setContentSize:self.supportView.frame.size];
    }
}

- (void)setupHeader{
    [super setupHeader];
//    [self SetHeaderTitle:[CoreStringUtils getStringRes:@"str_support"]];
    [self SetBackTitle:[CoreStringUtils getStringRes:@"str_support"] target:self action:@selector(btnBackPressed:)];
}

- (void)showCountryList {
    CountriesViewControllerV2* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"CountriesViewControllerV2"];
    controller.delegate = self;
    [self pushToViewController:controller];
}

- (void)countrySelected:(CountryEntityV2*)entity{
    [self.supportView setCountrySelected:entity];
}

#pragma mark - AlertView methods
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if ([AlertviewManager sharedClient].alertTag == kSubmitSuccessTag) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)requestContact{
    NSString* name = [self.supportView.txtName.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* email = [self.supportView.txtEmail.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phoneNumber = [self.supportView.txtPhone.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* country = self.supportView.countryEnitySelected.isoCode;
    NSString* message = [self.supportView.tvFeedback.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    self.params = [NSMutableDictionary dictionary];
    [self.params setValue:email forKey:REQUEST_EMAIL];
    [self.params setValue:name forKey:REQUEST_NAME];
    [self.params setValue:phoneNumber forKey:REQUEST_PHONE_NUMBER];
    [self.params setValue:country forKey:REQUEST_COUNTRY_CODE];
    [self.params setValue:message forKey:REQUEST_MESSAGE];
    [self.params setValue:[SessionManager sharedClient].sessionKey forKey:REQUEST_SESSION_KEY];

    [[RequestManager sharedClient] requestContactParams:self.params success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestContactParams success" message:results];
        [AlertviewManager sharedClient].alertTag = kSubmitSuccessTag;
        [[AlertviewManager sharedClient] showTitleMessage:@"" message:[CoreStringUtils getStringRes:@"str_thanks_for_your_feedback"] delegate:self cancelButton:[CoreStringUtils getStringRes:@"str_ok"] otherButton:nil];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestContactParams failure" message:results];
        [AlertviewManager sharedClient].alertTag = kSubmitFailTag;
        [[AlertviewManager sharedClient] showTitleMessage:@"" message:[CoreStringUtils getStringRes:@"str_error_submit"] delegate:self cancelButton:[CoreStringUtils getStringRes:@"str_ok"] otherButton:nil];
    }];
}

- (void)submitFeedback{
    NSString* feedBack = [self.supportView.tvFeedback.text stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [[RequestManager sharedClient] requestSendFeedback:[SessionManager sharedClient].sessionKey message:feedBack success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestSendFeedback success" message:results];
        [AlertviewManager sharedClient].alertTag = kSubmitSuccessTag;
        [[AlertviewManager sharedClient] showTitleMessage:@"" message:[CoreStringUtils getStringRes:@"str_thanks_for_your_feedback"] delegate:self cancelButton:[CoreStringUtils getStringRes:@"str_ok"] otherButton:nil];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestSendFeedback failure" message:results];
        [AlertviewManager sharedClient].alertTag = kSubmitFailTag;
        [[AlertviewManager sharedClient] showTitleMessage:@"" message:[CoreStringUtils getStringRes:@"str_error_submit"] delegate:self cancelButton:[CoreStringUtils getStringRes:@"str_ok"] otherButton:nil];
    }];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end

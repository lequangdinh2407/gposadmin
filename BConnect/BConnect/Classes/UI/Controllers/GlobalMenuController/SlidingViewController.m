//
//  SlidingViewController.m
//  luvkonect
//
//  Created by Dragon on 6/30/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "SlidingViewController.h"
//#import "MainMapViewController.h"
@interface SlidingViewController ()

@end

@implementation SlidingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.contentViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    self.menuViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"GlobalMenuViewController"];
}

@end

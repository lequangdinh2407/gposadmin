//
//  HelpViewController.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/29/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
}

- (void)setupHeader{
    [super setupHeader];
    [self SetBackTitle:@"FAQs" target:self action:@selector(btnBackPressed:)];
}

- (void)btnBackPressed:(id)sender{
    [super btnBackPressed:sender];
}

@end

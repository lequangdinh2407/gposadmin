//
//  HomeViewController.m
//  Giftalk
//
//  Created by Dragon on 11/9/15.
//  Copyright © 2015 DKMobility. All rights reserved.
//

#import "MainMapViewController+UpdateUI.h"
#import "AreaEntity.h"
#import "LocationManager.h"
#import "PointEntity.h"
#import "AddressHomberViewController.h"
#import "PaymentViewController.h"
#import "CardEntity.h"
#import "SessionManager.h"
#import "HomberManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "FastttCapturedImage.h"
#import "ResizeImageUtils.h"
#import "UploadPhotoEntity.h"
#import "UIAlertView+Blocks.h"
#import "FeedViewController.h"
#import "FreeHomberViewController.h"
#import "UploadPhotoGardenViewController.h"
#import "CustomServiceEntity.h"
#import "HomberServiceEntity.h"
#import "EstimateViewController.h"
#import "UserProfileViewController.h"

@implementation MainMapViewController (ActionUI)

- (IBAction)btnSearchPressed:(id)sender {
    AddressHomberViewController* controller = [homberStoryboard instantiateViewControllerWithIdentifier:@"AddressHomberViewController"];
    controller.gardenList = recentGardensList;
    controller.delegate = self;
    controller.home = self.home;
    if (!self.home) {
        [[RequestManager sharedClient] requestGetHome:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestGetHome success" message:results];
            controller.home = [results objectForKey:RESPONSE_DATA];
            controller.isHaveCurrentAddress = YES;
            [self pushToViewController:controller];
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestGetHome failure" message:results];
            [self pushToViewController:controller];
        }];
    } else {
        controller.isHaveCurrentAddress = YES;
        [self pushToViewController:controller];
    }
}

- (IBAction)btnPaymentPressed:(id)sender {
    PaymentViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"PaymentViewController"];
    controller.isPickCard = YES;
    controller.isPayment = YES;
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

- (IBAction)btnRequestPressed:(id)sender {
    [self clearUndoList];
    uploadedPhotos = nil;
    self.orderEntity = nil;
    [UIView animateWithDuration:0.5 animations:^{
        self.requestViewBottomConstraint.constant = -REQUEST_VIEW_HEIGHT;
        CGRect frame = _mapView.frame;
        frame.size.height = [UIScreen mainScreen].bounds.size.height;
        _mapView.frame = frame;
        
        CGRect btnLocalizeFrame = _localizeBtn.frame;
        btnLocalizeFrame.origin.y = [UIScreen mainScreen].bounds.size.height - 160;
        _localizeBtn.frame = btnLocalizeFrame;
        
    } completion:^(BOOL finished) {
        [self showUploadPopup];
    }];
}

- (IBAction)btnConfirmRequestPressed:(id)sender {
    [self addProviderInfoView];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.75 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self requestBookGarden];
    });
}

- (void)btnMenuPressed:(UIButton *)sender {
    if (!_isCanShowLeftMenu)
        return;
    
    @synchronized(self) {
        [self.view endEditing:YES];
        [self.frostedViewController.view endEditing:YES];
        [self.frostedViewController presentMenuViewController];
    }
}

-(void)btnFeedPressed:(UIButton *)sender {
    @synchronized(self) {
        FeedViewController *feedViewCon = [feedStoryboard instantiateViewControllerWithIdentifier:@"FeedViewController"];
        feedViewCon.tabTitle = @"Social";
        [self pushToViewController:feedViewCon];
    }
}

-(void)btnSharePressed:(UIButton *)sender {
    FreeHomberViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"FreeHomberViewController"];
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

-(void)btnHomberPressed:(UIButton *)sender {
    [self requestGetGardenDetail:self.home.gardenId];
}

-(void)btnRecentlyPressed:(UIButton *)sender {
    [self requestGetGardenDetail: ((GardenEntity*)[recentGardensList objectAtIndex:0]).gardenId];
}

- (IBAction)btnUndoPressed:(id)sender {
    [self undo];
}

- (IBAction)btnAddPressed:(id)sender {
    isMinusMode = NO;
    
    [self.btnDrawText setTitle:@"Unlock" forState:UIControlStateNormal];
    [self.btnDrawImage setImage:[UIImage imageNamed:@"ic_lock_map"] forState:UIControlStateNormal];
    _localizeBtn.alpha = 0;
    [_mapView.settings setAllGesturesEnabled:NO];
    [_mapView.settings setConsumesGesturesInView:NO];
    
    if (((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path.count > 0) {
        CLLocationCoordinate2D firstLoc = [((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path coordinateAtIndex:0];
        [((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path addCoordinate:firstLoc];
        ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polyline.path = ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path;
        ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).dashedPolyline.map = nil;
        
        currentArea++;
        [areasList addObject:[[AreaEntity alloc] init]];
        ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polyline.map = _mapView;
        
         _localizeBtn.alpha = 1;
        self.btnUndoText.enabled = NO;
        self.btnUndoText.alpha = 0.333;
        self.btnUndoImage.enabled = NO;
        
        if (areasList.count > 0 && ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path.count == 0) {
            ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polyline.map = _mapView;
        }
    } else {
        if (((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).isMinusArea){
            [((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]) switchToAddArea];
        }
    }
    
    [_undoList removeAllObjects];
    [self updateButtonInDrawingMode];
}

- (IBAction)btnMinusPressed:(id)sender {
    if (areasList.count > 0 && ((AreaEntity*)[areasList objectAtIndex:0]).path.count > 0) {
        [self btnAddPressed:nil];
        [self enableAllButtonInDrawView];
        [((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]) switchToMinusArea];
        minusParentArea = nil;
        isMinusMode = YES;
    }
}

- (IBAction)btnClearPressed:(id)sender {
    @synchronized(self) {
        
        isMinusMode = NO;
        
        if (areasList.count == 0 || (areasList.count == 1 && ((AreaEntity*)[areasList objectAtIndex:0]).path.count == 0 && self.btnUndoImage.isEnabled == NO)) return;
        
        if (self.clearAreasPopupView) {
            [self.clearAreasPopupView removeFromSuperview];
            self.clearAreasPopupView = nil;
        }
        
        int count;
        if (areasList.count > 0 && ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path.count > 0)
            count = (int)areasList.count;
        else
            count = (int)areasList.count - 1;
        
        if (count <= 0) {
            [self removeAll];
            //_currentAddressMarker.map = _mapView;
        } else {
            self.clearAreasPopupView =  [[ClearAreasPopupView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width * 4.4/5 , MIN(((count) * 40 + 60), [UIScreen mainScreen].bounds.size.height * 3/4))];
            
            self.clearAreasPopupView.areasList = areasList;
            self.clearAreasPopupView.delegate = self;
            [self setupPopper2WithSubView:self.clearAreasPopupView];
        }
    }
}

- (IBAction)btnNextPressed:(id)sender {
    uploadedPhotos = nil;
    self.lbSquareAreaRequest.text = [NSString stringWithFormat:@"%@ sqft", (int)[Helpers addCommasToInt:lround(squareArea)]];
    
    CardEntity* card = [SessionManager sharedClient].card;
    
    if (card) {
        self.lbPaymentMethod.text = card.number;
    } else {
        self.lbPaymentMethod.text = @"Add payment";
    }
    
    HomberPromotionEntity* promotion = [SessionManager sharedClient].promotion;    
    if (promotion) {
        [self.btnPromotion setTitle:promotion.code forState:UIControlStateNormal];
    } else {
        [self.btnPromotion setTitle:@"Have a promotion code ?" forState:UIControlStateNormal];
    }
    
    [self.btnRequest setAlpha:0.33333333];
    [self.btnRequest setEnabled:NO];
    
    if (areasList.count > 0 && ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path.count > 0) {
        CLLocationCoordinate2D firstLoc = [((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path coordinateAtIndex:0];
        [((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path addCoordinate:firstLoc];
        ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polyline.path = ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path;
        ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).dashedPolyline.map = nil;
    }
    
    _isRequestServiceListDone = NO;
    
    [[RequestManager sharedClient] requestAddMap:currentGarden.gardenId square:squareArea tiles:areasList success:^(NSMutableDictionary *results, int errorCode) {
        [((BaseViewViewController*)rootView) dismissProgressHub];
        [Log d:0 tag:@"requestAddMap success" message:results];
        
        [[RequestManager sharedClient] requestGetService:squareArea gardenId:currentGarden.gardenId success:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestGetListService success" message:results];
            
            _homberServiceCategories = [results objectForKey:RESPONSE_DATA];
            _isRequestServiceListDone = YES;
            
            [self reloadData];
            [self.buttonBarView reloadData];
            [self.buttonBarView moveToIndex:0 animated:YES swipeDirection:XLPagerTabStripDirectionNone pagerScroll:XLPagerScrollYES];
            [self.buttonBarView
             selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
             animated:YES
             scrollPosition:UICollectionViewScrollPositionCenteredVertically];
            
            [self setCurrentView:RequestView];
            
            [UIView animateWithDuration:0.5 animations:^{
                self.requestViewBottomConstraint.constant = 0;
                
                CGRect btnLocalizeFrame = _localizeBtn.frame;
                btnLocalizeFrame.origin.y = [UIScreen mainScreen].bounds.size.height - REQUEST_VIEW_HEIGHT - 70;
                _localizeBtn.frame = btnLocalizeFrame;
                
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                _currentAddressMarker.title = self.address.firstLine;
                [_mapView setSelectedMarker:_currentAddressMarker];
            }];
            
        } failure:^(NSMutableDictionary *results, int errorCode) {
            [Log d:0 tag:@"requestGetListService failure" message:results];
            [SVProgressHUD dismiss];
        }];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [((BaseViewViewController*)rootView) dismissProgressHub];
        [Log d:0 tag:@"requestAddMap failure" message:results];
    }];
}

- (void)btnRotatePressed:(id)sender {
    mapRotationAngle += ROTATE_ANGLE_DEVIATION;
    [_mapView animateToBearing:mapRotationAngle];
}

- (void)btnBackPressed:(id)sender {
    if (currentView == DrawView) {
        confirmBackAlert = [UIAlertController alertControllerWithTitle:[CoreStringUtils getStringRes:@"str_alert_confirm_back"]
                                                                 message:nil
                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * action) {
                                                             }];
        
        UIAlertAction* OKAction = [UIAlertAction actionWithTitle:@"DISCARD" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [[HomberManager sharedClient] cancel];
                                                             [self backToHome];
                                                         }];
        
        [confirmBackAlert addAction:cancelAction];
        [confirmBackAlert addAction:OKAction];
        [self presentViewController:confirmBackAlert animated:YES completion:nil];

    } else if (currentView == RequestView) {
        [self setCurrentView:DrawView];
        [self enableAllButtonInDrawView];
        
        [self.btnPromotion setTitle:@"" forState:UIControlStateNormal];
        self.lbPaymentMethod.text = @"";
        _backBtn.userInteractionEnabled = NO;
        [UIView animateWithDuration:0.5 animations:^{
            self.requestViewBottomConstraint.constant = - REQUEST_VIEW_HEIGHT;
            CGRect frame = _mapView.frame;
            frame.size.height = [UIScreen mainScreen].bounds.size.height;
            _mapView.frame = frame;
            
            CGRect btnLocalizeFrame = _localizeBtn.frame;
            btnLocalizeFrame.origin.y = [UIScreen mainScreen].bounds.size.height - 160;
            _localizeBtn.frame = btnLocalizeFrame;
            
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            _backBtn.userInteractionEnabled = YES;
            [_mapView setSelectedMarker:nil];
        }];
        
    } else if (currentView == ConfirmRequestView) {
        _backBtn.userInteractionEnabled = NO;
        [UIView animateWithDuration:0.5 animations:^{
            self.confirmRequestViewBottomConstraint.constant = - CONFIRM_REQUEST_VIEW_HEIGHT;
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            _backBtn.userInteractionEnabled = YES;
            self.requestViewBottomConstraint.constant = 0;
            [self setCurrentView:RequestView];
        }];
    }
}

- (void)btnLocalizePressed:(UIButton *)sender {
    if (areasList.count > 0 && ((AreaEntity*)[areasList objectAtIndex:0]).path.count > 0) {
        [self focusMapToShowAllAreas:_mapView andAreasList:areasList];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [_mapView animateWithCameraUpdate:[GMSCameraUpdate setCamera:[GMSCameraPosition cameraWithLatitude:_mapView.camera.target.latitude longitude:_mapView.camera.target.longitude zoom:ZOOM_LEVEL_DEFAULT + 2]]];
        });
    } else {
        [_mapView animateWithCameraUpdate:[GMSCameraUpdate setCamera:[GMSCameraPosition cameraWithLatitude:self.address.lattitude longitude:self.address.longtitude zoom:ZOOM_LEVEL_DEFAULT]]];
    }
    
    if (currentView != DrawView) {
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            _localizeBtn.alpha = 0;
            
        });
    }
}

- (void)btnAddPointPressed:(id)sender {
    isInDrawingMode = NO;
    [_mapView.settings setConsumesGesturesInView:NO];
    [self enableAllButtonInDrawView];
}

- (IBAction)btnDrawPressed:(id)sender {
    isInDrawingMode = YES;
    
    if ([self.btnDrawText.titleLabel.text isEqualToString:@"Draw"]) {
        [self.btnDrawText setTitle:@"Unlock" forState:UIControlStateNormal];
        [self.btnDrawImage setImage:[UIImage imageNamed:@"ic_lock_map"] forState:UIControlStateNormal];
        _localizeBtn.alpha = 0;
        
        [_mapView.settings setAllGesturesEnabled:NO];
        [_mapView.settings setConsumesGesturesInView:NO];
        
        if (areasList.count > 0 && ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path.count == 0) {
            GMSMutablePath *poly = [GMSMutablePath path];
            ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polyline.path = poly; // = [GMSPolygon polygonWithPath:poly];
            ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polyline.map = _mapView;
        }
        
        if (_timer) {
            [_timer invalidate];
            _timer = nil;
        }
        
        _timer = [NSTimer scheduledTimerWithTimeInterval:TIMER_COUNT
                                                  target:self
                                                selector:@selector(timerTick:)
                                                userInfo:nil
                                                 repeats:YES];
        
        
    } else {
        [self setDrawButton];
    }
}

- (IBAction)btnAddPromotionPressed:(id)sender {
    PaymentViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"PaymentViewController"];
    controller.isPromotion = YES;
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

- (IBAction)btnCancelOptimizePressed:(id)sender {
    [self btnCancelPressed];
}

#pragma mark - Business methods
- (void)setDrawButton{

    [self.btnDrawText setTitle:@"Draw" forState:UIControlStateNormal];
    [self.btnDrawImage setImage:[UIImage imageNamed:@"ic_draw"] forState:UIControlStateNormal];
    _localizeBtn.alpha = 1;
    
    [_mapView.settings setAllGesturesEnabled:YES];
    [_mapView.settings setConsumesGesturesInView:YES];
    if (areasList.count > 1) {
        [self enableAllButtonInDrawView];
    }
}

- (void)timerTick:(NSTimer *)timer {
    if (isPanning) {
        NSLog(@"timerTick isPanning");
        NSInteger index = ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path.count - 1;
        if (index > 0) {
            [_undoList addObject:@(index)];
            self.btnUndoText.enabled = YES;
            self.btnUndoText.alpha = 1;
            self.btnUndoImage.enabled = YES;
        }
    } else {
        NSLog(@"timerTick not Panning");
    }
    isPanning = NO;
}

- (void)undo{
    if (_undoList.count > 0) {
        
        NSInteger lastIndex = [[_undoList lastObject] integerValue];
        
        for (NSInteger i = lastIndex; i < ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path.count; i++) {
            [((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path removeLastCoordinate];
            i--;
        }
        
        ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polygone.path = ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polyline.path = ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path;
        
        if (((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path.count <= 2 ) {
            ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polygone.map = nil;
            if (((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path.count == 1) {
                [self renewUndoList];
                return;
            }
        }
        
        ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).dashedPolyline.map = nil;
        [self drawDashedLine];
        
        [_undoList removeLastObject];
        
        if (((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path.count == 0) {
            [_undoList removeAllObjects];
        }
        
        [self calculateTheArea];
        
    } else {
        [self renewUndoList];
    }
}

- (void)renewUndoList {
    [((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).path removeAllCoordinates];
    ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polyline.path = [[GMSPath alloc] init];
    ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polygone.path = [[GMSPath alloc] init];
    ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polyline.map = nil;
    ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).polygone.map = nil;
    ((AreaEntity*)[areasList objectAtIndex:areasList.count - 1]).dashedPolyline.map = nil;
    self.btnUndoImage.enabled = NO;
    self.btnUndoText.enabled = NO;
    self.btnUndoText.alpha = 0.333;
    
    double area = 0;
    for (int i = 0; i < areasList.count - 1; i++) {
        area += ((AreaEntity*)[areasList objectAtIndex:i]).area;
    }
    
    squareArea = area;
    
    [self updateSendBtn:squareArea];
    
    [self.btnSquare setTitle:[NSString stringWithFormat:@"%@ sqft", [Helpers addCommasToInt:(int)area]] forState:UIControlStateNormal];
    [self requestGetDefaultService:squareArea gardenId:currentGarden.gardenId];

}

- (void)clearUndoList {
    [_undoList removeAllObjects];
    [_timer invalidate];
    _timer = nil;
    self.btnUndoImage.enabled = NO;
    self.btnUndoText.enabled = NO;
    self.btnUndoText.alpha = 0.333;
}

- (void)showUploadPopup {
    uploadedPhotos = @[];
    
    UIAlertController* uploadAlert = [UIAlertController alertControllerWithTitle:[CoreStringUtils getStringRes:@"str_alert_upload_photo"]
                                                                                message:nil
                                                                         preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Skip" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             [self showIndicatior];
                                                             [self checkBook];
                                                             _backBtn.userInteractionEnabled = NO;
                                                         }];
    
    UIAlertAction* OKAction = [UIAlertAction actionWithTitle:@"Upload" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         UploadPhotoGardenViewController* vc = [homberStoryboard instantiateViewControllerWithIdentifier:@"UploadPhotoGardenViewController"];
                                                         vc.resultDelegate = self;
                                                         [self pushToViewController:vc];
                                                     }];
    [uploadAlert addAction:cancelAction];
    [uploadAlert addAction:OKAction];
    [self presentViewController:uploadAlert animated:YES completion:nil];
}

#pragma mark - ProviderInfoViewDelegate

-(void)gotoProviderProfile {
    UserProfileViewController *profileCV = [[UserProfileViewController alloc] init];
    profileCV.isProvider = YES; 
    [profileCV setOtherUserInfo:tmpProvider.userId otherUserName:tmpProvider.displayName otherUserAvatar:tmpProvider.avatar isFollow:NO];
    [self.navigationController pushViewController:profileCV animated:YES];
}

- (void)btnCloseProviderInfoPressed {
    [UIView animateWithDuration:0.3 animations:^{
        self.providerContainerTopConstraint.constant = [UIScreen mainScreen].bounds.size.height - PROVIDER_INFO_HEADER_HEIGHT;
        [self.view layoutIfNeeded];
        isLayoutIfNeeded = YES;
        self.providerView.closeView.alpha = 0;
        self.providerViewContainer.scrollEnabled = NO;
    } completion:^(BOOL finished) {
        isLayoutIfNeeded = NO;
        self.providerViewContainer.scrollEnabled = YES;
    }];
}

- (void)btnCancelPressed {
    NSString* message;
    if (currentStatus == PROVIDER_WORKING) {
        message = [NSString stringWithFormat:@"Provider is currently working in your yard. Total charge to your card: $%@. Do you want to cancel this workorder?", [Helpers addCommasToDouble:[self.orderEntity.cancelPrice doubleValue]]];
    } else {
        message = [CoreStringUtils getStringRes:@"str_alert_confirm_cancel"];
    }
    
    confirmCancelAlert = [UIAlertController alertControllerWithTitle:message
                                        message:nil
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction * action) {
                                                          }];
    
    UIAlertAction* OKAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [self showIndicatior];
                                                              [[HomberManager sharedClient] cancel:^(NSMutableDictionary *results, int errorCode) {
                                                                [self stopIndicator];
                                                                [self backToHome];
                                                                  
                                                              } failure:^(NSMutableDictionary *results, int errorCode) {
                                                                  [Helpers showToast:@"Cancel services fail"];
                                                                  [self stopIndicator];
                                                              }];
                                                          }];
    
    [confirmCancelAlert addAction:cancelAction];
    [confirmCancelAlert addAction:OKAction];
    [self presentViewController:confirmCancelAlert animated:YES completion:nil];
}

- (void)btnContactPressed {
    NSString* number  = [self.provider.phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString* tmpPhone = [NSString  stringWithFormat:@"tel://%@", number];
    NSURL *phoneUrl = [NSURL URLWithString:tmpPhone];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else{
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:[CoreStringUtils getStringRes:@"str_alert"] message:[CoreStringUtils getStringRes:@"str_error_phone_format_invalid"] delegate:nil cancelButtonTitle:[CoreStringUtils getStringRes:@"str_ok"] otherButtonTitles:nil, nil];
        [calert show];
    }
}

- (void)showImage:(int)index {
    @synchronized(self) {
        if (!self.coverList) {
            self.coverList = [[NSMutableArray alloc] init];
            [(BaseViewViewController*)rootView showProgressHub];
            for (int i = 0; i< self.orderEntity.clientUploadedPhotos.count; i++) {
                
                NYTExamplePhoto *photo = [[NYTExamplePhoto alloc] init];
                
                NSString *url = [self.orderEntity.clientUploadedPhotos objectAtIndex:i];
                UIImage *image = [Helpers getFullImageCache:url];
                
                //UIImage *image = [UIImage imageNamed:[self.imageList objectAtIndex:i]];
                
                if (image == nil)
                    image = [Helpers getImageCache:url size:240];
                photo.image = image;
                
                [self.coverList addObject:photo];
            }
            [(BaseViewViewController*)rootView dismissProgressHub];
        }
        
        NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc] initWithPhotos:self.coverList initialPhoto:[self.coverList objectAtIndex:index]];
        photosViewController.delegate = self;
        [self presentViewController:photosViewController animated:YES completion:nil];
        [photosViewController navigateToPhoto:[self.coverList objectAtIndex:index]];
    }
}

- (void)photosViewController:(NYTPhotosViewController *)photosViewController didNavigateToPhoto:(id <NYTPhoto>)photo atIndex:(NSUInteger)photoIndex {
    NSLog(@"Did Navigate To Photo: %@ identifier: %lu", photo, (unsigned long)photoIndex);
    if ([self.orderEntity.clientUploadedPhotos count] <= photoIndex)
        return;
    
    NSString *url = [self.orderEntity.clientUploadedPhotos objectAtIndex:photoIndex];
    if ([Helpers getFullImageCache:url])
        return;
    
    NYTExamplePhoto *photoLarge = (NYTExamplePhoto*)photo;
    
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:url done:^(UIImage *image, SDImageCacheType cacheType) {
        if (image) {
            photoLarge.image = image;
            [photosViewController updateImageForPhoto:photoLarge];
        } else {
            //            UIImage* thumbImage = [Helpers getImageCache:url size:240];
            //            if (thumbImage) {
            //                photoLarge.image = thumbImage;
            //                [photosViewController updateImageForPhoto:photoLarge];
            //            }
            
            [SDWebImageDownloader.sharedDownloader downloadImageWithURL:[NSURL URLWithString:url] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize){
                // progression tracking code
            } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished){
                if (image && finished) {
                    dispatch_async( dispatch_get_main_queue(), ^{
                        photoLarge.image = image;
                        [photosViewController updateImageForPhoto:photoLarge];
                    });
                    [[SDImageCache sharedImageCache] storeImage:image forKey:url toDisk:YES];
                }
            }];
        }
    }];
}

#pragma - mark ServiceItemDelegate
- (void)chooseService:(int)index andServicesList:(NSMutableArray*)servicesList {
    [self.selectedCaterogyList replaceObjectAtIndex:index withObject:servicesList];
    numberOfSelectedServices = 0;
    for (NSMutableArray* array in self.selectedCaterogyList) {
        numberOfSelectedServices += array.count;
    }
    
    CardEntity* card = [SessionManager sharedClient].card;
    if (numberOfSelectedServices > 0 && card) {
        [self.btnRequest setAlpha:1.0];
        [self.btnRequest setEnabled:YES];
    } else {
        [self.btnRequest setAlpha:0.33333333];
        [self.btnRequest setEnabled:NO];
    }
}

- (void)showServiceInfo:(int)caterogyIndex andServiceIndex:(int)serviceIndex {
    @synchronized(self) {
        
        NSString* name;
        NSString* desc;
        HomberServiceCategoryEntity* caterory = [_homberServiceCategories objectAtIndex:caterogyIndex];
        HomberServiceEntity* commonService = [caterory.services objectAtIndex:serviceIndex];
        
        desc = commonService.desc;
        name = commonService.name;
        
        if (self.descPopupView) {
            [self.descPopupView removeFromSuperview];
            self.descPopupView = nil;
        }
        
        CGFloat height = [Helpers calculateHeightText:desc width:self.view.frame.size.width * 2/3 font:[UIFont fontWithName:@"Roboto-Regular" size:15.f]].height;
        height = MIN(height, self.view.frame.size.height * 2/3);
        self.descPopupView = [[ProductDescriptionPopupView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width * 2/3 + 20, height + 60)];
        self.descPopupView.tvDescription.text = desc;
        self.descPopupView.lbTitle.text = name;
        [self setupPopper2WithSubView:self.descPopupView];
    }
}

#pragma mark - Popper

- (void)setupPopper2WithSubView:(UIView*)subView{
    popper2 = [[Popup alloc] initWithViewWidth:CGRectGetWidth(subView.frame) viewHeight:CGRectGetHeight(subView.frame)];
    [popper2 setDelegate:self];
    [popper2 setBackgroundBlurType:PopupBackGroundBlurTypeDark];
    [popper2 setIncomingTransition:PopupIncomingTransitionTypeEaseFromCenter];
    [popper2 setOutgoingTransition:PopupOutgoingTransitionTypeDisappearCenter];
    [popper2 setRoundedCorners:YES];
    [popper2 setTapBackgroundToDismiss:YES];
    [[popper2 getPopupView] addSubview:subView];
    [popper2 showPopup];
}

#pragma mark - ProviderInfoViewDelegate
- (void)showServiceInfo:(int)index {
    @synchronized(self) {
        
        NSString* name;
        NSString* desc;
        if (index < self.orderEntity.commonServices.count) {
            HomberServiceEntity* commonService = (HomberServiceEntity*)[self.orderEntity.commonServices objectAtIndex:index];
            desc = commonService.desc;
            name = commonService.name;
        } else {
            CustomServiceEntity* customService = (CustomServiceEntity*)[self.orderEntity.customServices objectAtIndex:(index - self.orderEntity.commonServices.count)];
            desc = customService.desc;
            name = customService.name;
        }
        
        if (self.descPopupView) {
            [self.descPopupView removeFromSuperview];
            self.descPopupView = nil;
        }
        
        CGFloat height = [Helpers calculateHeightText:desc width:self.view.frame.size.width * 2/3 font:[UIFont fontWithName:@"Roboto-Regular" size:15.f]].height;
        height = MIN(height, self.view.frame.size.height * 2/3);
        self.descPopupView = [[ProductDescriptionPopupView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width * 2/3 + 20, height + 60)];
        self.descPopupView.tvDescription.text = desc;
        self.descPopupView.lbTitle.text = name;
        [self setupPopper2WithSubView:self.descPopupView];
    }
}

@end

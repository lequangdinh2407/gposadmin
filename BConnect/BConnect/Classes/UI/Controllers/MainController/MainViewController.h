//
//  MainViewController.h
//  BConnect
//
//  Created by Inglab on 16/04/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleBarChart.h"
#import "ProductCategoryView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainViewController : BaseViewViewController<SimpleBarChartDataSource, SimpleBarChartDelegate> {
    UIButton* _btnSlideMenu;
    UIButton* _btnRightSlideMenu;
    
    NSArray *_values;
    NSArray *_xValues;
        
    SimpleBarChart *_chart;
        
    NSArray *_barColors;
    NSInteger _currentBarColor;
    
    CGRect _hiddenCategoryFrame;
    CGRect _showCategoryFrame;
    BOOL _isLoadedCategory;
}

@property (nonatomic, strong) ProductCategoryView* productCategoryView;

@property (weak, nonatomic) IBOutlet UILabel *lbTotal;

@property (weak, nonatomic) IBOutlet UILabel *lbOrders;

@property (weak, nonatomic) IBOutlet UILabel *lbStoreName;

@property (weak, nonatomic) IBOutlet UIButton *btnCover;

@end

NS_ASSUME_NONNULL_END

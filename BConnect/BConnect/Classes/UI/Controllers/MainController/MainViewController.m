//
//  MainViewController.m
//  BConnect
//
//  Created by Inglab on 16/04/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "MainViewController.h"
#import "SessionManager.h"
#import "NSMutableArray+TTMutableArray.h"
#import "SaleEntity.h"
@interface MainViewController () {
    NSMutableArray* monthsList;
    NSMutableArray* valuesList;
    double maximumValue;
}

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lbTotal.textColor = gposGreen;
    self.lbOrders.textColor = gposOrange;
   
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self hideHeaderView:true];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isInit) {
        if (![CoreStringUtils isEmpty:[SessionManager sharedClient].sessionKey]) {
            [self prepareSlideMenu];
            [self initData];
            isInit = true;
            [self makeListCategoryContainerView];
        }
    }
    
    [[RequestManager sharedClient] requestGetStoreList:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetStoreList succeeded" message:results];
        
        NSMutableArray* storeList = [[results objectForKey:RESPONSE_DATA] mutableCopy];
        self.productCategoryView.listCategories = storeList;
        self.productCategoryView.isSearching = false;
        [self.productCategoryView.tableView reloadData];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetStoreList failed" message:results];
        
    }];
    
    self.lbStoreName.text = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_STORENAME];
    
    [self getChartData];
}

-(void)getChartData {
    [[RequestManager sharedClient] requestGetRecentSaleList:12 storeId:[SharePrefUtils getStringPreference:PREFKEY_CONFIG_STORE_ID] success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetStoreList succeeded" message:results];
        
        NSMutableArray* saleList = [[results objectForKey:RESPONSE_SALES] mutableCopy];
        
        double totalOrder = [[results objectForKey:RESPONSE_TOTAL_ORDER] doubleValue];
        double totalSale = [[results objectForKey:RESPONSE_TOTAL_SALE] integerValue];
        
        [self fillInTotalOrder:totalOrder andTotalSale:totalSale];
        
        for (int i = 0; i < saleList.count; i++) {
            SaleEntity* entity = [saleList objectAtIndex:i];
            NSString* startDate = [DateUtils parseTimeStampToDate:entity.startTime];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"hh:mm aa MM/dd/yyyy"];
            NSDate *date = [dateFormatter dateFromString:startDate];
            
            [dateFormatter setDateFormat:@"MM/yyyy"];
            NSString *month = [dateFormatter stringFromDate:date];
            
            for (int j = 0; j < monthsList.count; j++) {
                if ([monthsList[j] isEqualToString:month]) {
                    valuesList[j] = @(entity.sale);
                    if (entity.sale > maximumValue) {
                        maximumValue = entity.sale;
                    }
                }
            }
        }
        
        [self addChartView];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetStoreList failed" message:results];
        
    }];
}

- (void)prepareSlideMenu {
    
    _btnSlideMenu = [[UIButton alloc] initWithFrame:CGRectMake(10, 15 + ((BaseViewViewController*)rootView).topPadding , 40, 40)];
    
    [_btnSlideMenu setTitle:nil forState:UIControlStateNormal];
    [_btnSlideMenu setBackgroundColor:[UIColor clearColor]];
    [_btnSlideMenu setImage:[UIImage imageNamed:@"ic_menu"] forState:UIControlStateNormal];
    _btnSlideMenu.imageEdgeInsets = UIEdgeInsetsMake(11.5, 10, 11.5, 10);
    [_btnSlideMenu addTarget:self action:@selector(btnMenuPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnSlideMenu];
    
    
    _btnRightSlideMenu = [[UIButton alloc] initWithFrame:CGRectMake( self.view.frame.size.width - 60 , 10 + ((BaseViewViewController*)rootView).topPadding , 60, 60)];
    
    [_btnRightSlideMenu setTitle:nil forState:UIControlStateNormal];
    [_btnRightSlideMenu setBackgroundColor:[UIColor clearColor]];
    [_btnRightSlideMenu setImage:[UIImage imageNamed:@"ic_store"] forState:UIControlStateNormal];
    _btnRightSlideMenu.imageEdgeInsets = UIEdgeInsetsMake(11.5, 10, 11.5, 10);
    [_btnRightSlideMenu addTarget:self action:@selector(btnRightMenuPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnRightSlideMenu];
    
    /*
    if (!self.notificationHub) {
        self.notificationHub = [[RKNotificationHub alloc] initWithView:_btnSlideMenu];
        [self.notificationHub setCountLabelFont:[UIFont fontWithName:@"SanFranciscoDisplay-Bold" size:9.0f]];
        [self.notificationHub setCircleColor:[UIColor redColor] labelColor:[UIColor whiteColor]];
        [self.notificationHub setCount:0];
    }
     */
}

- (void)btnMenuPressed:(UIButton *)sender {
    @synchronized(self) {
        [self.view endEditing:YES];
        [self btnCoverPressed:nil];
        [self.frostedViewController.view endEditing:YES];
        [self.frostedViewController presentMenuViewController];
    }
}

- (void)btnRightMenuPressed:(UIButton *)sender {
    @synchronized(self) {
        [self.view endEditing:YES];
        [self hideListCategory:NO];
    }
}

-(void)initData {

    [self initChartData];
    [self fillInTotalOrder:0 andTotalSale:0];
}

-(void)fillInTotalOrder:(int)totalOrder andTotalSale:(double)totalSale {
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Total:  " attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:14], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [attributeString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"$%0.2f", totalSale] attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:22], NSForegroundColorAttributeName: [UIColor greenColor]}]];
    self.lbTotal.attributedText = attributeString;
    
    NSMutableAttributedString *ordersAttributeString = [[NSMutableAttributedString alloc] initWithString:@"Orders:  " attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:14], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [ordersAttributeString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d", totalOrder] attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:22], NSForegroundColorAttributeName: [UIColor orangeColor]}]];
    self.lbOrders.attributedText = ordersAttributeString;
}

-(void)initChartData {
    monthsList = [[NSMutableArray alloc] init];
    valuesList = [[NSMutableArray alloc] init];
    maximumValue = 0;
    NSDate* date = [NSDate date];
    NSCalendar *calendar = [[NSLocale currentLocale] objectForKey:NSLocaleCalendar];
    NSDateComponents *components = [calendar components: NSCalendarUnitMonth fromDate:date];
    
    NSInteger month = [components month];
    
    NSDateComponents *componentYear = [calendar components: NSCalendarUnitYear fromDate:date];
    NSInteger year = [componentYear year];
    
    for (int i = 0; i<12; i++) {
        if (month < 10) {
            [monthsList addObject:[NSString stringWithFormat:@"0%d/%d", (int)month, (int)year]];
        } else {
            [monthsList addObject:[NSString stringWithFormat:@"%d/%d", (int)month, (int)year]];
        }
        
        if (month == 1) {
            month = 12;
            year -= 1;
        } else {
            month -= 1;
        }
        
        [valuesList addObject:@(0)];
    }
    
    monthsList = [[[monthsList reverseObjectEnumerator] allObjects] mutableCopy];
    
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark SimpleBarChartDataSource

- (NSUInteger)numberOfBarsInBarChart:(SimpleBarChart *)barChart
{
    return _values.count;
}

- (CGFloat)barChart:(SimpleBarChart *)barChart valueForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] floatValue];
}

- (NSString *)barChart:(SimpleBarChart *)barChart textForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] stringValue];
}

- (NSString *)barChart:(SimpleBarChart *)barChart xLabelForBarAtIndex:(NSUInteger)index
{

    return [_xValues objectAtIndex:index];
}

- (UIColor *)barChart:(SimpleBarChart *)barChart colorForBarAtIndex:(NSUInteger)index
{
    return [_barColors objectAtIndex:_currentBarColor];
}

- (void)addChartView
{
    if (_chart) {
        [_chart removeFromSuperview];
    }
    
    //[super loadView];
    _xValues = monthsList;
  //@[@"12-2018", @"1-2019", @"2-2019", @"3-2019", @"4-2019", @"5-2019", @"6-2019", @"7-2019" , @"8-2019", @"9-2019", @"10-2019", @"11-2019"];
    _values = valuesList;
  //@[@30, @45, @44, @60, @95, @2, @8, @9 , @12, @9, @9, @15];
    _barColors                        = @[[UIColor blueColor], [UIColor redColor], [UIColor whiteColor], [UIColor orangeColor], [UIColor purpleColor], [UIColor greenColor]];
    _currentBarColor                = 0;
    
    CGRect chartFrame                = CGRectMake(0.0,
                                                  0.0,
                                                  self.view.bounds.size.width * 0.9,
                                                  self.view.bounds.size.width * 0.9);
    _chart                            = [[SimpleBarChart alloc] initWithFrame:chartFrame];
    _chart.center                    = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    if (IS_IPHONE_5) {
        _chart.center                    = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0 + 50);
    }
    
    _chart.delegate                    = self;
    _chart.dataSource                = self;
    _chart.barShadowOffset            = CGSizeMake(2.0, 1.0);
    _chart.animationDuration        = 1.0;
    _chart.barShadowColor            = [UIColor grayColor];
    _chart.barShadowAlpha            = 0.5;
    _chart.barShadowRadius            = 1.0;
    _chart.barWidth                    = 15.0;
    _chart.xLabelType                = SimpleBarChartXLabelTypeVerticle;
    _chart.incrementValue            = (int)([Helpers newMaximumNumber:maximumValue]/10);
    _chart.barTextType                = SimpleBarChartBarTextTypeTop;
    _chart.barTextColor                = [UIColor whiteColor];
    _chart.gridColor                = [[UIColor lightGrayColor] colorWithAlphaComponent:0.2];
    
    [self.view addSubview:_chart];
    [self.view bringSubviewToFront:self.productCategoryView];
    [_chart reloadData];
    
}

#pragma mark Right menu

- (void)makeListCategoryContainerView {
    _hiddenCategoryFrame = CGRectMake(CGRectGetWidth(self.view.frame), 0, kListContainerWidth, CGRectGetHeight(self.view.frame));
    _showCategoryFrame = CGRectMake(CGRectGetWidth(self.view.frame) - kListContainerWidth, 0, kListContainerWidth, CGRectGetHeight(self.view.frame));
    
    self.productCategoryView = [[ProductCategoryView alloc] initWithFrame:_hiddenCategoryFrame];
    
    
    [self.view addSubview:self.productCategoryView];
    self.productCategoryView.delegate = self;
}

- (void)hideListCategory:(BOOL)isHide {
    [UIView animateWithDuration:0.5 animations:^{
        self.productCategoryView.frame = isHide ? _hiddenCategoryFrame : _showCategoryFrame;
        [self.btnCover setHidden:isHide];
        [self.view layoutIfNeeded];
        if (!isHide)
            [self hideCart:YES];
        
    } completion:^(BOOL finished) {
        if (!isHide && !_isLoadedCategory) {
            [self.productCategoryView.tableView reloadData];
            _isLoadedCategory = YES;
        }
        if (isHide)
            [self hideCart:NO];
    }];
}

- (IBAction)btnCoverPressed:(id)sender {
    [self hideListCategory:YES];
}

- (void)dismissRightMenu {
    [self hideListCategory:YES];
    self.lbStoreName.text = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_STORENAME];
    [self initChartData];
    [self getChartData];
}

@end

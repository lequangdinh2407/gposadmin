//
//  RatingPopupView.m
//  BConnect
//
//  Created by Dragon on 2/18/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import "CommentTitleView.h"

@implementation CommentTitleView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"CommentTitleView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        self.lineHeightConstraint.constant = 1.f/[UIScreen mainScreen].scale;
    }
    return self;
}

- (IBAction)btnTitlePressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentTitlePressed)]) {
        [self.delegate commentTitlePressed];
    }
}

- (IBAction)btnClosePressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentTitleClose)]) {
        [self.delegate commentTitleClose];
    }
}

- (void)loadInfo:(int)likes comments:(int)comments {
    
    self.lbLikes.text = [NSString stringWithFormat:@"%@ %@", [Helpers convertThousandToK:likes], [CoreStringUtils getStringRes:@"str_likes"]];
    self.lbLikesWidthConstraint.constant = [Helpers calculateWidthText:self.lbLikes.text height:self.lbLikes.frame.size.height font:self.lbLikes.font].width + 5;
    
    self.lbComments.text = [NSString stringWithFormat:@"%@ %@", [Helpers convertThousandToK:comments], [CoreStringUtils getStringRes:@"str_comments"]];
}

@end

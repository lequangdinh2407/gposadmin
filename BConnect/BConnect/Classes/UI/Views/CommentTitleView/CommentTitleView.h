//
//  RatingPopupView.h
//  BConnect
//
//  Created by Dragon on 2/18/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CommentTitleViewDelegate

- (void)commentTitlePressed;
- (void)commentTitleClose;

@end

@interface CommentTitleView : UIView

@property (weak, nonatomic) IBOutlet UILabel* lbLikes;
@property (weak, nonatomic) IBOutlet UILabel* lbComments;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbLikesWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineHeightConstraint;

@property (weak, nonatomic) id delegate;

- (void)loadInfo:(int)likes comments:(int)comments;

@end
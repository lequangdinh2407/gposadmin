//
//  NotificationTableViewCell.m
//  BConnect
//
//  Created by Dragon on 1/19/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import "NotificationTableViewCell.h"

#define IMG_PHOTO_WIDTH 40

@implementation NotificationTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.imgPhoto.layer.cornerRadius = IMG_PHOTO_WIDTH / 2;
    self.imgPhoto.layer.masksToBounds = YES;
    self.imgPhoto.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state}
}

- (void)loadCellData:(id)data{
    self.entity = data;
    if (self.entity.content && [self.entity.content isKindOfClass:[NSDictionary class]]) {
        [self.lbMessage setAttributedText:self.entity.attrString];
        NSString* thumb = [CoreStringUtils getStringDiffNull:[(NSDictionary*)self.entity.content objectForKey:@"thumb"]];
        [Helpers loadImage:self.imgPhoto placeHolder:[UIImage imageNamed:@"ic_tradeshow_logo"] withUrl:thumb thumbSize:80];
    }
    
    CGFloat descHeight = [self.entity.attrString boundingRectWithSize:(CGSize){CGRectGetWidth([UIScreen mainScreen].bounds) - 70, CGFLOAT_MAX} options:NSStringDrawingUsesLineFragmentOrigin context:nil].size.height + 10;
    self.descHeightConstraint.constant = descHeight;
    
    double time = [self.entity.createTime doubleValue];
    self.lbDate.text = self.entity.createTime > 0 ? [DateUtils getNotificationTime:time/1000] : @"";
    
    if (self.entity.isViewed) {
        [self.bg setBackgroundColor:NOTIFICATION_READ_COLOR];
    } else {
        [self.bg setBackgroundColor:NOTIFICATION_UNREAD_COLOR];
    }
    
    switch (self.entity.type) {
        case WebNotiType:
            self.imgIcon.image = [UIImage imageNamed:@"ic_notif_website"];
            break;
        case AccountNotiType:
            self.imgIcon.image = [UIImage imageNamed:@"ic_notif_welcome"];
            break;
        case SocialNotiType: {
            int socialType = [Helpers getUIntDiffNull:[(NSDictionary*)self.entity.content objectForKey:@"socialType"]];
            switch (socialType) {
                case LikeCommentSocialType:
                case LikeFeedSocialType:
                    self.imgIcon.image = [UIImage imageNamed:@"ic_notif_like"];
                    break;
                case ReplyCommentSocialType:
                case CommentFeedSocialType:
                    self.imgIcon.image = [UIImage imageNamed:@"ic_notif_comment"];
                    break;                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
}

@end

//
//  ExampleViewController.h
//  FastttCamera
//
//  Created by Laura Skelton on 2/9/15.
//  Copyright (c) 2015 IFTTT. All rights reserved.
//

@class FastttCapturedImage;
@class FasttCameraViewController;
@class ALAsset;

@protocol FasttCameraControllerDelegate <NSObject>

- (void)dismissFasttCameraController:(FasttCameraViewController*)controller;
@optional
- (void)savedPhotoFasttCamera:(NSURL*)assetUrl capturedImage:(FastttCapturedImage*)capturedImage;
- (void)capturedPhotoFasttCamera:(FastttCapturedImage*)capturedImage;
- (void)assetURLPhotoFasttCamera:(NSURL*)asset capturedImage:(FastttCapturedImage*)capturedImage;

@end

@interface FasttCameraViewController : UIViewController

@property (weak, nonatomic) id delegate;

@end

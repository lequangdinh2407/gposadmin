//
//  ConfirmViewController.h
//  FastttCamera
//
//  Created by Laura Skelton on 2/9/15.
//  Copyright (c) 2015 IFTTT. All rights reserved.
//

@import UIKit;
@class FastttCapturedImage;
@protocol FasttConfirmControllerDelegate;

@interface FasttConfirmViewController : UIViewController {
    NSURL *_assetURL;
}

@property (nonatomic, weak) id <FasttConfirmControllerDelegate> delegate;
@property (nonatomic, strong) FastttCapturedImage *capturedImage;
@property (nonatomic, assign) BOOL imagesReady;

@end

@protocol FasttConfirmControllerDelegate <NSObject>

- (void)dismissConfirmController:(FasttConfirmViewController *)controller;
- (void)usePhotoConfirmController:(FastttCapturedImage*)capturedImage;
- (void)savedURLPhotoConfirmController:(NSURL*)assetUrl capturedImage:(FastttCapturedImage*)capturedImage;
- (void)useAssetURLPhotoConfirmController:(NSURL*)asset capturedImage:(FastttCapturedImage*)capturedImage;

@end

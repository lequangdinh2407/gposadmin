//
//  ConfirmViewController.m
//  FastttCamera
//
//  Created by Laura Skelton on 2/9/15.
//  Copyright (c) 2015 IFTTT. All rights reserved.
//

#import "FasttConfirmViewController.h"
#import <Masonry/Masonry.h>
#import <FastttCamera/FastttCamera.h>
#import "UIAlertView+Blocks.h"

@import AssetsLibrary;
@import MessageUI;

@interface FasttConfirmViewController () <MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) UIButton *confirmButton;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIImageView *previewImageView;

@end

@implementation FasttConfirmViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    _previewImageView = [[UIImageView alloc] initWithImage:self.capturedImage.rotatedPreviewImage];
    self.previewImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.view addSubview:self.previewImageView];
    [self.previewImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    _backButton = [UIButton new];
    [self.backButton addTarget:self
                        action:@selector(dismissConfirmController)
              forControlEvents:UIControlEventTouchUpInside];
    
    [self.backButton setTitle:@"Back"
                     forState:UIControlStateNormal];
    
    [self.view addSubview:self.backButton];
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(20.f);
        make.left.equalTo(self.view).offset(20.f);
    }];
    
    _confirmButton = [UIButton new];
    [self.confirmButton addTarget:self
                           action:@selector(confirmButtonPressed)
                 forControlEvents:UIControlEventTouchUpInside];
    
    [self.confirmButton setTitle:@"Use Photo"
                        forState:UIControlStateNormal];
    
    if (!self.capturedImage.isNormalized) {
        self.confirmButton.enabled = NO;
    }
    
    [self.view addSubview:self.confirmButton];
    [self.confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-20.f);
    }];
}

- (void)setImagesReady:(BOOL)imagesReady
{
    _imagesReady = imagesReady;
    if (imagesReady) {
        
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        
        [library writeImageToSavedPhotosAlbum:[self.capturedImage.fullImage CGImage]
                                  orientation:(ALAssetOrientation)[self.capturedImage.fullImage imageOrientation]
                              completionBlock:^(NSURL *assetURL, NSError *error){
                                  if (error) {
                                      NSLog(@"Error saving photo: %@", error.localizedDescription);
                                      [self showAlertPermission];
                                  } else {
                                      self.confirmButton.enabled = YES;
                                      _assetURL = assetURL;
                                      
                                      if (self.delegate && [self.delegate respondsToSelector:@selector(useAssetURLPhotoConfirmController:capturedImage:)]) {
                                          [self.delegate savedURLPhotoConfirmController:_assetURL capturedImage:self.capturedImage];
                                      }
                                  }
                              }];
    }
}

#pragma mark - Actions

- (void)dismissConfirmController
{
    [self.delegate dismissConfirmController:self];
}

- (void)confirmButtonPressed
{
//    [self emailPhoto];
    
    [self savePhotoToCameraRoll];
}

- (void)savePhotoToCameraRoll {
    if (self.delegate && [self.delegate respondsToSelector:@selector(useAssetURLPhotoConfirmController:capturedImage:)]) {
        [self.delegate useAssetURLPhotoConfirmController:_assetURL capturedImage:self.capturedImage];
    }
}

- (void)showAlertPermission {
    [[[UIAlertView alloc] initWithTitle:[CoreStringUtils getStringRes:@"str_access_denied"]
                                message:[CoreStringUtils getStringRes:@"str_camera_support_warning_message"]
                       cancelButtonItem:[RIButtonItem itemWithLabel:[CoreStringUtils getStringRes:@"str_cancel"] action:^{
        
    }]
                       otherButtonItems:[RIButtonItem itemWithLabel:[CoreStringUtils getStringRes:@"str_open_setting"] action:^{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }], nil] show];
}

- (void)emailPhoto
{
    NSString *emailTitle = @"FastttCamera Photo";
    NSString *messageBody = @"Check out my FastttCamera photo!";
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCompose = [MFMailComposeViewController new];
        mailCompose.mailComposeDelegate = self;
        [mailCompose setSubject:emailTitle];
        [mailCompose setMessageBody:messageBody isHTML:NO];
        [mailCompose addAttachmentData:UIImageJPEGRepresentation(self.capturedImage.scaledImage, 0.85f)
                              mimeType:@"image/jpeg"
                              fileName:@"fast_camera_photo.jpg"];
        
        [self presentViewController:mailCompose animated:YES completion:nil];
    } else {
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Mail not configured"
                                                       message:@"Cannot share this photo without mail configured."
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self dismissConfirmController];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self dismissConfirmController];
}

@end

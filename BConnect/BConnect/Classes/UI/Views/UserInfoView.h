//
//  UserInfoView.h
//  BConnect
//
//  Created by Dragon on 1/8/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"
#import "CountryEntityV2.h"
#import "ActionSheetStringPicker.h"

@protocol UserInfoViewDelegate
@optional
//- (void)showCountryList;
//- (void)btnDonePressed;

-(void)btnNextPressed;

@end

@interface UserInfoView : UIView
/*
 * Container
 */

@property (weak, nonatomic) IBOutlet UIView *storeKeyContainer;
@property (weak, nonatomic) IBOutlet UITextField *txtStoreKey;


@property (weak, nonatomic) IBOutlet UIView *phonenumberContainer;
@property (weak, nonatomic) IBOutlet UIView *firstNameContainer;
@property (weak, nonatomic) IBOutlet UIView *lastNameContainer;
@property (weak, nonatomic) IBOutlet UIView *emailContainer;
@property (weak, nonatomic) IBOutlet UIView *dateContainer;
@property (weak, nonatomic) IBOutlet UIView *monthContainer;

/*
 * text field
 */
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtFirstName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtLastName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtEmail;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtPhonenumber;

@property (weak, nonatomic) IBOutlet UITextField *txtDate;
@property (weak, nonatomic) IBOutlet UITextField *txtMonth;

@property (weak, nonatomic) id delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;


//- (IBAction)btnCountryPressed:(id)sender;

-(BOOL)validateDate:(NSString*)date andMonth:(NSString*)month;
- (void)prepareLayout;

@end

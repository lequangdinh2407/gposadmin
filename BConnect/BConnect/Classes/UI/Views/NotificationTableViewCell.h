//
//  NotificationTableViewCell.h
//  BConnect
//
//  Created by Dragon on 1/19/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsEntity.h"

@interface NotificationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *bg;
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UITextView *lbMessage;
@property (weak, nonatomic) IBOutlet UILabel *lbDate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descHeightConstraint;
@property (weak, nonatomic) NewsEntity* entity;

- (void)loadCellData:(id)data;

@end

//
//  PhotoPickerView.m
//  luvkonect
//
//  Created by Dragon on 5/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "PhotoPickerView.h"

@implementation PhotoPickerView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"PhotoPickerView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        float contentWidth = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? (CGRectGetWidth([UIScreen mainScreen].bounds)*2/3) : (CGRectGetWidth([UIScreen mainScreen].bounds) - 2);
        self.containWidthConstraint.constant = contentWidth;
        [self layoutIfNeeded];
        UITapGestureRecognizer* touch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissView)];
        [self addGestureRecognizer:touch];
    }
    return self;
}

- (IBAction)btnGalleryPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(btnGalleryPressed)]) {
        [self dismissView];
        [self.delegate btnGalleryPressed];
    }
}

- (IBAction)btnCameraPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(btnCameraPressed)]) {
        [self dismissView];
        [self.delegate btnCameraPressed];
    }
}

- (void)dismissView{
    [self removeFromSuperview];
}
@end

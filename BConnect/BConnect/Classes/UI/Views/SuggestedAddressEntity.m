//
//  SuggestedAddressEntity.m
//  BConnect
//
//  Created by Dragon on 3/3/17.
//  Copyright © 2017 DKMobility. All rights reserved.
//

#import "SuggestedAddressEntity.h"

@implementation SuggestedAddressEntity

-(void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.fullAddress forKey:@"fullAdress"];
    [encoder encodeObject:self.firstLine forKey:@"firstLine"];
    [encoder encodeObject:self.lastLine forKey:@"lastLine"];
    [encoder encodeObject:self.placeId forKey:@"placeId"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.fullAddress = [decoder decodeObjectForKey:@"fullAdress"];
        self.firstLine = [decoder decodeObjectForKey:@"firstLine"];
        self.lastLine = [decoder decodeObjectForKey:@"lastLine"];
        self.placeId = [decoder decodeObjectForKey:@"placeId"];
    }
    return self;
}

@end

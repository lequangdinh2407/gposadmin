//
//  RatingPopupView.m
//  BConnect
//
//  Created by Dragon on 2/18/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import "UploadAvatarPopupView.h"

@implementation UploadAvatarPopupView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"UploadAvatarPopupView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        
        self.lineHeightConstraint.constant = 1.f/[UIScreen mainScreen].scale;        
    }
    return self;
}

- (IBAction)btnCameraPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(uploadCamera)]) {
        [self.delegate uploadCamera];
    }
}

- (IBAction)btnGalleryPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(uploadGallery)]) {
        [self.delegate uploadGallery];
    }
}

@end

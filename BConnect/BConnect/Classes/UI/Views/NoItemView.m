//
//  NoItemView.m
//  BConnect
//
//  Created by ToanPham on 1/7/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import "NoItemView.h"

@implementation NoItemView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"NoItemView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        self.btnTry.layer.cornerRadius = 15;
        [self layoutIfNeeded];
    }
    return self;
}

- (IBAction)btnPressed:(id)sender {
    if ([self.noItemDelegate respondsToSelector:@selector(btnClickedNoItemView)]) {
        [self.noItemDelegate btnClickedNoItemView];
    }
}

- (void)loadText:(NSString*)notificationText btnTitle:(NSString*)btnTitle {
    self.lbNotification.text = notificationText;
    if (![CoreStringUtils isEmpty:btnTitle]) {
        [self.btnTry setTitle:btnTitle forState:UIControlStateNormal];
    } else {
        [self.btnTry setHidden:YES];
    }
    [self.lbNotification sizeToFit];

//    self.lbTry.text = btnTitle;
//    [self.lbTry sizeToFit];
}

@end

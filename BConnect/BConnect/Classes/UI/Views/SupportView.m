//
//  SupportView.m
//  luvkonect
//
//  Created by Dragon on 6/8/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "SupportView.h"
#import "SessionManager.h"

@implementation SupportView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SupportView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUIDone) name:UITextViewTextDidChangeNotification object:nil];
        
        if (![CoreStringUtils isEmpty:[SessionManager sharedClient].sessionKey]) {
            self.inforHeighConstraint.constant = 0;
        }
        
        UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        numberToolbar.barStyle = UIBarStyleDefault;
        numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
        [numberToolbar sizeToFit];
        self.txtPhone.inputAccessoryView = numberToolbar;
        
        [self.txtPhone addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        [self.txtName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        [self.txtEmail addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];       
        
        self.btnSubmit.layer.cornerRadius = 20.0f;
        
        [self layoutIfNeeded];
    }
    return self;
}

-(void)cancelNumberPad{
    [self.txtPhone resignFirstResponder];
}

-(void)doneWithNumberPad{
    [self.txtPhone resignFirstResponder];
    [self.phoneContainer addBorderColor:normal withHeight:normalHeigh];
    [self.tvFeedback becomeFirstResponder];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateUIDone{
    if (![CoreStringUtils isEmpty:[SessionManager sharedClient].sessionKey]) {
        if (![CoreStringUtils isEmptyOrWhiteSpace:self.tvFeedback.text]) {
            [self.btnSubmit enableUI:YES];
        }else{
            [self.btnSubmit enableUI:NO];
        }
    }else{
        if (![CoreStringUtils isEmptyOrWhiteSpace:self.tvFeedback.text] &&
            ![CoreStringUtils isEmptyOrWhiteSpace:self.txtName.text] &&
            ![CoreStringUtils isEmptyOrWhiteSpace:self.txtEmail.text] &&
            ![CoreStringUtils isEmptyOrWhiteSpace:self.txtPhone.text] &&
            ![CoreStringUtils isEmptyOrWhiteSpace:self.txtCountry.text]) {
            [self.btnSubmit enableUI:YES];
        }else{
            [self.btnSubmit enableUI:NO];
        }
    }
}

-(void)textFieldDidChange :(UITextField *)theTextField{
    [self updateUIDone];
}

- (void)setFloatLabelUIDefault:(JVFloatLabeledTextField*)label{
    [label setFloatingLabelFont:[UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:12.0]];
    [label setFloatingLabelActiveTextColor:forcus];
}

- (void)prepareLayout{
    [self showBorderDefault];
    [self setFloatLabelUIDefault:self.txtName];
    [self setFloatLabelUIDefault:self.txtEmail];
    [self setFloatLabelUIDefault:self.txtPhone];
    [self setFloatLabelUIDefault:self.txtCountry];
    
    self.tvFeedback.placeholder = [CoreStringUtils getStringRes:@"str_support_placeholder"];
    self.tvFeedback.delegate = self;
    [self updateUIDone];
}

- (IBAction)btnCountryPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(showCountryList)]) {
        [self.delegate showCountryList];
    }
}

- (void)showBorderDefault{
    [self.nameContainer addBorderColor:normal withHeight:normalHeigh];
    [self.countryContainer addBorderColor:normal withHeight:normalHeigh];
    [self.emailContainer addBorderColor:normal withHeight:normalHeigh];
    [self.phoneContainer addBorderColor:normal withHeight:normalHeigh];
}

- (void)setCountrySelected:(CountryEntityV2*)entity{
    self.countryEnitySelected = entity;
    self.txtCountry.text = [NSString stringWithFormat:@"%@", self.countryEnitySelected.name];
}

#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.txtName]) {
        [self.txtEmail performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    }if ([textField isEqual:self.txtEmail]) {
        [self.txtPhone performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    }if ([textField isEqual:self.txtPhone]) {
        [self.tvFeedback performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    }else{
        [self endEditing:YES];
        [self showBorderDefault];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self showBorderDefault];
    if ([textField isEqual:self.txtName]) {
        [self.nameContainer addBorderColor:forcus withHeight:forcusHeigh];
    }else if ([textField isEqual:self.txtEmail]){
        [self.emailContainer addBorderColor:forcus withHeight:forcusHeigh];
    }else if ([textField isEqual:self.txtPhone]){
        [self.phoneContainer addBorderColor:forcus withHeight:forcusHeigh];
    }else if ([textField isEqual:self.txtCountry]){
        [self.countryContainer addBorderColor:forcus withHeight:forcusHeigh];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.txtPhone) {
        
        if (![string isEqualToString:@""]) {
            textField.text = [CoreStringUtils formatPhoneNumber:textField.text deleteLastChar:NO];
        }
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [self showBorderDefault];
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString* tmpString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if (tmpString.length > CHARACTER_LIMITED) {
        return NO;
    }
    self.lbTextReaming.text = [NSString stringWithFormat:@"%lu %@", (CHARACTER_LIMITED - tmpString.length), [CoreStringUtils getStringRes:@"str_text_remain"]];
    return YES;
}

- (IBAction)btnSubmitPressed:(id)sender {
    if (![CoreStringUtils isEmpty:[SessionManager sharedClient].sessionKey]) {
        if ([self.delegate respondsToSelector:@selector(submitFeedback)]) {
            [self.delegate submitFeedback];
        }
    }else{
        if (![Helpers isValidEmail:self.txtEmail.text]) {
            [[[UIAlertView alloc] initWithTitle:@"" message:[CoreStringUtils getStringRes:@"str_email_invalid"] delegate:self cancelButtonTitle:[CoreStringUtils getStringRes:@"str_ok"] otherButtonTitles:nil, nil] show];
            return;
        }
        if ([self.delegate respondsToSelector:@selector(requestContact)]) {
            [self.delegate requestContact];
        }
    }
    
}

@end

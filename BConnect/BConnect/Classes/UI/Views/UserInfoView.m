//
//  UserInfoView.m
//  BConnect
//
//  Created by Dragon on 1/8/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import "UserInfoView.h"
#import "SessionManager.h"
#import "UserEntity.h"

@implementation UserInfoView 

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"UserInfoView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUIDone) name:UITextFieldTextDidChangeNotification object:nil];
        
        UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        numberToolbar.barStyle = UIBarStyleDefault;
        numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
        [numberToolbar sizeToFit];
        self.txtPhonenumber.inputAccessoryView = numberToolbar;
        [self showBorderDefault];
        self.btnNext.layer.cornerRadius = 20.0f;
    }
    return self;
}

-(void)cancelNumberPad{
    [self.txtPhonenumber resignFirstResponder];
}

- (IBAction)btnNextPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(btnNextPressed)]) {
        [self.delegate btnNextPressed];
    }
}


-(void)doneWithNumberPad{
    [self.txtPhonenumber resignFirstResponder];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initData{
    
    self.txtStoreKey.text = [SessionManager sharedClient].userEntity.shopCode;
    self.txtFirstName.text = [SessionManager sharedClient].userEntity.firstName;
    self.txtLastName.text = [SessionManager sharedClient].userEntity.lastName;
    self.txtEmail.text = [SessionManager sharedClient].userEntity.email;
    self.txtPhonenumber.text = [SessionManager sharedClient].userEntity.nationalNumber;
    self.txtDate.text = [SessionManager sharedClient].userEntity.dateOB;
    self.txtMonth.text = [SessionManager sharedClient].userEntity.monthOB;

}

- (void)prepareLayout{
    //[self setFloatLabelUIDefault:self.txtCountryField];
    [self setFloatLabelUIDefault:self.txtFirstName];
    [self setFloatLabelUIDefault:self.txtLastName];
    [self setFloatLabelUIDefault:self.txtEmail];
    [self setFloatLabelUIDefault:self.txtPhonenumber];
    [self showBorderDefault];
    [self initData];
}

- (void)setFloatLabelUIDefault:(JVFloatLabeledTextField*)label{
    [label setFloatingLabelFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0]];
    [label setFloatingLabelActiveTextColor:forcus];
    [label setFloatingLabelTextColor:normal];
    [label setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:label.placeholder attributes:@{NSForegroundColorAttributeName:normal}]];
}

- (BOOL)checkUIDone{
    /*
     if (!self.isSocialLogin) {
     if (![CoreStringUtils isEmptyOrWhiteSpace:self.txtFirstName.text] &&
     ![CoreStringUtils isEmptyOrWhiteSpace:self.txtLastName.text] &&
     ![CoreStringUtils isEmptyOrWhiteSpace:self.txtEmail.text]) {
     return YES;
     } else {
     return NO;
     }
     } else { */
    
    if (![CoreStringUtils isEmptyOrWhiteSpace:self.txtFirstName.text] &&
        ![CoreStringUtils isEmptyOrWhiteSpace:self.txtLastName.text] &&
        ![CoreStringUtils isEmptyOrWhiteSpace:self.txtEmail.text] &&
        ![CoreStringUtils isEmptyOrWhiteSpace:self.txtPhonenumber.text] &&
        ![CoreStringUtils isEmptyOrWhiteSpace:self.txtStoreKey.text]) {
        return YES;
    } else {
        return NO;
    }
    
    //}
}

-(BOOL)validateDate:(NSString*)date andMonth:(NSString*)month {
    
    //NSArray *colors = @[@"Jan", @"Feb", @"Mar", @"Apr", @"May", @"Jun", @"Jul", @"Aug", @"Sep", @"Oct", @"Nov", @"Dec"];
    
    if ((([date  isEqual: @"30"] || [date  isEqual: @"31"]) && [month  isEqual: @"Feb"])
        || ([date  isEqual: @"31"] && ([month  isEqual: @"Apr"] || [month  isEqual: @"Jun"] || [month  isEqual: @"Sep"] || [month  isEqual:  @"Nov"]))
        ) {
        return false;
    }
    return true;
}

/*
 - (IBAction)btnCountryPressed:(id)sender {
 if ([self.delegate respondsToSelector:@selector(showCountryList)]) {
 [self.delegate showCountryList];
 }
 }
 */

- (void)updateUIDone{
    //if ([self.delegate respondsToSelector:@selector(updateUIDone)]) {
        //[self.delegate updateUIDone];
    //}
}

- (void)showBorderDefault{
    /*
     [self.phonenumberContainer addBorderColor:normal withHeight:normalHeigh];
     //[self.countryContainer addBorderColor:normal withHeight:normalHeigh];
     [self.firstNameContainer addBorderColor:normal withHeight:normalHeigh];
     [self.lastNameContainer addBorderColor:normal withHeight:normalHeigh];
     [self.emailContainer addBorderColor:normal withHeight:normalHeigh];
     */
    
    [self addCornerRadius:self.storeKeyContainer andRadius:5];
    [self addCornerRadius:self.phonenumberContainer andRadius:5];
    [self addCornerRadius:self.firstNameContainer andRadius:5];
    [self addCornerRadius:self.lastNameContainer andRadius:5];
    [self addCornerRadius:self.emailContainer andRadius:5];
    [self addCornerRadius:self.dateContainer andRadius:5];
    [self addCornerRadius:self.monthContainer andRadius:5];
}

-(void)addCornerRadius:(UIView*)view andRadius:(int)radius {
    view.layer.cornerRadius = radius;
    view.clipsToBounds = true;
}

#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.txtStoreKey]) {
        [self.txtPhonenumber performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    } else if ([textField isEqual:self.txtPhonenumber]) {
        [self.txtFirstName performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    } else if ([textField isEqual:self.txtFirstName]) {
        [self.txtLastName performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    } else if ([textField isEqual:self.txtLastName]) {
        [self.txtEmail performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    } else{
        [self endEditing:YES];
        //[self showBorderDefault];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    /*
     [self showBorderDefault];
     if ([textField isEqual:self.txtFirstName]) {
     [self.firstNameContainer addBorderColor:forcus withHeight:forcusHeigh];
     }else if ([textField isEqual:self.txtLastName]){
     [self.lastNameContainer addBorderColor:forcus withHeight:forcusHeigh];
     }else if ([textField isEqual:self.txtEmail]){
     [self.emailContainer addBorderColor:forcus withHeight:forcusHeigh];
     }else if ([textField isEqual:self.txtPhonenumber]){
     [self.phonenumberContainer addBorderColor:forcus withHeight:forcusHeigh];
     }
     */
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self updateUIDone];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.txtPhonenumber) {
        
        if (![string isEqualToString:@""]) {
            textField.text = [CoreStringUtils formatPhoneNumber:textField.text deleteLastChar:NO];
        }
    }
    return YES;
}

- (IBAction)btnDatePressed:(id)sender {
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        self.txtDate.text = selectedValue;
        /*
         if ([sender respondsToSelector:@selector(setText:)]) {
         [sender performSelector:@selector(setText:) withObject:selectedValue];
         }
         */
    };
    
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    NSArray *colors = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10"
                        , @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20"
                        , @"21", @"22", @"23", @"24", @"25", @"26", @"27", @"28", @"29", @"30", @"31"];
    [ActionSheetStringPicker showPickerWithTitle:@"Select Date" rows:colors initialSelection:0 doneBlock:done cancelBlock:cancel origin:sender];
}

- (IBAction)btnMonthPressed:(id)sender {
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        self.txtMonth.text = selectedValue;
        /*
         if ([sender respondsToSelector:@selector(setText:)]) {
         [sender performSelector:@selector(setText:) withObject:selectedValue];
         }
         */
    };
    
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    NSArray *colors = @[@"Jan", @"Feb", @"Mar", @"Apr", @"May", @"Jun", @"Jul", @"Aug", @"Sep", @"Oct", @"Nov", @"Dec"];
    [ActionSheetStringPicker showPickerWithTitle:@"Select Month" rows:colors initialSelection:0 doneBlock:done cancelBlock:cancel origin:sender];
}



@end
/*
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"UserInfoView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUIDone) name:UITextFieldTextDidChangeNotification object:nil];
        [self layoutIfNeeded];
        
        self.btnNext.layer.cornerRadius = 20;
    }
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initData{
    
    self.txtCountryField.text = [SessionManager sharedClient].userEntity.countryCode;
    self.txtFirstName.text = [SessionManager sharedClient].userEntity.firstName;
    self.txtLastName.text = [SessionManager sharedClient].userEntity.lastName;
    self.txtEmail.text = [SessionManager sharedClient].userEntity.email;
    self.txtHometown.text = [SessionManager sharedClient].userEntity.hometown;
    self.txtInterestedIn.text = [SessionManager sharedClient].userEntity.interestedIn;
    self.txtSlogan.text = [SessionManager sharedClient].userEntity.slogan;
    self.txtPhone.text = [SessionManager sharedClient].userEntity.nationalNumber;
    
    [self updateUIDone];
}

- (void)setFloatLabelUIDefault:(JVFloatLabeledTextField*)label{
    [label setFloatingLabelFont:[UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:12.0]];
    [label setFloatingLabelActiveTextColor:forcus];
    [label setFloatingLabelTextColor:normal];
    [label setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:label.placeholder attributes:@{NSForegroundColorAttributeName:normal}]];
}

- (void)prepareLayout{
    [self showBorderDefault];
    
    [self setFloatLabelUIDefault:self.txtCountryField];
    [self setFloatLabelUIDefault:self.txtFirstName];
    [self setFloatLabelUIDefault:self.txtLastName];
    [self setFloatLabelUIDefault:self.txtEmail];
    [self setFloatLabelUIDefault:self.txtPhone];
    [self setFloatLabelUIDefault:self.txtHometown];
    [self setFloatLabelUIDefault:self.txtInterestedIn];
    [self setFloatLabelUIDefault:self.txtSlogan];
    
    [_btnNext addShadow];
    
    [self initData];
}

- (IBAction)btnCountryPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(showCountryList)]) {
        [self.delegate showCountryList];
    }
}

- (IBAction)btnUpdatePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(btnDonePressed)]) {
        [self.delegate btnDonePressed];
    }
}

- (void)updateUIDone{
    if (![CoreStringUtils isEmptyOrWhiteSpace:self.txtFirstName.text] &&
        ![CoreStringUtils isEmptyOrWhiteSpace:self.txtLastName.text] && ![CoreStringUtils isEmptyOrWhiteSpace:self.txtEmail.text]) {
        self.btnNext.alpha = 1.0;
        [self.btnNext setEnabled:YES];
    }else{
        self.btnNext.alpha = 0.5;
        [self.btnNext setEnabled:NO];
    }
}

- (void)showBorderDefault{
    [self.firstNameContainer addBorderColor:normal withHeight:normalHeigh];
    [self.lastNameContainer addBorderColor:normal withHeight:normalHeigh];
    [self.favoriteMusicContainer addBorderColor:normal withHeight:normalHeigh];
    [self.hometownContainer addBorderColor:normal withHeight:normalHeigh];
    [self.emailContainer addBorderColor:normal withHeight:normalHeigh];
    [self.phoneContainer addBorderColor:normal withHeight:normalHeigh];
    [self.countryCodeContainer addBorderColor:normal withHeight:normalHeigh];
    [self.aboutMeContainer addBorderColor:normal withHeight:normalHeigh];
}

- (void)setCountrySelected:(CountryEntityV2*)entity{
    self.countryEnitySelected = entity;
    if (self.countryEnitySelected) {
        self.txtCountryField.text = [NSString stringWithFormat:@"%@", self.countryEnitySelected.phoneCode];
    }else{
        self.txtCountryField.text = @"";
    }
    [self updateUIDone];
}

- (CGFloat)showError:(NSString*)error {
    if (error && ![error isEqualToString:@""]) {
        self.lbMessage.text = error;
        self.lbMessage.numberOfLines = 9999;
        UIFont *font = [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:16];
        NSDictionary * attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
        
        CGRect tmpFrame = [error boundingRectWithSize:(CGSize){CGRectGetWidth(self.bounds) - 10, 99999}
                                                  options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                               attributes:attributes
                                                  context:nil];
        self.errorHeightConstraint.constant = tmpFrame.size.height + 5;
        return (self.kContentHeightDefault + self.errorHeightConstraint.constant);
    }else{
        self.errorHeightConstraint.constant = 0;
    }
    return self.kContentHeightDefault;
}

#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.txtFirstName]) {
        [self.txtLastName performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    }else if ([textField isEqual:self.txtLastName]) {
        [self.txtEmail performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    }else if ([textField isEqual:self.txtEmail]) {
        [self.txtHometown performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    }else if ([textField isEqual:self.txtHometown]) {
        [self.txtInterestedIn performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    }else if ([textField isEqual:self.txtInterestedIn]) {
        [self.txtSlogan performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    }else if ([textField isEqual:self.txtPhone]) {
        [self.txtPhone performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    }else{
        [self endEditing:YES];
        [self showBorderDefault];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self showBorderDefault];
    if ([textField isEqual:self.txtFirstName]) {
        [self.firstNameContainer addBorderColor:forcus withHeight:forcusHeigh];
    }else if ([textField isEqual:self.txtLastName]){
        [self.lastNameContainer addBorderColor:forcus withHeight:forcusHeigh];
    }else if ([textField isEqual:self.txtEmail]){
        [self.emailContainer addBorderColor:forcus withHeight:forcusHeigh];
    }else if ([textField isEqual:self.txtPhone]){
        [self.phoneContainer addBorderColor:forcus withHeight:forcusHeigh];
    }else if ([textField isEqual:self.txtHometown]){
        [self.hometownContainer addBorderColor:forcus withHeight:forcusHeigh];
    }else if ([textField isEqual:self.txtInterestedIn]){
        [self.favoriteMusicContainer addBorderColor:forcus withHeight:forcusHeigh];
    }else if ([textField isEqual:self.txtSlogan]){
        [self.aboutMeContainer addBorderColor:forcus withHeight:forcusHeigh];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self updateUIDone];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

@end
 */

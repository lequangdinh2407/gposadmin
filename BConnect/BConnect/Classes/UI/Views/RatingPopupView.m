//
//  RatingPopupView.m
//  BConnect
//
//  Created by Dragon on 2/18/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import "RatingPopupView.h"

@implementation RatingPopupView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"RatingPopupView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        self.starRatingView.starNormalColor = [UIColor lightGrayColor];
        self.starRatingView.starNormalColor = APP_STAR_BORDER_COLOR;
        self.starRatingView.starFillColor = APP_STAR_COLOR;
        self.starRatingView.canRate = YES;
        self.starRatingView.canRate = YES;
        self.starRatingView.starPadding = 0;
        self.starRatingView.starSize = (frame.size.width - 60) / 5;
    }
    return self;
}

- (IBAction)btnSubmitPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(submitRatingProduct)]) {
        [self.delegate submitRatingProduct];
    }
}

- (IBAction)btnCancelPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(cancelRatingProduct)]) {
        [self.delegate cancelRatingProduct];
    }
}
@end

//
//  BaseView.h
//  Loop
//
//  Created by Dragon on 7/14/15.
//  Copyright (c) 2015 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import <SMCalloutView/SMCalloutView.h>

#define kCellHeight 44
#define kTableHeightMax (CGRectGetHeight([UIScreen mainScreen].bounds) - 150)

@protocol BaseViewDelegate

@optional
- (void)selectPromotionIndex:(NSInteger)index;

@end

@interface BaseView : UIView<UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout>{
    BOOL isProcessing;
    UIActivityIndicatorView* spinner;
}

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray* promotionList;
@property (nonatomic, strong) NSMutableArray *cellSizes;

@property (weak, nonatomic) id delegate;
@property (strong, nonatomic) UINib * cellNib;

- (void)prepareLayout;

@end

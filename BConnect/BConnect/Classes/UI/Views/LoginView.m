//
//  LoginView.m
//  luvkonect
//
//  Created by Dragon on 5/21/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "LoginView.h"
#include <CoreText/CoreText.h>

#define kLoginViewFooterHeight 180
#define kLoginViewContentHeight 220

@implementation LoginView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"LoginView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        
        UITapGestureRecognizer* signupTouch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(signUpAccount)];
        [self.lbSignup addGestureRecognizer:signupTouch];
        
        self.btnLogin.layer.cornerRadius = 20.0f;
        
        self.bottomLineHeightConstraint.constant = 1.0 / [UIScreen mainScreen].scale;
    }
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupViewStyle:(UIView*)view{
    view.layer.borderColor = [normal CGColor];
    view.layer.borderWidth = 1;
}

- (void)showViewBorderDefault{
    [self.countryCodeContainer addBorderColor:normal withHeight:normalHeigh];
    [self.userNameContainer addBorderColor:normal withHeight:normalHeigh];
    [self.passContainer addBorderColor:normal withHeight:normalHeigh];
}

- (void)prepareLayout{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUIDone) name:UITextFieldTextDidChangeNotification object:nil];
    
//    UITapGestureRecognizer* forgotpassGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnForgotPaswordPressed)];
//    [self.lbForgotPassword addGestureRecognizer:forgotpassGesture];
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    self.lbForgotPassword.attributedText = [[NSAttributedString alloc] initWithString:@"Forgot Password ?"
                                                                           attributes:underlineAttribute];
    
    NSString* tmpAccountNotExisted = [CoreStringUtils getStringRes:@"str_account_not_exist"];
    NSString* tmpSignup = [CoreStringUtils getStringRes:@"str_signup"];
    NSString* tmpRegister = [NSString stringWithFormat:@"%@ %@", tmpAccountNotExisted, tmpSignup];
    NSRange rage = [tmpRegister rangeOfString:tmpSignup];
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:tmpRegister];
    
    [attrString addAttribute:NSUnderlineStyleAttributeName
                       value:[NSNumber numberWithInt:kCTUnderlineStyleSingle]
                       range:(NSRange){rage.location,rage.length}];
    [attrString addAttribute:NSForegroundColorAttributeName
                       value:APP_COLOR range:(NSRange){rage.location,rage.length}];
    rage = [tmpRegister rangeOfString:tmpAccountNotExisted];
    [attrString addAttribute:NSForegroundColorAttributeName
                       value:APP_TEXT_COLOR range:(NSRange){rage.location,rage.length}];
    
    [self.lbSignup setAttributedText:attrString];
    
    [self showViewBorderDefault];
    
    [self setFloatLabelUIDefault:self.txtCountryCode];
    [self setFloatLabelUIDefault:self.txtUserName];
    [self setFloatLabelUIDefault:self.txtPassword];
    
    [self updateUIDone];
}

- (void)setFloatLabelUIDefault:(JVFloatLabeledTextField*)label{
    [label setFloatingLabelFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0]];
    [label setFloatingLabelActiveTextColor:forcus];
}

- (void)updateUIDone {
    if (![CoreStringUtils isEmpty:self.txtUserName.text] &&
        ![CoreStringUtils isEmpty:self.txtPassword.text]) {
        [self.btnLogin enableUI:YES];
    } else {
        [self.btnLogin enableUI:NO];
    }
}

- (CGFloat)showErrorMessage:(NSString*)message{
    if (!message) {
        self.lbError.text = @"";
        if (self.lbErrorHeightConstraint.constant > 5) {
            self.contentHeightConstraint.constant = kLoginViewContentHeight;
            self.lbErrorHeightConstraint.constant = 5;
        }
    }else{
        self.lbError.text = message;
        [self.lbError sizeToFit];
        if (self.lbErrorHeightConstraint.constant == 5) {
            self.lbErrorHeightConstraint.constant = self.lbError.frame.size.height;
            self.contentHeightConstraint.constant = kLoginViewContentHeight + self.lbError.frame.size.height;
        }
    }
    [self layoutIfNeeded];
    return (self.contentHeightConstraint.constant + kLoginViewFooterHeight);
}

- (void)lockAllButton:(BOOL)isLock{
    [self.btnLogin setUserInteractionEnabled:!isLock];
    [self.lbForgotPassword setUserInteractionEnabled:!isLock];
}

#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.txtUserName]) {
        [self.txtUserName resignFirstResponder];
        [self.txtPassword becomeFirstResponder];
    }else{
        [self endEditing:YES];
        [self showViewBorderDefault];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self showViewBorderDefault];
    if ([textField isEqual:self.txtUserName]) {
        [self.userNameContainer addBorderColor:forcus withHeight:forcusHeigh];
    }else if ([textField isEqual:self.txtPassword]){
        [self.passContainer addBorderColor:forcus withHeight:forcusHeigh];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self updateUIDone];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.txtUserName) {
        if (![string isEqualToString:@""]) {
            textField.text = [CoreStringUtils formatPhoneNumber:textField.text deleteLastChar:NO];
        }
    } else if (textField == self.txtPassword) {
        if (textField.text.length == 1 && [CoreStringUtils isEmpty:string]) {
            [self.btnShowPass setHidden:YES];
        } else {
            [self.btnShowPass setHidden:NO];
        }
    }
    return YES;
}

#pragma mark - IBAction methods
- (void)signUpAccount {
    if ([self.delegate respondsToSelector:@selector(btnSignUpPressed)]) {
        [self.delegate btnSignUpPressed];
    }
}

- (IBAction)showPassword:(id)sender {
    if (!self.txtPassword.secureTextEntry){
        self.txtPassword.secureTextEntry = YES;
        [self.btnShowPass setTitle:@"SHOW" forState:UIControlStateNormal];
    } else {
        self.txtPassword.secureTextEntry = NO;
        [self.btnShowPass setTitle:@"HIDE" forState:UIControlStateNormal];
    }
    [self.txtPassword becomeFirstResponder];
}

- (IBAction)btnForgotPaswordPressed:(id)sender {
    [self lockAllButton:YES];
    if ([self.delegate respondsToSelector:@selector(btnForgetPaswordPressed)]) {
        [self.delegate btnForgetPaswordPressed];
    }
}

- (IBAction)btnLoginPressed:(id)sender {
    [self lockAllButton:YES];
    if ([self.delegate respondsToSelector:@selector(btnLoginPressed)]) {
        [self.delegate btnLoginPressed];
    }
}

- (IBAction)btnCountryCodePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(chooseCountryCode)]) {
        [self.delegate chooseCountryCode];
    }
}

- (IBAction)btnFBLoginPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(btnFBLoginPressed)]) {
        [self.delegate btnFBLoginPressed];
    }
}

- (IBAction)btnGGLoginPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(btnGGLoginPressed)]) {
        [self.delegate btnGGLoginPressed];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        
    } else if(buttonIndex == 1) {
        if (![CoreStringUtils isEmpty:self.txtUserName.text]) {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        }
    }
}
@end

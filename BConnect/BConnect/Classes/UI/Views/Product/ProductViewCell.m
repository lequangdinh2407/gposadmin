//
//  ProductViewCell.m
//  BConnect
//
//  Created by Dragon on 2/21/17.
//  Copyright © 2017 DKMobility. All rights reserved.
//

//
//  ProductViewCell.m
//  luvkonect
//
//  Created by Dragon on 6/5/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "ProductViewCell.h"
//#import "DistributorEntity.h"

#define kDescContentHeightDefault 90

@implementation ProductViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bg.layer.cornerRadius = 4;
    
    [self.shadowView.layer setMasksToBounds:NO];
    self.shadowView.layer.cornerRadius = 4;
    self.shadowView.layer.borderWidth = 0.5f/[UIScreen mainScreen].scale;
    self.shadowView.layer.borderColor = APP_BACKGROUND_COLOR.CGColor;
    self.shadowView.layer.shadowColor = APP_TEXT_COLOR.CGColor;
    self.shadowView.layer.shadowOffset = CGSizeMake(0.4, 0.45);
    self.shadowView.layer.shadowOpacity = 0.35;
    self.shadowView.layer.shadowRadius = 0.95;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(0, 40)];
    [path addLineToPoint:CGPointMake(40, 0)];
    [path closePath];
    
    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.frame = self.discountView.bounds;
    pathLayer.path = path.CGPath;
    
    pathLayer.strokeColor = [[UIColor colorWithRed:255.0/255.0 green:140.0/255.0 blue:3.0/255.0 alpha:1.0] CGColor];
    pathLayer.fillColor = [[UIColor colorWithRed:255.0/255.0 green:140.0/255.0 blue:3.0/255.0 alpha:1.0] CGColor];
    
    pathLayer.fillRule=kCAFillRuleEvenOdd;
    [self.discountView.layer setMask:pathLayer];
    [self.discountView.layer setMasksToBounds:YES];
}

- (void)drawRect:(CGRect)rect{
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

- (CGSize)calculateDescContentSize:(UILabel*)label{
    CGSize maximumLabelSize = CGSizeMake(9999, 21);
    CGRect textRect = [label.text boundingRectWithSize:maximumLabelSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName:label.font}
                                               context:nil];
    return textRect.size;
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    return layoutAttributes;
}

-(void)prepareForReuse{
    [super prepareForReuse];
}

/*
- (void)loadPromotionInfo:(SKUEntity *)data{
    
    [self setBorderRect:self.bounds];
    self.rateContainer.layer.cornerRadius = 9;
    
    self.entity = data;
    
    CGFloat nameHeight = MIN([Helpers calculateHeightText:self.entity.name width:kItemWidth font:[UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:15.0f]].height + 2, 65);
    self.lbNameHeighConstraint.constant = nameHeight;
    
    self.lbCartNumber.text = self.entity.strSkuOrderCount;
    self.lbLikeNumber.text = self.entity.strSkuWishListCount;
    self.lbServiceName.text = self.entity.name;
    self.lbOriginalPrice.attributedText = self.entity.attributeRetailPrice;
    self.lbDiscount.attributedText = self.entity.attributeSalePrice;
    self.lbDistributor.text = self.entity.distributor.catName;
    
    self.lbDistributor.layer.mask = nil;
    
    CGFloat lbDistributorWith = ([Helpers calculateWidthText:self.lbDistributor.text height:21 font:[UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:13.0f]]).width + 8;
    self.lbDistributorWidthConstraint.constant = lbDistributorWith;
    
    CGRect bounds = self.lbDistributor.bounds;
    bounds.size.width = lbDistributorWith;
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:bounds
                                                   byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft)
                                                         cornerRadii:CGSizeMake(30.0f, 30.0f)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = bounds;
    maskLayer.path = maskPath.CGPath;
    self.lbDistributor.layer.mask = maskLayer;
    self.lbDistributor.layer.masksToBounds = YES;
    
    CGFloat cartWidth = MIN([self calculateDescContentSize:self.lbCartNumber].width + 1, (CGRectGetWidth(self.bounds)/2 - 25));
    //    CGFloat likeFieldWidth = [self calculateDescContentSize:self.lbLikeNumber].width + 31;
    //    likeFieldWidth = MIN(CGRectGetWidth(self.bounds), likeFieldWidth);
    if (self.entity.isProductSaleOff) {
        self.discountView.hidden = NO;
        self.lbDiscountOnTop.hidden = NO;
        self.descHeightContraint.constant = kDescContentHeightDefault;
        self.originalPriceHeighContraint.constant = 21;
        int SavingPercent = (int)[self.entity.strSavingPercent doubleValue];
        self.lbDiscountOnTop.text = [NSString stringWithFormat:@"%d%%", SavingPercent];
    }else{
        self.descHeightContraint.constant = kDescContentHeightDefault - 21;
        self.originalPriceHeighContraint.constant = 0;
        self.discountView.hidden = YES;
        self.lbDiscountOnTop.hidden = YES;
    }
    
    self.cartNumberWidthContraint.constant = cartWidth;
    //    self.productLikeWidthContraint.constant = likeFieldWidth;
    [self loadImage:self.promotionThumb placeHolder:[UIImage imageNamed:@"ic_tradeshow_logo"] withUrl:self.entity.primaryPhoto isScrolling:self.isScrolling];
    
    self.promotionThumb.layer.cornerRadius = 3;
    
    [self.lbDiscountOnTop setTransform:CGAffineTransformMakeRotation(-M_PI / 4)];
    
    [self layoutIfNeeded];
}
*/

- (void)setBorderRect:(CGRect)rect {
}

@end


//
//  AddCartPopupView.h
//  Cyogel
//
//  Created by Dragon on 12/16/15.
//  Copyright © 2015 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class ProductEntity;
@class ProductOptionValueEntity;

@protocol ProductOptionsPopupDelegate

- (void)productOtionPopUpSelected:(NSInteger)index andType:(int)type;

@end

@interface ProductOptionsPopupView : UIView<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) id delegate;
@property (assign, nonatomic) int type;
//@property (strong, nonatomic) ProductEntity* entity;
@property (strong, nonatomic) ProductOptionValueEntity* selectedOption;
@property (weak, nonatomic) IBOutlet UILabel *lbTitile;

@end

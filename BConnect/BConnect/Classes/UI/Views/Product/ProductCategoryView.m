//
//  FeatureView.m
//  Loop
//
//  Created by vu van long on 9/23/15.
//  Copyright © 2015 DKMobility. All rights reserved.
//

#import "ProductCategoryView.h"
#import "StoreEntity.h"
#import "SearchUtils.h"

@interface ProductCategoryView() {
    BOOL isLoadedCategories;
    NSMutableDictionary* selectedDistMap;
   
}

@end

@implementation ProductCategoryView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ProductCategoryView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;

        UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(tableCategorySwipeGestureCaptured:)];
        [swipeGesture setDirection:UISwipeGestureRecognizerDirectionRight];
        [self.tableView addGestureRecognizer:swipeGesture];
        
        self.listCategories = [[NSMutableArray alloc] init];
      
        [self makeHeaderViewCategoryTable];
        
        self.lbFilter.text = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_STORENAME];
    }
    return self;
}

- (void)makeHeaderViewCategoryTable {
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kListContainerWidth, CATEGORY_HEADER_TABLE_HEIGHT + 40)];
    [headerView setBackgroundColor:[UIColor blackColor]];
    
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, CATEGORY_HEADER_TABLE_HEIGHT - 40, 30, 30)];
    imageView.image = [UIImage imageNamed:@"ic_store"];
    
    self.lbFilter = [[UILabel alloc] initWithFrame:CGRectMake(60, CATEGORY_HEADER_TABLE_HEIGHT - 35, kListContainerWidth - 60, 25)];
    [self.lbFilter setTextColor:[UIColor whiteColor]];
    [self.lbFilter setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Bold" size:16.0f]];
    
    UIView* bottomLine = [[UIView alloc] initWithFrame:CGRectMake(10, CATEGORY_HEADER_TABLE_HEIGHT - 5,kListContainerWidth, 1)];
    [bottomLine setBackgroundColor:[UIColor lightGrayColor]];
    
    [headerView addSubview:imageView];
    [headerView addSubview:self.lbFilter];
    [headerView addSubview:bottomLine];
    
    UISearchBar* searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, CATEGORY_HEADER_TABLE_HEIGHT, kListContainerWidth, 40)];
    searchBar.backgroundColor = [UIColor clearColor];
    searchBar.tintColor = [UIColor clearColor];
    searchBar.barTintColor =  [UIColor clearColor];
    searchBar.barStyle = UISearchBarStyleMinimal;
    searchBar.delegate = self;
    
    [searchBar setImage:[UIImage imageNamed:@"btn_search"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    
    [searchBar setImage:[UIImage imageNamed:@"ic_close_search"] forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
    
    UIView* subView = [[searchBar subviews] lastObject];
    UIView* lastObj = [subView.subviews lastObject];
    if ([lastObj isKindOfClass:[UITextField class]]) {
        UITextField *searchTextField = (UITextField*)lastObj;
        if ([searchTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
            [searchTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: kSearchBarColor}]];
            [searchTextField setBackgroundColor:[UIColor clearColor]];
            [searchTextField setTextColor:[UIColor whiteColor]];
        }
    }
    
    [headerView addSubview:searchBar];
    
    self.tableView.tableHeaderView = headerView;
}

#pragma mark - Tableview methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isSearching) {
        return searchResults.count;
    }
    return self.listCategories.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CATEGORY_ROW_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"StoreMenuTableViewCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"StoreMenuTableViewCell"];
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    
    UILabel* lbCategoryName = (UILabel *)[cell viewWithTag:1];
    //[lbCategoryName setTextAlignment:NSTextAlignmentCenter];
    if (!lbCategoryName) {
        lbCategoryName = [[UILabel alloc] initWithFrame:CGRectMake(30, CATEGORY_ROW_HEIGHT / 2 - 12, tableView.frame.size.width - 45, 25)];
        lbCategoryName.tag = 1;
        [lbCategoryName setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:14.0]];
        [lbCategoryName setTextColor:[UIColor whiteColor]];
        [lbCategoryName setBackgroundColor:[UIColor clearColor]];
        [cell addSubview:lbCategoryName];
    }
    
    if (indexPath.row == 0 && indexPath.section == 0)
        [lbCategoryName setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Bold" size:14.0]];
    else
        [lbCategoryName setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:14.0]];
    
    NSString* name = @"";
    
    StoreEntity* entity;
    if (self.isSearching) {
        entity = [searchResults objectAtIndex:indexPath.row];
    } else {
        entity = [self.listCategories objectAtIndex:indexPath.row];
    }
    
    name = entity.name;
    
    
    lbCategoryName.text = [name uppercaseString];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
    StoreEntity* entity = [self.listCategories objectAtIndex:indexPath.row];
    self.lbFilter.text = entity.name;
    [self.tableView setContentOffset:CGPointZero animated:YES];
    
    [SharePrefUtils writeStringPreference:PREFKEY_CONFIG_API_URL prefValue:entity.apiUrl];
    [SharePrefUtils writeStringPreference:PREFKEY_CONFIG_USERNAME prefValue:entity.user];
    [SharePrefUtils writeStringPreference:PREFKEY_CONFIG_STORENAME prefValue:entity.name];
    [SharePrefUtils writeStringPreference:PREFKEY_CONFIG_PASSWORD prefValue:entity.password];
    [SharePrefUtils writeStringPreference:PREFKEY_CONFIG_STOREID prefValue:entity.storeId];
    [SharePrefUtils writeStringPreference:PREFKEY_CONFIG_STORE_ID prefValue:entity.Id];
    
    [RequestManager sharedClient].apiUrl = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_API_URL];
    [RequestManager sharedClient].username = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_USERNAME];
    [RequestManager sharedClient].password = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_PASSWORD];
    [RequestManager sharedClient].storeId = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_STOREID];
    [RequestManager sharedClient].Id = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_STORE_ID];
    
    if ([self.delegate respondsToSelector:@selector(dismissRightMenu)]) {
        [self.delegate dismissRightMenu];
    }
}

- (void)tableCategorySwipeGestureCaptured:(UITapGestureRecognizer *)gesture {
    [self dismissCategoryList];
}

- (void)dismissCategoryList{
    if (self.delegate && [self.delegate respondsToSelector:@selector(dismissRightMenu)]) {
        [self.delegate dismissRightMenu];
    }
}

#pragma mark - Searchbar method
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    //[self hiddenSearchBar];
    self.isSearching = NO;
    [self handleSearch:searchBar];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    //Handle input text here
    self.isSearching = YES;
    [self handleSearch:searchBar];
}

- (void)handleSearch:(UISearchBar *)searchBar {
    //check what was passed as the query String and get rid of the keyboard
   
    searchResults = [SearchUtils filter:searchBar.text originalList:[NSMutableArray arrayWithArray:self.listCategories]];
    [self.tableView reloadData];
    if (searchResults.count > 0) {
        //[self hideNoItemView];
    } else {
        //[self showNoItemView:[CoreStringUtils getStringRes:@"str_search_no_results"] btnTitle:@""];
    }
    
    [self.tableView reloadData];
}




@end

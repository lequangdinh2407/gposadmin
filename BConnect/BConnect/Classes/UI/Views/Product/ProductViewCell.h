//
//  ProductViewCell.h
//  BConnect
//
//  Created by Dragon on 2/21/17.
//  Copyright © 2017 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SKUEntity.h"

@protocol ProductViewCellDelegate

@end

@interface ProductViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *bg;
@property (weak, nonatomic) IBOutlet UIImageView *promotionThumb;
@property (weak, nonatomic) IBOutlet UIView *descView;
@property (weak, nonatomic) IBOutlet UILabel *lbDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lbServiceName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descHeightContraint;
@property (weak, nonatomic) IBOutlet UIView *rateContainer;
@property (weak, nonatomic) IBOutlet UIView *thumbContainer;
@property (weak, nonatomic) IBOutlet UILabel *lbOriginalPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbCartNumber;
@property (weak, nonatomic) IBOutlet UILabel *lbLikeNumber;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *originalPriceHeighContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cartNumberWidthContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productLikeWidthContraint;
//@property (weak, nonatomic) SKUEntity* entity;
@property (weak, nonatomic) id delegate;
@property (strong, nonatomic) NSIndexPath* index;
@property (assign, nonatomic) BOOL isUpdateDistance;
@property (assign, nonatomic) BOOL isScrolling;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbNameHeighConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lbDiscountOnTop;

@property (weak, nonatomic) IBOutlet UIView *discountView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbDistributorWidthConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lbDistributor;

//- (void)loadPromotionInfo:(SKUEntity*)data;

@end


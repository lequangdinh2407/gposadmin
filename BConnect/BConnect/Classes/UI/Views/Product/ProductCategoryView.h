//
//  FeatureView.h
//  Loop
//
//  Created by vu van long on 9/23/15.
//  Copyright © 2015 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryEntity.h"
#import "DistributorEntity.h"

@protocol ProductCategoryViewDelegate

- (void)dismissRightMenu;

@end

@interface ProductCategoryView : UIView<UISearchBarDelegate>{
    NSArray *searchResults;
}

@property (weak, nonatomic) IBOutlet UITableView* tableView;
@property (weak, nonatomic) id delegate;
@property (strong, nonatomic) NSMutableArray* listCategories;
@property (strong, nonatomic) NSMutableArray* distributors;
@property (strong, nonatomic) CategoryEntity* selectedCategory;
@property (assign, nonatomic) BOOL isSearching;

@property (strong, nonatomic) UILabel* lbFilter;

@end

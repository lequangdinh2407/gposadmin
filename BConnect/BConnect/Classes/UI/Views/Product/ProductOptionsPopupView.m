//
//  AddCartPopupView.m
//  Cyogel
//
//  Created by Dragon on 12/16/15.
//  Copyright © 2015 DKMobility. All rights reserved.
//

#import "ProductOptionsPopupView.h"
//#import "ProductEntity.h"
#import "ProductOptionEntity.h"
#import "ProductOptionValueEntity.h"
#import "ShippingOptionEntity.h"
#import "CoreStringUtils.h"

@implementation ProductOptionsPopupView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ProductOptionsPopupView" owner:self options:nil];
        [self.tableView registerNib:[UINib nibWithNibName:@"OptionsTableViewCell" bundle:nil] forCellReuseIdentifier:@"OptionsTableViewCell"];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        //_tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        //_tableView.separatorColor = [UIColor blackColor];
    }
    return self;
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

#pragma mark - TableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.type == 2) self.lbTitile.text = @"Colors";
    else self.lbTitile.text = @"Sizes";
    
    /*
    if (self.type == 2)
        if (self.entity.productColorOptions.count > 0) {
            ProductOptionEntity* option = self.entity.productColorOptions[0];
            return option.productOptionValues.count;
        }
    
    if (self.type == 1)
        if (self.entity.productSizeOptions.count > 0) {
            ProductOptionEntity* option = self.entity.productSizeOptions[0];
            return option.productOptionValues.count;
        }    return 0;
     */
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.type == 1)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"optionTableViewCell"];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"optionTableViewCell"];
        }
        /*
        ProductOptionEntity* option = self.entity.productSizeOptions[0];
        ProductOptionValueEntity* optionValue = option.productOptionValues[indexPath.row];
        cell.textLabel.text = optionValue.name;
         */
        return cell;
    }
    
//    if (self.type == 2)
//    {
//        OptionsTableViewCell *cell = (OptionsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"OptionsTableViewCell"];
//        
//        if (cell == nil)
//        {
//            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OptionsTableViewCell" owner:self options:nil];
//            cell = [nib objectAtIndex:0];
//        }
//        
//        ProductOptionEntity* option = self.entity.productColorOptions[0];
//        ProductOptionValueEntity* optionValue = option.productOptionValues[indexPath.row];
//        
//        if (![CoreStringUtils isEmpty: optionValue.value])
//        {
//            UIColor *color = [self colorFromHexString:optionValue.value];
//            cell.thumb.backgroundColor = color;
//        }
//        else [Helpers loadImage:cell.thumb placeHolder:[UIImage imageNamed:@"ic_tradeshow_logo"] withUrl:optionValue.photo];
//        cell.lbName.text = optionValue.name;
//        cell.delegate = self;
//        return cell;
//    }
    return  nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(productOtionPopUpSelected:andType:)]) {
            [self.delegate productOtionPopUpSelected:indexPath.row andType:self.type];
    }
}

@end

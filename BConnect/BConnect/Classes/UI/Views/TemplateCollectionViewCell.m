//
//  TemplateCollectionViewCell.m
//  spabees
//
//  Created by vu van long on 1/6/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "TemplateCollectionViewCell.h"

@implementation TemplateCollectionViewCell

- (void)awakeFromNib {
    self.imgTemplateThumb.image = [UIImage imageNamed:@"ic_vertical_loading"];
    self.imgTemplateThumb.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)loadAsset:(ALAsset*)asset {
    self.asset = asset;
    self.imgTemplateThumb.image = [UIImage imageWithCGImage:self.asset.aspectRatioThumbnail];
}

- (void)setTemplateSelected:(BOOL)isSelected {
    [self.viewCheckerContainer setHidden:!isSelected];
    self.isSelected = isSelected;
}

- (IBAction)btnDeletePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(deleteTemplate:asset:)]) {
        [self setTemplateSelected:NO];
        [self.delegate deleteTemplate:self.index asset:self.asset] ;
    }
}

@end
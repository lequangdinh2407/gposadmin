//
//  SupportView.h
//  luvkonect
//
//  Created by Dragon on 6/8/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAMTextView.h"
#import "JVFloatLabeledTextField.h"
#import "CountryEntityV2.h"

#define kInforHeightDefault 150
#define CHARACTER_LIMITED   500

@protocol SupportViewDelegate

- (void)submitFeedback;
- (void)showCountryList;
- (void)requestContact;

@end

@interface SupportView : UIView<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet SAMTextView *tvFeedback;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UILabel *lbTextReaming;
@property (weak, nonatomic) IBOutlet UIView *feebackContainer;
@property (weak, nonatomic) IBOutlet UIView *inforContainer;
@property (weak, nonatomic) IBOutlet UIView *nameContainer;
@property (weak, nonatomic) IBOutlet UIView *emailContainer;
@property (weak, nonatomic) IBOutlet UIView *phoneContainer;
@property (weak, nonatomic) IBOutlet UIView *countryContainer;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtEmail;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtPhone;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtCountry;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inforHeighConstraint;
@property (strong, nonatomic) CountryEntityV2* countryEnitySelected;
@property (weak, nonatomic) id delegate;

- (IBAction)btnSubmitPressed:(id)sender;
- (IBAction)btnCountryPressed:(id)sender;
- (void)setCountrySelected:(CountryEntityV2*)entity;
- (void)prepareLayout;
@end

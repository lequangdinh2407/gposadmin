//
//  PhotoPickerView.h
//  luvkonect
//
//  Created by Dragon on 5/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PhotoPickerViewDelegate

- (void)btnGalleryPressed;
- (void)btnCameraPressed;

@end

@interface PhotoPickerView : UIView

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containWidthConstraint;
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UIButton *btnGallery;
@property (weak, nonatomic) id delegate;

- (IBAction)btnGalleryPressed:(id)sender;
- (IBAction)btnCameraPressed:(id)sender;
- (void)dismissView;
@end

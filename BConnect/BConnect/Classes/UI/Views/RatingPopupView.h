//
//  RatingPopupView.h
//  BConnect
//
//  Created by Dragon on 2/18/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"

@protocol RatingPopupViewDelegate
- (void)submitRatingProduct;
- (void)cancelRatingProduct;
@end

@interface RatingPopupView : UIView<RateViewDelegate>
@property (weak, nonatomic) IBOutlet RateView *starRatingView;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) id delegate;

- (IBAction)btnSubmitPressed:(id)sender;
- (IBAction)btnCancelPressed:(id)sender;
@end

//
//  NoItemView.h
//  BConnect
//
//  Created by ToanPham on 1/7/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAMTextView.h"

@protocol NoItemViewDelegate

- (void)btnClickedNoItemView;

@end

@interface NoItemView : UIView

@property (weak, nonatomic) IBOutlet UIButton *btnTry;
//@property (weak, nonatomic) IBOutlet UILabel *lbTry;
@property (weak, nonatomic) IBOutlet UILabel *lbNotification;
@property (weak, nonatomic) IBOutlet UIImageView *imgEmpty;
@property (weak, nonatomic) id noItemDelegate;

- (IBAction)btnPressed:(id)sender;
- (void)loadText:(NSString*)notificationText btnTitle:(NSString*)btnTitle;

@end
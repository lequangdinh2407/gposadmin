//
//  SelectCategoryTableViewCell.h
//  BConnect
//
//  Created by Inglab on 12/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSCheckBoxView.h"
#import "SelectCategoryEntity.h"
#import "GlobalStoreEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface SelectCategoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (strong, nonatomic) SSCheckBoxView* cbv;
@property (strong, nonatomic) SelectCategoryEntity* entity;
@property (strong, nonatomic) GlobalStoreEntity* storeEntity;

- (void)loadCellInfo:(SelectCategoryEntity*)entity;
- (void)loadStoreCellInfo:(GlobalStoreEntity*)entity;

@end

NS_ASSUME_NONNULL_END

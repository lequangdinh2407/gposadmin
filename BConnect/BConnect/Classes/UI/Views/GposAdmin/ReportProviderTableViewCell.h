//
//  ReportProviderTableViewCell.h
//  BConnect
//
//  Created by Inglab on 06/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProviderReportEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportProviderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbSale;
@property (weak, nonatomic) IBOutlet UILabel *lbTips;
@property (weak, nonatomic) IBOutlet UILabel *lbTotalWorkHours;
@property (weak, nonatomic) IBOutlet UILabel *lbStandardCost;

@property (weak, nonatomic) IBOutlet UILabel *lbCashCollected;

- (void)loadCellInfo:(ProviderReportEntity*)entity;

@end

NS_ASSUME_NONNULL_END

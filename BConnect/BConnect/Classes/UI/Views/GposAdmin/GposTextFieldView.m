//
//  GposTextFieldView.m
//  BConnect
//
//  Created by Inglab on 25/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "GposTextFieldView.h"
#import "Helpers.h"

@implementation GposTextFieldView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        
        [self addSubview:self.view];
        [self stretchToSuperView:self.view];
        
        [Helpers giveBorderWithCornerRadious:self.textField radius:5 borderColor:[UIColor clearColor] andBorderWidth:0.0];
        
        return self;
    }
     
    return nil;
     
}

- (void)stretchToSuperView:(UIView*) view {
    view.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *bindings = NSDictionaryOfVariableBindings(view);
    NSString *formatTemplate = @"%@:|[view]|";
    for (NSString * axis in @[@"H",@"V"]) {
        NSString * format = [NSString stringWithFormat:formatTemplate,axis];
        NSArray * constraints = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:bindings];
        [view.superview addConstraints:constraints];
    }
}

/*
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        self.view.bounds = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 100 , 60);
        self.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 100 , 60);
        [Helpers giveBorderWithCornerRadious:self.textField radius:5 borderColor:[UIColor whiteColor] andBorderWidth:2.0];
        [self addSubview:self.view];
        [self.view sizeToFit];
        
        return self;
    }
    return nil;
}
*/


-(void)setName:(NSString*)name {
    self.lbName.text = name;
    [Helpers giveBorderWithCornerRadious:self.textField radius:5 borderColor:[UIColor whiteColor] andBorderWidth:2.0];
}

-(void)setValue:(NSString*)value {
    self.textField.text = value;
    
}

-(void)setDurationKeyboard {
    [self.textField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [self.textField reloadInputViews];
}

-(void)setNumberAndPunctuationKeyboard {
    [self.textField setKeyboardType:UIKeyboardTypeDecimalPad];
    [self.textField reloadInputViews];
}

-(void)setPhoneKeyboard {
    [self.textField setKeyboardType:UIKeyboardTypePhonePad];
    [self.textField reloadInputViews];
}

-(void)setEmailKeyboard {
    [self.textField setKeyboardType:UIKeyboardTypeEmailAddress];
    [self.textField reloadInputViews];
}


-(void)setNumberKeyboard {
    [self.textField setKeyboardType:UIKeyboardTypeNumberPad];
    [self.textField reloadInputViews];
}

-(void)setPasswordTextField {
    self.textField.secureTextEntry = true;
}

@end

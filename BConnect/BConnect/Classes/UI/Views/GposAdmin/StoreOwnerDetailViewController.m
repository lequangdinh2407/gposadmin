//
//  StoreOwnerDetailViewController.m
//  BConnect
//
//  Created by Inglab on 11/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "StoreOwnerDetailViewController.h"
#import "SelectStoreViewController.h"
#import "AccountDetailViewController.h"

@interface StoreOwnerDetailViewController ()

@end

@implementation StoreOwnerDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareLayout];
    [self loadData:self.entity];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupHeader];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!isInit) {
        [self.txtStore setName:@"Store"];
        isInit = true;
    }
}

- (void)setupHeader{
    [super setupHeader];
    [self SetHeaderTitle:@"Store Owner"];
    [self SetBackTitle:[CoreStringUtils getStringRes:@""] target:self action:@selector(btnBackPressed:)];
    [self setRightItemWithImage:[UIImage imageNamed:@"ic_account"] target:self action:@selector(gotoAccount)];
}
    
-(void)gotoAccount {
    AccountDetailViewController* controller = [userProfileStoryboard instantiateViewControllerWithIdentifier:@"AccountDetailViewController"];
    controller.entity = self.entity;
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

-(void)prepareLayout {
    
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
    
}

-(void)loadData:(StoreOwnerEntity*)entity {
    //self.entity = entity;
    self.lbFirstName.text = entity.firstName;
    self.lbLastName.text = entity.lastName;
    self.lbEmail.text = entity.email;
    self.lbPhone.text = entity.phone;
    [self.switchActive setOn:entity.active];
    
    NSString* storeDisplayText = @"";
    for (int i = 0; i < entity.storeIdsList.count; i++) {
        if (i == 0) {
            storeDisplayText = [NSString stringWithFormat:@"#%@", [entity.storeIdsList objectAtIndex:i]];
        } else {
            storeDisplayText = [NSString stringWithFormat:@"%@,#%@", storeDisplayText, (NSString*)[entity.storeIdsList objectAtIndex:i]];
        }
    }
    self.txtStore.textField.text = storeDisplayText;
}

- (IBAction)btnSavePressed:(id)sender {
    
    [self showProgressHub];
    [[RequestManager sharedClient] requestEditStoreOwner:self.entity.Id firstName:self.entity.firstName lastName:self.entity.lastName email:self.entity.email phone:self.entity.phone password:@"" active:self.switchActive.isOn storeIds:self.entity.storeIdsList  success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestEditStoreOwner success" message:results];
        
        self.entity.active = self.switchActive.isOn;
        
        if ([self.delegate respondsToSelector:@selector(updateStoreOwner:)]) {
            [self.delegate updateStoreOwner:self.entity];
        }
        
        [self dismissProgressHub];
        [self btnBackPressed:nil];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestEditStoreOwner failure" message:results];
        [self dismissProgressHub];
    }];
    
}

- (IBAction)btnChooseStorePressed:(id)sender {
    SelectStoreViewController* controller = [moreStoryboard instantiateViewControllerWithIdentifier:@"SelectStoreViewController"];
    controller.storeIdsList = self.entity.storeIdsList;
    controller.delegate = self;
    [bconnectAppDelegate.rootNavigation pushViewController:controller animated:YES];
}

- (void)reassignStoreList:(NSMutableArray*)storeList {
    self.entity.storeIdsList = storeList;
    [self loadData:self.entity];
}

- (void)updateStoreOwner:(StoreOwnerEntity*)entity {
    self.lbFirstName.text = entity.firstName;
    self.lbLastName.text = entity.lastName;
    self.lbEmail.text = entity.email;
    self.lbPhone.text = entity.phone;
    
    if ([self.delegate respondsToSelector:@selector(updateStoreOwner:)]) {
        [self.delegate updateStoreOwner:entity];
    }
}

@end

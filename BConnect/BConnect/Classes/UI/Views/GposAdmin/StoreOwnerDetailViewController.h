//
//  StoreOwnerDetailViewController.h
//  BConnect
//
//  Created by Inglab on 11/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "BaseViewViewController.h"
#import "GposTextFieldView.h"
#import "StoreOwnerEntity.h"

@protocol StoreOwnerDetailViewControllerDelegate

- (void)updateStoreOwner:(StoreOwnerEntity*)entity;

@end

@interface StoreOwnerDetailViewController : BaseViewViewController

@property (weak, nonatomic) IBOutlet UILabel *lbFirstName;
@property (weak, nonatomic) IBOutlet UILabel *lbLastName;
@property (weak, nonatomic) IBOutlet UILabel *lbEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbPhone;
@property (weak, nonatomic) IBOutlet UISwitch *switchActive;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtStore;

@property (strong, nonatomic) StoreOwnerEntity* entity;

@property (nonatomic) id delegate;

@end



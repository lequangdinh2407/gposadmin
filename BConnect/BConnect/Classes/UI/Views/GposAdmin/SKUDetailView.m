//
//  SKUDetailView.m
//  BConnect
//
//  Created by Inglab on 03/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "SKUDetailView.h"
#import "Helpers.h"

@implementation SKUDetailView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SKUDetailView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        [self layoutIfNeeded];
    }
    return self;
}

- (void)prepareLayout{
    [self.txtName setName:@"Name"];
    [self.txtPrice setName:@"Price"];
    [self.txtDuration setName:@"Duration"];
    [self.txtSupplyCost setName:@"Suppy Cost"];
    [self.txtMinQuantity setName:@"Min Quantity"];
    [self.txtMaxQuantity setName:@"Max Quantity"];
    [self.txtSaleTaxRate setName:@"Sale Tax Rate"];
    [self.txtMinQuantity setNumberKeyboard];
    [self.txtMaxQuantity setNumberKeyboard];
    [self.txtSupplyCost setNumberAndPunctuationKeyboard];
    [self.txtDuration setDurationKeyboard];
    [self.txtPrice setNumberAndPunctuationKeyboard];
    
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
}

- (IBAction)btnSavePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(handleButtonSavePressed)]) {
        [self.delegate handleButtonSavePressed];
    }
}

-(void)loadData:(ListProductEntity*)entity {
    [self.txtName setValue:entity.Name];
    [self.txtPrice setValue:[NSString stringWithFormat:@"%0.2f", entity.Price]];
    [self.txtDuration setValue:entity.Duration];
    [self.txtSupplyCost setValue:[NSString stringWithFormat:@"%0.2f", entity.StandardCost]];
    [self.txtSaleTaxRate setValue:[NSString stringWithFormat:@"%0.2f", entity.SaleTaxRate]];
    [self.txtMinQuantity setValue:[NSString stringWithFormat:@"%d", entity.MinQuantity]];
    [self.txtMaxQuantity setValue:[NSString stringWithFormat:@"%d", entity.MaxQuantity]];
    [self.SwitchShowOnApp setOn:entity.ShowOnApp];
    
    NSString* baseUrl = [NSString stringWithFormat:@"https://www.gposdev.com/%@/images", [RequestManager sharedClient].storeId];
    
    NSString *uploadedUrl = [NSString stringWithFormat:@"%@/%@", baseUrl, entity.ImageUri];
    [Helpers loadImage:self.profileImageView placeHolder:[UIImage imageNamed:@"background-qrcode"] withUrl:uploadedUrl];
    
}

- (IBAction)btnSaleTaxRatePressed:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(handleButtonSaleTaxRatePressed)]) {
        [self.delegate handleButtonSaleTaxRatePressed];
    }
    
}

-(void)setSaleTaxRate:(double)rate {
    [self.txtSaleTaxRate setValue:[NSString stringWithFormat:@"%0.2f", rate]];
}

- (IBAction)btnUploadImagePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(handleButtonUploadImagePressed)]) {
        [self.delegate handleButtonUploadImagePressed];
    }
}

- (IBAction)switchShowOnAppValueChanged:(id)sender {
    
}



@end

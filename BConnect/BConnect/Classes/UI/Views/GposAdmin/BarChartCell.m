//
//  HomeFourTableViewCell.m
//  RecordLife
//
//  Created by 谢俊逸 on 15/03/2017.
//  Copyright © 2017 谢俊逸. All rights reserved.
//

#import "BarChartCell.h"
#import "XJYChart.h"
@interface BarChartCell ()<XJYChartDelegate>

@end

@implementation BarChartCell

- (void)awakeFromNib {
  [super awakeFromNib];
  // Initialization code
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString*)reuseIdentifier {
  if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
      self.contentView.backgroundColor = [UIColor clearColor];
      self.backgroundColor = [UIColor clearColor];

  }
  return self;
}

-(void)loadData {
    _xValues = self.chartMonthsList;
    _values                            = self.valuesList;
    _barColors                        = @[[UIColor blueColor], [UIColor redColor], [UIColor whiteColor], [UIColor orangeColor], [UIColor purpleColor], [UIColor greenColor]];
    _currentBarColor                = 0;
    
    CGRect chartFrame                = CGRectMake(0.0,
                                                  0.0,
                                                  self.bounds.size.width * 0.9,
                                                  400);
    _chart                            = [[SimpleBarChart alloc] initWithFrame:chartFrame];
    _chart.center                    = CGPointMake(self.frame.size.width / 2.0, chartFrame.size.height / 2.0);
    _chart.delegate                    = self;
    _chart.dataSource                = self;
    _chart.barShadowOffset            = CGSizeMake(2.0, 1.0);
    _chart.animationDuration        = 1.0;
    _chart.barShadowColor            = [UIColor grayColor];
    _chart.barShadowAlpha            = 0.5;
    _chart.barShadowRadius            = 1.0;
    _chart.barWidth                    = 15.0;
    _chart.xLabelType                = SimpleBarChartXLabelTypeVerticle;
    _chart.incrementValue            = (int)([Helpers newMaximumNumber:self.maximumValue]/10);;
    _chart.barTextType                = SimpleBarChartBarTextTypeTop;
    _chart.barTextColor                = [UIColor whiteColor];
    _chart.gridColor                = [[UIColor lightGrayColor] colorWithAlphaComponent:0.2];
    
    [self addSubview:_chart];
    
    [_chart reloadData];
    
}

#pragma mark SimpleBarChartDataSource

- (NSUInteger)numberOfBarsInBarChart:(SimpleBarChart *)barChart
{
    return _values.count;
}

- (CGFloat)barChart:(SimpleBarChart *)barChart valueForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] floatValue];
}

- (NSString *)barChart:(SimpleBarChart *)barChart textForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] stringValue];
}

- (NSString *)barChart:(SimpleBarChart *)barChart xLabelForBarAtIndex:(NSUInteger)index
{
    
    return [_xValues objectAtIndex:index];
}

- (UIColor *)barChart:(SimpleBarChart *)barChart colorForBarAtIndex:(NSUInteger)index
{
    return [_barColors objectAtIndex:_currentBarColor];
}



#pragma mark XBarChartDelegate

- (void)userClickedOnBarAtIndex:(NSInteger)idx {
  NSLog(@"XBarChartDelegate touch Bat At idx %lu", idx);
}
@end

//
//  SchedulePopupView.h
//  BConnect
//
//  Created by Inglab on 29/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SchedulePopupViewDelegate
- (void)showTimePicker:(int)pickerId start:(NSString*)start end:(NSString*)end;
- (void)saveTime:(NSString*)start end:(NSString*)end;
@end

@interface SchedulePopupView : UIView

@property (weak, nonatomic) id delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnFromTime;
@property (weak, nonatomic) IBOutlet UIButton *btnToTime;
@property (strong, nonatomic) NSString* start;
@property (strong, nonatomic) NSString* end;


-(void)setFromTitle:(NSString*)title;
-(void)setToTitle:(NSString*)title;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@end


//
//  StoreTableViewCell.m
//  BConnect
//
//  Created by Inglab on 30/12/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "StoreTableViewCell.h"

@implementation StoreTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"StoreTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)loadCellInfo:(GlobalStoreEntity*)entity {
    self.lbName.text = entity.name;
    self.lbUser.text = entity.user;
    self.lbPhone.text = entity.phone;
    self.lbStoreId.text = entity.storeId;
    self.lbDevice.text = entity.device;
    self.lbPassword.text = entity.password;
    self.lbStoreCode.text = entity.shopCode;
    self.lbActive.text = entity.active ? @"True" : @"False";
    self.lbCoordinate.text = [NSString stringWithFormat:@"[%0.2f, %0.2f]", entity.coordinateLat, entity.coordinateLon];
    
//    self.lbOrderDay.text = entity.OrderDate;
//    self.lbTips.text = [NSString stringWithFormat:@"%f", entity.Tips];
//    self.lbCreatedBy.text = entity.CreatedBy;
//    self.lbGrandTotal.text = [NSString stringWithFormat:@"%f", entity.GrandTotal];
    
    
}

@end

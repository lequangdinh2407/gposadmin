//
//  WorkingTimeTableViewCell.m
//  BConnect
//
//  Created by Inglab on 28/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "WorkingTimeTableViewCell.h"

@implementation WorkingTimeTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"WorkingTimeTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)loadCellInfo:(BussinessHourEntity*)entity {
    self.bussinessHourEntityEntity = entity;
    self.lbName.text = entity.day;
    self.lbTime.text = [NSString stringWithFormat:@"%@-%@", entity.start, entity.end];
    [self.switchIsOpen setOn:entity.isOpen];
}

- (void)loadWorkingDayCellInfo:(WorkingDayEntity*)entity {
    self.entity = entity;
    self.lbName.text = entity.Day;
    self.lbTime.text = [NSString stringWithFormat:@"%@-%@", entity.Start, entity.End];
    [self.switchIsOpen setOn:true];
}

- (IBAction)switchIsOpenChanged:(id)sender {
    self.bussinessHourEntityEntity.isOpen = self.switchIsOpen.isOn;
}

@end

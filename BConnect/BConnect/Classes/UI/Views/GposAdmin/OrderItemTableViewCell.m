//
//  OrderItemTableViewCell.m
//  BConnect
//
//  Created by Inglab on 09/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "OrderItemTableViewCell.h"

@implementation OrderItemTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"OrderItemTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)loadCellInfo:(OrderItemEntity*)entity {
    self.lbItemName.text = entity.Name;
    self.lbOrderNumber.text = entity.OrderNumber;
    self.lbDiscount.text = [NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, entity.Discount];
    self.lbGrandTotal.text = [NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, entity.GrandTotal];
    self.lbTax.text = [NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, entity.SaleTax];
    self.lbSubTotal.text = [NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, entity.SubTotalBeforeTax];
    self.lbTip.text = [NSString stringWithFormat:@"%@%0.2f", DEFAULT_CURRENCY, entity.Tip];
    self.lbTime.text = entity.CreatedOn;

}

@end

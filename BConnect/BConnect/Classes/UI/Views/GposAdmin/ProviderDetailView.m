//
//  ProviderDetailView.m
//  BConnect
//
//  Created by Inglab on 08/12/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ProviderDetailView.h"

@implementation ProviderDetailView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ProviderDetailView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        
        
        [self layoutIfNeeded];
    }
    return self;
}

-(void)prepareLayout {
    
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
    
    [self.txtNotificationKey setName:@"Notification Key"];
    [self.txtName setName:@"Name"];
    [self.txtPhone setName:@"Phone"];
    [self.txtCommissionRate setName:@"Commission Rate"];
    [self.txtPasscode setName:@"PassCode"];
    [self.txtPhone setPhoneKeyboard];
    [self.txtCommissionRate setNumberAndPunctuationKeyboard];
    self.txtNotificationKey.textField.delegate = self;
    self.txtName.textField.delegate = self;
    self.txtPhone.textField.delegate = self;
    self.txtCommissionRate.textField.delegate = self;
    self.txtPasscode.textField.delegate = self;

}

-(void)loadData {
    if (self.entity) {
        [self.txtNotificationKey setValue:self.entity.NotificationKey];
        [self.txtName setValue:self.entity.Name];
        [self.txtPhone setValue:self.entity.Phone];
        [self.txtCommissionRate setValue:[NSString stringWithFormat:@"%d", self.entity.commissionRate]];
        [self.txtPasscode setValue:self.entity.PassCode];
        [self.txtPasscode setValue:self.entity.PassCode];
        [self.switchActive setOn:self.entity.active animated:false];
    }
}

#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.txtNotificationKey.textField]) {
        [self.txtNotificationKey.textField resignFirstResponder];
        [self.txtName.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtName.textField]) {
        [self.txtName.textField resignFirstResponder];
        [self.txtPhone.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtPhone.textField]) {
        [self.txtPhone.textField resignFirstResponder];
        [self.txtCommissionRate.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtCommissionRate.textField]) {
        [self.txtCommissionRate.textField resignFirstResponder];
        [self.txtPasscode.textField becomeFirstResponder];
    }else {
        [self endEditing:YES];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}


- (IBAction)btnSavePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(handleButtonSavePressed)]) {
        [self.delegate handleButtonSavePressed];
    }
}

@end

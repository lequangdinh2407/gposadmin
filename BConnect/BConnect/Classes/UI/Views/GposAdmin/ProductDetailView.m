//
//  ProductDetailView.m
//  BConnect
//
//  Created by Inglab on 12/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ProductDetailView.h"

@implementation ProductDetailView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ProductDetailView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        
        
        [self layoutIfNeeded];
    }
    return self;
}

- (void)prepareLayout{
   
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
    
    [Helpers giveBorderWithCornerRadious:self.txtName radius:5 borderColor:[UIColor whiteColor] andBorderWidth:2.0];
    
    [Helpers giveBorderWithCornerRadious:self.txtPrinterName radius:5 borderColor:[UIColor whiteColor] andBorderWidth:2.0];
}

- (IBAction)btnSavePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(handleBtnSavePressed)]) {
        [self.delegate handleBtnSavePressed];
    }
}

- (IBAction)btnSKUListPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(gotoSKUList)]) {
        [self.delegate gotoSKUList];
    }
}

- (IBAction)btnCategoryPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(gotoCategory)]) {
        [self.delegate gotoCategory];
    }
}

- (IBAction)switchNonInventoryChangedValue:(id)sender {
}
- (IBAction)switchCustomizableChangedValue:(id)sender {
}
- (IBAction)switchIsGiftCardChangedValue:(id)sender {
}

-(void)loadData:(ProductEntity*)entity {
    self.txtName.text = entity.Name;
    self.txtPrinterName.text = entity.PrinterName;
    [self.switchNonInventory setOn:entity.NonInventory];
    [self.switchCustomizable setOn:entity.Customizable];
    [self.switchIsGiftCard setOn:entity.IsGiftCard];
    
    NSString* baseUrl = [NSString stringWithFormat:@"https://www.gposdev.com/%@/images", [RequestManager sharedClient].storeId];
    
    NSString *uploadedUrl = [NSString stringWithFormat:@"%@/%@", baseUrl, entity.ImageUri];
    [Helpers loadImage:self.profileImageView placeHolder:[UIImage imageNamed:@"background-qrcode"] withUrl:uploadedUrl];
}

- (IBAction)btnUploadImagePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(handleButtonUploadImagePressed)]) {
        [self.delegate handleButtonUploadImagePressed];
    }
}


@end

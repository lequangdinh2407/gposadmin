//
//  StoreDetailView.m
//  BConnect
//
//  Created by Inglab on 03/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "StoreDetailView.h"

@implementation StoreDetailView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"StoreDetailView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        
        
        [self layoutIfNeeded];
    }
    return self;
}

-(void)prepareLayout {
    
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
    
    
    [self.txtName setName:@"Name"];
    [self.txtPhone setName:@"Phone"];
    [self.txtShopCode setName:@"Shop Code"];
    [self.txtPasscode setName:@"Password"];
    [self.txtStoreId setName:@"StoreId"];
    [self.txtUser setName:@"User"];
    [self.txtDevice setName:@"Device"];
    [self.txtCoordinateLong setName:@"Coordinate.Lon"];
    [self.txtCoordinateLat setName:@"Coordinate.Lat"];
    
    
    [self.txtPhone setPhoneKeyboard];
    [self.txtCoordinateLong setNumberAndPunctuationKeyboard];
    [self.txtCoordinateLat setNumberAndPunctuationKeyboard];
   
    self.txtShopCode.textField.delegate = self;
    self.txtName.textField.delegate = self;
    self.txtPhone.textField.delegate = self;
    self.txtPasscode.textField.delegate = self;
    self.txtStoreId.textField.delegate = self;
    self.txtUser.textField.delegate = self;
    self.txtDevice.textField.delegate = self;
    self.txtCoordinateLat.textField.delegate = self;
    self.txtCoordinateLong.textField.delegate = self;
    
}

-(void)loadData {
    if (self.entity) {
        [self.txtShopCode setValue:self.entity.shopCode];
        [self.txtName setValue:self.entity.name];
        [self.txtPhone setValue:self.entity.phone];
        [self.txtPasscode setValue:self.entity.password];
        [self.txtUser setValue:self.entity.user];
        [self.txtDevice setValue:self.entity.device];
        [self.txtStoreId setValue:self.entity.storeId];
      
        
        
        [self.txtCoordinateLat setValue:[NSString stringWithFormat:@"%0.2f", self.entity.coordinateLat]];
        [self.txtCoordinateLong setValue:[NSString stringWithFormat:@"%0.2f", self.entity.coordinateLon]];
        
        [self.switchActive setOn:self.entity.active animated:false];
        
        
    }
}

#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.txtName.textField]) {
        [self.txtName.textField resignFirstResponder];
        [self.txtShopCode.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtShopCode.textField]) {
        [self.txtShopCode.textField resignFirstResponder];
        [self.txtPhone.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtPhone.textField]) {
        [self.txtPhone.textField resignFirstResponder];
        [self.txtStoreId.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtStoreId.textField]) {
        [self.txtStoreId.textField resignFirstResponder];
        [self.txtUser.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtUser.textField]) {
        [self.txtUser.textField resignFirstResponder];
        [self.txtPasscode.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtPasscode.textField]) {
        [self.txtPasscode.textField resignFirstResponder];
        [self.txtDevice.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtDevice.textField]) {
        [self.txtDevice.textField resignFirstResponder];
        [self.txtCoordinateLong.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtCoordinateLong.textField]) {
        [self.txtCoordinateLong.textField resignFirstResponder];
        [self.txtCoordinateLat.textField becomeFirstResponder];
    }else {
        [self endEditing:YES];
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (IBAction)btnSavePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(handleButtonSavePressed)]) {
        [self.delegate handleButtonSavePressed];
    }
}

@end

//
//  ReportOrderTableViewCell.h
//  BConnect
//
//  Created by Inglab on 06/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SaleOrderEntity.h"
NS_ASSUME_NONNULL_BEGIN

@interface ReportOrderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbOrderNumber;

@property (weak, nonatomic) IBOutlet UILabel *lbCustomerFirstName;
@property (weak, nonatomic) IBOutlet UILabel *lbCustomerLastName;

@property (weak, nonatomic) IBOutlet UILabel *lbOrderDay;
@property (weak, nonatomic) IBOutlet UILabel *lbStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbCreatedBy;
@property (weak, nonatomic) IBOutlet UILabel *lbGrandTotal;
@property (weak, nonatomic) IBOutlet UILabel *lbTips;
@property (weak, nonatomic) IBOutlet UILabel *lbSaleTax;

- (void)loadCellInfo:(SaleOrderEntity*)entity;

@end

NS_ASSUME_NONNULL_END

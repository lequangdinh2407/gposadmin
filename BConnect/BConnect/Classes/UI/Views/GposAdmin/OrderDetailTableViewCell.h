//
//  OrderDetailTableViewCell.h
//  BConnect
//
//  Created by Inglab on 02/11/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SaleOrderItemEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbProviderName;
@property (weak, nonatomic) IBOutlet UILabel *lbTax;
@property (weak, nonatomic) IBOutlet UILabel *lbUnitPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbItemName;
@property (weak, nonatomic) IBOutlet UILabel *lbQuantity;

- (void)loadCellInfo:(SaleOrderItemEntity*)entity;

@end

NS_ASSUME_NONNULL_END

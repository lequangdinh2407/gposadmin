//
//  CustomerTableViewCell.h
//  BConnect
//
//  Created by Inglab on 22/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface CustomerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;

@property (weak, nonatomic) IBOutlet UILabel *lbCustomerName;
@property (weak, nonatomic) IBOutlet UILabel *lbFirstName;
@property (weak, nonatomic) IBOutlet UILabel *lbLastName;
@property (weak, nonatomic) IBOutlet UILabel *lbPhone;

@property (weak, nonatomic) IBOutlet UILabel *lbRewardPoint;
@property (weak, nonatomic) IBOutlet UILabel *lbSaleCount;

@property (weak, nonatomic) IBOutlet UILabel *lbTotalSales;
@property (weak, nonatomic) IBOutlet UILabel *lbRewardCoupon;
@property (weak, nonatomic) IBOutlet UILabel *lbEmailAddress;


- (void)loadCellInfo:(CustomerEntity*)entity;

@end

NS_ASSUME_NONNULL_END

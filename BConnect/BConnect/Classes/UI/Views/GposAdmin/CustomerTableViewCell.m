//
//  CustomerTableViewCell.m
//  BConnect
//
//  Created by Inglab on 22/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "CustomerTableViewCell.h"


@implementation CustomerTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"CustomerTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)loadCellInfo:(CustomerEntity*)entity {
    self.lbCustomerName.text = [NSString stringWithFormat:@"%@ %@", entity.FirstName, entity.LastName];
    self.lbFirstName.text = entity.FirstName;
    self.lbLastName.text = entity.LastName;
    self.lbPhone.text = entity.Phone;
    self.lbEmailAddress.text = entity.EmailAddress;
    self.lbSaleCount.text = [NSString stringWithFormat:@"%d", entity.SalesCount];
    self.lbTotalSales.text = [Helpers formatCurrency:entity.TotalSales];
    self.lbRewardPoint.text = [NSString stringWithFormat:@"%0.2f", entity.RewardPoints];
    
    NSString* receiveCoupon = @"False";
    if (entity.ReceiveCoupon == true) {
        receiveCoupon = @"True";
    }
    
    self.lbRewardCoupon.text = receiveCoupon;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

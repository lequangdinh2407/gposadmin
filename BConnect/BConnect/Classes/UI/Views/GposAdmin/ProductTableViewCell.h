//
//  ProductTableViewCell.h
//  BConnect
//
//  Created by Inglab on 30/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductEntity.h"
#import "ListProductEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbPrice;
@property (weak, nonatomic) IBOutlet UISwitch *switchActive;
@property (weak, nonatomic) NSIndexPath* index;

- (void)loadCellInfo:(ProductEntity*)entity;

- (void)loadSkuCellInfo:(ListProductEntity*)entity;

@end

NS_ASSUME_NONNULL_END

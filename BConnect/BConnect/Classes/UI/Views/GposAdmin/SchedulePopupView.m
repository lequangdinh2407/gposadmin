//
//  SchedulePopupView.m
//  BConnect
//
//  Created by Inglab on 29/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "SchedulePopupView.h"

@implementation SchedulePopupView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SchedulePopupView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
       
    }
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:20 borderColor:[UIColor clearColor] andBorderWidth:0];
    return self;
}

- (IBAction)btnFromPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(showTimePicker:start:end:)]) {
        [self.delegate showTimePicker:1 start:self.start end:self.end];
    }
}
- (IBAction)btnToPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(showTimePicker:start:end:)]) {
        [self.delegate showTimePicker:2 start:self.start end:self.end];
    }
}

-(void)setFromTitle:(NSString*)title {
    [self.btnFromTime setTitle:title forState:UIControlStateNormal];
    self.start = title;
}

-(void)setToTitle:(NSString*)title {
    [self.btnToTime setTitle:title forState:UIControlStateNormal];
    self.end = title;
}

- (IBAction)btnSavePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(saveTime:end:)]) {
        [self.delegate saveTime:self.start end:self.end];
    }
}

@end

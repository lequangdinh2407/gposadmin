//
//  ProductDetailView.h
//  BConnect
//
//  Created by Inglab on 12/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductEntity.h"

@protocol ProductDetailViewDelegate
-(void)handleBtnSavePressed;
-(void)gotoSKUList;
-(void)gotoCategory;
-(void)handleButtonUploadImagePressed;
@end

@interface ProductDetailView : UIView

- (void)prepareLayout;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtPrinterName;

@property (weak, nonatomic) IBOutlet UISwitch *switchNonInventory;
@property (weak, nonatomic) IBOutlet UISwitch *switchCustomizable;
@property (weak, nonatomic) IBOutlet UISwitch *switchIsGiftCard;

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

@property (weak, nonatomic) id delegate;

-(void)loadData:(ProductEntity*)entity;

@end


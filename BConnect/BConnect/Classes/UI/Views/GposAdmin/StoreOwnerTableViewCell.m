//
//  StoreOwnerTableViewCell.m
//  BConnect
//
//  Created by Inglab on 05/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "StoreOwnerTableViewCell.h"

@implementation StoreOwnerTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"StoreOwnerTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)loadCellInfo:(StoreOwnerEntity*)entity {
    self.lbName.text = entity.firstName;
    self.lbPhone.text = entity.phone;
    self.lbStoreID.text = entity.storeId;
    self.lbEmail.text = entity.email;
    self.lbActive.text = entity.active ? @"True" : @"False";
}

@end

//
//  ReportOrderTableViewCell.m
//  BConnect
//
//  Created by Inglab on 06/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ReportOrderTableViewCell.h"

@implementation ReportOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ReportOrderTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
    }
    return self;
}


- (void)loadCellInfo:(SaleOrderEntity*)entity {
    self.lbOrderNumber.text = entity.OrderNumber;
    self.lbCustomerFirstName.text = entity.CustomerFirstName;
    self.lbCustomerLastName.text = entity.CustomerLastName;
    self.lbStatus.text = entity.Status;
    self.lbSaleTax.text = [NSString stringWithFormat:@"%f", entity.SaleTax];
    self.lbOrderDay.text = entity.OrderDate;
    self.lbTips.text = [NSString stringWithFormat:@"%f", entity.Tips];
    self.lbCreatedBy.text = entity.CreatedBy;
    self.lbGrandTotal.text = [NSString stringWithFormat:@"%f", entity.GrandTotal];
    
    
}
@end

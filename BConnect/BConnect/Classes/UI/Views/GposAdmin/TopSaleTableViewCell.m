//
//  TopSaleTableViewCell.m
//  BConnect
//
//  Created by Inglab on 01/02/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "TopSaleTableViewCell.h"
#import "TopSaleRowTableViewCell.h"
#import "SaleEntity.h"

@implementation TopSaleTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"TopSaleTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
        self.tableView.separatorColor = [UIColor clearColor];
        self.tableView.allowsSelection = false;
    }
    
    
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnMonthPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(handleButtonMonthPressed)]) {
        [self.delegate handleButtonMonthPressed];
    }
}

#pragma mark - tableView methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.topSalesList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TopSaleRowTableViewCell *cell = (TopSaleRowTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"TopSaleRowTableViewCell"];
    
    if(!cell){
        cell = [[TopSaleRowTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TopSaleRowTableViewCell"];
    }
    
    SaleEntity* entity = [self.topSalesList objectAtIndex:indexPath.row];
    cell.lbStore.text = entity.storeName;
    cell.lbSales.text = [NSString stringWithFormat:@"%0.02f", entity.sale];
    
    return cell;
}

@end

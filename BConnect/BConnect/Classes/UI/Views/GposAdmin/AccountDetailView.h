//
//  AccountDetailView.h
//  BConnect
//
//  Created by Inglab on 11/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GposTextFieldView.h"
#import "StoreOwnerEntity.h"
@protocol AccountDetailViewDelegate

- (void)handleButtonSavePressed;
- (void)handleButtonChangePasswordPressed;

@end

@interface AccountDetailView : UIView<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet GposTextFieldView *firstName;
@property (weak, nonatomic) IBOutlet GposTextFieldView *lastName;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtEmail;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtPhone;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (weak, nonatomic) id delegate;

-(void)loadData:(StoreOwnerEntity*)entity;

- (void)prepareLayout;

@end


//
//  ProviderTableViewCell.h
//  BConnect
//
//  Created by Inglab on 24/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProviderEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProviderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbActive;
@property (weak, nonatomic) IBOutlet UILabel *lbPhone;
@property (weak, nonatomic) IBOutlet UILabel *lbPasscode;
@property (weak, nonatomic) IBOutlet UILabel *lbCommissionRate;
@property (weak, nonatomic) IBOutlet UILabel *lbName;

- (void)loadCellInfo:(ProviderEntity*)entity;

@end

NS_ASSUME_NONNULL_END

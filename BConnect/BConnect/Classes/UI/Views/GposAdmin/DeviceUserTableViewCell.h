//
//  DeviceUserTableViewCell.h
//  BConnect
//
//  Created by Inglab on 22/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceUserEntity.h"
NS_ASSUME_NONNULL_BEGIN

@interface DeviceUserTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbEmail;

@property (weak, nonatomic) IBOutlet UILabel *lbCashier;
@property (weak, nonatomic) IBOutlet UILabel *lbController;
@property (weak, nonatomic) IBOutlet UILabel *lbPasscode;

@property (weak, nonatomic) IBOutlet UILabel *lbSupervisor;
@property (weak, nonatomic) IBOutlet UILabel *lbManager;
@property (weak, nonatomic) IBOutlet UILabel *lbAdminName;

- (void)loadCellInfo:(DeviceUserEntity*)entity;

@end

NS_ASSUME_NONNULL_END

//
//  SKUDetailView.h
//  BConnect
//
//  Created by Inglab on 03/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GposTextFieldView.h"
#import "ListProductEntity.h"

@protocol SKUDetailViewDelegate

- (void)handleButtonSavePressed;
- (void)handleButtonSaleTaxRatePressed;
- (void)handleButtonUploadImagePressed;
@end


@interface SKUDetailView : UIView
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtName;

@property (weak, nonatomic) IBOutlet GposTextFieldView *txtPrice;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtSupplyCost;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtDuration;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtMinQuantity;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtMaxQuantity;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtSaleTaxRate;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UISwitch *SwitchShowOnApp;

@property (weak, nonatomic) id delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnSaleTaxRate;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

- (void)prepareLayout;

-(void)loadData:(ListProductEntity*)entity;
-(void)setSaleTaxRate:(double)rate;

@end



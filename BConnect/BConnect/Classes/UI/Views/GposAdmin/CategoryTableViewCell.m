//
//  CategoryTableViewCell.m
//  BConnect
//
//  Created by Inglab on 24/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "CategoryTableViewCell.h"

@implementation CategoryTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"CategoryTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)loadCellInfo:(PosCategoryEntity*)entity {
    self.lbName.text = entity.Name;
    [self.btnSwitch setOn:entity.Active animated:false];
}



@end

//
//  TopSaleRowTableViewCell.h
//  BConnect
//
//  Created by Inglab on 02/02/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TopSaleRowTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbStore;
@property (weak, nonatomic) IBOutlet UILabel *lbSales;

@end

NS_ASSUME_NONNULL_END

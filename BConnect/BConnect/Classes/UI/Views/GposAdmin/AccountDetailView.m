//
//  AccountDetailView.m
//  BConnect
//
//  Created by Inglab on 11/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "AccountDetailView.h"

@implementation AccountDetailView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"AccountDetailView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        
        
        [self layoutIfNeeded];
    }
    return self;
}

-(void)prepareLayout {
    
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
    
    [self.firstName setName:@"First Name"];
    [self.lastName setName:@"Last Name"];
    [self.txtEmail setName:@"Email"];
    [self.txtPassword setName:@"Password"];
    [self.txtPhone setName:@"Phone"];
   
    [self.txtPhone setPhoneKeyboard];
    [self.txtEmail setEmailKeyboard];
    self.firstName.textField.delegate = self;
    self.lastName.textField.delegate = self;
    self.txtPhone.textField.delegate = self;
    self.txtEmail.textField.delegate = self;
    self.txtPassword.textField.delegate = self;
    
}

//-(void)loadData {
//    if (self.entity) {
//        [self.txtNotificationKey setValue:self.entity.NotificationKey];
//        [self.txtName setValue:self.entity.Name];
//        [self.txtPhone setValue:self.entity.Phone];
//        [self.txtCommissionRate setValue:[NSString stringWithFormat:@"%d", self.entity.commissionRate]];
//        [self.txtPasscode setValue:self.entity.PassCode];
//        [self.txtPasscode setValue:self.entity.PassCode];
//        [self.switchActive setOn:self.entity.active animated:false];
//    }
//}

#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.firstName.textField]) {
        [self.firstName.textField resignFirstResponder];
        [self.lastName.textField becomeFirstResponder];
    }else if ([textField isEqual:self.lastName.textField]) {
        [self.lastName.textField resignFirstResponder];
        [self.txtEmail.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtEmail.textField]) {
        [self.txtEmail.textField resignFirstResponder];
        [self.txtPhone.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtPhone.textField]) {
        [self.txtPhone.textField resignFirstResponder];
        [self.txtPassword.textField becomeFirstResponder];
    }else {
        [self endEditing:YES];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}
- (IBAction)btnSavePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(handleButtonSavePressed)]) {
        [self.delegate handleButtonSavePressed];
    }
}

- (IBAction)btnChangePasswordPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(handleButtonChangePasswordPressed)]) {
        [self.delegate handleButtonChangePasswordPressed];
    }
}

-(void)loadData:(StoreOwnerEntity*)entity {
    [self.firstName setValue:entity.firstName];
    [self.lastName setValue:entity.lastName];
    [self.txtPhone setValue:entity.phone];
    [self.txtEmail setValue:entity.email];
    
}

@end

//
//  DeviceUserTableViewCell.m
//  BConnect
//
//  Created by Inglab on 22/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "DeviceUserTableViewCell.h"

@implementation DeviceUserTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"DeviceUserTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)loadCellInfo:(DeviceUserEntity*)entity {
    self.lbEmail.text = entity.Email;
    self.lbAdminName.text = entity.UserName;
    self.lbPasscode.text = entity.PassCode;
    self.lbSupervisor.text = entity.isSupervisor;
    self.lbCashier.text = entity.isCashier;
    self.lbManager.text = entity.isManager;
    self.lbController.text = entity.isController;
    
    
   
}

@end

//
//  OrderDetailTableViewCell.m
//  BConnect
//
//  Created by Inglab on 02/11/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "OrderDetailTableViewCell.h"


@implementation OrderDetailTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"OrderDetailTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)loadCellInfo:(SaleOrderItemEntity*)entity {
    self.lbItemName.text = entity.Name;
    self.lbProviderName.text = entity.ProviderName;
    self.lbTax.text = [NSString stringWithFormat:@"%0.2f", entity.SaleTax];
    self.lbUnitPrice.text = [NSString stringWithFormat:@"%0.2f", entity.UnitPrice];
    self.lbQuantity.text = [NSString stringWithFormat:@"%d", entity.Quantity];
}

@end

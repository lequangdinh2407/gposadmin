//
//  TopSaleTableViewCell.h
//  BConnect
//
//  Created by Inglab on 01/02/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GposTextFieldView.h"
NS_ASSUME_NONNULL_BEGIN

@protocol TopSaleTableViewCellDelegate

- (void)handleButtonMonthPressed;

@end

@interface TopSaleTableViewCell : UITableViewCell<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtMonth;
@property (strong, nonatomic) NSMutableArray* topSalesList;

@property (weak, nonatomic) id delegate;

@end

NS_ASSUME_NONNULL_END

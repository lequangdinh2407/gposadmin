//
//  PaymentMethodCollectionViewCell.h
//  BConnect
//
//  Created by Inglab on 12/11/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PaymentMethodCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbAmount;
@property (weak, nonatomic) IBOutlet UIView *colorView;

-(void)loadCellInfo:(NSString*)name amount:(NSString*)amount color:(UIColor*)color;
@end

NS_ASSUME_NONNULL_END

//
//  StatisticView.m
//  BConnect
//
//  Created by Inglab on 02/11/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "StatisticView.h"

@implementation StatisticView


- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"StatisticView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        
        
        [self layoutIfNeeded];
    }
    return self;
}

-(void)lbHeader1:(NSString*)lbHeader1 lbContent1:(NSString*)lbContent1 lbHeader2:(NSString*)lbHeader2 lbContent2:(NSString*)lbContent2 lbHeader3:(NSString*)lbHeader3 lbContent3:(NSString*)lbContent3 {
    self.lbHeader1.text = lbHeader1;
    self.lbContent1.text = lbContent1;
    
    self.lbHeader2.text = lbHeader2;
    self.lbContent2.text = lbContent2;
    
    self.lbHeader3.text = lbHeader3;
    self.lbContent3.text = lbContent3;
    
}
@end

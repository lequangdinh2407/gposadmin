//
//  GposTextFieldView.h
//  BConnect
//
//  Created by Inglab on 25/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GposTextFieldView : UIView

@property (nonatomic, weak) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UITextField *textField;

-(void)setName:(NSString*)name;
-(void)setValue:(NSString*)value;

-(void)setNumberKeyboard;
-(void)setNumberAndPunctuationKeyboard;
-(void)setPhoneKeyboard;
-(void)setEmailKeyboard;
-(void)setDurationKeyboard;
-(void)setPasswordTextField;

@end



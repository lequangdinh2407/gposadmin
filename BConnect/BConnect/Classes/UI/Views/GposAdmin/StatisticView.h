//
//  StatisticView.h
//  BConnect
//
//  Created by Inglab on 02/11/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface StatisticView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lbHeader1;
@property (weak, nonatomic) IBOutlet UILabel *lbContent1;
@property (weak, nonatomic) IBOutlet UILabel *lbHeader2;
@property (weak, nonatomic) IBOutlet UILabel *lbContent2;
@property (weak, nonatomic) IBOutlet UILabel *lbHeader3;
@property (weak, nonatomic) IBOutlet UILabel *lbContent3;

-(void)lbHeader1:(NSString*)lbHeader1 lbContent1:(NSString*)lbContent1 lbHeader2:(NSString*)lbHeader2 lbContent2:(NSString*)lbContent2 lbHeader3:(NSString*)lbHeader3 lbContent3:(NSString*)lbContent3;

@end


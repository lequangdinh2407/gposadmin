//
//  CategoryTableViewCell.h
//  BConnect
//
//  Created by Inglab on 24/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PosCategoryEntity.h"


@interface CategoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbName;

@property (weak, nonatomic) IBOutlet UISwitch *btnSwitch;

- (void)loadCellInfo:(PosCategoryEntity*)entity;



@end



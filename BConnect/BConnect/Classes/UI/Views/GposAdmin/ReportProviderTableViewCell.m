//
//  ReportProviderTableViewCell.m
//  BConnect
//
//  Created by Inglab on 06/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ReportProviderTableViewCell.h"

@implementation ReportProviderTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ReportProviderTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)loadCellInfo:(ProviderReportEntity*)entity {
    self.lbName.text = entity.ProviderName;
    self.lbSale.text = [Helpers formatCurrency:entity.Sales];
    self.lbTips.text = [Helpers formatCurrency:entity.Tips];
    self.lbTotalWorkHours.text = [Helpers formatCurrency:entity.TotalWorkHours];
    self.lbStandardCost.text = [Helpers formatCurrency:entity.StandardCost];
    self.lbCashCollected.text = [Helpers formatCurrency:entity.CashCollected];
}

@end

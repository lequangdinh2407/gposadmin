//
//  SelectCategoryTableViewCell.m
//  BConnect
//
//  Created by Inglab on 12/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "SelectCategoryTableViewCell.h"


@implementation SelectCategoryTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SelectCategoryTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)loadCellInfo:(SelectCategoryEntity*)entity {
    if (!self.cbv) {
        SSCheckBoxViewStyle checkBoxStyle = kSSCheckBoxViewStyleGlossy;
        self.cbv = [[SSCheckBoxView alloc] initWithFrame:CGRectMake(UIScreen.mainScreen.bounds.size.width - 60, 15, 30, 30)
                                               style:checkBoxStyle
                                             checked:false];
        /*
        __weak __typeof(self) weakSelf = self;
        [self.cbv setStateChangedBlock:^(SSCheckBoxView *v) {
            [weakSelf checkBoxViewChangedState:v];
        }];
        */
        [self.cbv setStateChangedTarget:self
                               selector:@selector(checkBoxViewChangedState:)];
        [self addSubview:self.cbv];
    }
    self.entity = entity;
    self.lbName.text = entity.Name;
    [self.cbv setChecked:entity.isSelected];
    
}

- (void)loadStoreCellInfo:(GlobalStoreEntity*)entity {
    if (!self.cbv) {
        SSCheckBoxViewStyle checkBoxStyle = kSSCheckBoxViewStyleGlossy;
        self.cbv = [[SSCheckBoxView alloc] initWithFrame:CGRectMake(UIScreen.mainScreen.bounds.size.width - 60, 15, 30, 30)
                                                   style:checkBoxStyle
                                                 checked:false];
        /*
         __weak __typeof(self) weakSelf = self;
         [self.cbv setStateChangedBlock:^(SSCheckBoxView *v) {
         [weakSelf checkBoxViewChangedState:v];
         }];
         */
        [self.cbv setStateChangedTarget:self
                               selector:@selector(storeCheckBoxViewChangedState:)];
        [self addSubview:self.cbv];
    }
    self.storeEntity = entity;
    self.lbName.text = entity.name;
    [self.cbv setChecked:entity.isSelected];
}

- (void) checkBoxViewChangedState:(SSCheckBoxView *)cbv
{
    self.entity.isSelected = cbv.checked;
}

- (void) storeCheckBoxViewChangedState:(SSCheckBoxView *)cbv
{
    self.storeEntity.isSelected = cbv.checked;
}

@end

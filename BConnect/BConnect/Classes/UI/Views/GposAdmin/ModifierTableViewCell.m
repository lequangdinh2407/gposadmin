//
//  ModifierTableViewCell.m
//  BConnect
//
//  Created by Inglab on 28/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ModifierTableViewCell.h"

@implementation ModifierTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ModifierTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)loadCellInfo:(ModifierEntity*)entity {
    self.entity = entity;
    self.lbName.text = entity.Name;
    self.lbPrice.text = [NSString stringWithFormat:@"$%0.2f", entity.Price];
    
    if (!self.cbv) {
        SSCheckBoxViewStyle checkBoxStyle = kSSCheckBoxViewStyleGlossy;
        self.cbv = [[SSCheckBoxView alloc] initWithFrame:CGRectMake(10, 7, 30, 30)
                                                   style:checkBoxStyle
                                                 checked:false];
        /*
         __weak __typeof(self) weakSelf = self;
         [self.cbv setStateChangedBlock:^(SSCheckBoxView *v) {
         [weakSelf checkBoxViewChangedState:v];
         }];
         */
        [self.cbv setStateChangedTarget:self
                               selector:@selector(checkBoxViewChangedState:)];
        [self addSubview:self.cbv];
    }
    [self.cbv setChecked:entity.isSelected];
    
}

- (void) checkBoxViewChangedState:(SSCheckBoxView *)cbv
{
    self.entity.isSelected = cbv.checked;
}

@end

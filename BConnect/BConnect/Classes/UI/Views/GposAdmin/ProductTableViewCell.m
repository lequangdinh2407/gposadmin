//
//  ProductTableViewCell.m
//  BConnect
//
//  Created by Inglab on 30/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ProductTableViewCell.h"

@implementation ProductTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    //self.imageAvatar.clipsToBounds = NO;
    //[self.imageAvatar.layer setMasksToBounds:YES];
    self.backgroundColor = [UIColor clearColor];
}

- (void)loadCellInfo:(ProductEntity*)entity {
    self.lbName.text = entity.Name;
    self.lbPrice.text = [NSString stringWithFormat:@"$%0.2f", entity.Price];
    
    NSString* baseUrl = [NSString stringWithFormat:@"https://www.gposdev.com/%@/images", [RequestManager sharedClient].storeId];
    NSString *uploadedUrl = [NSString stringWithFormat:@"%@/%@", baseUrl, entity.ImageUri];
    //[Helpers loadImage:self.imageView withLink:entity.ImageUri];
    [Helpers loadImage:self.imageAvatar placeHolder:[UIImage imageNamed:@"background-qrcode"] withUrl:uploadedUrl];
    //background-qrcode
    [self.switchActive setOn:entity.Active];
    self.imageAvatar.frame = CGRectMake(0, 0, 50, 50);
}

- (void)loadSkuCellInfo:(ListProductEntity*)entity {
    self.lbName.text = entity.Name;
    
    NSString* baseUrl = [NSString stringWithFormat:@"https://www.gposdev.com/%@/images", [RequestManager sharedClient].storeId];
    
    NSString *uploadedUrl = [NSString stringWithFormat:@"%@/%@", baseUrl, entity.ImageUri];
   
    [Helpers loadImage:self.imageAvatar placeHolder:nil withUrl:uploadedUrl];
    self.lbPrice.text = [NSString stringWithFormat:@"$%0.2f", entity.Price];
    
    self.imageAvatar.frame = CGRectMake(0, 0, 50, 50);
    [self layoutIfNeeded];
}

@end

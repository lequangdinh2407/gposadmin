//
//  NewStoreOwnerView.m
//  BConnect
//
//  Created by Inglab on 07/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "NewStoreOwnerView.h"

@implementation NewStoreOwnerView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"NewStoreOwnerView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        
        
        [self layoutIfNeeded];
    }
    return self;
}

-(void)prepareLayout {
    
    [Helpers giveBorderWithCornerRadious:self.btnSave radius:self.btnSave.bounds.size.height/2 borderColor:[UIColor clearColor] andBorderWidth:0];
    
    [self.txtFirstName setName:@"First Name"];
    [self.txtLastName setName:@"Last Name"];
    [self.txtStore setName:@"Store"];
    [self.txtEmail setName:@"Email"];
    [self.txtPhone setName:@"Phone"];
    [self.txtEmail setEmailKeyboard];
    [self.txtPhone setPhoneKeyboard];
    
    self.txtFirstName.textField.delegate = self;
    self.txtLastName.textField.delegate = self;
    self.txtEmail.textField.delegate = self;
    self.txtStore.textField.delegate = self;
    self.txtPhone.textField.delegate = self;
    
}



#pragma mark - UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.txtFirstName.textField]) {
        [self.txtFirstName.textField resignFirstResponder];
        [self.txtLastName.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtLastName.textField]) {
        [self.txtLastName.textField resignFirstResponder];
        //[self.txtPhone.textField becomeFirstResponder];
    }else if ([textField isEqual:self.txtEmail.textField]) {
        [self.txtEmail.textField resignFirstResponder];
        [self.txtPhone.textField becomeFirstResponder];
    } else {
        [self endEditing:YES];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}



- (IBAction)btnSavePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(handleButtonSavePressed)]) {
        [self.delegate handleButtonSavePressed];
    }
}

- (IBAction)btnStorePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(handleButtonStorePressed)]) {
        [self.delegate handleButtonStorePressed];
    }
}

@end

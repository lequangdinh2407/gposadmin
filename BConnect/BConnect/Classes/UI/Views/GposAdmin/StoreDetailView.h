//
//  StoreDetailView.h
//  BConnect
//
//  Created by Inglab on 03/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GposTextFieldView.h"
#import "GlobalStoreEntity.h"

@protocol StoreDetailViewDelegate
- (void)handleButtonSavePressed;
@end

@interface StoreDetailView : UIView<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet GposTextFieldView *txtName;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtShopCode;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtPhone;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtStoreId;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtUser;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtPasscode;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtDevice;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtCoordinateLong;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtCoordinateLat;

@property (weak, nonatomic) IBOutlet UISwitch *switchActive;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (strong, nonatomic) GlobalStoreEntity* entity;

@property (weak, nonatomic) id delegate;


-(void)prepareLayout;
-(void)loadData;

@end

//
//  HomeFourTableViewCell.h
//  RecordLife
//
//  Created by 谢俊逸 on 15/03/2017.
//  Copyright © 2017 谢俊逸. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleBarChart.h"

static NSString* kBarChartCell = @"BarChartCell";

@interface BarChartCell : UITableViewCell<SimpleBarChartDataSource, SimpleBarChartDelegate> {
    SimpleBarChart *_chart;
    
    NSArray *_values;
    NSArray *_xValues;
    
    NSArray *_barColors;
    NSInteger _currentBarColor;
}

@property (strong, nonatomic) NSMutableArray* valuesList;
@property (strong, nonatomic) NSMutableArray* chartMonthsList;
@property (assign, nonatomic) double maximumValue;

-(void)loadData;

@end

//
//  NewStoreOwnerView.h
//  BConnect
//
//  Created by Inglab on 07/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GposTextFieldView.h"
#import "StoreOwnerEntity.h"

@protocol ProviderDetailViewDelegate
- (void)handleButtonSavePressed;
- (void)handleButtonStorePressed;
@end

@interface NewStoreOwnerView : UIView<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtFirstName;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtLastName;
@property (weak, nonatomic) IBOutlet UISwitch *switchActive;

@property (weak, nonatomic) IBOutlet GposTextFieldView *txtStore;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtEmail;

@property (weak, nonatomic) IBOutlet GposTextFieldView *txtPhone;

@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (strong, nonatomic) StoreOwnerEntity* entity;

@property (weak, nonatomic) id delegate;

- (void)prepareLayout;


@end



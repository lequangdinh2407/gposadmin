//
//  ProviderDetailView.h
//  BConnect
//
//  Created by Inglab on 08/12/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GposTextFieldView.h"
#import "ProviderEntity.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ProviderDetailViewDelegate

- (void)handleButtonSavePressed;


@end

@interface ProviderDetailView : UIView<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UISwitch *switchActive;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtNotificationKey;

@property (weak, nonatomic) IBOutlet GposTextFieldView *txtName;

@property (weak, nonatomic) IBOutlet GposTextFieldView *txtPhone;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtCommissionRate;
@property (weak, nonatomic) IBOutlet GposTextFieldView *txtPasscode;

@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (strong, nonatomic) ProviderEntity* entity;

@property (weak, nonatomic) id delegate;

- (void)prepareLayout;
- (void)loadData;

@end

NS_ASSUME_NONNULL_END

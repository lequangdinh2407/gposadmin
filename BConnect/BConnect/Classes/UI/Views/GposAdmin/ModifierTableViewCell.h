//
//  ModifierTableViewCell.h
//  BConnect
//
//  Created by Inglab on 28/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModifierEntity.h"
#import "SSCheckBoxView.h"

@interface ModifierTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbPrice;
@property (strong, nonatomic) SSCheckBoxView* cbv;
@property (strong, nonatomic) ModifierEntity* entity;

- (void)loadCellInfo:(ModifierEntity*)entity;

@end


//
//  StoreTableViewCell.h
//  BConnect
//
//  Created by Inglab on 30/12/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalStoreEntity.h"

@interface StoreTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbStoreCode;
@property (weak, nonatomic) IBOutlet UILabel *lbPhone;
@property (weak, nonatomic) IBOutlet UILabel *lbStoreId;

@property (weak, nonatomic) IBOutlet UILabel *lbUser;
@property (weak, nonatomic) IBOutlet UILabel *lbPassword;
@property (weak, nonatomic) IBOutlet UILabel *lbCoordinate;
@property (weak, nonatomic) IBOutlet UILabel *lbDevice;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbActive;

- (void)loadCellInfo:(GlobalStoreEntity*)entity;

@end



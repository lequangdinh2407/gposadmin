//
//  StoreOwnerTableViewCell.h
//  BConnect
//
//  Created by Inglab on 05/01/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreOwnerTableViewCell.h"
#import "StoreOwnerEntity.h"

@interface StoreOwnerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbName;

@property (weak, nonatomic) IBOutlet UILabel *lbStoreID;
@property (weak, nonatomic) IBOutlet UILabel *lbEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbPhone;
@property (weak, nonatomic) IBOutlet UILabel *lbActive;

- (void)loadCellInfo:(StoreOwnerEntity*)entity;


@end


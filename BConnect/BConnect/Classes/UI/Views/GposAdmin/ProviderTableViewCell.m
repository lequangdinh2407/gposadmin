//
//  ProviderTableViewCell.m
//  BConnect
//
//  Created by Inglab on 24/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "ProviderTableViewCell.h"

@implementation ProviderTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ProviderTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)loadCellInfo:(ProviderEntity*)entity {
    
    NSString* isActive = @"False";
    if (entity.active == true) {
        isActive = @"True";
    }
    
    self.lbName.text = entity.Name;
    self.lbPasscode.text = entity.PassCode;
    self.lbActive.text = isActive;
    self.lbPhone.text = entity.Phone;
    self.lbCommissionRate.text = [NSString stringWithFormat:@"%d", entity.commissionRate];
    
    
}

@end

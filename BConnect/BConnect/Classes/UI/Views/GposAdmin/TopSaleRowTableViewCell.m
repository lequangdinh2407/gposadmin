//
//  TopSaleRowTableViewCell.m
//  BConnect
//
//  Created by Inglab on 02/02/2020.
//  Copyright © 2020 DKMobility. All rights reserved.
//

#import "TopSaleRowTableViewCell.h"

@implementation TopSaleRowTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"TopSaleRowTableViewCell" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  WorkingTimeTableViewCell.h
//  BConnect
//
//  Created by Inglab on 28/09/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BussinessHourEntity.h"
#import "WorkingDayEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface WorkingTimeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbTime;

@property (weak, nonatomic) IBOutlet UISwitch *switchIsOpen;

@property (strong, nonatomic) WorkingDayEntity* entity;
@property (strong, nonatomic) BussinessHourEntity* bussinessHourEntityEntity;

- (void)loadCellInfo:(BussinessHourEntity*)entity;
- (void)loadWorkingDayCellInfo:(WorkingDayEntity*)entity;

@end

NS_ASSUME_NONNULL_END

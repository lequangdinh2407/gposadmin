//
//  PaymentMethodCollectionViewCell.m
//  BConnect
//
//  Created by Inglab on 12/11/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import "PaymentMethodCollectionViewCell.h"

@implementation PaymentMethodCollectionViewCell

- (void)drawRect:(CGRect)rect{
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    return layoutAttributes;
}

-(void)prepareForReuse{
    [super prepareForReuse];
}

//- (void)loadBoothInfo:(BoothEntity*)data {
-(void)loadCellInfo:(NSString*)name amount:(NSString*)amount color:(UIColor*)color {
    
    self.lbName.text = name;
    self.lbAmount.text = amount;
    self.colorView.backgroundColor = color;
    [Helpers giveBorderWithCornerRadious:self.colorView radius:5 borderColor:[UIColor clearColor] andBorderWidth:0];
    [self layoutIfNeeded];
}

@end

//
//  OrderItemTableViewCell.h
//  BConnect
//
//  Created by Inglab on 09/10/2019.
//  Copyright © 2019 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderItemEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbTime;
@property (weak, nonatomic) IBOutlet UILabel *lbItemName;

@property (weak, nonatomic) IBOutlet UILabel *lbDiscount;

@property (weak, nonatomic) IBOutlet UILabel *lbTax;
@property (weak, nonatomic) IBOutlet UILabel *lbOrderNumber;
@property (weak, nonatomic) IBOutlet UILabel *lbSubTotal;
@property (weak, nonatomic) IBOutlet UILabel *lbGrandTotal;
@property (weak, nonatomic) IBOutlet UILabel *lbTip;

- (void)loadCellInfo:(OrderItemEntity*)entity;

@end

NS_ASSUME_NONNULL_END

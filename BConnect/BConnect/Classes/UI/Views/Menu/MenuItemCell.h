//
//  MenuItemCell.h
//  luvkonect
//
//  Created by Dragon on 8/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuItemCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgItemIcon;
@property (weak, nonatomic) IBOutlet UILabel *lbItemName;
@property (weak, nonatomic) NSDictionary* itemDict;
@property (nonatomic) SEL selector;

@property (nonatomic, strong) UIButton* btnNotiNumber;

- (void)loadItemData:(id)data;

@end

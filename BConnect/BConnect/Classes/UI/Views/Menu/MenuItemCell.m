//
//  MenuItemCell.m
//  luvkonect
//
//  Created by Dragon on 8/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "MenuItemCell.h"

#define ORDER_KEY @"order"
#define ENABLE_KEY  @"enable"
#define TITLE_KEY   @"title"
#define ACTION_KEY  @"action"
#define ICON_KEY  @"icon"

@implementation MenuItemCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadItemData:(id)data{
    self.backgroundColor = [UIColor clearColor];
    self.itemDict = data;
    self.imgItemIcon.image = [UIImage imageNamed:[self.itemDict objectForKey:ICON_KEY]];
    self.lbItemName.text = [self.itemDict objectForKey:TITLE_KEY];
    self.selector = NSSelectorFromString([self.itemDict objectForKey:ACTION_KEY]);
    
    if ([self.lbItemName.text isEqualToString:@"Notification"]) {
        int count = (int)[SharePrefUtils getIntPreference:PREFKEY_NOTI_COUNT];
        if (count > 0) {
            if (self.btnNotiNumber) {
                [self.btnNotiNumber setTitle:[NSString stringWithFormat:@""] forState:UIControlStateNormal];
                self.btnNotiNumber.backgroundColor = [UIColor clearColor];
                [self.btnNotiNumber removeFromSuperview];
            }
            
            self.btnNotiNumber = [[UIButton alloc] initWithFrame:CGRectMake(180, 19, 20, 20)];
            self.btnNotiNumber.userInteractionEnabled = NO;
            if (count > 9) {
                [self.btnNotiNumber setTitle:[NSString stringWithFormat:@"9+"] forState:UIControlStateNormal];
            } else
                [self.btnNotiNumber setTitle:[NSString stringWithFormat:@"%d", count] forState:UIControlStateNormal];
            [self.btnNotiNumber.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:10]];
            self.btnNotiNumber.layer.cornerRadius = 10;
            self.btnNotiNumber.layer.masksToBounds = YES;
            self.btnNotiNumber.backgroundColor = [UIColor redColor];
            [self addSubview:self.btnNotiNumber];
        } else if (self.btnNotiNumber) {
            [self.btnNotiNumber setTitle:[NSString stringWithFormat:@""] forState:UIControlStateNormal];
            self.btnNotiNumber.backgroundColor = [UIColor clearColor];
            [self.btnNotiNumber removeFromSuperview];
        }
    }
}

@end

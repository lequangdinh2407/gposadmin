//
//  MenuUserInfoCell.m
//  luvkonect
//
//  Created by Dragon on 8/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "MenuUserInfoCell.h"
#import "SessionManager.h"
#import "UserEntity.h"

#define IMG_AVATAR_WIDTH 70

@implementation MenuUserInfoCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.lbEmailWidthConstraint.constant = CGRectGetWidth([UIScreen mainScreen].bounds) - 5 - 80 - 8 - 50;
    self.lbUserNameWidthConstraint.constant = CGRectGetWidth([UIScreen mainScreen].bounds) - 5 - 80 - 8 - 50;
    
    //self.imgUserAvt.layer.cornerRadius = IMG_AVATAR_WIDTH / 2;
    //self.imgUserAvt.layer.masksToBounds = YES;
    //self.imgUserAvt.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)loadUserInfo{
    self.backgroundColor = [UIColor clearColor];
    self.imgAvatarCenterConstraint.constant += 25;
    self.imgAvatarCenterConstraint.constant = MIN(self.imgAvatarCenterConstraint.constant, 0); 

    if (![CoreStringUtils isEmpty:[[SessionManager sharedClient] sessionKey]]) {
        
        self.lbUserName.text = [SessionManager sharedClient].userEntity.displayName;
        
        
       
        //[Helpers loadImage:self.imgUserAvt placeHolder:[UIImage imageNamed:@"default_avatar"] withUrl:[SessionManager sharedClient].userEntity.avatar thumbSize:160];
        /*
        self.lbUserName.marqueeType = MLContinuous;
        self.lbUserName.trailingBuffer = 10;
        CGSize expectSize = [self.lbUserName sizeThatFits:CGSizeMake(MAXFLOAT, CGRectGetHeight(self.lbUserName.frame))];
        float duration = expectSize.width / 120;// 120 is speech to scroll words
        [self.lbUserName setScrollDuration:duration];
        */
       
     
        
    } else{
        
        //self.imgUserAvt.image = [UIImage imageNamed:@"default_avatar"];
       
        //self.lbUserName.text = @"";
    
    }
}

@end

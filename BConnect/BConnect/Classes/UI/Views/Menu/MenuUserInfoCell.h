//
//  MenuUserInfoCell.h
//  luvkonect
//
//  Created by Dragon on 8/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"

@interface MenuUserInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgUserAvt;
@property (weak, nonatomic) IBOutlet MarqueeLabel *lbUserName;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbUserNameWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbEmailWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgAvatarCenterConstraint;

- (void)loadUserInfo;

@end

//
//  RatingPopupView.h
//  BConnect
//
//  Created by Dragon on 2/18/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UploadAvatarPopupViewDelegate
- (void)uploadCamera;
- (void)uploadGallery;
@end

@interface UploadAvatarPopupView : UIView

@property (weak, nonatomic) id delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineHeightConstraint;

@end
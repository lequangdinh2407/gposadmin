//
//  LoginView.h
//  luvkonect
//
//  Created by Dragon on 5/21/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"

@protocol LoginViewDelegate

- (void)chooseCountryCode;
- (void)btnLoginPressed;
- (void)btnForgetPaswordPressed;
- (void)btnSignUpPressed;
- (void)btnFBLoginPressed;
- (void)btnGGLoginPressed;

@end

@interface LoginView : UIView<UITextFieldDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbErrorHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *userNameContainer;
@property (weak, nonatomic) IBOutlet UIView *passContainer;
@property (weak, nonatomic) IBOutlet UIView *countryCodeContainer;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtUserName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtPassword;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtCountryCode;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UILabel *lbForgotPassword;
@property (weak, nonatomic) IBOutlet UILabel *lbError;
@property (weak, nonatomic) IBOutlet UILabel *lbSignup;
@property (weak, nonatomic) IBOutlet UIButton *btnShowPass;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLineHeightConstraint;

@property (weak, nonatomic) id delegate;

- (IBAction)showPassword:(id)sender;
- (IBAction)btnLoginPressed:(id)sender;
- (IBAction)btnCountryCodePressed:(id)sender;
- (IBAction)btnFBLoginPressed:(id)sender;
- (IBAction)btnGGLoginPressed:(id)sender;
- (CGFloat)showErrorMessage:(NSString*)message;
- (void)btnForgotPaswordPressed;
- (void)prepareLayout;
- (void)lockAllButton:(BOOL)isLock;
@end

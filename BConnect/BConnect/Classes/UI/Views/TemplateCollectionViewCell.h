//
//  TemplateCollectionViewCell.h
//  spabees
//
//  Created by vu van long on 1/6/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@protocol TemplateCollectionDelegate

- (void)deleteTemplate:(NSIndexPath*)index asset:(ALAsset*)assest;

@end

@interface TemplateCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *viewCheckerContainer;
@property (weak, nonatomic) IBOutlet UIImageView *imgTemplateThumb;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (assign, nonatomic) BOOL isSelected;
@property (weak, nonatomic) NSIndexPath* index;
@property (weak, nonatomic) id delegate;
@property (assign, nonatomic) ALAsset* asset;

- (void)loadAsset:(ALAsset*)asset;

- (void)setTemplateSelected:(BOOL)isSelected;
- (IBAction)btnDeletePressed:(id)sender;

@end
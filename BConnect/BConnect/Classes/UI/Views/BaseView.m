//
//  BaseView.m
//  Loop
//
//  Created by Dragon on 7/14/15.
//  Copyright (c) 2015 DKMobility. All rights reserved.
//

#import "BaseView.h"


@implementation BaseView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"BaseView" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame = frame;
        
        self.cellNib = [UINib nibWithNibName:@"ShopViewCell" bundle:nil];
        [self.collectionView registerNib:self.cellNib forCellWithReuseIdentifier:@"ShopViewCell"];
        
        [self prepareLayout];
    }
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)prepareLayout{    
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    layout.headerHeight = 0;
    layout.footerHeight = 0;
    layout.minimumColumnSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.columnCount = kPromotionItemInRow;
    [self.collectionView setCollectionViewLayout:layout];
    
    
   
    self.collectionView.alwaysBounceVertical = YES;
}

- (void)hideSpinner:(BOOL)isHide{
    if (!spinner) {
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner setFrame:CGRectMake(self.center.x, self.center.y, 30, 30)];
        [self addSubview:spinner];
        [self bringSubviewToFront:spinner];
    }
    [spinner setHidden:isHide];
    isHide ? [spinner stopAnimating] : [spinner startAnimating];
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
}

#pragma mark - Collection methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.promotionList.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(0, 0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    ShopViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShopViewCell" forIndexPath:indexPath];
//    cell.layer.shouldRasterize = YES;
//    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
//    
//    EventEntity *entity = [self.promotionList objectAtIndex:indexPath.row];
//    cell.index = indexPath;
//    return cell;
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.delegate respondsToSelector:@selector(selectPromotionIndex:)]) {
        [self.delegate selectPromotionIndex:indexPath.row];
    }
}

@end

//
//  SuggestedAddressEntity.h
//  BConnect
//
//  Created by Dragon on 3/3/17.
//  Copyright © 2017 DKMobility. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SuggestedAddressEntity : NSObject
@property (strong, nonatomic) NSString *fullAddress;
@property (strong, nonatomic) NSString *firstLine;
@property (strong, nonatomic) NSString *lastLine;
@property (strong, nonatomic) NSString *placeId;
@end

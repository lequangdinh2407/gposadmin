//
//  ResizeImageUtils.h
//  BConnect
//
//  Created by Toan Pham Thanh on 4/20/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IPPhoto.h"

@interface ResizeImageUtils : NSObject

+ (void)asyncLoadImages:(NSArray *)assets
             completion:(void(^)(NSMutableArray *photos))completion;
+ (void)asyncLoadImage:(ALAsset *)alasset
             completion:(void(^)(IPPhoto *photo))completion;
@end

//
//  ResizeImageUtils.m
//  BConnect
//
//  Created by Toan Pham Thanh on 4/20/16.
//  Copyright © 2016 DKMobility. All rights reserved.
//

#import "ResizeImageUtils.h"
#import <ImageIO/ImageIO.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "BDSelectableALAsset.h"
#import "IPPhoto.h"
#import "IPPhotoOptimizationManager.h"

@implementation ResizeImageUtils

//+ (void)asyncLoadImage:(UIImage *)image
//             completion:(void(^)(UIImage *photo))completion {
//    completion = [completion copy];
//    NSConditionLock *workersDone = [[NSConditionLock alloc] initWithCondition:1];
//    
//    IPPhoto *photo = [[IPPhoto alloc] init];
//    photo.image = image;
//    
//    __block UIImage *thumb;
//    
//    [[IPPhotoOptimizationManager sharedManager] asyncOptimizePhoto:photo withCompletion:^(void) {
//            [workersDone lock];
//            thumb = photo.thumbnail;
//            [workersDone unlockWithCondition:[workersDone condition] - 1];
//    }];
//    
//    dispatch_queue_t defaultQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_async(defaultQueue, ^(void) {
//        [workersDone lockWhenCondition:0];
//        dispatch_async(dispatch_get_main_queue(), ^(void) {
//            completion(thumb);
//        });
//        [workersDone unlockWithCondition:0];
//    });
//}


+ (void)asyncLoadImages:(NSArray *)assets
             completion:(void(^)(NSMutableArray *photos))completion {
    
    completion = [completion copy];
    NSMutableArray *photoList = [[NSMutableArray alloc] init];
    NSConditionLock *workersDone = [[NSConditionLock alloc] initWithCondition:[assets count]];
    
    for (ALAsset* alasset in assets) {
        BDSelectableALAsset *asset = [[BDSelectableALAsset alloc] initWithAsset:alasset];
        
        [asset imageAsyncWithCompletion:^(NSString *filename, NSString *uti) {
            if (filename == nil) {
                [workersDone lock];
                [workersDone unlockWithCondition:[workersDone condition] - 1];
                return;
            }
            
            IPPhoto *photo = [[IPPhoto alloc] init];
            photo.filename = filename;
            photo.title = [asset title];
            
            [[IPPhotoOptimizationManager sharedManager] asyncOptimizePhoto:photo withCompletion:^(void) {
                [workersDone lock];
                [photoList addObject:photo];
                [workersDone unlockWithCondition:[workersDone condition] - 1];
            }];
        }];
    }
    
    dispatch_queue_t defaultQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(defaultQueue, ^(void) {       
        [workersDone lockWhenCondition:0];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            completion(photoList);
        });
        [workersDone unlockWithCondition:0];
    });
}

+ (void)asyncLoadImage:(ALAsset *)alasset
             completion:(void(^)(IPPhoto *photo))completion {
    
    completion = [completion copy];
    NSMutableArray *photoList = [[NSMutableArray alloc] init];
    NSConditionLock *workersDone = [[NSConditionLock alloc] initWithCondition:1];
    
    BDSelectableALAsset *asset = [[BDSelectableALAsset alloc] initWithAsset:alasset];
    __block IPPhoto *photo;
    [asset imageAsyncWithCompletion:^(NSString *filename, NSString *uti) {
        if (filename == nil) {
            [workersDone lock];
            [workersDone unlockWithCondition:[workersDone condition] - 1];
            return;
        }
        
        photo = [[IPPhoto alloc] init];
        photo.filename = filename;
        photo.title = [asset title];
        
        [[IPPhotoOptimizationManager sharedManager] asyncOptimizePhoto:photo withCompletion:^(void) {
            [workersDone lock];
            [photoList addObject:photo];
            [workersDone unlockWithCondition:[workersDone condition] - 1];
        }];
    }];
    
    dispatch_queue_t defaultQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(defaultQueue, ^(void) {
        [workersDone lockWhenCondition:0];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            completion(photo);
        });
        [workersDone unlockWithCondition:0];
    });
}



@end


//
//  AppDelegate.m
//  luvkonect
//
//  Created by ToanPham on 3/2/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "AppDelegate.h"
#import "SessionManager.h"
#import "DatabaseManager.h"
#import "EntityManager.h"
#import "LoginViewController.h"
#import <Crashlytics/Crashlytics.h>

#import "IntroViewController.h"
#import "LuvKeyboardBuilder.h"

#import <CocoaLumberjack/CocoaLumberjack.h>
#import "ICETutorialController.h"

#import "AppAuth.h"
#import "SessionManager.h"

//#import "Firebase.h"
#import "CoreStringUtils.h"



#define TAG @"AppDelegate"


#define APP_SCHEME    @"dkmadmin"

@interface AppDelegate () <ICETutorialControllerDelegate> {
    BOOL _inBackground;
    BOOL _inForeground;
    UIBackgroundTaskIdentifier _backgroundTask;
}

@property (strong, nonatomic) NSTimer *timer;

@end

@implementation AppDelegate{
    BOOL isLauchingApp;

}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSLog(@"Startd: %f", [[NSDate date] timeIntervalSince1970]);
    
    [DDLog addLogger:[DDTTYLogger sharedInstance]]; // TTY = Xcode console
    [DDLog addLogger:[DDTTYLogger sharedInstance] withLevel:DDLogLevelVerbose];
    
    //Memory limited
    int cacheSizeMemory = 64*1024*1024; // 16MB
    int cacheSizeDisk = 128*1024*1024; // 32MB
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
    [NSURLCache setSharedURLCache:sharedCache];
    

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [application setApplicationSupportsShakeToEdit:NO];
   
    
    //Load sticker and emojis
    [LuvKeyboardBuilder sharedEmoticonsKeyboard];
    
    // Add observer track network status
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        [self networkStatusChanged:status];
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(kickOutUserDuplicate) name:NOTIFICATION_KICKOUT object:nil];
    
    // App lauch
    isLauchingApp = YES;
    
    [self requestGetHiddenFlag];
    [Log initPath];
    
    [[DatabaseManager sharedClient] getAccountAndSession:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"getAccountAndSession success" message:[SessionManager sharedClient].sessionKey];
        
        self.window.backgroundColor = [UIColor whiteColor];
        [self.window makeKeyAndVisible];
        UIViewController *initViewController = [mainStoryboard instantiateInitialViewController];
        [self.window setRootViewController:initViewController];
        
        if ([CoreStringUtils isEmpty:[SessionManager sharedClient].sessionKey]) {
            LoginViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            [[self getRootNavigation] setViewControllers:@[controller] animated:NO];
        } else {
            [RequestManager sharedClient].apiUrl = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_API_URL];
            [RequestManager sharedClient].username = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_USERNAME];
            [RequestManager sharedClient].storeName = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_STORENAME];
            [RequestManager sharedClient].password = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_PASSWORD];
            [RequestManager sharedClient].storeId = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_STOREID];
            [RequestManager sharedClient].Id = [SharePrefUtils getStringPreference:PREFKEY_CONFIG_STORE_ID];
        }
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"getAccountAndSession failure" message:results];
        
        self.window.backgroundColor = [UIColor whiteColor];
        [self.window makeKeyAndVisible];
        UIViewController *initViewController = [mainStoryboard instantiateInitialViewController];
        [self.window setRootViewController:initViewController];
        
        LoginViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [[self getRootNavigation] setViewControllers:@[controller] animated:NO];
    }];
    

    if ([[UIScreen mainScreen] respondsToSelector:NSSelectorFromString(@"scale")]) {
        if ([[UIScreen mainScreen] scale] < 1.1) {
            [SharePrefUtils writeIntPreference:PREFKEY_CONFIG_IMAGE_LOADING prefValue:IMAGE_LOADING_SMALL];
        }
        
        if ([[UIScreen mainScreen] scale] > 1.9) {
            long mem = [NSProcessInfo processInfo].physicalMemory / (1024.0 * 1024.0);
            if (mem < 500) {
                [SharePrefUtils writeIntPreference:PREFKEY_CONFIG_IMAGE_LOADING prefValue:IMAGE_LOADING_NORMAL];
            } else {
                [SharePrefUtils writeIntPreference:PREFKEY_CONFIG_IMAGE_LOADING prefValue:IMAGE_LOADING_LARGE];
            }
        }
    }
    

    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_APP_LAUNCHING_FINISH object:nil userInfo:nil];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //    [self extendBackgroundRunningTime];
    _inBackground = YES;
   
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    _inForeground = YES;

}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[[SDWebImageManager sharedManager] imageCache] clearDisk];
    [[[SDWebImageManager sharedManager] imageCache] clearMemory];
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] cleanDiskWithCompletionBlock:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CLEAR_MEM_DONE object:self];
    }];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self requestGetHiddenFlag];
    self.rootNavigation = [self getRootNavigation];
  
    isLauchingApp = NO;
    _inBackground = NO;
   
    [[NotificationManager sharedClient] clearNotification];
    
    _inForeground = NO;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DID_BECOME_ACTIVE object:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}

- (void)extendBackgroundRunningTime {
    if (_backgroundTask != UIBackgroundTaskInvalid) {
        // if we are in here, that means the background task is already running.
        // don't restart it.
        return;
    }
    
    [Log d:0 tag:TAG message:@"Attempting to extend background running time"];
    
    __block Boolean self_terminate = YES;
    
    _backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"DummyTask" expirationHandler:^{
        [Log d:0 tag:TAG message:@"Background task expired by iOS"];
        if (self_terminate) {
            [[UIApplication sharedApplication] endBackgroundTask:_backgroundTask];
            _backgroundTask = UIBackgroundTaskInvalid;
        }
    }];
}

- (BOOL)application:(nonnull UIApplication *)application
            openURL:(nonnull NSURL *)url
            options:(nonnull NSDictionary<NSString *, id> *)options {
    
    
    if ([url.scheme localizedCaseInsensitiveCompare:APP_SCHEME] == NSOrderedSame) {
        
        return YES;
    }
    


    
    if ([_currentAuthorizationFlow resumeAuthorizationFlowWithURL:url]) {
        _currentAuthorizationFlow = nil;
        return YES;
    }
    
    return YES;
}



- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    

    if ([url.scheme localizedCaseInsensitiveCompare:APP_SCHEME] == NSOrderedSame) {
       
        return YES;
    }
    
    return YES;
}



- (void)getDataFromDB {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:[NSNumber numberWithInteger:ACTION_GET_DATA_INIT_APP] forKey:DB_TYPE_ACTION];
    [data setValue:[SessionManager sharedClient].accountEntity forKey:DB_ACCOUNT_ENTITY];
    
    NSInteger flag = 0;
    [data setValue:[NSNumber numberWithInteger:flag] forKey:DB_FLAG_ACTION];
    
    [[DatabaseManager sharedClient] addToQueue:data success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"GetDataInitApp success" message:results];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"GetDataInitApp failure" message:results];
    }];
}

- (UINavigationController*)getRootNavigation{
    UINavigationController *navigationController;
    for (UIViewController*tmp in rootView.childViewControllers) {
        if ([tmp isKindOfClass:[UINavigationController class]]) {
            navigationController = (UINavigationController*)tmp;
            break;
        }
    }
    return navigationController;
}



- (void)showUnityWindow {
    //    [self.unityWindow makeKeyAndVisible];
}

- (void)showAppWindow {
    [self.window makeKeyAndVisible];
}



#pragma mark - Goto Notifications




#pragma mark - Network Changed

/*
 * Processing get data after network available.
 */
- (void)networkStatusChanged:(AFNetworkReachabilityStatus)status{
    if (status == AFNetworkReachabilityStatusReachableViaWiFi || status == AFNetworkReachabilityStatusReachableViaWWAN) {
        if (status == AFNetworkReachabilityStatusReachableViaWiFi) {
            [Log d:0 tag:TAG message:@"AFNetworkReachabilityStatusReachableViaWiFi"];
        } else if (status == AFNetworkReachabilityStatusReachableViaWWAN) {
            [Log d:0 tag:TAG message:@"AFNetworkReachabilityStatusReachableViaWWAN"];
        }
        
        if ([rootView isKindOfClass:[BaseViewViewController class]]) {
            [((BaseViewViewController*)rootView) dismissStatusMessage];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NETWORK_AVAILABLE object:nil];
        
       
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONNECT_COMMENT_SOCKET object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONNECT_STOMP object:nil];
        
    } else if (status == AFNetworkReachabilityStatusNotReachable){
        [Log d:0 tag:TAG message:@"AFNetworkReachabilityStatusNotReachable"];
        if ([rootView isKindOfClass:[BaseViewViewController class]]) {
            [((BaseViewViewController*)rootView) showStatusContent:@"Network not available!" statusColor:NOTE_COLOR opacity:1.0 duration:0];
            [Helpers showToast:@"Network not available, please check your network"];
            [((BaseViewViewController*)rootView) dismissProgressHub];
        }
    }
}

#pragma mark - NotificationObserver
- (void)kickOutUserDuplicate{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[AlertviewManager sharedClient] dismissMessageWithAnimation:NO];
        [rootView dismissViewControllerAnimated:YES completion:nil];
        [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(gotoRootView) userInfo:nil repeats:NO];
    });
}

- (void)gotoRootView{
    [((BaseViewViewController*)rootView) dismissProgressHub];
    [self.rootNavigation popToRootViewControllerAnimated:YES];

}

- (void)requestGetHiddenFlag{
    //    [[RequestManager sharedClient] requestGetFlag:^(NSMutableDictionary *results, int errorCode) {
    //        NSLog(@"requestGetFlag success");
    //        [SharePrefUtils writeIntPreference:PREFKEY_HIDDEN_FLAG prefValue:[[results objectForKey:RESPONSE_FLAG] integerValue]];
    //    } failure:^(NSMutableDictionary *results, int errorCode) {
    //        NSLog(@"requestGetFlag fail");
    //    }];
}

#pragma mark - ICETutorialController delegate

- (void)tutorialController:(ICETutorialController *)tutorialController scrollingFromPageIndex:(NSUInteger)fromIndex toPageIndex:(NSUInteger)toIndex {
    
}

- (void)tutorialControllerDidReachLastPage:(ICETutorialController *)tutorialController {
    
}

- (void)tutorialController:(ICETutorialController *)tutorialController didClickOnLeftButton:(UIButton *)sender {
   
    LoginViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [[self getRootNavigation] setViewControllers:@[controller] animated:NO];
    
}

- (void)tutorialController:(ICETutorialController *)tutorialController didClickOnMiddleButton:(UIButton *)sender {
    /*
    [self.viewController stopScrolling];
    [self.viewController.view removeFromSuperview];
    LoginViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    controller.isFBLogin = YES;
    [[self getRootNavigation] setViewControllers:@[controller] animated:NO];
    [BaseViewViewController hideHeaderView:NO];
     */
    
    
    [[RequestManager sharedClient] requestGetProvider:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetProvider success" message:results];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetProvider failure" message:results];
        //[Helpers showToast:@"Get salon info failed"];
    }];
    
    /*
    [[RequestManager sharedClient] requestGetSaleReports:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetProvider success" message:results];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetProvider failure" message:results];
        //[Helpers showToast:@"Get salon info failed"];
    }];
    */
    
    
    /*
    [[RequestManager sharedClient] requestGetPaymentReports:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetProvider success" message:results];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetProvider failure" message:results];
        //[Helpers showToast:@"Get salon info failed"];
    }];
    */
    
    /*
     
     {
     Id = 44;
     Name = "Full_Turn_Amount";
     Value = "100.0";
     },
     {
     Id = 38;
     Name = "Gratuity_Auto_Charge_Base";
     Value = 0;
     },
     */
    
    /*
    NSMutableArray* list = [[NSMutableArray alloc] init];
    SettingEntity* entity1 = [[SettingEntity alloc] init];
    entity1.Id = 44;
    entity1.Name = @"Full_Turn_Amount";
    entity1.Value = @"200.0";
    [list addObject:entity1];
   
    SettingEntity* entity2 = [[SettingEntity alloc] init];
    entity2.Id = 38;
    entity2.Name = @"Gratuity_Auto_Charge_Base";
    entity2.Value = @"0";
    //[list addObject:entity2];
    
    
    [[RequestManager sharedClient] requestSaveSettings:list success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestSaveSettings succeeded" message:results];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestSaveSettings failed" message:results];
        //[Helpers showToast:@"Get salon info failed"];
    }];
    */
     
    /*
    [[RequestManager sharedClient] requestGetSettings:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSettings succeeded" message:results];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSettings failed" message:results];
        //[Helpers showToast:@"Get salon info failed"];
    }];
    */
    
    /*
    [[RequestManager sharedClient] requestAddProvider:5.5 Name:@"Dinh" Phone:@"0545454" Active:true SignedIn:true LastSignedInTime:@"2019-07-18T14:25:19.8363583+00:00" JobCount:7.8 SaleAmount:8 CommissionRate:9 PassCode:@"sample string 10" NotificationKey:@"sample string 11" WorkHours:3 IsTardy:true success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetProvider success" message:results];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetProvider failure" message:results];
        //[Helpers showToast:@"Get salon info failed"];
    }];
    */
    
    
    /*
    [[RequestManager sharedClient] requestGetCategories:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetCategories success" message:results];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetCategories failure" message:results];
        //[Helpers showToast:@"Get salon info failed"];
    }];
     */
    
    /*
    [[RequestManager sharedClient] requestGetSaleOrders:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSaleOrders success" message:results];
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:@"requestGetSaleOrders failure" message:results];
        //[Helpers showToast:@"Get salon info failed"];
    }];
    */
}



@end

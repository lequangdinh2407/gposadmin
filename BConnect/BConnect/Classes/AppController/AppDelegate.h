//
//  AppDelegate.h
//  luvkonect
//
//  Created by ToanPham on 3/2/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol OIDAuthorizationFlowSession;

@interface AppDelegate : UIResponder <UIApplicationDelegate> {

}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController* rootNavigation;
@property (strong, nonatomic) UINavigationBar* navigationBar;
@property (strong, nonatomic) UINavigationItem *navigationItem;
@property (assign, nonatomic) BOOL isLaunchByPushNotification;
@property (assign, nonatomic) BOOL isWaitingGGCallbacked;
@property (assign, nonatomic) BOOL isShowNewMessageAlert;

- (UINavigationController*)getRootNavigation;
- (void)showTutorial;
- (void)showUnityWindow;
- (void)showAppWindow;

@property(nonatomic, strong, nullable) id<OIDAuthorizationFlowSession> currentAuthorizationFlow;

@end

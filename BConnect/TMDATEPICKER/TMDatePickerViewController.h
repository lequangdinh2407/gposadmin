//
//  TMDatePickerViewController.h
//  Task Manager
//
//  Created by AmitKumar on 10/12/14.
//  Copyright (c) 2014 RakeshKumar. All rights reserved.
//

#import "TSQTACalendarRowCell.h"
 
#import "TimesSquare.h"


@protocol TMDatePickerProtocol <NSObject>
@optional
-(void)userhasSelecteddate:(NSDate *)thedate;

@end

@interface TMDatePickerViewController : UIViewController<TSQCalendarViewDelegate>

{
    NSDateFormatter *df;
    NSDate *userselecteddate;
}


@property (strong, nonatomic) IBOutlet UIButton *donebtn;
@property(nonatomic,strong)NSDate *inputdate;
@property(nonatomic,assign)id<TMDatePickerProtocol> thedatedelegate;
@property (weak, nonatomic) IBOutlet UILabel *dayNameInWeek;
@property (strong, nonatomic) IBOutlet UIView *DatePickerView;
@property (strong, nonatomic) IBOutlet UILabel *DayLabel;
@property (strong, nonatomic) IBOutlet UILabel *DateSelectedLabel;
@property (strong, nonatomic) IBOutlet UILabel *DayNumberLabel;
@property (strong, nonatomic) IBOutlet UILabel *YearLabel;
@property (strong, nonatomic) IBOutlet UIButton *DoneOutlet;
@property (strong, nonatomic) IBOutlet UIView *CalViewOutlet;

- (NSDate*)getUserSelectedDate;
- (IBAction)DoneDateAction:(id)sender;
- (IBAction)BackViewTapped:(id)sender;
@end

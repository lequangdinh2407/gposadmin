//
//  TMDatePickerViewController.m
//  Task Manager
//
//  Created by AmitKumar on 10/12/14.
//  Copyright (c) 2014 RakeshKumar. All rights reserved.
//

#import "TMDatePickerViewController.h"


@interface TMDatePickerViewController ()

@end

@implementation TMDatePickerViewController
@synthesize thedatedelegate,inputdate;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
     df=[[NSDateFormatter alloc]init];
    [df setDateFormat:@"EEEE"];
    [df stringFromDate:self.inputdate];
    self.dayNameInWeek.text=[[df stringFromDate:self.inputdate] capitalizedString];
    
    self.DayLabel.text=[[df stringFromDate:self.inputdate] uppercaseString];
    
    [df setDateFormat:@"yyyy"];
    self.YearLabel.text=[df stringFromDate:self.inputdate];
    
    [df setDateFormat:@"MMMM dd , yyyy"];
    self.DateSelectedLabel.text=[[df stringFromDate:self.inputdate] capitalizedString];
    
    
    [df setDateFormat:@"dd"];
    self.DayNumberLabel.text=[df stringFromDate:self.inputdate];
    
    [self.donebtn addTarget:self action:@selector(highlightaction:) forControlEvents:UIControlEventTouchDown];
    [self.donebtn addTarget:self action:@selector(highlightactioncancel:) forControlEvents:UIControlEventTouchUpOutside];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.view addSubview:self.DatePickerView];
    [self setcaleView];
    //self.DatePickerView.center=self.view.center;
}

-(void)highlightaction:(UIButton *)sender{
    [self.donebtn setBackgroundColor:[UIColor blueColor]];
}

-(void)highlightactioncancel:(UIButton *)sender{
    
    [self.donebtn setBackgroundColor:[UIColor clearColor]];
}

-(void)setcaleView{
    
    TSQCalendarView *calendarView = [[TSQCalendarView alloc] initWithFrame:self.CalViewOutlet.frame];
    calendarView.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    calendarView.rowCellClass = [TSQTACalendarRowCell class];
    calendarView.firstDate = [NSDate dateWithTimeIntervalSinceNow:-60 * 60 * 24 * 365 * 1];
    calendarView.lastDate = [NSDate dateWithTimeIntervalSinceNow:60 * 60 * 24 * 365 * 5];
    calendarView.backgroundColor = [UIColor clearColor];
    calendarView.pagingEnabled = NO;
    CGFloat onePixel = 1.0f / [UIScreen mainScreen].scale;
    calendarView.contentInset = UIEdgeInsetsMake(0.0f, onePixel, 0.0f, onePixel);
    calendarView.delegate=self;
     calendarView.selectedDate=self.inputdate;
    
    
    [self.DatePickerView  addSubview: calendarView];
    [calendarView scrollToDate:self.inputdate animated:YES];
    
    
    self.DatePickerView.transform=CGAffineTransformMakeScale(1.0, 1.0);
    self.DatePickerView.center=self.view.center;
    
    if (IS_IPHONE_6 ) {
        self.DatePickerView.transform=CGAffineTransformMakeScale(1.1, 1.1);
        self.DatePickerView.center=self.view.center;
    }
    if (IS_IPHONE_6P ) {
        self.DatePickerView.transform=CGAffineTransformMakeScale(1.1, 1.1);
        self.DatePickerView.center=self.view.center;
    }
    if (IS_IPHONE_4_OR_LESS ) {
        self.DatePickerView.transform=CGAffineTransformMakeScale(.9, .9);
        self.DatePickerView.center=self.view.center;
    }
    if (IS_IPAD) {
        self.DatePickerView.transform=CGAffineTransformMakeScale(1.1, 1.1);
        self.DatePickerView.center=self.view.center;
    }
}

- (void)calendarView:(TSQCalendarView *)calendarView didSelectDate:(NSDate *)date{
    NSLog(@"%@",date);
    [df setDateFormat:@"EEEE"];
    [df stringFromDate:date];
    self.dayNameInWeek.text=[[df stringFromDate:date] capitalizedString];
    
    self.DayLabel.text=[[df stringFromDate:date] uppercaseString];
    
    [df setDateFormat:@"yyyy"];
    self.YearLabel.text=[df stringFromDate:date];
    
    [df setDateFormat:@"MMMM dd , yyyy"];
    self.DateSelectedLabel.text=[[df stringFromDate:date] capitalizedString];
    
    [df setDateFormat:@"dd"];
    self.DayNumberLabel.text=[df stringFromDate:date];
    
    userselecteddate=date;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSDate*)getUserSelectedDate{
    return userselecteddate ? userselecteddate : inputdate;
}

- (IBAction)DoneDateAction:(UIButton *)sender {
   [self.thedatedelegate userhasSelecteddate:userselecteddate];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)BackViewTapped:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end

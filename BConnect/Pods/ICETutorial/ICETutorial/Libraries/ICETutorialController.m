//
//  ICETutorialController.m
//  ICETutorial
//
//  Created by Patrick Trillsam on 25/03/13.
//  Copyright (c) 2014 https://github.com/icepat/ICETutorial. All rights reserved.
//

#import "ICETutorialController.h"

#define GRADIENT_OPACITY 0.6
#define SHADOW_RADIUS 6.0

@interface ICETutorialController ()
@property (nonatomic, strong, readonly) UIImageView *frontLayerView;
@property (nonatomic, strong, readonly) UIImageView *backLayerView;
@property (nonatomic, strong, readonly) UIImageView *gradientView;
@property (nonatomic, strong, readonly) UIScrollView *scrollView;
@property (nonatomic, strong, readonly) UILabel *overlayTitle;
@property (nonatomic, strong, readonly) UIPageControl *pageControl;

/*dinhlq*/

@property (nonatomic, strong) UIView* leftLine;
@property (nonatomic, strong) UIView* rightLine;

@property (nonatomic, strong) UILabel *lbOrConnectSocial;

@property (nonatomic, strong) UILabel *lbWelcome;
@property (nonatomic, strong) UILabel *lbHappyCheckin;

@property (nonatomic, strong) UIButton *leftButton; // phone
@property (nonatomic, strong) UIButton *middleButton; //facebook
@property (nonatomic, strong) UIButton *rightButton; // google
@property (nonatomic, strong) UIImageView *centerImage; // center image

//@property (nonatomic, strong) UIView *backgroundView;

/*~dinhlq*/

@property (nonatomic, assign) ScrollingState currentState;
@property (nonatomic, strong) NSArray *pages;
@property (nonatomic, assign) NSInteger currentPageIndex;
@property (nonatomic, assign) NSInteger nextPageIndex;

@end

@implementation ICETutorialController

- (instancetype)initWithPages:(NSArray *)pages {
    self = [self init];
    if (self) {
        _autoScrollEnabled = YES;
        _pages = pages;
        
        //_backgroundView = [[UIView alloc] init];
        //_backgroundView.backgroundColor = [UIColor whiteColor];
        _frontLayerView = [[UIImageView alloc] init];
        _backLayerView = [[UIImageView alloc] init];
        _gradientView = [[UIImageView alloc] init];
        _scrollView = [[UIScrollView alloc] init];
        
        
        _lbWelcome = [[UILabel alloc] init];
        _lbHappyCheckin = [[UILabel alloc] init];
        
        _overlayTitle = [[UILabel alloc] init];
        _pageControl = [[UIPageControl alloc] init];
        _lbOrConnectSocial = [[UILabel alloc] init];
        _leftLine = [[UIView alloc] init];
        _rightLine = [[UIView alloc] init];
        
        /*dinhlq*/
        
        _leftButton = [[UIButton alloc] init];
        _middleButton = [[UIButton alloc] init];
        _rightButton = [[UIButton alloc] init];
        
        _centerImage = [[UIImageView alloc] init];
        
        /*~dinhlq*/
    }
    return self;
}

- (instancetype)initWithPages:(NSArray *)pages
                     delegate:(id<ICETutorialControllerDelegate>)delegate {
    self = [self initWithPages:pages];
    if (self) {
        _delegate = delegate;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    [self setupView];
    
    // Overlays.
    [self setOverlayTexts];
    [self setOverlayTitle];
    
    // Preset the origin state.
    [self setOriginLayersState];
}

- (void)setupView {
    [self.frontLayerView setFrame:self.view.bounds];
    [self.backLayerView setFrame:self.view.bounds];
    //[self.backgroundView setFrame:self.view.bounds];
    // Decoration.
    //    [self.gradientView setImage:[UIImage imageNamed:@"background-gradient.png"]];
    
    // ScrollView configuration.
    [self.scrollView setFrame:self.view.bounds];
    [self.scrollView setDelegate:self];
    [self.scrollView setPagingEnabled:YES];
    [self.scrollView setContentSize:CGSizeMake([self numberOfPages] * self.view.frame.size.width,
                                               self.scrollView.contentSize.height)];
    
    // Title.
    [self.overlayTitle setTextColor:[UIColor whiteColor]];
    [self.overlayTitle setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Bold" size:32.0]];
    [self.overlayTitle setTextAlignment:NSTextAlignmentCenter];
    
    // lbOrSocialConnect
    [self.lbOrConnectSocial setTextColor:[UIColor colorWithRed:228/255.0 green:194/255.0 blue:92/255.0 alpha:1.0]];
    [self.lbOrConnectSocial setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:16.0]];
    [self.lbOrConnectSocial setTextAlignment:NSTextAlignmentCenter];
    self.lbOrConnectSocial.text = @"or Social Connect";

    
    [self.lbHappyCheckin setTextColor:[UIColor colorWithRed:228/255.0 green:194/255.0 blue:92/255.0 alpha:1.0]];
    [self.lbHappyCheckin setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Bold" size:25.0]];
    [self.lbHappyCheckin setTextAlignment:NSTextAlignmentCenter];
    self.lbHappyCheckin.text = @"HAPPY CHECK-IN";
    
    [self.lbWelcome setTextColor:[UIColor blackColor]];
    [self.lbWelcome setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Bold" size:35.0]];
    [self.lbWelcome setTextAlignment:NSTextAlignmentCenter];
    self.lbWelcome.text = @"WELCOME";
    
    _leftLine.backgroundColor = [UIColor colorWithRed:228/255.0 green:194/255.0 blue:92/255.0 alpha:1.0];
    _rightLine.backgroundColor = [UIColor colorWithRed:228/255.0 green:194/255.0 blue:92/255.0 alpha:1.0];
    
    self.centerImage.image = [UIImage imageNamed:@"image-intro"];
    
    // PageControl configuration.
    /*
    [self.pageControl setNumberOfPages:[self numberOfPages]];
    [self.pageControl setCurrentPage:0];
    [self.pageControl addTarget:self
                         action:@selector(didClickOnPageControl:)
               forControlEvents:UIControlEventValueChanged];
    */
    /*dinhlq*/
    
    // UIButtons.
    [self.leftButton setBackgroundColor:[UIColor colorWithRed:228/255.0 green:194/255.0 blue:92/255.0 alpha:1.0]];
    [[self.leftButton layer] setCornerRadius:20.0f];
    [self.leftButton.titleLabel setFont:[UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:18]];
    [self.leftButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.leftButton setTitle:@"Get Started" forState:UIControlStateNormal];
    
    [self.leftButton addTarget:self
                        action:@selector(didClickOnButton1:)
              forControlEvents:UIControlEventTouchUpInside];
    
    [self.middleButton setBackgroundColor:[UIColor clearColor]];
    [self.middleButton setImage:[UIImage imageNamed:@"facebook"] forState:UIControlStateNormal];
    [self.middleButton addTarget:self
                          action:@selector(didClickOnButton2:)
                forControlEvents:UIControlEventTouchUpInside];
    
    [self.rightButton setBackgroundColor:[UIColor clearColor]];
    [self.rightButton setImage:[UIImage imageNamed:@"google"] forState:UIControlStateNormal];
    [self.rightButton addTarget:self
                         action:@selector(didClickOnButton3:)
               forControlEvents:UIControlEventTouchUpInside];
    
    
    /*~dinhlq*/
    //[self.view addSubview:_backgroundView];
    
    [self.view addSubview:self.frontLayerView];
    [self.view addSubview:self.backLayerView];
    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.overlayTitle];
    //[self.view addSubview:self.pageControl];
    
    [self.view addSubview:_leftButton];
    [self.view addSubview:_middleButton];
    [self.view addSubview:_rightButton];
    [self.view addSubview:_lbOrConnectSocial];
    [self.view addSubview:_leftLine];
    [self.view addSubview:_rightLine];
    [self.view addSubview:_centerImage];
    [self.view addSubview:_lbWelcome];
    [self.view addSubview:_lbHappyCheckin];
    
    /*~dinhlq*/
    
    [self addAllConstraints];
    [self addFrame];
}

-(void)addFrame {
    
    CGSize screen = [UIScreen mainScreen].bounds.size;
    
    double welcomeLabelWidth = 280;
    double checkinLabelWidth = 260;
    
    double leftButtonWidth = 230;
    double leftButtonHeight = 40;
    
    double squareButtonSize = 55;
    self.leftButton.layer.bounds = CGRectMake(screen.width/2 - leftButtonWidth/2 , screen.height - 130, leftButtonWidth, leftButtonHeight);
    self.leftButton.frame = self.leftButton.layer.bounds;
    
    CALayer *shadowLayer = self.leftButton.layer;
    shadowLayer.shadowOffset = CGSizeMake(1, 1);
    shadowLayer.shadowColor = [[UIColor blackColor] CGColor];
    shadowLayer.shadowRadius = SHADOW_RADIUS;
    shadowLayer.shadowOpacity = GRADIENT_OPACITY;
    shadowLayer.shadowPath = [[UIBezierPath bezierPathWithRoundedRect:self.leftButton.frame cornerRadius:self.leftButton.layer.bounds.size.height/2] CGPath];
    
    self.lbOrConnectSocial.layer.bounds = CGRectMake(screen.width/2 - 70, screen.height - 90, 140, 30);
    self.lbOrConnectSocial.frame = self.lbOrConnectSocial.layer.bounds;
    
    self.leftLine.frame = CGRectMake(screen.width/2 - 70 - 30 , screen.height - 75, 30, 1);
    self.rightLine.frame = CGRectMake(screen.width/2 + 70 , screen.height - 75, 30, 1);
    
    //1215 1381
    double centerImageWidth = screen.width*0.8;
    self.centerImage.frame = CGRectMake(screen.width/2 - centerImageWidth/2, screen.height/2 - (centerImageWidth * 1381/1215)/2 - 30, centerImageWidth, centerImageWidth * 1381/1215);
    
    self.lbHappyCheckin.frame = CGRectMake(screen.width/2 - checkinLabelWidth/2 , screen.height - 185, checkinLabelWidth, 50);
    
    self.lbWelcome.frame = CGRectMake(screen.width/2 - welcomeLabelWidth/2 , self.centerImage.frame.origin.y - 70, welcomeLabelWidth, 60);
    
    
    self.middleButton.layer.bounds = CGRectMake(screen.width/2 - 50 - squareButtonSize/2  , screen.height - 70, squareButtonSize, squareButtonSize);
    self.middleButton.frame = self.middleButton.layer.bounds;
   
    self.rightButton.layer.bounds = CGRectMake(screen.width/2 + 50 - squareButtonSize/2 , screen.height - 70, squareButtonSize, squareButtonSize);
    self.rightButton.frame = self.rightButton.layer.bounds;
    
    /*
    CALayer *middleShadowLayer = self.middleButton.layer;
    middleShadowLayer.shadowOffset = CGSizeMake(1, 1);
    middleShadowLayer.shadowColor = [[UIColor blackColor] CGColor];
    middleShadowLayer.shadowRadius = SHADOW_RADIUS/2;
    middleShadowLayer.shadowOpacity = GRADIENT_OPACITY/2;
    middleShadowLayer.shadowPath = [[UIBezierPath bezierPathWithRoundedRect:self.middleButton.frame cornerRadius:self.middleButton.layer.bounds.size.height/2] CGPath];
    
   
    
    CALayer *rightShadowLayer = self.rightButton.layer;
    rightShadowLayer.shadowOffset = CGSizeMake(1, 1);
    rightShadowLayer.shadowColor = [[UIColor blackColor] CGColor];
    rightShadowLayer.shadowRadius = SHADOW_RADIUS/2;
    rightShadowLayer.shadowOpacity = GRADIENT_OPACITY/2;
    rightShadowLayer.shadowPath = [[UIBezierPath bezierPathWithRoundedRect:self.rightButton.frame cornerRadius:self.rightButton.layer.bounds.size.height/2] CGPath];
    */
}

#pragma mark - Constraints management.
- (void)addAllConstraints {
    [self.frontLayerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [self.backLayerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    //[self.backgroundView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    
    UIView* superView = self.view;
    
    /*dinhlq*/
    NSDictionary *views = NSDictionaryOfVariableBindings(superView, _overlayTitle, _leftButton, _rightButton, _middleButton, _pageControl, _centerImage);
    NSMutableArray *constraints = [NSMutableArray array];
    
    
    
    // Overlay title.
    [self.overlayTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
    [constraints addObject:@"V:|-116-[_overlayTitle(==50)]"];
    [constraints addObject:@"H:|-54-[_overlayTitle(==212)]-|"];
    
    
    // PageControl.
    
    /*
    [self.pageControl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [constraints addObject:@"V:[_pageControl(==32)]-100-|"];
    [constraints addObject:@"[_pageControl(==40)]"];
    //self.pageControl.isHidden = true;
    */
    
    for (NSString *string in constraints) {
        [self.view addConstraints:[NSLayoutConstraint
                                   constraintsWithVisualFormat:string
                                   options:0 metrics:nil
                                   views:views]];
    }
    
    //NSLayoutConstraint *c;
    
    
    /*
    c = [NSLayoutConstraint constraintWithItem:self.pageControl
                                     attribute:NSLayoutAttributeCenterX
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self.view
                                     attribute:NSLayoutAttributeCenterX
                                    multiplier:1
                                      constant:0];
     */
    //[self.view addConstraint:c];
    
    /*~dinhlq*/
    
}

#pragma mark - Actions
- (IBAction)didClickOnButton1:(id)sender {
    if ([self.delegate respondsToSelector:@selector(tutorialController:didClickOnLeftButton:)]) {
        [self.delegate tutorialController:self didClickOnLeftButton:sender];
    }
}

- (IBAction)didClickOnButton2:(id)sender {
    if ([self.delegate respondsToSelector:@selector(tutorialController:didClickOnMiddleButton:)]) {
        [self.delegate tutorialController:self didClickOnMiddleButton:sender];
    }
}

- (IBAction)didClickOnButton3:(id)sender {
    if ([self.delegate respondsToSelector:@selector(tutorialController:didClickOnRightButton:)]) {
        [self.delegate tutorialController:self didClickOnRightButton:sender];
    }
}

- (IBAction)didClickOnPageControl:(UIPageControl *)sender {
    [self stopScrolling];
    
    // Make the scrollView animation.
    [self scrollToNextPageIndex:sender.currentPage];
}

#pragma mark - Pages
// Set the list of pages (ICETutorialPage).
- (void)setPages:(NSArray *)pages {
    _pages = pages;
}

- (NSUInteger)numberOfPages {
    if (self.pages) {
        return [self.pages count];
    }
    
    return 0;
}

#pragma mark - Animations
- (void)animateScrolling {
    if (self.currentState & ScrollingStateManual) {
        return;
    }
    
    // Jump to the next page...
    NSInteger nextPage = self.currentPageIndex + 1;
    if (nextPage == [self numberOfPages]) {
        // ...stop the auto-scrolling or...
        if (!self.autoScrollEnabled) {
            self.currentState = ScrollingStateManual;
            return;
        }
        
        // ...jump to the first page.
        nextPage = 0;
        self.currentState = ScrollingStateLooping;
        
        // Set alpha on layers.
        [self setOriginLayerAlpha];
        [self setBackLayerPictureWithPageIndex:-1];
    } else {
        self.currentState = ScrollingStateAuto;
    }
    
    // Make the scrollView animation.
    [self scrollToNextPageIndex:nextPage];
    
    // Call the next animation after X seconds.
    [self autoScrollToNextPage];
}

// Call the next animation after X seconds.
- (void)autoScrollToNextPage {
    ICETutorialPage *page = self.pages[self.currentPageIndex];
    
    if (self.autoScrollEnabled) {
        [self performSelector:@selector(animateScrolling)
                   withObject:nil
                   afterDelay:page.duration];
    }
}

- (void)scrollToNextPageIndex:(NSUInteger)nextPageIndex {
    // Make the scrollView animation.
    [self.scrollView setContentOffset:CGPointMake(nextPageIndex * self.view.frame.size.width,0)
                             animated:YES];
    
    // Set the PageControl on the right page.
    //[self.pageControl setCurrentPage:nextPageIndex];
}

#pragma mark - Scrolling management
// Run it.
- (void)startScrolling {
    [self autoScrollToNextPage];
}

// Manually stop the scrolling
- (void)stopScrolling {
    self.currentState = ScrollingStateManual;
}

#pragma mark - State management
// State.
- (ScrollingState)getCurrentState {
    return self.currentState;
}

#pragma mark - Overlay management
// Setup the Title Label.
- (void)setOverlayTitle {
    // ...or change by an UIImageView if you need it.
    [self.overlayTitle setText:@""];
}

// Setup the Title/Subtitle style/text.
- (void)setOverlayTexts {
    int index = 0;
    for (ICETutorialPage *page in self.pages) {
        // SubTitles.
        if ([[[page title] text] length]) {
            [self overlayLabelWithStyle:[page title]
                            commonStyle:[[ICETutorialStyle sharedInstance] titleStyle]
                                  index:index];
        }
        // Description.
        if ([[[page subTitle] text] length]) {
            [self overlayLabelWithStyle:[page subTitle]
                            commonStyle:[[ICETutorialStyle sharedInstance] subTitleStyle]
                                  index:index];
        }
        
        index++;
    }
}

- (void)overlayLabelWithStyle:(ICETutorialLabelStyle *)style
                  commonStyle:(ICETutorialLabelStyle *)commonStyle
                        index:(NSUInteger)index {
    // SubTitles.
    UILabel *overlayLabel = [[UILabel alloc] initWithFrame:CGRectMake((index * self.view.frame.size.width),
                                                                      self.view.frame.size.height - [commonStyle offset],
                                                                      self.view.frame.size.width,
                                                                      TUTORIAL_LABEL_HEIGHT)];
    [overlayLabel setNumberOfLines:[commonStyle linesNumber]];
    [overlayLabel setBackgroundColor:[UIColor clearColor]];
    [overlayLabel setTextAlignment:NSTextAlignmentCenter];
    
    // Datas and style.
    [overlayLabel setText:[style text]];
    [style font] ? [overlayLabel setFont:[style font]] :
    [overlayLabel setFont:[commonStyle font]];
    [style textColor] ? [overlayLabel setTextColor:[style textColor]] :
    [overlayLabel setTextColor:[commonStyle textColor]];
    
    [self.scrollView addSubview:overlayLabel];
}

#pragma mark - Layers management
// Handle the background layer image switch.
- (void)setBackLayerPictureWithPageIndex:(NSInteger)index {
    [self setBackgroundImage:self.backLayerView withIndex:index + 1];
}

// Handle the front layer image switch.
- (void)setFrontLayerPictureWithPageIndex:(NSInteger)index {
    [self setBackgroundImage:self.frontLayerView withIndex:index];
}

// Handle page image's loading
- (void)setBackgroundImage:(UIImageView *)imageView withIndex:(NSInteger)index {
    if (index >= [self.pages count]) {
        [imageView setImage:nil];
        return;
    }
    
    [imageView setImage:[UIImage imageNamed:[self.pages[index] pictureName]]];
}

// Setup lapyer's alpha.
- (void)setOriginLayerAlpha {
    [self.frontLayerView setAlpha:1];
    [self.backLayerView setAlpha:0];
}

// Preset the origin state.
- (void)setOriginLayersState {
    self.currentState = ScrollingStateAuto;
    [self.backLayerView setBackgroundColor:[UIColor whiteColor]];
    [self.frontLayerView setBackgroundColor:[UIColor whiteColor]];
    [self setLayersPicturesWithIndex:0];
}

// Setup the layers with the page index.
- (void)setLayersPicturesWithIndex:(NSInteger)index {
    self.currentPageIndex = index;
    [self setOriginLayerAlpha];
    [self setFrontLayerPictureWithPageIndex:index];
    [self setBackLayerPictureWithPageIndex:index];
}

// Animate the fade-in/out (Cross-disolve) with the scrollView translation.
- (void)disolveBackgroundWithContentOffset:(float)offset {
    if (self.currentState & ScrollingStateLooping){
        // Jump from the last page to the first.
        [self scrollingToFirstPageWithOffset:offset];
    } else {
        // Or just scroll to the next/previous page.
        [self scrollingToNextPageWithOffset:offset];
    }
}

// Handle alpha on layers when the auto-scrolling is looping to the first page.
- (void)scrollingToFirstPageWithOffset:(float)offset {
    // Compute the scrolling percentage on all the page.
    offset = (offset * self.view.frame.size.width) / (self.view.frame.size.width * [self numberOfPages]);
    
    // Scrolling finished...
    if (!offset){
        // ...reset to the origin state.
        [self setOriginLayersState];
        return;
    }
    
    // Invert alpha for the back picture.
    float backLayerAlpha = (1 - offset);
    float frontLayerAlpha = offset;
    
    // Set alpha.
    [self.backLayerView setAlpha:backLayerAlpha];
    [self.frontLayerView setAlpha:frontLayerAlpha];
}

// Handle alpha on layers when we are scrolling to the next/previous page.
- (void)scrollingToNextPageWithOffset:(float)offset {
    // Current page index in scrolling.
    NSInteger nextPage = (int)offset;
    
    // Keep only the float value.
    float alphaValue = offset - nextPage;
    
    // This is only when you scroll to the right on the first page.
    // That will fade-in black the first picture.
    if (alphaValue < 0 && self.currentPageIndex == 0){
        [self.backLayerView setImage:nil];
        [self.frontLayerView setAlpha:(1 + alphaValue)];
        return;
    }
    
    // Switch pictures, and imageView alpha.
    if (nextPage != self.currentPageIndex ||
        ((nextPage == self.currentPageIndex) && (0.0 < offset) && (offset < 1.0)))
        [self setLayersPicturesWithIndex:nextPage];
    
    // Invert alpha for the front picture.
    float backLayerAlpha = alphaValue;
    float frontLayerAlpha = (1 - alphaValue);
    
    // Set alpha.
    [self.backLayerView setAlpha:backLayerAlpha];
    [self.frontLayerView setAlpha:frontLayerAlpha];
}

#pragma mark - ScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Get scrolling position, and nextPageindex.
    float scrollingPosition = scrollView.contentOffset.x / self.view.frame.size.width;
    int nextPageIndex = (int)scrollingPosition;
    
    // If we are looping, we reset the indexPage.
    if (self.currentState & ScrollingStateLooping) {
        nextPageIndex = 0;
    }
    
    // If we are going to the next page, let's call the delegate.
    if (nextPageIndex != self.nextPageIndex) {
        //        self.rightButton.hidden = TRUE;
        
        if ([self.delegate respondsToSelector:@selector(tutorialController:scrollingFromPageIndex:toPageIndex:)]) {
            [self.delegate tutorialController:self scrollingFromPageIndex:self.currentPageIndex toPageIndex:nextPageIndex];
        }
        
        self.nextPageIndex = nextPageIndex;
    }
    
    // Delegate when we reach the end.
    if (self.nextPageIndex == [self numberOfPages] - 1) {
        if ([self.delegate respondsToSelector:@selector(tutorialControllerDidReachLastPage:)]) {
            //            self.rightButton.hidden = FALSE;
            [self.delegate tutorialControllerDidReachLastPage:self];
        }
    }
    
    // Animate.
    [self disolveBackgroundWithContentOffset:scrollingPosition];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    // At the first user interaction, we disable the auto scrolling.
    if (self.scrollView.isTracking) {
        [self stopScrolling];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // Update the page index.
    //[self.pageControl setCurrentPage:self.currentPageIndex];
}

@end



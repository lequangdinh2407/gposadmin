//
//  Cryptor.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "Cryptor.h"

@implementation NSData(AES)

- (NSData*)encryptAES: (NSString *) key
{
    NSData *sha256EN = [[NSData alloc] sha256:key];
    
    NSData* dataNeedEncrypt = self;
    
    unsigned char *keyPtr = (unsigned char *)[sha256EN bytes];
    size_t numBytesEncrypted = 0;
    
    NSUInteger dataLength = [self length];
    
    if (dataLength %16 >0) {
        NSMutableData* data11 = [[NSMutableData alloc] initWithData:self];
        NSMutableData* dataAppend = [[NSMutableData alloc] initWithLength:(16 - (self.length % 16))];
        [data11 appendData:dataAppend];
        dataNeedEncrypt = data11;
    }
    
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    NSString* ivParam = @"dkmobile@#3%&123";
    NSData* ivData = [ivParam dataUsingEncoding:NSASCIIStringEncoding];
    unsigned char* array = (unsigned char*) [ivData bytes];
    
    
    
    
    CCCryptorStatus result = CCCrypt( kCCEncrypt,
                                     kCCAlgorithmAES,
                                     0x0000,
                                     keyPtr,
                                     [sha256EN length],
                                     array,
                                     [dataNeedEncrypt bytes], [dataNeedEncrypt length],
                                     buffer, bufferSize,
                                     &numBytesEncrypted );
    
    if( result == kCCSuccess )
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    else {
        NSLog(@"Failed AES");
    }
    return nil;
}

- (NSData*)decryptAES: (NSString *) key
{
    
    size_t numBytesEncrypted = 0;
    
    NSUInteger dataLength = [self length];
    
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer_decrypt = malloc(bufferSize);
    
    NSString* ivParam = @"dkmobile@#3%&123";
    NSData* ivData = [ivParam dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char* array = (unsigned char*) [ivData bytes];
    
    
    NSData *sha256EN = [[NSData alloc] sha256:key];
    unsigned char *keyPtr = (unsigned char *)[sha256EN bytes];
    
    CCCryptorStatus result = CCCrypt( kCCDecrypt ,
                                     kCCAlgorithmAES128,
                                     0x0000,
                                     keyPtr,
                                     kCCKeySizeAES256,
                                     array,
                                     [self bytes], [self length],
                                     buffer_decrypt, bufferSize,
                                     &numBytesEncrypted );
    
    if( result == kCCSuccess )
        return [NSData dataWithBytesNoCopy:buffer_decrypt length:numBytesEncrypted];
    
    return nil;
}

- (NSData*)sha256: (NSString *)key
{
    NSData *data = [key dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char hash[CC_SHA256_DIGEST_LENGTH];
    if (CC_SHA256([data bytes], [data length], hash)) {
        NSData *sha256 = [NSData dataWithBytes:hash length:CC_SHA256_DIGEST_LENGTH];
        return sha256;
    }
    
    return nil;
}

- (NSData *)AES256EncryptWithKey:(NSString *)key {
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    char keyPtr[kCCKeySizeAES256+1]; // room for terminator (unused)
    bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    NSUInteger dataLength = [self length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                          keyPtr, kCCKeySizeAES256,
                                          NULL /* initialization vector (optional) */,
                                          [self bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesEncrypted);
    if (cryptStatus == kCCSuccess) {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
    
    free(buffer); //free the buffer;
    return nil;
}

- (NSData *)AES256DecryptWithKey:(NSString *)key {
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    char keyPtr[kCCKeySizeAES256+1]; // room for terminator (unused)
    bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    NSUInteger dataLength = [self length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    size_t numBytesDecrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                          keyPtr, kCCKeySizeAES256,
                                          NULL /* initialization vector (optional) */,
                                          [self bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesDecrypted);
    
    if (cryptStatus == kCCSuccess) {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
    }
    
    free(buffer); //free the buffer;
    return nil;
}

@end

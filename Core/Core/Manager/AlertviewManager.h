//
//  AlertviewManager.h
//  Core
//
//  Created by vu van long on 6/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AlertviewManager : NSObject
@property (nonatomic, strong) UIAlertView* alertView;
@property (nonatomic, assign) NSInteger alertTag;
+ (instancetype)sharedClient;
- (instancetype)init;
- (void)showMessage:(NSString*)message delegate:(id)delegate;
- (void)showTitleMessage:(NSString*)title message:(NSString*)message delegate:(id)delegate;
- (void)showTitleMessage:(NSString*)title message:(NSString*)message delegate:(id)delegate cancelButton:(NSString*)cancel otherButton:(NSString*)otherButton;
- (void)dismissMessageWithAnimation:(BOOL)animation;
@end

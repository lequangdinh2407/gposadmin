//
//  EntityManager.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserEntity.h"
#import "AccountEntity.h"
#import "LocationEntity.h"
#import "AddressEntity.h"
#import "CategoryEntity.h"
#import "CountryEntityV2.h"
#import "StateEntity.h"
#import "NewsEntity.h"
#import "PostEntity.h"
#import "BannerEntity.h"
#import "CityEntity.h"
#import "SKUEntity.h"
#import "ProductOptionValueEntity.h"
#import "ProductOptionEntity.h"
#import "ProductEntity.h"
#import "SocialProfileEntity.h"
#import "UploadPhotoEntity.h"


// GPOS AMDIN
#import "ProviderEntity.h"
#import "SaleReportsEntity.h"
#import "PosCategoryEntity.h"
#import "ModifierEntity.h"
#import "ListProductEntity.h"
#import "SaleOrderEntity.h"
#import "DaySaleReportEntity.h"
#import "SettingEntity.h"

#import "CustomerEntity.h"
#import "DeviceUserEntity.h"
#import "OrderItemEntity.h"
#import "ProviderReportEntity.h"
#import "SaleOrderItemEntity.h"
#import "PaymentEntity.h"
#import "GeneralPaymentReportEntity.h"
#import "PaymentReportEntity.h"
#import "BussinessHourEntity.h"
#import "StoreEntity.h"
#import "WorkingDayEntity.h"
#import "SaleTaxGroupEntity.h"
#import "GlobalStoreEntity.h"
#import "StoreOwnerEntity.h"
#import "SaleEntity.h"

static NSInteger const USER_ENTITY = 0;
static NSInteger const SOCIAL_PROFILE_ENTITY = 1;
static NSInteger const LOCATION_ENTITY = 2;
static NSInteger const ADDRESS_ENTITY = 3;
static NSInteger const CATEGORY_ENTITY = 4;
static NSInteger const COUNTRY_ENTITY_V2 = 5;
static NSInteger const STATE_ENTITY = 6;
static NSInteger const NEWS_ENTITY = 7;
static NSInteger const FEED_ENTITY = 8;
static NSInteger const COMMENT_ENTITY = 9;
static NSInteger const LINK_ENTITY = 10;
static NSInteger const BANNER_ENTITY = 11;
static NSInteger const FEED_PHOTO_ENTITY = 12;
static NSInteger const SUBFEED_ENTITY = 13;
static NSInteger const PHOTO_CMT_ENTITY = 14;
static NSInteger const TS_ADDRESS_ENTITY = 15;
static NSInteger const TS_EVENT_ENTITY = 16;
static NSInteger const CITY_ENTITY = 17;
static NSInteger const FLOOR_ENTITY = 18;
static NSInteger const BOOTH_CATEGORY_ENTITY = 19;
static NSInteger const BOOTH_ENTITY = 20;
static NSInteger const PRODUCT_ENTITY = 21;
static NSInteger const PRODUCT_OPT_ENTITY = 22;
static NSInteger const SKU_ENTITY = 23;
static NSInteger const PRODUCT_OPT_VALUE_ENTITY = 24;
static NSInteger const SPEAKER_ENTITY = 25;
static NSInteger const TALK_ENTITY = 26;
static NSInteger const UPLOAD_PHOTO_ENTITY = 39;
static NSInteger const PROVIDER_ENTITY = 41;
static NSInteger const SALE_REPORTS_ENTITY = 42;
static NSInteger const MODIFIER_ENTITY = 43;
static NSInteger const LIST_PRODUCT_ENTITY = 44;
static NSInteger const SALE_ORDER_ENTITY = 45;
static NSInteger const SETTING_ENTITY = 46;
static NSInteger const CUSTOMER_ENTITY = 47;
static NSInteger const DEVICE_USER_ENTITY = 48;

static NSInteger const ORDER_ITEM_ENTITY = 49;
static NSInteger const PROVIDER_REPORT_ENTITY = 50;
static NSInteger const DAY_SALE_REPORT_ENTITY = 51;
static NSInteger const SALE_ORDER_ITEM_ENTITY = 52;

static NSInteger const GENERAL_PAYMENT_REPORT_ENTITY = 53;
static NSInteger const PAYMENT_REPORT_ENTITY = 54;
static NSInteger const PAYMENT_ENTITY = 55;
static NSInteger const BUSSINESS_HOUR_ENTITY = 56;
static NSInteger const STORE_ENTITY = 57;
static NSInteger const WORKING_DAY_ENTITY = 58;
static NSInteger const SALE_TAX_GROUP_ENTITY = 59;
static NSInteger const GLOBAL_STORE_ENTITY = 60;
static NSInteger const STORE_OWNER_ENTITY = 61;
static NSInteger const SALE_ENTITY = 62;

@interface EntityManager : NSObject

+ (instancetype)sharedClient;
- (NSObject*)createItem:(NSObject*)data type:(NSInteger)type;

@end

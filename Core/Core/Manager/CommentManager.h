//
//  ConversationManager.h
//  luvkonectcore
//
//  Created by ToanPham on 3/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestManager.h"

@protocol CommentManagerDelegate

- (void)receivedNewComments:(NSArray*)comments;

@end

@interface CommentManager : NSObject

@property (readonly, nonatomic) BOOL isConnected;
@property (weak, nonatomic) id delegate;


+ (instancetype)sharedClient;
- (instancetype)init;
- (void)InComment:(NSString*)dest delegate:(id<CommentManagerDelegate>)delegate;
- (void)OutComment;

@end

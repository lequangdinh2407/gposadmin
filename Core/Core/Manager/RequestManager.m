//
//  RequestManager.m
//  Core
//
//  Created by ToanPham on 2/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

/*GPOS ADMIN */
#import "GetProviderRequest.h"
#import "PostProviderRequest.h"
#import "GetPosCategoriesRequest.h"
#import "GetSaleOrdersRequest.h"
#import "GetSaleOrderDetailRequest.h"
#import "GetPaymentReportsRequest.h"
#import "GetSettingsRequest.h"
#import "SaveSettingsRequest.h"
#import "GetCustomerListRequest.h"
#import "GetDeviceUsersListRequest.h"
#import "GetCategoryDetailRequest.h"
#import "GetProviderReportRequest.h"
#import "GetOrderReportRequest.h"
#import "PostDeviceUserRequest.h"
#import "AddCategoryRequest.h"
#import "AddNewProductRequest.h"
#import "AddNewSKURequest.h"
#import "GetListProductRequest.h"
#import "GetSKUDetailRequest.h"
#import "EditProviderRequest.h"
#import "EditSkuDetailRequest.h"
#import "EditProductDetailRequest.h"
#import "GetModifiderRequest.h"
#import "EditCategoryRequest.h"
#import "GetProviderReportDetailRequest.h"
#import "GetSaleTaxGroupRequest.h"
#import "GetProviderDetailRequest.h"
#import "EditDeviceUserRequest.h"
#import "GetStoreListRequest.h"
#import "GetGlobalStoreListRequest.h"
#import "AddGlobalStoreRequest.h"
#import "EditGlobalStoreRequest.h"
#import "GetStoreOwnerListRequest.h"
#import "AddStoreOwnerRequest.h"
#import "EditStoreOwnerRequest.h"
#import "GetRecentSaleRequest.h"
#import "GetInMonthSaleRequest.h"


#import <Foundation/Foundation.h>

#import "RequestManager.h"
#import "EntityManager.h"
#import "SessionManager.h"
#import "RequestCodeRequest.h"
#import "CheckValidCodeRequest.h"
#import "AcceptLicenseRequest.h"

#import "LoginRequest.h"
#import "LogoutRequest.h"
#import "RefreshTokenRequest.h"
#import "ContactUsRequest.h"
#import "ChangePasswordRequest.h"
#import "ResetPasswordRequest.h"
#import "SendFeedbackRequest.h"


#import "GetListCitiesRequest.h"
#import "SubmitTokenPNSRequest.h"
#import "FindAddressRequest.h"
#import "GetAddressRequest.h"
#import "GetPlaceRequest.h"

/* FB Request */
#import "AcceptLicenseFBRequest.h"
#import "LoginFBRequest.h"

/* GG Request */
#import "AcceptLicenseGGRequest.h"

#import "LoginGGRequest.h"

/* Loop */
#import "UpdateUserInfoRequest.h"

/* BConnect */
#import "ShareRequest.h"

#import "CommentSkuRequest.h"

#import "GetListRelationshipRequest.h"
#import "GetCountRelationshipRequest.h"


/* Upload API Request */
#import "SendUploadInfoRequest.h"
#import "UploadSocketRequest.h"
#import "UploadFileRequest.h"


#import "UploadPhotoRequest.h"


/* Comment API */
#import "EditCommentRequest.h"
#import "DeleteCommentRequest.h"
#import "GetCommentsRequest.h"
#import "GetSocketCommentUrlRequest.h"
#import "CommentSocketRequest.h"
#import "InCommentRequest.h"
#import "OutCommentRequest.h"

#import "LikeTSRequest.h"
#import "ViewTSRequest.h"
#import "FollowTSRequest.h"
#import "RatingTSRequest.h"
#import "AddTSCommentRequest.h"


#import "GetSaleReportsRequest.h"



/*Rating API */
#import "SubmitRatingRequest.h"

/* Stomp API */
#import "BaseStompRequest.h"
#import "StompRequest.h"
#import "SubscribeRequest.h"
#import "SendRequest.h"
#import "StompFrame.h"






@implementation RequestManager

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.waitingQueueRequest = [[NSMutableArray alloc] init];
    
    return self;
}

+ (instancetype)sharedClient {
    static RequestManager* volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)addToWaitingQueue:(BaseRequest*)request {
    [self.waitingQueueRequest addObject:request];
}

- (void)addWaitingQueueToRealQueue {
    // set the default tag if tag is empty
    for (BaseRequest *request in self.waitingQueueRequest) {
        NSMutableDictionary* params = [Helpers parseJsonToMap:request.jsonData];
        NSArray* keys = [params allKeys];
        for (NSString* key in keys) {
            [key isEqualToString:REQUEST_SESSION_KEY];
            [params setObject:[SessionManager sharedClient].sessionKey forKey:REQUEST_SESSION_KEY];
            break;
        }        
        if (!request.isUpload)
            [request addToQueue];
        else
            [request addToUploadQueue];
    }
    [self.waitingQueueRequest removeAllObjects];
}

- (void)requestRequestCode:(NSInteger)userType sendInfo:(NSString*)sendInfo imei:(NSString*)imei countryCode:(NSString*)countryCode success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = LOOP_URL;
    url = [NSString stringWithFormat:@"%@%@", url, REQUEST_CODE_URI];
    
    RequestCodeRequest *request = [[RequestCodeRequest alloc] init:url sendInfo:sendInfo imei:imei countryCode:countryCode success:success failure:failure];
    [request addToQueue];
}


- (void)requestCheckValidCode:(NSInteger)userType sendInfo:(NSString*)sendInfo activeCode:(NSString*)activeCode imei:(NSString*)imei countryCode:(NSString*)countryCode success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = LOOP_URL;
    url = [NSString stringWithFormat:@"%@%@", url, CHECK_VALID_CODE_URI];
    
    CheckValidCodeRequest *request = [[CheckValidCodeRequest alloc] init:url sendInfo:sendInfo activeCode:activeCode imei:imei countryCode:countryCode success:success failure:failure];
    [request addToQueue];
}

- (void)requestAcceptLicense:(NSInteger)userType sendInfo:(NSString*)sendInfo activeCode:(NSString*)activeCode imei:(NSString*)imei countryCode:(NSString*)countryCode success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = LOOP_URL;
    url = [NSString stringWithFormat:@"%@%@", url, ACCEPT_LICENSE_URI];
    
    AcceptLicenseRequest *request = [[AcceptLicenseRequest alloc] init:url email:sendInfo activeCode:activeCode imei:imei countryCode:countryCode success:success failure:failure];
    [request addToQueue];
}



- (void)requestCountriesListV2Request:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@%@", url, GET_LIST_COUNTRIES_V2_URI];
    
    
}

- (void)requestStatesListRequest:(NSString*)isoCode success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, GET_LIST_STATES_URI];
    
   
}

- (void)requestCitiesListRequest:(NSString*)isoCode success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, GET_LIST_CITIES_URI];
    
    GetListCitiesRequest *request = [[GetListCitiesRequest alloc] init:url state:isoCode success:success failure:failure];
    [request addToQueue];
}

- (void)requestLogin:(NSString*)token type:(int)type lon:(float)lon lat:(float)lat imei:(NSString*)imei success:(SuccessCallback)success failure:(FailureCallback)failure {
    //NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, LOGIN_URI];
    //LoginRequest* request = [[LoginRequest alloc] init:url token:token type:type lon:lon lat:lat imei:imei success:success failure:failure];
    //[request addToQueue];
}

- (void)requestLoginEmail:(NSString*)name password:(NSString*)password success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", GPOS_ADMIN_URL, LOGIN_URI];
    LoginRequest* request = [[LoginRequest alloc] init:url name:name password:password success:success failure:failure];
    [request addToQueue];
}

- (void)requestLogout:(NSString*)deviceId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", GPOS_ADMIN_URL, LOGOUT_URI];
    LogoutRequest* request = [[LogoutRequest alloc] init:url deviceId:deviceId success:success failure:failure];
    [request addToQueue];    
}

- (void)requestRefreshToken:(NSString*)refreshToken lon:(float)lon lat:(float)lat success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", GPOS_ADMIN_URL, REFRESH_TOKEN_URI];
    RefreshTokenRequest* request = [[RefreshTokenRequest alloc] init:url refreshToken:refreshToken lon:lon lat:lat success:success failure:failure];
    [request addToQueue];
}

- (void)requestSubmitTokenPNS:(NSString*)sessionKey imei:(NSString*)imei token:(NSString*)token success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@%@", url, SUBMIT_TOKEN_PNS_URI];
    SubmitTokenPNSRequest* request = [[SubmitTokenPNSRequest alloc] init:url sessionKey:sessionKey imei:imei token:token success:success failure:failure];
    [request addToQueue];
}

- (void)requestGetUserInfo:(NSString*)userId success:(SuccessCallback)success failure:(FailureCallback)failure {
//    GetUserInfoRequest* request = [[GetUserInfoRequest alloc] init:userId success:success failure:failure];
//    [request addToQueue];
}

- (void)requestChangePassword:(int)Id curPass:(NSString*)curPass newPass:(NSString*)newPass rePass:(NSString*)rePass type:(uint8_t)type success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = GPOS_ADMIN_URL;
    url = [NSString stringWithFormat:@"%@%@", url, UPDATE_PROFILE_PASSWORD];
    ChangePasswordRequest *request = [[ChangePasswordRequest alloc] init:url Id:Id curPass:curPass newPass:newPass rePass:rePass type:type success:success failure:failure];
    [request addToQueue];
}

- (void)requestResetPassword:(NSString*)userName imei:(NSString*)imei success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@%@", url, RESET_PASS_URI];

    ResetPasswordRequest *request = [[ResetPasswordRequest alloc] init:url userName:userName imei:imei success:success failure:failure];
    [request addToQueue];
}

- (NSString*)getServicePhoto:(NSString*)url {
    NSString* thumbUrl = [NSString stringWithFormat:@"%@&size=730", url];
    return thumbUrl;
}

- (NSString*)getServicePhotoThumb:(NSString*)url {
    NSString* thumbUrl = [NSString stringWithFormat:@"%@&size=160", url];
    return thumbUrl;
}

- (NSString*)getTemplateUrl2:(NSString*)url {
    NSString* thumbUrl = [NSString stringWithFormat:@"%@?size=730", url];
    return thumbUrl;
}

- (NSString*)getThumbTemplateUrl2:(NSString*)url {
    NSString* thumbUrl = [NSString stringWithFormat:@"%@?size=160", url];
    return thumbUrl;
}

- (void)requestGetAddress:(NSString*)address success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* encodedAddress = [address stringByAddingPercentEscapesUsingEncoding:
                                NSUTF8StringEncoding];
    NSString* url = [NSString stringWithFormat:@"%@%@", GOOGLE_MAP_URL, encodedAddress];
    GetAddressRequest *request = [[GetAddressRequest alloc] init:url success:success failure:failure];
    [request addToQueue];
}

- (void)requestGetPlaceDetailFormPlaceId:(NSString*)placeId success:(SuccessCallback)success failure:(FailureCallback)failure {

    NSString* url = [NSString stringWithFormat:@"%@%@&key=%@", GOOGLE_PLACE_URL, placeId, GOOGLE_API_KEY];
    GetPlaceRequest *request = [[GetPlaceRequest alloc] init:url success:success failure:failure];
    [request addToQueue];
}

- (void)requestAcceptFBLicense:(NSString*)accessToken imei:(NSString*)imei success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@%@", url, ACCEPT_LICENSE_FB_URI];
    
    AcceptLicenseFBRequest* request = [[AcceptLicenseFBRequest alloc] init:url accessToken:accessToken imei:imei success:success failure:failure];
    [request addToQueue];
}



- (void)requestLoginFB:(NSString*)token success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, LOGIN_FB_URI];
    LoginFBRequest* request = [[LoginFBRequest alloc] init:url token:token success:success failure:failure];
    [request addToQueue];
}

- (void)requestAcceptGGLicense:(NSString*)accessToken imei:(NSString*)imei success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@%@", url, ACCEPT_LICENSE_GG_URI];
    
    AcceptLicenseGGRequest* request = [[AcceptLicenseGGRequest alloc] init:url accessToken:accessToken imei:imei success:success failure:failure];
    [request addToQueue];
}



- (void)requestLoginGG:(NSString*)token success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, LOGIN_GG_URI];
    LoginGGRequest* request = [[LoginGGRequest alloc] init:url token:token success:success failure:failure];
    [request addToQueue];
}

- (void)requestSendFeedback:(NSString*)sessionKey message:(NSString*)message success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@%@", url, SEND_FEEDBACK_URI];
    
    SendFeedbackRequest* request = [[SendFeedbackRequest alloc] init:url sessionKey:sessionKey message:message success:success failure:failure];
    [request addToQueue];
}

- (void)requestContactParams:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@%@", url, CONTACT_US_URI];
    
    ContactUsRequest* request = [[ContactUsRequest alloc] init:url params:params success:success failure:failure];
    [request addToQueue];
}

- (void)requestFindAddress:(NSString*)zipCode success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", SPABEE_URL, FIND_ADDRESS_URI];
    
    FindAddressRequest *request = [[FindAddressRequest alloc] init:url sessionKey:zipCode success:success failure:failure];
    [request addToQueue];
}

- (void)requestUpdateUserInfo:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, UPDATE_PROFILE];
    UpdateUserInfoRequest* request = [[UpdateUserInfoRequest alloc] init:url params:params success:success failure:failure];
    [request addToQueue];
}

- (void)requestUploadUserAvatar:(NSData*)file success:(SuccessCallback)success failure:(FailureCallback)failure progress:(ProgressCallback)progress {
    NSString* url = [NSString stringWithFormat:@"%@/upload/user", HOMBER_UPLOAD_URL];
    UploadPhotoRequest* request = [[UploadPhotoRequest alloc] init:url file:file success:success failure:failure progress:progress];
    [request addToProgressUploadQueue];
}

- (void)requestUploadImage:(NSData*)file success:(SuccessCallback)success failure:(FailureCallback)failure progress:(ProgressCallback)progress {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, IMAGES_URI];
    UploadPhotoRequest* request = [[UploadPhotoRequest alloc] init:url file:file success:success failure:failure progress:progress];
    [request addToProgressUploadQueue];
    //[request addToPosQueue];
}

- (void)requestGetWishList:(NSString*)sessionKey objectType:(int)objectType lon:(float)lon lat:(float)lat page:(uint16_t)page count:(uint16_t)count success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@%@", url, GET_WISHLIST_URI];
    
  
}

#pragma mark - News API Request
- (void)requestGetUnreadNewsCount:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@%@", url, GET_UNREAD_NEWS_COUNT_URI];
    
 
}

- (void)requestGetNews:(int)page count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@%@?page=%d&count=%d", url, GET_NEWS_URI, page, count];
    
  
}

- (void)requestViewNews:(NSString*)userId ids:(NSArray*)ids success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@%@", url, VIEW_NEWS_URI];
    
  
}

- (void)requestGetListRelationship:(NSString*)sessionKey userId:(NSString*)userId type:(int)type page:(int)page count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, GET_LIST_RELATIONSHIP_URI];
    
    GetListRelationshipRequest* request = [[GetListRelationshipRequest alloc] initGET:url success:success failure:failure];
    [request addToQueue];
}

- (void)requestGetCountRelationship:(NSString*)sessionKey userId:(NSString*)userId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@/profile/statistics/%@", url, userId];
    
    GetCountRelationshipRequest* request = [[GetCountRelationshipRequest alloc] initGET:url success:success failure:failure];
    [request addToQueue];
}

#pragma mark - Upload API Request

- (void)requestSendUploadInfo:(uint16_t)totalPart size:(uint64_t)size type:(uint8_t)type success:(SuccessCallback)success failure:(FailureCallback)failure {
    SendUploadInfoRequest* request = [[SendUploadInfoRequest alloc] init:totalPart size:size type:type success:success failure:failure];
    [request addToQueue];
}

- (void)requestUploadSocket:(NSString*)sessionKey roomId:(NSString*)roomId url:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure {
//    NSString* url = [SessionManager sharedClient].uploadSockets[0];
    UploadSocketRequest* request = [[UploadSocketRequest alloc] init:url sessionKey:sessionKey roomId:roomId success:success failure:failure];
    [request addToQueue];
}

- (void)requestSendFile:(uint64_t)uploadID partID:(uint32_t)partID data:(NSData*)data success:(SuccessCallback)success failure:(FailureCallback)failure {
    UploadFileRequest* request = [[UploadFileRequest alloc] init:uploadID partID:partID data:data success:success failure:failure];
    [request addToQueue];
}

- (void)requestUploadPhoto:(NSString*)sessionKey fileName:(NSString*)fileName file:(NSData*)file success:(SuccessCallback)success failure:(FailureCallback)failure progress:(ProgressCallback)progress {
    NSString* url = [NSString stringWithFormat:@"%@/upload/feed", HOMBER_UPLOAD_URL];
    
    UploadPhotoRequest *request = [[UploadPhotoRequest alloc] init:url file:file success:success failure:failure progress:progress];
    [request addToProgressUploadQueue];
}

- (void)requestStompSocket:(SuccessCallback)success failure:(FailureCallback)failure {
//    NSString* url = @"ws://115.79.45.86:20002/api/websocket";
    NSString* url = [SessionManager sharedClient].socket;
    StompRequest* request = [[StompRequest alloc] init:url success:success failure:failure];
    [request addToQueue];
}



#pragma mark - Comment API Request

- (void)requestGetNewFeedComments:(NSString*)sessionKey feedId:(NSString*)feedId photoId:(NSString*)photoId commentId:(NSString*)commentId page:(uint16_t)page count:(uint16_t)count success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = self.apiUrl;
    url = [NSString stringWithFormat:@"%@%@", url, GET_NEW_COMMENTS_URI];
}

#pragma mark - Place API

- (void)requestSuggestPlace:(NSString*)name countryCode:(NSString*)countryCode count:(uint16_t)count success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, SUGGEST_EVENT_URI];
    
}

#pragma mark - Event API



- (void)requestViewTS:(int)objectType objectId:(NSString*)objectId photoId:(NSString*)photoId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, VIEW_TS_URI];
    
    ViewTSRequest* request = [[ViewTSRequest alloc] init:url objectType:objectType objectId:objectId photoId:photoId success:success failure:failure];
    [request addToQueue];
}

- (void)requestLikeTS:(int)objectType objectId:(NSString*)objectId photoId:(NSString*)photoId type:(int)type success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, LIKE_TS_URI];
    LikeTSRequest* request = [[LikeTSRequest alloc] init:url objectType:objectType objectId:objectId photoId:photoId type:type success:success failure:failure];
    [request addToQueue];
}

- (void)requestFollowTS:(NSString*)sessionKey objectType:(int)objectType objectId:(NSString*)objectId photoId:(NSString*)photoId type:(int)type success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@/feed/follow/%@", self.apiUrl, objectId];
    FollowTSRequest* request = [[FollowTSRequest alloc] init:url sessionKey:sessionKey objectType:objectType objectId:objectId photoId:photoId type:type success:success failure:failure];
    [request addToQueue];
}

- (void)requestShareTS:(int)objectType objectId:(NSString*)objectId message:(NSString*)message platformId:(NSString*)platformId facebookId:(NSString*)facebookId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, SHARE_TS_URI];
    
    ShareRequest *request = [[ShareRequest alloc] init:url objectType:objectType objectId:objectId message:message platformId:platformId facebookId:facebookId success:success failure:failure];
    [request addToQueue];
}

- (void)requestAddUpdateTSRating:(NSString*)sessionKey objectType:(int)objectType objectId:(NSString*)objectId point:(float)point success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, RATING_TS_URI];
    RatingTSRequest* request = [[RatingTSRequest alloc] init:url sessionKey:sessionKey objectType:objectType objectId:objectId point:point success:success failure:failure];
    [request addToQueue];
}

- (void)requestDeleteTSRating:(NSString*)sessionKey objectType:(int)objectType objectId:(NSString*)objectId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, DELETE_RATING_TS_URI];
    RatingTSRequest* request = [[RatingTSRequest alloc] init:url sessionKey:sessionKey objectType:objectType objectId:objectId success:success failure:failure];
    [request addToQueue];
}

- (void)requestGetListComments:(int)objectType objectId:(NSString*)objectId feedPhotoId:(NSString*)feedPhotoId page:(int)page count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = nil;
    if (![CoreStringUtils isEmpty:feedPhotoId])
        url = [NSString stringWithFormat:@"%@/comment?%@=%d&%@=%@&%@=%@&%@=%d&%@=%d", self.apiUrl, REQUEST_OBJECT_TYPE, objectType, REQUEST_OBJECT_ID, objectId, REQUEST_FEED_PHOTO_ID, feedPhotoId, REQUEST_PAGE, page, REQUEST_COUNT, count];
    else
        url = [NSString stringWithFormat:@"%@/comment?%@=%d&%@=%@&%@=%d&%@=%d", self.apiUrl, REQUEST_OBJECT_TYPE, objectType, REQUEST_OBJECT_ID, objectId, REQUEST_PAGE, page, REQUEST_COUNT, count];

    GetCommentsRequest* request = [[GetCommentsRequest alloc] initGET:url success:success failure:failure];
    [request addToQueue];
}

- (void)requestAddTSComment:(int)objectType objectId:(NSString*)objectId feedPhotoId:(NSString*)feedPhotoId photoId:(NSString*)photoId text:(NSString*)text sticker:(NSString*)sticker success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@/comment", self.apiUrl];
    AddTSCommentRequest* request = [[AddTSCommentRequest alloc] init:url objectType:objectType objectId:objectId feedPhotoId:feedPhotoId text:text sticker:sticker photoId:photoId success:success failure:failure];
    [request addToQueue];
}

- (void)requestEditComment:(NSString*)commentId text:(NSString*)text success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@/comment/%@", self.apiUrl, commentId];
    EditCommentRequest* request = [[EditCommentRequest alloc] init:url feedPhotoId:nil text:text sticker:nil photoId:nil success:success failure:failure];
    [request addToQueue];
}

- (void)requestDeleteTSComment:(NSString*)sessionKey commentId:(NSString*)commentId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@/comment/%@", self.apiUrl, commentId];
    
    DeleteCommentRequest* request = [[DeleteCommentRequest alloc] initDELETE:url success:success failure:failure];
    [request addToQueue];
}

- (void)requestGetListReactionUsers:(int)objectType objectId:(NSString*)objectId photoId:(NSString*)photoId reactType:(int)reactType page:(int)page count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = nil;
    if (![CoreStringUtils isEmpty:photoId])
        url = [NSString stringWithFormat:@"%@/react/user?%@=%d&%@=%@&%@=%@&%@=%d&%@=%d&%@=%d", self.apiUrl, REQUEST_OBJECT_TYPE, objectType, REQUEST_OBJECT_ID, objectId, REQUEST_FEED_PHOTO_ID, photoId, REQUEST_REACT_TYPE, reactType, REQUEST_PAGE, page, REQUEST_COUNT, count];
    else
        url = [NSString stringWithFormat:@"%@/react/user?%@=%d&%@=%@&%@=%d&%@=%d&%@=%d", self.apiUrl, REQUEST_OBJECT_TYPE, objectType, REQUEST_OBJECT_ID, objectId, REQUEST_REACT_TYPE, reactType, REQUEST_PAGE, page, REQUEST_COUNT, count];
    
    GetListRelationshipRequest* request = [[GetListRelationshipRequest alloc] initGET:url success:success failure:failure];
    [request addToQueue];
}

#pragma mark - Rating API
- (void)requestSubmitRating:(NSString*)lanscapingId comment:(NSString*)comment point:(double)point userId:(NSString*)userId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, RATING_URI];
    SubmitRatingRequest* request = [[SubmitRatingRequest alloc] init:url lanscapingId:lanscapingId comment:comment point:point userId:userId success:success failure:failure];
    [request addToQueue];
}

#pragma mark - Stomp API
- (void)requestSubscribeUser:(NSString*)subscriptionId success:(SuccessCallback)success failure:(FailureCallback)failure {
    SubscribeRequest* request = [[SubscribeRequest alloc] init:USER_DEMAND_DEST subscriptionId:@"1" unsubscribe:NO success:success failure:failure];
    [request addToQueue];
}

- (void)requestUnsubscribeUser:(NSString*)subscriptionId success:(SuccessCallback)success failure:(FailureCallback)failure {
    SubscribeRequest* request = [[SubscribeRequest alloc] init:USER_DEMAND_DEST subscriptionId:@"1" unsubscribe:YES success:success failure:failure];
    [request addToQueue];
}

- (void)requestSubscribeComment:(NSString*)commentDest subscriptionId:(NSString*)subscriptionId success:(SuccessCallback)success failure:(FailureCallback)failure {
    SubscribeRequest* request = [[SubscribeRequest alloc] init:commentDest subscriptionId:@"2" unsubscribe:NO success:success failure:failure];
    [request addToQueue];
}

- (void)requestUnsubscribeComment:(NSString*)commentDest subscriptionId:(NSString*)subscriptionId success:(SuccessCallback)success failure:(FailureCallback)failure {
    SubscribeRequest* request = [[SubscribeRequest alloc] init:commentDest subscriptionId:@"2" unsubscribe:YES success:success failure:failure];
    [request addToQueue];
}

- (void)requestUpdateStastus:(ProviderStatusEnum)status success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* receiptId = [NSString stringWithFormat:@"%u", [Helpers getReqIDFormPref]];
    NSDictionary* header = @{DESTINATION_HDR : APP_DEMAND_UPDATE_STATUS_DEST, RECEIPT_HDR : receiptId};
    NSDictionary* body = @{REQUEST_STATUS : @(status)};
    BaseStompRequest* request = [[BaseStompRequest alloc] init:SEND_CMD headers:header body:body receiptId:receiptId success:success failure:failure];
    [request addToQueue];
}

- (void)requestAcceptEstimate:(BOOL)isAccept success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* receiptId = [NSString stringWithFormat:@"%u", [Helpers getReqIDFormPref]];
    NSDictionary* header = @{DESTINATION_HDR : ACCEPT_REESTIMATE_DEST, RECEIPT_HDR : receiptId};
    NSDictionary* body = @{REQUEST_ACCEPT : @(isAccept)};
    BaseStompRequest* request = [[BaseStompRequest alloc] init:SEND_CMD headers:header body:body receiptId:receiptId success:success failure:failure];
    [request addToQueue];
}



#pragma mark - POS ADMIN API

//customer
- (void)requestGetCustomers:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, CUSTOMERS_URI];
    
    GetCustomerListRequest * request = [[GetCustomerListRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

//device-user
- (void)requestGetDeviceUsers:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, DEVICE_USERS_URI];
    
    GetDeviceUsersListRequest * request = [[GetDeviceUsersListRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestAddDeviceUser:(NSString*)UserName Email:(NSString*)Email PassCode:(NSString*)PassCode IsCashier:(BOOL)IsCasiher IsSupervisor:(BOOL)IsSupervisor IsController:(BOOL)IsController IsManager:(BOOL)IsManager success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, DEVICE_USERS_URI];
    
    PostDeviceUserRequest* request = [[PostDeviceUserRequest alloc] init:url UserName:UserName Email:Email PassCode:PassCode IsCashier:IsCasiher IsSupervisor:IsSupervisor IsController:IsController IsManager:IsManager success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestEditDeviceUser:(int)deviceUserId UserName:(NSString*)UserName Email:(NSString*)Email PassCode:(NSString*)PassCode IsCashier:(BOOL)IsCasiher IsSupervisor:(BOOL)IsSupervisor IsController:(BOOL)IsController IsManager:(BOOL)IsManager success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, DEVICE_USERS_URI];
    
    EditDeviceUserRequest* request = [[EditDeviceUserRequest alloc] init:url deviceUserId:deviceUserId UserName:UserName Email:Email PassCode:PassCode IsCashier:IsCasiher IsSupervisor:IsSupervisor IsController:IsController IsManager:IsManager success:success failure:failure];
    [request addToPosQueue];
}


- (void)requestDeleteDeviceUser:(NSString*)username success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@?userName=%@", self.apiUrl, DEVICE_USERS_URI, username];
    
    BaseRequest* request = [[BaseRequest alloc] initDELETE:url success:success failure:failure];
    [request addToPosQueue];
}


- (void)requestGetProvider:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, PROVIDER_URI];
    
    GetProviderRequest* request = [[GetProviderRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestGetProviderDetail:(int)providerId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@/%d", self.apiUrl, PROVIDER_URI, providerId];
    GetProviderDetailRequest * request = [[GetProviderDetailRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestAddProvider:(NSString*)Name Phone:(NSString*)Phone Active:(BOOL)Active SignedIn:(BOOL)SignedIn LastSignedInTime:(NSString*)LastSignedInTime JobCount:(int)JobCount SaleAmount:(double)SaleAmount CommissionRate:(double)CommissionRate PassCode:(NSString*)PassCode NotificationKey:(NSString*)NotificationKey WorkHours:(double)WorkHours IsTardy:(BOOL)IsTardy  success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, PROVIDER_URI];
    
    PostProviderRequest* request = [[PostProviderRequest alloc] init:url  Name:Name Phone:Phone Active:Active SignedIn:SignedIn LastSignedInTime:LastSignedInTime JobCount:JobCount SaleAmount:SaleAmount CommissionRate:CommissionRate PassCode:PassCode NotificationKey:NotificationKey WorkHours:WorkHours IsTardy:IsTardy  success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestEditProvider:(int)Id Name:(NSString*)Name Phone:(NSString*)Phone Active:(BOOL)Active SignedIn:(BOOL)SignedIn LastSignedInTime:(NSString*)LastSignedInTime JobCount:(int)JobCount SaleAmount:(double)SaleAmount CommissionRate:(double)CommissionRate PassCode:(NSString*)PassCode NotificationKey:(NSString*)NotificationKey WorkHours:(double)WorkHours IsTardy:(BOOL)IsTardy WorkingDays:WorkingDays success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, PROVIDER_URI];
    
    EditProviderRequest* request = [[EditProviderRequest alloc] init:url providerId:Id Name:Name Phone:Phone Active:Active SignedIn:SignedIn LastSignedInTime:LastSignedInTime JobCount:JobCount SaleAmount:SaleAmount CommissionRate:CommissionRate PassCode:PassCode NotificationKey:NotificationKey WorkHours:WorkHours IsTardy:IsTardy WorkingDays:WorkingDays success:success failure:failure];
    [request addToPosQueue];
}

//sale reports
/*
- (void)requestGetSaleReports:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, SALRE_REPORTS_URI];
    
    GetSaleReportsRequest * request = [[GetSaleReportsRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}
 */

- (void)requestGetSaleReports:(NSString*)startDate endDate:(NSString*)EndDate  success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@?StartDate=%@&EndDate=%@&ShowDetails=true", self.apiUrl, SALE_REPORTS_URI, startDate, EndDate];
    
    
    /*
    NSString* url = @"https://www.gposdev.com/20002/api/SaleReports?StartDate=11-13-2019&EndDate=11-24-2019&ShowDetails=true";
    */
    
    GetSaleReportsRequest * request = [[GetSaleReportsRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
    
    /*
    NSString* url = @"https://www.gposdev.com/20002/api/Providers?StartDate=10-05-2019&EndDate=10-06-2019&isRestaurant=false";
    
    GetProviderReportRequest * request = [[GetProviderReportRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
     */
}


//providerReport

- (void)requestGetProviderReports:(NSString*)startDate endDate:(NSString*)EndDate  success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    NSString* url = [NSString stringWithFormat:@"%@%@?StartDate=%@&EndDate=%@", self.apiUrl, PROVIDER_URI, startDate, EndDate];
    
    //NSString* url = @"https://www.gposdev.com/20002/api/Providers?StartDate=10-05-2019&EndDate=10-06-2019&isRestaurant=false";
    
    GetProviderReportRequest * request = [[GetProviderReportRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestGetProviderDetailReports:(int)providerId startDate:(NSString*)startDate endDate:(NSString*)EndDate  success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    NSString* url = [NSString stringWithFormat:@"%@%@/%d?StartDate=%@&EndDate=%@", self.apiUrl, PROVIDER_URI, providerId, startDate, EndDate];
    
    //NSString* url = @"https://www.gposdev.com/20002/api/Providers?StartDate=10-05-2019&EndDate=10-06-2019&isRestaurant=false";
    
    GetProviderReportDetailRequest * request = [[GetProviderReportDetailRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}


//categories
- (void)requestGetCategories:(SuccessCallback)success failure:(FailureCallback)failure {
    
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, CATEGORIES_URI];
    
    GetPosCategoriesRequest* request = [[GetPosCategoriesRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestAddCategory:(NSString*)Name success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, CATEGORIES_URI];
    AddCategoryRequest* request = [[AddCategoryRequest alloc] init:url Name:Name success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestGetCategoryDetail:(int)Id success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@/%d", self.apiUrl, CATEGORIES_URI, Id];
    
    GetCategoryDetailRequest* request = [[GetCategoryDetailRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestEditCategoryDetail:(int)categoryId Name:(NSString*)Name Modifiders:(NSArray*)Modifiders updateModifier:(BOOL)updateModifier success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* isUpdateModifider = updateModifier ? @"true" : @"false";
    NSString* url = [NSString stringWithFormat:@"%@%@?updateModifier=%@", self.apiUrl, CATEGORIES_URI, isUpdateModifider];
    EditCategoryRequest * request = [[EditCategoryRequest alloc] init:url categoryId:categoryId Name:Name Modifiers:Modifiders success:success failure:failure];
    [request addToPosQueue];
}


//modifier
- (void)requestGetModifiers:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, MODIFIERS_URI];
    GetModifiderRequest *request = [[GetModifiderRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

//product
- (void)requestAddNewProduct:(NSString*)Name isActive:(BOOL)isActive image:(NSString*)image categoryId:(int)categoryId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@?categoryId=%d", self.apiUrl, LIST_PRODUCTS_URI, categoryId];
    AddNewProductRequest * request = [[AddNewProductRequest alloc] init:url Name:Name isActive:isActive image:image success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestGetListProduct:(int)listProductId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@/%d?showAll=true", self.apiUrl, LIST_PRODUCTS_URI, listProductId];
    GetListProductRequest * request = [[GetListProductRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestEditProductDetail:(int)productId Name:(NSString*)Name DislayOrder:(int)DislayOrder ImageUri:(NSString*)ImageUri NonInventory:(BOOL)NonInventory Customizable:(BOOL)Customizable IsGiftCard:(BOOL)IsGiftCard PrinterName:(NSString*)PrinterName Products:(NSArray*)Products Categories:(NSArray*)Categories success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, LIST_PRODUCTS_URI];
    EditProductDetailRequest * request = [[EditProductDetailRequest alloc] init:url productId:productId Name:Name DislayOrder:DislayOrder ImageUri:ImageUri NonInventory:NonInventory Customizable:Customizable IsGiftCard:IsGiftCard PrinterName:PrinterName Products:Products Categories:Categories success:success failure:failure];
    [request addToPosQueue];
}

//sku

- (void)requestAddNewSKU:(NSString*)Name DislayOrder:(int)DislayOrder Price:(double)Price SupplyCost:(double)SupplyCost Duration:(NSString*)Duration MinQuantity:(int)MinQuantity MaxQuantity:(int)MaxQuantity SaleTaxRate:(double)SaleTaxRate listProductId:(int)listProductId image:(NSString*)image success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@?listProductId=%d", self.apiUrl, PRODUCTS_URI, listProductId];
    AddNewSKURequest * request = [[AddNewSKURequest alloc] init:url Name:Name DislayOrder:DislayOrder Price:Price SupplyCost:SupplyCost Duration:Duration MinQuantity:MinQuantity MaxQuantity:MaxQuantity SaleTaxRate:SaleTaxRate image:image success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestEditSKU:(int)skuId listProductId:(int)listProductId firstCategoryId:(int)firstCategoryId Name:(NSString*)Name DislayOrder:(int)DislayOrder Price:(double)Price SupplyCost:(double)SupplyCost Duration:(NSString*)Duration MinQuantity:(int)MinQuantity MaxQuantity:(int)MaxQuantity SaleTaxRate:(double)SaleTaxRate image:(NSString*)image success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, PRODUCTS_URI];
    EditSkuDetailRequest * request = [[EditSkuDetailRequest alloc] init:url skuId:skuId listProductId:listProductId firstCategoryId:firstCategoryId Name:Name DislayOrder:DislayOrder Price:Price SupplyCost:SupplyCost Duration:Duration MinQuantity:MinQuantity MaxQuantity:MaxQuantity SaleTaxRate:SaleTaxRate image:image success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestGetSkuDetail:(int)skuId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@/%d", self.apiUrl, PRODUCTS_URI, skuId];
    GetSKUDetailRequest * request = [[GetSKUDetailRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestDeleteSKU:(int)SKUId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@/%d", self.apiUrl, PRODUCTS_URI, SKUId];
    
    BaseRequest* request = [[BaseRequest alloc] initDELETE:url success:success failure:failure];
    [request addToPosQueue];
}

//sale orders

- (void)requestGetSaleOrders:(SuccessCallback)success failure:(FailureCallback)failure {
    
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, SALE_ORDERS_URI];
    
    GetSaleOrdersRequest* request = [[GetSaleOrdersRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
    
}

- (void)requestGetSaleOrderDetail:(int)Id success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    NSString* url = [NSString stringWithFormat:@"%@%@/%d", self.apiUrl, SALE_ORDERS_URI, Id];
    
    GetSaleOrderDetailRequest* request = [[GetSaleOrderDetailRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
    
}

//payment reports
- (void)requestGetPaymentReports:(NSString*)startDate endDate:(NSString*)EndDate  success:(SuccessCallback)success failure:(FailureCallback)failure {
    

    NSString* url = [NSString stringWithFormat:@"%@%@?StartDate=%@&EndDate=%@", self.apiUrl, PAYMENT_REPORTS_URI, startDate, EndDate];
    
    GetPaymentReportsRequest* request = [[GetPaymentReportsRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

//settings

- (void)requestGetSettings:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, SETTINGS_URI];
    GetSettingsRequest* request = [[GetSettingsRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

- (void)requestSaveSettings:(NSMutableArray*)list success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, SETTINGS_URI];
    SaveSettingsRequest* request = [[SaveSettingsRequest alloc] init:url list:list success:success failure:failure];
    [request addToPosQueue];
}

//sale tax group

- (void)requestGetSaleTaxGroup:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", self.apiUrl, SALE_TAX_GROUP_URI];
    GetSaleTaxGroupRequest* request = [[GetSaleTaxGroupRequest alloc] initGET:url success:success failure:failure];
    [request addToPosQueue];
}

//store
- (void)requestGetStoreList:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", GPOS_ADMIN_URL, STORE_URI];
    GetStoreListRequest* request = [[GetStoreListRequest alloc] initGET:url success:success failure:failure];
    [request addToQueue];
}

- (void)requestGetGlobalStoreList:(int)page count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@?page=%d&count=%d", GPOS_ADMIN_URL, STORE_GLOBAL_URI, page, count];
   
    GetGlobalStoreListRequest* request = [[GetGlobalStoreListRequest alloc] initGET:url success:success failure:failure];
    
    [request addToQueue];
}

//sku

- (void)requestAddNewGlobalStore:(NSString*)name shopCode:(NSString*)shopCode storeId:(NSString*)storeId password:(NSString*)password active:(BOOL)active device:(NSString*)device phone:(NSString*)phone coordinateLon:(double)coordinateLon coordinateLat:(double)coordinateLat success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", GPOS_ADMIN_URL, STORE_GLOBAL_URI];
    AddGlobalStoreRequest * request = [[AddGlobalStoreRequest alloc] init:url name:name shopCode:shopCode storeId:storeId password:password active:active device:device phone:phone coordinateLon:coordinateLon coordinateLat:coordinateLat success:success failure:failure];
    [request addToQueue];
}

- (void)requestEditGlobalStore:(int)Id name:(NSString*)name shopCode:(NSString*)shopCode storeId:(NSString*)storeId password:(NSString*)password active:(BOOL)active device:(NSString*)device phone:(NSString*)phone coordinateLon:(double)coordinateLon coordinateLat:(double)coordinateLat success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", GPOS_ADMIN_URL, STORE_GLOBAL_URI];
    EditGlobalStoreRequest * request = [[EditGlobalStoreRequest alloc] init:url Id:Id name:name shopCode:shopCode storeId:storeId password:password active:active device:device phone:phone coordinateLon:coordinateLon coordinateLat:coordinateLat success:success failure:failure];
    [request addToQueue];
}

//global store owner
- (void)requestGetGlobalStoreOwnerList:(int)page count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@?page=%d&count=%d", GPOS_ADMIN_URL, STORE_OWNER_GLOBAL_URI, page, count];
    
    GetStoreOwnerListRequest* request = [[GetStoreOwnerListRequest alloc] initGET:url success:success failure:failure];
    
    [request addToQueue];
}

- (void)requestAddNewStoreOwner:(NSString*)firstName lastName:(NSString*)lastName email:(NSString*)email phone:(NSString*)phone  active:(BOOL)active storeIds:(NSArray*)storeIds success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", GPOS_ADMIN_URL, STORE_OWNER_GLOBAL_URI];
    AddStoreOwnerRequest* request = [[AddStoreOwnerRequest alloc] init:url firstName:firstName lastName:lastName email:email phone:phone active:active storeIds:storeIds success:success failure:failure];
    [request addToQueue];
}

- (void)requestEditStoreOwner:(int)Id firstName:(NSString*)firstName lastName:(NSString*)lastName email:(NSString*)email phone:(NSString*)phone password:(NSString*)password active:(BOOL)active storeIds:(NSArray*)storeIds success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@", GPOS_ADMIN_URL, STORE_OWNER_GLOBAL_URI];
    EditStoreOwnerRequest * request = [[EditStoreOwnerRequest alloc] init:url Id:Id firstName:firstName lastName:lastName email:email phone:phone password:password active:active storeIds:storeIds success:success failure:failure];
    [request addToQueue];
}

- (void)requestGetRecentSaleList:(int)months storeId:(NSString*)storeId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@?storeId=%@&months=%d", GPOS_ADMIN_URL, SALE_RECENT_URI, storeId, months];
    GetRecentSaleRequest* request = [[GetRecentSaleRequest alloc] initGET:url success:success failure:failure];
    [request addToQueue];
}

- (void)requestGetInMonthSaleList:(int)fromMonth success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* url = [NSString stringWithFormat:@"%@%@?fromMonth=%d", GPOS_ADMIN_URL, SALE_IN_MONTH_URI, fromMonth];
    GetInMonthSaleRequest* request = [[GetInMonthSaleRequest alloc] initGET:url success:success failure:failure];
    [request addToQueue];
}

@end

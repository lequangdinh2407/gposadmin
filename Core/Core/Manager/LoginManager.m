//
//  LoginManager.m
//  Core
//
//  Created by vu van long on 12/25/15.
//  Copyright © 2015 ToanPham. All rights reserved.
//

#import "LoginManager.h"

@implementation LoginManager
+ (instancetype)sharedClient {
    static LoginManager * volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)reset{
    self.nextViewController = nil;
}

- (void)gotoController:(id)next{
    self.nextViewController = next;
}
@end

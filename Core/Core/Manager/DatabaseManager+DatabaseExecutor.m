//
//  DatabaseManager+DatabaseExecutor.m
//  spabees
//
//  Created by ToanPham on 2/3/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "DatabaseManager+DatabaseExecutor.h"
#import "FMDatabase.h"
#import "CoreConstants.h"
#import "FMDatabase+FMDBHelpers.h"
#import "Helpers.h"
#import "SessionManager.h"
#import "EntityManager.h"

static NSString* const TAG = @"DatabaseManager";

@implementation DatabaseManager (Executor)

/**
 * Actions
 */

#pragma mark - User

- (void)executeInsertUser:(NSMutableDictionary*)data success:(DBSuccessCallback)success failure:(DBFailureCallback)failure {
    int result = DB_NO_ERROR;
    
    UserEntity *userEntity = (UserEntity*)[data objectForKey:DB_USER_ENTITY];
    if (userEntity != nil && [userEntity isKindOfClass:[UserEntity class]]) {
        if ([self insertUserInfo:userEntity] == 0) {
            result = DB_UNKNOW_ERROR;
        }
    } else {
        result = DB_UNKNOW_ERROR;
    }
    
    if (result != DB_NO_ERROR) {
        [Log d:0 tag:TAG message:@"Error in Insert User"];
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(nil, result);
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            success(nil, 0);
        });
    }
}

- (void)executeUpdateUser:(NSMutableDictionary*)data success:(DBSuccessCallback)success failure:(DBFailureCallback)failure {
    int result = DB_NO_ERROR;
    
    UserEntity *userEntity = (UserEntity*)[data objectForKey:DB_USER_ENTITY];
    if (userEntity != nil && [userEntity isKindOfClass:[UserEntity class]]) {
        if ([self updateUserInfo:userEntity] == 0) {
            result = DB_UNKNOW_ERROR;
        }
    } else {
        result = DB_UNKNOW_ERROR;
    }
    
    if (result != DB_NO_ERROR) {
        [Log d:0 tag:TAG message:@"Error in Update User"];
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(nil, result);
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            success(nil, 0);
        });
    }
}

#pragma mark - Login

- (void)executeInsertAllLoginInfo:(NSMutableDictionary*)data success:(DBSuccessCallback)success failure:(DBFailureCallback)failure {
    int result = DB_NO_ERROR;
    NSInteger flag = 0;
    
    @try {
        flag = [[data objectForKey:DB_FLAG_ACTION] integerValue];
    }
    @catch (NSException *exception) {
        
    }

    if ((flag & FLAG_DELETE_USER) == FLAG_DELETE_USER && result == DB_NO_ERROR) {
        if ([self deleteAllUserInfo] < 0) {
            result = DB_UNKNOW_ERROR;
        }
        
        if ([self deleteAllSocialProfile] < 0) {
            result = DB_UNKNOW_ERROR;
        }
    }

    if ((flag & FLAG_DELETE_SESSION) == FLAG_DELETE_SESSION && result == DB_NO_ERROR) {
        if ([self deleteAllSession] < 0) {
            result = DB_UNKNOW_ERROR;
        }
    }
    
    if ((flag & FLAG_DELETE_ACCOUNT) == FLAG_DELETE_ACCOUNT && result == DB_NO_ERROR) {
        if ([self deleteAllAccount] < 0) {
            result = DB_UNKNOW_ERROR;
        }
    }
    
    // insert user info
    UserEntity *userEntity = (UserEntity*)[data objectForKey:DB_USER_ENTITY];
    if (userEntity && [userEntity isKindOfClass:[UserEntity class]]) {
        if ([self insertUserInfo:userEntity] == 0) {
            result = DB_UNKNOW_ERROR;
        }
    } else {
        result = DB_UNKNOW_ERROR;
    }
    
    // insert social profile
    SocialProfileEntity *socialProfile = (SocialProfileEntity*)[data objectForKey:DB_SOCIAL_PROFILE];
    if (socialProfile && [socialProfile isKindOfClass:[SocialProfileEntity class]]) {
        if ([self insertSocialProfile:socialProfile] == 0) {
            result = DB_UNKNOW_ERROR;
        }
    } else {
        result = DB_UNKNOW_ERROR;
    }
    
    // insert session info
    NSString *sessionKey = [data objectForKey:DB_SESSION_KEY];
    NSString *refreshToken = [data objectForKey:DB_REFRESH_TOKEN];
    NSString *domain = [data objectForKey:DB_DOMAIN];
    NSString *socket = [data objectForKey:DB_SOCKET];
    NSString *uploadSocket = [data objectForKey:DB_UPLOADSOCKET];
    
    if ([self insertSession:sessionKey refreshToken:refreshToken domain:domain userId:userEntity.userId socket:socket uploadSocket:uploadSocket] == 0) {
        result = DB_UNKNOW_ERROR;
    }

    // insert account info
    AccountEntity *accountEntity = (AccountEntity*)[data objectForKey:DB_ACCOUNT_ENTITY];
    if (accountEntity && [accountEntity isKindOfClass:[AccountEntity class]])
    {
        if ([self insertAccount:accountEntity] == 0) {
            result = DB_UNKNOW_ERROR;
        }
    } else {
        result = DB_UNKNOW_ERROR;
    }

    if (result != DB_NO_ERROR) {
        [Log d:0 tag:TAG message:@"Error in InsertAllLoginInfo"];
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(nil, result);
        });
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            success(nil, 0);
        });
    }
}

- (void)executeDeleteLoginInfo:(NSMutableDictionary*)data success:(DBSuccessCallback)success failure:(DBFailureCallback)failure {
    int result = DB_NO_ERROR;
    
    NSInteger flag = [[data objectForKey:DB_FLAG_ACTION] integerValue];
    
    if ((flag & FLAG_DELETE_USER) == FLAG_DELETE_USER) {
        if ([self deleteAllUserInfo] < 0) {
            result = DB_UNKNOW_ERROR;
        }
    }
    
    if ((flag & FLAG_DELETE_SESSION) == FLAG_DELETE_SESSION) {
        if ([self deleteAllSession] < 0) {
            result = DB_UNKNOW_ERROR;
        }
    }
    
    if ((flag & FLAG_DELETE_ACCOUNT) == FLAG_DELETE_ACCOUNT) {
        if ([self deleteAllAccount] < 0) {
            result = DB_UNKNOW_ERROR;
        }
    }
    
    if (result != DB_NO_ERROR) {
        [Log d:0 tag:TAG message:@"Error in DeleteLoginInfo"];
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(nil, result);
        });
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            success(nil, 0);
        });
    }
}

#pragma mark - Init App

- (void)executeGetDataInitApp:(NSMutableDictionary*)data success:(DBSuccessCallback)success failure:(DBFailureCallback)failure {
    AccountEntity *accountEntity = [data objectForKey:DB_ACCOUNT_ENTITY];
    NSInteger flag = 0;
    
    @try {
        flag = [[data objectForKey:DB_FLAG_ACTION] integerValue];
    }
    @catch (NSException *exception) {
        
    }
    
    if ((flag & FLAG_GET_USER) == FLAG_GET_USER) {
        [self getUserInfo:accountEntity.userId];
        [self getSocialProfile:accountEntity.userId];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        success(nil, 0);
    });
}

- (void)getDataInitApp:(DBSuccessCallback)success failure:(DBFailureCallback)failure {
    dispatch_sync(self.backgroundQueue, ^{
        RLMRealm* realm = [RLMRealm defaultRealm];
        RLMResults *results = [RLMAccount allObjectsInRealm:realm];
        
        if (results.count > 0) {
            RLMAccount* rlmAccount = [results objectAtIndex:0];
            [SessionManager sharedClient].accountEntity = [[AccountEntity alloc] initWithRLMObject:rlmAccount];
            
            results = [RLMSession objectsInRealm:realm where:[NSString stringWithFormat:@"userId = '%@'", rlmAccount.userId]];
            if (results.count > 0) {
                RLMSession* rlmSession = [results objectAtIndex:0];
                [SessionManager sharedClient].sessionKey = rlmSession.sessionKey;
                [SessionManager sharedClient].refreshToken = rlmSession.refreshToken;
                [SessionManager sharedClient].socket = [SharePrefUtils getStringPreference:PREFKEY_SOCKET];
                [SessionManager sharedClient].uploadSocket = [SharePrefUtils getStringPreference:PREFKEY_UPLOAD_SOCKET];
                [SessionManager sharedClient].domain = rlmSession.domain;
                [Log d:0 tag:@"getAccountAndSession sessionKey:" message:[SessionManager sharedClient].sessionKey];
                [Log d:0 tag:@"getAccountAndSession userId:" message:rlmAccount.userId];
            }
        }
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        [data setValue:[NSNumber numberWithInteger:ACTION_GET_DATA_INIT_APP] forKey:DB_TYPE_ACTION];
        [data setValue:[SessionManager sharedClient].accountEntity forKey:DB_ACCOUNT_ENTITY];
        
        NSInteger flag = FLAG_GET_USER;
        [data setValue:[NSNumber numberWithInteger:flag] forKey:DB_FLAG_ACTION];
        
        [self executeGetDataInitApp:data success:success failure:failure];
    });
}

#pragma mark - Feed Book

- (void)executeInsertAllFeed:(NSMutableDictionary*)data success:(DBSuccessCallback)success failure:(DBFailureCallback)failure {
    int result = DB_NO_ERROR;
    
    NSInteger flag = [[data objectForKey:DB_FLAG_ACTION] integerValue];
    
    if ((flag & FLAG_DELETE_FEED) == FLAG_DELETE_FEED) {
        if ([self deleteAllFeed] < 0) {
            result = DB_UNKNOW_ERROR;
        }
    }
    
    // insert feed
    NSArray *feedList = (NSArray*)[data objectForKey:DB_FEEDS];
    
    
    if (result != DB_NO_ERROR) {
        [Log d:0 tag:TAG message:@"Error in InsertFeed"];
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(nil, result);
        });
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            success(nil, 0);
        });
    }
}

- (void)executeGetFeed:(NSMutableDictionary*)data success:(DBSuccessCallback)success failure:(DBFailureCallback)failure {
   
}

#pragma mark -

- (void)execute:(NSMutableDictionary*)data success:(DBSuccessCallback)success failure:(DBFailureCallback)failure {
    dispatch_async(self.backgroundQueue, ^{
        NSInteger typeAction = -1;
        
        @try {
            typeAction = [[data objectForKey:DB_TYPE_ACTION] integerValue];
        }
        @catch (NSException *exception) {
            
        }
        
        switch (typeAction) {
            case ACTION_INSERT_ALL_LOGIN_INFO: {
                [self executeInsertAllLoginInfo:data success:success failure:failure];
                break;
            }
            case ACTION_DELETE_LOGIN_INFO: {
                [self executeDeleteLoginInfo:data success:success failure:failure];
                break;
            }
            case ACTION_INSERT_USER: {
                [self executeInsertUser:data success:success failure:failure];
                break;
            }
            case ACTION_UPDATE_USER: {
                [self executeUpdateUser:data success:success failure:failure];
                break;
            }
                
            case ACTION_GET_DATA_INIT_APP: {
                [self executeGetDataInitApp:data success:success failure:failure];
                break;
            }
                
            case ACTION_INSERT_FEED: {
                [self executeInsertAllFeed:data success:success failure:failure];
                break;
            }
                
            case ACTION_GET_ALL_FEED: {
                [self executeGetFeed:data success:success failure:failure];
                break;
            }
                
            default:
                failure(nil, DB_UNKNOW_ERROR);
                break;
        }
    });
}

#pragma mark - User methods

- (int)insertUserInfo:(UserEntity*)userEntity {
    if ([Helpers isNotNull:userEntity] == false) {
        return 0;
    }

    /*
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *results = [RLMUser objectsInRealm:realm where:[NSString stringWithFormat:@"userId = '%@'", userEntity.userId]];
    
    RLMUser* rlmUser;
    if (results.count > 0) {
        rlmUser = [results objectAtIndex:0];
        [rlmUser loadDataFromBaseEntityWithoutChangingKey:userEntity];
    } else {
        rlmUser = [[RLMUser alloc] init];
        [rlmUser loadDataFromBaseEntity:userEntity];
    }
    [rlmUser loadDataFromBaseEntity:userEntity];
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:rlmUser];
    [realm commitWriteTransaction];
     */
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:userEntity];
    [SharePrefUtils writeObjectPreference:PREFKEY_USER_ENTITY prefValue:data];
    
    return 1;
}

- (NSInteger)updateUserInfo:(UserEntity*)userEntity {
    if ([Helpers isNotNull:userEntity] == false) {
        return 0;
    }
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:userEntity];
    [SharePrefUtils writeObjectPreference:PREFKEY_USER_ENTITY prefValue:data];
    
    /*
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *results = [RLMUser objectsInRealm:realm where:[NSString stringWithFormat:@"userId = '%@'", userEntity.userId]];
    
    RLMUser* rlmUser;
    if (results.count > 0) {
        rlmUser = [results objectAtIndex:0];
        [rlmUser loadDataFromBaseEntityWithoutChangingKey:userEntity];
    } else {
        rlmUser = [[RLMUser alloc] init];
        [rlmUser loadDataFromBaseEntity:userEntity];
    }
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:rlmUser];
    [realm commitWriteTransaction];
     */
    
    return 1;
}

- (void)getUserInfo:(NSString*)userId {
    /*
    RLMResults *results = [RLMUser objectsInRealm:[RLMRealm defaultRealm] where:[NSString stringWithFormat:@"userId = '%@'", userId]];
    
    if (results.count > 0) {
        RLMUser* rlmUser = [results objectAtIndex:0];
        UserEntity *userEntity = [[UserEntity alloc] initWithRLMObject:rlmUser];
        [SessionManager sharedClient].userEntity = userEntity;
    }
     */
    
    NSData *data = [SharePrefUtils getObjectPreference:PREFKEY_USER_ENTITY];
    UserEntity* userEntity  = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    [SessionManager sharedClient].userEntity = userEntity;
}

- (NSInteger)deleteAllUserInfo {
    
    [SharePrefUtils writeObjectPreference:PREFKEY_USER_ENTITY prefValue:nil];
    
    /*
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *results = [RLMUser allObjects];
    [realm beginWriteTransaction];
    [realm deleteObjects:results];
    [realm commitWriteTransaction];
    */
     return 1;
}

#pragma mark - Social Profile methods

- (int)insertSocialProfile:(SocialProfileEntity*)socialProfile {
    if ([Helpers isNotNull:socialProfile] == false) {
        return 0;
    }
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *results = [RLMSocialProfile objectsInRealm:realm where:[NSString stringWithFormat:@"userId = '%@'", socialProfile.userId]];
    
    RLMSocialProfile* rlmSocialProfile;
    if (results.count > 0) {
        rlmSocialProfile = [results objectAtIndex:0];
    } else {
        rlmSocialProfile = [[RLMSocialProfile alloc] init];
    }
    [rlmSocialProfile loadDataFromBaseEntity:socialProfile];
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:rlmSocialProfile];
    [realm commitWriteTransaction];
    return 1;
}

- (NSInteger)updateSocialProfile:(SocialProfileEntity*)socialProfile {
    if ([Helpers isNotNull:socialProfile] == false) {
        return 0;
    }
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *results = [RLMSocialProfile objectsInRealm:realm where:[NSString stringWithFormat:@"userId = '%@'", socialProfile.userId]];
    
    RLMSocialProfile* rlmSocialProfile;
    if (results.count > 0) {
        rlmSocialProfile = [results objectAtIndex:0];
    } else {
        rlmSocialProfile = [[RLMSocialProfile alloc] init];
    }
    [rlmSocialProfile loadDataFromBaseEntity:socialProfile];
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:rlmSocialProfile];
    [realm commitWriteTransaction];
    return 1;
}

- (void)getSocialProfile:(NSString*)userId {
    RLMResults *results = [RLMSocialProfile objectsInRealm:[RLMRealm defaultRealm] where:[NSString stringWithFormat:@"userId = '%@'", userId]];
    
    if (results.count > 0) {
        RLMSocialProfile* rlmSocialProfile = [results objectAtIndex:0];
        SocialProfileEntity *socialProfile = [[SocialProfileEntity alloc] initWithRLMObject:rlmSocialProfile];
        [SessionManager sharedClient].socialProfile = socialProfile;
    }
}

- (NSInteger)deleteAllSocialProfile {
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *results = [RLMSocialProfile allObjects];
    [realm beginWriteTransaction];
    [realm deleteObjects:results];
    [realm commitWriteTransaction];
    return 1;
}

#pragma mark - Session Methods

- (NSInteger)insertSession:(NSString*)sessionKey refreshToken:(NSString*)refreshToken domain:(NSString*)domain userId:(NSString*)userId socket:(NSString*)socket uploadSocket:(NSString*)uploadSocket {
    if ([Helpers isNotNull:sessionKey] == false ||
        [Helpers isNotNull:refreshToken] == false ||
        [Helpers isNotNull:domain] == false ||
        [Helpers isNotNull:userId] == false) {
        return 0;
    }
    
    RLMSession* rlmSession = [[RLMSession alloc] init];
    rlmSession.sessionKey = sessionKey;
    rlmSession.refreshToken = refreshToken;
    rlmSession.domain = domain;
    rlmSession.userId = userId;
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addObject:rlmSession];
    [realm commitWriteTransaction];
    
    [SharePrefUtils writeStringPreference:PREFKEY_SOCKET prefValue:socket];
    [SharePrefUtils writeStringPreference:PREFKEY_UPLOAD_SOCKET prefValue:uploadSocket];
    
    return 1;
}

- (void)getSession:(NSString*)userId {
    RLMResults *results = [RLMSession objectsInRealm:[RLMRealm defaultRealm] where:[NSString stringWithFormat:@"userId = '%@'", userId]];
    if (results.count > 0) {
        RLMSession* rlmSession = [results objectAtIndex:0];
        [SessionManager sharedClient].sessionKey = rlmSession.sessionKey;
        [SessionManager sharedClient].refreshToken = rlmSession.refreshToken;
        [SessionManager sharedClient].domain = rlmSession.domain;
        
        [SessionManager sharedClient].socket = [SharePrefUtils getStringPreference:PREFKEY_SOCKET];
        [SessionManager sharedClient].uploadSocket = [SharePrefUtils getStringPreference:PREFKEY_UPLOAD_SOCKET];
    }
}

- (NSInteger)deleteAllSession {
    RLMResults *results = [RLMSession allObjectsInRealm:[RLMRealm defaultRealm]];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteObjects:results];
    [realm commitWriteTransaction];
    
    [SharePrefUtils writeStringPreference:PREFKEY_SOCKET prefValue:nil];
    [SharePrefUtils writeStringPreference:PREFKEY_UPLOAD_SOCKET prefValue:nil];
    
    return 1;
}

#pragma mark - Account Methods

- (NSInteger)insertAccount:(AccountEntity*)account{
    if ([CoreStringUtils isEmpty:account.userId]) {
        return 0;
    }
    
    RLMAccount* rlmAccount = [[RLMAccount alloc] init];
    [rlmAccount loadDataFromBaseEntity:account];
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addObject:rlmAccount];
    [realm commitWriteTransaction];
    return 1;
}

- (void)getAccount {
}

- (NSInteger)deleteAllAccount {
    RLMResults* results = [RLMAccount allObjects];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteObjects:results];
    [realm commitWriteTransaction];
    return 1;
}

#pragma mark - Addresses Book Methods

- (NSInteger)deleteAllAddressesBook {
    static NSInteger result;
    result = 0;
    
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        result = [db deleteFrom:TABLE_ADDRESSES_BOOK where:nil arguments:nil error:nil];
    }];
    return result;
}

#pragma mark - Feed Methods

- (NSInteger)deleteAllFeed {
    static NSInteger result;
    result = 0;
    
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        result = [db deleteFrom:TABLE_FEED where:nil arguments:nil error:nil];
    }];
    return result;
}




@end

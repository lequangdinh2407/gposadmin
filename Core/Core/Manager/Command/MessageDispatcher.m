//
//  CommandDispatcher.m
//  luvkonectcore
//
//  Created by ToanPham on 3/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "MessageDispatcher.h"
#import "MessageFrame.h"
#import "CoreConstants.h"
#import "EntityManager.h"
#import "Helpers.h"
#import "SessionManager.h"

@implementation MessageDispatcher

+ (instancetype)sharedClient {
    static MessageDispatcher * volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)dispatch:(MessageFrame*)message {
    if (message == nil || ![message isKindOfClass:[MessageFrame class]])
        return;
 
    if ([message.destination isEqualToString:USER_DEMAND_DEST]) {
        NSDictionary* data = [Helpers getDictDiffNull:[message.body objectForKey:DATA_BODY_MESSAGE]];
        
        
    } else if ([message.destination isEqualToString:self.commentDest]) {
        NSDictionary* data = [[Helpers getDictDiffNull:message.body] objectForKey:RESPONSE_COMMENT];
        CommentEntity* comment = (CommentEntity*)[[EntityManager sharedClient] createItem:data type:COMMENT_ENTITY];
        
        
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(dispatchComment:)]) {
            [self.delegate dispatchComment:comment];
        }
    }
}

@end

//
//  CommandDispatcher.h
//  luvkonectcore
//
//  Created by ToanPham on 3/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SocketCommand;

@interface CommandDispatcher : NSObject

+ (instancetype)sharedClient;
- (void)dispatch:(SocketCommand*)command;

@end

//
//  CommandDispatcher.m
//  luvkonectcore
//
//  Created by ToanPham on 3/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "CommandDispatcher.h"
//#import "ChatReceiver.h"
#import "SocketCommand.h"

@implementation CommandDispatcher

+ (instancetype)sharedClient {
    static CommandDispatcher * volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)dispatch:(SocketCommand*)command {
    if (command == nil || ![command isKindOfClass:[SocketCommand class]])
        return;
    
    switch (command.cmd) {
        case ADDUSERS_CMD:
        case REMOVEUSERS_CMD:
        case TYPING_CMD:
        case RECEIVED_ACK_CMD:
        case UPDATEMESSAGESEEN_CMD:
        case UPDATEMESSAGESEENACK_CMD:
        case CHATACK_CMD:
        case CHAT_CMD:
//            [[ChatReceiver sharedClient] receive:command];
            break;
    }   
}

@end
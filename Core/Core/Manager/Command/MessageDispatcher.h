//
//  CommandDispatcher.h
//  luvkonectcore
//
//  Created by ToanPham on 3/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreConstants.h"

@class ProviderEntity;
@class PointEntity;
@class CommentEntity;
@class LandscapingEntity;
@class ResponseHomberEntity;

@protocol MessageDispatchProtocol

@optional
- (void)dispatchUserDemand:(ResponseHomberEntity*)response;
- (void)dispatchEstimateDemand:(ResponseHomberEntity*)response;
- (void)dispatchComment:(CommentEntity*)comment;

@end

@class MessageFrame;

@interface MessageDispatcher : NSObject

@property (nonatomic, weak) id delegate;
@property (nonatomic, weak) id commentDelegate;

@property (nonatomic, strong) NSString* commentDest;

+ (instancetype)sharedClient;
- (void)dispatch:(MessageFrame*)message;

@end

//
//  CommandReceiver.h
//  luvkonectcore
//
//  Created by ToanPham on 3/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SocketCommand;

@interface CommandReceiver : NSObject

+ (instancetype)sharedClient;
- (void)receive:(SocketCommand*)command;

@end

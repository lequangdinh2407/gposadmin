//
//  CommandReceiver.m
//  luvkonectcore
//
//  Created by ToanPham on 3/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "CommandReceiver.h"

@implementation CommandReceiver

+ (instancetype)sharedClient {
    static CommandReceiver * volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)receive:(SocketCommand*)command {
    
}

@end
//
//  ConversationManager.m
//  luvkonectcore
//
//  Created by ToanPham on 3/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "CommentManager.h"
#import "StompBuilder.h"
#import "MessageDispatcher.h"
#import "StompManager.h"


#define TAG @"CommentManager"

@interface CommentManager () {
    NSString* _dest;
}

@end

@implementation CommentManager

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connect) name:NOTIFICATION_CONNECT_COMMENT_SOCKET object:nil];
    
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)sharedClient {
    static CommentManager * volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
        [MessageDispatcher sharedClient].commentDelegate = _sharedManager;
    });
    
    return _sharedManager;
}

- (BOOL)isConnected {
    return [StompManager sharedClient].isConnected;
}

- (void)subscribe {
    [[RequestManager sharedClient] requestSubscribeComment:_dest subscriptionId:@"1" success:^(NSMutableDictionary *results, int errorCode) {
        [MessageDispatcher sharedClient].commentDest = _dest;
        [MessageDispatcher sharedClient].delegate = self;
    } failure:^(NSMutableDictionary *results, int errorCode) {
        
    }];
}

- (void)connect {
    if (![CoreStringUtils isEmpty:_dest]) {
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            if (![StompManager sharedClient].isConnecting) {
                [[RequestManager sharedClient] requestStompSocket:^(NSMutableDictionary *results, int errorCode) {
                    
                } failure:^(NSMutableDictionary *results, int errorCode) {
                    
                }];
            }
        });
    }
}

- (void)unsubscribe {
    [[RequestManager sharedClient] requestUnsubscribeComment:_dest subscriptionId:@"1" success:^(NSMutableDictionary *results, int errorCode) {
        
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        
    }];
}

- (void)InComment:(NSString*)dest delegate:(id<CommentManagerDelegate>)delegate {
    _dest = dest;
    self.delegate = delegate;
    
    if (![[StompManager sharedClient] isConnected]) {
        [[RequestManager sharedClient] requestStompSocket:^(NSMutableDictionary *results, int errorCode) {
            [self subscribe];
            
        } failure:^(NSMutableDictionary *results, int errorCode) {
            
        }];
    } else {
        [self subscribe];
    }
}

- (void)OutComment {
    [self unsubscribe];
}

- (void)dispatchComment:(CommentEntity*)comment {
    NSArray* comments = @[comment];
    if (self.delegate && [self.delegate respondsToSelector:@selector(receivedNewComments:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate receivedNewComments:comments];
        });
    }
}

@end

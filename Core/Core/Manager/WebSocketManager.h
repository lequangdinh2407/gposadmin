//
//  WebSocketManager.h
//  luvkonectcore
//
//  Created by ToanPham on 3/10/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SRWebSocket.h"

@class SocketCommand;
@class BaseWebSocketRequest;
@class WebSocketRequest;

typedef void (^SuccessCallback)(NSMutableDictionary *results, int errorCode);
typedef void (^FailureCallback)(NSMutableDictionary *results, int errorCode);

@interface WebSocketManager : NSObject <SRWebSocketDelegate> {
    bool isPing;
    bool isRetry;
    bool isForceClose;
    bool pingHasResponse;
    bool isPinging;
    int pingCount;
    int pingTimeOutCount;
    int timeRetryConnectCount;
}

@property (nonatomic) bool isConnected;
@property (nonatomic) bool isConnecting;
@property (nonatomic) int fibo1;
@property (nonatomic) int fibo2;
@property (nonatomic, strong) dispatch_queue_t backgroundQueue;
@property (nonatomic, strong) dispatch_queue_t chatQueue;
@property (nonatomic, strong) SuccessCallback pingSuccess;

- (instancetype)init;
+ (instancetype)sharedClient;
- (void)connect:(WebSocketRequest*)request;
- (void)close;
- (void)sendCommand:(SocketCommand*)command;
- (void)addToQueue:(BaseWebSocketRequest*)request;
- (void)sendChat:(SocketCommand*)command;
- (void)setRetry:(bool)value;

@end

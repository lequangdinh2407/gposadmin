//
//  WebSocketManager.m
//  luvkonectcore
//
//  Created by ToanPham on 3/10/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "WebSocketManager.h"
#import "CommandBuilder.h"
#import "Helpers.h"
#import "BaseWebSocketRequest.h"
#import "ChatCommand.h"
#import "ChatACKCommand.h"
#import "UpdateSeenCommand.h"
#import "AddUserCommand.h"
#import "RemoveUserCommand.h"
#import "CommandDispatcher.h"
#import "SessionManager.h"
#import "UploadSocketManager.h"
#import "WebSocketRequest.h"
#import "RequestErrorCode.h"

#define TAG @"WebSocketManager"

static int WS_PING_TIME = 20;
static int WS_PING_TIMEOUT = 15;
static int WS_REQUEST_TIME_OUT = 5;
static int WS_CONNECT_TIME_OUT = 5;
static int WS_PING_TIMEOUT_COUNT = 3;

static NSLock* requestLock;

@interface WebSocketManager ()

@property (nonatomic, strong) SRWebSocket *webSocket;
@property (nonatomic, strong) NSLock* connectLock;
@property (nonatomic, strong) NSMutableArray* connectQueue;
@property (nonatomic, strong) NSMutableArray* requestQueue;
@property (nonatomic, strong) NSMutableArray* waitingQueue;
@property (nonatomic, strong) SocketCommand* currentCommand;
@property (nonatomic, strong) NSTimer* timer;

@end

@implementation WebSocketManager

- (void)processConnectResponse:(SocketCommand*)command{
    isForceClose = false;
    self.isConnecting = false;
    [self setRetry:false];
    self.fibo1 = 1;
    self.fibo2 = 1;
    
    if (command.errorCode == 0) {
        [Log d:0 tag:TAG message:@"WebSocket connected"];
        self.isConnected = true;
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONNECT_SOCKET_SUCCESS object:nil userInfo:nil];

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//            [[ConversationManager sharedClient] resendWhenConnected];
        });
        
        [self.connectLock lock];
        if (self.connectQueue.count > 0) {
            for (int i = 0; i < self.connectQueue.count; i++) {
                WebSocketRequest* request = self.connectQueue[i];
                dispatch_async(dispatch_get_main_queue(), ^{
                    request.success(nil, command.errorCode);
                });
                [self.connectQueue removeObjectAtIndex:i];
                i--;
            }
        }
        [self.connectLock unlock];
        
        [self resetPing:true];
    }
    else {
        [Log d:0 tag:TAG message:[NSString stringWithFormat:@"WebSocket connect failed! errorCode=%d", command.errorCode]];
        self.isConnected = false;

        [self.connectLock lock];
        if (self.connectQueue.count > 0) {
            for (int i = 0; i < self.connectQueue.count; i++) {
                WebSocketRequest* request = self.connectQueue[i];
                dispatch_async(dispatch_get_main_queue(), ^{
                    request.failure(nil, command.errorCode);
                });
                [self.connectQueue removeObjectAtIndex:i];
                i--;
            }
        }
        [self.connectLock unlock];
        
        [self resetPing:false];
    }
}

- (void)processRequestResponse:(SocketCommand*)command{
    [requestLock lock];
    if (self.requestQueue.count > 0) {
        for (int i = 0; i < self.requestQueue.count; i++) {
            BaseWebSocketRequest* request = (BaseWebSocketRequest*)[self.requestQueue objectAtIndex:i];
            if (command.reqID == [request getCommand].reqID) {
                [Log d:0 tag:TAG message:[NSString stringWithFormat:@"processRequestResponse received cmd=%@, reqId=%@, errorCode=%@", @(command.cmd), @(command.reqID), @(command.errorCode)]];
                if (command.errorCode == 0) {
                    NSMutableDictionary* result = nil;
                    if (command.contentLength > 0) {
                        NSObject* jsonObj = [JSONUtils parseDataToObject:command.content];
                        [Log d:0 tag:TAG message:[NSString stringWithFormat:@"WebSocket received json=%@", jsonObj]];
                        result = [request parseNetworkRespone:jsonObj];
                    }

                    if (request.success != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            request.success(result, command.errorCode);
                        });
                    }
                } else {
                    if (request.failure != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            request.failure(nil, command.errorCode);
                        });
                    }
                }
                [self.requestQueue removeObject:request];
                break;
            }
        }
    } else {
        // TODO
    }
    [requestLock unlock];
}

- (void)processChat:(SocketCommand*)command{
    [[CommandDispatcher sharedClient] dispatch:command];    
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    dispatch_async(self.backgroundQueue, ^{
        if (message != nil && [message isKindOfClass:[NSData class]]) {
            NSData* data = (NSData*)message;
            SocketCommand *command = [[CommandBuilder sharedClient] parseDataResponse:data];
            if (command != nil) {
                [Log d:0 tag:TAG message:[NSString stringWithFormat:@"WebSocket received cmd=%@, reqId=%@, errorCode=%@", @(command.cmd), @(command.reqID), @(command.errorCode)]];
                switch (command.cmd) {
                    case KICKOUT_CMD:
                        isForceClose = true;
                        [self resetPing:false];
                        [[SessionManager sharedClient] logOut];
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_KICKOUT object:nil];
                        break;
                    case DISCONNECT_NOTIFY_CMD:
//                        isForceClose = true;
                        [self resetPing:false];
                        break;
                    case CONNECTED_CMD:
                        if (self.isConnecting == true) {
                            [self processConnectResponse:command];
                        }
                        break;
                    case PING_CMD:
                        if (self.pingSuccess != nil) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.pingSuccess(nil, 0);
                            });
                        }
                        pingHasResponse = true;
                        isPinging = false;
                        pingTimeOutCount = 0;
                        break;
                    case ADDUSERS_CMD:
                    case REMOVEUSERS_CMD:
                    case TYPING_CMD:
                    case RECEIVED_ACK_CMD:
                    case UPDATEMESSAGESEEN_CMD:
                    case UPDATEMESSAGESEENACK_CMD:
                    case CHAT_CMD:
                    case CHATACK_CMD:
                        [self processChat:command];
                        break;
                    default:
                        [self processRequestResponse:command];
                }
            }
        }
    });
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {

}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    dispatch_async(self.backgroundQueue, ^{
        [Log d:0 tag:TAG message:[NSString stringWithFormat:@":( Websocket Failed With Error %@", error]];
        [self resetPing:false];
        
        if (self.isConnected == false) {
            self.isConnecting = false;
            
            [self.connectLock lock];
            if (self.connectQueue.count > 0) {
                for (int i = 0; i < self.connectQueue.count; i++) {
                    WebSocketRequest* request = self.connectQueue[i];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        request.failure(nil, UNKNOWN_EXCEPTION);
                    });
                    [self.connectQueue removeObjectAtIndex:i];
                    i--;
                }
            }
            [self.connectLock unlock];
            
            if (isForceClose == false)
                [self setRetry:true];
        } else {
            if (error.code == 57) { // socket is not connected
                self.isConnected = FALSE;
                self.isConnecting = FALSE;
                [self setRetry:true];
            }

            if (self.currentCommand.cmd == CHAT_CMD) {
//                [[ConversationManager sharedClient] notifySendingChatError:(ChatCommand*)self.currentCommand];
            }
            if (self.requestQueue.count > 0) {
                [requestLock lock];
                for (int i = 0; i < self.requestQueue.count; i++) {
                    BaseWebSocketRequest* request = self.requestQueue[i];
                    if (request.failure != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (error.code == 57)
                                request.failure(nil, SOCKET_NOT_CONNECTED);
                            else
                                request.failure(nil, UNKNOWN_EXCEPTION);
                        });
                    }
                    [self.requestQueue removeObjectAtIndex:i];
                    i--;
                }
                [requestLock unlock];
            }
        }
    });
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"WebSocket closed! isForceClose=%d isConnected=%d isConnecting=%d", isForceClose, self.isConnected, self.isConnecting]];
    if (self.isConnecting)
        return;
    
    self.isConnected = false;
    self.isConnecting = false;
    self.fibo1 = 1;
    self.fibo2 = 1;
    [self resetPing:false];
//    [[ConversationManager sharedClient] notifySendingNotConnected];
    
    if (isForceClose == false) {
        [self setRetry:true];
    } else {
        isForceClose = false;
    }
    
    [self.connectLock lock];
    if (self.connectQueue.count > 0) {
        for (int i = 0; i < self.connectQueue.count; i++) {
            WebSocketRequest* request = self.connectQueue[i];
            dispatch_async(dispatch_get_main_queue(), ^{
                request.failure(nil, UNKNOWN_EXCEPTION);
            });
            [self.connectQueue removeObjectAtIndex:i];
            i--;
        }
    }
    [self.connectLock unlock];
    
    [requestLock lock];
    for (int i = 0; i < self.requestQueue.count; i++) {
        BaseWebSocketRequest* request = self.requestQueue[i];
        if (request.failure != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                request.failure(nil, SOCKET_NOT_CONNECTED);
            });
        }
        [self.requestQueue removeObjectAtIndex:i];
        i--;
    }
    [requestLock unlock];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload {
    [Log d:0 tag:TAG message:@"WebSocket receive pong"];
}

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.webSocket = nil;
    self.connectLock = [[NSLock alloc] init];
    self.backgroundQueue = dispatch_queue_create("com.dkm.luvkonnect.wsqueue", NULL);
    self.chatQueue = dispatch_queue_create("com.dkm.luvkonnect.wschatqueue", NULL);
    self.requestQueue = [[NSMutableArray alloc] init];
    self.connectQueue = [[NSMutableArray alloc] init];
    self.waitingQueue = [[NSMutableArray alloc] init];
    self.isConnected = false;
    self.isConnecting = false;
    self.fibo1 = 1;
    self.fibo2 = 1;
    self.pingSuccess = nil;
    isPing = false;
    isRetry = false;
    pingCount = 0;
    requestLock = [[NSLock alloc] init];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(onTimer)
                                                userInfo:nil
                                                 repeats:YES];
    return self;
}

+ (instancetype)sharedClient {
    static WebSocketManager* volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)close {
    if (self.webSocket != nil) {
        [Log d:0 tag:TAG message:@"Force close"];
        isForceClose = true;
        [self.webSocket close];
        self.webSocket = nil;
        self.isConnected = false;
        self.isConnecting = false;
        self.fibo1 = 1;
        self.fibo2 = 1;
        [self resetPing:false];
    }
}

- (void)connect:(WebSocketRequest*)request {
    dispatch_async(self.backgroundQueue, ^{
        [self.connectLock lock];
        if (!self.isConnecting) {
            [self.connectQueue addObject:request];
            self.isConnected = false;
            self.isConnecting = true;
            [self resetPing:false];
            [self setRetry:false];
            request.timeOutCount = 0;
            isForceClose = 0;
            NSString* url = [request getURL];
            self.webSocket = (SRWebSocket*)[[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
            self.webSocket.delegate = self;
            [self.webSocket open];
            [Log d:0 tag:TAG message:@"Start connecting"];
        } else {
            [self.connectQueue addObject:request];
            [Log d:0 tag:TAG message:@"connecting"];
        }
        [self.connectLock unlock];
    });
}

- (void)onPing {
    if (isPing) {
        pingCount++;
        if (pingCount >= WS_PING_TIME) {
            if (!self.isConnected) {
                [Log d:0 tag:TAG message:@"Ping Failed: WebSocket not connected!"];
            } else {
                NSDictionary* dict = [[NSDictionary alloc] initWithObjectsAndKeys:@(PING_CMD) ,HDR_CMD, nil];
                SocketCommand* command = [[CommandBuilder sharedClient] createCommand:dict type:PING_CMD];
                [self sendCommand:command];
            }
            pingHasResponse = false;
            isPinging = true;
            pingCount = 0;
        }
        if (pingCount >= WS_PING_TIMEOUT) {
            if (isPinging == true) {
                if (pingHasResponse == false) {
                    pingTimeOutCount++;
                    if (pingTimeOutCount > WS_PING_TIMEOUT_COUNT) {
                        [Log d:0 tag:TAG message:@"Ping Failed: Time-out! Reconnecting"];
                        [self resetPing:false];
                        self.isConnected = false;
                        self.isConnecting = false;
                        [[SessionManager sharedClient] setSessionExpiredWebSocket];
                    }
                }
            }
            isPinging = false;
        }
    }
}

- (void)onConnecting {
    if (self.isConnecting) {
        [self.connectLock lock];
        if (self.connectQueue.count > 0) {
            for (int i = 0; i < self.connectQueue.count; i++) {
                WebSocketRequest* request = self.connectQueue[i];
                request.timeOutCount++;
                if (request.timeOutCount >= WS_CONNECT_TIME_OUT) {
                    [Log d:0 tag:TAG message:@"Connect Time-Out!"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        request.failure(nil, REQUEST_TIMEOUT);
                    });
                    [self.connectQueue removeObjectAtIndex:i];
                    i--;
                }
            }
            if (self.connectQueue.count == 0) {
                self.isConnected = false;
                self.isConnecting = false;
                [self setRetry:true];
            }
        }
        [self.connectLock unlock];
    }
}

- (void)onRetry {
    if (isRetry) {
        timeRetryConnectCount++;
        if (timeRetryConnectCount > self.fibo1 + self.fibo2) {
            self.fibo2 = self.fibo1 + self.fibo2;
            self.fibo1 = self.fibo2 - self.fibo1;
            timeRetryConnectCount = 0;
            [self resetPing:false];
            [Log d:0 tag:TAG message:@"Connect failed! Retry"];
            [self setRetry:false];
            [[SessionManager sharedClient] setSessionExpiredWebSocket];
        }
    }
}

- (void)onRequest {
    [requestLock lock];
    if (self.requestQueue.count > 0) {
        for (int i = 0; i < self.requestQueue.count; i++) {
            BaseWebSocketRequest* request = self.requestQueue[i];
            request.timeOutCount++;
            if (request.timeOutCount >= WS_REQUEST_TIME_OUT) {
                SocketCommand* command = [request getCommand];
                [Log d:0 tag:TAG message:[NSString stringWithFormat:@"Request Time-Out! with cmd=%@, reqId=%@", @(command.cmd), @(command.reqID)]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    request.failure(nil, REQUEST_TIMEOUT);
                });
                [self.requestQueue removeObjectAtIndex:i];
                i--;
            }
        }
    }
    [requestLock unlock];
}

- (void)onTimer{
    [self onPing];
    [self onConnecting];
    [self onRetry];
    [self onRequest];
}

- (void)sendCommand:(SocketCommand*)command {
    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"Send cmd=%@ reqId=%@", @(command.cmd), @(command.reqID)]];
    self.currentCommand = command;
    [self.webSocket send:[command toData]];
}

- (void)addToQueue:(BaseWebSocketRequest*)request {
    if (!self.isConnected) {
        [Log d:0 tag:TAG message:@"WebSocket not connected!"];
        dispatch_async(dispatch_get_main_queue(), ^{
            request.failure(nil, SOCKET_NOT_CONNECTED);
        });
        return;
    }
    
    dispatch_async(self.backgroundQueue, ^{
        [requestLock lock];
        [self.requestQueue addObject:request];
        request.timeOutCount = 0;
        [self sendCommand:[request getCommand]];
        [requestLock unlock];
    });
}

- (void)sendChat:(SocketCommand*)command {
    if (!self.isConnected) {
        [Log d:0 tag:TAG message:@"WebSocket not connected!"];
//        if ([command isKindOfClass:[ChatCommand class]])
//            [[ConversationManager sharedClient] notifySendingNotConnected];
        return;
    }
    
//    if ([command isKindOfClass:[ChatCommand class]])
//        ((ChatCommand*)command).message.waiting = 0;
    
    dispatch_async(self.chatQueue, ^{
        [self sendCommand:command];
    });
}

- (void)resetPing:(bool)isReset {
    if (isReset == true) {
        isPing = true;
    } else {
        isPing = false;
    }
    isPinging = false;
    pingCount = 0;
    pingHasResponse = false;
}

- (void)setRetry:(bool)value {
    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"Set Retry with value=%d fibo1=%d fibo2=%d", value, self.fibo1, self.fibo2]];
    if (value == true) {
        isRetry = true;
    } else {
        isRetry = false;
    }
    timeRetryConnectCount = 0;
}

@end
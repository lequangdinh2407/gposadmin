//
//  SessionManager.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserEntity.h"
#import "AccountEntity.h"
#import "GTMSessionFetcher.h"
#import "GTMSessionFetcherService.h"
#import "SocialProfileEntity.h"


@class OIDAuthState;
@class GTMAppAuthFetcherAuthorization;
@class OIDServiceConfiguration;

@interface SessionManager : NSObject

@property (nonatomic, strong) UserEntity* userEntity;
@property (nonatomic, strong) SocialProfileEntity* socialProfile;
@property (nonatomic, strong) AccountEntity* accountEntity;
@property (nonatomic, strong) NSString* sessionKey;
@property (nonatomic, strong) NSString* refreshToken;
@property (nonatomic, strong) NSString* domain;
@property (nonatomic, strong) NSString* socket;
@property (nonatomic, strong) NSString* uploadSocket;

@property (nonatomic, nullable) GTMAppAuthFetcherAuthorization *authorization;
@property (nonatomic, strong) NSArray* statusArray;
@property (nonatomic, strong) NSArray* scheduleStatusArray;

+ (instancetype)sharedClient;
- (instancetype)init;
- (void)setSessionExpired;
- (void)setSessionExpiredWebSocket;
- (void)clearAll;
- (void)insertDBLoginInfo;
- (void)deleteDBLoginInfo;
- (void)logOut;

@end

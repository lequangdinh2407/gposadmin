//
//  RequestManager.h
//  Core
//
//  Created by ToanPham on 2/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"
#import "RequestErrorCode.h"

static NSString* const SPABEE_URL = @"http://api.spabee.com";

static NSString* const GPOS_ADMIN_URL = @"http://45.55.39.77/gpos";

//@"http://45.55.39.77/pos";
static NSString* const HOMBER_UPLOAD_URL = @"http://45.55.39.77/pos";

static NSString* const GOOGLE_MAP_URL = @"https://maps.googleapis.com/maps/api/geocode/json?address=";
static NSString* const GOOGLE_PLACE_URL = @"https://maps.googleapis.com/maps/api/place/details/json?placeid=";
static NSString* const GOOGLE_API_KEY = @"AIzaSyBGnn5yWHvYrUXnT0nTcHtkKYVJhNKc_Xg";

static NSString* const LOOP_URL = @"http://usavall.com";
//spabee
//static NSString* const API_KEY =  @"e51d9051e24fbcfa0ecdc9c943c7ae8a";
//static NSString* const SECRECT_KEY = @"a9740eb258207e44cc531a15daaae3f2";

static NSString* const API_KEY =  @"e51d9051e24fbcfa0ecdc9c943c7ae8a";
static NSString* const SECRECT_KEY = @"a9740eb258207e44cc531a15daaae3f2";

/* GPOS ADMIN URL */
static NSString* const PROVIDER_URI = @"Providers";
static NSString* const SALE_REPORTS_URI = @"SaleReports";
static NSString* const CUSTOMERS_URI = @"Customers";
static NSString* const DEVICE_USERS_URI = @"DeviceUsers";
static NSString* const CATEGORIES_URI = @"Categories";
static NSString* const SALE_ORDERS_URI = @"SaleOrders";
static NSString* const PAYMENT_REPORTS_URI = @"PaymentReports";
static NSString* const SETTINGS_URI = @"Settings";
static NSString* const LIST_PRODUCTS_URI = @"ListProducts";
static NSString* const PRODUCTS_URI = @"Products";
static NSString* const MODIFIERS_URI = @"Modifiers";
static NSString* const SALE_TAX_GROUP_URI = @"SaleTaxGroups";
static NSString* const IMAGES_URI= @"Images";



static NSString* const STORE_URI= @"/store";
static NSString* const STORE_GLOBAL_URI= @"/store/global";
static NSString* const STORE_OWNER_GLOBAL_URI= @"/store-owner/global";
static NSString* const UPDATE_PROFILE_PASSWORD = @"/profile/password";
static NSString* const SALE_RECENT_URI = @"/sale/recent";
static NSString* const SALE_IN_MONTH_URI = @"/sale/in-month";

static NSString* const REQUEST_CODE_URI = @"/loop/user/requestcode";
static NSString* const CHECK_VALID_CODE_URI = @"/loop/user/validcode";
static NSString* const ACCEPT_LICENSE_URI = @"/loop/user/acceptlicense";
static NSString* const REGISTER_URI = @"/register/social";
static NSString* const LOGIN_URI = @"/login/email";
static NSString* const LOGOUT_URI = @"/logout";
static NSString* const REFRESH_TOKEN_URI = @"/login/token";
static NSString* const CHANGE_PASS_URI = @"/tradeshow/password/change";
static NSString* const RESET_PASS_URI = @"/tradeshow/password/reset";
static NSString* const SUBMIT_TOKEN_PNS_URI = @"/device";
static NSString* const ACCEPT_LICENSE_FB_URI = @"/tradeshow/user/fb/acceptlicense";
static NSString* const REGISTER_FB_URI = @"/tradeshow/user/fb/register";
static NSString* const LOGIN_FB_URI = @"/tradeshow/user/fb/login";
static NSString* const ACCEPT_LICENSE_GG_URI = @"/tradeshow/user/gg/acceptlicense";
static NSString* const REGISTER_GG_URI = @"/tradeshow/user/gg/register";
static NSString* const LOGIN_GG_URI = @"/tradeshow/user/gg/login";
static NSString* const SEND_FEEDBACK_URI = @"/tradeshow/feedback/send";
static NSString* const CONTACT_US_URI = @"/tradeshow/contactus/send";
static NSString* const GET_LIST_COUNTRIES_V2_URI = @"/address/country";
static NSString* const GET_LIST_STATES_URI = @"/tradeshow/state/getlist";
static NSString* const GET_LIST_CITIES_URI = @"/tradeshow/city/getlist";
static NSString* const FIND_ADDRESS_URI = @"/api/mobile/utilities/findzip";

/* Loop URI */
static NSString* const UPLOAD_USER_AVATAR = @"/profile/avatar";

/* BConnect URI */
static NSString* const UPDATE_USER_INFO_URI = @"/tradeshow/user/update";
static NSString* const GET_WISHLIST_URI = @"/tradeshow/wishlist/list";
static NSString* const GET_UNREAD_NEWS_COUNT_URI = @"/news/new";
static NSString* const GET_NEWS_URI = @"/news";
static NSString* const VIEW_NEWS_URI = @"/news/view";
static NSString* const GET_LIST_RELATIONSHIP_URI = @"/tradeshow/user/reaction/list";
static NSString* const GET_COUNT_RELATIONSHIP_URI = @"/tradeshow/social/user/profile/statistics";

/* Feed URI */
static NSString* const POST_FEED_URI = @"/feed";
static NSString* const PARSE_LINK_URI = @"/feed/link/parse";
static NSString* const UPLOAD_PHOTO_URI = @"/tradeshow/upload/feed/photo";
static NSString* const HIDE_FEED_URI = @"/tradeshow/social/feed/hide";
static NSString* const HIDE_FEEDS_FROM_USER_URI = @"/tradeshow/social/feed/hffu";
static NSString* const REPORT_FEED_URI = @"/tradeshow/social/feed/report";
static NSString* const GET_NUMBER_NEW_FEEDS_URI = @"/tradeshow/social/feed/new/count";
static NSString* const GET_NEW_FEEDS_URI = @"/tradeshow/social/feed/new/list";

/* Comment URI */
static NSString* const GET_LIST_COMMENTS_URI = @"/tradeshow/comment/list";
static NSString* const GET_NEW_COMMENTS_URI = @"/tradeshow/social/feed/comment/new/list";
static NSString* const GET_SOCKET_COMMENT_URL_URI = @"/tradeshow/socket/comment/geturl";

/* Biz URI */
static NSString* const GET_BANNER_URI = @"/tradeshow/banner/get";

/* Event URI */
static NSString* const GET_EVENT_CATEGORY_URI = @"/tradeshow/event/category";
static NSString* const GET_LIST_EVENTS_URI = @"/tradeshow/event/list";
static NSString* const GET_EVENT_DETAIL_URI = @"/tradeshow/event/detail";
static NSString* const SUGGEST_EVENT_URI = @"/tradeshow/suggest";
static NSString* const SEARCH_EVENTS_URI = @"/tradeshow/search";
static NSString* const VIEW_TS_URI = @"/view";
static NSString* const LIKE_TS_URI = @"/like";
static NSString* const FOLLOW_TS_URI = @"/tradeshow/follow";
static NSString* const SHARE_TS_URI = @"/share";
static NSString* const RATING_TS_URI = @"/tradeshow/rating/addate";
static NSString* const DELETE_RATING_TS_URI = @"/tradeshow/rating/delete";
static NSString* const ADD_TS_COMMENT_URI = @"/tradeshow/comment/add";
static NSString* const EDIT_TS_COMMENT_URI = @"/tradeshow/comment/edit";
static NSString* const DELETE_TS_COMMENT_URI = @"/tradeshow/comment/delete";
static NSString* const GET_LIST_TS_COMMENT_URI = @"/tradeshow/comment/list";
static NSString* const CHECK_ONLINE_EVENT_URI = @"/tradeshow/event/online";

/* Booth URI */
static NSString* const GET_BOOTH_CATEGORY_URI = @"/tradeshow/booth/category/list";
static NSString* const GET_LIST_FLOORS_URI = @"/tradeshow/floor/list";
static NSString* const GET_LIST_BOOTHS_URI = @"/tradeshow/booth/list";
static NSString* const GET_BOOTH_DETAIL_URI = @"/tradeshow/booth/detail";
static NSString* const GET_BOOTH_PRODUCT_URI = @"/tradeshow/product/list";
static NSString* const GET_PRODUCT_DETAIL_URI = @"/tradeshow/product/detail";
static NSString* const GET_CURRENT_LOCATION_URI = @"/tradeshow/location/indoor/get";
static NSString* const GET_ROUTE_URI = @"/tradeshow/route/find";
static NSString* const GET_TOP_BOOTH_AT_MAP_LEVEL = @"/tradeshow/booth/live/top";
static NSString* const GET_NEAREAST_LOCATION = @"/tradeshow/location/indoor/near";

/* Payment URI */
static NSString* const GET_CARD_DEFAULT_URI = @"/card";
static NSString* const GET_CARD_URI = @"/card/list";
static NSString* const GET_CARD_TOKEN_URI = @"/card/token";
static NSString* const ADD_CARD_URI = @"/card";

/* Garden URI */
static NSString* const GET_HOME_URI = @"/garden/home";
static NSString* const ADD_GARDEN_URI = @"/garden/add";
static NSString* const GET_GARDEN_URI = @"/garden/%@/detail";
static NSString* const GET_HISTORY_URI = @"/garden/history";
static NSString* const ADD_MAP_URI = @"/garden/map";
static NSString* const CHECK_BOOK_URI = @"/garden/book/check";
static NSString* const BOOK_MAP_URI = @"/garden/book";
static NSString* const GET_GARDEN_ADDRESS_URI = @"/garden/address";
static NSString* const EDIT_HOME_URI = @"/garden/home";
static NSString* const GET_RECENT_GARDEN = @"/garden/";
static NSString* const GET_SCHEDULE_URI = @"/garden/schedule";
static NSString* const SCHEDULE_URI = @"/garden/schedule";
static NSString* const POST_GARDEN_LOCATION_URI = @"/garden/location";
static NSString* const PUT_GARDEN_BORDER_URI = @"/garden/border";

/*PROFILE URL*/
static NSString* const UPDATE_PROFILE = @"/profile";

/* Rating URL */
static NSString* const RATING_URI = @"/rating";

/* PROMOTION URL */
static NSString* const ADD_PROMOTION_CODE_URI = @"/promotion";
static NSString* const GET_PROMOTION_DEFAULT_URI = @"/promotion/default";
static NSString* const GET_PROMOTION_DETAIL_URI = @"/promotion";


/* HAPPY CHECKIN URL */

static NSString* const QR_URI = @"/gpos/qr";
static NSString* const SALON_URI = @"/gpos/salon";
static NSString* const REWARD_URI = @"/gpos/reward";
static NSString* const CHECKIN_URI = @"/gpos/checkin";
static NSString* const DELETE_ACCOUNT_URI = @"/profile/delete";



@interface RequestManager : NSObject

@property (nonatomic, strong) NSString* apiUrl;
@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) NSString* storeName;
@property (nonatomic, strong) NSString* password;
@property (nonatomic, strong) NSString* storeId;
@property (nonatomic, strong) NSString* Id;

@property (nonatomic, strong) NSMutableArray *waitingQueueRequest;
- (instancetype)init;
+ (instancetype)sharedClient;
- (void)addToWaitingQueue:(BaseRequest*)request;
- (void)addWaitingQueueToRealQueue;

- (void)requestRequestCode:(NSInteger)userType sendInfo:(NSString*)sendInfo imei:(NSString*)imei countryCode:(NSString*)countryCode success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestCheckValidCode:(NSInteger)userType sendInfo:(NSString*)sendInfo activeCode:(NSString*)activeCode imei:(NSString*)imei countryCode:(NSString*)countryCode success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestAcceptLicense:(NSInteger)userType sendInfo:(NSString*)sendInfo activeCode:(NSString*)activeCode imei:(NSString*)imei countryCode:(NSString*)countryCode success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestLoginEmail:(NSString*)name password:(NSString*)password success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestLogin:(NSString*)token type:(int)type lon:(float)lon lat:(float)lat imei:(NSString*)imei success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestLogout:(NSString*)deviceId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestRefreshToken:(NSString*)refreshToken lon:(float)lon lat:(float)lat success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestRegister:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestCountriesListV2Request:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestStatesListRequest:(NSString*)isoCode success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestCitiesListRequest:(NSString*)isoCode success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestSubmitTokenPNS:(NSString*)sessionKey imei:(NSString*)imei token:(NSString*)token success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetUserInfo:(NSString*)userId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestChangePassword:(int)Id curPass:(NSString*)curPass newPass:(NSString*)newPass rePass:(NSString*)rePass type:(uint8_t)type success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestResetPassword:(NSString*)userName imei:(NSString*)imei success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestAcceptFBLicense:(NSString*)accessToken imei:(NSString*)imei success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestRegisterFB:(NSString*)accessToken acceptLicense:(int)accept imei:(NSString*)imei mobilePhone:(NSString*)mobilePhone success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestLoginFB:(NSString*)token success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestAcceptGGLicense:(NSString*)accessToken imei:(NSString*)imei success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestRegisterGG:(NSString*)accessToken acceptLicense:(int)accept imei:(NSString*)imei mobilePhone:(NSString*)mobilePhone success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestLoginGG:(NSString*)token success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestSendFeedback:(NSString*)sessionKey message:(NSString*)message success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestContactParams:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestFindAddress:(NSString*)zipCode success:(SuccessCallback)success failure:(FailureCallback)failure;


- (NSString*)getThumbTemplateUrl2:(NSString*)url;
- (NSString*)getTemplateUrl2:(NSString*)url;
- (NSString*)getServicePhoto:(NSString*)url;
- (NSString*)getServicePhotoThumb:(NSString*)url;
- (void)requestGetAddress:(NSString*)address success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetPlaceDetailFormPlaceId:(NSString*)address success:(SuccessCallback)success failure:(FailureCallback)failure;

/* LOOP */

- (void)requestUpdateUserInfo:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestUploadUserAvatar:(NSData*)file success:(SuccessCallback)success failure:(FailureCallback)failure progress:(ProgressCallback)progress;


/* BCONNECT */
- (void)requestGetUnreadNewsCount:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetNews:(int)page count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestViewNews:(NSString*)userId ids:(NSArray*)ids success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetListRelationship:(NSString*)sessionKey userId:(NSString*)userId type:(int)type page:(int)page count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetCountRelationship:(NSString*)sessionKey userId:(NSString*)userId success:(SuccessCallback)success failure:(FailureCallback)failure;

#pragma mark - Wishlist API Request
- (void)requestGetWishList:(NSString*)sessionKey objectType:(int)objectType lon:(float)lon lat:(float)lat page:(uint16_t)page count:(uint16_t)count success:(SuccessCallback)success failure:(FailureCallback)failure;

#pragma mark - Upload API Request
- (void)requestSendUploadInfo:(uint16_t)totalPart size:(uint64_t)size type:(uint8_t)type success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestUploadSocket:(NSString*)sessionKey roomId:(NSString*)roomId url:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestSendFile:(uint64_t)uploadID partID:(uint32_t)partID data:(NSData*)data success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestUploadPhoto:(NSString*)sessionKey fileName:(NSString*)fileName file:(NSData*)file success:(SuccessCallback)success failure:(FailureCallback)failure progress:(ProgressCallback)progress;
- (void)requestStompSocket:(SuccessCallback)success failure:(FailureCallback)failure;

#pragma mark - Feed API Request
- (void)requestPostFeed:(NSArray*)feedPhotos feedLink:(NSString*)feedLink feedMessage:(NSString*)feedMessage feedSticker:(NSString*)feedSticker success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestEditFeed:(NSString*)sessionKey feedId:(NSString*)feedId feedPhotos:(NSArray*)feedPhotos feedLink:(NSString*)feedLink feedMessage:(NSString*)feedMessage feedSticker:(NSString*)feedSticker success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestDeleteFeed:(NSString*)sessionKey feedId:(NSString*)feedId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetListFeeds:(int)type page:(uint16_t)page count:(uint16_t)count userId:(NSString*)userId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetFeedDetail:(NSString*)sessionKey feedId:(NSString*)feedId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetSubFeed:(NSString*)sessionKey feedId:(NSString*)feedId photoId:(NSString*)photoId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestParseLink:(NSString*)sessionKey link:(NSString*)link success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestHideFeed:(NSString*)sessionKey feedId:(NSString*)feedId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestHideFeedsFromUser:(NSString*)sessionKey userId:(NSString*)userId type:(int)type success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestReportFeed:(NSString*)feedId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetNumberNewFeeds:(int)type feedId:(NSString*)feedId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetNewFeeds:(int)type feedId:(NSString*)feedId count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure;

#pragma mark - Comment API Request
- (void)requestGetNewFeedComments:(NSString*)sessionKey feedId:(NSString*)feedId photoId:(NSString*)photoId commentId:(NSString*)commentId page:(uint16_t)page count:(uint16_t)count success:(SuccessCallback)success failure:(FailureCallback)failure;

#pragma mark - Place API
- (void)requestSuggestPlace:(NSString*)name countryCode:(NSString*)countryCode count:(uint16_t)count success:(SuccessCallback)success failure:(FailureCallback)failure;


#pragma mark - Event API



- (void)requestViewTS:(int)objectType objectId:(NSString*)objectId photoId:(NSString*)photoId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestLikeTS:(int)objectType objectId:(NSString*)objectId photoId:(NSString*)photoId type:(int)type success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestFollowTS:(NSString*)sessionKey objectType:(int)objectType objectId:(NSString*)objectId photoId:(NSString*)photoId type:(int)type success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestShareTS:(int)objectType objectId:(NSString*)objectId message:(NSString*)message platformId:(NSString*)platformId facebookId:(NSString*)facebookId success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestAddUpdateTSRating:(NSString*)sessionKey objectType:(int)objectType objectId:(NSString*)objectId point:(float)point success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestDeleteTSRating:(NSString*)sessionKey objectType:(int)objectType objectId:(NSString*)objectId success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestGetListComments:(int)objectType objectId:(NSString*)objectId feedPhotoId:(NSString*)feedPhotoId page:(int)page count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestAddTSComment:(int)objectType objectId:(NSString*)objectId feedPhotoId:(NSString*)feedPhotoId photoId:(NSString*)photoId text:(NSString*)text sticker:(NSString*)sticker success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestEditComment:(NSString*)commentId text:(NSString*)text success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestDeleteTSComment:(NSString*)sessionKey commentId:(NSString*)commentId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetListReactionUsers:(int)objectType objectId:(NSString*)objectId photoId:(NSString*)photoId reactType:(int)reactType page:(int)page count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure;

#pragma mark - Service API
- (void)requestGetService:(double)square gardenId:(NSString*)gardenId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetDefaultService:(double)square gardenId:(NSString*)gardenId success:(SuccessCallback)success failure:(FailureCallback)failure;

#pragma mark - Payment API
- (void)requestGetCardDefault:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetCard:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetCardToken:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestAddCard:(NSString*)nonce success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestDeleteCard:(NSString*)cardId success:(SuccessCallback)success failure:(FailureCallback)failure;



#pragma mark - - GET BANNER API
- (void)requestGetBanners:(SuccessCallback)success failure:(FailureCallback)failure;

#pragma mark - Rating API
- (void)requestSubmitRating:(NSString*)lanscapingId comment:(NSString*)comment point:(double)point userId:(NSString*)userId success:(SuccessCallback)success failure:(FailureCallback)failure;

#pragma mark - Promotion API
- (void)requestGetPromotionDefault:(NSString*)gardenId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestAddPromotionCode:(NSString*)code success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetInviteCode:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetPromotion:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetPromotionDetail:(NSString*)promotionId success:(SuccessCallback)success failure:(FailureCallback)failure;

#pragma mark - Stomp API
- (void)requestSubscribeUser:(NSString*)subscriptionId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestUnsubscribeUser:(NSString*)subscriptionId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestSubscribeComment:(NSString*)commentDest subscriptionId:(NSString*)subscriptionId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestUnsubscribeComment:(NSString*)commentDest subscriptionId:(NSString*)subscriptionId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestUpdateStastus:(ProviderStatusEnum)status success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestAcceptEstimate:(BOOL)isAccept success:(SuccessCallback)success failure:(FailureCallback)failure;



#pragma mark - HAPPY CHECKING API
- (void)requestGetQrCode:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetSalonInfo:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestCheckin:(double)longtitude latitude:(double)latitude success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestGetRewardPoint:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)requestDeleteAccount:(SuccessCallback)success failure:(FailureCallback)failure;

#pragma mark - POS ADMIN API

//customer
- (void)requestGetCustomers:(SuccessCallback)success failure:(FailureCallback)failure;

//device-user
- (void)requestGetDeviceUsers:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestAddDeviceUser:(NSString*)UserName Email:(NSString*)Email PassCode:(NSString*)PassCode IsCashier:(BOOL)IsCasiher IsSupervisor:(BOOL)IsSupervisor IsController:(BOOL)IsController IsManager:(BOOL)IsManager success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestEditDeviceUser:(int)deviceUserId UserName:(NSString*)UserName Email:(NSString*)Email PassCode:(NSString*)PassCode IsCashier:(BOOL)IsCasiher IsSupervisor:(BOOL)IsSupervisor IsController:(BOOL)IsController IsManager:(BOOL)IsManager success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestDeleteDeviceUser:(NSString*)username success:(SuccessCallback)success failure:(FailureCallback)failure;

//provider
- (void)requestGetProvider:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestGetProviderDetail:(int)providerId success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestAddProvider:(NSString*)Name Phone:(NSString*)Phone Active:(BOOL)Active SignedIn:(BOOL)SignedIn LastSignedInTime:(NSString*)LastSignedInTime JobCount:(int)JobCount SaleAmount:(double)SaleAmount CommissionRate:(double)CommissionRate PassCode:(NSString*)PassCode NotificationKey:(NSString*)NotificationKey WorkHours:(double)WorkHours IsTardy:(BOOL)IsTardy success:(SuccessCallback)success failure:(FailureCallback)failure;


- (void)requestEditProvider:(int)Id Name:(NSString*)Name Phone:(NSString*)Phone Active:(BOOL)Active SignedIn:(BOOL)SignedIn LastSignedInTime:(NSString*)LastSignedInTime JobCount:(int)JobCount SaleAmount:(double)SaleAmount CommissionRate:(double)CommissionRate PassCode:(NSString*)PassCode NotificationKey:(NSString*)NotificationKey WorkHours:(double)WorkHours IsTardy:(BOOL)IsTardy WorkingDays:(NSString*)WorkingDays success:(SuccessCallback)success failure:(FailureCallback)failure;

//payment reports
- (void)requestGetPaymentReports:(NSString*)startDate endDate:(NSString*)EndDate  success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestGetProviderDetailReports:(int)providerId startDate:(NSString*)startDate endDate:(NSString*)EndDate  success:(SuccessCallback)success failure:(FailureCallback)failure;

// provider report
- (void)requestGetProviderReports:(NSString*)startDate endDate:(NSString*)EndDate  success:(SuccessCallback)success failure:(FailureCallback)failure;

//categories
- (void)requestGetCategories:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestAddCategory:(NSString*)Name success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestGetCategoryDetail:(int)Id success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestEditCategoryDetail:(int)categoryId Name:(NSString*)Name Modifiders:(NSArray*)Modifiders updateModifier:(BOOL)updateModifier success:(SuccessCallback)success failure:(FailureCallback)failure;

//modifier
- (void)requestGetModifiers:(SuccessCallback)success failure:(FailureCallback)failure;

//product
- (void)requestAddNewProduct:(NSString*)Name isActive:(BOOL)isActive image:(NSString*)image categoryId:(int)categoryId success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestGetListProduct:(int)listProductId success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestEditProductDetail:(int)productId Name:(NSString*)Name DislayOrder:(int)DislayOrder ImageUri:(NSString*)ImageUri NonInventory:(BOOL)NonInventory Customizable:(BOOL)Customizable IsGiftCard:(BOOL)IsGiftCard PrinterName:(NSString*)PrinterName Products:(NSArray*)Products Categories:(NSArray*)Categories success:(SuccessCallback)success failure:(FailureCallback)failure;

//sku
- (void)requestAddNewSKU:(NSString*)Name DislayOrder:(int)DislayOrder Price:(double)Price SupplyCost:(double)SupplyCost Duration:(NSString*)Duration MinQuantity:(int)MinQuantity MaxQuantity:(int)MaxQuantity SaleTaxRate:(double)SaleTaxRate listProductId:(int)listProductId image:(NSString*)image success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestGetSkuDetail:(int)skuId success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestEditSKU:(int)skuId listProductId:(int)listProductId firstCategoryId:(int)firstCategoryId Name:(NSString*)Name DislayOrder:(int)DislayOrder Price:(double)Price SupplyCost:(double)SupplyCost Duration:(NSString*)Duration MinQuantity:(int)MinQuantity MaxQuantity:(int)MaxQuantity SaleTaxRate:(double)SaleTaxRate image:(NSString*)image success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestDeleteSKU:(int)SKUId success:(SuccessCallback)success failure:(FailureCallback)failure;

//sale reports
//- (void)requestGetSaleReports:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestGetSaleReports:(NSString*)startDate endDate:(NSString*)EndDate  success:(SuccessCallback)success failure:(FailureCallback)failure;

//sale order


- (void)requestGetSaleOrders:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestGetSaleOrderDetail:(int)Id success:(SuccessCallback)success failure:(FailureCallback)failure;

//settings

- (void)requestGetSettings:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestSaveSettings:(NSMutableArray*)list success:(SuccessCallback)success failure:(FailureCallback)failure;


//saleTaxGroup
- (void)requestGetSaleTaxGroup:(SuccessCallback)success failure:(FailureCallback)failure;

//images

- (void)requestUploadImage:(NSData*)file success:(SuccessCallback)success failure:(FailureCallback)failure progress:(ProgressCallback)progress;

//store
- (void)requestGetStoreList:(SuccessCallback)success failure:(FailureCallback)failure;

//global store
- (void)requestGetGlobalStoreList:(int)page count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestAddNewGlobalStore:(NSString*)name shopCode:(NSString*)shopCode storeId:(NSString*)storeId password:(NSString*)password active:(BOOL)active device:(NSString*)device phone:(NSString*)phone coordinateLon:(double)coordinateLon coordinateLat:(double)coordinateLat success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestEditGlobalStore:(int)Id name:(NSString*)name shopCode:(NSString*)shopCode storeId:(NSString*)storeId password:(NSString*)password active:(BOOL)active device:(NSString*)device phone:(NSString*)phone coordinateLon:(double)coordinateLon coordinateLat:(double)coordinateLat success:(SuccessCallback)success failure:(FailureCallback)failure;

//global store owner
- (void)requestGetGlobalStoreOwnerList:(int)page count:(int)count success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestAddNewStoreOwner:(NSString*)firstName lastName:(NSString*)lastName email:(NSString*)email phone:(NSString*)phone active:(BOOL)active storeIds:(NSArray*)storeIds success:(SuccessCallback)success failure:(FailureCallback)failure;


- (void)requestEditStoreOwner:(int)Id firstName:(NSString*)firstName lastName:(NSString*)lastName email:(NSString*)email phone:(NSString*)phone password:(NSString*)password active:(BOOL)active storeIds:(NSArray*)storeIds success:(SuccessCallback)success failure:(FailureCallback)failure;

//SALE
- (void)requestGetRecentSaleList:(int)months storeId:(NSString*)storeId success:(SuccessCallback)success failure:(FailureCallback)failure;

- (void)requestGetInMonthSaleList:(int)fromMonth success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

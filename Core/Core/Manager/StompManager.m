//
//  WebSocketManager.m
//  luvkonectcore
//
//  Created by ToanPham on 3/10/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "StompManager.h"
#import "StompBuilder.h"
#import "Helpers.h"
#import "MessageDispatcher.h"
#import "SessionManager.h"
#import "RequestErrorCode.h"
#import "StompFrame.h"
#import "BaseStompRequest.h"
#import "StompRequest.h"
#import "ReceiptFrame.h"
#import "MessageFrame.h"

#define TAG @"StompManager"

static int WS_PING_TIME = 5;
static int WS_PING_TIMEOUT = 10;
static int WS_REQUEST_TIME_OUT = 10;
static int WS_CONNECT_TIME_OUT = 10;
static int WS_PING_TIMEOUT_COUNT = 2;

static NSLock* requestLock;

static NSString *const SRHTTPResponseErrorKey = @"HTTPResponseStatusCode";

@interface StompManager ()

@property (nonatomic, strong) SRWebSocket *webSocket;
@property (nonatomic, strong) NSLock* connectLock;
@property (nonatomic, strong) NSMutableArray* connectQueue;
@property (nonatomic, strong) NSMutableArray* requestQueue;
@property (nonatomic, strong) NSMutableArray* waitingQueue;
@property (nonatomic, strong) StompFrame* currentFrame;
@property (nonatomic, strong) NSTimer* timer;

@end

@implementation StompManager

- (void)processConnectResponse:(StompFrame*)frame{
    isForceClose = false;
    self.isConnecting = false;
    [self setRetry:false];
    self.fibo1 = 1;
    self.fibo2 = 1;
    
    [Log d:0 tag:TAG message:@"Stomp connected"];
    self.isConnected = true;
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONNECT_STOMP_SUCCESS object:nil userInfo:nil];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//        [[ConversationManager sharedClient] resendWhenConnected];
    });

    [self.connectLock lock];
    if (self.connectQueue.count > 0) {
        for (int i = 0; i < self.connectQueue.count; i++) {
            StompRequest* request = self.connectQueue[i];
            dispatch_async(dispatch_get_main_queue(), ^{
                request.success(nil, 0);
            });
            [self.connectQueue removeObjectAtIndex:i];
            i--;
        }
    }
    [self.connectLock unlock];

    [self resetPing:true];
}

- (void)processRequestResponse:(StompFrame*)frame{
    [requestLock lock];
    if (self.requestQueue.count > 0) {
        for (int i = 0; i < self.requestQueue.count; i++) {
            BaseStompRequest* request = (BaseStompRequest*)[self.requestQueue objectAtIndex:i];
            if ([frame isKindOfClass:[ReceiptFrame class]]) {
                if ([request.receiptId isEqualToString:((ReceiptFrame*)frame).receiptId]) {
//                    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"processRequestResponse received cmd=%@, header=%@, body=%@", frame.command, frame.headersMap, frame.body]];
                    NSMutableDictionary* result = nil;
                    
                    if (request.success != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            request.success(result, 0);
                        });
                    }
                    [self.requestQueue removeObject:request];
                    break;
                }
            }
        }
    }
            
    [requestLock unlock];
}

- (void)processMessage:(StompFrame*)frame{
    if ([frame isKindOfClass:[MessageFrame class]]) {
        MessageFrame* message = (MessageFrame*)frame;       
        [[MessageDispatcher sharedClient] dispatch:message];
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    dispatch_async(self.backgroundQueue, ^{
        if (message != nil && [message isKindOfClass:[NSString class]]) {
            NSString* data = (NSString*)message;
            
            if ([data isEqualToString:@"\n"]) {
                [Log d:0 tag:TAG message:[NSString stringWithFormat:@"WebSocket received pingHasResponse"]];
                pingHasResponse = YES;
                return;
            }
            
            StompFrame *frame = [[StompBuilder sharedClient] parseDataResponse:data];
            if (frame != nil) {
                [Log d:0 tag:TAG message:[NSString stringWithFormat:@"WebSocket received cmd=%@, header=%@, body=%@", frame.command, frame.headersMap, frame.body]];
                
                if ([frame.command isEqualToString:STOMP_CONNECTED_CMD]) {
                    if (self.isConnecting == true) {
                        [self processConnectResponse:frame];
                    }
                    
                } else if ([frame.command isEqualToString:RECEIPT_CMD]) {
                    if (self.isConnected == true) {
                        [self processRequestResponse:frame];
                    }
                    
                } else if ([frame.command isEqualToString:MESSAGE_CMD]) {
                    if (self.isConnected == true) {
                        [self processMessage:frame];
                    }
                    
                } else {

                }
            }
        }
    });
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    [Log d:0 tag:TAG message:@"Websocket didOpen"];
    NSString* hearbeat = [NSString stringWithFormat:@"%d,%d", WS_PING_TIME * 3 * 1000, WS_PING_TIMEOUT * 1000];
    NSDictionary* header = @{@"heart-beat" : hearbeat, @"accept-version" : @"1.1,1.2"};
    StompFrame* frame = [[StompBuilder sharedClient] createFrame:CONNECT_CMD headers:header body:nil];
//    [[StompManager sharedClient] sendFrame:frame];
    self.currentFrame = frame;
    NSString* str = [frame toString];
    [self.webSocket send:str];
    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"Send frame=%@", str]];
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    dispatch_async(self.backgroundQueue, ^{
        [Log d:0 tag:TAG message:[NSString stringWithFormat:@":( Websocket Failed With Error %@", error]];
        [self resetPing:false];
        
        NSInteger responseCode = [Helpers getUIntDiffNull:[error.userInfo objectForKey:SRHTTPResponseErrorKey]];
        if (responseCode == 401) {
            [Log d:0 tag:TAG message:[NSString stringWithFormat:@"huydna websocket receive 401"]];
            [[SessionManager sharedClient] setSessionExpiredWebSocket];
        }
        
        if (self.isConnected == false) {
            self.isConnecting = false;
            
            [self.connectLock lock];
            if (self.connectQueue.count > 0) {
                for (int i = 0; i < self.connectQueue.count; i++) {
                    StompRequest* request = self.connectQueue[i];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        request.failure(nil, UNKNOWN_EXCEPTION);
                    });
                    [self.connectQueue removeObjectAtIndex:i];
                    i--;
                }
            }
            [self.connectLock unlock];
            
            if (isForceClose == false)
                [self setRetry:true];
        } else {
            if (error.code == 57) { // socket is not connected
                self.isConnected = FALSE;
                self.isConnecting = FALSE;
                [self setRetry:true];
            } else if (error.code == 2145 || error.code == 54) { // off/on app causes socket error
                self.isConnected = FALSE;
                self.isConnecting = FALSE;
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONNECT_STOMP object:nil userInfo:nil];
            }

//            if (self.currentFrame.cmd == CHAT_CMD) {
//                [[ConversationManager sharedClient] notifySendingChatError:(ChatCommand*)self.currentFrame];
//            }
            if (self.requestQueue.count > 0) {
                [requestLock lock];
                for (int i = 0; i < self.requestQueue.count; i++) {
                    BaseStompRequest* request = self.requestQueue[i];
                    if (request.failure != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (error.code == 57)
                                request.failure(nil, SOCKET_NOT_CONNECTED);
                            else
                                request.failure(nil, UNKNOWN_EXCEPTION);
                        });
                    }
                    [self.requestQueue removeObjectAtIndex:i];
                    i--;
                }
                [requestLock unlock];
            }
        }
    });
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"WebSocket didCloseWithCode:%ld isForceClose=%d isConnected=%d isConnecting=%d", code, isForceClose, self.isConnected, self.isConnecting]];
    if (self.isConnecting)
        return;
    
    self.isConnected = false;
    self.isConnecting = false;
    self.fibo1 = 1;
    self.fibo2 = 1;
    [self resetPing:false];
//    [[ConversationManager sharedClient] notifySendingNotConnected];
    
    if (isForceClose == false) {
        [self setRetry:true];
    } else {
        isForceClose = false;
    }
    
    [self.connectLock lock];
    if (self.connectQueue.count > 0) {
        for (int i = 0; i < self.connectQueue.count; i++) {
            StompRequest* request = self.connectQueue[i];
            dispatch_async(dispatch_get_main_queue(), ^{
                request.failure(nil, UNKNOWN_EXCEPTION);
            });
            [self.connectQueue removeObjectAtIndex:i];
            i--;
        }
    }
    [self.connectLock unlock];
    
    [requestLock lock];
    for (int i = 0; i < self.requestQueue.count; i++) {
        BaseStompRequest* request = self.requestQueue[i];
        if (request.failure != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                request.failure(nil, SOCKET_NOT_CONNECTED);
            });
        }
        [self.requestQueue removeObjectAtIndex:i];
        i--;
    }
    [requestLock unlock];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload {
    [Log d:0 tag:TAG message:@"WebSocket receive pong"];
}

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.webSocket = nil;
    self.connectLock = [[NSLock alloc] init];
    self.backgroundQueue = dispatch_queue_create("com.dkm.luvkonnect.wsqueue", NULL);
    self.chatQueue = dispatch_queue_create("com.dkm.luvkonnect.wschatqueue", NULL);
    self.requestQueue = [[NSMutableArray alloc] init];
    self.connectQueue = [[NSMutableArray alloc] init];
    self.waitingQueue = [[NSMutableArray alloc] init];
    self.isConnected = false;
    self.isConnecting = false;
    self.fibo1 = 1;
    self.fibo2 = 1;
    self.pingSuccess = nil;
    isPing = false;
    isRetry = false;
    pingCount = 0;
    serverPingCount = 0;
    pingTimeOutCount = 0;
    requestLock = [[NSLock alloc] init];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(onTimer)
                                                userInfo:nil
                                                 repeats:YES];
    return self;
}

+ (instancetype)sharedClient {
    static StompManager* volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)close {
    if (self.webSocket != nil) {
        [Log d:0 tag:TAG message:@"Force close"];
        isForceClose = true;
        [self.webSocket close];
        self.webSocket = nil;
        self.isConnected = false;
        self.isConnecting = false;
        self.fibo1 = 1;
        self.fibo2 = 1;
        [self resetPing:false];
    }
}

- (void)connect:(StompRequest*)request {
    dispatch_async(self.backgroundQueue, ^{
        [self.connectLock lock];
        if (!self.isConnecting) {
            [self.connectQueue addObject:request];
            self.isConnected = false;
            self.isConnecting = true;
            [self resetPing:false];
            [self setRetry:false];
            request.timeOutCount = 0;
            isForceClose = 0;
            NSString* url = [request getURL];
            NSMutableURLRequest* urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            [urlRequest setValue:[SessionManager sharedClient].sessionKey forHTTPHeaderField:@"Session"];
            self.webSocket = (SRWebSocket*)[[SRWebSocket alloc] initWithURLRequest:urlRequest];
            self.webSocket.delegate = self;
            [self.webSocket open];
            [Log d:0 tag:TAG message:@"Start connecting"];
        } else {
            [self.connectQueue addObject:request];
            [Log d:0 tag:TAG message:@"connecting"];
        }
        [self.connectLock unlock];
    });
}

- (void)onPing {
    if (isPing) {
        pingCount++;
        if (pingCount >= WS_PING_TIME) {
            if (!self.isConnected) {
                [Log d:0 tag:TAG message:@"Ping Failed: WebSocket not connected!"];
            } else {
                [self.webSocket send:@"\n"];
            }
//            pingHasResponse = false;
//            isPinging = true;
            pingCount = 0;
        }
        
        serverPingCount++;
        if (serverPingCount >= WS_PING_TIMEOUT) {
            if (isPinging == true) {
                if (pingHasResponse == false) {
                    pingTimeOutCount++;
                    if (pingTimeOutCount >= WS_PING_TIMEOUT_COUNT) {
                        [Log d:0 tag:TAG message:@"Ping Failed: Time-out! Reconnecting"];
                        [self resetPing:false];
                        self.isConnected = false;
                        self.isConnecting = false;
//                        [[SessionManager sharedClient] setSessionExpiredWebSocket];
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONNECT_STOMP object:nil userInfo:nil];
                        isPinging = false;
                    }
                } else {
                    serverPingCount = 0;
                    pingTimeOutCount = 0;
                    pingHasResponse = FALSE;
                }
            }
        }
    }
}

- (void)onConnecting {
    if (self.isConnecting) {
        [self.connectLock lock];
        if (self.connectQueue.count > 0) {
            for (int i = 0; i < self.connectQueue.count; i++) {
                BaseStompRequest* request = self.connectQueue[i];
                request.timeOutCount++;
                if (request.timeOutCount >= WS_CONNECT_TIME_OUT) {
                    [Log d:0 tag:TAG message:@"Connect Time-Out!"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        request.failure(nil, REQUEST_TIMEOUT);
                    });
                    [self.connectQueue removeObjectAtIndex:i];
                    i--;
                }
            }
            if (self.connectQueue.count == 0) {
                self.isConnected = false;
                self.isConnecting = false;
                [self setRetry:true];
            }
        }
        [self.connectLock unlock];
    }
}

- (void)onRetry {
    if (isRetry) {
        timeRetryConnectCount++;
        if (timeRetryConnectCount > self.fibo1 + self.fibo2) {
            self.fibo2 = self.fibo1 + self.fibo2;
            self.fibo1 = self.fibo2 - self.fibo1;
            timeRetryConnectCount = 0;
            [self resetPing:false];
            [Log d:0 tag:TAG message:@"Connect failed! Retry"];
            [self setRetry:false];
//            [[SessionManager sharedClient] setSessionExpiredWebSocket];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONNECT_STOMP object:nil userInfo:nil];
        }
    }
}

- (void)onRequest {
    [requestLock lock];
    if (self.requestQueue.count > 0) {
        for (int i = 0; i < self.requestQueue.count; i++) {
            BaseStompRequest* request = self.requestQueue[i];
            request.timeOutCount++;
            if (request.timeOutCount >= WS_REQUEST_TIME_OUT) {
                StompFrame* command = [request getFrame];
                [Log d:0 tag:TAG message:[NSString stringWithFormat:@"Request Time-Out! with cmd=%@, header=%@", command.command, command.headersMap]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    request.failure(nil, REQUEST_TIMEOUT);
                });
                [self.requestQueue removeObjectAtIndex:i];
                i--;
            }
        }
    }
    [requestLock unlock];
}

- (void)onTimer{
    [self onPing];
    [self onConnecting];
    [self onRetry];
    [self onRequest];
}

- (void)sendFrame:(StompFrame*)frame {
    if (self.isConnected) {
        self.currentFrame = frame;
        NSString* str = [frame toString];
        [self.webSocket send:str];
        [Log d:0 tag:TAG message:[NSString stringWithFormat:@"Send frame=%@", str]];
    }
}

- (void)addToQueue:(BaseStompRequest*)request {
    if (!self.isConnected) {
        [Log d:0 tag:TAG message:@"WebSocket not connected!"];
        dispatch_async(dispatch_get_main_queue(), ^{
            request.failure(nil, SOCKET_NOT_CONNECTED);
        });
        return;
    }

    dispatch_async(self.backgroundQueue, ^{
        [requestLock lock];
        [self.requestQueue addObject:request];
        request.timeOutCount = 0;
        [self sendFrame:[request getFrame]];
        [requestLock unlock];
    });
}

- (void)resetPing:(bool)isReset {
    if (isReset == true) {
        isPing = true;
    } else {
        isPing = false;
    }
    isPinging = false;
    pingCount = 0;
    serverPingCount = 0;
    pingTimeOutCount = 0;
    pingHasResponse = false;
}

- (void)setRetry:(bool)value {
    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"Set Retry with value=%d fibo1=%d fibo2=%d", value, self.fibo1, self.fibo2]];
    if (value == true) {
        isRetry = true;
    } else {
        isRetry = false;
    }
    timeRetryConnectCount = 0;
}

@end

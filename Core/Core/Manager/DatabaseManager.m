//
//  DatabaseManager.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/24/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.


#import "DatabaseManager.h"
#import "FMDatabase.h"
#import "CoreConstants.h"
#import "EntityManager.h"
#import "FMDatabase+FMDBHelpers.h"
#import "SessionManager.h"
#import "CoreStringUtils.h"
#import "JSONUtils.h"
#import "EntityManager.h"
#import "DatabaseManager+DatabaseExecutor.h"
#import "Helpers.h"

@implementation DatabaseManager

static NSArray* userColumn = nil;
static NSArray* sessionColumn = nil;
static NSArray* accountColumn = nil;
static NSArray* addressesColumn = nil;
static NSArray* feedColumn = nil;

+ (instancetype)sharedClient {
    static DatabaseManager * volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    [self initColumn];
    
    self.backgroundQueue = dispatch_queue_create("com.dkm.loop.databasequeue", NULL);
    self.databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[self databasePath]];
    [self createTable];
    return self;
}

- (void) initColumn {
    if (userColumn == nil) {
        userColumn = [NSArray arrayWithObjects:(COLUMN_USER_ID ATTRIBUTE_PRIMARY_KEY), (COLUMN_FIRST_NAME ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_LAST_NAME ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_EMAIL ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_BIRTHDAY ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_ADDRESS ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_CITY ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_STATE ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_COUNTRY ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_COUNTRY_CODE ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_ZIPCODE ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_MOBILEPHONE ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_STATUS ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_AVATAR ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_GENDER ATTRIBUTE_INTEGER), (COLUMN_POINT ATTRIBUTE_INTEGER), (COLUMN_LICENSE_CODE ATTRIBUTE_TEXT_NOT_NULL), nil];
    }
    
    if (sessionColumn == nil) {
        sessionColumn = [NSArray arrayWithObjects:(COLUMN_USER_ID ATTRIBUTE_PRIMARY_KEY), (COLUMN_SESSION_KEY ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_DOMAIN ATTRIBUTE_TEXT_NOT_NULL), nil];
    }
    
    if (accountColumn == nil) {
        accountColumn = [NSArray arrayWithObjects:(COLUMN_USER_ID ATTRIBUTE_PRIMARY_KEY), (COLUMN_USER_NAME ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_PASSWORD ATTRIBUTE_TEXT_NOT_NULL), nil];
    }
    
    if (addressesColumn == nil) {
        addressesColumn = [NSArray arrayWithObjects:(COLUMN_ADDRESS_ID ATTRIBUTE_PRIMARY_KEY), (COLUMN_FIRST_NAME ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_LAST_NAME ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_ADDRESSLINE1 ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_ADDRESSLINE2 ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_CITY ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_STATE ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_COUNTRY ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_ZIPCODE ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_MOBILEPHONE ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_COUNTRY_CODE ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_DEFAULT_SHIPPING_ADDRESS ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_DEFAULT_BILLING_ADDRESS ATTRIBUTE_TEXT_NOT_NULL), nil];
    }
    
    if (feedColumn == nil) {
        feedColumn = [NSArray arrayWithObjects:(COLUMN_FEED_ID ATTRIBUTE_PRIMARY_KEY), (COLUMN_FEED_ITEM_ID ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_USER_ID ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_AVATAR ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_DISPLAY_NAME ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_CREATE_TIME ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_UPDATE_TIME ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_STICKER ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_MESSAGE ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_PHOTOS ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_FEED_LINK ATTRIBUTE_TEXT_NOT_NULL), (COLUMN_TYPE ATTRIBUTE_INTEGER), (COLUMN_LIKES ATTRIBUTE_INTEGER), (COLUMN_LOVES ATTRIBUTE_INTEGER), (COLUMN_HAHAS ATTRIBUTE_INTEGER), (COLUMN_WOWS ATTRIBUTE_INTEGER), (COLUMN_ANGRIES ATTRIBUTE_INTEGER), (COLUMN_SADS ATTRIBUTE_INTEGER), (COLUMN_COMMENTS ATTRIBUTE_INTEGER), (COLUMN_SHARES ATTRIBUTE_INTEGER), nil];
    }
}

- (void)createTable {
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        [db createTableWithName:TABLE_USER columns:userColumn constraints:nil error:nil];
        [db createTableWithName:TABLE_SESSION columns:sessionColumn constraints:nil error:nil];
        [db createTableWithName:TABLE_ACCOUNT columns:accountColumn constraints:nil error:nil];
        [db createTableWithName:TABLE_ADDRESSES_BOOK columns:addressesColumn constraints:nil error:nil];
        [db createTableWithName:TABLE_FEED columns:feedColumn constraints:nil error:nil];
    }];
}

- (NSString *)databasePath {
    NSString *docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    return [docsPath stringByAppendingPathComponent:@"LuvKonect.db"];
}

- (void)getAccountAndSession:(DBSuccessCallback)success failure:(DBFailureCallback)failure {    
    [self getDataInitApp:success failure:failure];
}

- (void)addToQueue:(NSMutableDictionary*)data success:(DBSuccessCallback)success failure:(DBFailureCallback)failure {
    if (data != nil && [data isKindOfClass:[NSMutableDictionary class]]) {
        [self execute:data success:success failure:failure];
    } else {       
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(nil, DB_UNKNOW_ERROR);
        });
    }    
}
@end

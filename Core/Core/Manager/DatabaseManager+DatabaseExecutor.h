//
//  DatabaseManager+DatabaseExecutor.h
//  spabees
//
//  Created by ToanPham on 2/3/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "DatabaseManager.h"
#import "EntityManager.h"
#import "SessionManager.h"
#import "RLMUser.h"
#import "RLMSocialProfile.h"
#import "RLMAccount.h"
#import "RLMSession.h"

@interface DatabaseManager (Executor)

- (void)execute:(NSMutableDictionary*)data success:(DBSuccessCallback)success failure:(DBFailureCallback)failure;

@end

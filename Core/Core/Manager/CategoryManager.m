//
//  CategoryManager.m
//  Core
//
//  Created by ToanPham on 12/22/15.
//  Copyright © 2015 ToanPham. All rights reserved.
//

#import "CategoryManager.h"
#import "EntityManager.h"

@implementation CategoryManager

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.eventCategoryList = [[NSMutableArray alloc] init];
    self.boothCategoryList = [[NSMutableArray alloc] init];
    
    return self;
}

+ (instancetype)sharedClient {
    static CategoryManager* volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)clearAllWhenUpdate {
    
}

- (void)clearAll {
    if (self.eventCategoryList != nil)
        [self.eventCategoryList removeAllObjects];
    if (self.boothCategoryList != nil)
        [self.boothCategoryList removeAllObjects];
    [self clearAllWhenUpdate];
}

@end
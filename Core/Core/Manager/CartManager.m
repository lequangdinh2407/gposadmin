//
//  CartManager.m
//  Core
//
//  Created by ToanPham on 1/5/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "CartManager.h"
#import "RequestManager.h"
#import "SessionManager.h"
#import "CartEntity.h"
#import "SKUEntity.h"
#import "CartItemEntity.h"
//#import "PromotionCombinationEntity.h"

@implementation CartManager

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.cartSize = 0;
    self.entity = nil;
    
    return self;
}

+ (instancetype)sharedClient {
    static CartManager* volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)reset {
    self.cartSize = 0;
    self.entity = nil;
}

- (void)getCartSize:(SuccessCallback)success failure:(FailureCallback)failure {
//    [[RequestManager sharedClient] requestGetCartSize:[SessionManager sharedClient].sessionKey success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"getCartSize success" message:results];
//        self.cartSize = [Helpers getUIntDiffNull:[results objectForKey:RESPONSE_TOTAL_QUANTITY]];
//        success(results, errorCode);
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"getCartSize failure" message:results];
//        failure(results, errorCode);
//    }];
}

- (void)addCart:(NSString*)skuID quantity:(int)quantity success:(SuccessCallback)success failure:(FailureCallback)failure {
//    [[RequestManager sharedClient] requestAddCart:[SessionManager sharedClient].sessionKey SKUId:skuID quantity:quantity success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"addCart success" message:results];
//        int quantity = [Helpers getUIntDiffNull:[results objectForKey:RESPONSE_TOTAL_QUANTITY]];
//        self.cartSize = quantity;
//        self.entity = [results objectForKey:RESPONSE_CART];
//        success(results, errorCode);
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"addCart failure" message:results];
//        failure(results, errorCode);
//    }];
}

- (void)getCart:(SuccessCallback)success failure:(FailureCallback)failure {
//    [[RequestManager sharedClient] requestGetCart:[SessionManager sharedClient].sessionKey success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"CartManager get cart success" message:results];
//        self.entity = (CartEntity*)[results objectForKey:RESPONSE_CART];
//        self.cartSize = self.entity.totalQuantity;
//        success(results, errorCode);
//        
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"CartManager get cart failure" message:results];
//        failure(results, errorCode);
//    }];
}

- (void)removeCart:(NSString*)skuID success:(SuccessCallback)success failure:(FailureCallback)failure {
//    [[RequestManager sharedClient] requestRemoveCart:[SessionManager sharedClient].sessionKey SKUId:skuID success:^(NSMutableDictionary *results, int errorCode) {
//        [self reset];
//        self.cartSize = [Helpers getUIntDiffNull:[results objectForKey:RESPONSE_TOTAL_QUANTITY]];
//        self.entity = [results objectForKey:RESPONSE_CART];
//        [Log d:0 tag:@"removeCart success" message:results];
//        success(results, errorCode);
//        
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"removeCart failure" message:results];
//        failure(results, errorCode);
//    }];
}

- (void)updateCart:(NSString*)skuID quantity:(int)quantity success:(SuccessCallback)success failure:(FailureCallback)failure {
//    [[RequestManager sharedClient] requestUpdateCart:[SessionManager sharedClient].sessionKey SKUId:skuID quantity:quantity success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"updateCart success" message:results];
//        [self reset];
//        self.cartSize = [Helpers getUIntDiffNull:[results objectForKey:RESPONSE_TOTAL_QUANTITY]];
//        self.entity = [results objectForKey:RESPONSE_CART];
//        success(results, errorCode);
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"updateCart failure" message:results];
//        failure(results, errorCode);
//    }];
}

- (void)addPromoCode:(NSString*)promoCode success:(SuccessCallback)success failure:(FailureCallback)failure {
//    [[RequestManager sharedClient] requestAddPromoCode:[SessionManager sharedClient].sessionKey promoCode:promoCode success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"addPromoCode success" message:results];
//        self.entity = [results objectForKey:RESPONSE_CART];
//        self.entity.promoCode = promoCode;        
//        success(results, errorCode);
//    } failure:failure];
}

- (void)addPromoReception:(PromotionCombinationEntity*)promoCombination success:(SuccessCallback)success failure:(FailureCallback)failure {
//    NSString* promoIds = [promoCombination getPromotionIds];
//    NSString* repIds = [promoCombination getReceptionIds];
//    NSString* skuIds = [promoCombination getSkuIds];
//    NSString* quantities = [promoCombination getQuantities];
//    
//    [[RequestManager sharedClient] requestAddPromoReception:[SessionManager sharedClient].sessionKey promotionIds:promoIds receptionIds:repIds skuIds:skuIds quantities:quantities success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"addPromoReception success" message:results];
//        self.entity = [results objectForKey:RESPONSE_CART];
//        success(results, errorCode);
//    } failure:failure];
}

- (void)updatePromoReception:(PromotionCombinationEntity*)promoCombination success:(SuccessCallback)success failure:(FailureCallback)failure {
//    NSString* promoIds = [promoCombination getPromotionIds];
//    NSString* repIds = [promoCombination getReceptionIds];
//    NSString* skuIds = [promoCombination getSkuIds];
//    NSString* quantities = [promoCombination getQuantities];
//    
//    [[RequestManager sharedClient] requestUpdatePromoReception:[SessionManager sharedClient].sessionKey promotionIds:promoIds receptionIds:repIds skuIds:skuIds quantities:quantities success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"updatePromoReception success" message:results];
//        self.entity = [results objectForKey:RESPONSE_CART];
//        success(results, errorCode);
//    } failure:failure];
}

- (void)clearPromoReception:(SuccessCallback)success failure:(FailureCallback)failure {
//    [[RequestManager sharedClient] requestClearPromoReception:[SessionManager sharedClient].sessionKey  success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"clearPromoReception success" message:results];
//        self.entity = [results objectForKey:RESPONSE_CART];
//        success(results, errorCode);
//    } failure:failure];
}

@end

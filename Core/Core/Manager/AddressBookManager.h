//
//  AddressBookManager.h
//  Core
//
//  Created by ToanPham on 1/4/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TiffinyAddressEntity.h"

@interface AddressBookManager : NSObject

typedef void (^SuccessCallback)(NSMutableDictionary *results, int errorCode);
typedef void (^FailureCallback)(NSMutableDictionary *results, int errorCode);

@property (nonatomic, strong) NSMutableArray* shippingAddressList;
@property (nonatomic, strong) NSMutableArray* billingAddressList;
@property (nonatomic, strong) TiffinyAddressEntity* defaultShipping;
@property (nonatomic, strong) TiffinyAddressEntity* defaultBilling;

+ (instancetype)sharedClient;
- (instancetype)init;
- (void)getShippingAddress:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)getBillingAddress:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)addShippingAddress:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)addBillingAddress:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)deleteShippingAddress:(NSString*)addressId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)deleteBillingAddress:(NSString*)addressId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)updateShippingAddress:(TiffinyAddressEntity*)entity isDefault:(BOOL)isDefault success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)updateBillingAddress:(TiffinyAddressEntity*)entity isDefault:(BOOL)isDefault success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)updateShippingAddress:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)updateBillingAddress:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)reset;

@end

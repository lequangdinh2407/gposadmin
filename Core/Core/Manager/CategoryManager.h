//
//  CategoryManager.h
//  Core
//
//  Created by ToanPham on 12/22/15.
//  Copyright © 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderedDictionary.h"

@interface CategoryManager : NSObject

@property (nonatomic, strong) NSMutableArray* eventCategoryList;
@property (nonatomic, strong) NSMutableArray* boothCategoryList;

+ (instancetype)sharedClient;
- (instancetype)init;
- (void)clearAllWhenUpdate;
- (void)clearAll;

@end
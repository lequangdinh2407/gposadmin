//
//  AlertviewManager.m
//  Core
//
//  Created by vu van long on 6/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "AlertviewManager.h"
#import "CoreStringUtils.h"

@implementation AlertviewManager

+ (instancetype)sharedClient {
    static AlertviewManager* volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    return self;
}

- (void)showMessage:(NSString*)message delegate:(id)delegate{
    [self showTitleMessage:@"" message:message delegate:delegate cancelButton:[CoreStringUtils getStringRes:@"str_ok"] otherButton:nil];
}

- (void)showTitleMessage:(NSString*)title message:(NSString*)message delegate:(id)delegate{
    [self showTitleMessage:title message:message delegate:delegate cancelButton:[CoreStringUtils getStringRes:@"str_ok"] otherButton:nil];
}

- (void)showTitleMessage:(NSString*)title message:(NSString*)message delegate:(id)delegate cancelButton:(NSString*)cancel otherButton:(NSString*)otherButton{
    self.alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancel otherButtonTitles:otherButton, nil];
    [self.alertView show];
}

- (void)dismissMessageWithAnimation:(BOOL)animation{
    self.alertView.delegate = nil;
    [self.alertView dismissWithClickedButtonIndex:-1 animated:animation];
    self.alertView = nil;
}

@end

//
//  CartManager.h
//  Core
//
//  Created by ToanPham on 1/5/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CartEntity;
@class PromotionCombinationEntity;

@interface CartManager : NSObject

typedef void (^SuccessCallback)(NSMutableDictionary *results, int errorCode);
typedef void (^FailureCallback)(NSMutableDictionary *results, int errorCode);

@property (nonatomic) int cartSize;
@property (strong, nonatomic) CartEntity* entity;

+ (instancetype)sharedClient;
- (instancetype)init;
- (void)reset;
- (void)getCartSize:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)addCart:(NSString*)skuID quantity:(int)quantity success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)getCart:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)removeCart:(NSString*)skuID success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)updateCart:(NSString*)skuID quantity:(int)quantity success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)addPromoCode:(NSString*)promoCode success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)addPromoReception:(PromotionCombinationEntity*)promoCombination success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)updatePromoReception:(PromotionCombinationEntity*)promoCombination success:(SuccessCallback)success failure:(FailureCallback)failure;
- (void)clearPromoReception:(SuccessCallback)success failure:(FailureCallback)failure;

@end
//
//  EntityManager.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "EntityManager.h"
#import "Helpers.h"
#import "CoreStringUtils.h"

@implementation EntityManager

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    return self;
}

+ (instancetype)sharedClient {
    static EntityManager * volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (NSObject*)createItem:(NSObject*)data type:(NSInteger)type {
    NSObject* item = nil;
    if ([Helpers isNotNull:data] == false) {
        return item;
    }
    
    switch (type) {
        case USER_ENTITY: {
            item = [[UserEntity alloc] initWithData:data];
            break;
        }       
        case LOCATION_ENTITY: {
            item = [[LocationEntity alloc] initWithData:data];
            break;
        }
        case ADDRESS_ENTITY: {
            item = [[AddressEntity alloc] initWithData:data];
            break;
        }

        case COUNTRY_ENTITY_V2: {
            item = [[CountryEntityV2 alloc] initWithData:data];
            break;
        }
        case STATE_ENTITY: {
            item = [[StateEntity alloc] initWithData:data];
            break;
        }
        case NEWS_ENTITY: {
            item = [[NewsEntity alloc] initWithData:data];
            break;
        }
       
        case BANNER_ENTITY: {
            item = [[BannerEntity alloc] initWithData:data];
            break;
        }
        
        case TS_ADDRESS_ENTITY: {
            item = [[TSAddressEntity alloc] initWithData:data];
            break;
        }

        case CITY_ENTITY: {
            item = [[CityEntity alloc] initWithData:data];
            break;
        }
    
    
        
        case SOCIAL_PROFILE_ENTITY:
        {
            item = [[SocialProfileEntity alloc] initWithData:data];
            break;
        }
        
        
        case UPLOAD_PHOTO_ENTITY:
        {
            item = [[UploadPhotoEntity alloc] initWithData:data];
            break;
        }
            
        case PROVIDER_ENTITY:
        {
            item = [[ProviderEntity alloc] initWithData:data];
            break;
        }
            
        case SALE_REPORTS_ENTITY:
        {
            item = [[SaleReportsEntity alloc] initWithData:data];
            break;
        }
            
        case CATEGORY_ENTITY:
        {
            item = [[PosCategoryEntity alloc] initWithData:data];
            break;
        }
            
        case PRODUCT_ENTITY:
        {
            item = [[ProductEntity alloc] initWithData:data];
            break;
        }
            
        case MODIFIER_ENTITY:
        {
            item = [[ModifierEntity alloc] initWithData:data];
            break;
        }
            
        case LIST_PRODUCT_ENTITY:
        {
            item = [[ListProductEntity alloc] initWithData:data];
            break;
        }
            
        case SALE_ORDER_ENTITY:
        {
            item = [[SaleOrderEntity alloc] initWithData:data];
            break;
        }
            
        case SETTING_ENTITY:
        {
            item = [[SettingEntity alloc] initWithData:data];
            break;
        }
        
        case CUSTOMER_ENTITY:
        {
            item = [[CustomerEntity alloc] initWithData:data];
            break;
        }
            
        case DEVICE_USER_ENTITY:
        {
            item = [[DeviceUserEntity alloc] initWithData:data];
            break;
        }
            
        case ORDER_ITEM_ENTITY:
        {
            item = [[OrderItemEntity alloc] initWithData:data];
            break;
        }
            
        case PROVIDER_REPORT_ENTITY:
        {
            item = [[ProviderReportEntity alloc] initWithData:data];
            break;
        }
            
        case DAY_SALE_REPORT_ENTITY:
        {
            item = [[DaySaleReportEntity alloc] initWithData:data];
            break;
        }
            
        case SALE_ORDER_ITEM_ENTITY:
        {
            item = [[SaleOrderItemEntity alloc] initWithData:data];
            break;
        }
        
        case GENERAL_PAYMENT_REPORT_ENTITY:
        {
            item = [[GeneralPaymentReportEntity alloc] initWithData:data];
            break;
        }
            
        case PAYMENT_REPORT_ENTITY:
        {
            item = [[PaymentReportEntity alloc] initWithData:data];
            break;
        }
            
        case PAYMENT_ENTITY:
        {
            item = [[PaymentEntity alloc] initWithData:data];
            break;
        }
        
        case BUSSINESS_HOUR_ENTITY:
        {
            item = [[BussinessHourEntity alloc] initWithData:data];
            break;
        }
            
        case STORE_ENTITY:
        {
            item = [[StoreEntity alloc] initWithData:data];
            break;
        }
            
        case WORKING_DAY_ENTITY:
        {
            item = [[WorkingDayEntity alloc] initWithData:data];
            break;
        }
         
        case SALE_TAX_GROUP_ENTITY:
        {
            item = [[SaleTaxGroupEntity alloc] initWithData:data];
            break;
        }
            
        case GLOBAL_STORE_ENTITY:
        {
            item = [[GlobalStoreEntity alloc] initWithData:data];
            break;
        }
        
        case STORE_OWNER_ENTITY:
        {
            item = [[StoreOwnerEntity alloc] initWithData:data];
            break;
        }
            
        case SALE_ENTITY:
        {
            item = [[SaleEntity alloc] initWithData:data];
            break;
        }
            
        default:
            break;
    }
    return item;
}

@end

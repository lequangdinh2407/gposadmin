//
//  UploadSocketManager.h
//  luvkonectcore
//
//  Created by ToanPham on 4/28/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SRWebSocket.h"

@class SocketCommand;
@class BaseUploadSocketRequest;
@class UploadSocketRequest;

typedef void (^SuccessCallback)(NSMutableDictionary *results, int errorCode);
typedef void (^FailureCallback)(NSMutableDictionary *results, int errorCode);

@interface UploadSocketManager : NSObject <SRWebSocketDelegate>

@property (nonatomic) bool isConnected;
@property (nonatomic) bool isConnecting;
@property (nonatomic, strong) dispatch_queue_t backgroundQueue;

- (instancetype)init;
+ (instancetype)sharedClient;
- (void)connect:(UploadSocketRequest*)request;
- (void)close;
- (void)addToQueue:(BaseUploadSocketRequest*)request;

@end
//
//  SessionManager.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "SessionManager.h"
#import "RequestManager.h"
#import "Log.h"
#import "DatabaseManager.h"
#import "CoreConstants.h"
#import "RequestErrorCode.h"
#import "Helpers.h"
#import "WebSocketManager.h"
#import "UploadSocketManager.h"
#import "CommentSocketManager.h"
#import "AppAuth.h"
#import "GTMAppAuth.h"
#import <QuartzCore/QuartzCore.h>
#import "StompManager.h"


static NSString *const kIssuer = @"https://accounts.google.com";
static NSString *const kClientID = @"112490652344-7ammjln8drs5jbv0gann4ueqq912mfle.apps.googleusercontent.com";
static NSString *const kRedirectURI =
@"com.googleusercontent.apps.112490652344-7ammjln8drs5jbv0gann4ueqq912mfle:/oauthredirect";
static NSString *const kExampleAuthorizerKey = @"authorization";

#define TAG @"SessionManager"

@interface SessionManager ()

@property (nonatomic, assign) BOOL isSessionExpired;
@property (nonatomic, assign) BOOL isConnectWS;

@end

@implementation SessionManager

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.userEntity = [[UserEntity alloc] init];
    self.socialProfile = [[SocialProfileEntity alloc] init];
    self.accountEntity = [[AccountEntity alloc] init];
    self.sessionKey = @"";
    self.refreshToken = @"";
    self.domain = @"";
    self.isSessionExpired = FALSE;
    self.socket = @"";
    self.uploadSocket = @"";
    self.statusArray = @[@"Unknown", @"Processing", @"Finish", @"Cancel"];
    self.scheduleStatusArray = @[@"Unknown", @"Created", @"Booked", @"Provider Not Found"];
    return self;
}

+ (instancetype)sharedClient {
    static SessionManager * volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)setSessionExpired {
    if (self.sessionKey.length > 0) {
        if (self.isSessionExpired == FALSE) {
            self.isConnectWS = FALSE;
            self.isSessionExpired = TRUE;
            [self requestAutoLogin];
        }
    }
}

- (void)setSessionExpiredWebSocket {
    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"setSessionExpiredWebSocket with SessionKey:%@   isSessionExpired=%d", self.sessionKey, self.isSessionExpired]];
    if (self.sessionKey.length > 0) {
        self.isConnectWS = TRUE;
        if (self.isSessionExpired == FALSE) {
            self.isSessionExpired = TRUE;
            [self requestAutoLogin];
        }
    }
}

- (void)requestAutoLogin {
    [SharePrefUtils writeBoolPreference:PREFKEY_LOGIN_FLAG prefValue:NO];
    
    SuccessCallback success = ^(NSMutableDictionary *results, int errorCode) {
       
        [Log d:0 tag:TAG message:@"requestAutoLogin success"];
        [[RequestManager sharedClient] addWaitingQueueToRealQueue];
        [self insertDBLoginInfo];
        self.isSessionExpired = FALSE;
        
        [SharePrefUtils writeBoolPreference:PREFKEY_LOGIN_FLAG prefValue:YES];
        
        if (self.isConnectWS == TRUE) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONNECT_COMMENT_SOCKET object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONNECT_STOMP object:nil userInfo:nil];
        }
    };
    
    FailureCallback failure = ^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:TAG message:[NSString stringWithFormat:@"requestAutoLogin failure errorCode=%d", errorCode]];
        self.isSessionExpired = FALSE;
        switch (errorCode) {
            case ACCOUNT_BANNED: {
                [Helpers showToast:[CoreStringUtils getStringWithErrorCode:@"str_account_banned" errorCode:errorCode]];
                [self logOutWhenRefreshTokenTimeOut];
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_KICKOUT object:nil];
                break;
            }
            
            case RESTRICT_AUTOLOGIN:
            case INVALID_LOGIN: {
                [self logOut];
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_KICKOUT object:nil];
                break;
            }
            case REFRESHTOKEN_TIMEOUT: {
                //NSInteger login_type = [SharePrefUtils getIntPreference:PREFKEY_LOGIN_TYPE];
                
                [self logOutWhenRefreshTokenTimeOut];
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_KICKOUT object:nil];
                
                /*
                if (login_type == LOGIN_FB) {
                    [self fbLogin];
                } else if (login_type == LOGIN_GG) {
                    [self ggLogin];
                } else {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_KICKOUT object:nil];
                }
                 */
                
                break;
            }
                
            default:
                break;
        }
    };
    
    [[RequestManager sharedClient] requestRefreshToken:self.refreshToken lon:0 lat:0 success:success failure:failure];
}

- (void)insertDBLoginInfo {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:[NSNumber numberWithInteger:ACTION_INSERT_ALL_LOGIN_INFO] forKey:DB_TYPE_ACTION];
    [data setValue:self.userEntity forKey:DB_USER_ENTITY];
    [data setValue:self.socialProfile forKey:DB_SOCIAL_PROFILE];
    [data setValue:self.sessionKey forKey:DB_SESSION_KEY];
    [data setValue:self.refreshToken forKey:DB_REFRESH_TOKEN];
    [data setValue:self.domain forKey:DB_DOMAIN];
    [data setValue:self.socket forKey:DB_SOCKET];
    [data setValue:self.uploadSocket forKey:DB_UPLOADSOCKET];
    [data setValue:self.accountEntity forKey:DB_ACCOUNT_ENTITY];
    
    NSInteger flag = (FLAG_DELETE_ACCOUNT | FLAG_DELETE_SESSION | FLAG_DELETE_USER);
    [data setValue:[NSNumber numberWithInteger:flag] forKey:DB_FLAG_ACTION];

    [[DatabaseManager sharedClient] addToQueue:data success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:TAG message:@"insertDBLoginInfo success"];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:TAG message:[NSString stringWithFormat:@"insertDBLoginInfo failure errorCode=%d", errorCode]];
    }];
}

- (void)deleteDBLoginInfo {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:[NSNumber numberWithInteger:ACTION_DELETE_LOGIN_INFO] forKey:DB_TYPE_ACTION];
    NSInteger flag = (FLAG_DELETE_USER | FLAG_DELETE_SESSION | FLAG_DELETE_ACCOUNT);
    [data setValue:[NSNumber numberWithInteger:flag] forKey:DB_FLAG_ACTION];
    
    [[DatabaseManager sharedClient] addToQueue:data success:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:TAG message:@"deleteDBLoginInfo success"];
        
    } failure:^(NSMutableDictionary *results, int errorCode) {
        [Log d:0 tag:TAG message:[NSString stringWithFormat:@"deleteDBLoginInfo failure errorCode=%d", errorCode]];
    }];
}

- (void)clearAll {
    self.userEntity = [[UserEntity alloc] init];
    self.socialProfile = [[SocialProfileEntity alloc] init];
    self.accountEntity = [[AccountEntity alloc] init];
    self.sessionKey = @"";
    self.refreshToken = @"";
    self.domain = @"";
    self.isSessionExpired = FALSE;
    self.socket = @"";
    self.uploadSocket = @"";
}

- (void)logOut {
    [Log d:0 tag:TAG message:@"Log Out!"];
    
    [SharePrefUtils writeBoolPreference:PREFKEY_LOGIN_FLAG prefValue:NO];
    [SharePrefUtils writeIntPreference:PREFKEY_NOTI_COUNT prefValue:0];
    
    [SessionManager sharedClient].userEntity = nil;
   
    
    [self clearAll];
    [SharePrefUtils clearAll];
    [[WebSocketManager sharedClient] close];
    [[UploadSocketManager sharedClient] close];
    [[CommentSocketManager sharedClient] close];
    [[StompManager sharedClient] close];

    [self deleteDBLoginInfo];
}

- (void)logOutWhenRefreshTokenTimeOut {
    [Log d:0 tag:TAG message:@"logOutWhenRefreshTokenTimeOut!"];
   
    [SharePrefUtils writeBoolPreference:PREFKEY_LOGIN_FLAG prefValue:NO];
    
    [SessionManager sharedClient].userEntity = nil;
   
    [self clearAll];
    
    
    [SharePrefUtils writeStringPreference:PREFKEY_UPDATE_TIME prefValue:@""];
    [SharePrefUtils writeStringPreference:PREFKEY_ACCESSTOKEN prefValue:@""];
    [SharePrefUtils writeStringPreference:PREFKEY_CURRENT_TOKEN prefValue:@""];
    
    //[[WebSocketManager sharedClient] close];
    //[[UploadSocketManager sharedClient] close];
    //[[CommentSocketManager sharedClient] close];
    //[[StompManager sharedClient] close];
    //[self fbDidlogout];
    //[self ggLogout];
    [self deleteDBLoginInfo];
}







- (void)getOfflineMessage:(NSString*)next count:(uint16_t)count{
    // Getting offline message here.
    if ([SessionManager sharedClient].sessionKey.length > 0) {
        
    }
}

@end

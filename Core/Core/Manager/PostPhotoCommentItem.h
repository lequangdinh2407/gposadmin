//
//  PostFeedItem.h
//  Core
//
//  Created by ToanPham on 4/1/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@class CommentEntity;

@interface PostPhotoCommentItem : NSObject

@property (nonatomic, strong) CommentEntity* comment;
@property (nonatomic, strong) NSString* photoID;
@property (nonatomic, strong) NSData* data;
@property (nonatomic, strong) SuccessCallback postCommentSuccess;
@property (nonatomic, strong) FailureCallback postCommentFailure;

@end
//
//  AddressBookManager.m
//  Core
//
//  Created by ToanPham on 1/4/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "AddressBookManager.h"
#import "EntityManager.h"
#import "RequestManager.h"
#import "SessionManager.h"

#define SHIPPING_ADDRESS_TYPE 0
#define BILLING_ADDRESS_TYPE 1

@implementation AddressBookManager

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.shippingAddressList = [[NSMutableArray alloc] init];
    self.billingAddressList = [[NSMutableArray alloc] init];
//    self.defaultShipping = nil;
//    self.defaultBilling = nil;
    
    return self;
}

+ (instancetype)sharedClient {
    static AddressBookManager* volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

//- (void)reset {
//    self.shippingAddressList = [[NSMutableArray alloc] init];
//    self.billingAddressList = [[NSMutableArray alloc] init];
//    self.defaultBilling = nil;
//    self.defaultShipping = nil;
//}
//
//- (void)getShippingAddress:(SuccessCallback)success failure:(FailureCallback)failure {
//    NSString* sessionKey = [SessionManager sharedClient].sessionKey;
//    [[RequestManager sharedClient] requestGetShippingAddress:sessionKey success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestGetShippingAddress success" message:results];
//        self.shippingAddressList = (NSMutableArray*)[Helpers getArrayDiffNull:[results objectForKey:RESPONSE_SHIPPING_ADDRESSES]];
//        NSString* defaultAddressId = [CoreStringUtils getStringDiffNull:[results objectForKey:RESPONSE_DEFAULT_SHIPPING_ADDRESS]];
//        for (int i = 0; i < self.shippingAddressList.count; i++) {
//            TiffinyAddressEntity* entity = [self.shippingAddressList objectAtIndex:i];
//            if ([entity.addressId isEqualToString:defaultAddressId]) {
//                self.defaultShipping = entity;
//                [self.shippingAddressList removeObjectAtIndex:i];
//                [self.shippingAddressList insertObject:entity atIndex:0];
//                break;
//            }
//        }
//        success(results, errorCode);
//
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestGetShippingAddress failure" message:results];
//        [self reset];
//        failure(results, errorCode);
//    }];
//}
//
//
//- (void)getBillingAddress:(SuccessCallback)success failure:(FailureCallback)failure {
//    NSString* sessionKey = [SessionManager sharedClient].sessionKey;
//    [[RequestManager sharedClient] requestGetBillingAddress:sessionKey success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestGetBillingAddress success" message:results];
//        self.billingAddressList = (NSMutableArray*)[Helpers getArrayDiffNull:[results objectForKey:RESPONSE_BILLING_ADDRESSES]];
//        NSString* defaultAddressId = [CoreStringUtils getStringDiffNull:[results objectForKey:RESPONSE_DEFAULT_BILLING_ADDRESS]];
//        for (int i = 0; i < self.billingAddressList.count; i++) {
//            TiffinyAddressEntity* entity = [self.billingAddressList objectAtIndex:i];
//            if ([entity.addressId isEqualToString:defaultAddressId]) {
//                self.defaultBilling = entity;
//                [self.billingAddressList removeObjectAtIndex:i];
//                [self.billingAddressList insertObject:entity atIndex:0];
//                break;
//            }
//        }
//        success(results, errorCode);
//        
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestGetBillingAddress failure" message:results];
//        [self reset];
//        failure(results, errorCode);
//    }];
//}
//
//- (void)addShippingAddress:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure {
//    [[RequestManager sharedClient] requestAddShippingAddress:params success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestAddShippingAddress success" message:results];
//        TiffinyAddressEntity* entity = [results objectForKey:RESPONSE_DATA];
//        BOOL isDefault = [Helpers getBoolDiffNull:[params objectForKey:ENTITY_DEFAULT]];
//        if (isDefault) {
//            self.defaultShipping = entity;
//            [self.shippingAddressList insertObject:entity atIndex:0];
//        } else {
//            [self.shippingAddressList addObject:entity];
//        }
//        [results setObject:@(isDefault) forKey:ENTITY_DEFAULT];
//        success(results, errorCode);
//
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestAddShippingAddress failure" message:results];
//        failure(results, errorCode);
//    }];
//}

- (void)addBillingAddress:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure {
//    [[RequestManager sharedClient] requestAddBillingAddress:params success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestAddBillingAddress success" message:results];
//        TiffinyAddressEntity* entity = [results objectForKey:RESPONSE_DATA];
//        BOOL isDefault = [Helpers getBoolDiffNull:[params objectForKey:ENTITY_DEFAULT]];
//        if (isDefault) {
//            self.defaultBilling = entity;
//            [self.billingAddressList insertObject:entity atIndex:0];
//        } else {
//            [self.billingAddressList addObject:entity];
//        }
//        [results setObject:@(isDefault) forKey:ENTITY_DEFAULT];
//        success(results, errorCode);
//
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestAddBillingAddress failure" message:results];
//        failure(results, errorCode);
//    }];
}

- (void)deleteShippingAddress:(NSString*)addressId success:(SuccessCallback)success failure:(FailureCallback)failure {
//    NSString* sessionKey = [SessionManager sharedClient].sessionKey;
//
//    [[RequestManager sharedClient] requestDeleteShippingAddress:sessionKey addressId:addressId success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestDeleteShippingAddress success" message:results];
//        for (int i = 0; i < self.shippingAddressList.count; i++) {
//            TiffinyAddressEntity* entity = (TiffinyAddressEntity*)[self.shippingAddressList objectAtIndex:i];
//            if ([entity.addressId isEqualToString:addressId]) {
//                if (self.defaultShipping == entity)
//                    self.defaultShipping = nil;
//                [self.shippingAddressList removeObjectAtIndex:i];
//                break;
//            }
//        }
//        success(results, errorCode);
//
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestDeleteShippingAddress failure" message:results];
//        failure(results, errorCode);
//    }];
}

- (void)deleteBillingAddress:(NSString*)addressId success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSString* sessionKey = [SessionManager sharedClient].sessionKey;

//    [[RequestManager sharedClient] requestDeleteBillingAddress:sessionKey addressId:addressId success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestDeleteBillingAddress success" message:results];
//        for (int i = 0; i < self.billingAddressList.count; i++) {
//            TiffinyAddressEntity* entity = (TiffinyAddressEntity*)[self.billingAddressList objectAtIndex:i];
//            if ([entity.addressId isEqualToString:addressId]) {
//                if (self.defaultBilling == entity)
//                    self.defaultBilling = nil;
//                [self.billingAddressList removeObjectAtIndex:i];
//                break;
//            }
//        }
//        success(results, errorCode);
//
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestDeleteBillingAddress failure" message:results];
//        failure(results, errorCode);
//    }];
}

//- (void)updateShippingAddress:(TiffinyAddressEntity*)entity isDefault:(BOOL)isDefault success:(SuccessCallback)success failure:(FailureCallback)failure {
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//    [params setValue:[SessionManager sharedClient].sessionKey forKey:REQUEST_SESSION_KEY];
//    [params setValue:entity.addressId forKey:REQUEST_ID];
//    [params setValue:entity.firstName forKey:REQUEST_FIRST_NAME];
//    [params setValue:entity.lastName forKey:REQUEST_LAST_NAME];
//    [params setValue:entity.businessName forKey:REQUEST_BUSINESS_NAME];
//    [params setValue:entity.addressLine1 forKey:REQUEST_ADDRESS_LINE_1];
//    [params setValue:entity.addressLine2 forKey:REQUEST_ADDRESS_LINE_2];
//    [params setValue:entity.phone forKey:REQUEST_PHONE];
//    [params setValue:entity.city forKey:REQUEST_CITY];
//    [params setValue:entity.state forKey:REQUEST_STATE];
//    [params setValue:entity.country forKey:REQUEST_COUNTRY];
//    [params setValue:entity.countryCode forKey:REQUEST_COUNTRY_CODE];
//    [params setValue:entity.zipCode forKey:REQUEST_ZIP_CODE];
//    [params setValue:@(isDefault ? 1 : 0) forKey:REQUEST_DEFAULT];
//    
//    [[RequestManager sharedClient] requestUpdateShippingAddress:params success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestUpdateShippingAddress success" message:results];
//        for (int i = 0; i < self.shippingAddressList.count; i++) {
//            TiffinyAddressEntity* addressEntity = [self.shippingAddressList objectAtIndex:i];
//            if ([addressEntity.addressId isEqualToString:entity.addressId]) {
//                [self.shippingAddressList removeObjectAtIndex:i];
//                if (isDefault)
//                    [self.shippingAddressList insertObject:entity atIndex:0];
//                else
//                    [self.shippingAddressList insertObject:entity atIndex:i];
//                break;
//            }
//        }
//        
//        if (isDefault) {
//            self.defaultShipping = entity;
//        } else {
//            if (self.defaultShipping) {
//                if ([self.defaultShipping.addressId isEqualToString:entity.addressId]) {
//                    self.defaultShipping = nil;
//                }
//            }
//        }
//        [results setObject:@(isDefault) forKey:ENTITY_DEFAULT];
//        success(results, errorCode);
//
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestUpdateShippingAddress failure" message:results];
//        failure(results, errorCode);
//    }];
//}
//
//- (void)updateBillingAddress:(TiffinyAddressEntity*)entity isDefault:(BOOL)isDefault success:(SuccessCallback)success failure:(FailureCallback)failure {
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//    [params setValue:[SessionManager sharedClient].sessionKey forKey:REQUEST_SESSION_KEY];
//    [params setValue:entity.addressId forKey:REQUEST_ID];
//    [params setValue:entity.firstName forKey:REQUEST_FIRST_NAME];
//    [params setValue:entity.lastName forKey:REQUEST_LAST_NAME];
//    [params setValue:entity.businessName forKey:REQUEST_BUSINESS_NAME];
//    [params setValue:entity.addressLine1 forKey:REQUEST_ADDRESS_LINE_1];
//    [params setValue:entity.addressLine2 forKey:REQUEST_ADDRESS_LINE_2];
//    [params setValue:entity.phone forKey:REQUEST_PHONE];
//    [params setValue:entity.city forKey:REQUEST_CITY];
//    [params setValue:entity.state forKey:REQUEST_STATE];
//    [params setValue:entity.country forKey:REQUEST_COUNTRY];
//    [params setValue:entity.countryCode forKey:REQUEST_COUNTRY_CODE];
//    [params setValue:entity.zipCode forKey:REQUEST_ZIP_CODE];
//    [params setValue:@(isDefault ? 1 : 0) forKey:REQUEST_DEFAULT];
//    
//    [[RequestManager sharedClient] requestUpdateBillingAddress:params success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestUpdateBillingAddress success" message:results];
//
//        for (int i = 0; i < self.billingAddressList.count; i++) {
//            TiffinyAddressEntity* addressEntity = [self.billingAddressList objectAtIndex:i];
//            if ([addressEntity.addressId isEqualToString:entity.addressId]) {
//                [self.billingAddressList removeObjectAtIndex:i];
//                if (isDefault)
//                    [self.billingAddressList insertObject:entity atIndex:0];
//                else
//                    [self.billingAddressList insertObject:entity atIndex:i];
//                break;
//            }
//        }
//        
//        if (isDefault) {
//            self.defaultBilling = entity;
//        } else {
//            if (self.defaultBilling) {
//                if ([self.defaultBilling.addressId isEqualToString:entity.addressId]) {
//                    self.defaultBilling = nil;
//                }
//            }
//        }
//        [results setObject:@(isDefault) forKey:ENTITY_DEFAULT];
//        success(results, errorCode);
//
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestUpdateBillingAddress failure" message:results];
//        failure(results, errorCode);
//    }];
//}

- (void)updateShippingAddress:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure {
    BOOL isDefault = [Helpers getBoolDiffNull:[params objectForKey:ENTITY_DEFAULT]];
    
//    [[RequestManager sharedClient] requestUpdateShippingAddress:params success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestUpdateShippingAddress success" message:results];
//        TiffinyAddressEntity* entity = [results objectForKey:RESPONSE_DATA];
//        
//        for (int i = 0; i < self.shippingAddressList.count; i++) {
//            TiffinyAddressEntity* addressEntity = [self.shippingAddressList objectAtIndex:i];
//            if ([addressEntity.addressId isEqualToString:entity.addressId]) {
//                [self.shippingAddressList removeObjectAtIndex:i];
//                if (isDefault)
//                    [self.shippingAddressList insertObject:entity atIndex:0];
//                else
//                    [self.shippingAddressList insertObject:entity atIndex:i];
//                break;
//            }
//        }
//
//        if (isDefault) {
//            self.defaultShipping = entity;
//        } else {
//            if (self.defaultShipping) {
//                if ([self.defaultShipping.addressId isEqualToString:entity.addressId]) {
//                    self.defaultShipping = nil;
//                }
//            }
//        }
//        [results setObject:@(isDefault) forKey:ENTITY_DEFAULT];
//        success(results, errorCode);
//
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestUpdateShippingAddress failure" message:results];
//        failure(results, errorCode);
//    }];
}

- (void)updateBillingAddress:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure {
    BOOL isDefault = [Helpers getBoolDiffNull:[params objectForKey:ENTITY_DEFAULT]];
    
//    [[RequestManager sharedClient] requestUpdateBillingAddress:params success:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestUpdateBillingAddress success" message:results];
//        TiffinyAddressEntity* entity = [results objectForKey:RESPONSE_DATA];
//
//        for (int i = 0; i < self.billingAddressList.count; i++) {
//            TiffinyAddressEntity* addressEntity = [self.billingAddressList objectAtIndex:i];
//            if ([addressEntity.addressId isEqualToString:entity.addressId]) {
//                [self.billingAddressList removeObjectAtIndex:i];
//                if (isDefault)
//                    [self.billingAddressList insertObject:entity atIndex:0];
//                else
//                    [self.billingAddressList insertObject:entity atIndex:i];
//                break;
//            }
//        }
//        
//        if (isDefault) {
//            self.defaultBilling = entity;
//        } else {
//            if (self.defaultBilling) {
//                if ([self.defaultBilling.addressId isEqualToString:entity.addressId]) {
//                    self.defaultBilling = nil;
//                }
//            }
//        }
//        [results setObject:@(isDefault) forKey:ENTITY_DEFAULT];
//        success(results, errorCode);
//        
//    } failure:^(NSMutableDictionary *results, int errorCode) {
//        [Log d:0 tag:@"requestUpdateBillingAddress failure" message:results];
//        failure(results, errorCode);
//    }];
}

@end

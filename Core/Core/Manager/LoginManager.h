//
//  LoginManager.h
//  Core
//
//  Created by vu van long on 12/25/15.
//  Copyright © 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    EmailLoginType = 0,
    FBLoginType = 1,
    GGLoginType = 2,
} LoginType;

@interface LoginManager : NSObject
@property (nonatomic, assign) LoginType loginType;
@property (nonatomic, strong) id nextViewController;
+ (instancetype)sharedClient;
- (void)reset;
- (void)gotoController:(id)next;
@end

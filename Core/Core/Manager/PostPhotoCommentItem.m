//
//  PostFeedItem.m
//  Core
//
//  Created by ToanPham on 4/1/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "PostPhotoCommentItem.h"

@implementation PostPhotoCommentItem

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.comment = nil;
    self.photoID = @"";
    self.postCommentSuccess = nil;
    self.postCommentFailure = nil;
    
    return self;
}

@end
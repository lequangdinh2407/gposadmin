//
//  DatabaseManager.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/24/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabaseQueue.h"
#import "CoreConstants.h"

typedef void (^DBSuccessCallback)(NSMutableDictionary *results, int errorCode);
typedef void (^DBFailureCallback)(NSMutableDictionary *results, int errorCode);

//static NSInteger const ACTION_RESET_DB = 0;
static NSInteger const ACTION_INSERT_USER = 1;
static NSInteger const ACTION_UPDATE_USER = 2;
static NSInteger const ACTION_INSERT_ALL_LOGIN_INFO = 3;
static NSInteger const ACTION_GET_DATA_INIT_APP = 4;
static NSInteger const ACTION_GET_MESSAGE = 5;
static NSInteger const ACTION_INSERT_MESSAGE = 6;
static NSInteger const ACTION_DELETE_CONVERSATION = 7;
static NSInteger const ACTION_DELETE_LOGIN_INFO = 8;
static NSInteger const ACTION_INSERT_ALL_ADDRESSES_BOOK = 9;
static NSInteger const ACTION_INSERT_CONVERSATION = 10;
static NSInteger const ACTION_UPDATE_CONVERSATION = 11;
static NSInteger const ACTION_GET_MESSAGE_CONVERSATION = 12;
static NSInteger const ACTION_UPDATE_MESSAGE = 13;
static NSInteger const ACTION_UPDATE_MESSAGE_ACK = 14;
static NSInteger const ACTION_UPDATE_MESSAGE_CONTENT = 15;
static NSInteger const ACTION_UPDATE_MESSAGE_STATE = 16;
static NSInteger const ACTION_UPDATE_MESSAGE_STATE_REQID = 17;
static NSInteger const ACTION_DELETE_MESSAGE = 18;
static NSInteger const ACTION_INSERT_FEED = 19;
static NSInteger const ACTION_GET_ALL_FEED = 20;

/**
 * Error Code
 */
static NSInteger const DB_NO_ERROR = 0;
static NSInteger const DB_UNKNOW_ERROR = 1;


/**
 * Flag Delete App Info
 */
static NSInteger const FLAG_DELETE_USER = 1;
static NSInteger const FLAG_DELETE_SESSION = 4;
static NSInteger const FLAG_DELETE_ACCOUNT = 8;

/**
 * Flag Delete Customer
 */
static NSInteger const FLAG_DELETE_CUSTOMER = 16;

/**
 * Flag Get App Info
 */
static NSInteger const FLAG_GET_USER = 1;
static NSInteger const FLAG_GET_MESSAGE = 4;
static NSInteger const FLAG_GET_CONVERSATION = 8;

/**
 * Flag Get Message
 */
static NSInteger const FLAG_GET_MESSAGE_FROM_REQ_ID = 1;
static NSInteger const FLAG_GET_MESSAGE_FROM_MSG_ID = 2;

/**
 * Flag Insert Feed
 */
static NSInteger const FLAG_DELETE_FEED = 1;

/**
 * Load More Limit
 */
static NSInteger const LOAD_MORE_LIMIT = 50;

@interface DatabaseManager : NSObject

@property (nonatomic, strong) dispatch_queue_t backgroundQueue;
@property (nonatomic, strong) FMDatabaseQueue *databaseQueue;

+ (instancetype)sharedClient;
- (instancetype)init;
- (void)addToQueue:(NSMutableDictionary*)data success:(DBSuccessCallback)success failure:(DBFailureCallback)failure;
- (void)getAccountAndSession:(DBSuccessCallback)success failure:(DBFailureCallback)failure;
- (void)getDataInitApp:(DBSuccessCallback)success failure:(DBFailureCallback)failure;

@end

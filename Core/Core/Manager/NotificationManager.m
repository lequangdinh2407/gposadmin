//
//  NotificationManager.m
//  spabees
//
//  Created by Toan Pham Thanh on 1/15/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "NotificationManager.h"

@implementation NotificationManager

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    return self;
}

+ (instancetype)sharedClient {
    static NotificationManager * volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)updateBanner:(BOOL)value {
    isUpdateBanner = value;
}

- (BOOL)isUpdateBanner {
    return isUpdateBanner;
}

- (void)clearNotification {
    self.data = nil;
}

@end

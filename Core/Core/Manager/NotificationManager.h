//
//  NotificationManager.h
//  spabees
//
//  Created by Toan Pham Thanh on 1/15/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationManager : NSObject {
    BOOL isUpdateBanner;
    BOOL isHaveNotification;
}
+ (instancetype)sharedClient;

- (void)updateBanner:(BOOL)value;
- (BOOL)isUpdateBanner;

@property (nonatomic, strong) NSString* typeNotification;
@property (nonatomic, strong) NSDictionary* data;

- (void)clearNotification;

@end

//
//  UploadSocketManager.m
//  luvkonectcore
//
//  Created by ToanPham on 4/28/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "UploadSocketManager.h"
#import "CommandBuilder.h"
#import "Helpers.h"
#import "BaseUploadSocketRequest.h"
#import "SessionManager.h"
#import "UploadSocketRequest.h"
#import "RequestErrorCode.h"

#define TAG @"UploadSocketManager"

static int WS_PING_TIME = 20;
static int WS_PING_TIMEOUT = 15;
static int WS_REQUEST_TIME_OUT = 20;
static int WS_PING_TIMEOUT_COUNT = 3;

static int pingCount = 0;
static int pingTimeOutCount = 0;

static bool isPinging = false;
static bool pingHasResponse = false;

static NSLock* requestLock;

@interface UploadSocketManager ()

@property (nonatomic, strong) SRWebSocket *webSocket;
@property (nonatomic, strong) NSLock* connectLock;
@property (nonatomic, strong) NSMutableArray* connectQueue;
@property (nonatomic, strong) NSMutableArray* requestQueue;
@property (nonatomic, strong) SocketCommand* currentCommand;
@property (nonatomic, strong) NSTimer* timer;

@end

@implementation UploadSocketManager

- (void)processConnectResponse:(SocketCommand*)command{
    self.isConnecting = false;
   
    if (command.errorCode == 0) {
        [Log d:0 tag:TAG message:@"UploadSocket connected"];
        self.isConnected = true;
                
        [self.connectLock lock];
        if (self.connectQueue.count > 0) {
            for (int i = 0; i < self.connectQueue.count; i++) {
                UploadSocketRequest* request = self.connectQueue[i];
                dispatch_async(dispatch_get_main_queue(), ^{
                    request.success(nil, command.errorCode);
                });
                [self.connectQueue removeObjectAtIndex:i];
                i--;
            }
        }
        [self.connectLock unlock];
        if (self.timer != nil) {
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                          target:self
                                                        selector:@selector(onTimer)
                                                        userInfo:nil
                                                         repeats:YES];
        }
        
    } else {
        [Log d:0 tag:TAG message:[NSString stringWithFormat:@"UploadSocket connect failed! errorCode=%d", command.errorCode]];
        self.isConnected = false;
        
        [self.connectLock lock];
        if (self.connectQueue.count > 0) {
            for (int i = 0; i < self.connectQueue.count; i++) {
                UploadSocketRequest* request = self.connectQueue[i];
                dispatch_async(dispatch_get_main_queue(), ^{
                    request.failure(nil, command.errorCode);
                });
                [self.connectQueue removeObjectAtIndex:i];
                i--;
            }
        }
        [self.connectLock unlock];
    }
}

- (void)processRequestResponse:(SocketCommand*)command{
    [requestLock lock];
    if (self.requestQueue.count > 0) {
        for (int i = 0; i < self.requestQueue.count; i++) {
            BaseUploadSocketRequest* request = self.requestQueue[i];
            if ([request getCommand].reqID == command.reqID) {
                if (command.errorCode == 0) {
                    NSMutableDictionary* result = nil;
                   if (command.contentLength > 0) {
                        NSObject* jsonObj = [JSONUtils parseDataToObject:command.content];
                        [Log d:0 tag:TAG message:[NSString stringWithFormat:@"UploadSocket received json=%@", jsonObj]];
                        result = [request parseNetworkRespone:jsonObj];
                        if (result == nil) {
                            result = [request receiveResponse:command];
                        }
                    }
                    if (request.success != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            request.success(result, command.errorCode);
                        });
                    }
                } else {
                    if (request.failure != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            request.failure(nil, command.errorCode);
                        });
                    }
                }
                [self.requestQueue removeObjectAtIndex:i];
                break;
            }
        }
    } else {
        // TODO
    }
    [requestLock unlock];
}


- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message;
{
    dispatch_async(self.backgroundQueue, ^{
        if (message != nil && [message isKindOfClass:[NSData class]]) {
            NSData* data = (NSData*)message;
            SocketCommand *command = [[CommandBuilder sharedClient] parseDataResponse:data];
            if (command != nil) {
                [Log d:0 tag:TAG message:[NSString stringWithFormat:@"UploadSocket received cmd=%@, errorCode=%@", @(command.cmd), @(command.errorCode)]];
                
                switch (command.cmd) {
                    case CONNECTED_CMD:
                        if (self.isConnecting)
                            [self processConnectResponse:command];
                        break;
                    case PING_CMD:
                        pingHasResponse = true;
                        isPinging = false;
                        pingTimeOutCount = 0;
                        break;
                    default:
                        [self processRequestResponse:command];
                }
            }
        }
    });
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket;
{
    
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error;
{
    dispatch_async(self.backgroundQueue, ^{
        [Log d:0 tag:TAG message:[NSString stringWithFormat:@"UploadSocket Failed With Error %@", error]];
        if (self.isConnected == false) {
            self.isConnecting = false;
            [self.connectLock lock];
            if (self.connectQueue.count > 0) {
                for (int i = 0; i < self.connectQueue.count; i++) {
                    UploadSocketRequest* request = self.connectQueue[i];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        request.failure(nil, UNKNOWN_EXCEPTION);
                    });
                    [self.connectQueue removeObjectAtIndex:i];
                    i--;
                }
            }
            [self.connectLock unlock];
        }
        [requestLock lock];
        if (self.requestQueue.count > 0) {
            for (int i = 0; i < self.requestQueue.count; i++) {
                BaseUploadSocketRequest* request = self.requestQueue[i];
                if (request.failure != nil) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        request.failure(nil, -1);
                    });
                }
                [self.requestQueue removeObjectAtIndex:i];
                i--;
            }
        }
        [requestLock unlock];
        [self.webSocket close];
        self.isConnected = false;
        self.isConnecting = false;
        [self.timer invalidate];
        self.timer = nil;
    });
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    [Log d:0 tag:TAG message:@"UploadSocket closed!"];
    self.isConnected = false;
    self.isConnecting = false;
    [self.timer invalidate];
    self.timer = nil;
    
    [requestLock lock];
    if (self.requestQueue.count > 0) {
        for (int i = 0; i < self.requestQueue.count; i++) {
            BaseUploadSocketRequest* request = self.requestQueue[i];
            if (request.failure != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    request.failure(nil, SOCKET_NOT_CONNECTED);
                });
            }
            [self.requestQueue removeObjectAtIndex:i];
            i--;
        }
    }
    [requestLock unlock];
    
    [self.connectLock lock];
    if (self.connectQueue.count > 0) {
        for (int i = 0; i < self.connectQueue.count; i++) {
            UploadSocketRequest* request = self.connectQueue[i];
            dispatch_async(dispatch_get_main_queue(), ^{
                request.failure(nil, UNKNOWN_EXCEPTION);
            });
            [self.connectQueue removeObjectAtIndex:i];
            i--;
        }
    }
    [self.connectLock unlock];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload {
    [Log d:0 tag:TAG message:@"UploadSocket receive pong"];
}

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.webSocket = nil;
    self.connectLock = [[NSLock alloc] init];
    self.backgroundQueue = dispatch_queue_create("com.dkm.luvkonnect.uploadwsqueue", NULL);
    self.requestQueue = [[NSMutableArray alloc] init];
    self.connectQueue = [[NSMutableArray alloc] init];
    self.isConnected = false;
    self.isConnecting = false;
    requestLock = [[NSLock alloc] init];
    return self;
}

+ (instancetype)sharedClient {
    static UploadSocketManager* volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (void)close {
    if (self.webSocket != nil) {
        [Log d:0 tag:TAG message:@"Socket force close"];
        [self.webSocket close];
        self.webSocket = nil;
        self.isConnected = false;
        self.isConnecting = false;
        [self.timer invalidate];
        self.timer = nil;
        
        [self.connectLock lock];
        if (self.connectQueue.count > 0) {
            for (int i = 0; i < self.connectQueue.count; i++) {
                UploadSocketRequest* request = self.connectQueue[i];
                dispatch_async(dispatch_get_main_queue(), ^{
                    request.failure(nil, UNKNOWN_EXCEPTION);
                });
                [self.connectQueue removeObjectAtIndex:i];
                i--;
            }
        }
        [self.connectLock unlock];
        
        [requestLock lock];
        for (int i = 0; i < self.requestQueue.count; i++) {
            BaseUploadSocketRequest* request = self.requestQueue[i];
            if (request.failure != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    request.failure(nil, SOCKET_NOT_CONNECTED);
                });
            }
            [self.requestQueue removeObjectAtIndex:i];
            i--;
        }
        [requestLock unlock];
    }
}

- (void)connect:(UploadSocketRequest*)request {
    dispatch_async(self.backgroundQueue, ^{
        [self.connectLock lock];
        if (!self.isConnecting) {
            [self.connectQueue addObject:request];
            self.webSocket = (SRWebSocket*)[[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[request getURL]]]];
            self.webSocket.delegate = self;
            self.isConnected = false;
            self.isConnecting = true;
            [self.webSocket open];
            [Log d:0 tag:TAG message:@"Start connecting"];
        } else {
            [self.connectQueue addObject:request];
            [Log d:0 tag:TAG message:@"connecting"];
        }
        [self.connectLock unlock];
    });
}

- (void)onPing {
    pingCount++;
    if (pingCount >= WS_PING_TIME) {
        if (!self.isConnected) {
            [Log d:0 tag:TAG message:@"Ping Failed: WebSocket not connected!"];
            [self close];
        } else {
            NSDictionary* dict = [[NSDictionary alloc] initWithObjectsAndKeys:@(PING_CMD) ,HDR_CMD, nil];
            SocketCommand* command = [[CommandBuilder sharedClient] createCommand:dict type:PING_CMD];
            [self sendCommand:command];
            pingHasResponse = false;
            isPinging = true;
        }
        pingCount = 0;
    }
    
    if (pingCount >= WS_PING_TIMEOUT) {
        if (isPinging == true) {
            if (pingHasResponse == false) {
                pingTimeOutCount++;
                if (pingTimeOutCount > WS_PING_TIMEOUT_COUNT) {
                    [Log d:0 tag:TAG message:@"Ping Failed: Time-out!"];
                    self.isConnected = false;
                    self.isConnecting = false;
                    [self close];
                }
            }
        }
        isPinging = false;
    }
}

- (void)onRequest {
    [requestLock lock];
    if (self.requestQueue.count > 0) {
        for (int i = 0; i < self.requestQueue.count; i++) {
            BaseUploadSocketRequest* request = self.requestQueue[i];
            request.timeOutCount++;
            if (request.timeOutCount >= WS_REQUEST_TIME_OUT) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    request.failure(nil, 0);
                });
                [self.requestQueue removeObjectAtIndex:i];
                i--;
            }
        }
    }
    [requestLock unlock];
}

- (void)onTimer{
    [self onPing];
    [self onRequest];
}

- (void)sendCommand:(SocketCommand*)command {
    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"Send cmd=%@ reqId=%@", @(command.cmd), @(command.reqID)]];
    self.currentCommand = command;
    [self.webSocket send:[command toData]];
}

- (void)addToQueue:(BaseUploadSocketRequest*)request {
    if (!self.isConnected) {
        [Log d:0 tag:TAG message:@"UploadSocket not connected!"];
        dispatch_async(dispatch_get_main_queue(), ^{
            request.failure(nil, SOCKET_NOT_CONNECTED);
        });
        return;
    }
    
    dispatch_async(self.backgroundQueue, ^{
        [requestLock lock];
        [self.requestQueue addObject:request];
        [self sendCommand:[request getCommand]];
        [requestLock unlock];
    });
}

@end
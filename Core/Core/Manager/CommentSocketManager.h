//
//  WebSocketManager.h
//  luvkonectcore
//
//  Created by ToanPham on 3/10/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SRWebSocket.h"

@class CommentSocketCommand;
@class BaseCommentSocketRequest;
@class CommentSocketRequest;

typedef void (^SuccessCallback)(NSMutableDictionary *results, int errorCode);
typedef void (^FailureCallback)(NSMutableDictionary *results, int errorCode);

@interface CommentSocketManager : NSObject <SRWebSocketDelegate> {
    bool isPing;
    bool isRetry;
    bool isForceClose;
    bool pingHasResponse;
    bool isPinging;
    int pingCount;
    int pingTimeOutCount;
    int timeRetryConnectCount;
}

@property (nonatomic) bool isConnected;
@property (nonatomic) bool isConnecting;
@property (nonatomic) int fibo1;
@property (nonatomic) int fibo2;
@property (nonatomic, strong) dispatch_queue_t backgroundQueue;
@property (nonatomic, strong) dispatch_queue_t chatQueue;
@property (nonatomic, strong) SuccessCallback pingSuccess;
@property (nonatomic) NSString* socketUrl;

- (instancetype)init;
+ (instancetype)sharedClient;
- (void)connect:(CommentSocketRequest*)request;
- (void)close;
- (void)sendCommand:(CommentSocketCommand*)command;
- (void)addToQueue:(BaseCommentSocketRequest*)request;
- (void)setRetry:(bool)value;

@end
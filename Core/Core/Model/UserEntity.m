//
//  UserEntity.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "UserEntity.h"
#import "Helpers.h"
#import "EntityManager.h"
#import "CoreStringUtils.h"
#import "RLMUser.h"
#import "RequestManager.h"
#import "SessionManager.h"

@implementation UserEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.userId = @"";
    self.uint_UserId = 0;
    self.firstName = @"";
    self.lastName = @"";
    self.email = @"";
    self.birthday = @"";
    self.address = nil;
    self.phone = @"";
    self.status = @"";
    self.displayName = @"";
    self.avatar = @"";
    self.followed = FALSE;
    self.hometown = @"";
    self.interestedIn = @"";
    self.slogan = @"";
    self.profileType = 0;
    self.gender = 0;
    self.profileUserDisplayName = @"";
    self.countryCode = @"";
    self.nationalNumber = @"";
    self.dateOB = @"";
    self.monthOB = @"";
    self.storesList = [[NSMutableArray alloc] init];
    return self;
}


- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];    
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.userId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.firstName = [CoreStringUtils getStringDiffNull:[map objectForKey:@"firstName"]];
        self.lastName = [CoreStringUtils getStringDiffNull:[map objectForKey:@"lastName"]];
        self.displayName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DISPLAY_NAME]];
        self.email = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_EMAIL]];
        self.birthday = [CoreStringUtils getStringDiffNull:[map objectForKey:@"birthday"]];
        self.address = (TSAddressEntity*)[[EntityManager sharedClient] createItem:[map objectForKey:ENTITY_ADDRESS] type:TS_ADDRESS_ENTITY];
        self.profileType = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_PROFILE_TYPE]];
        self.phone = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_phone]];
        self.profileUserDisplayName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DISPLAY_NAME]];
        self.uint_UserId = [CoreStringUtils convertStringToUnsignedLong:self.userId];

        NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_STORES]];
        if (array) {
            for (int i = 0; i < array.count; i++) {
                StoreEntity *entity = (StoreEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:STORE_ENTITY];
                [self.storesList addObject:entity];
            }
        }
        
        StoreEntity *entity = [self.storesList objectAtIndex:0];
        [RequestManager sharedClient].apiUrl = entity.apiUrl;
        [RequestManager sharedClient].username = entity.user;
        [RequestManager sharedClient].storeName = entity.name;
        [RequestManager sharedClient].password = entity.password;
        [RequestManager sharedClient].storeId = entity.storeId;
        
        [SharePrefUtils writeStringPreference:PREFKEY_CONFIG_API_URL prefValue:entity.apiUrl];
        [SharePrefUtils writeStringPreference:PREFKEY_CONFIG_USERNAME prefValue:entity.user];
        [SharePrefUtils writeStringPreference:PREFKEY_CONFIG_STORENAME prefValue:entity.name];
        [SharePrefUtils writeStringPreference:PREFKEY_CONFIG_PASSWORD prefValue:entity.password];
        [SharePrefUtils writeStringPreference:PREFKEY_CONFIG_STOREID prefValue:entity.storeId];
        [SharePrefUtils writeStringPreference:PREFKEY_CONFIG_STORE_ID prefValue:entity.Id];
        
        if ([SessionManager sharedClient].userEntity.profileType != 1) {
            
        }
        
    }
    return self;
}

- (NSObject*)initWithRLMObject:(RLMObject*)rlmObj {
    self = [self init];
    if ([rlmObj isKindOfClass:[RLMUser class]]) {
        RLMUser* rlmUser = (RLMUser*)rlmObj;
        self.userId = rlmUser.userId;
        self.firstName = rlmUser.firstName;
        self.lastName = rlmUser.lastName;
        self.email = rlmUser.email;
        self.birthday = rlmUser.birthday;
        self.status = rlmUser.status;
        self.avatar = rlmUser.avatar;
        self.uint_UserId = [CoreStringUtils convertStringToUnsignedLong:self.userId];
        
        self.rewardPoint = rlmUser.rewardPoint;
        self.qr = rlmUser.qr;
        self.shopCode = rlmUser.shopCode;
        self.dateOB = rlmUser.dateOB;
        self.monthOB = rlmUser.monthOB;
        self.displayName = rlmUser.displayName;
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeObject:self.firstName forKey:@"firstName"];
    [encoder encodeObject:self.lastName forKey:@"lastName"];
    [encoder encodeObject:self.avatar forKey:@"avatar"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.phone forKey:@"phone"];
    [encoder encodeObject:self.nationalNumber forKey:@"nationalNumber"];
    [encoder encodeObject:self.countryCode forKey:@"countryCode"];
    [encoder encodeObject:self.slogan forKey:@"slogan"];
    [encoder encodeObject:self.hometown forKey:@"hometown"];
    [encoder encodeObject:self.interestedIn forKey:@"interestedIn"];
    [encoder encodeObject:@(self.profileType) forKey:@"profileType"];
    
    [encoder encodeObject:self.qr forKey:@"qr"];
    [encoder encodeObject:self.shopCode forKey:@"shopCode"];
    [encoder encodeObject:@(self.rewardPoint) forKey:@"rewardPoint"];
    [encoder encodeObject:self.dateOB forKey:@"dateOB"];
    [encoder encodeObject:self.monthOB forKey:@"monthOB"];
    [encoder encodeObject:self.displayName forKey:@"_displayName"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.userId = [decoder decodeObjectForKey:@"userId"];
        self.firstName = [decoder decodeObjectForKey:@"firstName"];
        self.lastName = [decoder decodeObjectForKey:@"lastName"];
        self.displayName = [decoder decodeObjectForKey:@"_displayName"];
        self.avatar = [decoder decodeObjectForKey:@"avatar"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.phone = [decoder decodeObjectForKey:@"phone"];
        self.countryCode = [decoder decodeObjectForKey:@"countryCode"];
        self.nationalNumber = [decoder decodeObjectForKey:@"nationalNumber"];
        self.profileType = (int)[[decoder decodeObjectForKey:@"profileType"] integerValue];
        self.slogan = [decoder decodeObjectForKey:@"slogan"];
        self.hometown = [decoder decodeObjectForKey:@"hometown"];
        self.interestedIn = [decoder decodeObjectForKey:@"interestedIn"];
        
        self.qr = [decoder decodeObjectForKey:@"qr"];
        self.shopCode = [decoder decodeObjectForKey:@"shopCode"];
        self.rewardPoint = (int)[[decoder decodeObjectForKey:@"rewardPoint"] integerValue];
        self.dateOB = [decoder decodeObjectForKey:@"dateOB"];
        self.monthOB = [decoder decodeObjectForKey:@"monthOB"];
    }
    return self;
}

@end

//
//  ContactEntity.h
//  spabees
//
//  Created by Toan Pham Thanh on 1/5/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface ContactEntity : BaseEntity {
    NSString* contact;
}

@property (nonatomic, strong) NSString* contactID;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* avatarName;
@property (assign, nonatomic) int avatarColor;

- (instancetype)init;

@end

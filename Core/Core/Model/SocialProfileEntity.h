//
//  SocialProfileEntity.h
//  Core
//
//  Created by Dragon on 2/23/17.
//  Copyright © 2017 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

@interface SocialProfileEntity : BaseEntity

@property (nonatomic, strong) NSString* userId;
@property (nonatomic, strong) NSString* avatar;
@property (nonatomic, strong) NSString* birthday;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* displayName;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* countryCode;
@property (nonatomic, strong) NSString* socialProfileId;




@property (nonatomic, assign) int type;
@property (nonatomic, assign) int gender;

@end

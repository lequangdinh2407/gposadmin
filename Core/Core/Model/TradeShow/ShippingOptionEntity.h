//
//  ShippingOptionEntity.h
//  Core
//
//  Created by ToanPham on 1/9/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface ShippingOptionEntity : BaseEntity

@property (nonatomic, strong) NSString* shippingOptionId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* desc;
@property (nonatomic, strong) NSString* price;
@property (nonatomic, strong) NSMutableAttributedString* attributePrice;

@property (nonatomic) int type;

@end
//
//  PaymentOptionEntity.h
//  Core
//
//  Created by ToanPham on 1/9/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface PaymentOptionEntity : BaseEntity

@property (nonatomic, strong) NSString* paymentOptionId;
@property (nonatomic, strong) NSString* name;


@end
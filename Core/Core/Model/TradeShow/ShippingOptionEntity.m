//
//  ShippingOptionEntity.m
//  Core
//
//  Created by ToanPham on 1/9/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "ShippingOptionEntity.h"
#import "Helpers.h"

@implementation ShippingOptionEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.shippingOptionId = @"";
    self.name = @"";
    self.desc = @"";
    self.price = @"";
    self.type = 0;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.shippingOptionId =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.name = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.desc = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DESC]];
        self.price = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PRICE]]];
        self.price = [NSString stringWithFormat:@"%@%@", DEFAULT_CURRENCY, [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PRICE]]];
//        self.attributePrice = [[NSMutableAttributedString alloc] initWithData:[htmlPrice dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

        self.type = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_TYPE]];
    }
    return self;
}

@end

//
//  ProductOptionEntity.h
//  Core
//
//  Created by ToanPham on 12/28/15.
//  Copyright © 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface ProductOptionEntity : BaseEntity

@property (nonatomic, strong) NSString* productOptionId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* label;
@property (nonatomic) int type;
@property (nonatomic, strong) NSString* displayOrder;
@property (nonatomic, strong) NSMutableArray* productOptionValues;

- (instancetype)init;

@end
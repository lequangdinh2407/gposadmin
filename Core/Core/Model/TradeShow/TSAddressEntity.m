//
//  AddressEntity.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "TSAddressEntity.h"
#import "Helpers.h"

@implementation TSAddressEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.addressId = @"";
    self.addressLine1 = @"";
    self.addressLine2 = @"";
    self.city = @"";
    self.state = @"";
    self.country = @"USA";
    self.zipCode = @"";
    self.phone = @"";
    self.countryCode = @"";
    self.placeId = @"";
    self.lon = 0;
    self.lat = 0;
    self.fullAddress = @"";
    return self;
}

- (NSString*)getAddress1 {
    NSString* address = @"";
    if (![CoreStringUtils isEmpty:self.addressLine1]) {
        return [NSString stringWithFormat:@"%@", self.addressLine1];
    }
    return address;
}

- (NSString*)getCityStateCountry {
    NSString*tmpAddress = @"";
    
    if (![CoreStringUtils isEmpty:self.city]) {
        tmpAddress = [tmpAddress stringByAppendingFormat:@"%@", self.city];
    }
    if (![CoreStringUtils isEmpty:self.state]) {
        tmpAddress = [tmpAddress stringByAppendingFormat:@", %@", self.state];
        
        if (![CoreStringUtils isEmpty:self.zipCode]) {
            tmpAddress = [tmpAddress stringByAppendingFormat:@" %@", self.zipCode];
        }
    }
    if (![CoreStringUtils isEmpty:self.country]) {
        tmpAddress = [tmpAddress stringByAppendingFormat:@", %@", self.country];
    }
    
    return tmpAddress;
}

- (NSString*)getFullAddress {
    NSString*tmpAddress = @"";
    if (![CoreStringUtils isEmpty:self.addressLine1]) {
        tmpAddress = [tmpAddress stringByAppendingString:self.addressLine1];
    }
    if (![CoreStringUtils isEmpty:self.addressLine2]) {
        tmpAddress = [tmpAddress stringByAppendingFormat:@", %@", self.addressLine2];
    }
    
    return tmpAddress;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.addressId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.addressLine1 = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ADDRESSLINE1]];
        self.addressLine2 = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ADDRESSLINE2]];
        self.city = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CITY]];
        self.state = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_STATE]];
        self.country = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_COUNTRY]];
        self.zipCode = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ZIPCODE]];
        self.phone = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PHONE]];
        self.countryCode = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_COUNTRY_CODE]];
        self.placeId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PLACE_ID]];
        NSDictionary* coordinate = [Helpers getDictDiffNull:[map objectForKey:ENTITY_COORDINATE]];
        self.lon = [Helpers getDoubleDiffNull:[coordinate objectForKey:ENTITY_LON]];
        self.lat = [Helpers getDoubleDiffNull:[coordinate objectForKey:ENTITY_LAT]];
        self.fullAddress = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_FULL_ADDRESS]];
    }
    return self;
}

@end

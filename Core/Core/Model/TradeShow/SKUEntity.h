//
//  SKUEntity.h
//  Core
//
//  Created by ToanPham on 12/28/15.
//  Copyright © 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@class CommentEntity;
@class DistributorEntity;

@interface SKUEntity : BaseEntity

@property (nonatomic, strong) NSString* skuId;
@property (nonatomic, strong) NSString* productId;
@property (nonatomic, strong) NSString* categoryId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* desc;
@property (nonatomic, strong) NSString* retailPrice;
@property (nonatomic, strong) NSString* salePrice;
@property (nonatomic, strong) NSString* qtyOnHand;
@property (nonatomic, strong) NSString* displayOrder;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) NSString* primaryPhoto;
@property (nonatomic, strong) NSString* manufacturer;
@property (nonatomic, strong) NSString* model;
@property (nonatomic, strong) NSString* bannerPhoto;
@property (nonatomic, strong) NSString* contentLink;
@property (nonatomic, strong) NSString* createTime;
@property (nonatomic, strong) NSString* updateTime;
@property (nonatomic) BOOL feature;
@property (nonatomic) BOOL taxable;
@property (nonatomic) int type;
@property (nonatomic, strong) NSMutableArray* combos;
@property (nonatomic, strong) NSMutableArray* photos;
@property (nonatomic, strong) NSMutableArray* productOptionValueIds;
@property (nonatomic) BOOL productViewed;
@property (nonatomic) BOOL productLiked;
@property (nonatomic) BOOL productWished;
@property (nonatomic) int productViewCount;
@property (nonatomic) int productLikeCount;
@property (nonatomic) int productOrderCount;
@property (nonatomic) int productWishListCount;
@property (nonatomic) BOOL skuViewed;
@property (nonatomic) BOOL skuLiked;
@property (nonatomic) BOOL skuWished;
@property (nonatomic) BOOL isRating;
@property (nonatomic) int skuCommentCount;
@property (nonatomic) int skuViewCount;
@property (nonatomic) int skuLikeCount;
@property (nonatomic) int skuOrderCount;
@property (nonatomic) int skuWishListCount;
@property (nonatomic) int skuRatingCount;
@property (nonatomic) float ratingPoint;
@property (nonatomic) float userRatingPoint;
@property (nonatomic, strong) CommentEntity* lastComment;
@property (nonatomic, strong) NSString* strSkuViewCount;
@property (nonatomic, strong) NSString* strSkuLikeCount;
@property (nonatomic, strong) NSString* strSkuOrderCount;
@property (nonatomic, strong) NSString* strSkuWishListCount;
@property (nonatomic, strong) NSString* strSkuRatingPoint;
@property (nonatomic, strong) NSString* strSkuUserRatingPoint;
@property (nonatomic, strong) NSString* strSavingPercent;
@property (nonatomic) float width;
@property (nonatomic) float height;
@property (nonatomic, strong) NSMutableAttributedString* attributeRetailPrice;
@property (nonatomic, strong) NSMutableAttributedString* attributeSalePrice;
@property (nonatomic, strong) NSMutableAttributedString* attributeSalePrice2;
@property (nonatomic) BOOL isProductSaleOff;
@property (nonatomic) int remainInPromoTime;
@property (nonatomic) BOOL isOnlyLoyalty;
@property (nonatomic, strong) DistributorEntity* distributor;
- (instancetype)init;

@end

//
//  BookingEntity.h
//  Core
//
//  Created by Dragon on 2/23/17.
//  Copyright © 2017 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

@interface BookingEntity : BaseEntity

@property (nonatomic, strong) NSString* time;
@property (nonatomic, strong) NSString* numberRemainingSlot;

@end

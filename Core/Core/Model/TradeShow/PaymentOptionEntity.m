//
//  PaymentOptionEntity.m
//  Core
//
//  Created by ToanPham on 1/9/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "PaymentOptionEntity.h"
#import "Helpers.h"

@implementation PaymentOptionEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.paymentOptionId = @"";
    self.name = @"";
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.paymentOptionId =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.name = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
    }
    return self;
}

@end

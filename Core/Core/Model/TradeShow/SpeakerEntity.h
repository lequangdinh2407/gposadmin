//
//  AddressEntity.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface SpeakerEntity : BaseEntity

@property (nonatomic, strong) NSString* speakerId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* job;
@property (nonatomic, strong) NSString* sumStory;
@property (nonatomic, strong) NSString* photo;

- (instancetype)init;

@end

//
//  ShippingAddressEntity
//  spabeecore
//
//  Created by ToanPham on 12/11/15.
//  Copyright © 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface TiffinyAddressEntity : BaseEntity

@property (nonatomic, strong) NSString* addressId;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* addressLine1;
@property (nonatomic, strong) NSString* addressLine2;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* state;
@property (nonatomic, strong) NSString* country;
@property (nonatomic, strong) NSString* zipCode;
@property (nonatomic, strong) NSString* countryCode;
@property (nonatomic, strong) NSString* businessName;
@property (nonatomic, strong) NSString* email;

- (instancetype)init;
- (NSString*)getDisplayName;
- (NSString*)fullAddress;
- (NSString*)getCityStateCountry;

@end

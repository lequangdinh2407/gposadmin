//
//  AddressEntity.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "SpeakerEntity.h"
#import "Helpers.h"

@implementation SpeakerEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.speakerId = @"";
    self.name = @"";
    self.job = @"";
    self.photo = @"";
    self.sumStory = @"";
    
    return self;
}

- (NSString*)displayName {
    return self.name;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.speakerId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.name = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.job = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_JOB]];
        self.photo = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PHOTO]];
        self.sumStory = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_SUM_STORY]];
    }
    return self;
}

@end

//
//  ProductOptionValueEntity.m
//  Core
//
//  Created by ToanPham on 12/28/15.
//  Copyright © 2015 ToanPham. All rights reserved.
//

#import "ProductOptionValueEntity.h"

@implementation ProductOptionValueEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.productOptionValueId = @"";
    self.productOptionId = @"";
    self.value = @"";
    self.displayOrder = @"";
    self.name = @"";
    self.photo = @"";
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.productOptionValueId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.productOptionId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PRODUCT_OPT_ID]];
        self.value = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_VALUE]];
        self.displayOrder = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DISPLAY_ORDER]];
        self.name = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.photo = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PHOTO]];
    }
    return self;
}

@end

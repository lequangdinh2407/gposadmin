//
//  ShippingAddressEntity.m
//  spabeecore
//
//  Created by ToanPham on 12/11/15.
//  Copyright © 2015 ToanPham. All rights reserved.
//

#import "TiffinyAddressEntity.h"
#import "Helpers.h"
#import "NSString+phoneNumber.h"

@implementation TiffinyAddressEntity


- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.addressId = @"";
    self.firstName = @"";
    self.lastName = @"";
    self.addressLine1 = @"";
    self.addressLine2 = @"";
    self.phone = @"";
    self.city = @"";
    self.state = @"";
    self.country = @"";
    self.zipCode = @"";
    self.countryCode = @"";
    self.businessName = @"";
    
    return self;
}

- (NSString*)getDisplayName {
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.addressId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.firstName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_FIRST_NAME]];
        self.lastName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_LAST_NAME]];
        self.addressLine1 = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ADDRESSLINE1]];
        self.addressLine2 = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ADDRESSLINE2]];
        self.phone = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PHONE]];
        self.city = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CITY]];
        self.state = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_STATE]];
        self.country = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_COUNTRY]];
        self.countryCode = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_COUNTRY_CODE]];
        self.zipCode = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ZIP_CODE]];
        self.businessName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_BUSINESS_NAME]];
        self.email = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_EMAIL]];
    }
    return self;
}

- (NSString*)getCityStateCountry {
    NSString* cityStateCountry = @"";
    
    if (![CoreStringUtils isEmpty:self.city]) {
        cityStateCountry = [cityStateCountry stringByAppendingFormat:@"%@", self.city];
    }
    if (![CoreStringUtils isEmpty:self.state]) {
        cityStateCountry = [cityStateCountry stringByAppendingFormat:@", %@", self.state];
        
        if (![CoreStringUtils isEmpty:self.zipCode]) {
            cityStateCountry = [cityStateCountry stringByAppendingFormat:@" %@", self.zipCode];
        }
    }
    if (![CoreStringUtils isEmpty:self.country]) {
        cityStateCountry = [cityStateCountry stringByAppendingFormat:@", %@", self.country];
    }
    
    return cityStateCountry;
}

- (NSString*)fullAddress {
    NSString*tmpAddress = @"";
    if (![CoreStringUtils isEmpty:self.addressLine1]) {
        tmpAddress = [tmpAddress stringByAppendingString:self.addressLine1];
    }
    if (![CoreStringUtils isEmpty:self.addressLine2]) {
        tmpAddress = [tmpAddress stringByAppendingFormat:@", %@", self.addressLine2];
    }
    return tmpAddress;
}

@end

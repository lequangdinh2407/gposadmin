//
//  SKUEntity.m
//  Core
//
//  Created by ToanPham on 12/28/15.
//  Copyright © 2015 ToanPham. All rights reserved.
//

#import "SKUEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation SKUEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.skuId = @"";
    self.productId = @"";
    self.categoryId = @"";
    self.name = @"";
    self.desc = @"";
    self.retailPrice = @"";
    self.salePrice = @"";
    self.qtyOnHand = @"";
    self.displayOrder = @"";
    self.status = @"";
    self.primaryPhoto = @"";
    self.bannerPhoto = @"";
    self.contentLink = @"";
    self.manufacturer = @"";
    self.model = @"";
    self.createTime = @"";
    self.updateTime = @"";
    self.feature = FALSE;
    self.taxable = FALSE;
    self.combos = [[NSMutableArray alloc] init];
    self.photos = [[NSMutableArray alloc] init];
    self.productOptionValueIds = [[NSMutableArray alloc] init];
    self.width = 150;
    self.height = 150;
    self.productViewed = NO;
    self.productLiked = NO;
    self.productWished = NO;
    self.productViewCount = 0;
    self.productLikeCount = 0;
    self.productOrderCount = 0;
    self.productWishListCount = 0;
    self.skuViewed = NO;
    self.skuLiked = NO;
    self.skuWished = NO;
    self.skuCommentCount = 0;
    self.skuViewCount = 0;
    self.skuLikeCount = 0;
    self.skuOrderCount = 0;
    self.skuWishListCount = 0;
    self.isProductSaleOff = YES;
    self.strSkuLikeCount = @"";
    self.strSkuOrderCount = @"";
    self.strSkuViewCount = @"";
    self.strSkuWishListCount = @"";
    self.isRating = NO;
    self.skuRatingCount = 0;
    self.userRatingPoint = 0;
    self.ratingPoint = 0;
    self.strSkuUserRatingPoint = @"0.0";
    self.strSkuRatingPoint = @"0.0";
    self.strSavingPercent = @"0.0";
    self.type = 0;
    self.lastComment = nil;
    self.remainInPromoTime = 0;
    self.isOnlyLoyalty = NO;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.skuId =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.productId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PRODUCT_ID]];
        self.categoryId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CATEGORY_ID]];
        self.name = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.desc = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DESC]];
        self.retailPrice =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_RETAIL_PRICE]];
        self.salePrice = [Helpers replacePriceWithCurrency:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_SALE_PRICE]]];
        
        NSString* strObj =  [[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_RETAIL_PRICE]] stringByReplacingOccurrencesOfString:@"," withString:@""];
        float fRetailPrice = [Helpers getDoubleDiffNull:strObj];
        fRetailPrice = floorf(fRetailPrice * 100) / 100;
        
        strObj =  [[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_SALE_PRICE]] stringByReplacingOccurrencesOfString:@"," withString:@""];
        float fSalePrice = [Helpers getDoubleDiffNull:strObj];
        fSalePrice = floorf(fSalePrice * 100) / 100;
        
        if (fSalePrice >= fRetailPrice) {
            self.isProductSaleOff = NO;
        }
        
        self.qtyOnHand = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_QTY_ONHAND]];
        self.displayOrder = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DISPLAY_ORDER]];
        self.status = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_STATUS]];
        self.primaryPhoto = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PRIMARY_PHOTO]];
        self.bannerPhoto = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_BANNER_PHOTO]];
        self.contentLink = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CONTENT_LINK]];
        self.manufacturer = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_MANUFACTURER]];
        self.model = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_MODEL]];
        self.createTime = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CREATE_TIME]];
        self.updateTime = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_UPDATE_TIME]];
        float savingPercent = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_SAVING_PERCENT]];
        long percent = lround(savingPercent);
        self.strSavingPercent = [NSString stringWithFormat:@"-%lu%%", percent];
        self.feature = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_FEATURE]];
        self.taxable = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_TAXABLE]];
        self.type = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_TYPE]];
        self.remainInPromoTime = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_REMAIN_IN_PROMO_TIME]];
        self.isOnlyLoyalty = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_ONLY_LOYALTY]];
        
        NSDictionary* dict;
        //dict = [Helpers getDictDiffNull:[map objectForKey:ENTITY_DISTRIBUTOR]];
        //self.distributor = (DistributorEntity*)[[EntityManager sharedClient] createItem:dict type:DISTRIBUTOR_ENTITY];
        
        NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_SKUS]];
        for (int i = 0; i < array.count; i++) {
            SKUEntity* entity = (SKUEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:SKU_ENTITY];
            [self.combos addObject:entity];
        }
        
        array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_PHOTOS]];
        for (int i = 0; i < array.count; i++) {
            [self.photos addObject:[array objectAtIndex:i]];
        }
        
        array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_PRODUCT_OPTVALUE_IDS]];
        for (int i = 0; i < array.count; i++) {
            [self.productOptionValueIds addObject:[array objectAtIndex:i]];
        }
        
        dict = [Helpers getDictDiffNull:[map objectForKey:ENTITY_PRODUCT_USER]];
        self.productViewed = [Helpers getBoolDiffNull:[dict objectForKey:ENTITY_VIEWED]];
        self.productLiked = [Helpers getBoolDiffNull:[dict objectForKey:ENTITY_LIKED]];
        self.productWished = [Helpers getBoolDiffNull:[dict objectForKey:ENTITY_WISHED]];
        
        dict = [Helpers getDictDiffNull:[map objectForKey:ENTITY_PRODUCT_STATS]];
        self.productViewCount = [Helpers getUIntDiffNull:[dict objectForKey:ENTITY_VIEW_COUNT]];
        self.productLikeCount = [Helpers getUIntDiffNull:[dict objectForKey:ENTITY_LIKE_COUNT]];
        self.productOrderCount = [Helpers getUIntDiffNull:[dict objectForKey:ENTITY_ORDER_COUNT]];
        self.productWishListCount = [Helpers getUIntDiffNull:[dict objectForKey:ENTITY_WISHLIST_COUNT]];
        
        dict = [Helpers getDictDiffNull:[map objectForKey:ENTITY_SKU_USER]];
        self.skuViewed = [Helpers getBoolDiffNull:[dict objectForKey:ENTITY_VIEWED]];
        self.skuLiked = [Helpers getBoolDiffNull:[dict objectForKey:ENTITY_LIKED]];
        self.skuWished = [Helpers getBoolDiffNull:[dict objectForKey:ENTITY_WISHED]];
        self.isRating = [Helpers getBoolDiffNull:[dict objectForKey:ENTITY_IS_RATING]];
        self.userRatingPoint = [Helpers getDoubleDiffNull:[dict objectForKey:ENTITY_RATING_POINT]];
        self.strSkuUserRatingPoint = [NSString stringWithFormat:@"%.1f", self.userRatingPoint];
        
        dict = [Helpers getDictDiffNull:[map objectForKey:ENTITY_SKU_STATS]];
        self.skuCommentCount = [Helpers getUIntDiffNull:[dict objectForKey:ENTITY_COMMENT_COUNT]];
        self.skuViewCount = [Helpers getUIntDiffNull:[dict objectForKey:ENTITY_VIEW_COUNT]];
        self.skuLikeCount = [Helpers getUIntDiffNull:[dict objectForKey:ENTITY_LIKE_COUNT]];
        self.skuOrderCount = [Helpers getUIntDiffNull:[dict objectForKey:ENTITY_ORDER_COUNT]];
        self.skuWishListCount = [Helpers getUIntDiffNull:[dict objectForKey:ENTITY_WISHLIST_COUNT]];
        self.skuRatingCount = [Helpers getUIntDiffNull:[dict objectForKey:ENTITY_RATING_COUNT]];
        self.ratingPoint = [Helpers getDoubleDiffNull:[dict objectForKey:ENTITY_RATING_POINT]];
        self.strSkuRatingPoint = [NSString stringWithFormat:@"%.1f", self.ratingPoint];
        
        self.strSkuLikeCount = [Helpers convertThousandToK:self.skuLikeCount];
        self.strSkuViewCount = [Helpers convertThousandToK:self.skuViewCount];
        self.strSkuOrderCount = [Helpers convertThousandToK:self.skuOrderCount];
        self.strSkuWishListCount = [Helpers convertThousandToK:self.skuWishListCount];
        
        NSString* retailPrice = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue-Medium; font-size: 12; color:#8899a6\">%@%@</span>", DEFAULT_CURRENCY, self.retailPrice];
        
        NSString* salePrice = @"";
        NSString* salePrice2 = @"";
        
        if (self.isOnlyLoyalty) {
            salePrice = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue-Bold; font-size: 18; color:#6c3f99\">%@ </font>", self.salePrice];
            salePrice2 = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue-Bold; font-size: 15; color:#6c3f99\">%@ </font>", self.salePrice];
        } else {
            salePrice = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue-Bold; font-size: 18; color:#6c3f99\">%@</font>", self.salePrice];
            salePrice2 = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue-Bold; font-size: 15; color:#6c3f99\">%@</font>", self.salePrice];
        }
        
        self.attributeRetailPrice = [[NSMutableAttributedString alloc] initWithData:[retailPrice dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        [self.attributeRetailPrice addAttribute:NSStrikethroughStyleAttributeName
                                          value:@1
                                          range:NSMakeRange(0, [self.attributeRetailPrice length])];
        
        self.attributeSalePrice = [[NSMutableAttributedString alloc] initWithData:[salePrice dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        self.attributeSalePrice2 = [[NSMutableAttributedString alloc] initWithData:[salePrice2 dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        
        if (self.isOnlyLoyalty) {
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image =  [Helpers imageWithImage:[UIImage imageNamed:@"ic_loyalty_product"] scaledToSize:CGSizeMake(14, 14)];
            attachment.bounds = CGRectMake(0, -2, attachment.image.size.width, attachment.image.size.height);
            NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
            [self.attributeSalePrice appendAttributedString:attachmentString];
            [self.attributeSalePrice2 appendAttributedString:attachmentString];
        }
        
        dict = [Helpers getDictDiffNull:[map objectForKey:ENTITY_LAST_CMT]];
        if ([dict allKeys].count > 0) {
            self.lastComment = (CommentEntity*)[[EntityManager sharedClient] createItem:dict type:COMMENT_ENTITY];
        }
    }
    return self;
}

@end

//
//  AddressEntity.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface TSAddressEntity : BaseEntity

@property (nonatomic, strong) NSString* addressId;
@property (nonatomic, strong) NSString* addressLine1;
@property (nonatomic, strong) NSString* addressLine2;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* state;
@property (nonatomic, strong) NSString* country;
@property (nonatomic, strong) NSString* zipCode;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* countryCode;
@property (nonatomic, strong) NSString* placeId;
@property (nonatomic, strong) NSString* fullAddress;
@property (nonatomic, assign) double lon;
@property (nonatomic, assign) double lat;

- (NSString*)getAddress1;
- (NSString*)getCityStateCountry;
- (NSString*)getFullAddress;

- (instancetype)init;

@end

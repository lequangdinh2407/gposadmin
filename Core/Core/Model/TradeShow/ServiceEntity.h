//
//  ServiceEntity.h
//  Core
//
//  Created by Dragon on 2/21/17.
//  Copyright © 2017 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

@interface ServiceEntity : BaseEntity

@property (nonatomic, strong) NSString* serviceId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* photo;
//@property (nonatomic, strong) NSString* newPrice;
@property (nonatomic, strong) NSString* oldPrice;
@property (nonatomic, strong) NSString* buyNumber;
@property (nonatomic, strong) NSString* saleOff;


@end

//
//  ProductOptionValueEntity.h
//  Core
//
//  Created by ToanPham on 12/28/15.
//  Copyright © 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface ProductOptionValueEntity : BaseEntity

@property (nonatomic, strong) NSString* productOptionValueId;
@property (nonatomic, strong) NSString* productOptionId;
@property (nonatomic, strong) NSString* value;
@property (nonatomic, strong) NSString* displayOrder;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* photo;
- (instancetype)init;

@end

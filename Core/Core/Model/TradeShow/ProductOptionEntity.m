//
//  ProductOptionEntity.m
//  Core
//
//  Created by ToanPham on 12/28/15.
//  Copyright © 2015 ToanPham. All rights reserved.
//

#import "ProductOptionEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation ProductOptionEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.productOptionId = @"";
    self.name = @"";
    self.label = @"";
    self.type = 0;
    self.displayOrder = @"";
    self.productOptionValues = [[NSMutableArray alloc] init];
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.productOptionId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.name = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.label = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_LABEL]];
        self.type = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_TYPE]];
        self.displayOrder = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DISPLAY_ORDER]];
        NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_PRODUCT_OPTVALUES]];
        if (array) {
            for (int i = 0; i < array.count; i++) {
                ProductOptionValueEntity* entity = (ProductOptionValueEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:PRODUCT_OPT_VALUE_ENTITY];
                [self.productOptionValues addObject:entity];
            }
        }
    }
    return self;
}

@end

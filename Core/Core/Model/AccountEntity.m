//
//  AccountEntity.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "AccountEntity.h"
#import "RLMAccount.h"

@implementation AccountEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.userId = @"";
    self.password = @"";
    self.userId = @"";
    
    return self;
}

- (NSObject*)initWithRLMObject:(RLMObject*)rlmObj {
    self = [self init];
    if ([rlmObj isKindOfClass:[RLMAccount class]]) {
        RLMAccount* rlmAccount = (RLMAccount*)rlmObj;
        self.userId = rlmAccount.userId;
        self.password = rlmAccount.password;
        self.userName = rlmAccount.userName;
    }
    
    return self;
}

@end

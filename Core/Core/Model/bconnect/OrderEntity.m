//
//  OrderEntity.m
//  Core
//
//  Created by ToanPham on 1/11/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "OrderEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation OrderEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.orderId = @"";
    self.orderNumber = @"";
    self.gardenId = @"";
    self.totalPrice = @"";
    self.totalSaving = @"";
    self.totalRevenue = @"";
    self.servicePrice = @"";
    self.cancelPrice = @"";
    self.canceledBy = @"";
    self.preStatus = 0;
    self.status = 0;
    self.groupStatus = 0;
   
    self.baseCommonServicesPrice = @"";
    self.commonServices = [[NSMutableArray alloc] init];
    self.proEstimateCommonServicesPrice = @"";
    self.customServices = [[NSMutableArray alloc] init];
    self.comment = @"";
    self.photos = [[NSMutableArray alloc] init];
    self.clientUploadedPhotos = [[NSMutableArray alloc] init];
    return self;
}



@end

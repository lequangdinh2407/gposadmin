//
//  NewsEntity.m
//  Core
//
//  Created by ToanPham on 1/19/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "NewsEntity.h"
#import "Helpers.h"
#import "JSONUtils.h"

@implementation NewsEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.type = 0;
    self.content = nil;
    self.createTime = @"";
    self.newsId = @"";
    self.isViewed = NO;
    self.attrString = [[NSAttributedString alloc] initWithData:[@"" dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];    
    return self;
}

- (NSString*)getMessageFromSocialFeedNotification {
    NSArray* userNames = [Helpers getArrayDiffNull:[(NSDictionary*)self.content objectForKey:@"fromUserNames"]];
    NSString* names = @"";
    NSString* msg = @"";
    msg = [CoreStringUtils getStringDiffNull:[(NSDictionary*)self.content objectForKey:@"msg"]];

    if (userNames.count == 1) {
        names = [CoreStringUtils getStringDiffNull:[userNames objectAtIndex: 0]];
        msg = [NSString stringWithFormat:@"<span style=\"font-family: SanFranciscoDisplay-Regular; font-size: 15; color:#373d3b\"><b>%@</b> %@</span>", names, msg];

    } else if (userNames.count == 2) {
        NSString* name1 = [CoreStringUtils getStringDiffNull:[userNames objectAtIndex:0]];
        NSString* name2 = [CoreStringUtils getStringDiffNull:[userNames objectAtIndex:1]];
        //names = [NSString stringWithFormat:@"%@ and %@",name1, name2];
        msg = [NSString stringWithFormat:@"<span style=\"font-family: SanFranciscoDisplay-Regular; font-size: 15; color:#373d3b\"><b>%@</b> and <b>%@</b> %@</span>", name1, name2, msg];
        
    } else if (userNames.count == 3) {
        NSString* name1 = [CoreStringUtils getStringDiffNull:[userNames objectAtIndex:0]];
        NSString* name2 = [CoreStringUtils getStringDiffNull:[userNames objectAtIndex:1]];
        //        names = [NSString stringWithFormat:@"%@, %@ and 1 other",name1, name2];
        
        msg = [NSString stringWithFormat:@"<span style=\"font-family: SanFranciscoDisplay-Regular; font-size: 15; color:#373d3b\"><b>%@</b>, <b>%@</b> and <b>%@</b> %@</span>", name1, name2, @"1 other", msg];
        
        
    } else {
        NSString* name1 = [CoreStringUtils getStringDiffNull:[userNames objectAtIndex:0]];
        NSString* name2 = [CoreStringUtils getStringDiffNull:[userNames objectAtIndex:1]];
        msg = [NSString stringWithFormat:@"<span style=\"font-family: SanFranciscoDisplay-Regular; font-size: 15; color:#373d3b\"><b>%@</b>, <b>%@</b> and <b>%@</b> %@</span>", name1, name2, [NSString stringWithFormat:@"%u others", (unsigned int)(userNames.count - 2)], msg];
        
        
        //        names = [NSString stringWithFormat:@"%@, %@ and %u others",name1, name2, (unsigned int)(userNames.count - 2)];
    }

    return msg;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.type = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_TYPE]];
        NSObject* content = [map objectForKey:ENTITY_CONTENT];
        if (content != nil && ([content isKindOfClass:[NSDictionary class]] || [content isKindOfClass:[NSArray class]])) {
            self.content = content;
        } else {
            self.content = [[NSDictionary alloc] init];
        }        
        self.createTime = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CREATED_DATE]];
        self.newsId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.isViewed = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_VIEWED]];
        
        NSString* msg = @"";
        if (self.type == SocialNotiType) {
            msg = [self getMessageFromSocialFeedNotification];
        } else {
            msg = [NSString stringWithFormat:@"<span style=\"font-family: SanFranciscoDisplay-Regular; font-size: 15; color:#000000\">%@</span>", [CoreStringUtils getStringDiffNull:[(NSDictionary*)content objectForKey:@"msg"]]];
        }
        self.attrString = [[NSAttributedString alloc] initWithData:[msg dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    }
    return self;
}

@end

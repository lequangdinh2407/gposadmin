//
//  BizCategoryEntity.m
//  Core
//
//  Created by ToanPham on 9/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "CategoryEntity.h"

@implementation CategoryEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.catId = @"";
    self.catName = @"";
    self.desc = @"";
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        //self.distributor = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DISTRIBUTOR]];
        self.catId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.catName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.desc = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DESC]];
    }
    return self;
}

- (BOOL)isEqual:(id)other {
    if (other == self)
        return YES;
    if (!other || ![other isKindOfClass:[self class]])
        return NO;
    CategoryEntity *entity = (CategoryEntity*)other;
    return [self.catId isEqualToString:entity.catId];
}

- (NSUInteger)hash {
    return self.catId.hash;
}

@end

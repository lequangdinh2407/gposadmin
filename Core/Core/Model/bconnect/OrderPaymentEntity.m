//
//  OrderPaymentEntity.m
//  Core
//
//  Created by ToanPham on 1/11/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "OrderPaymentEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation OrderPaymentEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.orderId = @"";
    self.firstName = @"";
    self.lastName = @"";
    self.addressLine1 = @"";
    self.addressLine2 = @"";
    self.phone = @"";
    self.city = @"";
    self.state = @"";
    self.country = @"";
    self.zipCode = @"";
    self.countryCode = @"";
    self.status = @"";
    self.gateWay = @"";
    self.paymentOption = nil;
    self.createTime = @"";
    self.updateTime = @"";
    self.transactionId = @"";
    
    return self;
}

- (NSString*)getDisplayName {
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

- (NSString*)fullAddress {
    NSString*tmpAddress = @"";
    if (![CoreStringUtils isEmpty:self.addressLine1]) {
        tmpAddress = [tmpAddress stringByAppendingString:self.addressLine1];
    }
    if (![CoreStringUtils isEmpty:self.addressLine2]) {
        tmpAddress = [tmpAddress stringByAppendingFormat:@", %@", self.addressLine2];
    }
    return tmpAddress;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.orderId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ORDER_ID]];
        self.firstName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_FIRST_NAME]];
        self.lastName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_LAST_NAME]];
        self.addressLine1 = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ADDRESSLINE1]];
        self.addressLine2 = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ADDRESSLINE2]];
        self.phone = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PHONE]];
        self.city = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CITY]];
        self.state = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_STATE]];
        self.country = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_COUNTRY]];
        self.countryCode = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_COUNTRY_CODE]];
        self.zipCode = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ZIP_CODE]];
        self.gateWay = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ZIP_CODE]];
        //self.paymentOption = (PaymentOptionEntity*)[[EntityManager sharedClient] createItem:[map objectForKey:ENTITY_PAYMENT_OPTION] type:PAYMENT_OPTION_ENTITY];
        self.status = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_STATUS]];
        self.createTime = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CREATE_TIME]];
        self.updateTime = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_UPDATE_TIME]];
        self.transactionId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TRANSACTION_ID]];
        
        NSString* name = [NSString stringWithFormat:@"<span style=\"font-family: SanFranciscoDisplay-BoldItalic; font-size: 16; color:#2a2a2a\">%@</span>", [self getDisplayName]];
        NSString* info = @"";
        
        info = [NSString stringWithFormat:@"<span style=\"font-family: SanFranciscoDisplay-Italic; font-size: 16; color:#8899a6; line-height:23px\">%@<br/>%@<br/>%@</span>", [self fullAddress], [self getCityStateCountry], self.phone];
        
        NSString* content = [NSString stringWithFormat:@"%@<br/>%@", name, info];
        self.attributedPaymentInfo = [[NSMutableAttributedString alloc] initWithData:[content dataUsingEncoding:NSUTF16StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    }
    return self;
}

- (NSString*)getCityStateCountry {
    NSString*tmpAddress = @"";
    
    if (![CoreStringUtils isEmpty:self.city]) {
        tmpAddress = [tmpAddress stringByAppendingFormat:@"%@", self.city];
    }
    if (![CoreStringUtils isEmpty:self.state]) {
        tmpAddress = [tmpAddress stringByAppendingFormat:@", %@", self.state];
        
        if (![CoreStringUtils isEmpty:self.zipCode]) {
            tmpAddress = [tmpAddress stringByAppendingFormat:@" %@", self.zipCode];
        }
    }
    if (![CoreStringUtils isEmpty:self.country]) {
        tmpAddress = [tmpAddress stringByAppendingFormat:@", %@", self.country];
    }
    
    return tmpAddress;
}

- (NSString*)getFullAddress {
    NSString*tmpAddress = @"";
    if (![CoreStringUtils isEmpty:self.addressLine1]) {
        tmpAddress = [tmpAddress stringByAppendingString:self.addressLine1];
    }
    if (![CoreStringUtils isEmpty:self.addressLine2]) {
        tmpAddress = [tmpAddress stringByAppendingFormat:@", %@", self.addressLine2];
    }
    
    return tmpAddress;
}

@end

//
//  CartItemEntity.m
//  Core
//
//  Created by Dragon on 2/21/17.
//  Copyright © 2017 ToanPham. All rights reserved.
//

#import "CartItemEntity.h"
#import "EntityManager.h"
#import "Helpers.h"

@implementation CartItemEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.sku = nil;
    self.quantity = 0;
    self.totalPrice = 0;
    self.totalDiscount = 0;
    self.totalOriginalPrice = 0;
    self.totalRetailPrice = 0;
    self.totalSaving = 0;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.sku =  (SKUEntity*)[[EntityManager sharedClient] createItem:[map objectForKey:ENTITY_SKU] type:SKU_ENTITY];
        
        self.quantity = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_QUANTITY]];
        
        NSString* strObj =  [[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_PRICE]] stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.totalPrice = [Helpers getDoubleDiffNull:strObj];
        
        strObj =  [[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_DISCOUNT]] stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.totalDiscount = [Helpers getDoubleDiffNull:strObj];
        
        strObj =  [[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_RETAIL_PRICE]] stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.totalRetailPrice = [Helpers getDoubleDiffNull:strObj];
        
        strObj =  [[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_ORIGINAL_PRICE]] stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.totalOriginalPrice = [Helpers getDoubleDiffNull:strObj];
        
        strObj =  [[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_SAVING]] stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.totalSaving = [Helpers getDoubleDiffNull:strObj];
    }
    return self;
}

@end

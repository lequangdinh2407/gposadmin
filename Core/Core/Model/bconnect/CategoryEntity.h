//
//  BizCategoryEntity.h
//  Core
//
//  Created by ToanPham on 9/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CatEntity.h"

@interface CategoryEntity : CatEntity

@property (nonatomic, strong) NSString* desc;
@property (nonatomic, strong) NSString* distributor;

- (instancetype)init;

@end

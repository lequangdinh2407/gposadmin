//
//  CartItemEntity.h
//  Core
//
//  Created by Dragon on 2/21/17.
//  Copyright © 2017 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"
@class SKUEntity;

@interface CartItemEntity : BaseEntity

@property (nonatomic, strong) SKUEntity* sku;
@property (nonatomic) float totalPrice;
@property (nonatomic) int quantity;
@property (nonatomic) float totalDiscount;
@property (nonatomic) float totalOriginalPrice;
@property (nonatomic) float totalRetailPrice;
@property (nonatomic) float totalSaving;

@end

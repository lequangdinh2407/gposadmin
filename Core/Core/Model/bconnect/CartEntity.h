//
//  CartEntity.h
//  Core
//
//  Created by ToanPham on 1/4/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@class PromotionReceptionEntity;

@interface CartEntity : BaseEntity

@property (nonatomic, strong) NSMutableArray* cartItems;
@property (nonatomic, strong) NSMutableArray* bcCartItems;
@property (nonatomic, strong) NSMutableArray* cndCartItems;
@property (nonatomic, strong) NSMutableArray* promotionCombinations;
@property (nonatomic, strong) NSMutableArray* selectedReceptions;
@property (nonatomic, strong) NSMutableArray* cndSelectedReceptions;
@property (nonatomic, strong) NSMutableArray* bcSelectedReceptions;

@property (nonatomic, strong) NSString* promoCode;
@property (nonatomic) int totalSku;
@property (nonatomic) int totalQuantity;
@property (nonatomic) float totalOriginalPrice;
@property (nonatomic) float totalPrice;
@property (nonatomic) float totalDiscount;
@property (nonatomic) float totalRetailPrice;
@property (nonatomic) float totalSaving;
@property (nonatomic, strong) NSString* totalOriginalPriceString;
@property (nonatomic, strong) NSString* totalPriceString;
@property (nonatomic, strong) NSString* totalDiscountString;
@property (nonatomic, strong) NSString* totalRetailPriceString;
@property (nonatomic, strong) NSString* totalSavingString;

@property (nonatomic, strong) NSString* bcTotalOriginalPriceString;
@property (nonatomic, strong) NSString* bcTotalPriceString;
@property (nonatomic, strong) NSString* bcTotalDiscountString;
@property (nonatomic, strong) NSString* bcTotalRetailPriceString;
@property (nonatomic, strong) NSString* bcTotalSavingString;
@property (nonatomic, strong) NSString* bcLoyaltyRequire;

@property (nonatomic, strong) NSString* cndTotalOriginalPriceString;
@property (nonatomic, strong) NSString* cndTotalPriceString;
@property (nonatomic, strong) NSString* cndTotalDiscountString;
@property (nonatomic, strong) NSString* cndTotalRetailPriceString;
@property (nonatomic, strong) NSString* cndTotalSavingString;
@property (nonatomic, strong) NSString* cndLoyaltyRequire;
@end
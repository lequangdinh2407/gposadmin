//
//  OrderPaymentEntity.h
//  Core
//
//  Created by ToanPham on 1/11/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@class PaymentOptionEntity;

@interface OrderPaymentEntity : BaseEntity

@property (nonatomic, strong) NSString* orderId;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* addressLine1;
@property (nonatomic, strong) NSString* addressLine2;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* state;
@property (nonatomic, strong) NSString* country;
@property (nonatomic, strong) NSString* zipCode;
@property (nonatomic, strong) NSString* countryCode;
@property (nonatomic, strong) NSString* gateWay;
@property (nonatomic, strong) PaymentOptionEntity* paymentOption;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) NSString* createTime;
@property (nonatomic, strong) NSString* updateTime;
@property (nonatomic, strong) NSString* transactionId;
@property (nonatomic, strong) NSMutableAttributedString* attributedPaymentInfo;

- (instancetype)init;
- (NSString*)getDisplayName;
- (NSString*)fullAddress;

@end
//
//  NewsEntity.h
//  Core
//
//  Created by ToanPham on 1/19/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface NewsEntity : BaseEntity

@property (nonatomic) int type;
@property (nonatomic, strong) NSObject* content;
@property (nonatomic, strong) NSString* createTime;
@property (nonatomic, strong) NSString* newsId;
@property (nonatomic, strong) NSAttributedString* attrString;
@property (nonatomic) BOOL isViewed;

- (instancetype)init;

@end
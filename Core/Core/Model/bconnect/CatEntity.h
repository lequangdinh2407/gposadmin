//
//  CatEntity.h
//  Core
//
//  Created by Toan Pham Thanh on 3/21/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface CatEntity : BaseEntity
@property (nonatomic, strong) NSString* catId;
@property (nonatomic, strong) NSString* catName;
@end

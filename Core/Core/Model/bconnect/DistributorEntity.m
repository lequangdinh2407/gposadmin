//
//  BizCategoryEntity.m
//  Core
//
//  Created by ToanPham on 9/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "DistributorEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation DistributorEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.categories = [[NSMutableArray alloc] init];
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [super initWithData:data];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        NSArray* categories = [Helpers getArrayDiffNull:[map objectForKey:RESPONSE_CATEGORIES]];
        
        CategoryEntity* allCat = [[CategoryEntity alloc] init];
        allCat.catName = @"All";
        allCat.catId = @"0";
        allCat.distributor = self.catId;
        [self.categories addObject:allCat];
        
        for (int i = 0; i < categories.count; i++) {
            CategoryEntity* entity = (CategoryEntity*)[[EntityManager sharedClient] createItem:[categories objectAtIndex:i] type:CATEGORY_ENTITY];
            [self.categories addObject:entity];
        }
    }
    return self;
}

@end

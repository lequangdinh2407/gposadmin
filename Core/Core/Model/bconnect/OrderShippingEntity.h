//
//  OrderShippingEntity.h
//  Core
//
//  Created by ToanPham on 1/11/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@class ShippingOptionEntity;

@interface OrderShippingEntity : BaseEntity

@property (nonatomic, strong) NSString* orderId;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* addressLine1;
@property (nonatomic, strong) NSString* addressLine2;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* state;
@property (nonatomic, strong) NSString* country;
@property (nonatomic, strong) NSString* zipCode;
@property (nonatomic, strong) NSString* countryCode;
@property (nonatomic, strong) NSString* businessName;
@property (nonatomic, strong) NSString* carrier;
@property (nonatomic, strong) NSString* trackingNumber;
@property (nonatomic, strong) NSString* trackingURL;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) ShippingOptionEntity* shippingOption;
@property (nonatomic, strong) NSMutableAttributedString* attributedShippingInfo;

- (instancetype)init;
- (NSString*)getDisplayName;
- (NSString*)fullAddress;

@end
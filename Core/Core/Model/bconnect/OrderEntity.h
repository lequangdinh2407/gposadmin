//
//  OrderEntity.h
//  Core
//
//  Created by ToanPham on 1/11/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"



@interface OrderEntity : BaseEntity

@property (nonatomic, strong) NSString* orderId;
@property (nonatomic, strong) NSString* orderNumber;
@property (nonatomic, strong) NSString* gardenId;
@property (nonatomic, strong) NSString* totalPrice;
@property (nonatomic, strong) NSString* cancelPrice;
@property (nonatomic, strong) NSString* totalSaving;
@property (nonatomic, strong) NSString* totalRevenue;
@property (nonatomic, strong) NSString* servicePrice;
@property (nonatomic, strong) NSString* createdDate;
@property (nonatomic, strong) NSString* canceledBy;

@property (nonatomic, assign) int preStatus;
@property (nonatomic, assign) int status;
@property (nonatomic, assign) int groupStatus;


@property (nonatomic, strong) NSString* baseCommonServicesPrice;
@property (nonatomic, strong) NSMutableArray* commonServices;

@property (nonatomic, strong) NSString* proEstimateCommonServicesPrice;
@property (nonatomic, strong) NSMutableArray* customServices;

@property (nonatomic, strong) NSString* comment;

@property (nonatomic, strong) NSMutableArray* photos;
@property (nonatomic, strong) NSMutableArray* clientUploadedPhotos;

- (instancetype)init;

@end

//
//  CartEntity.m
//  Core
//
//  Created by ToanPham on 1/4/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "CartEntity.h"
#import "EntityManager.h"
#import "Helpers.h"

@implementation CartEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.cartItems = [[NSMutableArray alloc] init];
    self.bcCartItems = [[NSMutableArray alloc] init];
    self.cndCartItems = [[NSMutableArray alloc] init];
    self.promotionCombinations = [[NSMutableArray alloc] init];
    self.selectedReceptions = [[NSMutableArray alloc] init];
    self.cndSelectedReceptions = [[NSMutableArray alloc] init];
    self.bcSelectedReceptions = [[NSMutableArray alloc] init];
    
    self.promoCode = @"";
    self.totalSku = 0;
    self.totalQuantity = 0;
    self.totalOriginalPrice = 0;
    self.totalPrice = 0;
    self.totalDiscount = 0;
    self.totalRetailPrice = 0;
    self.totalOriginalPriceString = @"";
    self.totalDiscountString = @"";
    self.totalPriceString = @"";
    self.totalDiscountString = @"";
    
    self.bcTotalPriceString = @"";
    self.bcTotalDiscountString = @"";
    self.bcTotalPriceString = @"";
    self.bcTotalDiscountString = @"";
    self.bcLoyaltyRequire = @"";
    
    self.cndTotalPriceString = @"";
    self.cndTotalDiscountString = @"";
    self.cndTotalPriceString = @"";
    self.cndTotalDiscountString = @"";
    self.cndLoyaltyRequire = @"";
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_CART_ITEMS]];
        for (int i = 0; i < array.count; i++) {
            //CartItemEntity* entity = (CartItemEntity*) [[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:CART_ITEM_ENTITY];
            //[self.cartItems addObject:entity];
        }
        array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_BC_CART_ITEMS]];
        for (int i = 0; i < array.count; i++) {
            //CartItemEntity* entity = (CartItemEntity*) [[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:CART_ITEM_ENTITY];
            //[self.bcCartItems addObject:entity];
        }
        array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_CND_CART_ITEMS]];
        for (int i = 0; i < array.count; i++) {
            //CartItemEntity* entity = (CartItemEntity*) [[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:CART_ITEM_ENTITY];
            //[self.cndCartItems addObject:entity];
        }
        self.totalSku = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_TOTAL_SKU]];
        self.promoCode = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PROMO_CODE]];

        NSArray* promotionCombinations = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_PROMOTION_COMBINATIONS]];
        for (int i = 0; i < promotionCombinations.count; i++) {
            //PromotionCombinationEntity* entity = (PromotionCombinationEntity*)[[EntityManager sharedClient] createItem:[promotionCombinations objectAtIndex:i] type:PROMOTION_COMBINATION_ENTITY];
            //[self.promotionCombinations addObject:entity];
        }

        NSArray* selectedReceptions = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_SELECTED_RECEPTIONS]];
        for (int i = 0; i < selectedReceptions.count; i++) {
            NSDictionary* data = [[selectedReceptions objectAtIndex:i] objectForKey:ENTITY_PROMOTION_RECEPTION];
            id quantity = [[selectedReceptions objectAtIndex:i] objectForKey:ENTITY_QUANTITY];
//            PromotionReceptionEntity* entity = (PromotionReceptionEntity*)[[EntityManager sharedClient] createItem:data type:PROMOTION_RECEPTION_ENTITY];
//            [self.selectedReceptions addObject:@{@"quantity" : quantity, @"promotionReception" : entity}];
        }
        
        NSArray* cndSelectedReceptions = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_CND_SELECTED_RECEPTIONS]];
        for (int i = 0; i < cndSelectedReceptions.count; i++) {
            NSDictionary* data = [[selectedReceptions objectAtIndex:i] objectForKey:ENTITY_PROMOTION_RECEPTION];
            id quantity = [[selectedReceptions objectAtIndex:i] objectForKey:ENTITY_QUANTITY];
//            PromotionReceptionEntity* entity = (PromotionReceptionEntity*)[[EntityManager sharedClient] createItem:data type:PROMOTION_RECEPTION_ENTITY];
//            [self.cndSelectedReceptions addObject:@{@"quantity" : quantity, @"promotionReception" : entity}];
        }
        
        NSArray* bcSelectedReceptions = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_BC_SELECTED_RECEPTIONS]];
        for (int i = 0; i < bcSelectedReceptions.count; i++) {
            NSDictionary* data = [[selectedReceptions objectAtIndex:i] objectForKey:ENTITY_PROMOTION_RECEPTION];
            id quantity = [[selectedReceptions objectAtIndex:i] objectForKey:ENTITY_QUANTITY];
//            PromotionReceptionEntity* entity = (PromotionReceptionEntity*)[[EntityManager sharedClient] createItem:data type:PROMOTION_RECEPTION_ENTITY];
//            [self.bcSelectedReceptions addObject:@{@"quantity" : quantity, @"promotionReception" : entity}];
        }
        
        NSString* strObj =  [[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_PRICE]] stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.totalPrice = [Helpers getDoubleDiffNull:strObj];
        
        self.totalQuantity = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_TOTAL_QUANTITY]];

        strObj =  [[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_DISCOUNT]] stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.totalDiscount = [Helpers getDoubleDiffNull:strObj];
        
        strObj =  [[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_RETAIL_PRICE]] stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.totalRetailPrice = [Helpers getDoubleDiffNull:strObj];

        strObj =  [[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_ORIGINAL_PRICE]] stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.totalOriginalPrice = [Helpers getDoubleDiffNull:strObj];
        
        strObj =  [[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_SAVING]] stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.totalSaving = [Helpers getDoubleDiffNull:strObj];

        self.totalOriginalPriceString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_ORIGINAL_PRICE]]];
        self.totalRetailPriceString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_RETAIL_PRICE]]];
        self.totalDiscountString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_DISCOUNT]]];
        self.totalPriceString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_PRICE]]];
        self.totalSavingString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_TOTAL_SAVING]]];

        self.bcTotalOriginalPriceString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_BC_TOTAL_ORIGINAL_PRICE]]];
        self.bcTotalRetailPriceString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_BC_TOTAL_RETAIL_PRICE]]];
        self.bcTotalDiscountString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_BC_TOTAL_DISCOUNT]]];
        self.bcTotalPriceString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_BC_TOTAL_PRICE]]];
        self.bcTotalSavingString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_BC_TOTAL_SAVING]]];
        self.bcLoyaltyRequire = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_BC_LOYALTY_REQUIRE]]];
        
        self.cndTotalOriginalPriceString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CND_TOTAL_ORIGINAL_PRICE]]];
        self.cndTotalRetailPriceString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CND_TOTAL_RETAIL_PRICE]]];
        self.cndTotalDiscountString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CND_TOTAL_DISCOUNT]]];
        self.cndTotalPriceString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CND_TOTAL_PRICE]]];
        self.cndTotalSavingString = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CND_TOTAL_SAVING]]];
        self.cndLoyaltyRequire = [Helpers replacePriceWithSmall:[CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CND_LOYALTY_REQUIRE]]];
    }
    return self;
}

@end

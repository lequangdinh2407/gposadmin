//
//  CountryEntity.h
//  spabeecore
//
//  Created by ToanPham on 2/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface CountryEntityV2 : BaseEntity

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* isoCode;
@property (nonatomic, strong) NSString* phoneCode;
@property (nonatomic) int status;

- (instancetype)init;
@end

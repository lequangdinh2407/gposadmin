//
//  StateEntity.m
//  spabeecore
//
//  Created by ToanPham on 2/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "StateEntity.h"
#import "Helpers.h"

@implementation StateEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.name = @"";
    self.isoCode = @"";
    self.status = 0;
    
    return self;
}

- (NSString*)getFieldName {
    return self.name;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.name = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.isoCode = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ISO_CODE]];
        self.status = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_STATUS]];
    }
    return self;
}

@end

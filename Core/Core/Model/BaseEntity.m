//
//  BaseEntity.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/31/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "BaseEntity.h"

@implementation BaseEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.fieldName = @"";
    self.priority = 0;
    return self;
}

- (NSString*)getFieldName {
    return self.fieldName;
}

- (NSComparisonResult)compare:(BaseEntity *)otherObject {
    NSInteger p1 = self.priority;
    NSInteger p2 = otherObject.priority;
    
    if (p1 < p2 ){
        return 1;
    }
    else if (p1 > p2){
        return -1;
    }
    return 0;
}

- (NSObject*)initWithData:(NSObject*)data {
    return [self init];
}

- (NSObject*)initWithRLMObject:(RLMObject*)rlmObj {
    return [self init];
}

- (NSObject*)convertToData {
    return nil;
}

@end

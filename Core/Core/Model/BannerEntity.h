//
//  BarEntity.h
//  luvkonectcore
//
//  Created by ToanPham on 3/15/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"
#import "UploadPhotoEntity.h"

@interface BannerEntity : BaseEntity

@property (nonatomic, strong) NSString* bannerId;
@property (nonatomic, strong) NSString* content;
@property (nonatomic, strong) NSString* msg;
@property (nonatomic, strong) UploadPhotoEntity* photo;
@property (nonatomic) int type;
@property (nonatomic) int objectType;
@property (nonatomic, strong) NSString* objectId;

@end

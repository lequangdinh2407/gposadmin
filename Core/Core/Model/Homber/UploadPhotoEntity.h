//
//  UploadPhotoEntity.h
//  Core
//
//  Created by ToanPham on 1/9/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface UploadPhotoEntity : BaseEntity

@property (nonatomic, strong) NSString* photoId;
@property (nonatomic, strong) NSString* link;
@property (nonatomic) float height;
@property (nonatomic) float width;

@end

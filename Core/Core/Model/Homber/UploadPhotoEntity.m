//
//  UploadPhotoEntity.m
//  Core
//
//  Created by ToanPham on 1/9/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "UploadPhotoEntity.h"
#import "Helpers.h"

@implementation UploadPhotoEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.photoId = @"";
    self.link = @"";
    self.width = 0;
    self.height = 0;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.photoId =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.link =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_LINK]];
        self.width = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_WIDTH]];
        self.height = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_HEIGHT]];
    }
    return self;
}

@end

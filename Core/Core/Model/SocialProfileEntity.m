//
//  SocialProfileEntity.m
//  Core
//
//  Created by Dragon on 2/23/17.
//  Copyright © 2017 ToanPham. All rights reserved.
//

#import "SocialProfileEntity.h"
#import "Helpers.h"
#import "EntityManager.h"
#import "CoreStringUtils.h"
#import "RLMSocialProfile.h"

@implementation SocialProfileEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
  
    self.userId = @"";
    self.socialProfileId = @"";
    self.firstName = @"";
    self.lastName = @"";
    self.email = @"";
    self.birthday = @"";
    self.phone = @"";
    self.displayName = @"";
    self.avatar = @"";
    self.gender = 1;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        self.socialProfileId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.firstName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_FIRST_NAME]];
        self.lastName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_LAST_NAME]];
        self.email = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_EMAIL]];
        self.birthday = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_BIRTHDAY]];
        self.phone = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PHONE]];
        self.avatar = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_AVATAR]];
        self.gender = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_GENDER]];
        self.countryCode = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_COUNTRY_CODE]];
        
    }
    return self;
}

- (NSString*)displayName {
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

- (NSObject*)initWithRLMObject:(RLMObject*)rlmObj {
    self = [self init];
    if ([rlmObj isKindOfClass:[RLMSocialProfile class]]) {
        RLMSocialProfile* rlmSocialProfile = (RLMSocialProfile*)rlmObj;
        self.userId = rlmSocialProfile.userId;
        self.firstName = rlmSocialProfile.firstName;
        self.lastName = rlmSocialProfile.lastName;
        self.email = rlmSocialProfile.email;
        self.birthday = rlmSocialProfile.birthday;
        self.countryCode = rlmSocialProfile.countryCode;
        self.phone = rlmSocialProfile.phone;
        self.avatar = rlmSocialProfile.avatar;
        self.type = rlmSocialProfile.type;
        self.gender = rlmSocialProfile.gender;
        
    }
    return self;
}

@end

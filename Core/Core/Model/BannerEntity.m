//
//  BarEntity.m
//  luvkonectcore
//
//  Created by ToanPham on 3/15/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "BannerEntity.h"
#import "Helpers.h"
#import "CoreStringUtils.h"
#import "EntityManager.h"
@implementation BannerEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.bannerId = @"";
    self.content = @"";
    self.type = 0;
    self.photo = [[UploadPhotoEntity alloc] init];
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.bannerId = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.type = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_TYPE]];
        self.content = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CONTENT]];
        self.photo = (UploadPhotoEntity*)[[EntityManager sharedClient] createItem:[map objectForKey:ENTITY_THUMB] type:UPLOAD_PHOTO_ENTITY];
        self.msg = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_MSG]];
        
        NSDictionary *objRef = [map objectForKey:ENTITY_OBJECT_REF];
        self.objectType = [Helpers getUIntDiffNull:[objRef objectForKey:ENTITY_TYPE]];
        self.objectId = [CoreStringUtils getStringDiffNull:[objRef objectForKey:ENTITY_ID]];
    }
    return self;
}

@end

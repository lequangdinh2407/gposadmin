//
//  AddressEntity.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "AddressEntity.h"

@implementation AddressEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.city = @"";
    self.state = @"";
    self.country = @"USA";
    self.lon = 0;
    self.lat = 0;
    self.zipCode = @"";
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.city = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PLACENAME]];
        self.state = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_STATE]];
    }
    return self;
}

@end

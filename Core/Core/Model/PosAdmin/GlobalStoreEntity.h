//
//  GlobalStoreEntity.h
//  Core
//
//  Created by Inglab on 27/12/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

@interface GlobalStoreEntity : BaseEntity
@property (nonatomic, strong) NSString* address;
@property (nonatomic, strong) NSString* createdDate;
@property (nonatomic, strong) NSString* updatedDate;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* user;
@property (nonatomic, strong) NSString* password;
@property (nonatomic, strong) NSString* storeId;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* device;
@property (nonatomic, strong) NSString* shopCode;
@property (nonatomic, strong) NSString* shopCodeBase;

@property (nonatomic, assign) BOOL active;
@property (nonatomic, assign) BOOL deleted;
@property (nonatomic, assign) BOOL isNew;
@property (nonatomic, assign) BOOL newed;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) int Id;
@property (nonatomic, assign) double coordinateLon;
@property (nonatomic, assign) double coordinateLat;

@end


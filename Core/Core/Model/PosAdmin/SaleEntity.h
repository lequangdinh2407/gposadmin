//
//  SaleEntity.h
//  Core
//
//  Created by Inglab on 13/02/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

@interface SaleEntity : BaseEntity

@property (nonatomic, strong) NSString* startTime;
@property (nonatomic, strong) NSString* endTime;
@property (nonatomic, strong) NSString* storeName;
@property (nonatomic, strong) NSString* storeId;
@property (nonatomic, assign) int Id;
@property (nonatomic, assign) int status;
//@property (nonatomic, assign) int storeId;
@property (nonatomic, assign) int type;
@property (nonatomic, assign) double sale;

@end



//
//  SelectCategoryEntity.m
//  Core
//
//  Created by Inglab on 13/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "SelectCategoryEntity.h"

@implementation SelectCategoryEntity

- (id)initWithName:(NSString*)name id:(int)Id {
    
    self = [super init];
    self.Name = name;
    self.Id = Id;
    self.isSelected = false;
    return self;
}

@end

//
//  SaleEntity.m
//  Core
//
//  Created by Inglab on 13/02/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "SaleEntity.h"
#import "Helpers.h"

@implementation SaleEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.storeId = @"";
    self.startTime = @"";
    self.endTime = @"";
    self.storeName = @"";
    
    self.Id = 0;
    self.type = 0;
    self.status = 0;
    self.sale = 0;
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.storeId =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_StoreId]];
        self.startTime =  [CoreStringUtils getStringDiffNull:[map objectForKey:@"startTime"]];
        self.endTime =  [CoreStringUtils getStringDiffNull:[map objectForKey:@"endTime"]];
        self.storeName = [CoreStringUtils getStringDiffNull:[map objectForKey:@"storeName"]];
        
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_ID]];
        self.type = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_TYPE]];
        self.status = [Helpers getUIntDiffNull:[map objectForKey:@"status"]];
        self.sale = [Helpers getUIntDiffNull:[map objectForKey:@"sale"]];
        
    }
    return self;
}

@end

//
//  WorkingDayEntity.m
//  Core
//
//  Created by Inglab on 24/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "WorkingDayEntity.h"
#import <objc/runtime.h>

@implementation WorkingDayEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.Day = @"";
    self.Start = @"";
    self.End = @"";
  
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        self.Day =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_Day]];
        self.Start =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_Start]];
        self.End = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_End]];
        
    }
    return self;
}

+ (NSDictionary *)dictionaryWithPropertiesOfObject:(id)obj {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        [dict setObject:[obj valueForKey:key] ? [obj valueForKey:key] : @"" forKey:key];
    }
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}


@end

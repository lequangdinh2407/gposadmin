//
//  WorkingDayEntity.h
//  Core
//
//  Created by Inglab on 24/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface WorkingDayEntity : BaseEntity

@property (nonatomic, strong) NSString* Day;
@property (nonatomic, strong) NSString* Start;
@property (nonatomic, strong) NSString* End;

+ (NSDictionary *)dictionaryWithPropertiesOfObject:(id)obj;


@end

NS_ASSUME_NONNULL_END

//
//  SaleReportsEntity.h
//  Core
//
//  Created by Inglab on 14/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"


@interface SaleReportsEntity : BaseEntity

@property (nonatomic) BOOL Cashback;
@property (nonatomic) int DiscountItemCount;
@property (nonatomic) int DiscountOrderCount;
@property (nonatomic) int DiscountValue;

@property (nonatomic, strong) NSString* End;
@property (nonatomic, strong) NSString* Start;

@property (nonatomic) double GrandTotal;
@property (nonatomic) int OrderCount;
@property (nonatomic) int ReturnItemCount;
@property (nonatomic) double ReturnSaleTax;
@property (nonatomic) double ReturnValue;
@property (nonatomic) double SaleTax;
@property (nonatomic) double Tips;

@property (nonatomic, strong) NSMutableArray* DaySaleReports;
@property (nonatomic, strong) NSMutableArray* SaleOrders;

@end



//
//  SaleOrderItemEntity.m
//  Core
//
//  Created by Inglab on 02/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "SaleOrderItemEntity.h"
#import "Helpers.h"

@implementation SaleOrderItemEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.Id = 0;
    self.Name = @"";
    self.ProviderName = @"";
    self.Quantity = 0;
    self.SaleTax = 0;
    self.UnitPrice = 0;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_Id]];
        self.Name =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.ProviderName =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ProviderName]];
        
        self.UnitPrice = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_UnitPrice]];
        self.Quantity = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_QUANTITY]];
        self.SaleTax = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_SaleTax]];
        
        
    }
    return self;
}

@end

//
//  CustomerEntity.h
//  Core
//
//  Created by Inglab on 21/09/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

@interface CustomerEntity : BaseEntity

@property (nonatomic) int Id;
@property (nonatomic, strong) NSString* JoinedDate;
@property (nonatomic, strong) NSString* FirstName;
@property (nonatomic, strong) NSString* LastName;
@property (nonatomic, strong) NSString* EmailAddress;
@property (nonatomic, strong) NSString* Phone;
@property (nonatomic, strong) NSString* Notes;
@property (nonatomic, strong) NSString* SerialNo;
@property (nonatomic, strong) NSString* BirthDay;
@property (nonatomic, strong) NSString* LastPurchase;

@property (nonatomic) BOOL ReceiveCoupon;
@property (nonatomic) double RewardPointRate;
@property (nonatomic) double RewardPoints;
@property (nonatomic) double TotalSales;

@property (nonatomic) int SalesCount;

@end

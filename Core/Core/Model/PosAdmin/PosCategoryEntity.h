//
//  PosCategoryEntity.h
//  Core
//
//  Created by Inglab on 20/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseEntity.h"

@interface PosCategoryEntity : BaseEntity

@property (nonatomic) BOOL Active;
@property (nonatomic) int Id;
@property (nonatomic) int DisplayOrder;
@property (nonatomic, strong) NSString* Name;
@property (nonatomic, strong) NSMutableArray* ListProducts;
@property (nonatomic, strong) NSMutableArray* Modifiers;

+ (NSDictionary *)dictionaryWithPropertiesOfObject:(id)obj;

@end

//
//  PaymentReportEntity.h
//  Core
//
//  Created by Inglab on 03/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface PaymentReportEntity : BaseEntity

@property (nonatomic, strong) NSString* PaymentMethod;
@property (nonatomic, strong) NSMutableArray* Payments;

@end

NS_ASSUME_NONNULL_END

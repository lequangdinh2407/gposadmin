//
//  ShortFormPosCategoryEntity.m
//  Core
//
//  Created by Inglab on 26/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "ShortFormPosCategoryEntity.h"
#import <objc/runtime.h>

@implementation ShortFormPosCategoryEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.Id = 0;
    self.Name = @"";
    
    return self;
}

+ (NSDictionary *)dictionaryWithPropertiesOfObject:(id)obj {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        [dict setObject:[obj valueForKey:key] ? [obj valueForKey:key] : @"" forKey:key];
    }
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}
@end

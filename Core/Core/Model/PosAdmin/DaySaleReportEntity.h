//
//  DaySaleReportEntity.h
//  Core
//
//  Created by Inglab on 01/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface DaySaleReportEntity : BaseEntity

@property (nonatomic) BOOL Cashback;

@property (nonatomic, strong) NSString* Date;

@property (nonatomic) int GrandTotal;
@property (nonatomic) int OrderCount;

@property (nonatomic) int SaleTax;
@property (nonatomic) double Tip;
@property (nonatomic) double SubTotal;

@end

NS_ASSUME_NONNULL_END

//
//  SelectCategoryEntity.h
//  Core
//
//  Created by Inglab on 13/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface SelectCategoryEntity : BaseEntity

@property (nonatomic, strong) NSString* Name;
@property (nonatomic, assign) int Id;
@property (nonatomic, assign) BOOL isSelected;

- (id)initWithName:(NSString*)name id:(int)Id;

@end

NS_ASSUME_NONNULL_END

//
//  ProviderEntity.m
//  Core
//
//  Created by Inglab on 14/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "ProviderEntity.h"
#import "Helpers.h"

@implementation ProviderEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.active = false;
    self.commissionRate = 0;
    self.Id = 0;
    self.IsTardy = false;
    self.JobCount = 0;
    self.commissionRate = 0;
    self.LastSignedInTime = @"";
    self.Name = @"";
    self.NotificationKey = @"";
    self.PassCode = @"";
    self.SaleAmount = 0;
    self.Phone = @"";
    self.SignedIn = false;
    self.WorkHours = 0;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.active = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_ACTIVE]];
        self.commissionRate = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_COMMISSION_RATE]];
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_Id]];
        self.IsTardy = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_IS_TARDY]];
        self.JobCount = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_JOB_COUNT]];
        
        self.commissionRate = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_COMMISSION_RATE]];
        
        self.LastSignedInTime =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_LAST_SIGNEDIN_TIME]];
        self.Name =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.NotificationKey = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NOTIFICATION_KEY]];
        self.PassCode = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PASSCODE]];
        self.Phone = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PHONE]];
        
        self.SaleAmount = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_SALE_AMOUNT]];
        self.SignedIn = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_SIGNED_IN]];
        self.WorkHours = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_WORK_HOURS]];
        
        self.WorkingDays = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_WorkingDays]];
        
        
    }
    return self;
}


@end

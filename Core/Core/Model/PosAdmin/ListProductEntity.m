//
//  ListProductEntity.m
//  Core
//
//  Created by Inglab on 21/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "ListProductEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation ListProductEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.Id = 0;
    self.Name = @"";
    self.DisplayOrder = 0;
    self.ImageUri = @"";
    self.Details = @"";
    self.Active = false;
    self.NonInventory = false;
    self.Customizable = false;
    self.IsGiftCard = false;
    //self.SKU = @"";
    self.Price = 0;
    self.SKU = 0;
    self.ListProductId = 0;
    self.SaleTaxGroupId = 0;
    self.MinQuantity = 0;
    self.MaxQuantity = 0;
    self.StandardCost = 0;
    self.SaleTaxRate = 0;
    self.QOH = 0;
    self.Duration = @"";
    self.ShowOnApp = false;
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        self.ListProductId = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_LIST_PRODUCT_ID]];
        self.SaleTaxGroupId = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_SALE_TAX_GROUP_ID]];
        self.MinQuantity = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_MIN_QUANTITY]];
        self.MaxQuantity = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_MAX_QUANTITY]];
        self.StandardCost = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_STANDARD_COST]];
        self.SaleTaxRate = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_STANDARD_COST]];
        self.QOH = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_QOH]];

        self.ShowOnApp = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_SHOW_ON_APP]];
        self.Duration = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DURATION]];
        //self.SKU =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_SKU]];
        self.SKU = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_SKU]];
        self.Price = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_PRICE]];
 
        
        self.Active = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_ACTIVE]];
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_Id]];
        self.DisplayOrder = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_DISPLAY_ORDER]];
        self.Name =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.ImageUri =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_IMAGE_URI]];
        self.Details =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DETAILS]];
        self.PrinterName =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PRINTER_NAME]];
        
        self.NonInventory = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_NON_INVENTORY]];
        self.Customizable = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_CUSTOMIZEABLE]];
        self.IsGiftCard = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_IS_GIFT_CARD]];
        
       
    }
    return self;
}

@end

//
//  SaleOrderEntity.h
//  Core
//
//  Created by Inglab on 22/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface SaleOrderEntity : BaseEntity

@property (nonatomic) int Id;
@property (nonatomic, strong) NSString* OrderNumber;
@property (nonatomic, strong) NSString* OrderDate;
@property (nonatomic, strong) NSString* CustomerPhone;
@property (nonatomic, strong) NSString* CustomerEmailAddress;
@property (nonatomic, strong) NSString* CustomerFirstName;
@property (nonatomic, strong) NSString* CustomerLastName;

@property (nonatomic, strong) NSString* Status;
@property (nonatomic, strong) NSString* CreatedBy;
@property (nonatomic, assign) double GrandTotal;
@property (nonatomic, assign) double SaleTax;
@property (nonatomic, assign) double Tips;
@property (nonatomic, assign) double BalanceDue;


@property (nonatomic, strong) NSMutableArray* SaleOrderItems;


@end

NS_ASSUME_NONNULL_END

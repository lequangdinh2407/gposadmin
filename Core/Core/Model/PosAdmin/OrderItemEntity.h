//
//  OrderItemEntity.h
//  Core
//
//  Created by Inglab on 08/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderItemEntity : BaseEntity


@property (nonatomic, assign) double GrandTotal;
@property (nonatomic, assign) double SubTotalBeforeTax;
@property (nonatomic, assign) double Discount;
@property (nonatomic, assign) double Tip;
@property (nonatomic, assign) double SaleTax;

@property (nonatomic, strong) NSString* CreatedOn;
@property (nonatomic, strong) NSString* Name;
@property (nonatomic, strong) NSString* OrderNumber;





@end

NS_ASSUME_NONNULL_END

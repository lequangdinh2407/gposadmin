//
//  PosCategoryEntity.m
//  Core
//
//  Created by Inglab on 20/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "PosCategoryEntity.h"
#import "Helpers.h"
#import "EntityManager.h"
#import <objc/runtime.h>

@implementation PosCategoryEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.Id = 0;
    self.Name = @"";
    self.DisplayOrder = 0;
    self.Active = false;
    
    self.ListProducts = [[NSMutableArray alloc] init];
    self.Modifiers = [[NSMutableArray alloc] init];
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.Active = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_ACTIVE]];
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_Id]];
        self.Name =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.DisplayOrder = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_DISPLAY_ORDER]];
        
        NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_LIST_PRODUCTS]];
        if (array) {
            for (int i = 0; i < array.count; i++) {
                ProductEntity *entity = (ProductEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:PRODUCT_ENTITY];
                [self.ListProducts addObject:entity];
            }
        }
        
        array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_MODIFIERS]];
        if (array) {
            for (int i = 0; i < array.count; i++) {
                ModifierEntity* entity = (ModifierEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:MODIFIER_ENTITY];
                [self.Modifiers addObject:entity];
            }
        }
    }
    return self;
}

+ (NSDictionary *)dictionaryWithPropertiesOfObject:(id)obj {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        [dict setObject:[obj valueForKey:key] ? [obj valueForKey:key] : @"" forKey:key];
    }
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

@end

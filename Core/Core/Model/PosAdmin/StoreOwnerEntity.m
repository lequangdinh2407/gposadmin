//
//  StoreOwnerEntity.m
//  Core
//
//  Created by Inglab on 05/01/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "StoreOwnerEntity.h"
#import "Helpers.h"

@implementation StoreOwnerEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    
    self.active = false;
    self.storeId = @"";
    self.firstName = @"";
    self.lastName = @"";
    self.phone = @"";
    self.email = @"";
    self.password = @"";
    self.Id = 0;
    self.storeIdsList = [[NSArray alloc] init];
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dictionary = (NSDictionary*)data;
        
        NSDictionary *map = (NSDictionary*)[dictionary objectForKey:@"storeOwner"];
        
        self.storeId =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_StoreId]];
        self.firstName =  [CoreStringUtils getStringDiffNull:[map objectForKey:@"firstName"]];
        self.lastName =  [CoreStringUtils getStringDiffNull:[map objectForKey:@"lastName"]];
        self.email = [CoreStringUtils getStringDiffNull:[map objectForKey:@"email"]];
        self.password = [CoreStringUtils getStringDiffNull:[map objectForKey:@"password"]];
        self.phone =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_phone]];
        self.active = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_aCTIVE]];
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_ID]];
        self.storeIdsList = [Helpers getArrayDiffNull:[map objectForKey:@"storeIds"]];
    }
    return self;
}

@end

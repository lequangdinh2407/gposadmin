//
//  SaleTaxGroupEntity.m
//  Core
//
//  Created by Inglab on 25/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "SaleTaxGroupEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation SaleTaxGroupEntity



- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.GroupName = @"";
    self.Id = 0;
    self.SalesTaxRate = 0;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_Id]];
        self.GroupName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_GROUP_NAME]];
        self.SalesTaxRate = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_SALES_TAX_RATE]];
    }
    return self;
}

@end

//
//  BussinessHourEntity.h
//  Core
//
//  Created by Inglab on 11/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface BussinessHourEntity : BaseEntity

@property (nonatomic, strong) NSString* day;
@property (nonatomic, strong) NSString* start;
@property (nonatomic, strong) NSString* end;
@property (nonatomic, strong) NSString* hashKey;
@property (nonatomic) int Object;
@property (nonatomic) BOOL isOpen;

@end

NS_ASSUME_NONNULL_END

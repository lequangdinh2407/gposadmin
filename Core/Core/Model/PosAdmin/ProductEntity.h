//
//  ProductEntity.h
//  Core
//
//  Created by Inglab on 21/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

@interface ProductEntity : BaseEntity

@property (nonatomic) int Id;
@property (nonatomic, strong) NSString* Name;
@property (nonatomic) int DisplayOrder;
@property (nonatomic, strong) NSString* ImageUri;
@property (nonatomic, strong) NSString* Details;
@property (nonatomic) BOOL Active;
@property (nonatomic) BOOL NonInventory;
@property (nonatomic) BOOL Customizable;
@property (nonatomic) BOOL IsGiftCard;
@property (nonatomic) double Price;
@property (nonatomic, strong) NSString* PrinterName;
@property (nonatomic, strong) NSMutableArray* Products;
@property (nonatomic, strong) NSMutableArray* Categories;

@end


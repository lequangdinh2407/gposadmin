//
//  PaymentReportEntity.m
//  Core
//
//  Created by Inglab on 03/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "PaymentReportEntity.h"
#import "EntityManager.h"
#import "Helpers.h"

@implementation PaymentReportEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.PaymentMethod = @"";
    self.Payments = [[NSMutableArray alloc] init];
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        self.PaymentMethod = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PaymentMethod]];
        
        NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_Payments]];
        if (array) {
            for (int i = 0; i < array.count; i++) {
                PaymentEntity *entity = (PaymentEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:PAYMENT_ENTITY];
                [self.Payments addObject:entity];
            }
        }
        
    }
    return self;
}


@end

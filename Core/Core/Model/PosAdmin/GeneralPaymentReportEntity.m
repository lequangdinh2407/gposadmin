//
//  GeneralPaymentReportEntity.m
//  Core
//
//  Created by Inglab on 03/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GeneralPaymentReportEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation GeneralPaymentReportEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
   
    self.PaymentAmount = 0;
    self.PaymentReports = [[NSMutableArray alloc] init];
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        self.PaymentAmount = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_PaymentAmount]];
        
        NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_PaymentReports]];
        if (array) {
            for (int i = 0; i < array.count; i++) {
                PaymentReportEntity *entity = (PaymentReportEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:PAYMENT_REPORT_ENTITY];
                [self.PaymentReports addObject:entity];
            }
        }
        
    }
    return self;
}

@end

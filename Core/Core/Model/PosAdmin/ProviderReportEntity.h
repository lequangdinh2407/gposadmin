//
//  ProviderReportEntity.h
//  Core
//
//  Created by Inglab on 08/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProviderReportEntity : BaseEntity

@property (nonatomic, assign) double CashCollected;
@property (nonatomic, assign) double StandardCost;
@property (nonatomic, assign) double Sales;
@property (nonatomic, assign) double Tips;
@property (nonatomic, assign) double TaxableCashPaymentRate;
@property (nonatomic, assign) double TotalWorkHours;

@property (nonatomic, assign) int ProviderId;
@property (nonatomic, strong) NSString* ProviderName;

@property (nonatomic, assign) double Commission;

@property (nonatomic, strong) NSMutableArray* OrderItems;

@end

NS_ASSUME_NONNULL_END

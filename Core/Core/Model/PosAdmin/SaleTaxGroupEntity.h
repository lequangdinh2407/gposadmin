//
//  SaleTaxGroupEntity.h
//  Core
//
//  Created by Inglab on 25/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface SaleTaxGroupEntity : BaseEntity

@property (nonatomic, strong) NSString* GroupName;
@property (nonatomic) double SalesTaxRate;
@property (nonatomic) int Id;

@end

NS_ASSUME_NONNULL_END

//
//  GlobalStoreEntity.m
//  Core
//
//  Created by Inglab on 27/12/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GlobalStoreEntity.h"
#import "Helpers.h"

@implementation GlobalStoreEntity
- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    
    self.active = false;
    self.device = @"";
    self.shopCode = @"";
    self.storeId = @"";
    self.user = @"";
    self.password = @"";
    self.storeId = @"";
    self.name = @"";
    self.coordinateLat = 0;
    self.coordinateLon = 0;
    self.Id = 0;
    self.isSelected = false;
    
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        self.storeId =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_StoreId]];
        self.shopCode =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_SHOPCODE]];
        self.device =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DEVICE]];
        self.user =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_USER]];
        self.phone =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_phone]];
        self.password =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_password]];
        self.storeId =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_StoreId]];
        self.name =  [CoreStringUtils getStringDiffNull:[map objectForKey:@"name"]];
        
        self.active = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_aCTIVE]];
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_ID]];
        
        NSDictionary* coordinate = [Helpers getDictDiffNull:[map objectForKey:@"coordinate"]];
        self.coordinateLat = [Helpers getDoubleDiffNull:[coordinate objectForKey:@"lat"]];
        self.coordinateLon = [Helpers getDoubleDiffNull:[coordinate objectForKey:@"lon"]];
        
    }
    return self;
}

@end

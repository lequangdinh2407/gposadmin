//
//  PaymentEntity.m
//  Core
//
//  Created by Inglab on 03/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "PaymentEntity.h"
#import "Helpers.h"

@implementation PaymentEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.PaymentId = 0;
    self.OrderId = 0;
    self.PaymentMethod = @"";
    self.Amount = 0;
    
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        self.PaymentId = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_PaymentId]];
        self.Amount = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_AMOUNT]];
        self.OrderId = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_ORDER_ID]];
        self.PaymentMethod =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PaymentMethod]];
        
    }
    return self;
}

@end

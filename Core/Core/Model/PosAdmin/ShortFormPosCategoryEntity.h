//
//  ShortFormPosCategoryEntity.h
//  Core
//
//  Created by Inglab on 26/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShortFormPosCategoryEntity : BaseEntity


@property (assign,nonatomic) int Id;
@property (nonatomic, strong) NSString* Name;


+ (NSDictionary *)dictionaryWithPropertiesOfObject:(id)obj;

@end

NS_ASSUME_NONNULL_END

//
//  DeviceUserEntity.m
//  Core
//
//  Created by Inglab on 21/09/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "DeviceUserEntity.h"
#import "Helpers.h"

@implementation DeviceUserEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
  
    self.Id = 0;
  
    self.UserName = @"";
    self.PassCode = @"";
    self.Password = @"";
    self.Email = @"";
    self.isCashier = @"False";
    self.isManager = @"False";
    self.isController = @"False";
    self.isSupervisor = @"False";
    
    self.Roles = [[NSMutableArray alloc] init];
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
       
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_Id]];
     
       
        self.PassCode = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PASSCODE]];
        self.Password = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PASSWORD]];
        self.UserName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_USER_NAME]];
        self.Email = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_Email]];
        
        
        NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_ROLES]];
        if (array) {
            
//            Cashier,
//            Supervisor,
//            Controller,
//            Manager
            
            for (int i = 0; i < array.count; i++) {
                if ([array[i] isEqualToString:@"Cashier"]) {
                    self.isCashier = @"True";
                }
                
                if ([array[i] isEqualToString:@"Supervisor"]) {
                    self.isSupervisor = @"True";
                }
                
                if ([array[i] isEqualToString:@"Controller"]) {
                    self.isController = @"True";
                }
                
                if ([array[i] isEqualToString:@"Manager"]) {
                    self.isManager = @"True";
                }
            }
        }
        
    }
    return self;
}


@end

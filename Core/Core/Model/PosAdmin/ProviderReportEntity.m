//
//  ProviderReportEntity.m
//  Core
//
//  Created by Inglab on 08/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "ProviderReportEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation ProviderReportEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.CashCollected = 0;
    self.StandardCost = 0;
    self.Sales = 0;
    self.Tips = 0;
    self.TotalWorkHours = 0;
    self.TaxableCashPaymentRate = 0;
    self.Commission = 0;
    self.ProviderName = @"";
    self.OrderItems = [[NSMutableArray alloc] init];
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.ProviderName =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ProviderName]];
       self.CashCollected = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_CashCollected]];
        self.StandardCost = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_StandardCost]];
        self.Sales = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_Sales]];
        self.Tips = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_Tips]];
        self.TotalWorkHours = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_TotalWorkHours]];
        self.TaxableCashPaymentRate = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_TaxableCashPaymentRate]];
        self.Commission = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_Commission]];
        self.ProviderId = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_PROVIDER_ID]];
        NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_OrderItems]];
        if (array) {
            for (int i = 0; i < array.count; i++) {
                OrderItemEntity *entity = (OrderItemEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:ORDER_ITEM_ENTITY];
                [self.OrderItems addObject:entity];
            }
        }
    }
    return self;
}

@end

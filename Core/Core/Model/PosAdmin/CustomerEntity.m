//
//  CustomerEntity.m
//  Core
//
//  Created by Inglab on 21/09/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "CustomerEntity.h"
#import "Helpers.h"

@implementation CustomerEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.Id = 0;
    
    self.JoinedDate = @"";
    self.FirstName = @"";
    self.LastName = @"";
    self.EmailAddress = @"";
    self.Phone = @"";
    self.Notes = @"";
    self.SerialNo = @"";
    self.BirthDay = @"";
    self.LastPurchase = @"";
    
    self.SalesCount = 0;
    
    self.ReceiveCoupon = false;
    self.RewardPoints = 0;
    self.RewardPointRate = 0;
    self.TotalSales = 0;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_Id]];
        
        self.JoinedDate = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_JOINED_DATE]];
        self.FirstName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_FIRST_NAME]];
        self.LastName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_LAST_NAME]];
        self.EmailAddress = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_EMAIL_ADDRESS]];
        self.Phone = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PHONE]];
        self.Notes = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NOTES]];
        
        self.SerialNo = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_SERIAL_NO]];
        self.BirthDay = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_BIRTHDAY]];
        self.LastPurchase = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_LAST_PURCHASE]];
        
        self.SalesCount = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_SALES_COUNT]];
        self.ReceiveCoupon = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_RECEIVE_COUPON]];
        
        self.RewardPointRate = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_RECEIVE_COUPON]];
        self.RewardPoints = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_REWARD_POINTS]];
        self.TotalSales = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_TOTAL_SALES]];
        
    }
    return self;
}

@end

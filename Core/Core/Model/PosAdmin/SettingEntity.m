//
//  SettingEntity.m
//  Core
//
//  Created by Inglab on 05/08/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "SettingEntity.h"
#import "Helpers.h"
#import "EntityManager.h"
#import <objc/runtime.h>

@implementation SettingEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    //self.Id = 0;
    self.name = @"";
    self.value = @"";
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        //self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_Id]];
        self.name =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        
        self.value =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_VALUE]];
        //self.Value = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_VALUE]];
        
    }
    return self;
}

+ (NSDictionary *)dictionaryWithPropertiesOfObject:(id)obj {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        [dict setObject:[obj valueForKey:key] ? [obj valueForKey:key] : @"" forKey:key];
    }
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

@end

//
//  SaleOrderItemEntity.h
//  Core
//
//  Created by Inglab on 02/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface SaleOrderItemEntity : BaseEntity

@property (nonatomic) int Id;
@property (nonatomic, strong) NSString* ProviderName;
@property (nonatomic, strong) NSString* Name;
@property (nonatomic, assign) double SaleTax;
@property (nonatomic, assign) double UnitPrice;
@property (nonatomic, assign) int Quantity;
@end

NS_ASSUME_NONNULL_END

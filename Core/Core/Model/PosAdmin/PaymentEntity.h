//
//  PaymentEntity.h
//  Core
//
//  Created by Inglab on 03/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface PaymentEntity : BaseEntity

@property (nonatomic, strong) NSString* PaymentMethod;
@property (nonatomic, assign) int OrderId;
@property (nonatomic, assign) int PaymentId;
@property (nonatomic, assign) double Amount;

@end

NS_ASSUME_NONNULL_END

//
//  OrderItemEntity.m
//  Core
//
//  Created by Inglab on 08/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "OrderItemEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation OrderItemEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.Name = @"";
    self.OrderNumber = @"";
    self.CreatedOn = @"";
    self.GrandTotal = 0;
    
    self.SubTotalBeforeTax = 0;
    self.SaleTax = 0;
    self.Tip = 0;
    self.Discount = 0;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        self.SaleTax = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_SaleTax]];
        self.Tip = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_Tip]];
        self.Discount = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_Discount]];
        self.GrandTotal = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_GrandTotal]];
        self.SubTotalBeforeTax = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_SubTotalBeforeTax]];
        self.Name =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.OrderNumber =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_OrderNumber]];
        self.CreatedOn =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CreatedOn]];
        
        
        
    }
    return self;
}

@end

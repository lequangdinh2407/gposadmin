//
//  DaySaleReportEntity.m
//  Core
//
//  Created by Inglab on 01/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "DaySaleReportEntity.h"
#import "Helpers.h"
@implementation DaySaleReportEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.Date = @"";
    
    self.GrandTotal = 0;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
       
        self.Date =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DATE]];
        self.Date = [self.Date substringToIndex:10];
        self.GrandTotal = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_Grandtotal]];
        
        
        
    }
    return self;
}


@end

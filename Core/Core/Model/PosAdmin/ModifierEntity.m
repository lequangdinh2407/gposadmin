//
//  ModifierEntity.m
//  Core
//
//  Created by Inglab on 21/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "ModifierEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation ModifierEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.Id = 0;
    self.Name = @"";
    self.Note = @"";
    self.Price = 0;
    self.Categories = [[NSMutableArray alloc] init];
    self.isSelected = false;
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
    
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_Id]];
        self.Name =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.Note =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NOTE]];
        self.Price = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_PRICE]];
        self.isSelected = false;
        
        if ([map objectForKey:ENTITY_CATEGORIES] != nil) {
            NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_PRODUCTS]];
            array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_CATEGORIES]];
            if (array) {
                for (int i = 0; i < array.count; i++) {
                    PosCategoryEntity *entity = (PosCategoryEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:CATEGORY_ENTITY];
                    [self.Categories addObject:entity];
                }
            }
        }
    }
    return self;
}

@end


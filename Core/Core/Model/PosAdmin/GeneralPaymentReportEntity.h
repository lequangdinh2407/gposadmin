//
//  GeneralPaymentReportEntity.h
//  Core
//
//  Created by Inglab on 03/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface GeneralPaymentReportEntity : BaseEntity

@property (nonatomic, assign) double PaymentAmount;
@property (nonatomic, strong) NSMutableArray* PaymentReports;

@end

NS_ASSUME_NONNULL_END

//
//  ProviderEntity.h
//  Core
//
//  Created by Inglab on 14/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

@interface ProviderEntity : BaseEntity

@property (nonatomic) BOOL active;
@property (nonatomic) int commissionRate;
@property (nonatomic) int Id;
@property (nonatomic) BOOL IsTardy;
@property (nonatomic) int JobCount;
@property (nonatomic, strong) NSString* LastSignedInTime;
@property (nonatomic, strong) NSString* Name;
@property (nonatomic, strong) NSString* NotificationKey;
@property (nonatomic, strong) NSString* PassCode;
@property (nonatomic, strong) NSString* Phone;
@property (nonatomic) int SaleAmount;
@property (nonatomic) BOOL SignedIn;
@property (nonatomic) int WorkHours;
@property (nonatomic, strong) NSString* WorkingDays;


@end


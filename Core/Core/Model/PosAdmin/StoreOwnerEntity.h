//
//  StoreOwnerEntity.h
//  Core
//
//  Created by Inglab on 05/01/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "BaseEntity.h"


@interface StoreOwnerEntity : BaseEntity

@property (nonatomic, strong) NSString* storeId;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* password;
@property (nonatomic, assign) int Id;
@property (nonatomic, assign) BOOL active;

@property (nonatomic, strong) NSArray* storeIdsList;

@end

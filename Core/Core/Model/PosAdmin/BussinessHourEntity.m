//
//  BussinessHourEntity.m
//  Core
//
//  Created by Inglab on 11/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BussinessHourEntity.h"
#import "Helpers.h"

@implementation BussinessHourEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.day = @"";
    self.start = @"";
    self.end = @"";
    self.hashKey = @"";
    self.Object = 0;
    self.isOpen = false;
    
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
       
        
        self.day =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_day]];
        self.start =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_start]];
        self.end = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_end]];
        self.hashKey = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_$$hashKey]];
        self.Object = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_object]];
        self.isOpen = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_isOpen]];
        
    }
    return self;
}


@end

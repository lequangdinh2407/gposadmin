//
//  StoreEntity.h
//  Core
//
//  Created by Inglab on 22/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface StoreEntity : BaseEntity

@property (nonatomic, strong) NSString* apiUrl;
@property (nonatomic, strong) NSString* imgUrl;
@property (nonatomic, strong) NSString* user;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* password;
@property (nonatomic, strong) NSString* storeId;
@property (nonatomic, strong) NSString* Id;

@end

NS_ASSUME_NONNULL_END

//
//  SaleOrderEntity.m
//  Core
//
//  Created by Inglab on 22/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "SaleOrderEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation SaleOrderEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.Id = 0;
    self.OrderNumber = @"";
    self.OrderDate = @"";
    self.CustomerPhone = @"";
    self.CustomerEmailAddress = @"";
    self.CustomerFirstName = @"";
    self.CustomerLastName = @"";
    self.SaleOrderItems = [[NSMutableArray alloc] init];
    self.BalanceDue = 0;
    self.GrandTotal = 0;
    self.Tips = 0;
    self.SaleTax = 0;
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_Id]];
        self.OrderNumber =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ORDER_NUMBER]];
        self.OrderDate =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ORDER_DATE]];
        self.CustomerPhone =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CUSTOMER_PHONE]];
        self.CustomerFirstName =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CUSTOMER_FIRST_NAME]];
        self.CustomerLastName =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CUSTOMER_LAST_NAME]];
        self.CustomerEmailAddress =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CUSTOMER_EMAIL_ADDRESS]];
        self.Status =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_STATUS]];
        self.CreatedBy =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CreatedBy]];
        self.GrandTotal = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_GrandTotal]];
        self.Tips = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_TIPS]];
        self.SaleTax = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_SaleTax]];
        self.BalanceDue = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_BalanceDue]];
        
        if ([map objectForKey:ENTITY_SALE_ORDER_ITEMS] != nil) {
            NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_SALE_ORDER_ITEMS]];
            if (array) {
                for (int i = 0; i < array.count; i++) {
                    SaleOrderItemEntity *entity = (SaleOrderItemEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:SALE_ORDER_ITEM_ENTITY];
                    [self.SaleOrderItems addObject:entity];
                }
            }
        }
        
    }
    return self;
}

@end

//
//  DeviceUserEntity.h
//  Core
//
//  Created by Inglab on 21/09/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

@interface DeviceUserEntity : BaseEntity


@property (nonatomic) int Id;
@property (nonatomic, strong) NSString* UserName;
@property (nonatomic, strong) NSString* Email;
@property (nonatomic, strong) NSString* PassCode;
@property (nonatomic, strong) NSString* Password;

@property (nonatomic, strong) NSString* isCashier;
@property (nonatomic, strong) NSString* isSupervisor;
@property (nonatomic, strong) NSString* isController;
@property (nonatomic, strong) NSString* isManager;

@property (nonatomic, strong) NSMutableArray* Roles;

@end



//
//  ListProductEntity.h
//  Core
//
//  Created by Inglab on 21/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ListProductEntity : BaseEntity

@property (nonatomic) int Id;
@property (nonatomic, strong) NSString* Name;
@property (nonatomic) int DisplayOrder;
@property (nonatomic, strong) NSString* ImageUri;
@property (nonatomic, strong) NSString* Details;
@property (nonatomic) BOOL Active;
@property (nonatomic) BOOL NonInventory;
@property (nonatomic) BOOL Customizable;
@property (nonatomic) BOOL IsGiftCard;
@property (nonatomic, strong) NSString* PrinterName;

//@property (nonatomic, strong) NSString* SKU;
@property (nonatomic, strong) NSString* Duration;

@property (nonatomic) int SKU;
@property (nonatomic) int ListProductId;
@property (nonatomic) int SaleTaxGroupId;
@property (nonatomic) int MinQuantity;
@property (nonatomic) int MaxQuantity;

@property (nonatomic) double Price;

@property (nonatomic) double StandardCost;
@property (nonatomic) double SaleTaxRate;
@property (nonatomic) double QOH;

@property (nonatomic) BOOL ShowOnApp;



@end

NS_ASSUME_NONNULL_END

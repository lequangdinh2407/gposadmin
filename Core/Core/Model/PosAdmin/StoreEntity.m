//
//  StoreEntity.m
//  Core
//
//  Created by Inglab on 22/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "StoreEntity.h"

@implementation StoreEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
   

    self.apiUrl = @"";
    self.imgUrl = @"";
    self.user = @"";
    self.password = @"";
    self.storeId = @"";
    self.name = @"";
    self.Id = @"";
    
    return self;
}

- (NSString*)getFieldName {
    return self.name;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.Id =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_ID]];
        self.apiUrl =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_API_URL]];
        self.imgUrl =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_IMG_URL]];
        self.user =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_USER]];
        self.password =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_password]];
        self.storeId =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_StoreId]];
        self.name =  [CoreStringUtils getStringDiffNull:[map objectForKey:@"name"]];
    }
    return self;
}


@end

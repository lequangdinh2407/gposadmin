//
//  ProductEntity.m
//  Core
//
//  Created by Inglab on 21/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "ProductEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation ProductEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.Id = 0;
    self.Name = @"";
    self.DisplayOrder = 0;
    self.ImageUri = @"";
    self.Details = @"";
    self.Price = 0;
    self.Active = false;
    self.NonInventory = false;
    self.Customizable = false;
    self.IsGiftCard = false;
    self.Products = [[NSMutableArray alloc] init];
    self.Categories = [[NSMutableArray alloc] init];
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        
        self.Active = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_ACTIVE]];
        self.Id = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_Id]];
        self.DisplayOrder = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_DISPLAY_ORDER]];
        self.Name =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_NAME]];
        self.ImageUri =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_IMAGE_URI]];
        self.Details =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_DETAILS]];
        self.PrinterName =  [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_PRINTER_NAME]];
        
        self.NonInventory = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_NON_INVENTORY]];
        self.Customizable = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_CUSTOMIZEABLE]];
        self.IsGiftCard = [Helpers getBoolDiffNull:[map objectForKey:ENTITY_IS_GIFT_CARD]];
       
        if ([map objectForKey:ENTITY_PRODUCTS] != nil) {
            NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_PRODUCTS]];
            if (array) {
                for (int i = 0; i < array.count; i++) {
                    ListProductEntity *entity = (ListProductEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:LIST_PRODUCT_ENTITY];
                    [self.Products addObject:entity];
                    if (entity.Price < self.Price || self.Price == 0) {
                        self.Price = entity.Price;
                    }
                }
            }
        }
        
        
        if ([map objectForKey:ENTITY_CATEGORIES] != nil) {
            NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_PRODUCTS]];
            array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_CATEGORIES]];
            if (array) {
                for (int i = 0; i < array.count; i++) {
                    PosCategoryEntity *entity = (PosCategoryEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:CATEGORY_ENTITY];
                    [self.Categories addObject:entity];
                }
            }
        }
    }
    return self;
}

@end

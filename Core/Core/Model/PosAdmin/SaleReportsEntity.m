//
//  SaleReportsEntity.m
//  Core
//
//  Created by Inglab on 14/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "SaleReportsEntity.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation SaleReportsEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
   
    self.DaySaleReports = [[NSMutableArray alloc] init];
    self.SaleOrders = [[NSMutableArray alloc] init];
    return self;
}

- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.DiscountItemCount = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_DISCOUNT_ITEM_COUNT]];
        self.DiscountOrderCount = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_DISCOUNT_ORDER_COUNT]];
        self.Start = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_START]];
        self.End = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_END]];
        
        self.GrandTotal = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_GrandTotal]];
        self.Tips = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_Tips]];
        self.ReturnSaleTax = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_RETURN_SALE_TAX]];
        self.SaleTax = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_SaleTax]];
        
        
        if ([map objectForKey:ENTITY_SALE_ORDERS] != nil) {
            NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_SALE_ORDERS]];
            if (array) {
                for (int i = 0; i < array.count; i++) {
                    SaleOrderEntity *entity = (SaleOrderEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:SALE_ORDER_ENTITY];
                    [self.SaleOrders addObject:entity];
                }
            }
        }
        
        if ([map objectForKey:ENTITY_DAY_SALE_REPORTS] != nil) {
            NSArray* array = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_DAY_SALE_REPORTS]];
            if (array) {
                for (int i = 0; i < array.count; i++) {
                    DaySaleReportEntity *entity = (DaySaleReportEntity*)[[EntityManager sharedClient] createItem:[array objectAtIndex:i] type:DAY_SALE_REPORT_ENTITY];
                    [self.DaySaleReports addObject:entity];
                }
            }
        }
    }
    return self;
}

@end

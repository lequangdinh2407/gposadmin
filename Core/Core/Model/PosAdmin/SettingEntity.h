//
//  SettingEntity.h
//  Core
//
//  Created by Inglab on 05/08/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface SettingEntity : BaseEntity

//@property (nonatomic) int Id;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* value;
//@property (nonatomic, assign) BOOL Value;

+ (NSDictionary *)dictionaryWithPropertiesOfObject:(id)obj;

@end

NS_ASSUME_NONNULL_END

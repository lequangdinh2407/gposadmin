//
//  ModifierEntity.h
//  Core
//
//  Created by Inglab on 21/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseEntity.h"


@interface ModifierEntity : BaseEntity

@property (nonatomic) int Id;
@property (nonatomic, strong) NSString* Name;
@property (nonatomic, strong) NSString* Note;
@property (nonatomic) double Price;
@property (nonatomic, strong) NSMutableArray* Categories;
@property (nonatomic, assign) BOOL isSelected;

@end


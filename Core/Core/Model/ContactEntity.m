//
//  ContactEntity.m
//  spabees
//
//  Created by Toan Pham Thanh on 1/5/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "ContactEntity.h"
#import "Helpers.h"

@implementation ContactEntity

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.contactID = @"";
    self.name = @"";
    self.phone = @"";
    contact = @"";
    
    return self;
}

- (NSString*)getDisplayName {
    if ([CoreStringUtils isEmpty:contact]) {
        contact = [NSString stringWithFormat:@"%@ %@", self.name, self.phone];
    }
    return contact;
}

- (NSString*)getFieldName {
    return [self getDisplayName];
}

@end

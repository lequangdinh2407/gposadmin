//
//  UserEntity.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"
#import "TSAddressEntity.h"
#import "CountryEntityV2.h"

@interface UserEntity : BaseEntity

@property (nonatomic, strong) NSString* userId;
@property (nonatomic) uint32_t uint_UserId;
@property (nonatomic, strong) NSString* avatar;
@property (nonatomic, strong) NSString* birthday;
@property (nonatomic, strong) NSString* cover;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* displayName;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) NSString* timeZone;
@property (nonatomic, assign) BOOL followed;
@property (nonatomic, assign) int profileType;
@property (nonatomic, strong) NSString* profileUserDisplayName;
@property (nonatomic, strong) NSString* countryCode;
@property (nonatomic, strong) NSString* nationalNumber;
@property (nonatomic, strong) CountryEntityV2* countryEntity;
@property (nonatomic, strong) TSAddressEntity* address;

@property (nonatomic, strong) NSString *hometown;
@property (nonatomic, strong) NSString *interestedIn;
@property (nonatomic, strong) NSString *slogan;

@property (nonatomic, strong) NSString *dateOB;
@property (nonatomic, strong) NSString *monthOB;

@property (nonatomic, strong) NSString* qr;
@property (nonatomic, strong) NSString* shopCode;
@property (nonatomic, assign) int rewardPoint;

@property (nonatomic, strong) NSMutableArray* storesList;


@property (nonatomic) int gender;

- (instancetype)init;

@end

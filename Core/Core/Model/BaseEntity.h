//
//  BaseEntity.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/31/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreConstants.h"
#import "CoreStringUtils.h"
#import "Realm/RLMObject.h"

@interface BaseEntity : NSObject

@property (nonatomic, strong) NSString* fieldName;
@property (nonatomic, assign) NSInteger priority;

- (NSObject*)initWithData:(NSObject*)data;
- (NSObject*)initWithRLMObject:(RLMObject*)rlmObj;
- (NSString*)getFieldName;
- (NSObject*)convertToData;

@end

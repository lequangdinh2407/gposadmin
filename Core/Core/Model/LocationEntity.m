//
//  LocationEntity.m
//  Core
//
//  Created by Dragon on 3/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "LocationEntity.h"
#import "Helpers.h"

@implementation LocationEntity
- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.longitude = 0.0;
    self.latitude = 0.0;
    self.city = @"";
    self.state = @"";
    self.timeLocalUpdated = [[NSDate date] timeIntervalSince1970];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:[NSNumber numberWithDouble:self.longitude] forKey:@"longitude"];
    [encoder encodeObject:[NSNumber numberWithDouble:self.latitude] forKey:@"latitude"];
    [encoder encodeObject:self.city forKey:@"city"];
    [encoder encodeObject:self.state forKey:@"state"];
    [encoder encodeObject:self.locationName forKey:@"locationName"];
    [encoder encodeObject:[NSNumber numberWithDouble:self.timeLocalUpdated] forKey:@"timeLocalUpdated"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.longitude = [[decoder decodeObjectForKey:@"longitude"] doubleValue];
        self.latitude = [[decoder decodeObjectForKey:@"latitude"] doubleValue];
        self.city = [decoder decodeObjectForKey:@"city"];
        self.state = [decoder decodeObjectForKey:@"state"];
        self.locationName = [decoder decodeObjectForKey:@"locationName"];
        self.timeLocalUpdated = [[decoder decodeObjectForKey:@"timeLocalUpdated"] doubleValue];
    }
    return self;
}

/*huydna*/
- (NSObject*)initWithData:(NSObject*)data{
    self = [self init];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSDictionary *map = (NSDictionary*)data;
        self.longitude = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_LONGITUDE]];
        self.latitude = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_LATITUDE]];
        self.state = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_STATE]];
        self.city = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CITY]];
        
        self.locationName = [NSString stringWithFormat:@"%@, %@", self.city, self.state];

        self.timeLocalUpdated = [Helpers getDoubleDiffNull:[map objectForKey:ENTITY_TIME_LOCAL_UPDATED]];
    }
    return self;
}
/*~huydna*/
@end

//
//  AddressEntity.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface AddressEntity : BaseEntity

@property (nonatomic, strong) NSString* zipCode;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* state;
@property (nonatomic, strong) NSString* country;
@property (nonatomic, strong) NSString* formatedAddress;
@property (nonatomic, assign) double lon;
@property (nonatomic, assign) double lat;

- (instancetype)init;

@end

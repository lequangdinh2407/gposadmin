//
//  LocationEntity.h
//  Core
//
//  Created by Dragon on 3/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface LocationEntity : BaseEntity

@property (assign, nonatomic) double longitude;
@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double timeLocalUpdated;
@property (strong, nonatomic) NSString* locationName;
@property (strong, nonatomic) NSString* city;
@property (strong, nonatomic) NSString* state;
@property (strong, nonatomic) NSString* country;
@property (strong, nonatomic) NSString* zipCode;

@property (strong, nonatomic) NSString* firstLine;
@property (strong, nonatomic) NSString* secondLine;

- (instancetype)init;

@end

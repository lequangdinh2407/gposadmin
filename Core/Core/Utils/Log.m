//
//  Log.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "Log.h"
#import "DateUtils.h"

static NSString *fileName;
static NSFileHandle *fileHandle;

@implementation Log

+ (void)initPath {
#ifdef DEBUG
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* time = [DateUtils getCurrentMoment];
    
    fileName = [NSString stringWithFormat:@"%@/fconnect_log_%@.txt",
                          documentsDirectory, time];
    [@"" writeToFile:fileName atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
    
    fileHandle = [NSFileHandle fileHandleForWritingAtPath:fileName];
#endif
}

+ (void)d:(NSInteger)level tag:(NSString*)tag message:(NSObject*)message {
#ifdef DEBUG
    NSLog(@"%@ %@", tag, message, nil);
    NSString* time = [DateUtils getCurrentMoment];
    NSString *content = [NSString stringWithFormat:@"%@ %@ %@\n", time, tag, message];
    [fileHandle writeData:[content dataUsingEncoding:NSUTF8StringEncoding]];
#endif
//    if (DEBUG == 1) {
//        NSLog(@"%@ %@", tag, message, nil);
//        NSString* time = [DateUtils getCurrentMoment];
//        NSString *content = [NSString stringWithFormat:@"%@ %@ %@\n", time, tag, message];
//        [fileHandle writeData:[content dataUsingEncoding:NSUTF8StringEncoding]];
//    }
}

@end

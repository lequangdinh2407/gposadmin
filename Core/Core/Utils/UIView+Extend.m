//
//  UIView+Extend.m
//  Core
//
//  Created by Dragon on 4/6/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "UIView+Extend.h"
#import "CoreConstants.h"
#import "SDWebImageDownloader.h"
#import "UIImageView+WebCache.h"

#define upperBorderTag 1023
#define leftBehideBorderTag 1034
#define rightBehideBorderTag 1045

#define GRADIENT_OPACITY 0.3
#define SHADOW_RADIUS 3.0


@implementation UIView (Extend)

-(void)addShadow{
    CALayer *shadowLayer = self.layer;
    shadowLayer.shadowOffset = CGSizeMake(1, 1);
    shadowLayer.shadowColor = [[UIColor blackColor] CGColor];
    shadowLayer.shadowRadius = SHADOW_RADIUS;
    shadowLayer.shadowOpacity = GRADIENT_OPACITY;
    shadowLayer.shadowPath = [[UIBezierPath bezierPathWithRoundedRect:self.layer.bounds cornerRadius:self.layer.bounds.size.height/2] CGPath];
    
}

-(void)addNonRoundedShadow{
    CALayer *shadowLayer = self.layer;
    shadowLayer.shadowOffset = CGSizeMake(1, 1);
    shadowLayer.shadowColor = [[UIColor blackColor] CGColor];
    shadowLayer.shadowRadius = SHADOW_RADIUS;
    shadowLayer.shadowOpacity = GRADIENT_OPACITY;
    shadowLayer.shadowPath = [[UIBezierPath bezierPathWithRect:self.layer.bounds ] CGPath];
    shadowLayer.masksToBounds = NO;
}

- (void)addBorderColor:(UIColor*)color withHeight:(float)height{
    
    [self addBorderColorNoBesides:color withHeight:height];
    
//    [[self viewWithTag:upperBorderTag] removeFromSuperview];
//    [[self viewWithTag:leftBehideBorderTag] removeFromSuperview];
//    [[self viewWithTag:rightBehideBorderTag] removeFromSuperview];
//    
//    float width = CGRectGetWidth(self.frame);
//    UIView *upperBorder = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame), width, height)];
//    upperBorder.backgroundColor = color;
//    upperBorder.tag = upperBorderTag;
//    [self addSubview:upperBorder];
//    
//    UIView *leftBehideBorder = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-5, height, 5)];
//    leftBehideBorder.backgroundColor = color;
//    leftBehideBorder.tag = leftBehideBorderTag;
//    [self addSubview:leftBehideBorder];
//    
//    UIView *rightBehideBorder = [[UIView alloc] initWithFrame:CGRectMake(width - height, CGRectGetHeight(self.frame)-5, height, 5)];
//    rightBehideBorder.backgroundColor = color;
//    rightBehideBorder.tag = rightBehideBorderTag;
//    [self addSubview:rightBehidewBorder];
}

- (void)addBorderColorNoBesides:(UIColor*)color withHeight:(float)height{
    [[self viewWithTag:upperBorderTag] removeFromSuperview];
    [[self viewWithTag:leftBehideBorderTag] removeFromSuperview];
    [[self viewWithTag:rightBehideBorderTag] removeFromSuperview];
    
    float width = CGRectGetWidth(self.frame);
    UIView *upperBorder = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame), width, height)];
    upperBorder.backgroundColor = color;
    upperBorder.tag = upperBorderTag;
    [self addSubview:upperBorder];
}

- (void)loadImage:(UIImageView*)imgV placeHolder:(UIImage*)placeHolder withUrl:(NSString*)link isScrolling:(BOOL)isScrolling {
    if([[UIScreen mainScreen] respondsToSelector:NSSelectorFromString(@"scale")])
    {
        if ([[UIScreen mainScreen] scale] < 1.1)
            [self loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:320 isScrolling:isScrolling];

        if ([[UIScreen mainScreen] scale] > 1.9)
            [self loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:480 isScrolling:isScrolling];
        return;
    }
    [self loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:320 isScrolling:isScrolling];
}

- (void)loadImage:(UIImageView*)imgV placeHolder:(UIImage*)placeHolder withUrl:(NSString*)link {
    if([[UIScreen mainScreen] respondsToSelector:NSSelectorFromString(@"scale")])
    {
        if ([[UIScreen mainScreen] scale] < 1.1)
            [self loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:320 isScrolling:FALSE];
        
        if ([[UIScreen mainScreen] scale] > 1.9)
            [self loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:480 isScrolling:FALSE];
        return;
    }
    [self loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:320 isScrolling:FALSE];
}

- (void)loadImage:(UIImageView*)imgV placeHolder:(UIImage*)placeHolder withUrl:(NSString*)link thumbSize:(int)size isScrolling:(BOOL)isScrolling {
    @try {
        NSString* thumbUrl = @"";
        if ([link rangeOfString:@"?"].location == NSNotFound) {
            thumbUrl = [NSString stringWithFormat:@"%@?size=%d", link, size];
        } else {
            thumbUrl = [NSString stringWithFormat:@"%@&size=%d", link, size];
        }
        UIImage *image = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:thumbUrl];
        if (image) {
            imgV.image = image;
        }
        
        if (isScrolling == TRUE) {
            imgV.image = placeHolder;
            return;
        }
        
        SEL selector = NSSelectorFromString(@"index");
        NSIndexPath* oldIndex = (NSIndexPath*)[self performSelector:selector];
        
        [[SDImageCache sharedImageCache] queryDiskCacheForKey:thumbUrl done:^(UIImage *image, SDImageCacheType cacheType) {
            NSIndexPath* currentIndex = (NSIndexPath*)[self performSelector:selector];
            
            if (image) {
                if (oldIndex.row == currentIndex.row && oldIndex.section == currentIndex.section)
                    imgV.image = image;
            } else {
                __weak UIImageView* tmpImage = imgV;
                
                imgV.image = placeHolder;
                
                [SDWebImageDownloader.sharedDownloader downloadImageWithURL:[NSURL URLWithString:thumbUrl] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize){
                    // progression tracking code
                } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished){
                    if (image && finished) {
                        NSIndexPath* currentIndex = (NSIndexPath*)[self performSelector:selector];
                        if (oldIndex.row == currentIndex.row && oldIndex.section == currentIndex.section) {
                            dispatch_async( dispatch_get_main_queue(), ^{
                                [UIView transitionWithView:tmpImage duration:0.5f options:UIViewAnimationOptionTransitionCrossDissolve
                                                animations:^{
                                                    tmpImage.image = image;
                                                } completion:nil];
                            });
                        }
                        [[SDImageCache sharedImageCache] storeImage:image forKey:thumbUrl toDisk:YES];
                    }
                }];
            }
        }];
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

@end

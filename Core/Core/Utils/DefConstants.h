//
//  CoreConstants.h
//  Core
//
//  Created by ToanPham on 2/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//


/**
 * Device Defs
 */
#pragma mark - Device Defs

#define IS_IPAD ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

/**
 * Size Defs
 */
#pragma mark - Size Defs

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define AVATAR_SIZE CGSizeMake(400, 400)
#define COVER_WIDTH (CGRectGetWidth([UIScreen mainScreen].bounds))
#define COVER_HEIGH (COVER_WIDTH*0.5)
#define COVER_SIZE CGSizeMake(COVER_WIDTH, COVER_HEIGH)

/**
 * Links Defs
 */
#pragma mark - Links Defs

#define CUSTOMER_SERVICE_PHONE  @"18883599397"
#define APP_DOWNLOAD_LINK @""
#define APP_PROVIDER_DOWNLOAD_LINK @"itms://itunes.apple.com/us/app/apple-store/id375380948?mt=8"
#define GOOGLE_CLIENT_ID    @"112490652344-7ammjln8drs5jbv0gann4ueqq912mfle.apps.googleusercontent.com"
#define TERM_LINK @"http://45.55.39.77/pos/load/terms/TermOfService.html"
#define FAQ_LINK @"https://file.homeberapp.com/homber/load/terms/FAQs.html"
#define PRIVACY_POLICY @"http://45.55.39.77/pos/load/terms/PrivacyPolicy.html"
#define DELIVER_POLICY @"http://115.79.45.86/ts/load/static/ts_DeliverPolicy.html"

#define REFRESH_BANNER_TIME 5.0
#define SWITCH_BANNER_DURATION 1.25

/**
 * Storyboard
 */
#pragma mark - Storyboard Defs

#define userProfileStoryboard [UIStoryboard storyboardWithName:@"UserProfileStoryboard" bundle:nil]
#define promotionStoryboard [UIStoryboard storyboardWithName:@"PromotionStoryboard" bundle:nil]
#define registerStoryboard [UIStoryboard storyboardWithName:@"RegisterStoryboard" bundle:nil]
#define moreStoryboard [UIStoryboard storyboardWithName:@"MoreStoryboard" bundle:nil]
#define mainStoryboard [UIStoryboard storyboardWithName:@"Main" bundle:nil]
#define radarStoryboard [UIStoryboard storyboardWithName:@"RadarStoryboard" bundle:nil]
#define rootView [[[UIApplication sharedApplication] keyWindow] rootViewController]
#define shoppingCartStoryboard [UIStoryboard storyboardWithName:@"ShoppingCartStoryboard" bundle:nil]
#define bconnectAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define feedStoryboard [UIStoryboard storyboardWithName:@"FeedStoryboard" bundle:nil]



/*
 * Keyboard
 */
#pragma mark - Keyboard Defs

#define kKeyboardItemAlign 4
#define kKeyboardItemNumberInRow (( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ? 8 : 4)

/*
 * Width, Height constant
 */
#pragma mark - Width, Height Defs

#define HEADER_HEIGHT   64
#define TUTORIAL_STEPS   4
#define FOOTER_HEIGHT   45
#define CHAT_AVATAR_COLLECTION_HEIGHT   30
#define HOMBER_ADDRESS_ROW_HEIGHT 60
#define kListContainerWidth 250
#define CATEGORY_HEADER_TABLE_HEIGHT 60
#define CATEGORY_HEADER_SECTION_HEIGHT 50
#define CATEGORY_ROW_HEIGHT 30


/*
 * Color
 */
#pragma mark - Color Defs

#define normal  APP_SUB_TEXT_COLOR
#define normalHeigh  0.75
#define forcus  APP_COLOR
#define forcusHeigh  1.25


#define NOTE_COLOR           [UIColor colorWithRed:218/255.0 green:196/255.0 blue:60/255.0 alpha:0.9]

#define kSearchBarMainViewColor [UIColor colorWithRed:200/255.0 green:228/255.0 blue:239/255.0 alpha:1.0]
#define kSearchBarColor [UIColor colorWithRed:136/255.0 green:153/255.0 blue:166/255.0 alpha:1.0]
#define kShopViewTabBarNormalColor APP_COLOR_50
#define kShopViewTabBarHighlightedColor APP_COLOR

#define kSearchMainViewColor  [UIColor colorWithRed:1/255.0 green:133/255.0 blue:182/255.0 alpha:1.0]
#define kSearchViewColor  [UIColor colorWithRed:225.0/255.0 green:232.0/255.0 blue:237.0/255.0 alpha:1.0]

#define kFilterEventColor           [UIColor colorWithRed:136/255.0 green:153/255.0 blue:166/255.0 alpha:1.0]
#define kTimeLeftColor           [UIColor colorWithRed:237/255.0 green:20/255.0 blue:91/255.0 alpha:1.0]
#define kEventStatusColor           APP_COLOR

#define gposGreen           [UIColor colorWithRed:76/255.0 green:219/255.0 blue:101/255.0 alpha:1.0]
#define gposOrange           [UIColor colorWithRed:253/255.0 green:183/255.0 blue:88/255.0 alpha:1.0]

#define kShadowConerRadius  0
#define kShadowOffsetWidth  -0.5
#define kShadowOffsetHeight  1.5
#define kShadowRadius  2
#define kShadowOpacity  0.1

#define kFilterEventRadius 12

#define kTitleColor [UIColor colorWithRed:48/255.0 green:49/255.0 blue:51/255.0 alpha:1.0]

#define NOTIFICATION_UNREAD_COLOR [UIColor colorWithRed:236/255.0 green:245/255.0 blue:254/255.0 alpha:1.0]
#define NOTIFICATION_READ_COLOR [UIColor whiteColor]
#define NOT_LIKE_COLOR [UIColor colorWithRed:136/255.0 green:153/255.0 blue:166/255.0 alpha:1.0]
#define LIKED_COLOR [UIColor colorWithRed:229/255.0 green:0/255.0 blue:73/255.0 alpha:1.0]

/*
 * Promotion
 */
#pragma mark - Promotion Defs

#define kPromotionInPageNumber 20
#define kFavoriteFooterHeight   30
#define kPromotionItemInRow (( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ? 3 : 2)
#define kTradeMarkItemInRow (( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ? 3 : 2)
#define kItemAlign 10
#define kPadding 10
#define kItemInfoBottomHeight 85
#define kShopPromotionInfoHeight 70
#define kItemWidth ((CGRectGetWidth([UIScreen mainScreen].bounds) - (kPromotionItemInRow-1)*kItemAlign - 2*kPadding)/kPromotionItemInRow)
#define kItemHeight (kItemWidth + 50)
#define kPromotionItemMax   (kPromotionItemInRow*2)
#define LOAD_PROMOTION_COUNT 20

#define kItemAlignAlasset 10
#define kPaddingAlasset 10

/*
 * Promotion of service
 */
#define kItemInRow (( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ? 6 : 3)
#define kPromoItemAlign 1
#define kPromoItemWidth ((CGRectGetWidth([UIScreen mainScreen].bounds) - (kItemInRow-1)*kPromoItemAlign) / kItemInRow)

/**
 * Table Name Database
 */
#pragma mark - Database Defs

#define TABLE_USER @"table_user"
#define TABLE_ACCOUNT @"table_account"
#define TABLE_SESSION @"table_session"
#define TABLE_ADDRESSES_BOOK @"table_addresses_book"
#define TABLE_FEED @"table_feed"

/**
 * Column Name Database
 */
#define COLUMN_USER_ID @"user_id"
#define COLUMN_FIRST_NAME @"first_name"
#define COLUMN_LAST_NAME @"last_ame"
#define COLUMN_DISPLAY_NAME @"display_name"
#define COLUMN_EMAIL @"email"
#define COLUMN_BIRTHDAY @"birthday"
#define COLUMN_ANNIVERSARY_DAY @"anniversary_day"
#define COLUMN_ADDRESS @"address"
#define COLUMN_CITY @"city"
#define COLUMN_STATE @"state"
#define COLUMN_COUNTRY @"country"
#define COLUMN_ZIPCODE @"zip_code"
#define COLUMN_COUNTRY_CODE @"country_code"
#define COLUMN_MOBILEPHONE @"mobile_phone"
#define COLUMN_STATUS @"status"
#define COLUMN_USER_NAME @"user_name"
#define COLUMN_USER_AVATAR @"user_avatar"
#define COLUMN_PASSWORD @"password"
#define COLUMN_SESSION_KEY @"session_key"
#define COLUMN_DOMAIN @"domain"
#define COLUMN_AVATAR @"avatar"
#define COLUMN_GENDER @"gender"
#define COLUMN_POINT @"point"
#define COLUMN_LICENSE_CODE @"license_code"

#define COLUMN_POSITION @"position"
#define COLUMN_TYPE_ID @"type_id"
#define COLUMN_TYPE_NAME @"type_name"
#define COLUMN_CATEGORY_ID @"category_id"
#define COLUMN_CATEGORY_NAME @"category_name"
#define COLUMN_SUBCATEGORY_ID @"subcategory_id"
#define COLUMN_SUBCATEGORY_NAME @"subcategory_name"

#define COLUMN_ADDRESS_ID @"address_id"
#define COLUMN_ADDRESSLINE1 @"addressLine1"
#define COLUMN_ADDRESSLINE2 @"addressLine2"
#define COLUMN_DEFAULT_BILLING_ADDRESS @"defaultBillingAddress"
#define COLUMN_DEFAULT_SHIPPING_ADDRESS @"defaultShippingAddress"

#define COLUMN_FEED_ID @"feed_id"
#define COLUMN_FEED_ITEM_ID @"feed_item_id"
#define COLUMN_STICKER @"sticker"
#define COLUMN_MESSAGE @"message"
#define COLUMN_PHOTOS @"photos"
#define COLUMN_FEED_LINK @"feed_link"
#define COLUMN_FEED_LINK_ID @"feed_link_id"
#define COLUMN_LINK @"link"
#define COLUMN_SITE @"site"
#define COLUMN_TITLE @"title"
#define COLUMN_DESC @"desc"
#define COLUMN_THUMBNAIL @"thumbnail"
#define COLUMN_TYPE @"type"
#define COLUMN_LIKES @"likes"
#define COLUMN_LOVES @"loves"
#define COLUMN_HAHAS @"hahas"
#define COLUMN_WOWS @"wows"
#define COLUMN_ANGRIES @"angries"
#define COLUMN_SADS @"sads"
#define COLUMN_COMMENTS @"comments"
#define COLUMN_SHARES @"shares"
#define COLUMN_CREATE_TIME @"create_time"
#define COLUMN_UPDATE_TIME @"update_time"
#define COLUMN_LINK_CREATE_TIME @"link_create_time"
#define COLUMN_LINK_UPDATE_TIME @"link_update_time"


#define ATTRIBUTE_PRIMARY_KEY @" TEXT NOT NULL PRIMARY KEY "
#define ATTRIBUTE_TEXT_NOT_NULL @" TEXT NOT NULL "
#define ATTRIBUTE_INTEGER_PRIMARY_KEY @" INTEGER PRIMARY KEY "
#define ATTRIBUTE_INTEGER @" INTEGER "

/**
 * Message Badge
 */
#define BADGE_NUMBER_MAX    5

/**
 * Search recents limited number
 */
#define SEARCH_RECENT_NUMBER_MAX    5

/**
 * Font
 */
#pragma mark - Font Defs

#define HEADER_TITLE_FONT [UIFont fontWithName:@"SanFranciscoDisplay-bold" size:16]
#define EventViewCellFont [UIFont fontWithName:@"SanFranciscoDisplay-Regular" size:16.0f]
#define BoothViewCellFont [UIFont fontWithName:@"SanFranciscoDisplay-Medium" size:14.0f]

/**
 * Text Color
 */
#pragma mark - Text Color Defs

#define TEXT_HOVER_COLOR    [UIColor colorWithRed:239/255.0 green:49/255.0 blue:117/255.0 alpha:1.0]
#define TEXT_NORMAL_COLOR   [UIColor whiteColor]
#define TEXT_VIEW_COLOR     [UIColor colorWithRed:239/255.0 green:49/255.0 blue:117/255.0 alpha:1.0]
#define TEXT_COLOR           [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]

#define BORDER_COLOR           [UIColor colorWithRed:255/255.0 green:102/255.0 blue:27/255.0 alpha:1.0]
#define BORDER_ITEM_COLOR           [UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1.0]

#define TextFieldBorderNormal  [UIColor colorWithRed:184/255.0 green:184/255.0 blue:179/255.0 alpha:1.0]
#define TextFieldBorderNormalHeigh  0.75
#define TextFieldBorderforcus  APP_COLOR
#define TextFieldBorderForcusHeigh  1.25

/**
 * App Color
 */
#pragma mark - App Color Defs

#define APP_COLOR           [UIColor colorWithRed:40/255.0 green:37/255.0 blue:44/255.0 alpha:1.0]
#define APP_SECONDARY_COLOR           [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define APP_SUB_TEXT_COLOR           [UIColor colorWithRed:136/255.0 green:153/255.0 blue:166/255.0 alpha:1.0]
#define APP_TEXT_COLOR           [UIColor colorWithRed:55/255.0 green:61/255.0 blue:59/255.0 alpha:1.0]
#define APP_BACKGROUND_COLOR [UIColor colorWithRed:40/255.0 green:37/255.0 blue:44/255.0 alpha:1.0]
//[UIColor colorWithRed:237/255.0 green:241/255.0 blue:246/255.0 alpha:1.0]

#define APP_COLOR_70           [UIColor colorWithRed:228/255.0 green:194/255.0 blue:92/255.0 alpha:0.7]
#define APP_COLOR_30           [UIColor colorWithRed:228/255.0 green:194/255.0 blue:92/255.0 alpha:0.3]
#define APP_COLOR_50           [UIColor colorWithRed:228/255.0 green:194/255.0 blue:92/255.0 alpha:0.5]
#define APP_STAR_COLOR           [UIColor colorWithRed:255/255.0 green:205/255.0 blue:0/255.0 alpha:1.0]
#define APP_STAR_BORDER_COLOR           [UIColor colorWithRed:208/255.0 green:221/255.0 blue:225/255.0 alpha:1.0]

#define ZOOM_LEVEL_DEFAULT 18
#define ZOOM_LEVEL_HOUSE 20
#define LAT_DEFAULT 28.3825166
#define LON_DEFAULT -81.3560895


#define INSTAGRAM_AUTHURL @"https://api.instagram.com/oauth/authorize/"
#define INSTAGRAM_APIURl  @"https://api.instagram.com/v1/users/"
#define INSTAGRAM_CLIENT_ID @"0c3884e4130b4a3fb6424e4de00abe65"
#define INSTAGRAM_CLIENTSERCRET @"38ae23a83d8e43bb94363176cce7a42a"
#define INSTAGRAM_REDIRECT_URL  @"http://45.55.39.77/pos/register/instagram"
#define INSTAGRAM_ACCESS_TOKEN  @"access_token"
#define INSTAGRAM_SCOPE         @"basic"

//Contant Url
#define ACCESS_TOKEN    @"#access_token="
#define UNSIGNED        @"UNSIGNED"
#define CODE            @"code="
#define HTTP_METHOD     @"POST"
#define CONTENT_LENGTH  @"Content-Length"
#define REQUEST_DATA    @"application/x-www-form-urlencoded"
#define CONTENT_TYPE    @"Content-Type"

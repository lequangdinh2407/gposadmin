//
//  DateUtils.m
//  spabees
//
//  Created by vu van long on 1/5/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import "DateUtils.h"

@implementation DateUtils

+ (NSUInteger)getTotalDayOfMonth:(int)month{
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* comps = [[NSDateComponents alloc] init];
    
    // Set your month here
    [comps setMonth:month];
    NSRange range = [cal rangeOfUnit:NSCalendarUnitDay
                              inUnit:NSCalendarUnitMonth
                             forDate:[cal dateFromComponents:comps]];
    
    return range.length + (month == 2 ? 1 : 0);// month 2 has 29 days
}

+ (NSString*)getCurrentDate {
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSString *currentTime = [dateFormatter stringFromDate:today];
    return currentTime;
}

+ (NSString*)getCurrentMoment {
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd_HH:mm:ss"];
    NSString *currentTime = [dateFormatter stringFromDate:today];
    return currentTime;
}

+ (double)getCurrentTimestamp {
    return [[NSDate date] timeIntervalSince1970] * 1000;
}

+ (int)getCurrentYear{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy"];
    NSDate *currentDate = [NSDate date];
    NSString *year = [dateFormat stringFromDate:currentDate];
    
    return [year intValue];
}

+ (NSString*)parseTimeStampToDate:(id)data {
    double timestamp = 0;
    @try {
        timestamp = [data doubleValue];
    }
    @catch (NSException *exception) {
        
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(timestamp / 1000)];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm aa MM/dd/yyyy"];
    NSString *dateString = [formatter stringFromDate:date];
    return dateString;
}

+ (NSString*)parseTimeStampToOnlyDate:(id)data {
    double timestamp = 0;
    @try {
        timestamp = [data doubleValue];
    }
    @catch (NSException *exception) {
        
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(timestamp / 1000)];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *dateString = [formatter stringFromDate:date];
    return dateString;
}

+ (NSString*)parseTimeStampToHour:(id)data {
    double timestamp = 0;
    @try {
        timestamp = [data doubleValue];
    }
    @catch (NSException *exception) {
        
    }
    
//    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(timestamp / 1000)];
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"hh:mm"];
//    NSString *dateString = [formatter stringFromDate:date];
//    return dateString;
    
    NSInteger ti = (NSInteger)(timestamp / 1000);
//    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)hours, (long)minutes];
}


+ (NSString*)convertDateToTimestamp:(NSString*)strDate {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *date = [dateFormatter dateFromString:strDate];
    NSLog(@"date=%@",date) ;
    NSTimeInterval interval  = [date timeIntervalSince1970] * 1000 ;
    NSLog(@"interval=%f",interval) ;
    //    NSDate *methodStart = [NSDate dateWithTimeIntervalSince1970:interval] ;
    //    [dateFormatter setDateFormat:@"yyyy/MM/dd "] ;
    //    NSLog(@"result: %@", [dateFormatter stringFromDate:methodStart]) ;
    
    return [NSString stringWithFormat:@"%@", @(interval)];
}

+ (NSString*)getNotificationTime:(double)data{
    
    return [DateUtils getHistoryMessageTime:data];
}

+ (NSString*)getHistoryMessageTime:(double)data{
    
    NSString* strTime;
    NSDate* tmpDate = [NSDate dateWithTimeIntervalSince1970:data];
    NSDateComponents *tmpComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:tmpDate];
    NSInteger tmpYear = tmpComponents.year;
    
    NSDateComponents *currentComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger currentYear = currentComponents.year;
    
    double deltaSeconds = fabs([tmpDate timeIntervalSinceDate:[NSDate date]]);
    double deltaMinutes = deltaSeconds/ 60.0f;
    int deltaHours = deltaMinutes/ 60;
    if (deltaSeconds < 60 && tmpComponents.day == currentComponents.day)
    {
        if (deltaSeconds < 5) {
            strTime = @"Just now";
        } else
            strTime = [NSString stringWithFormat:@"%0.f %@", deltaSeconds, @"secs ago"];
    }
    else if (deltaMinutes < 60 && tmpComponents.day == currentComponents.day)
    {
        int minute = (int)deltaMinutes;
        strTime = [NSString stringWithFormat:@"%d %@", minute, (minute <= 1) ? @"min ago" : @"mins ago"];
    }
    else if (deltaHours < 24 && tmpComponents.day == currentComponents.day)
    {
        int hour = (int)deltaHours;
        strTime = [NSString stringWithFormat:@"%d %@", hour, (hour <= 1) ? @"hr ago" : @"hrs ago"];
    }
    else if (deltaHours < (24 * 2) && tmpComponents.day == currentComponents.day - 1)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];

        [dateFormatter setDateFormat:@"hh:mm a"];
        NSString *strTime1 = [dateFormatter stringFromDate:tmpDate];
        
        strTime = [NSString stringWithFormat:@"%@ at %@", @"Yesterday", strTime1];
    }
    else if (currentYear == tmpYear)
    {
        NSDateFormatter *monthFormatter = [[NSDateFormatter alloc] init];
        monthFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];

        [monthFormatter setDateFormat:@"MMM dd"];
        NSString* strMonth = [monthFormatter stringFromDate:tmpDate];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];

        [dateFormatter setDateFormat:@"hh:mm a"];
        NSString* strDate = [dateFormatter stringFromDate:tmpDate];
        
        strTime = [NSString stringWithFormat:@"%@ at %@", strMonth, strDate];
    }
    else
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];

        [dateFormatter setDateFormat:@"MM/DD/YYYY"];
        strTime = [dateFormatter stringFromDate:tmpDate];
    }
    return strTime;
}

+ (NSString*)getMessageTime:(double)data{
    
    NSString* strTime;
    NSDate* tmpDate = [NSDate dateWithTimeIntervalSince1970:data];
    NSDateComponents *tmpComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:tmpDate];
    NSInteger tmpYear = tmpComponents.year;
    
    NSDateComponents *currentComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger currentYear = currentComponents.year;
    
    double deltaSeconds = fabs([tmpDate timeIntervalSinceDate:[NSDate date]]);
    double deltaMinutes = deltaSeconds/ 60.0f;
    if (deltaMinutes < (24 * 60))
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];

        [dateFormatter setDateFormat:@"hh:mm a"];
        strTime = [dateFormatter stringFromDate:tmpDate];
    }
    else if (currentYear == tmpYear)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];

        [dateFormatter setDateFormat:@"MM/dd hh:mm a"];
        strTime = [dateFormatter stringFromDate:tmpDate];
    }
    else
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];

        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        strTime = [dateFormatter stringFromDate:tmpDate];
    }
    return strTime;
}

+ (NSDate*)getDateFromString:(NSString *)string format:(NSString *)format{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:format];
    NSDate *date = [formatter dateFromString:string];
    return date;
}

+ (NSUInteger)daysBetween:(NSDate *)date1 andDate:(NSDate*)date2
{
    NSDate *dt1 = [DateUtils dateWithoutTimeComponents:date1];
    NSDate *dt2 = [DateUtils dateWithoutTimeComponents:date2];
    return ABS([dt1 timeIntervalSinceDate:dt2] / SecondsPerDay);
}

+ (NSComparisonResult)timelessCompare:(NSDate *)date1 andDate:(NSDate*)date2
{
    NSDate *dt1 = [DateUtils dateWithoutTimeComponents:date1];
    NSDate *dt2 = [DateUtils dateWithoutTimeComponents:date2];
    return [dt1 compare:dt2];
}

+ (NSDate *)dateWithoutTimeComponents:(NSDate*)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit  |
                                    NSMonthCalendarUnit |
                                    NSDayCalendarUnit
                                               fromDate:date];
    return [calendar dateFromComponents:components];
}

+ (NSString*)getStringDate:(NSDate*)date withFormat:(NSString*)format{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:format];
    NSString *currentTime = [dateFormatter stringFromDate:date];
    return currentTime;
}

+ (NSString*)getStringStartTime:(id)startTime endTime:(id)endTime {
    double startTimestamp = 0;
    double endTimestamp = 0;
    @try {
        startTimestamp = [startTime doubleValue];
        endTimestamp = [endTime doubleValue];
    }
    @catch (NSException *exception) {
        
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm aa LLL dd"];
    
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:(startTimestamp / 1000)];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:(endTimestamp / 1000)];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* componentsStartDay = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:startDate]; // Get necessary date components
    
    NSDateComponents* componentsEndDay = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:endDate]; // Get necessary date components
    
    NSString *dateString = @"";
    if ([componentsStartDay day] != [componentsEndDay day]) {
        [formatter setDateFormat:@"hh:mm aa"];
        NSDateFormatter *monthFormatter = [[NSDateFormatter alloc] init];
        [monthFormatter setDateFormat:@"LLL dd"];
        dateString = [NSString stringWithFormat:@"%@, %@ - %@, %@", [monthFormatter stringFromDate:startDate], [formatter stringFromDate:startDate], [monthFormatter stringFromDate:endDate], [formatter stringFromDate:endDate]];
    } else {
        [formatter setDateFormat:@"hh:mm aa"];
        NSDateFormatter *monthFormatter = [[NSDateFormatter alloc] init];
        [monthFormatter setDateFormat:@"LLL dd"];
        dateString = [NSString stringWithFormat:@"%@, %@ - %@ ", [monthFormatter stringFromDate:startDate], [formatter stringFromDate:startDate], [formatter stringFromDate:endDate]];
    }
    dateString = [dateString stringByReplacingOccurrencesOfString:@"00:00 AM" withString:@"12:00 AM"];
    return dateString;
}

@end

//
//  UIButton+extend.h
//  Core
//
//  Created by Dragon on 4/20/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (extend)
- (void)enableUI:(BOOL)isEnable;
@end

//
//  ViewControllerDelegateConstants.h
//  Core
//
//  Created by ToanPham on 2/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//


/**
 *
 */
static NSInteger const VC_CODE = 1;
static NSInteger const VC_EDIT_ADDRESS = 2;
static NSInteger const VC_SELECT_CATEGORY = 3;
static NSInteger const VC_NEW_FEED = 5;
static NSInteger const VC_TOTAL_COMMENTS = 6;
static NSInteger const VC_NEW_OR_EDIT_COMMENT = 7;
static NSInteger const VC_EDIT_FEED = 8;
static NSInteger const VC_EDIT_COMMENT = 9;
static NSInteger const VC_CODE_LOGIN = 10;
static NSInteger const VC_SELECT_BOOTH = 11;
static NSInteger const VC_SELECT_YOUR_LOCATION = 12;
static NSInteger const VC_SELECT_NEAREST_WC = 13;

static NSInteger const VC_SELECT_ADDRESS = 14;
static NSInteger const VC_SELECT_ADDRESS_HAVE_MAP = 32;
static NSInteger const VC_SELECT_HOME = 15;
static NSInteger const VC_SELECT_PAYMENT = 16;
static NSInteger const VC_SELECT_PROMOTION = 17;
static NSInteger const VC_DELETE_PAYMENT = 18;
static NSInteger const VC_RATING_DONE = 19;
static NSInteger const VC_SET_PIN = 20;
static NSInteger const VC_UPLOAD_PHOTO_GARDEN = 21;
static NSInteger const VC_ACCEPT_REESTIMATE = 22;
static NSInteger const VC_UPLOAD_PHOTO_STEP4_PROVIDER = 23;
static NSInteger const VC_UPLOAD_PHOTO_STEP4_ROUTING_NUMBER = 24;
static NSInteger const VC_UPLOAD_PHOTO_STEP4_SSN = 25;
static NSInteger const VC_UPLOAD_PHOTO_STEP4_ID = 26;
static NSInteger const VC_UPLOAD_PHOTO_STEP4_DRIVER = 27;

static NSInteger const VC_CANCEL_SCHEDULED_SERVICE = 28;
static NSInteger const VC_EDIT_SCHEDULED_SERVICE = 29;

static NSInteger const VC_GET_BORDER_POINTS_LIST = 30;
static NSInteger const VC_GET_SET_PIN_ROTATION = 31;

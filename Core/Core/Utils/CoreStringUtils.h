//
//  CoreStringUtils.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NSLocalizedString(key, comment) [[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:nil]

@interface CoreStringUtils : NSObject

+ (NSString*)getStringDiffNull:(NSObject*)object;
+ (BOOL)isStrongPassword:(NSString*)password;
+ (BOOL)isEmpty:(NSString*)str;
+ (BOOL)isContainWhiteSpace:(NSString*)str;
+ (BOOL)isEmptyOrWhiteSpace:(NSString*)str;
+ (BOOL)isValidEmail:(NSString*)email;
+ (NSString*)getStringWithErrorCode:(NSString*)errorMessage errorCode:(NSInteger)errorCode;
+ (NSString*)getStringRes:(NSString*)resStr;
+ (NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar;
+ (NSString*)getStringNumberDiffNull:(NSObject*)object;
+ (uint32_t)convertStringToUnsignedLong:(NSString*)strObject;
+ (NSString*)getVideoIDFromYoutubeURL:(NSString*)youtubeURL;
+ (NSString*)getFollowers:(int)totalFollow InEvent:(int)totalLive;
+ (NSMutableAttributedString*)getFollowers:(int)totalFollow InEvent:(int)totalLive status:(int)status;

@end

//
//  CoreStringUtils.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "CoreStringUtils.h"
#import "Helpers.h"
#import "CoreConstants.h"

@implementation CoreStringUtils

+ (NSString*)getStringDiffNull:(NSObject*)object {
    if (object != nil && ![object isKindOfClass:[NSNull class]]) {
        NSString *s = [NSString stringWithFormat:@"%@", object];
        if (![ s isEqual:@"null"])
            return s;
    }
    return @"";
}

+ (BOOL)isStrongPassword:(NSString*)password {
    if (password.length < 6) {
        return false;
    }
    return true;
}

+ (BOOL)isEmpty:(NSString*)str {
//    BOOL isEmpty = true;
//    
//    for (int i = 0; i<str.length; i++) {
//        if (![[str substringWithRange:NSMakeRange(i, 1)] isEqualToString:@" "]) {
//            isEmpty = false;
//        }
//    }
//    if (isEmpty == true) {
//        return true;
//    }
    
    if (str != nil && str.length > 0)
        return FALSE ;
    return TRUE;
}

+ (BOOL)isContainWhiteSpace:(NSString*)str {
    NSRange whiteSpaceRange = [str rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        return YES;
    }
    return NO;
}

+ (BOOL)isEmptyOrWhiteSpace:(NSString*)str {
    if (str != nil && str.length > 0) {
        NSString* trimmed = [str stringByTrimmingCharactersInSet:                                                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (trimmed.length > 0)
            return FALSE ;
        else
            return TRUE;
    }
    return TRUE;
}

+ (BOOL)isValidEmail:(NSString*)email {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
}

+ (NSString*)getStringWithErrorCode:(NSString*)errorMessage errorCode:(NSInteger)errorCode {
    NSMutableString *s = [[NSMutableString alloc] init];
    [s appendString:NSLocalizedString(errorMessage, nill)];
    [s appendString:@"("];
    [s appendString:[NSString stringWithFormat:@"%ld", (long)errorCode]];
    [s appendString:@")"];
    return [NSString stringWithString:s];
}

+ (NSString*)getStringRes:(NSString*)resStr {
    return NSLocalizedString(resStr, nill);
}

+(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return @"";
    
    simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    // check if the number is to long
//    if(simpleNumber.length>10) {
//        // remove last extra chars.
//        simpleNumber = [simpleNumber substringToIndex:simpleNumber.length];
//    }
//    
//    if(deleteLastChar) {
//        // should we delete the last digit?
//        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
//    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"$1-$2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"$1-$2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}

+ (NSString*)getStringNumberDiffNull:(NSObject*)object {
    if (object != nil && ![object isKindOfClass:[NSNull class]]) {
        return [NSString stringWithFormat:@"%@", object];
    }
    return @"0";
}

+ (uint32_t)convertStringToUnsignedLong:(NSString*)strObject {
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    return (uint32_t)[[formatter numberFromString:strObject] unsignedLongValue];
}

+ (NSString*)getVideoIDFromYoutubeURL:(NSString*)youtubeURL {
    NSError *error = NULL;
    NSRegularExpression *regex =
    [NSRegularExpression regularExpressionWithPattern:@"(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*"
                                              options:NSRegularExpressionCaseInsensitive
                                                error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:youtubeURL
                                                    options:0
                                                      range:NSMakeRange(0, [youtubeURL length])];
    if (match) {
        NSRange videoIDRange = [match rangeAtIndex:0];
        return [youtubeURL substringWithRange:videoIDRange];
    }
    return @"";
}

+ (NSString*)getFollowers:(int)totalFollow InEvent:(int)totalLive {
    NSString* follower = totalFollow <= 1 ? [CoreStringUtils getStringRes:@"str_follower"] : [CoreStringUtils getStringRes:@"str_followers"];
    NSString* live = totalLive <= 1 ? [CoreStringUtils getStringRes:@"str_in_event"] : [CoreStringUtils getStringRes:@"str_in_events"];
    return [NSString stringWithFormat:@"%@ %@ • %@ %@", [Helpers convertThousandToK:totalFollow], follower, [Helpers convertThousandToK:totalLive], live];
}

+ (NSMutableAttributedString*)getFollowers:(int)totalFollow InEvent:(int)totalLive status:(int)status {
    NSString* follower = totalFollow <= 1 ? [CoreStringUtils getStringRes:@"str_follower"] : [CoreStringUtils getStringRes:@"str_followers"];
    NSString* live = totalLive <= 1 ? [CoreStringUtils getStringRes:@"str_in_event"] : [CoreStringUtils getStringRes:@"str_in_events"];
    NSString* strStatus = @"";
    switch (status) {
        case EventDefaultStatus: {
            strStatus = @"";
        }
            break;
        case EventJustFinished: {
            strStatus = [CoreStringUtils getStringRes:@"str_just_finished"];
        }
            break;
        case EventOngoing: {
            strStatus = [CoreStringUtils getStringRes:@"str_ongoing"];
        }
            break;
        case EventUpcoming: {
            strStatus = [CoreStringUtils getStringRes:@"str_upcoming"];
        }
            break;
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    NSString* followers = [NSString stringWithFormat:@"<span style=\"font-family: SanFranciscoDisplay-Regular; font-size: 14\"><font color='#8899A6'>%@ %@ • %@ %@</font><font color='#ff0000'> • %@</font></span>", [Helpers convertThousandToK:totalFollow], follower, [Helpers convertThousandToK:totalLive], live, strStatus];
    NSMutableAttributedString* attributeString = [[NSMutableAttributedString alloc] initWithData:[followers dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attributeString length])];
    return attributeString;
}

@end

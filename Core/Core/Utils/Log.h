//
//  Log.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Log : NSObject

+ (void)initPath;
+ (void)d:(NSInteger)level tag:(NSString*)tag message:(NSObject*)message;

@end

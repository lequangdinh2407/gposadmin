//
//  SharePrefUtils.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/28/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "SharePrefUtils.h"
#import "CoreConstants.h"
#import "SessionManager.h"
#import "UserEntity.h"

@implementation SharePrefUtils

+ (void)writeStringPreference:(NSString*)prefKey prefValue:(NSString*)prefValue {
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:prefValue forKey:prefKey];
    [preferences synchronize];
}

+ (NSString*)getStringPreference:(NSString*)prefKey {
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    return [preferences stringForKey:prefKey];
}

+ (void)writeIntPreference:(NSString*)prefKey prefValue:(NSInteger)prefValue {
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    [preferences setInteger:prefValue forKey:prefKey];
    [preferences synchronize];
}

+ (NSInteger)getIntPreference:(NSString*)prefKey prefValue:(NSInteger)prefValue {
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    NSObject * object = [preferences objectForKey:prefKey];
    if (!object) {
        return prefValue;// Default is YES
    }
    return [preferences integerForKey:prefKey];
}

+ (void)writeDoublePreference:(NSString*)prefKey prefValue:(double)prefValue {
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    [preferences setDouble:prefValue forKey:prefKey];
    [preferences synchronize];
}

+ (NSInteger)getDoublePreference:(NSString*)prefKey prefValue:(double)prefValue {
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    NSObject * object = [preferences objectForKey:prefKey];
    if (!object) {
        return prefValue;// Default is YES
    }
    return [preferences doubleForKey:prefKey];
}

+ (void)writeBoolPreference:(NSString*)prefKey prefValue:(BOOL)prefValue{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    [preferences setBool:prefValue forKey:prefKey];
    [preferences synchronize];
}

+ (BOOL)getBoolPreference:(NSString*)prefKey{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    NSObject * object = [preferences objectForKey:prefKey];
    if (!object) {
        return NO;// Default is NO
    }
    return [preferences boolForKey:prefKey];
}

+ (void)writeArrayPreference:(NSString*)prefKey prefValue:(NSArray*)prefValue{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    NSData* archivedServerModules = [NSKeyedArchiver archivedDataWithRootObject:prefValue];
    [preferences setObject:archivedServerModules forKey:prefKey];
    [preferences synchronize];
}

+ (NSArray*)getArrayPreference:(NSString*)prefKey{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    NSData* archivedServerModules = [preferences objectForKey:prefKey];
    return [NSKeyedUnarchiver unarchiveObjectWithData:archivedServerModules];
}

+ (void)writeObjectPreference:(NSString*)prefKey prefValue:(id)prefValue{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:prefValue forKey:prefKey];
    [preferences synchronize];
}

+ (id)getObjectPreference:(NSString*)prefKey{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    return [preferences objectForKey:prefKey];
}

+ (NSInteger)getIntPreference:(NSString*)prefKey {
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    return [preferences integerForKey:prefKey];
}

+ (void)setMessageSoundEnable:(BOOL)isEnable{
    NSString* key = [PREFKEY_MESSAGE_SOUND_ENABLE stringByAppendingString:[SessionManager sharedClient].userEntity.userId];
    [SharePrefUtils writeBoolPreference:key prefValue:isEnable];
}

+ (BOOL)getMessageSoundEnable{
    NSString* key = [PREFKEY_MESSAGE_SOUND_ENABLE stringByAppendingString:[SessionManager sharedClient].userEntity.userId];
    return [SharePrefUtils getBoolPreference:key];
}

+ (void)writeDictionaryPreference:(NSString*)prefKey prefValue:(NSDictionary*)prefValue{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:prefValue forKey:prefKey];
    [preferences synchronize];
}

+ (NSDictionary*)getDictionaryPreference:(NSString*)prefKey{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    return [preferences dictionaryForKey:prefKey];
}

+ (void)clearAll {

    [self writeStringPreference:PREFKEY_UPDATE_TIME prefValue:@""];
    [self writeStringPreference:PREFKEY_ACCESSTOKEN prefValue:@""];
    [self writeStringPreference:PREFKEY_CURRENT_TOKEN prefValue:@""];
    
    [self writeStringPreference:PREFKEY_CONFIG_API_URL prefValue:@""];
    [self writeStringPreference:PREFKEY_CONFIG_USERNAME prefValue:@""];
    [self writeStringPreference:PREFKEY_CONFIG_STORENAME prefValue:@""];
    [self writeStringPreference:PREFKEY_CONFIG_PASSWORD prefValue:@""];
    [self writeStringPreference:PREFKEY_CONFIG_STOREID prefValue:@""];
    [self writeStringPreference:PREFKEY_CONFIG_STORE_ID prefValue:@""];
}

+ (BOOL)existedCached:(NSString*)prefKey{
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    NSObject * object = [preferences objectForKey:prefKey];
    if (!object) {
        return NO;
    }
    return YES;
}

@end

//
//  JSONUtils.m
//  Core
//
//  Created by ToanPham on 4/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "JSONUtils.h"
#import "Helpers.h"


@implementation JSONUtils

+ (NSString*)convertToJson:(NSObject*)params {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:0 // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!jsonData) {
        [Log d:0 tag:@"" message:error];
        return @"";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

+ (NSMutableDictionary*)parseJsonToMap:(NSString*)jsonData {
    if (![Helpers isNotNull:jsonData])
        return nil;
    
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&jsonError];
    
    if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        return [(NSDictionary*)jsonObject mutableCopy];
    }
    return nil;
}

+ (NSMutableDictionary*)parseDataToMap:(NSData*)jsonData {
    if (![Helpers isNotNull:jsonData])
        return nil;
    
    NSString* jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return [self parseJsonToMap:jsonStr];
}

+ (NSObject*)parseDataToObject:(NSData*)jsonData {
    if (![Helpers isNotNull:jsonData])
        return nil;
    
    NSString* jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return [self parseJsonToObject:jsonStr];
}

+ (NSObject*)parseJsonToObject:(NSString*)jsonData {
    if (![Helpers isNotNull:jsonData])
        return nil;
    
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&jsonError];
    if (jsonError == nil)
        return jsonObject;
    return nil;
}

@end
//
//  SearchUtils.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/31/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "SearchUtils.h"
#import "BaseEntity.h"
#import "Log.h"

static NSInteger const PRIORITY_EQUAL_FULLNAME 	= 100;
static NSInteger const PRIORITY_EQUAL_FIRSTNAME = 50;
static NSInteger const PRIORITY_EQUAL_LASTNAME 	= 50;

@implementation SearchUtils

+ (NSInteger)calculateSimilar:(NSArray*)array1 array2:(NSArray*)array2 {
    NSInteger priority = 0;
    for (int i = 0; i < array1.count; i++) {
        for (int j = 0; j < array2.count; j++) {
            NSString *s1 = array1[i];
            NSString *s2 = array2[j];
            if ([s1 hasPrefix:s2]) {
                priority += s2.length;
            } else if ([s2 hasPrefix:s1]) {
                priority += s1.length;
            }
            if ([s1 isEqualToString:s2]) {
                priority += PRIORITY_EQUAL_FULLNAME;
            }
        }
    }
    
    if (array1.count > 0 && array2.count > 0) {
        NSString *s1 = array1[0];
        NSString *sl1 = array1[array1.count -1];
        NSString *s2 = array2[0];
        NSString *sl2 = array2[array2.count -1];
        
        if ([s1 isEqualToString:s2]) {
            priority += PRIORITY_EQUAL_FIRSTNAME;
        } else if ([s1 isEqualToString:sl2]) {
            priority += PRIORITY_EQUAL_LASTNAME;
        } else if ([sl1 isEqualToString:s2]) {
            priority += PRIORITY_EQUAL_FIRSTNAME;
        } else if ([sl1 isEqualToString:sl2]) {
            priority += PRIORITY_EQUAL_LASTNAME;
        }
    }
    
    return priority;
}

+ (NSMutableArray*)filter:(NSString*)searchString originalList:(NSMutableArray*)originList {
    NSMutableArray *resultList = [[NSMutableArray alloc] init];

    if (![CoreStringUtils isEmpty:searchString]) {
        NSArray *arrayKey = [self preprocessSearchStr:searchString];
        for (BaseEntity *baseEntity in originList) {
            baseEntity.priority = 0;
            NSArray *arrayStr = [self preprocessSearchStr:[baseEntity getFieldName]];
            baseEntity.priority += [self calculateSimilar:arrayKey array2:arrayStr];
            
            //[Log d:0 tag:@"filter" message:baseEntity.fieldName];
            if ([baseEntity.fieldName isEqualToString:searchString]) {
//                [Log d:0 tag:@"filter" message:baseEntity.fieldName];
//                [Log d:0 tag:@"filter searchString " message:searchString];

                baseEntity.priority += PRIORITY_EQUAL_FULLNAME;
            }
            
            if (baseEntity.priority > 0) {

                [resultList addObject:baseEntity];
            }
        }
        resultList = [[resultList sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
    } else
        resultList = originList;
    
    return resultList;
}

+ (NSArray*)preprocessSearchStr:(NSString*)org {
    const char *arrChar = [org UTF8String];
    char *result = calloc([org length] + 1, 1);
    for (int i = 0; i < org.length; i++) {
        switch (arrChar[i]) {
            case '(':
            case ':':
            case ';':
            case '/':
            case '-':
            case ')':
            {
                result[i] = ' ';
                break;
            }
            case L'\u00E1':
            case L'\u00E0':
            case L'\u1EA3':
            case L'\u00E3':
            case L'\u1EA1':
            case L'\u0103':
            case L'\u1EAF':
            case L'\u1EB1':
            case L'\u1EB3':
            case L'\u1EB5':
            case L'\u1EB7':
            case L'\u00E2':
            case L'\u1EA5':
            case L'\u1EA7':
            case L'\u1EA9':
            case L'\u1EAB':
            case L'\u1EAD':
            case L'\u01CE':
            {
                result[i] = 'a';
                break;
            }
            case L'\u00E9':
            case L'\u00E8':
            case L'\u1EBB':
            case L'\u1EBD':
            case L'\u1EB9':
            case L'\u00EA':
            case L'\u1EBF':
            case L'\u1EC1':
            case L'\u1EC3':
            case L'\u1EC5':
            case L'\u1EC7':
            case L'\u0207':
            {
                result[i] = 'e';
                break;
            }
            case L'\u00ED':
            case L'\u00EC':
            case L'\u1EC9':
            case L'\u1ECB':
            {
                result[i] = 'i';
                break;
            }
            case L'\u00F3':
            case L'\u00F2':
            case L'\u1ECF':
            case L'\u00F5':
            case L'\u1ECD':
            case L'\u00F4':
            case L'\u1ED1':
            case L'\u1ED3':
            case L'\u1ED5':
            case L'\u1ED7':
            case L'\u1ED9':
            case L'\u1EDB':
            case L'\u1EDD':
            case L'\u1EDF':
            case L'\u020F':
            {
                result[i] = 'o';
                break;
            }
            case L'\u00FA':
            case L'\u00F9':
            case L'\u1EE7':
            case L'\u0169':
            case L'\u1EE5':
            case L'\u01B0':
            case L'\u1EEB':
            case L'\u1EEF':
            case L'\u1EF1':
            {
                result[i] = 'u';
                break;
            }
            case L'\u1EF7':
            {
                result[i] = 'y';
                break;
            }
            case L'\u0111':
            {
                result[i] = 'd';
                break;
            }
            case L'\u00C0':
            case L'\u1EA2':
            case L'\u1EA0':
            case L'\u0102':
            case L'\u1EAE':
            case L'\u1EB2':
            case L'\u1EB4':
            case L'\u1EB6':
            case L'\u00C2':
            case L'\u1EA4':
            case L'\u1EA6':
            case L'\u1EA8':
            case L'\u1EAA':
            case L'\u1EAC':
            {
                result[i] = 'A';
                break;
            }
            case L'\u00C8':
            case L'\u1EBA':
            case L'\u1EBC':
            case L'\u1EB8':
            case L'\u00CA':
            case L'\u1EBE':
            case L'\u1EC4':
            case L'\u1EC6':
            case L'\u0206':
            {
                result[i] = 'E';
                break;
            }
            case L'\u00CC':
            {
                result[i] = 'I';
                break;
            }
            case L'\u00D2':
            case L'\u00D4':
            case L'\u1ED0':
            case L'\u1ED6':
            case L'\u1ED8':
            case L'\u1EDA':
            case L'\u1EDC':
            case L'\u1EDE':
            case L'\u020E':
            {
                result[i] = 'O';
                break;
            }
            case L'\u1EE6':
            case L'\u0168':
            case L'\u1EE4':
            case L'\u1EEE':
            case L'\u1EF0':
            {
                result[i] = 'U';
                break;
            }
            
            case L'\u1EF6':
            case L'\u1EF8':
            {
                result[i] = 'Y';
                break;
            }
            case L'\u0110':
            {
                result[i] = 'D';
                break;
            }
            
            default:
                result[i] = arrChar[i];
            break;
        }
    }
    NSString *s = [[NSString stringWithUTF8String:result] lowercaseString];
    return [s componentsSeparatedByString:@" "];
}

@end

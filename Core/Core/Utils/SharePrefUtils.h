//
//  SharePrefUtils.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/28/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharePrefUtils : NSObject

+ (void)writeStringPreference:(NSString*)prefKey prefValue:(NSString*)prefValue;
+ (NSString*)getStringPreference:(NSString*)prefKey;

+ (void)writeIntPreference:(NSString*)prefKey prefValue:(NSInteger)prefValue;
+ (NSInteger)getIntPreference:(NSString*)prefKey prefValue:(NSInteger)prefValue;
+ (NSInteger)getIntPreference:(NSString*)prefKey;

+ (void)writeBoolPreference:(NSString*)prefKey prefValue:(BOOL)prefValue;
+ (BOOL)getBoolPreference:(NSString*)prefKey;

+ (void)writeArrayPreference:(NSString*)prefKey prefValue:(NSArray*)prefValue;
+ (NSArray*)getArrayPreference:(NSString*)prefKey;

+ (void)writeObjectPreference:(NSString*)prefKey prefValue:(id)prefValue;
+ (id)getObjectPreference:(NSString*)prefKey;

+ (void)setMessageSoundEnable:(BOOL)isEnable;
+ (BOOL)getMessageSoundEnable;

+ (void)writeDictionaryPreference:(NSString*)prefKey prefValue:(NSDictionary*)prefValue;
+ (NSDictionary*)getDictionaryPreference:(NSString*)prefKey;

+ (void)clearAll;
+ (BOOL)existedCached:(NSString*)prefKey;

+ (void)writeDoublePreference:(NSString*)prefKey prefValue:(double)prefValue;
+ (NSInteger)getDoublePreference:(NSString*)prefKey prefValue:(double)prefValue;

@end

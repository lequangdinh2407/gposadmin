//
//  Helpers.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "Helpers.h"
#import "Cryptor.h"
#import "Log.h"
#import "iToast.h"
#import "CRToast.h"
#import "UIImageView+WebCache.h"
#import "CoreConstants.h"
#import <math.h>
#import <sys/utsname.h> 

@implementation Helpers

static uint64_t msgID = 0;

+ (BOOL)checkIfIphoneX {
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        if ((int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
            return true;
        }
    }
    return false;
}

+ (NSString*)convertToJson:(NSMutableDictionary*)params {
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!jsonData) {
        [Log d:0 tag:@"" message:error];
        return @"";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

+ (BOOL) isValidEmail:(NSString *)email {
//    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
//    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
//    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
//    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//    return [emailTest evaluateWithObject:email];
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+[A-Za-z0-9]{1}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (BOOL)isContantGifExtension:(NSString*)link{
    
    NSRange rangeOfString = NSMakeRange(0, [link length]);
    NSError* error = nil;
    
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:@".gif" options:0 error:&error];
    NSArray *matchs = [regex matchesInString:link options:0 range:rangeOfString];
    if (matchs.count > 0) {
        return YES;
    }
    return NO;
}

+ (BOOL)isContantImageLink:(NSString*)link{
    NSArray *imageExtensions = @[@".png", @".jpg", @".gif"]; //...
    
    NSRange rangeOfString = NSMakeRange(0, [link length]);
    //NSLog(@"searchedString: %@", searchedString);
    
    for (NSString* pattern in imageExtensions) {
        NSError* error = nil;
        
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
        NSArray *matchs = [regex matchesInString:link options:0 range:rangeOfString];
        if (matchs.count > 0) {
            return YES;
        }
    }
    return NO;
}

+ (void)setViewCircle:(UIView*)view{
    view.layer.cornerRadius = view.frame.size.width / 2;
    view.layer.masksToBounds = YES;
    view.clipsToBounds = YES;
}

+ (void)giveBorderWithCornerRadious:(UIView*)view radius:(CGFloat)radius borderColor:(UIColor *)borderColor andBorderWidth:(CGFloat)borderWidth {
    CGRect rect = view.bounds;
    
    //Make round
    // Create the path for to make circle
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:rect
                                                   byRoundingCorners:UIRectCornerAllCorners
                                                         cornerRadii:CGSizeMake(radius, radius)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
    maskLayer.frame = rect;
    maskLayer.path  = maskPath.CGPath;
    
    // Set the newly created shape layer as the mask for the view's layer
    view.layer.mask = maskLayer;
    
    //Give Border
    //Create path for border
    UIBezierPath *borderPath = [UIBezierPath bezierPathWithRoundedRect:rect
                                                     byRoundingCorners:UIRectCornerAllCorners
                                                           cornerRadii:CGSizeMake(radius, radius)];
    
    // Create the shape layer and set its path
    CAShapeLayer *borderLayer = [CAShapeLayer layer];
    
    borderLayer.frame       = rect;
    borderLayer.path        = borderPath.CGPath;
    borderLayer.strokeColor = [UIColor whiteColor].CGColor;
    borderLayer.fillColor   = [UIColor clearColor].CGColor;
    borderLayer.lineWidth   = borderWidth;
    
    //Add this layer to give border.
    [[view layer] addSublayer:borderLayer];
}

+ (void)giveBorderWithCornerRadious:(UIView*)view radius:(CGFloat)radius borderColor:(UIColor *)borderColor andBorderWidth:(CGFloat)borderWidth rect:(CGRect)rect {
    
    //Make round
    // Create the path for to make circle
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:rect
                                                   byRoundingCorners:UIRectCornerAllCorners
                                                         cornerRadii:CGSizeMake(radius, radius)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
    maskLayer.frame = rect;
    maskLayer.path  = maskPath.CGPath;
    
    // Set the newly created shape layer as the mask for the view's layer
    view.layer.mask = maskLayer;
    
    //Give Border
    //Create path for border
    UIBezierPath *borderPath = [UIBezierPath bezierPathWithRoundedRect:rect
                                                     byRoundingCorners:UIRectCornerAllCorners
                                                           cornerRadii:CGSizeMake(radius, radius)];
    
    // Create the shape layer and set its path
    CAShapeLayer *borderLayer = [CAShapeLayer layer];
    
    borderLayer.frame       = rect;
    borderLayer.path        = borderPath.CGPath;
    borderLayer.strokeColor = [UIColor whiteColor].CGColor;
    borderLayer.fillColor   = [UIColor clearColor].CGColor;
    borderLayer.lineWidth   = borderWidth;
    
    //Add this layer to give border.
    [[view layer] addSublayer:borderLayer];
}

+ (NSString*) getBasic:(NSString*)userName andPassword:(NSString*)password {
    NSString* key = [NSString stringWithFormat:@"%@:%@", userName, password];
    NSData* data = [key dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [data base64EncodedStringWithOptions:0];
    return base64Encoded;
}

+ (NSString*) getToken:(NSString*)basic {
    NSDate *currentDate = [[NSDate alloc] init];

    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    
    
    // or Timezone with specific name like
    // [NSTimeZone timeZoneWithName:@"Europe/Riga"] (see link below)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *localDateString = [dateFormatter stringFromDate:currentDate];
  
   //NSString *encryptedString = [Helpers hmacsha1:basic secret:[dateFormatter stringFromDate:[NSDate date]]];
    NSString *encryptedString = [Helpers hmacsha1:localDateString secret:basic];
    //NSString *base64Encoded = [Helpers encodeStringTo64:encryptedString];
    return encryptedString;
    
    //@"97jpmh2Wja6QzFNgx0IfnKsNxqA=";
    //base64Encoded;
}

+ (NSString*)encodeStringTo64:(NSString*)fromString
{
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String;
    if ([plainData respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        base64String = [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
    } else {
        base64String = [plainData base64EncodedStringWithOptions:0];                              // pre iOS7
    }
    
    return base64String;
}

+ (NSString*) getCurrentTimeStamp {
    long long milliseconds = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);
    return [NSString stringWithFormat:@"%lld", milliseconds];
}

+ (uint64_t) getCurrentTimeStampUInt {
    uint64_t milliseconds = (uint64_t)([[NSDate date] timeIntervalSince1970] * 1000.0);
    return milliseconds;
}

+ (BOOL)isValidList:(NSMutableArray*)list {
    if (list != nil && list.count > 0) {
        return true;
    }
    return false;
}

+ (BOOL)isValidArray:(NSArray*)list {
    if (list != nil && list.count > 0) {
        return true;
    }
    return false;
}

/*huydna: Check not nil or NSNul*/
+ (BOOL)isNotNull:(NSObject*)data {
    if (data == nil || [data isKindOfClass:[NSNull class]]) {
        return false;
    }
    return true;
}
/*~huydna*/

+ (NSString*)getUDID {
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

/*
+ (NSString *)hmacsha1:(NSString *)data secret:(NSString *)key {
    
    const char *cKey  = [key cStringUsingEncoding:NSUTF8StringEncoding];
    const char *cData = [data cStringUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    
    NSString *hash = [HMAC base64EncodedStringWithOptions:NSUTF8StringEncoding];
    
    return hash;
}
*/

+ (NSString*)hmacsha1:(NSString*)stringData secret:(NSString*)key {
    [Log d:0 tag:@"signature = " message:stringData];
    if (![CoreStringUtils isEmpty:stringData]) {
        const char *cKey  = [key cStringUsingEncoding:NSUTF8StringEncoding];
        const char *cData = [stringData cStringUsingEncoding:NSUTF8StringEncoding];
        unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
        CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
        NSData *HMACData = [NSData dataWithBytes:cHMAC length:sizeof(cHMAC)];
        NSString* stringBase64 = [HMACData base64EncodedStringWithOptions:NSUTF8StringEncoding];
        return stringBase64;
    }
    return @"DataSubmitEncryptedWithHMAC_SHA1";
}

+ (NSString*)encryptDataSHA256:(NSString*)stringData key:(NSString*)privateKey {
    [Log d:0 tag:@"signature = " message:stringData];
    if (![CoreStringUtils isEmpty:stringData]) {
        const char *cKey  = [privateKey cStringUsingEncoding:NSUTF8StringEncoding];
        const char *cData = [stringData cStringUsingEncoding:NSUTF8StringEncoding];
        unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
        CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
        NSData *HMACData = [NSData dataWithBytes:cHMAC length:sizeof(cHMAC)];
        NSString* stringBase64 = [HMACData base64EncodedStringWithOptions:NSUTF8StringEncoding];
        return stringBase64;
    }
    return @"DataSubmitEncryptedWithHMAC_SHA256";
}

+ (NSString*) encryptData:(NSString*) jsonStringData key:(NSString*)privateKey
{
    [Log d:0 tag:@"jSonDataNddEncrypt = " message:jsonStringData];
    
    NSData *data = [jsonStringData dataUsingEncoding:NSUTF8StringEncoding];
    if (data !=nil) {
        NSData *encryptData = [data encryptAES:privateKey];
        NSString* stringBase64 = [encryptData base64EncodedStringWithOptions:NSUTF8StringEncoding];
//        [Log d:0 tag:@"Base64 Encrypted with AES %@" message:stringBase64];
        return stringBase64;
    }
    return @"DataSubmitEncryptedWithAES";
}

+ (NSString*) decryptData:(NSString*) jsonStringData key:(NSString*)privateKey
{
    [Log d:0 tag:@"jSonDataNddDecrypt =" message:jsonStringData];
    NSData* data = [[NSData alloc] initWithBase64EncodedString:jsonStringData options:NSUTF8StringEncoding];
    NSData *decryptData = [data decryptAES:privateKey];
    const char* fileBytes = (const char*)[decryptData bytes];
    NSUInteger length = [decryptData length];
    NSInteger count = 0;
    for (NSInteger index = length - 1; index >= 0; index--)
    {
        char aByte = fileBytes[index];
        if (aByte == 0) {
            count++;
        }
    }
    
    length = length - count;
    NSMutableData * decryptData2 = [NSMutableData dataWithData:decryptData];
    [decryptData2 setLength:length];
    
    NSData *immutableData = [NSData dataWithData:decryptData2];
    
    return [[NSString alloc] initWithData:immutableData
                                 encoding:NSUTF8StringEncoding];
}


+ (BOOL)isValidTaxCode:(NSString*)taxcode {
    if (![CoreStringUtils isEmpty:taxcode]) {
        if (taxcode.length >= 9 && [taxcode hasPrefix:@"0"] == FALSE) {
            return TRUE;
        }
    }
    return FALSE;
}

+ (NSString*)encryptString:(NSString*)plaintext withKey:(NSString*)key {
    NSData *data= [[plaintext dataUsingEncoding:NSUTF8StringEncoding] AES256EncryptWithKey:key];
    return [data base64EncodedStringWithOptions:0];
}

+ (NSString*)decryptString:(NSString*)ciphertext withKey:(NSString*)key {  
    NSData* data = [[NSData alloc] initWithBase64EncodedString:ciphertext options:0];
    return [[NSString alloc] initWithData:[data AES256DecryptWithKey:key]
                                  encoding:NSUTF8StringEncoding];
}

+ (void)showToast:(NSString*)message {
    [CRToastManager dismissAllNotifications:YES];
    [CRToastManager showNotificationWithMessage:NSLocalizedString(message, nill)
     
                                completionBlock:^(void) {
                                    NSLog(@"Completed");
                                }];

}
//
//+ (NSString*)convert:(NSString *)text
//{
//    int length = (int)[self getLength:text];
//    //NSLog(@"Length  =  %d ",length);
//    
//    if(length == 10)
//    {
////        if(range.length == 0)
////            return NO;
//    }
//    
//    if(length == 3)
//    {
//        NSString *num = [self formatNumber:textField.text];
//        text = [NSString stringWithFormat:@"%@-",num];
//        
////        if(range.length > 0)
//            text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
//    }
//    else if(length == 6)
//    {
//        NSString *num = [self formatNumber:textField.text];
//        //NSLog(@"%@",[num  substringToIndex:3]);
//        //NSLog(@"%@",[num substringFromIndex:3]);
//        text = [NSString stringWithFormat:@"%@-%@-",[num  substringToIndex:3],[num substringFromIndex:3]];
//        
////        if(range.length > 0)
//            text = [NSString stringWithFormat:@"%@-%@",[num substringToIndex:3],[num substringFromIndex:3]];
//    }
//    
//    return YES;
//}

+(NSString*)formatNumber:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    NSInteger length = [mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        [Log d:0 tag:@"mobileNumber =" message:mobileNumber];
    }
    
    return mobileNumber;
}

+(int)getLength:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSInteger length = [mobileNumber length];
    
    return (int)length;
}

+ (void)logoutFB {
    
}

+ (void)clearData {
//    [ShopManager sharedClient].shopEntity = nil;
//    [[ShopManager sharedClient] clearAll];
//    [[ServiceMenuManager sharedClient] clearAll];
//    [[CustomerManager sharedClient] clearAll];
//    [[SupplierManager sharedClient] clearAll];
}

+ (uint32_t)getUIntDiffNull:(id)object {
    if (object != nil && ![object isKindOfClass:[NSNull class]]) {
        return [object unsignedIntValue];
    }
    return 0;
}

+ (double)getDoubleDiffNull:(id)object {
    if (object != nil && ![object isKindOfClass:[NSNull class]]) {
        return [object doubleValue];
    }
    return 0;
}

+ (uint64_t)getULongLongDiffNull:(id)object {
    if (object != nil && ![object isKindOfClass:[NSNull class]]) {
        return [object unsignedLongLongValue];
    }
    return 0;
}

+ (NSString*)convertMeterToDistance:(id)object {
    NSInteger distanceUnit = [SharePrefUtils getIntPreference:PREFKEY_DISTANCE_UNIT];
    //return [self convertMeterKm:object];
    if (distanceUnit == DISTANCE_METER) {
        return [self convertMeterKm:object];
    }
    return [self convertMeterToMiles:object];
}

+ (NSString*)convertMeterToMiles:(id)object {
    if (object != nil && ![object isKindOfClass:[NSNull class]]) {
        double mile = [object doubleValue] / 1609.344;
        if (mile < 1) {
            long ft = mile * 1760;
            return [NSString stringWithFormat:@"%ld yd", ft];
        } else if (mile == 1) {
            return [NSString stringWithFormat:@"%ld mi", (long)mile];
        } else {
            return [NSString stringWithFormat:@"%.0f mi", (float)mile];
        }
    }
    return @"0 mile";
}

+ (NSString*)convertMeterKm:(id)object {
    if (object != nil && ![object isKindOfClass:[NSNull class]]) {
        double meter = [object doubleValue];
        double Kmeter = meter / 1000;
        if (Kmeter < 1) {
            return [NSString stringWithFormat:@"%d m", (int)meter];
        } else {
            return [NSString stringWithFormat:@"%.02lf km", (float)Kmeter];
        }
    }
    return @"0 m";
}

+ (BOOL)isDateToday:(long)timestamp {
    NSDate *aDate = [NSDate dateWithTimeIntervalSince1970:timestamp];
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:aDate];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    if([today isEqualToDate:otherDate]) {
        return TRUE;
    }
    return FALSE;
}

+ (uint64_t) msgID {
    @synchronized(self) {
        return msgID++;
    }
}
+ (void) setMsgID:(uint64_t)val {
    @synchronized(self)
    {
        msgID = val;
    }
}

+ (NSArray*)getArrayDiffNull:(id)object {
    if (object != nil && [object isKindOfClass:[NSArray class]]) {
        return (NSArray*)object;
    }
    return [[NSArray alloc] init];
}

+ (NSDictionary*)getDictDiffNull:(id)object {
    if (object != nil && [object isKindOfClass:[NSDictionary class]]) {
        return (NSDictionary*)object;
    }
    return [[NSDictionary alloc] init];
}

+ (NSArray*)sortBarList:(NSArray*)list{
    NSSortDescriptor *countInSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"countInBar"
                                                                          ascending:NO];
    NSSortDescriptor *distanceSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance"
                                                                           ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:countInSortDescriptor, distanceSortDescriptor, nil];
    NSArray* result = [list sortedArrayUsingDescriptors:sortDescriptors];
    return result;
}

+ (bool)getBoolDiffNull:(id)object {
    if (object != nil && ![object isKindOfClass:[NSNull class]]) {
        return [object boolValue];
    }
    return false;
}

+ (NSMutableDictionary*)parseJsonToMap:(NSString*)jsonData {
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&jsonError];
    
    if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        return [(NSDictionary*)jsonObject mutableCopy];
    }
    return nil;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize keepRatio:(BOOL)isRatio
{
    CGSize tmpSize = newSize;
    if (isRatio) {
        if (img.size.height <= img.size.width) {
            tmpSize = CGSizeMake(newSize.width, newSize.width * img.size.height / img.size.width);
        }else{
            tmpSize = CGSizeMake(newSize.height * img.size.width / img.size.height, newSize.height);
        }
    }
    
    return [Helpers imageResize:img andResizeTo:tmpSize];
}

+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize
{
    CGFloat scale = [[UIScreen mainScreen]scale];
    UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(UIImage *)setBackgroundImageByColor:(UIColor *)backgroundColor withFrame:(CGRect )rect cornerRadius:(float)radius{
    
    UIView *tcv = [[UIView alloc] initWithFrame:rect];
    [tcv setBackgroundColor:backgroundColor];
    
    CGSize gcSize = tcv.frame.size;
    UIGraphicsBeginImageContext(gcSize);
    [tcv.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    const CGRect RECT = CGRectMake(0, 0, image.size.width, image.size.height);;
    [[UIBezierPath bezierPathWithRoundedRect:RECT cornerRadius:radius] addClip];
    [image drawInRect:RECT];
    UIImage* imageNew = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageNew;
}

+ (void)loadMapInView:(UIImageView*)view withLon:(double)lon lat:(double)lat mapWidth:(float)width mapHeight:(float)height{
    
    NSString *strLat = [NSString stringWithFormat:@"%lf", lat];
    NSString *strLon = [NSString stringWithFormat:@"%lf", lon];
    NSString *str = @"&markers=color:blue%7Flabel:S%7C";
    
    NSString *url = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?center=%@,%@%@%@,%@&zoom=15&size=%dx%d&sensor=false", strLat, strLon, str, strLat, strLon, (int)width, (int)height];
    [Helpers loadImage:view placeHolder:[UIImage imageNamed:@"default_img_map"] withUrl:url];
}

+ (void)gotoMapAtAddress:(NSString*)address city:(NSString*)city state:(NSString*)state country:(NSString*)country{
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        NSString *directionString = [NSString stringWithFormat:@"comgooglemaps://?q=%@, %@, %@, %@", address, city, state, country];
        
        directionString = [directionString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:directionString]];
    } else {
        NSString *directionString = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%@, %@, %@, %@", address, city, state, country];
        directionString = [directionString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        UIApplication *app = [UIApplication sharedApplication];
        [app openURL:[NSURL URLWithString: directionString]];
    }
}

+ (NSString*)replacePriceWithSmall:(NSString*)price {
    return price;
//    if ([price rangeOfString:@"."].location == NSNotFound) {
//        return price;
//    } else {
//        NSString* newPrice = [price stringByReplacingOccurrencesOfString:@"."
//                                             withString:@".<small>"];
//        return [newPrice stringByAppendingString:@"</small>"];
//    }
}

+ (NSString*)replacePriceWithCurrency:(NSString*)price {
    CGFloat fPrice = [price doubleValue];
    NSString* newPrice = nil;
    if (fPrice > 0)
        newPrice = [NSString stringWithFormat:@"%@%@", DEFAULT_CURRENCY, price];
    else
        newPrice = @"Free";
    return newPrice;
}

+ (NSString*)convertThousandToKWithSpaces:(int)count {
    if (count > 1000000) {
        int m = count / 1000000;
        int mod = count % 1000000;
        if (mod > 100000)
            return [NSString stringWithFormat:@"%d.%dm     ", m, mod / 100000];
        else
            return [NSString stringWithFormat:@"%dm     ", m];
    } else if (count > 1000) {
        int k = count / 1000;
        int mod = count % 1000;
        if (mod > 100)
            return [NSString stringWithFormat:@"%d.%dk     ", k, mod / 100];
        else
            return [NSString stringWithFormat:@"%dk     ", k];
    } else {
        return [NSString stringWithFormat:@"%d     ", count]; /* cheat, dont remove space character */
    }
}

+ (NSString*)convertThousandToK:(int)count {
    if (count > 1000000) {
        int m = count / 1000000;
        int mod = count % 1000000;
        if (mod > 100000)
            return [NSString stringWithFormat:@"%d.%dm", m, mod / 100000];
        else
            return [NSString stringWithFormat:@"%dm", m];
    } else if (count > 1000) {
        int k = count / 1000;
        int mod = count % 1000;
        if (mod > 100)
            return [NSString stringWithFormat:@"%d.%dk", k, mod / 100];
        else
            return [NSString stringWithFormat:@"%dk", k];
    } else {
        return [NSString stringWithFormat:@"%d", count];
    }
}

+ (NSString*)addCommasToDouble:(double)doubleNum{
    
    if (doubleNum < 1) {
        return [NSString stringWithFormat:@"%0.02f", doubleNum];
    }
    
    NSNumber *num = [NSNumber numberWithDouble:doubleNum];
    
    // Set up the formatter:
    NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setUsesGroupingSeparator:YES];
    [numFormatter setGroupingSeparator:@","];
    [numFormatter setGroupingSize:3];
    [numFormatter setMaximumFractionDigits:2];
    
    // Get the formatted string:
    NSString *stringNum = [numFormatter stringFromNumber:num];
    return stringNum;
}

+ (NSString*)formatCurrency:(double)doubleNum{
    return [NSString stringWithFormat:@"%@%@", DEFAULT_CURRENCY, [Helpers addCommasToDouble:doubleNum]];
}

+ (NSString*)addCommasToInt:(int)intNum {
    NSNumber *num = [NSNumber numberWithInt:intNum];
    
    // Set up the formatter:
    NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setUsesGroupingSeparator:YES];
    [numFormatter setGroupingSeparator:@","];
    [numFormatter setGroupingSize:3];
    
    // Get the formatted string:
    NSString *stringNum = [numFormatter stringFromNumber:num];
    return stringNum;
}

+ (NSString*)convertMeterToKm:(double)meters {
    double km = meters / 1000;
    return [NSString stringWithFormat:@"%0.0fkm", km];
}


#pragma mark- UIImage methods
+ (void)loadImage:(UIImageView*)imgView withLink:(NSString*)link {
    @try {
        [[SDImageCache sharedImageCache] queryDiskCacheForKey:link done:^(UIImage *image, SDImageCacheType cacheType) {
            if (image) {
                imgView.image = image;
            } else {
                __weak UIImageView* tmpImage = imgView;
                
                imgView.image = nil;
                [SDWebImageDownloader.sharedDownloader downloadImageWithURL:[NSURL URLWithString:link] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize){
                    // progression tracking code
                } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished){
                    if (image && finished) {
                        dispatch_async( dispatch_get_main_queue(), ^{
                            [UIView transitionWithView:tmpImage duration:0.5f options:UIViewAnimationOptionTransitionCrossDissolve
                                            animations:^{
                                                tmpImage.image = image;
                                            } completion:nil];
                        });
                        [[SDImageCache sharedImageCache] storeImage:image forKey:link toDisk:YES];

                    }
                }];
            }
        }];

    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

+ (CGSize)calculateContentSize:(NSString*)content ContainerSize:(CGSize)size font:(UIFont*)textFont{
    CGSize contentLabelSize = CGSizeMake(size.width, size.height);
    CGRect textRect = [content boundingRectWithSize:contentLabelSize
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:textFont}
                                            context:nil];
    return textRect.size;
}

+ (CGSize)calculateHeightText:(NSString*)text width:(CGFloat)width font:(UIFont*)font{
    if ([CoreStringUtils isEmpty:text]) {
        return CGSizeMake(0, 0);
    }
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: font}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX} options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return rect.size;
}

+ (CGSize)calculateWidthText:(NSString*)text height:(CGFloat)height font:(UIFont*)font{
    if ([CoreStringUtils isEmpty:text]) {
        return CGSizeMake(0, 0);
    }
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: font}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){CGFLOAT_MAX, height} options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return rect.size;
}

+ (CGSize)calculateHeightAttributedText:(NSAttributedString*)text width:(CGFloat)width {
    CGRect rect = [text boundingRectWithSize:(CGSize){width, CGFLOAT_MAX} options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return rect.size;
}

+ (CGSize)calculateWidthAttributedText:(NSAttributedString*)text height:(CGFloat)height {
    CGRect rect = [text boundingRectWithSize:(CGSize){CGFLOAT_MAX, height} options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return rect.size;
}

+ (void)loadImage:(UIImageView*)imgV placeHolder:(UIImage*)placeHolder withUrl:(NSString*)link {
    if([[UIScreen mainScreen] respondsToSelector:NSSelectorFromString(@"scale")])
    {
        if ([[UIScreen mainScreen] scale] < 1.1)
            [Helpers loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:320 isScrolling:FALSE];
        
        if ([[UIScreen mainScreen] scale] > 1.9)
            [Helpers loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:480 isScrolling:FALSE];
        return;
    }
    [Helpers loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:320 isScrolling:FALSE];
    //[Helpers loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:50 isScrolling:FALSE];
}

+ (void)loadImage:(UIImageView*)imgV placeHolder:(UIImage*)placeHolder withUrl:(NSString*)link isScrolling:(BOOL)isScrolling {
    if([[UIScreen mainScreen] respondsToSelector:NSSelectorFromString(@"scale")])
    {
        if ([[UIScreen mainScreen] scale] < 1.1)
            [Helpers loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:320 isScrolling:isScrolling];
        
        if ([[UIScreen mainScreen] scale] > 1.9)
            [Helpers loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:480 isScrolling:isScrolling];
        return;
    }
    [Helpers loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:320 isScrolling:isScrolling];
}

+ (void)loadImage:(UIImageView*)imgV placeHolder:(UIImage*)placeHolder withUrl:(NSString*)link thumbSize:(int)size {
    [Helpers loadImage:imgV placeHolder:placeHolder withUrl:link thumbSize:size isScrolling:FALSE];
}

+ (NSString*)getThumbnailUrl:(NSString*)url size:(NSInteger)size {
    if ([url rangeOfString:@"?"].location == NSNotFound) {
        return [NSString stringWithFormat:@"%@?size=%ld", url, (long)size];
    }
    return [NSString stringWithFormat:@"%@&size=%ld", url, (long)size];
}

+ (UIImage*)getImageCache:(NSString*)url size:(NSInteger)size {
    NSString* thumbUrl = [self getThumbnailUrl:url size:size];
    return [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:thumbUrl];
}

+ (UIImage*)getFullImageCache:(NSString*)url {
    return [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:url];
}

+ (void)loadImage:(UIImageView*)imgV placeHolder:(UIImage*)placeHolder withUrl:(NSString*)link thumbSize:(int)size isScrolling:(BOOL)isScrolling {
    @try {
        NSString* thumbUrl = @"";
        if ([link rangeOfString:@"?"].location == NSNotFound) {
           thumbUrl = [NSString stringWithFormat:@"%@?size=%d", link, size];
        } else {
            thumbUrl = [NSString stringWithFormat:@"%@&size=%d", link, size];
        }
        UIImage *image = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:thumbUrl];
        if (image) {
            imgV.image = image;
        }
        
        if (isScrolling == TRUE) {
            imgV.image = placeHolder;
            return;
        }
        
        [[SDImageCache sharedImageCache] queryDiskCacheForKey:thumbUrl done:^(UIImage *image, SDImageCacheType cacheType) {
            if (image) {
                imgV.image = image;
            } else {
                __weak UIImageView* tmpImage = imgV;
                
                imgV.image = placeHolder;
                
                [SDWebImageDownloader.sharedDownloader downloadImageWithURL:[NSURL URLWithString:thumbUrl] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize){
                    // progression tracking code
                } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished){
                    if (image && finished) {
                        dispatch_async( dispatch_get_main_queue(), ^{
                            [UIView transitionWithView:tmpImage duration:0.5f options:UIViewAnimationOptionTransitionCrossDissolve
                                            animations:^{
                                                tmpImage.image = image;
                                            } completion:nil];
                        });
                        [[SDImageCache sharedImageCache] storeImage:image forKey:thumbUrl toDisk:YES];
                    }
                }];
            }
        }];
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

+ (UIImage*)changeImage:(UIImage*)image toColor:(UIColor*)color{
    
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [color setFill];
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextClipToMask(context, CGRectMake(0, 0, image.size.width, image.size.height), [image CGImage]);
    CGContextFillRect(context, CGRectMake(0, 0, image.size.width, image.size.height));
    
    UIImage* coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return coloredImg;
}

+ (id)loadNibNamed:(NSString *)nibName ofClass:(Class)objClass {
    if (nibName && objClass) {
        NSArray *objects = [[NSBundle mainBundle] loadNibNamed:nibName
                                                         owner:nil
                                                       options:nil];
        for (id currentObject in objects ){
            if ([currentObject isKindOfClass:objClass])
                return currentObject;
        }
    }
    
    return nil;
}


+ (uint32_t) getReqIDFormPref {
    uint32_t reqId = (uint32_t)[SharePrefUtils getIntPreference:PREFKEY_REQ_ID];
    [SharePrefUtils writeIntPreference:PREFKEY_REQ_ID prefValue:++reqId];
    return reqId;
}

+(NSString*)getBusinessHours:(NSDictionary*)params {
    NSString *monStart = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_MONDAY_START]];
    NSString *monEnd = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_MONDAY_END]];
    NSString *mon = @"";
    if ([CoreStringUtils isEmpty:monStart] || [CoreStringUtils isEmpty:monEnd])
        mon = @"Closed";
    else
        mon = [NSString stringWithFormat:@"%@ - %@", monStart, monEnd];
    
    NSString *tueStart = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_TUESDAY_START]];
    NSString *tueEnd = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_TUESDAY_END]];
    NSString *tue = @"";
    if ([CoreStringUtils isEmpty:tueStart] || [CoreStringUtils isEmpty:tueEnd])
        tue = @"Closed";
    else
        tue = [NSString stringWithFormat:@"%@ - %@", tueStart, tueEnd];
    
    NSString *wedStart = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_WEDNESDAY_START]];
    NSString *wedEnd = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_WEDNESDAY_END]];
    NSString *wed = @"";
    if ([CoreStringUtils isEmpty:wedStart] || [CoreStringUtils isEmpty:wedEnd])
        wed = @"Closed";
    else
        wed = [NSString stringWithFormat:@"%@ - %@", wedStart, wedEnd];
    
    NSString *thuStart = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_THURSDAY_START]];
    NSString *thuEnd = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_THURSDAY_END]];
    NSString *thu = @"";
    if ([CoreStringUtils isEmpty:thuStart] || [CoreStringUtils isEmpty:thuEnd])
        thu = @"Closed";
    else
        thu = [NSString stringWithFormat:@"%@ - %@", thuStart, thuEnd];
    
    NSString *friStart = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_FRIDAY_START]];
    NSString *friEnd = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_FRIDAY_END]];
    NSString *fri = @"";
    if ([CoreStringUtils isEmpty:friStart] || [CoreStringUtils isEmpty:friEnd])
        fri = @"Closed";
    else
        fri = [NSString stringWithFormat:@"%@ - %@", friStart, friEnd];
    
    NSString *satStart = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_SATURDAY_START]];
    NSString *satEnd = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_SATURDAY_END]];
    NSString *sat = @"";
    if ([CoreStringUtils isEmpty:satStart] || [CoreStringUtils isEmpty:satEnd])
        sat = @"Closed";
    else
        sat = [NSString stringWithFormat:@"%@ - %@", satStart, satEnd];
    
    NSString *sunStart = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_SUNDAY_START]];
    NSString *sunEnd = [CoreStringUtils getStringDiffNull:[params objectForKey:REQUEST_SUNDAY_END]];
    NSString *sun = @"";
    if ([CoreStringUtils isEmpty:sunStart] || [CoreStringUtils isEmpty:sunEnd])
        sun = @"Closed";
    else
        sun = [NSString stringWithFormat:@"%@ - %@", sunStart, sunEnd];
    
    NSMutableArray *hours = [[NSMutableArray alloc] initWithObjects:mon, tue, wed, thu, fri, sat, sun, @"2003", nil];
    NSMutableArray *dataHours = [[NSMutableArray alloc] initWithObjects:@"Mon", @"Tue", @"Wed", @"Thu", @"Fri", @"Sat", @"Sun", @"2003", nil];
    
    NSMutableString *str, *tempStr;
    
    str = [NSMutableString stringWithString:@""];
    tempStr = [NSMutableString stringWithString:@"Mon"];
    NSInteger count  = 0;
    
    for (int i = 0; i < [hours count] - 1; i++) {
        //        if (i == 6) {
        //            if (count > 0)
        //                [str appendString:[NSString stringWithFormat:@"%@ : %@\n", tempStr, [hours objectAtIndex: i]]];
        //        } else {
        
        if ([[hours objectAtIndex:i] isEqualToString:[hours objectAtIndex:i + 1]]) {
            count++;
        } else {
            if (count == 0) {
                
            } else {
                [tempStr appendString:[NSString stringWithFormat:@" - %@", [dataHours objectAtIndex:i]]];
            }
            count = 0;
//            if(i + 1 == [hours count] - 1) {
//                [str appendString:[NSString stringWithFormat:@"<b>%@</b> : %@", tempStr, [hours objectAtIndex: i]]];
//            } else
//                [str appendString:[NSString stringWithFormat:@"<b>%@</b> : %@<br>", tempStr, [hours objectAtIndex: i]]];
            if(i + 1 == [hours count] - 1) {
                [str appendString:[NSString stringWithFormat:@"%@ : %@", tempStr, [hours objectAtIndex: i]]];
            } else
                [str appendString:[NSString stringWithFormat:@"%@ : %@<br>", tempStr, [hours objectAtIndex: i]]];
            tempStr = [NSMutableString stringWithString:[dataHours objectAtIndex:i + 1]];
        }
        //        }
    }
    
    return str;
}

+(UIImage *)fixImageRotaion:(UIImage*) image {
    
    CGImageRef imgRef = image.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    
    CGFloat scaleRatio = 1;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return imageCopy;
}

+ (void)copytext:(NSString*)text {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = text;
}

+ (BOOL)isDeviceBeforeIP5 {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString* machineName = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    if ([machineName containsString:@"iPhone1"] ||
        [machineName containsString:@"iPhone2"] ||
        [machineName containsString:@"iPhone3"] ||
        [machineName containsString:@"iPhone4"] ||
        [machineName containsString:@"iPad1"] ||
        [machineName containsString:@"iPad2"] ||
        [machineName containsString:@"iPad3"] ||
        [machineName containsString:@"iPod1"] ||
        [machineName containsString:@"iPod2"] ||
        [machineName containsString:@"iPod3"] ||
        [machineName containsString:@"iPod4"] ||
        [machineName containsString:@"iPod5"]) {
        return YES;
    } else {
        return NO;
    }
}

+ (double)distanceBetweenPoint1:(CGPoint)point1 andPoint2:(CGPoint)point2{
    return sqrt((point1.x - point2.x)*(point1.x - point2.x) + (point1.y - point2.y)*(point1.y - point2.y));
}


+ (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

+ (int)newMaximumNumber:(double)maximum {
    int count = 0;
    while (maximum >= 1) {
        maximum = maximum / 10;
        count = count + 1;
    }
    
    double newMaximum = floor(((maximum * 10) + 1));
    
    for (int i = 0; i < count - 1; i++) {
        newMaximum = newMaximum * 10;
    }
    
    return newMaximum;
}

@end

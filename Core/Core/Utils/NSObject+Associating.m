//
//  NSObject+Associating.m
//  Core
//
//  Created by Dragon on 2/23/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "NSObject+Associating.h"
#import <objc/runtime.h>

@implementation NSObject (Associating)

- (id)associatedObject
{
    return objc_getAssociatedObject(self, @selector(associatedObject));
}

- (void)setAssociatedObject:(id)associatedObject
{
    objc_setAssociatedObject(self,
                             @selector(associatedObject),
                             associatedObject,
                             OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end

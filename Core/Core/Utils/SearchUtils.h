//
//  SearchUtils.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/31/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchUtils : NSObject

+ (NSMutableArray*)filter:(NSString*)searchString originalList:(NSMutableArray*)originList;

@end

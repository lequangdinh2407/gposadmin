//
//  Helpers.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CoreStringUtils.h"
#import "Log.h"
#import "SharePrefUtils.h"
#import "SearchUtils.h"
#import "JSONUtils.h"
#import "DateUtils.h"

@interface Helpers : NSObject
+ (NSString*) getToken:(NSString*)basic;
+ (NSString*) getBasic:(NSString*)userName andPassword:(NSString*)password;
+ (BOOL)checkIfIphoneX;
+ (NSString*)convertToJson:(NSMutableDictionary*)params;
+ (BOOL)isContantGifExtension:(NSString*)link;
+ (BOOL)isContantImageLink:(NSString*)link;
+ (void)setViewCircle:(UIView*)view;
+ (void)giveBorderWithCornerRadious:(UIView*)view radius:(CGFloat)radius borderColor:(UIColor *)borderColor andBorderWidth:(CGFloat)borderWidth;
+ (void)giveBorderWithCornerRadious:(UIView*)view radius:(CGFloat)radius borderColor:(UIColor *)borderColor andBorderWidth:(CGFloat)borderWidth rect:(CGRect)rect;
+ (NSString*)getCurrentTimeStamp;
+ (uint64_t) getCurrentTimeStampUInt;
+ (BOOL)isValidList:(NSMutableArray*)list;
+ (BOOL)isValidArray:(NSArray*)list;
+ (BOOL)isNotNull:(NSObject*)data;
+ (NSString*)getUDID;
+ (NSString*)encryptDataSHA256:(NSString*)stringData key:(NSString*)privateKey;
+ (NSString*)encryptData:(NSString*)jsonStringData key:(NSString*)privateKey;
+ (NSString*)decryptData:(NSString*) jsonStringData key:(NSString*)privateKey;
+ (void)showToast:(NSString*)message;
+ (NSString*)formatNumber:(NSString*)mobileNumber;
+ (int)getLength:(NSString*)mobileNumber;
+ (void)logoutFB ;
+ (void)clearData;
+ (BOOL)isValidTaxCode:(NSString*)taxcode;
+ (uint32_t)getUIntDiffNull:(id)object;
+ (double)getDoubleDiffNull:(id)object;
+ (uint64_t)getULongLongDiffNull:(id)object;
+ (uint64_t) msgID;
+ (void) setMsgID:(uint64_t)val;
+ (NSArray*)getArrayDiffNull:(id)object;
+ (NSDictionary*)getDictDiffNull:(id)object;
+ (NSArray*)sortBarList:(NSArray*)list;
+ (NSString*)convertMeterToMiles:(id)object;
+ (bool)getBoolDiffNull:(id)object;
+ (NSMutableDictionary*)parseJsonToMap:(NSString*)jsonData;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (BOOL)isDateToday:(long)timestamp;
+ (NSString*)convertMeterToDistance:(id)object;
+ (UIImage*)changeImage:(UIImage*)image toColor:(UIColor*)color;
+ (id)loadNibNamed:(NSString *)nibName ofClass:(Class)objClass;

+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize keepRatio:(BOOL)isRatio;
+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize;
+(UIImage *)setBackgroundImageByColor:(UIColor *)backgroundColor withFrame:(CGRect )rect cornerRadius:(float)radius;

+ (void)loadMapInView:(UIImageView*)view withLon:(double)lon lat:(double)lat mapWidth:(float)width mapHeight:(float)height;
+ (void)gotoMapAtAddress:(NSString*)address city:(NSString*)city state:(NSString*)state country:(NSString*)country;

+ (NSString*)replacePriceWithSmall:(NSString*)price;
+ (NSString*)replacePriceWithCurrency:(NSString*)price;
+ (NSString*)convertThousandToKWithSpaces:(int)count;
+ (NSString*)convertThousandToK:(int)count;
+ (NSString*)convertMeterToKm:(double)meters;
+ (BOOL)isValidEmail:(NSString *)email;
+ (UIImage*)getImageCache:(NSString*)url size:(NSInteger)size;
+ (UIImage*)getFullImageCache:(NSString*)url;
+ (NSString*)getThumbnailUrl:(NSString*)url size:(NSInteger)size;
+ (NSString*)addCommasToDouble:(double)doubleNum;
+ (NSString*)formatCurrency:(double)doubleNum;
+ (NSString*)addCommasToInt:(int)intNum;
/*
 * Image loader
 */
+ (void)loadImage:(UIImageView*)imgView withLink:(NSString*)link;
+ (void)loadImage:(UIImageView*)imgV placeHolder:(UIImage*)placeHolder withUrl:(NSString*)link;
+ (void)loadImage:(UIImageView*)imgV placeHolder:(UIImage*)placeHolder withUrl:(NSString*)link thumbSize:(int)size;
+ (void)loadImage:(UIImageView*)imgV placeHolder:(UIImage*)placeHolder withUrl:(NSString*)link isScrolling:(BOOL)isScrolling;

+ (uint32_t) getReqIDFormPref;
+ (NSString*)getBusinessHours:(NSDictionary*)params;
+ (CGSize)calculateContentSize:(NSString*)content ContainerSize:(CGSize)size font:(UIFont*)textFont;
+ (CGSize)calculateHeightText:(NSString*)text width:(CGFloat)width font:(UIFont*)font;
+ (CGSize)calculateWidthText:(NSString*)text height:(CGFloat)height font:(UIFont*)font;
+ (CGSize)calculateHeightAttributedText:(NSAttributedString*)text width:(CGFloat)width;
+ (CGSize)calculateWidthAttributedText:(NSAttributedString*)text height:(CGFloat)height;
+ (UIImage *)fixImageRotaion:(UIImage*)image;
+ (void)copytext:(NSString*)text;
+ (BOOL)isDeviceBeforeIP5;
+ (double)distanceBetweenPoint1:(CGPoint)point1 andPoint2:(CGPoint)point2;
+ (UIImage *) imageWithView:(UIView *)view;

+ (int)newMaximumNumber:(double)maximum;
@end

//
//  CoreConstants.h
//  Core
//
//  Created by ToanPham on 2/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//


/**
 * Command headers Constants
 */
static NSString* const HDR_TYPE = @"hdr_type";
static NSString* const HDR_USER_AGENT = @"hdr_userAgent";
static NSString* const HDR_REQ_ID = @"hdr_reqID";
static NSString* const HDR_CMD = @"hdr_cmd";
static NSString* const HDR_CMD_VERSION = @"hdr_cmdVersion";
static NSString* const HDR_SRC_ID = @"hdr_srcID";
static NSString* const HDR_DEST_ID = @"hdr_destID";
static NSString* const HDR_USER_ID = @"hdr_userID";
static NSString* const HDR_LIVE_ID = @"hdr_liveID";
static NSString* const HDR_FEED_ID = @"hdr_feedID";
static NSString* const HDR_ERROR_CODE = @"hdr_errorCode";
static NSString* const HDR_SESSION_ID = @"hdr_sessionID";
static NSString* const HDR_CONTENT_LENGTH = @"hdr_contentLength";
static NSString* const HDR_CONTENT = @"hdr_content";


/**
 * Notification Key Constants
 */
static NSString* const NOTIFICATION_KICKOUT = @"kickOut";
static NSString* const NOTIFICATION_WILL_SHOW_SLIDING_MENU = @"WillShowMenu";
static NSString* const NOTIFICATION_WILL_HIDE_SLIDING_MENU = @"WillHideMenu";
static NSString* const NOTIFICATION_CLEAR_MEM_DONE = @"notification_clear_mem_done";
static NSString* const NOTIFICATION_LOGIN = @"reload_controller_after_login";
static NSString* const NOTIFICATION_LOGOUT = @"reload_controller_after_logout";
static NSString* const NOTIFICATION_CHECKOUT = @"reload_controller_after_checkout";
static NSString* const NOTIFICATION_NETWORK_AVAILABLE = @"reload_network_available";

static NSString* const NOTIFICATION_CONNECT_SOCKET_SUCCESS = @"connect_socket_success";
static NSString* const NOTIFICATION_CONNECT_COMMENT_SOCKET = @"connect_comment_socket";
static NSString* const NOTIFICATION_CONNECT_COMMENT_SOCKET_SUCCESS = @"connect_comment_socket_success";
static NSString* const NOTIFICATION_CONNECT_STOMP = @"connect_stomp";
static NSString* const NOTIFICATION_CONNECT_STOMP_SUCCESS = @"connect_stomp_success";
static NSString* const NOTIFICATION_CLOSE_COMMENT_VIEW = @"close_comment_view";
static NSString* const NOTIFICATION_DELETE_FEED = @"delete_feed";
static NSString* const NOTIFICATION_CHANGE_USER_INFO = @"change_userinfo";

static NSString* const NOTIFICATION_GET_NEWS_NEW = @"notification_get_news_new";
static NSString* const NOTIFICATION_NEW_APNS = @"notification_apple_push";

static NSString* const NOTIFICATION_DID_BECOME_ACTIVE = @"notification_did_become_active";

static NSString* const NOTIFICATION_APP_LAUNCHING_FINISH = @"notification_launching_finish";
static NSString* const NOTIFICATION_APP_REFRESHED_TOKEN_PNS = @"notification_app_refreshed_token_pns";


static NSString* const NOTIFICATION_MESSAGE = @"1";
static NSString* const NOTIFICATION_WEB = @"2";
static NSString* const NOTIFICATION_LANDSCAPING = @"3";
static NSString* const NOTIFICATION_SOCIAL = @"4";
static NSString* const NOTIFICATION_PROMOTION = @"5";
static NSString* const NOTIFICATION_ACCOUNT = @"6";
static NSString* const NOTIFICATION_SCHEDULE = @"7";

/**
 * Pref Key Constants
 */
static NSString* const PREFNAME_ACCOUNT_INFO = @"prefname_account_info";
static NSString* const PREFNAME_APP_INFO = @"prefname_app_info";

static NSString* const PREFKEY_ACCESSTOKEN = @"prefkey_accesstoken";

static NSString* const PREFKEY_REGISTER_INFO = @"prefkey_reg_info";


static NSString* const PREFKEY_UPDATE_TIME = @"prefkey_update_time";
static NSString* const PREFKEY_APP_VERSION = @"prefkey_app_version";
static NSString* const PREFKEY_TOKEN = @"prefkey_token";
static NSString* const PREFKEY_CURRENT_TOKEN = @"prefkey_current_token";
static NSString* const PREFKEY_ACCOUNT_LOGIN = @"prefkey_account_login";
static NSString* const PREFKEY_MESSAGE_SOUND_ENABLE = @"prefkey_message_sound_enable";
static NSString* const PREFKEY_LOGIN_TYPE = @"prefkey_login_type";


static NSString* const PREFKEY_NOTI_COUNT = @"prefkey_noti_count";


static NSString* const PREFKEY_HOME_GARDEN_ID = @"prefkey_home_garden_id";
static NSString* const PREFKEY_USER_ENTITY = @"prefkey_user_entity";


static NSString* const PREFKEY_COUNTRY_CODE_LOGIN = @"prefkey_country_code_login";
static NSString* const PREFKEY_COUNTRY_NAME_LOGIN = @"prefkey_country_name_login";
static NSString* const PREFKEY_COUNTRY_PHONE_LOGIN = @"prefkey_country_phone_login";
static NSString* const PREFKEY_VIDEO_LANGUAGURE_TYPE = @"prefkey_video_languagure_type";

static NSString* const PREFKEY_DISTANCE_UNIT = @"prefkey_distance_unit";
static NSString* const PREFKEY_CURRENCY = @"prefkey_currency";
static NSString* const PREFKEY_AVATAR_PATH = @"prefkey_avatar_path";
static NSString* const PREFKEY_HIDDEN_FLAG = @"prefkey_hidden_flag";
static NSString* const PREFKEY_LOGIN_FLAG = @"prefkey_login_flag";

static NSString* const PREFKEY_REQ_ID = @"prefkey_req_id";

static NSString* const PREFKEY_CONFIG_IMAGE_LOADING = @"prefkey_config_image_loading";

static NSString* const PREFKEY_CONFIG_API_URL = @"prefkey_config_api_url";
static NSString* const PREFKEY_CONFIG_USERNAME = @"prefkey_config_username";
static NSString* const PREFKEY_CONFIG_PASSWORD = @"prefkey_config_password";
static NSString* const PREFKEY_CONFIG_STOREID = @"prefkey_config_storeId";
static NSString* const PREFKEY_CONFIG_STORE_ID = @"prefkey_config_Id";
static NSString* const PREFKEY_CONFIG_STORENAME = @"prefkey_config_store_name";

static NSString* const PREFKEY_SOCKET = @"prefkey_socket";
static NSString* const PREFKEY_UPLOAD_SOCKET = @"prefkey_upload_socket";


/**
 * Database String Constants
 */
static NSString* const DB_USER_ENTITY = @"db_user_entity";
static NSString* const DB_SOCIAL_PROFILE = @"db_social_profile";
static NSString* const DB_TYPE_ACTION = @"db_type_action";
static NSString* const DB_FLAG_ACTION = @"flag_action";
static NSString* const DB_SESSION_KEY = @"session_key";
static NSString* const DB_REFRESH_TOKEN = @"refresh_token";
static NSString* const DB_DOMAIN = @"domain";
static NSString* const DB_ACCOUNT_ENTITY = @"account_entity";
static NSString* const DB_SOCKET = @"socket";
static NSString* const DB_UPLOADSOCKET = @"upload_socket";
static NSString* const DB_REQ_ID = @"req_id";
static NSString* const DB_CREATE_TIME = @"create_time";
static NSString* const DB_SEEN_TIME = @"seen_time";
static NSString* const DB_MSG_ID = @"msg_id";
static NSString* const DB_STATE = @"state";
static NSString* const DB_CONTENT = @"content";
static NSString* const DB_BAR_ENTITY = @"db_bar_entity";
static NSString* const DB_BAR_ID = @"bar_id";
static NSString* const DB_ADDRESSES_BOOK = @"db_addresses_book";
static NSString* const DB_CONVERSATION_ENTITY = @"db_conversation_entity";
static NSString* const DB_MESSAGE_ENTITY = @"db_message_entity";
static NSString* const DB_FEEDS = @"db_feeds";

/**
 * Country Default
 */
static NSString* const COUNTRY_NAME_DEFAULT = @"United States";
static NSString* const COUNTRY_CODE_DEFAULT = @"US";
static NSString* const COUNTRY_PHONE_DEFAULT = @"+1";

/**
 * State Default
 */
static NSString* const STATE_NAME_DEFAULT = @"All States";
static NSString* const STATE_CODE_DEFAULT = @"";

/**
 * Show Case
 */

static NSString* const SHOWCASE_SHARE = @"Share Invites Code";
static NSString* const SHOWCASE_MENU = @"Menu Left";
static NSString* const SHOWCASE_SOCIAL = @"Social";
static NSString* const SHOWCASE_SEARCH = @"Search";
static NSString* const SHOWCASE_HOME = @"Home";
static NSString* const SHOWCASE_RECENTLY = @"Recent Garden";
static NSString* const SHOWCASE_POST_FEED = @"Post Feed";
static NSString* const SHOWCASE_SEND_AI = @"Send AI";
static NSString* const SHOWCASE_UNDO = @"Undo";
static NSString* const SHOWCASE_ADD_MORE = @"Add More";
static NSString* const SHOWCASE_SUBTRACT = @"Subtract";
static NSString* const SHOWCASE_ROTATE = @"Rotate";
static NSString* const SHOWCASE_CLEAR = @"Clear";
static NSString* const SHOWCASE_QUESTION = @"Question";
static NSString* const SHOWCASE_NEXT = @"Next";
static NSString* const SHOWCASE_POSITION = @"Position";
static NSString* const SHOWCASE_UNLOCK = @"Unlock";
static NSString* const SHOWCASE_AREA = @"Area";
static NSString* const SHOWCASE_ESTIMATED_PRICE = @"Estimated Price";
static NSString* const SHOWCASE_SCHEDULE = @"Schedule";
/**
 *
 */
static NSString* const DEFAULT_CURRENCY = @"$";


/* Stomp */
static NSString* const APP_DEMAND_DEST = @"/app/demand-client";
static NSString* const USER_DEMAND_DEST = @"/user/queue/demand-client";
static NSString* const APP_DEMAND_STATUS_DEST = @"/app/demand-client-status";
static NSString* const APP_DEMAND_UPDATE_STATUS_DEST = @"/app/demand-client-status-update";
static NSString* const ACCEPT_REESTIMATE_DEST = @"/app/demand-client-reestimate-accept";

static NSString* const DATA_BODY_MESSAGE = @"data";



//
//  DateUtils.h
//  spabees
//
//  Created by vu van long on 1/5/15.
//  Copyright (c) 2015 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef SecondsPerDay
#define SecondsPerDay 86400
#endif

@interface DateUtils : NSObject
+ (NSUInteger)getTotalDayOfMonth:(int)month;
+ (NSString*)getCurrentDate;
+ (NSString*)getCurrentMoment;
+ (double)getCurrentTimestamp;
+ (int)getCurrentYear;
+ (NSString*)parseTimeStampToDate:(id)data;
+ (NSString*)parseTimeStampToOnlyDate:(id)data;
+ (NSString*)parseTimeStampToHour:(id)data;

+ (NSString*)convertDateToTimestamp:(NSString*)strDate;
+ (NSString*)getNotificationTime:(double)data;
+ (NSString*)getMessageTime:(double)data;
+ (NSDate*)getDateFromString:(NSString*)string format:(NSString*)format;
/**
 *  This method returns the number of days between this date and the given date.
 */
+ (NSUInteger)daysBetween:(NSDate *)date1 andDate:(NSDate*)date2;

/**
 *  This method compares the dates without time components.
 */
+ (NSComparisonResult)timelessCompare:(NSDate *)date1 andDate:(NSDate*)date2;

/*
 * This method returns a new date with the time set to 12:00 AM midnight local time.
 */
+ (NSDate *)dateWithoutTimeComponents:(NSDate *)date;
+ (NSString*)getHistoryMessageTime:(double)data;
+ (NSString*)getStringDate:(NSDate*)date withFormat:(NSString*)format;
+ (NSString*)getStringStartTime:(id)startTime endTime:(id)endTime;

@end

//
//  CoreConstants.h
//  Core
//
//  Created by ToanPham on 2/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//


/*
 * Notification type
 */
typedef enum : NSUInteger {
    MessageNotiType = 1,
    WebNotiType = 2,
    LanscapingNotiType = 3,
    SocialNotiType = 4,
    PromotionNotiType = 5,
    AccountNotiType = 6,
} NotificationType;

/*
 * Banner type
 */

// WELCOME(1, "Welcome"), WEB(2, "Web"), FREE_SERVICE(7, "Free Service"), SOCIAL(4, "Social"), BECOME_PROVIDER(6, "Become Provider")
typedef enum : NSUInteger {
    WelcomeBannerType = 1,
    WebBannerType = 2,
    FreeServiceBannerType = 7,
    SocialBannerType = 4,
    BecomeProviderBannerType = 6,
} BannerType;

/*
 * Feed status type
 */
typedef enum : NSUInteger {
    FeedPhotoType = 1,
    FeedLinkType = 2,
    FeedMessageType = 3,
} FeedType;

/*
 * order status define
 */
typedef enum : NSUInteger {
    OnHold = 1,
    Canceled = 2,
    PendingPayment = 3,
    PaymentReceived = 4,
    PreparingOrder = 5,
    Shipping = 6,
    Finished = 7,
} OrderStatus;

/*
 * Picker
 */
typedef enum : NSUInteger {
    NonePicker = 0,
    ListPicker = 1,
    TimePicker = 2,
    DatePicker = 3,
    ImagePicker = 4,
    VideoPicker = 5,
} PickerType;

/*
 * Dialog
 */
typedef enum : NSInteger {
    ADD_TO_CART = 0,
    BUY_NOW = 1,
} DialogType;

/*
 * Link Type
 */
typedef enum : NSInteger {
    FAQS_TYPE = 1,
    TERMS_OF_SERVICE_TYPE = 2,
    PRIVACY_POLICY_TYPE = 3,
    DELIVER_POLICY_TYPE = 4,
    PROMO_DETAIL_TYPE = 5
} LinkType;

/*
 * Emotion Type
 */
typedef enum : NSInteger {
    NotEmote = 0,
    EmoLike = 1,
    EmoLove = 2,
    EmoHaha = 3,
    EmoWow = 4,
    EmoAngry = 5,
    EmoSad = 6,
} EmotionType;

/*
 * Emotion Type
 */
typedef enum : NSInteger {
    NotEmoteComment = 0,
    EmoCommentLike = 1,
    EmoCommentDislike = 2,
} EmoteCommentType;

/*
 * Socket Comment Type
 */
typedef enum : NSInteger {
    CommentFeedType = 1,
    CommentProductType = 2,
    CommentBoothType = 3,
} CommentSocketType;

/*
 * Chat Type
 */
typedef enum : NSUInteger {
    UnknownMessageType = 0,
    TextMessage = 1,
    StickerMessage = 2,
    ImageMessage = 3,
} ChatContentType;

typedef enum : NSUInteger {
    UnknowComment = 0,
    TextComment = 1,
    ImageComment = 2,
    StickerComment = 3,
} CommentContentType;

/*
 * Feed comment type
 */

typedef enum : NSUInteger {
    FeedComment = 0,
    BoothComment = 1,
    ProductComment = 2,
    ReplyComment = 3,
} CommentType;

/*
 * Feed Comment Social Type
 */
typedef enum : NSInteger {
    LikeFeedSocialType = 1,
    CommentFeedSocialType = 2,
    LikeCommentSocialType = 3,
    ReplyCommentSocialType = 4,
    LikeReplySocialType = 5,
} FeedCommentSocialType;

/*
 * Feed Users Type
 */
typedef enum : NSInteger {
    CommentFeedUsersType = 1,
    LikeFeedUsersType = 2,
    ViewFeedUsersType = 3,
} FeedUsersType;

/*
 * SKU Type
 */
typedef enum : NSInteger {
    ProductSkuType = 1,
    ComboSkuType = 2,
    EduSkuType = 3,
    LocationSkuType = 4,
} SkuType;


/*
 * Suggest Type
 */
typedef enum : NSInteger {
    SuggestEventType = 0,
    SuggestBookType = 1,
    SuggestAddressType = 2,
} SuggestType;

/*
 * Suggest Type
 */
typedef enum : NSInteger {
    TSFeedObject = 2,
    TSSubFeedObject = 3,
    TSCommentObject = 4,
    TSUserObject = 5,
    TSInviteCodeObject = 14,
} TSObjectType;

/*
 * User Relationship Follow
 */
typedef enum : NSInteger {
    FollowTSObject = 1,
    UnfollowTSObject = 2,
} TSFollowType;

/*
 * User Relationship Follow
 */
typedef enum : NSInteger {
    HideFromUser = 1,
    UnhideFromUser = 0,
} TSHffuType;

/*
 * User Relationship Like
 */
typedef enum : NSInteger {
    DislikeTSObject = 0,
    LikeTSObject = 1,
} TSLikeType;

/*
 * React Type
 */
typedef enum : NSInteger {
    CommentReact = 1,
    LikeReact = 2,
    ViewReact = 3,
    LiveReact = 4,
    RatingReact = 5,
    ShareReact = 6,
    FollowerReact = 7,
    FollowReact = 8,
    HffuReact = 9,
} TSReactType;

/*
 * Event StatusType
 */

typedef enum : NSUInteger {
    EventDefaultStatus = 0,
    EventJustFinished = 1,
    EventOngoing = 2,
    EventUpcoming = 3,
} EventStatusType;



typedef enum : NSInteger {
    GetListDefaultFeeds = 1,
    GetListHotFeeds = 2,
    GetListFollowFeeds = 3,
    GetListMineFeeds = 4,
} GetListFeedsType;

typedef enum : NSInteger {
    SortBoothsDefault = 0,
    SortBoothsAZType = 1,
    SortBoothsZAType = 2,
    SortBoothsOnlineType = 3,
    SortBoothsDistanceType = 4,
} SortBoothType;

/**
 * Garden Type
 */
typedef enum : NSInteger {
    GardenDefaultType = 0,
    GardenHomeType = 1,
} GardenType;


/**
 * Auto Login
 */
typedef enum : NSInteger {
    ManualLogin = 0,
    AutoLogin = 1,
} AutoLoginType;

/**
 * Login Type
 */
static NSInteger const LOGIN_PHONE = 1;
static NSInteger const LOGIN_FB = 2;
static NSInteger const LOGIN_GG = 3;
static NSInteger const LOGIN_INSTAGRAM = 4;
/**
 * Distance Type
 */
static NSInteger const DISTANCE_METER = 1;
static NSInteger const DISTANCE_MILE = 0;

/**
 * Image Loading Type
 */
static NSInteger const IMAGE_LOADING_SMALL = 1;
static NSInteger const IMAGE_LOADING_NORMAL = 2;
static NSInteger const IMAGE_LOADING_LARGE = 3;


/**
 * Provider Status
 */
typedef enum : NSInteger {
    UNKNOWN = 0,
    HOMBER_CREATE = 1,
    PROVIDER_FINDING = 2,
    PROVIDER_FOUND = 3,
    PROVIDER_COMING = 4,
    PROVIDER_ARRIVED = 5,
    PROVIDER_WORKING = 6,
    PROVIDER_DONE = 7,
    SERVICE_FINISH = 8,
    REQUEST_CANCELED = 9,
    PROVIDER_NOT_FOUND = 10,
    PROVIDER_REESTIMATE = 11,
    HOMBER_PAYMENT_ERROR = 12,
    RATING_FINISH = 13
} ProviderStatusEnum;


/**
 * Estimate Status
 */
typedef enum : NSInteger {
    NONE = 0,
    ESTIMATING = 1,
    ACCEPTED_ESTIMATE = 2,
    DECLINED_ESTIMATE = 3
} EstimateStatusEnum;

/**
 * Become Provider Status
 */
typedef enum : NSInteger {
    BECOME_UNDEFINED = 0,
    BECOME_PENDING = 1,
    BECOME_QUALIFIED = 2,
    BECOME_DISQUALIFIED = 3,
    BECOME_PAYMENT_RECEIVED = 4,
    BECOME_APPROVED = 5,
    BECOME_REJECTED = 6,
    BECOME_PAYMENT_PENDING = 7,
} BecomeProviderStatusEnum;

/**
 * Schedule Status
 */
typedef enum : NSInteger {
    UNKNOWN_SCHEDULE = 0,
    CREATED_SCHEDULE = 1,
    BOOKED_SCHEDULE = 2,
    PROVIDER_NOT_FOUND_SCHEDULE = 3,
} ScheduleStatusEnum;

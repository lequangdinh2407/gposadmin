//
//  JSONUtils.h
//  Core
//
//  Created by ToanPham on 4/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONUtils : NSObject

//+ (NSString*)convertToJson:(NSMutableDictionary*)params;
+ (NSString*)convertToJson:(NSObject*)params;
+ (NSMutableDictionary*)parseJsonToMap:(NSString*)jsonData;
+ (NSMutableDictionary*)parseDataToMap:(NSData*)jsonData;
+ (NSObject*)parseDataToObject:(NSData*)jsonData;
+ (NSObject*)parseJsonToObject:(NSString*)jsonData;

@end
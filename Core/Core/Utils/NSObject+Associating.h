//
//  NSObject+Associating.h
//  Core
//
//  Created by Dragon on 2/23/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Associating)
@property (nonatomic, strong) id associatedObject;
@end

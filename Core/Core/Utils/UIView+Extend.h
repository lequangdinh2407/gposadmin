//
//  UIView+Extend.h
//  Core
//
//  Created by Dragon on 4/6/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extend)

- (void)addBorderColor:(UIColor*)color withHeight:(float)height;
- (void)addBorderColorNoBesides:(UIColor*)color withHeight:(float)height;
- (void)loadImage:(UIImageView*)imgV placeHolder:(UIImage*)placeHolder withUrl:(NSString*)link;
- (void)loadImage:(UIImageView*)imgV placeHolder:(UIImage*)placeHolder withUrl:(NSString*)link isScrolling:(BOOL)isScrolling;
- (void)addShadow;
- (void)addNonRoundedShadow;
@end

//
//  NSString+phoneNumber.h
//  Core
//
//  Created by Toan Pham Thanh on 1/9/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (phoneNumber)
-(NSString*) phoneNumber;
@end

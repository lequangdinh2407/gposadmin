//
//  CoreConstants.h
//  Core
//
//  Created by ToanPham on 2/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

/**
 * Response Key Constants
 */
static NSString* const RESPONSE_ERROR_CODE = @"code";
static NSString* const RESPONSE_DATA = @"data";
static NSString* const RESPONSE_PAGING = @"paging";
static NSString* const RESPONSE_NEXT = @"next";
static NSString* const RESPONSE_PREVIOUS = @"previous";
static NSString* const RESPONSE_USERINFO = @"userInfo";
static NSString* const RESPONSE_PROFILE = @"profile";
static NSString* const RESPONSE_SOCIAL_PROFILE = @"socialProfile";
static NSString* const RESPONSE_SESSIONKEY = @"sessionKey";
static NSString* const RESPONSE_API_SOCKET = @"apiSocket";
static NSString* const RESPONSE_TOKEN = @"token";
static NSString* const RESPONSE_REFRESH_TOKEN = @"refreshToken";
static NSString* const RESPONSE_DOMAIN = @"domain";
static NSString* const RESPONSE_TYPES = @"types";
static NSString* const RESPONSE_CATEGORIES = @"categories";
static NSString* const RESPONSE_UPDATE_TIME = @"updateTime";
static NSString* const RESPONSE_IS_LIKED = @"isLiked";
static NSString* const RESPONSE_IS_USED = @"isUsed";
static NSString* const RESPONSE_LIKE = @"like";
static NSString* const RESPONSE_VIEW = @"view";
static NSString* const RESPONSE_VIEWS = @"views";
static NSString* const RESPONSE_IS_VIEWED = @"isViewed";
static NSString* const RESPONSE_COUNTRY = @"country";
static NSString* const RESPONSE_COUNTRIES = @"countries";
static NSString* const RESPONSE_ZIPCODE = @"zipCode";
static NSString* const RESPONSE_STATE = @"state";
static NSString* const RESPONSE_CITY = @"city";
static NSString* const RESPONSE_STATES = @"states";
static NSString* const RESPONSE_CITIES = @"cities";
static NSString* const RESPONSE_AVATAR = @"avatar";
static NSString* const RESPONSE_NEW = @"news";
static NSString* const RESPONSE_FLAG = @"flag";
static NSString* const RESPONSE_DISPLAY_NAME = @"displayName";
static NSString* const RESPONSE_EVENT = @"event";
static NSString* const RESPONSE_EVENTS = @"events";
static NSString* const RESPONSE_TALK = @"talk";
static NSString* const RESPONSE_TALKS = @"talks";
static NSString* const RESPONSE_ROUTES = @"routes";
static NSString* const RESPONSE_LOCATION = @"location";
static NSString* const RESPONSE_X = @"x";
static NSString* const RESPONSE_Y = @"y";
static NSString* const RESPONSE_FLOOR_ID = @"floorId";
static NSString* const RESPONSE_BEING_ONLINE = @"beingOnline";
static NSString* const RESPONSE_BORDER = @"border";
static NSString* const RESPONSE_SALES = @"sales";
static NSString* const RESPONSE_TOTAL_ORDER = @"totalOrder";
static NSString* const RESPONSE_TOTAL_SALE = @"totalSale";

/* LuvKonect */
static NSString* const RESPONSE_SOCKETS = @"sockets";
static NSString* const RESPONSE_UPLOADSOCKETS = @"uploadSockets";
static NSString* const RESPONSE_UPLOADFILE_ID = @"id";
static NSString* const RESPONSE_UPLOADFILE_PART = @"part";
static NSString* const RESPONSE_UPLOADFILE_URL = @"url";
static NSString* const RESPONSE_MSG = @"msg";

/* Loop */
static NSString* const RESPONSE_HAS_MORE = @"hasMore";
static NSString* const RESPONSE_IS_APPEND = @"appended";
static NSString* const RESPONSE_NEW_COUNT = @"newCount";
static NSString* const RESPONSE_KEYWORDS = @"keywords";
static NSString* const RESPONSE_SUGGEST = @"suggest";
static NSString* const RESPONSE_ADDRESS_LIST = @"addressList";
static NSString* const RESPONSE_ADDRESS_ENTITY = @"addressEntity";
static NSString* const RESPONSE_ANGLE = @"angle";
static NSString* const RESPONSE_ZOOM_LEVEL = @"zoomLevel";
static NSString* const RESPONSE_MAP_IMAGE = @"mapImage";

/* BConnect */
static NSString* const RESPONSE_SKUS = @"skus";
static NSString* const RESPONSE_DEFAULT_SKUID = @"defaultSkuId";
static NSString* const RESPONSE_PRODUCT = @"product";
static NSString* const RESPONSE_PRODUCTS = @"products";
static NSString* const RESPONSE_POINT = @"point";
static NSString* const RESPONSE_WISHLIST = @"wishList";
static NSString* const RESPONSE_NEWS = @"news";
static NSString* const RESPONSE_BANNERS = @"banners";
static NSString* const RESPONSE_FEED = @"feed";
static NSString* const RESPONSE_SUBFEED = @"subfeed";
static NSString* const RESPONSE_FEEDS_LIST = @"feeds";
static NSString* const RESPONSE_COMMENT = @"comment";
static NSString* const RESPONSE_SUBCOMMENT = @"subcomment";
static NSString* const RESPONSE_COMMENTS_LIST = @"comments";
static NSString* const RESPONSE_FEED_LINK = @"feedLink";
static NSString* const RESPONSE_PHOTO_ID = @"photoId";
static NSString* const RESPONSE_PHOTO = @"photo";
static NSString* const RESPONSE_ID = @"id";
static NSString* const RESPONSE_TOTAL_COMMENT = @"totalComment";
static NSString* const RESPONSE_TOTAL_CHILD = @"childs";
static NSString* const RESPONSE_FAKE_COMMENT = @"fakeComment";
static NSString* const RESPONSE_NEW_COMMENT = @"newComment";
static NSString* const RESPONSE_RATING_POINT = @"ratingPoint";
static NSString* const RESPONSE_TOTAL_RATING = @"totalRating";
static NSString* const RESPONSE_TOTAL_NEW = @"totalNew";
static NSString* const RESPONSE_TOTAL_LIKE = @"totalLike";
static NSString* const RESPONSE_TOTAL_LIKES = @"likes";
static NSString* const RESPONSE_TOTAL_FOLLOW = @"totalFollower";

static NSString* const RESPONSE_TOTAL_CLIENT = @"totalClient";
static NSString* const RESPONSE_URL = @"url";
static NSString* const RESPONSE_USERS = @"users";
static NSString* const RESPONSE_USER = @"user";
static NSString* const RESPONSE_TOTAL_USER = @"totalUser";
static NSString* const RESPONSE_NUMFEED = @"numFeed";
static NSString* const RESPONSE_NUMFOLLOW = @"numFollow";
static NSString* const RESPONSE_NUMFOLLOWED = @"numFollowed";
static NSString* const RESPONSE_FOLLOWED = @"followed";
static NSString* const RESPONSE_SET_USERID = @"setUserId";
static NSString* const RESPONSE_LIST_USERS = @"listUser";
static NSString* const RESPONSE_UNFOLLOW_LIST = @"unfollowList";
static NSString* const RESPONSE_TOTAL_VIEW = @"totalView";
static NSString* const RESPONSE_LIVE_ID = @"liveId";
static NSString* const RESPONSE_SOCIAL_STREAMS = @"socialStreams";
static NSString* const RESPONSE_LAST_CHILD = @"lstChild";
static NSString* const RESPONSE_FLOORS = @"floors";
static NSString* const RESPONSE_BOOTHS = @"booths";
static NSString* const RESPONSE_BOOTH = @"booth";
static NSString* const RESPONSE_ADDRESS = @"address";
static NSString* const RESPONSE_SERVICE = @"service";
static NSString* const RESPONSE_SERVICES = @"services";
static NSString* const RESPONSE_GARDEN = @"garden";
static NSString* const RESPONSE_MAP = @"map";
static NSString* const RESPONSE_CARD = @"card";
static NSString* const RESPONSE_PROVIDER = @"provider";
static NSString* const RESPONSE_ESTIMATE_STATUS = @"estimateStatus";
static NSString* const RESPONSE_STATUS = @"status";
static NSString* const RESPONSE_COORDINATE = @"coordinate";
static NSString* const RESPONSE_COMMON_SERVICES = @"commonServices";
static NSString* const RESPONSE_CUSTOM_SERVICES = @"customServices";
static NSString* const RESPONSE_PHOTOS = @"photos";
static NSString* const RESPONSE_LANDSCAPING_HISTORY = @"landscapingHistory";
static NSString* const RESPONSE_REQUESTED_PHOTOS = @"requestedPhotos";
static NSString* const RESPONSE_PROVIDER_REGISTER_TIME = @"providerRegisterTime";
static NSString* const RESPONSE_SERVE_TOTAL = @"serveTotal";
static NSString* const RESPONSE_APP_URL = @"appUrl";
static NSString* const RESPONSE_STEP5_CONTENT = @"step5Content";
static NSString* const RESPONSE_STEP7_CONTENT = @"step7Content";
static NSString* const RESPONSE_DATE = @"date";
static NSString* const RESPONSE_SERVICE_PRICE = @"servicePrice";

static NSString* const RESPONSE_QR = @"qr";

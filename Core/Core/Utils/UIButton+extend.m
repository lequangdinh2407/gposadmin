//
//  UIButton+extend.m
//  Core
//
//  Created by Dragon on 4/20/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "UIButton+extend.h"

@implementation UIButton (extend)
- (void)enableUI:(BOOL)isEnable{
    self.alpha = isEnable ? 1.0 : 0.5;
    [self setEnabled:isEnable];
}
@end

//
//  SocketCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 3/9/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "StompFrame.h"
#import "Helpers.h"

@implementation StompFrame

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.command = @"";
    self.headersMap = [[NSMutableDictionary alloc] init];
    self.body = [[NSMutableDictionary alloc] init];
    
    return self;
}

- (NSString*)toString {
    NSMutableString* frame = [[NSMutableString alloc] init];
    [frame appendString:self.command];
    [frame appendString:@"\r\n"];   
    NSArray* keys = self.headersMap.allKeys;
    
    for (int i = 0; i < keys.count; i++) {
        NSString* key = [keys objectAtIndex:i];
        NSString* value = [self.headersMap objectForKey:key];
        [frame appendString:[NSString stringWithFormat:@"%@:%@\r\n", key, value]];
    }
    
    NSString* bodyString = @"";
    if (self.body.count > 0) {
        bodyString = [JSONUtils convertToJson:self.body];
        [frame appendString:[NSString stringWithFormat:@"%@:%@\r\n", CONTENT_TYPE_HDR, @"application/json;charset=UTF-8"]];
        NSData *bytes = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
        [frame appendString:[NSString stringWithFormat:@"%@:%lu\r\n", CONTENT_LENGHT_HDR, (unsigned long)bytes.length]];
    }
    [frame appendString:@"\r\n"];
    [frame appendString:bodyString];
    [frame appendString:@"\0"];
    
    return frame;
}

- (NSObject*)initWithCmd:(NSString*)cmd headers:(NSDictionary*)headers body:(NSDictionary*)body {
    self = [self init];
    
    if (![CoreStringUtils isEmpty:cmd])
        self.command = [NSString stringWithString:cmd];
    
    if ([Helpers isNotNull:headers])
        self.headersMap = [NSMutableDictionary dictionaryWithDictionary:headers];
    
    if ([Helpers isNotNull:body])
        self.body = [NSMutableDictionary dictionaryWithDictionary:body];
    
    return self;
}

@end

//
//  SocketCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 3/9/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString* const CONNECT_CMD = @"CONNECT";
static NSString* const STOMP_CONNECTED_CMD = @"CONNECTED";
static NSString* const DISCONNECT = @"DISCONNECT";
static NSString* const SEND_CMD = @"SEND";
static NSString* const UNSUBSCRIBE_CMD = @"UNSUBSCRIBE";
static NSString* const SUBSCRIBE_CMD = @"SUBSCRIBE";
static NSString* const RECEIPT_CMD = @"RECEIPT";
static NSString* const MESSAGE_CMD = @"MESSAGE";

#pragma mark - Header
static NSString* const DESTINATION_HDR = @"destination";
static NSString* const ID_HDR = @"id";
static NSString* const CONTENT_LENGHT_HDR = @"content-length";
static NSString* const CONTENT_TYPE_HDR = @"content-type";
static NSString* const RECEIPT_HDR = @"receipt";
static NSString* const RECEIPT_ID_HDR = @"receipt-id";
static NSString* const SUBSCRIPTION_HDR = @"subscription";
static NSString* const MESSAGE_ID = @"message-id";


#pragma mark - Body
static NSString* const LANDSCAPING_BODY = @"landscapingHistoryId";

@interface StompFrame : NSObject

@property (nonatomic, strong) NSString* command;
@property (nonatomic, strong) NSMutableDictionary* headersMap;
@property (nonatomic, strong) NSMutableDictionary* body;

- (NSString*)toString;
- (NSObject*)initWithCmd:(NSString*)cmd headers:(NSDictionary*)headers body:(NSDictionary*)body;

@end

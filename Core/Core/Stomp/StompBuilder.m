//
//  CommandBuilder.m
//  luvkonectcore
//
//  Created by ToanPham on 3/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "StompBuilder.h"
#import "ReceiptFrame.h"
#import "CoreConstants.h"
#import "Helpers.h"
#import "MessageFrame.h"

@implementation StompBuilder

+ (instancetype)sharedClient {
    static StompBuilder * volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (StompFrame*)createFrame:(NSString*)cmd headers:(NSDictionary*)headers body:(NSDictionary*)body {
    StompFrame* item = nil;
    if ([CoreStringUtils isEmpty:cmd]) {
        return item;
    }
    
    if ([cmd isEqualToString:RECEIPT_CMD]) {
        return [[ReceiptFrame alloc] initWithHeaders:headers body:body];
    } else if ([cmd isEqualToString:MESSAGE_CMD]) {
        return [[MessageFrame alloc] initWithHeaders:headers body:body];
    } else {
        return [[StompFrame alloc] initWithCmd:cmd headers:headers body:body];
    }
}

- (StompFrame*)parseData:(NSString*)data {
    NSString* command = @"";
    NSString* header = @"";
    NSString* body = @"";
    
    NSArray* components = [data componentsSeparatedByString:@"\n\n"];
    
    if (components.count > 1) {
        header = components[0];
        body = components[1];
        body = [body substringToIndex:[body length] - 1];

    } else {
        header = components[0];
    }
    
    components = [header componentsSeparatedByString:@"\n"];
    
    NSMutableDictionary* headerMap = [[NSMutableDictionary alloc] init];
    
    if (components.count > 0) {
        command = components[0];
        
        for (int i = 1; i < components.count; i++) {
            NSArray* headerLine = [components[i] componentsSeparatedByString:@":"];
            if (headerLine.count > 1) {
                NSString* headerName = headerLine[0];
                NSString* headerValue = headerLine[1];
                [headerMap setValue:headerValue forKey:headerName];
            }
        }
    }
    
    NSDictionary* bodyMap = [JSONUtils parseJsonToMap:body];
    
    if (![CoreStringUtils isEmpty:command])
        return [self createFrame:command headers:headerMap body:bodyMap];
    else
        return nil;
}

- (StompFrame*)parseDataResponse:(NSString*)data {
    StompFrame* command = [self parseData:data];
    return command;
}

@end

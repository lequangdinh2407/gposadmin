//
//  CommandBuilder.h
//  luvkonectcore
//
//  Created by ToanPham on 3/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StompFrame.h"

@interface StompBuilder : NSObject

+ (instancetype)sharedClient;
- (StompFrame*)createFrame:(NSString*)cmd headers:(NSDictionary*)headers body:(NSDictionary*)body;
- (StompFrame*)parseDataResponse:(NSString*)data;

@end

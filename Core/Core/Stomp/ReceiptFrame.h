//
//  SocketCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 3/9/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StompFrame.h"

@interface ReceiptFrame : StompFrame

@property (nonatomic, strong) NSString* receiptId;

- (NSObject*)initWithHeaders:(NSDictionary*)headers body:(NSDictionary*)body;

@end

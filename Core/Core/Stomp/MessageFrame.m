//
//  SocketCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 3/9/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "MessageFrame.h"
#import "Helpers.h"

@implementation MessageFrame

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.subscription = @"";
    self.messageId = @"";
    self.destination = @"";

    return self;
}

- (NSObject*)initWithHeaders:(NSDictionary*)headers body:(NSDictionary*)body {
    self = [super initWithCmd:MESSAGE_CMD headers:headers body:body];
    
    if ([Helpers isNotNull:headers]) {
        self.subscription = [CoreStringUtils getStringDiffNull:[headers objectForKey:SUBSCRIPTION_HDR]];
        self.messageId = [CoreStringUtils getStringDiffNull:[headers objectForKey:MESSAGE_ID]];
        self.destination = [CoreStringUtils getStringDiffNull:[headers objectForKey:DESTINATION_HDR]];
    }
    return self;
}

@end

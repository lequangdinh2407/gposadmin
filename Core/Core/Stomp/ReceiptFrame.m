//
//  SocketCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 3/9/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "ReceiptFrame.h"
#import "Helpers.h"

@implementation ReceiptFrame

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.receiptId = @"";
    
    return self;
}

- (NSObject*)initWithHeaders:(NSDictionary*)headers body:(NSDictionary*)body {
    self = [super initWithCmd:RECEIPT_CMD headers:headers body:body];
    
    if ([Helpers isNotNull:headers]) {
        self.receiptId = [CoreStringUtils getStringDiffNull:[headers objectForKey:RECEIPT_ID_HDR]];
        if ([CoreStringUtils isEmpty:self.receiptId]) {
            self.receiptId = [CoreStringUtils getStringDiffNull:[headers objectForKey:RECEIPT_HDR]];    
        }
    }
    return self;
}

@end

//
//  SocketCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 3/9/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSInteger const PING_CMD = 10;
static NSInteger const KICKOUT_CMD = 12;
static NSInteger const DISCONNECT_NOTIFY_CMD = 13;
static NSInteger const TYPING_CMD = 4;
static NSInteger const CONNECTED_CMD = 11;
static NSInteger const PINGNEARBY_CMD = 16;
static NSInteger const CHAT_CMD = 140;
static NSInteger const CHATACK_CMD = 141;
static NSInteger const UPDATEMESSAGESEEN_CMD = 102;
static NSInteger const UPDATEMESSAGESEENACK_CMD = 103;
static NSInteger const GETOFFLINEMESSAGE_CMD = 104;
static NSInteger const GET_MSG_STATUS_CMD = 105;
static NSInteger const GETOFFLINEACK_CMD = 106;
static NSInteger const RECEIVED_ACK_CMD = 107;
static NSInteger const FINDNEARBYBARS_CMD = 110;
static NSInteger const CHECKINBAR_CMD = 111;
static NSInteger const CHECKOUTBAR_CMD = 112;
static NSInteger const ADDUSERS_CMD = 14;
static NSInteger const REMOVEUSERS_CMD = 15;
static NSInteger const PING_CHECKIN_CMD = 115;
static NSInteger const UPDATE_NEARBY_SETTINGS_CMD = 116;
static NSInteger const GET_NEARBY_BARINFO_CMD = 117;
static NSInteger const GET_PROMO_LOCATION = 118;
static NSInteger const GET_PROMO_BAR = 119;
static NSInteger const GET_USERINFO_CMD = 120;
static NSInteger const UPDATE_USERINFO_CMD = 121;
static NSInteger const BAN_CHAT_CMD = 123;
static NSInteger const BAN_USER_CMD = 124;
static NSInteger const REMOVE_BAN_CHAT_CMD = 125;
static NSInteger const REMOVE_BAN_USER_CMD = 126;
static NSInteger const FOLLOW_BAR_CMD = 127;
static NSInteger const GET_LIST_BAR_FOLLOWED_CMD = 128;
static NSInteger const GET_SERVICEMENU_CMD = 129;
static NSInteger const VIEW_LIKE_PROMO_CMD = 130;
static NSInteger const GET_PROMO_STATS_CMD = 131;
static NSInteger const GET_PROMOS_NEAR_BY_CMD = 132;
static NSInteger const GET_PROMO_COUNT_FOLLOWED_BARS_CMD = 133;
static NSInteger const SHARE_PROMOTION_FB_CMD = 134;
static NSInteger const GET_BAN_USER_CMD = 136;
static NSInteger const GET_TOTAL_CLIENT_IN_ROOM_CMD = 142;
static NSInteger const GET_ALL_CLIENT_IN_ROOM_CMD = 144;

/* Upload API */
static NSInteger const SEND_UPLOAD_INFO_CMD = 2010;
static NSInteger const UPLOAD_FILE_CMD = 2011;

static NSInteger const SUBMIT_TOKEN_PNS_CMD = 9999;


@interface SocketCommand : NSObject

@property (nonatomic) uint8_t userAgent;
@property (nonatomic) uint32_t reqID;
@property (nonatomic) uint16_t cmd;
@property (nonatomic) uint8_t cmdVersion;
@property (nonatomic) uint32_t srcID;
@property (nonatomic) uint32_t destID;
@property (nonatomic) int16_t errorCode;
@property (nonatomic) uint32_t contentLength;
@property (nonatomic, strong) NSData* content;

- (void)writeStringContent:(NSString*)content;
- (void)writeContent:(NSData*)content;
- (NSData*)toData;
- (NSString*)toString;
- (void)readData:(NSObject*)data;
- (NSObject*)initWithData:(NSObject*)data;

@end

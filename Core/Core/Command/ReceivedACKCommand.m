//
//  ReceivedACKCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 5/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "ReceivedACKCommand.h"
#import "Helpers.h"
#import "CoreConstants.h"

@implementation ReceivedACKCommand

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.cmd = RECEIVED_ACK_CMD;
    self.IDs = [[NSArray alloc] init];
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data {
    self = [self init];
    
    [self readData:data];
    self.cmd = RECEIVED_ACK_CMD;
    
    NSDictionary* map = [JSONUtils parseDataToMap:self.content];
    self.IDs = [Helpers getArrayDiffNull:[map objectForKey:ENTITY_IDS]];

    return self;
}

- (NSData*)toData {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:self.IDs forKey:ENTITY_IDS];
    
    NSString* jsonData = [JSONUtils convertToJson:params];
    
    self.content = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    self.contentLength = (uint32_t)self.content.length;
    
    return [super toData];
}

@end
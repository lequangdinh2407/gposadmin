//
//  RemoveUserCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 5/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketCommand.h"

@interface RemoveUserCommand : SocketCommand

@property (strong, nonatomic) NSDictionary* info;

@end
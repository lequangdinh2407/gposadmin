//
//  ChatCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 3/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketCommand.h"

//@class LuvMessage;

@interface ChatCommand : SocketCommand

//@property (nonatomic, strong) LuvMessage* message;
@property (nonatomic, strong) NSString* createTime;
@property (nonatomic) uint32_t timeBlockSpam;

@end
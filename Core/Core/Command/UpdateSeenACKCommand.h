//
//  UpdateSeenACKCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 3/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketCommand.h"
#import "UpdateSeenCommand.h"

@interface UpdateSeenACKCommand : UpdateSeenCommand

@end

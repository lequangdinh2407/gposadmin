//
//  ChatACKCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 3/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "ChatACKCommand.h"
#import "Helpers.h"
#import "CoreConstants.h"

@implementation ChatACKCommand

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.cmd = CHATACK_CMD;
    self.createTime = 0;
    self.ID = 0;
    self.IDs = [[NSArray alloc] init];
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data {
    self = [self init];
    
    [self readData:data];
    self.cmd = CHATACK_CMD;
       
    NSDictionary* map = [JSONUtils parseDataToMap:self.content];
    self.createTime = [Helpers getULongLongDiffNull:[map objectForKey:ENTITY_CREATE_TIME]];
    self.ID = [Helpers getULongLongDiffNull:[map objectForKey:ENTITY_ID]];
    return self;
}

- (NSData*)toData {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:self.IDs forKey:ENTITY_IDS];
    
    NSString* jsonData = [JSONUtils convertToJson:params];
    
    self.content = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    self.contentLength = self.content.length;
    
    return [super toData];
}

@end

//
//  ChatCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 3/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommentSocketCommand.h"

@interface SendCommentCommand : CommentSocketCommand

@property (nonatomic, strong) NSMutableArray* comments;

@end
//
//  SocketCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 3/9/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSInteger const PING_COMMENT_CMD = 50;
static NSInteger const CONNECT_COMMENT_CMD = 51;
static NSInteger const IN_COMMENT_CMD = 52;
static NSInteger const OUT_COMMENT_CMD = 53;
static NSInteger const SEND_COMMENT_CMD = 54;
static NSInteger const DISCONNECT_COMMENT_CMD = 55;

@interface CommentSocketCommand : NSObject

@property (nonatomic) uint16_t type;
@property (nonatomic) uint8_t userAgent;
@property (nonatomic) uint32_t reqID;
@property (nonatomic) uint16_t cmd;
@property (nonatomic) uint8_t cmdVersion;
@property (nonatomic) uint64_t srcID;
@property (nonatomic) uint64_t destID;
@property (nonatomic) int16_t errorCode;
@property (nonatomic) uint32_t contentLength;
@property (nonatomic, strong) NSData* content;

- (void)writeStringContent:(NSString*)content;
- (void)writeContent:(NSData*)content;
- (NSData*)toData;
//- (NSString*)toString;
- (void)readData:(NSObject*)data;
- (NSObject*)initWithData:(NSObject*)data;
@end
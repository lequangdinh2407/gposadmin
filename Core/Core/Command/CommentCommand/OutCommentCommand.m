//
//  ChatCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 3/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "OutCommentCommand.h"
#import "Helpers.h"
#import "CoreConstants.h"

@implementation OutCommentCommand

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.cmd = OUT_COMMENT_CMD;
    self.error = 0;
    self.msg = @"";
    self.createTime = @"";
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data {
    self = [self init];
    
    [self readData:data];
    self.cmd = OUT_COMMENT_CMD;
    
    NSDictionary* map = [JSONUtils parseDataToMap:self.content];
    self.error = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_ERROR]];
    self.msg = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_MSG]];
    self.createTime = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CREATE_TIME]];
        
    return self;
}

- (NSData*)toData {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:@(self.error) forKey:ENTITY_TYPE];
    [params setValue:self.msg forKey:ENTITY_MSG];
    [params setValue:self.createTime forKey:ENTITY_CREATE_TIME];
    
    NSString* jsonData = [JSONUtils convertToJson:params];
    
    self.content = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    self.contentLength = (uint32_t)self.content.length;

    return [super toData];
}

@end

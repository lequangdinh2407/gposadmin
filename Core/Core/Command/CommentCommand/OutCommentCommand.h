//
//  ChatCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 3/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommentSocketCommand.h"

@interface OutCommentCommand : CommentSocketCommand

@property (nonatomic) uint16_t error;
@property (nonatomic, strong) NSString* msg;
@property (nonatomic, strong) NSString* createTime;

@end
//
//  ChatCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 3/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "SendCommentCommand.h"
#import "Helpers.h"
#import "CoreConstants.h"
#import "EntityManager.h"

@implementation SendCommentCommand

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.cmd = SEND_COMMENT_CMD;
    self.comments = [[NSMutableArray alloc] init];
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data {
    self = [self init];
    
    [self readData:data];
    self.cmd = SEND_COMMENT_CMD;
    [self.comments removeAllObjects];
    
    NSDictionary* map = [JSONUtils parseDataToMap:self.content];
    NSDictionary* dict = [Helpers getDictDiffNull:[map objectForKey:RESPONSE_DATA]];
    dict = [Helpers getDictDiffNull:[dict objectForKey:RESPONSE_DATA]];
    
    if ([dict objectForKey:RESPONSE_COMMENT]) {
        NSArray* comments = [Helpers getArrayDiffNull:[dict objectForKey:RESPONSE_COMMENT]];
        if (comments.count > 0) {
            for (int i = 0; i < comments.count; i++) {
                CommentEntity* entity = (CommentEntity*)[[EntityManager sharedClient] createItem:[comments objectAtIndex:i] type:COMMENT_ENTITY];
                [self.comments addObject:entity];
            }
        } else {
            CommentEntity* entity = (CommentEntity*)[[EntityManager sharedClient] createItem:[dict objectForKey:RESPONSE_COMMENT] type:COMMENT_ENTITY];
            [self.comments addObject:entity];
        }
    }
    
    return self;
}

@end
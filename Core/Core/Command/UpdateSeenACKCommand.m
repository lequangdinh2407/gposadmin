//
//  UpdateSeenACKCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 3/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "UpdateSeenACKCommand.h"
#import "Helpers.h"
#import "CoreConstants.h"

@implementation UpdateSeenACKCommand

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.cmd = UPDATEMESSAGESEENACK_CMD;
    self.IDList = [[NSArray alloc] init];
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data {
    self = [self init];
    
    [self readData:data];
    self.cmd = UPDATEMESSAGESEENACK_CMD;
    
    NSDictionary* map = [JSONUtils parseDataToMap:self.content];
    self.IDList = [map objectForKey:ENTITY_IDS];
    return self;
}

@end


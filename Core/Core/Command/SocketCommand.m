//
//  SocketCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 3/9/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "SocketCommand.h"
#import "Helpers.h"
#import "CoreConstants.h"

@implementation SocketCommand

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.userAgent = 2;
    self.reqID = 0;
    self.cmd = 0;
    self.cmdVersion = 0;
    self.srcID = 0;
    self.destID = 0;
    self.errorCode = 0;
    self.contentLength = 0;
    self.content = [[NSData alloc] init];
    
    return self;
}

- (void)writeStringContent:(NSString*)content;
{
    if (content == nil || [content isKindOfClass:[NSNull class]]) {
        self.content = [@"" dataUsingEncoding:NSUTF8StringEncoding];
    } else {
        self.content = [content dataUsingEncoding:NSUTF8StringEncoding];
    }
}

- (void)writeContent:(NSData*)content;
{
    if (content == nil || [content isKindOfClass:[NSNull class]]) {
        self.content = [@"" dataUsingEncoding:NSUTF8StringEncoding];
    } else {
        self.content = [NSData dataWithBytes:[content bytes] length:[content length]];
    }
}

- (NSData*)toData {
    self.contentLength = (uint32_t)[self.content length];
    
    unsigned long length = sizeof(uint8_t) + sizeof(uint32_t) + sizeof(uint32_t) + sizeof(uint16_t) + sizeof(uint8_t) + sizeof(uint32_t) + sizeof(int16_t) + sizeof(uint32_t) + self.contentLength;
    
    uint8_t* data = malloc(length);
    uint8_t* head = data;
    
    *(uint8_t*)head = self.userAgent;
    head += sizeof(self.userAgent);
    
    *(uint32_t*)head = CFSwapInt32HostToBig(self.reqID);
    head += sizeof(self.reqID);
    
    *(uint16_t*)head = CFSwapInt16HostToBig(self.cmd);
    head += sizeof(self.cmd);
    
    *(uint8_t*)head = self.cmdVersion;
    head += sizeof(self.cmdVersion);

    *(uint32_t*)head = CFSwapInt32HostToBig(self.srcID);
    head += sizeof(self.srcID);
    
    *(uint32_t*)head = CFSwapInt32HostToBig(self.destID);
    head += sizeof(self.destID);
    
    *(int16_t*)head = CFSwapInt16HostToBig(self.errorCode);
    head += sizeof(self.errorCode);
    
    *(uint32_t*)head = CFSwapInt32HostToBig(self.contentLength);
    head += sizeof(self.contentLength);
    
    memcpy(head, [self.content bytes], self.contentLength);
    
    NSData* result = [[NSData alloc] initWithBytes:data length:length];
    
    free(data);
    
    return result;
}

- (NSString*)toString {
    NSString* stringBase64 = [[self toData] base64EncodedStringWithOptions:NSUTF8StringEncoding];
    NSString* result = [NSString stringWithFormat:@"42[\"luvevent\",\"%@\"]", stringBase64];
    return result;
}

- (void)readData:(NSObject*)data {
    if (data == nil || ![data isKindOfClass:[NSDictionary class]])
        return;
    
    NSDictionary* dict =(NSDictionary*)data;
    
    id dictValue = [dict objectForKey:HDR_USER_AGENT];
    if ([Helpers isNotNull:dictValue]) {
        self.userAgent = [dictValue unsignedCharValue];
    }
    
    dictValue = [dict objectForKey:HDR_REQ_ID];
    if ([Helpers isNotNull:dictValue]) {
        self.reqID = (uint32_t)[dictValue unsignedLongValue];
    }
    
    dictValue = [dict objectForKey:HDR_CMD];
    if ([Helpers isNotNull:dictValue]) {
        self.cmd = [dictValue unsignedShortValue];
    }
    
    dictValue = [dict objectForKey:HDR_CMD_VERSION];
    if ([Helpers isNotNull:dictValue]) {
        self.cmdVersion = [dictValue unsignedCharValue];
    }
    
    dictValue = [dict objectForKey:HDR_SRC_ID];
    if ([Helpers isNotNull:dictValue]) {
        self.srcID = (uint32_t)[dictValue unsignedLongValue];
    }
    
    dictValue = [dict objectForKey:HDR_DEST_ID];
    if ([Helpers isNotNull:dictValue]) {
        self.destID = (uint32_t)[dictValue unsignedLongValue];
    }
    
    dictValue = [dict objectForKey:HDR_ERROR_CODE];
    if ([Helpers isNotNull:dictValue]) {
        self.errorCode = [dictValue shortValue];
    }
    
    dictValue = [dict objectForKey:HDR_CONTENT_LENGTH];
    if ([Helpers isNotNull:dictValue]) {
        self.contentLength = (uint32_t)[dictValue unsignedLongValue];
    }
    
    dictValue = [dict objectForKey:HDR_CONTENT];
    if ([Helpers isNotNull:dictValue]) {
        [self writeContent:dictValue];
    }
}

- (NSObject*)initWithData:(NSObject*)data {
    self = [self init];
    
    [self readData:data];    
    return self;
}

@end
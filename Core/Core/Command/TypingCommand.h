//
//  TypingCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 5/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketCommand.h"

@interface TypingCommand : SocketCommand

@property (nonatomic) uint32_t srcId;

@end
//
//  AddUserCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 5/14/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "AddUserCommand.h"
#import "Helpers.h"
#import "EntityManager.h"

@implementation AddUserCommand

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.cmd = ADDUSERS_CMD;
    self.info = nil;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data {
    self = [self init];
    
    [self readData:data];
    self.cmd = ADDUSERS_CMD;
    self.info = [JSONUtils parseDataToMap:self.content];
    
    return self;
}

@end
//
//  UploadFileCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 5/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "UploadFileCommand.h"
#import "Helpers.h"
#import "CoreConstants.h"

@implementation UploadFileCommand

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.cmd = UPLOAD_FILE_CMD;
    self.uploadID = 0;
    self.partID = 0;
    self.url = @"";
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data {
    self = [self init];
    
    [self readData:data];
    self.cmd = UPLOAD_FILE_CMD;
    
    NSDictionary* map = [JSONUtils parseDataToMap:self.content];
    
    self.uploadID = [Helpers getULongLongDiffNull:[map objectForKey:RESPONSE_UPLOADFILE_ID]];
    self.partID = [Helpers getUIntDiffNull:[map objectForKey:RESPONSE_UPLOADFILE_PART]];
    self.url = [CoreStringUtils getStringDiffNull:[map objectForKey:RESPONSE_UPLOADFILE_URL]];

    return self;
}

@end

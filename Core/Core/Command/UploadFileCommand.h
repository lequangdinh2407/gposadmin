//
//  UploadFileCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 5/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketCommand.h"

@interface UploadFileCommand : SocketCommand

@property (nonatomic) uint64_t uploadID;
@property (nonatomic) uint32_t partID;
@property (nonatomic, strong) NSString* url;

@end
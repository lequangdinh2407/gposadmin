//
//  TypingCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 5/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "TypingCommand.h"
#import "Helpers.h"
#import "CoreConstants.h"
#import "RequestErrorCode.h"

@implementation TypingCommand

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.cmd = TYPING_CMD;
    self.srcId = 0;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data {
    self = [self init];
    
    [self readData:data];
    self.cmd = TYPING_CMD;

    if (self.errorCode == BANNED)
        return self;
    
    uint8_t* head = (uint8_t*)[self.content bytes];
    uint32_t srcId;
    memcpy(&srcId, head, 4);
    srcId = CFSwapInt32BigToHost(srcId);
    self.srcId = srcId;
    
    return self;
}

@end
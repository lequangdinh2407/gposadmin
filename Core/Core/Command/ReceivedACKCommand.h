//
//  ReceivedACKCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 5/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketCommand.h"

@interface ReceivedACKCommand : SocketCommand

@property (nonatomic) NSArray* IDs;

@end
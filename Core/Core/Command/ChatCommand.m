//
//  ChatCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 3/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "ChatCommand.h"
#import "Helpers.h"
#import "CoreConstants.h"
//#import "LuvMessage.h"

@implementation ChatCommand

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.cmd = CHAT_CMD;
//    self.message = [[LuvMessage alloc] init];
    self.createTime = @"";
    self.timeBlockSpam = 0;
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data {
    self = [self init];
    
    [self readData:data];
    self.cmd = CHAT_CMD;
    
    NSDictionary* map = [JSONUtils parseDataToMap:self.content];
//    self.message.reqID = self.reqID;
//    self.message.type = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_TYPE]];
//    self.message.content = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CONTENT]];
//    self.message.srcId = self.destID;
//    self.message.roomId = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_SRCID]];
//    self.message.ID = [Helpers getULongLongDiffNull:[map objectForKey:ENTITY_ID]];
//    self.message.createTime = [Helpers getULongLongDiffNull:[map objectForKey:ENTITY_CREATE_TIME]];
//    self.message.srcAvaURL = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_AVA_URL]];
//    self.message.firstName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_FIRST_NAME]];
//    self.message.lastName = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_LAST_NAME]];
    
    self.createTime = [CoreStringUtils getStringDiffNull:[map objectForKey:ENTITY_CREATE_TIME]];
    self.timeBlockSpam = [Helpers getUIntDiffNull:[map objectForKey:ENTITY_TIME_BLOCK_SPAM]];
    
    return self;
}

- (NSData*)toData {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//    if (self.message.type != 0)
//        [params setValue:@(self.message.type) forKey:ENTITY_TYPE];
//    if ([Helpers isNotNull:self.message.content])
//        [params setValue:self.message.content forKey:ENTITY_CONTENT];
//    if (self.message.srcId != 0)
//        [params setValue:@(self.message.srcId) forKey:ENTITY_SRCID];
//    if (self.message.ID != 0)
//        [params setValue:@(self.message.ID) forKey:ENTITY_ID];    
    
    NSString* jsonData = [JSONUtils convertToJson:params];
    
    self.content = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    self.contentLength = (uint32_t)self.content.length;

    return [super toData];
}

@end

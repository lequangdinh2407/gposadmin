//
//  CharACKCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 3/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketCommand.h"

@interface ChatACKCommand : SocketCommand

@property (nonatomic) uint64_t createTime;
@property (nonatomic) uint64_t ID;
@property (nonatomic) NSArray* IDs;

@end

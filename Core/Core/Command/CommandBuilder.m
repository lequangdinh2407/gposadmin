//
//  CommandBuilder.m
//  luvkonectcore
//
//  Created by ToanPham on 3/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "CommandBuilder.h"
#import "CoreConstants.h"
#import "CommentSocketCommand.h"
#import "ChatCommand.h"
#import "ChatACKCommand.h"
#import "UpdateSeenCommand.h"
#import "UpdateSeenACKCommand.h"
#import "UploadFileCommand.h"
#import "ReceivedACKCommand.h"
#import "Helpers.h"
#import "TypingCommand.h"
#import "AddUserCommand.h"
#import "RemoveUserCommand.h"

/* CommentSocketCommand */
#import "InCommentCommand.h"
#import "OutCommentCommand.h"
#import "SendCommentCommand.h"

@implementation CommandBuilder

+ (instancetype)sharedClient {
    static CommandBuilder * volatile _sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (CommentSocketCommand*)createCommentCommand:(NSObject*)data type:(NSInteger)type {
    CommentSocketCommand* item = nil;
    if ([Helpers isNotNull:data] == false) {
        return item;
    }
    
    switch (type) {
        case IN_COMMENT_CMD:
            item = [[InCommentCommand alloc] initWithData:data];
            break;
        case OUT_COMMENT_CMD:
            item = [[OutCommentCommand alloc] initWithData:data];
            break;
        case SEND_COMMENT_CMD:
            item = [[SendCommentCommand alloc] initWithData:data];
            break;
        default:
            item = [[CommentSocketCommand alloc] initWithData:data];
            break;
    }
    return item;
}

- (SocketCommand*)createCommand:(NSObject*)data type:(NSInteger)type {
    SocketCommand* item = nil;
    if ([Helpers isNotNull:data] == false) {
        return item;
    }
    
    switch (type) {
        case UPDATEMESSAGESEENACK_CMD:
            item = [[UpdateSeenACKCommand alloc] initWithData:data];
            break;
        case UPDATEMESSAGESEEN_CMD:
            item = [[UpdateSeenCommand alloc] initWithData:data];
            break;
        case CHATACK_CMD:
            item = [[ChatACKCommand alloc] initWithData:data];
            break;
        case CHAT_CMD:
            item = [[ChatCommand alloc] initWithData:data];
            break;
        case UPLOAD_FILE_CMD:
            item = [[UploadFileCommand alloc] initWithData:data];
            break;
        case RECEIVED_ACK_CMD:
            item = [[ReceivedACKCommand alloc] initWithData:data];
            break;
        case TYPING_CMD:
            item = [[TypingCommand alloc] initWithData:data];
            break;
        case ADDUSERS_CMD:
            item = [[AddUserCommand alloc] initWithData:data];
            break;
        case REMOVEUSERS_CMD:
            item = [[RemoveUserCommand alloc] initWithData:data];
            break;
        default:
            item = [[SocketCommand alloc] initWithData:data];
            break;
    }
    return item;
}

- (SocketCommand*)parseData:(NSData*)data {
    int headerLength = sizeof(uint8_t) + sizeof(uint32_t) + sizeof(uint16_t) + sizeof(uint8_t) + sizeof(uint32_t) + sizeof(int16_t) + sizeof(uint32_t) + sizeof(uint32_t);
    if ([data length] < headerLength) {
        return nil;
    }
    uint8_t* bytes = (uint8_t*)[data bytes];
    uint32_t contentLength;
    memcpy(&contentLength, bytes + (headerLength - sizeof(uint32_t)), sizeof(uint32_t));
    contentLength = CFSwapInt32BigToHost(contentLength);
    
    if ([data length] != (headerLength + contentLength)) {
        return nil;
    }
    
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    
    uint8_t userAgent;
    memcpy(&userAgent, bytes, sizeof(uint8_t));
    bytes += sizeof(uint8_t);
    
    uint32_t reqID;
    memcpy(&reqID, bytes, sizeof(uint32_t));
    bytes += sizeof(uint32_t);
    reqID = CFSwapInt32BigToHost(reqID);

    uint16_t cmd;
    memcpy(&cmd, bytes, sizeof(uint16_t));
    bytes += sizeof(uint16_t);
    cmd = CFSwapInt16BigToHost(cmd);
    
    uint8_t cmdVersion;
    memcpy(&cmdVersion, bytes, sizeof(uint8_t));
    bytes += sizeof(uint8_t);

    uint32_t srcID;
    memcpy(&srcID, bytes, sizeof(uint32_t));
    bytes += sizeof(uint32_t);
    srcID = CFSwapInt32BigToHost(srcID);
    
    uint32_t destID;
    memcpy(&destID, bytes, sizeof(uint32_t));
    bytes += sizeof(uint32_t);
    destID = CFSwapInt32BigToHost(destID);
    
    int16_t errorCode;
    memcpy(&errorCode, bytes, sizeof(int16_t));
    bytes += sizeof(int16_t);
    errorCode = CFSwapInt16BigToHost(errorCode);
    
    [dict setValue:@(userAgent) forKey:HDR_USER_AGENT];
    [dict setValue:@(reqID) forKey:HDR_REQ_ID];
    [dict setValue:@(cmd) forKey:HDR_CMD];
    [dict setValue:@(cmdVersion) forKey:HDR_CMD_VERSION];
    [dict setValue:@(srcID) forKey:HDR_SRC_ID];
    [dict setValue:@(destID) forKey:HDR_DEST_ID];
    [dict setValue:@(errorCode) forKey:HDR_ERROR_CODE];
    [dict setValue:@(contentLength) forKey:HDR_CONTENT_LENGTH];
    
    NSData* content = nil;
    if (contentLength >0) {
        bytes += sizeof(uint32_t);
        content = [NSData dataWithBytes:bytes length:contentLength];
        [dict setValue:content forKey:HDR_CONTENT];
    }   
    
    return [self createCommand:dict type:cmd];
}

- (CommentSocketCommand*)parseCommentData:(NSData*)data {
    int headerLength = sizeof(uint16_t) + sizeof(uint8_t) + sizeof(uint32_t) + sizeof(uint16_t) + sizeof(uint8_t) + sizeof(uint64_t) + sizeof(uint64_t) + sizeof(int16_t) + sizeof(uint32_t);
    if ([data length] < headerLength) {
        return nil;
    }
    uint8_t* bytes = (uint8_t*)[data bytes];
    uint32_t contentLength;
    memcpy(&contentLength, bytes + (headerLength - sizeof(uint32_t)), sizeof(uint32_t));
    contentLength = CFSwapInt32BigToHost(contentLength);
    
    if ([data length] != (headerLength + contentLength)) {
        return nil;
    }
    
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    
    uint16_t type;
    memcpy(&type, bytes, sizeof(uint16_t));
    bytes += sizeof(uint16_t);
    
    uint8_t userAgent;
    memcpy(&userAgent, bytes, sizeof(uint8_t));
    bytes += sizeof(uint8_t);
    
    uint32_t reqID;
    memcpy(&reqID, bytes, sizeof(uint32_t));
    bytes += sizeof(uint32_t);
    reqID = CFSwapInt32BigToHost(reqID);
    
    uint16_t cmd;
    memcpy(&cmd, bytes, sizeof(uint16_t));
    bytes += sizeof(uint16_t);
    cmd = CFSwapInt16BigToHost(cmd);
    
    uint8_t cmdVersion;
    memcpy(&cmdVersion, bytes, sizeof(uint8_t));
    bytes += sizeof(uint8_t);
    
    uint64_t srcID;
    memcpy(&srcID, bytes, sizeof(uint64_t));
    bytes += sizeof(uint64_t);
    srcID = CFSwapInt64BigToHost(srcID);
    
    uint64_t destID;
    memcpy(&destID, bytes, sizeof(uint64_t));
    bytes += sizeof(uint64_t);
    destID = CFSwapInt64BigToHost(destID);
    
    int16_t errorCode;
    memcpy(&errorCode, bytes, sizeof(int16_t));
    bytes += sizeof(int16_t);
    errorCode = CFSwapInt16BigToHost(errorCode);
    
    [dict setValue:@(type) forKey:HDR_TYPE];
    [dict setValue:@(userAgent) forKey:HDR_USER_AGENT];
    [dict setValue:@(reqID) forKey:HDR_REQ_ID];
    [dict setValue:@(cmd) forKey:HDR_CMD];
    [dict setValue:@(cmdVersion) forKey:HDR_CMD_VERSION];
    [dict setValue:@(srcID) forKey:HDR_SRC_ID];
    [dict setValue:@(destID) forKey:HDR_DEST_ID];
    [dict setValue:@(errorCode) forKey:HDR_ERROR_CODE];
    [dict setValue:@(contentLength) forKey:HDR_CONTENT_LENGTH];
    
    NSData* content = nil;
    if (contentLength >0) {
        bytes += sizeof(uint32_t);
        content = [NSData dataWithBytes:bytes length:contentLength];
        [dict setValue:content forKey:HDR_CONTENT];
    }
    
    return [self createCommentCommand:dict type:cmd];
}

- (SocketCommand*)parseDataResponse:(NSData*)data {
    SocketCommand* command = [self parseData:data];
    return command;
}

- (CommentSocketCommand*)parseDataCommentResponse:(NSData*)data {
    CommentSocketCommand* command = [self parseCommentData:data];
    return command;
}

@end
//
//  CommandBuilder.h
//  luvkonectcore
//
//  Created by ToanPham on 3/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SocketCommand;
@class CommentSocketCommand;

@interface CommandBuilder : NSObject

+ (instancetype)sharedClient;
- (SocketCommand*)createCommand:(NSObject*)data type:(NSInteger)type;
- (CommentSocketCommand*)createCommentCommand:(NSObject*)data type:(NSInteger)type;
- (SocketCommand*)parseData:(NSData*)data;
- (SocketCommand*)parseDataResponse:(NSData*)data;
- (CommentSocketCommand*)parseDataCommentResponse:(NSData*)data;

@end
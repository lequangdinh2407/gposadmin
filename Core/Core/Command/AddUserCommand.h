//
//  AddUserCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 5/14/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketCommand.h"
@class UserEntity;

@interface AddUserCommand : SocketCommand

@property (strong, nonatomic) NSDictionary* info;

@end
//
//  UpdateSeenCommand.h
//  luvkonectcore
//
//  Created by ToanPham on 3/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketCommand.h"

@interface UpdateSeenCommand : SocketCommand

@property (nonatomic, strong) NSArray* IDList;
@property (nonatomic) uint32_t srcId;

@end


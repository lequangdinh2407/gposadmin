//
//  UpdateSeenCommand.m
//  luvkonectcore
//
//  Created by ToanPham on 3/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "UpdateSeenCommand.h"
#import "Helpers.h"
#import "CoreConstants.h"

@implementation UpdateSeenCommand

- (instancetype)init{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.cmd = UPDATEMESSAGESEEN_CMD;
    self.IDList = [[NSArray alloc] init];
    
    return self;
}

- (NSObject*)initWithData:(NSObject*)data {
    self = [self init];
    
    [self readData:data];
    self.cmd = UPDATEMESSAGESEEN_CMD;
    
    NSDictionary* map = [JSONUtils parseDataToMap:self.content];
    self.IDList = [map objectForKey:ENTITY_IDS];
    return self;
}

- (NSData*)toData {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:self.IDList forKey:ENTITY_IDS];
    NSString* jsonData = [JSONUtils convertToJson:params];
    
    self.content = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    
    return [super toData];
}

@end
//
//  UserEntity.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RLMObject(BaseEntity).h"
#import "Realm/Realm.h"

@interface RLMUser : RLMObject

@property (nonatomic, strong) NSString* userId;
@property (nonatomic, strong) NSString* avatar;
@property (nonatomic, strong) NSString* birthday;
@property (nonatomic, strong) NSString* cover;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* displayName;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* mobilePhone;
@property (nonatomic, strong) NSString* status;

@property (nonatomic, strong) NSString* address;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* state;
@property (nonatomic, strong) NSString* country;
@property (nonatomic, strong) NSString* countryCode;
@property (nonatomic, strong) NSString* zipCode;

@property (nonatomic, strong) NSString* qr;
@property (nonatomic, strong) NSString* shopCode;
@property (nonatomic, assign) int rewardPoint;
@property (nonatomic, strong) NSString* dateOB;
@property (nonatomic, strong) NSString* monthOB;
@end

//
//  AccountEntity.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "RLMAccount.h"
#import "AccountEntity.h"

@implementation RLMAccount

- (void)loadDataFromBaseEntity:(BaseEntity*)base {
    if ([base isKindOfClass:[AccountEntity class]]) {
        AccountEntity* account = (AccountEntity*)base;
        self.userId = account.userId;
        self.userName = account.userName;
        self.password = account.password;
    }
}

@end

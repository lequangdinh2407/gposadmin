

#import "Realm/RLMObject.h"
#import "BaseEntity.h"

@interface RLMObject (BaseEntity)

- (void)loadDataFromBaseEntity:(BaseEntity*)base;
- (void)loadDataFromBaseEntityWithoutChangingKey:(BaseEntity*)base;

@end

//
//  UserEntity.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "RLMSocialProfile.h"
#import "SocialProfileEntity.h"

@implementation RLMSocialProfile

- (void)loadDataFromBaseEntity:(BaseEntity*)base {
    if ([base isKindOfClass:[SocialProfileEntity class]]) {
        SocialProfileEntity* socialProfile = (SocialProfileEntity*)base;
        
        self.userId = socialProfile.userId;
        self.avatar = socialProfile.avatar;
        self.birthday = socialProfile.birthday;
        self.firstName = socialProfile.firstName;
        self.lastName = socialProfile.lastName;
        self.email = socialProfile.email;
        self.phone = socialProfile.phone;
        self.countryCode = socialProfile.countryCode;
        self.socialProfileId = socialProfile.socialProfileId;
        self.type = socialProfile.type;
        self.gender = socialProfile.gender;
        
        /*
        self.rewardPoint = socialProfile.rewardPoint;
        self.shopCode = socialProfile.shopCode;
        self.slogan = socialProfile.slogan;
        self.status = socialProfile.status;
        self.qr = socialProfile.qr;
         */
    }
}

+ (nullable NSString *)primaryKey {
    return @"userId";
}

@end

//
//  UserEntity.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "RLMUser.h"
#import "UserEntity.h"

@implementation RLMUser

- (void)loadDataFromBaseEntity:(BaseEntity*)base {
    if ([base isKindOfClass:[UserEntity class]]) {
        UserEntity* user = (UserEntity*)base;
        
        self.userId = user.userId;
        self.firstName = user.firstName;
        self.lastName = user.lastName;
        self.email = user.email;
        self.birthday = user.birthday;
        self.status = user.status;
        self.avatar = user.avatar;
        self.qr = user.qr;
        self.shopCode = user.shopCode;
        self.rewardPoint = user.rewardPoint;
        self.dateOB = user.dateOB;
        self.monthOB = user.monthOB;
        self.displayName = user.displayName;
    }
}

- (void)loadDataFromBaseEntityWithoutChangingKey:(BaseEntity*)base {
    if ([base isKindOfClass:[UserEntity class]]) {
        UserEntity* user = (UserEntity*)base;
        
        self.firstName = user.firstName;
        self.lastName = user.lastName;
        self.email = user.email;
        self.birthday = user.birthday;
        self.status = user.status;
        self.avatar = user.avatar;
        self.qr = user.qr;
        self.shopCode = user.shopCode;
        self.rewardPoint = user.rewardPoint;
        self.dateOB = user.dateOB;
        self.monthOB = user.monthOB;
        self.displayName = user.displayName;
    }
}

+ (nullable NSString *)primaryKey {
    return @"userId";
}

@end

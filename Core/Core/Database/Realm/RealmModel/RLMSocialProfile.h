//
//  UserEntity.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RLMObject(BaseEntity).h"
#import "Realm/Realm.h"

@interface RLMSocialProfile : RLMObject

@property (nonatomic, strong) NSString* userId;
@property (nonatomic, strong) NSString* avatar;
@property (nonatomic, strong) NSString* birthday;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* countryCode;
@property (nonatomic, strong) NSString* socialProfileId;
@property (nonatomic, assign) int type;
@property (nonatomic, assign) int gender;

@property (nonatomic, strong) NSString* qr;
@property (nonatomic, strong) NSString* shopCode;
@property (nonatomic, strong) NSString* slogan;
@property (nonatomic, assign) int rewardPoint;
@property (nonatomic, assign) int status;

@end

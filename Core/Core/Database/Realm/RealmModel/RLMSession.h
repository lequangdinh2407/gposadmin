//
//  AccountEntity.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RLMObject(BaseEntity).h"
#import "Realm/Realm.h"

@interface RLMSession : RLMObject

@property (nonatomic, strong) NSString* userId;
@property (nonatomic, strong) NSString* sessionKey;
@property (nonatomic, strong) NSString* refreshToken;
@property (nonatomic, strong) NSString* domain;

@end

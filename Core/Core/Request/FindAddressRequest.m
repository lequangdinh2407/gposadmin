//
//  FindAddressRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "FindAddressRequest.h"
#import "AddressEntity.h"
#import "EntityManager.h"

@implementation FindAddressRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)zipCode success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];

    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:zipCode forKey:REQUEST_ZIP_CODE];
    
    self.jsonData = [Helpers convertToJson:params];
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSArray *data = (NSArray*)responseObject;
    NSMutableArray* addressList = [[NSMutableArray alloc] init];

    for (int i = 0; i < data.count; i++) {
        AddressEntity *addressEntity = (AddressEntity*) [[EntityManager sharedClient] createItem:[data objectAtIndex:i] type:ADDRESS_ENTITY];
        [addressList addObject:addressEntity];
    }

    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:addressList forKey:RESPONSE_ADDRESS_LIST];

    return dataResult;
}

@end
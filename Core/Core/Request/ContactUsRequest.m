//
//  SendFeedbackRequest.m
//  Core
//
//  Created by ToanPham on 6/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "ContactUsRequest.h"

@implementation ContactUsRequest

- (instancetype)init:(NSString*)url params:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

@end

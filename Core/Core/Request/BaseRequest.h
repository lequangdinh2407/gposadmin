//
//  BaseRequest.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helpers.h"
#import "CoreConstants.h"

typedef enum : NSUInteger {
    POST = 1,
    GET = 2,
    PUT = 3,
    DELETE = 4
} httpMethod;

@interface BaseRequest : NSObject

typedef void (^SuccessCallback)(NSMutableDictionary *results, int errorCode);
typedef void (^FailureCallback)(NSMutableDictionary *results, int errorCode);
typedef void (^ProgressCallback)(NSUInteger __unused bytesWritten,
                                 long long totalBytesWritten,
                                 long long totalBytesExpectedToWrite);

@property (nonatomic, assign) NSInteger retryNetworkSlow;
@property (nonatomic, assign) NSInteger retrySessionExpired;
@property (nonatomic, strong) NSString* jsonData;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSMutableDictionary* params;
@property (nonatomic, strong) NSString* arrayParams;
@property (nonatomic, strong) SuccessCallback success;
@property (nonatomic, strong) FailureCallback failure;
@property (nonatomic, strong) ProgressCallback progress;
@property (nonatomic, strong) NSData* data;
@property (nonatomic, strong) NSString* mimeType;
@property (nonatomic) BOOL isUpload;
@property (nonatomic, strong) NSString* signature;
@property (nonatomic, strong) NSString* timeStamp;

@property (nonatomic, strong) NSString* contentType;
@property (nonatomic, strong) NSString* authorization;
@property (nonatomic, strong) NSString* token;
@property (nonatomic, strong) NSString* device;

@property (nonatomic) httpMethod  method;

- (instancetype)init:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure;
- (instancetype)init:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure progress:(ProgressCallback)progress;
- (instancetype)initGET:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure;
- (instancetype)initDELETE:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure;
- (instancetype)init:(NSString*)url file:(NSData*)file success:(SuccessCallback)success failure:(FailureCallback)failure progress:(ProgressCallback)progress;
- (instancetype)init:(NSString*)url file:(NSData*)file success:(SuccessCallback)success failure:(FailureCallback)failure;

- (NSMutableDictionary*)getParams;
- (void)addToQueue;
- (void)addToPosQueue;
- (void)addToUploadQueue;
- (void)addToProgressUploadQueue;
- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject;

@end

//
//  BaseWebSocketRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 3/13/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"
#import "CommentSocketCommand.h"
#import "CommentSocketManager.h"

@interface BaseCommentSocketRequest : BaseRequest

@property (nonatomic, strong) CommentSocketCommand* command;
@property (nonatomic) uint64_t sendTime;
@property (nonatomic) uint8_t timeOutCount;

- (instancetype)init:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure;
- (CommentSocketCommand*)getCommand;

@end
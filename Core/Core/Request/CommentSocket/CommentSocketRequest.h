//
//  UploadSocketRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 4/29/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebSocketRequest.h"

@interface CommentSocketRequest : WebSocketRequest

@property (strong, nonatomic) NSString* userId;

- (instancetype)init:(NSString*)url userId:(NSString*)userId success:(SuccessCallback)success failure:(FailureCallback)failure;

@end
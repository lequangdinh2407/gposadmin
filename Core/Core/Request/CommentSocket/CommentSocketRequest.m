//
//  UploadSocketRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 4/29/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "CommentSocketRequest.h"
#import "CommentSocketManager.h"
#import "RequestManager.h"

@implementation CommentSocketRequest

- (instancetype)init:(NSString*)url userId:(NSString*)userId success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    self.userId = userId;
    
    return self;
}

- (NSString *)getURL {
    NSString* url = [NSString stringWithFormat:@"%@?%@=%@", self.url, REQUEST_USER_ID, self.userId];
    return url;
}

- (void)addToQueue {
    [[CommentSocketManager sharedClient] connect:self];
}

@end
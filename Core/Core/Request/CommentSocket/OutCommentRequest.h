//
//  UploadSocketRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 4/29/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseCommentSocketRequest.h"

@interface OutCommentRequest : BaseCommentSocketRequest

@property (nonatomic, strong) NSString *srcId;
@property (nonatomic, strong) NSString *destId;
@property (nonatomic) int type;

- (instancetype)init:(NSString*)srcId destId:(NSString*)destId type:(int)type success:(SuccessCallback)success failure:(FailureCallback)failure;

@end
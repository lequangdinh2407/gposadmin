//
//  UploadSocketRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 4/29/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "InCommentRequest.h"
#import "CommandBuilder.h"

@implementation InCommentRequest

- (instancetype)init:(NSString*)srcId destId:(NSString*)destId type:(int)type success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    self = [super init:params success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    self.srcId = srcId;
    self.destId = destId;
    self.type = type;
    
    return self;
}

- (CommentSocketCommand*)getCommand {
    if (self.command != nil)
        return self.command;
    
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    NSData *content = [self.jsonData dataUsingEncoding:NSUTF8StringEncoding];
    
    [dict setValue:@(IN_COMMENT_CMD) forKey:HDR_CMD];
    [dict setValue:@([Helpers getReqIDFormPref]) forKey:HDR_REQ_ID];
    
    NSScanner *scanner = [NSScanner scannerWithString:self.srcId];
    uint64_t srcId = 0;
    [scanner scanUnsignedLongLong:&srcId];
    [dict setValue:@(srcId) forKey:HDR_SRC_ID];
    
    scanner = [NSScanner scannerWithString:self.destId];
    uint64_t destId = 0;
    [scanner scanUnsignedLongLong:&destId];
    [dict setValue:@(destId) forKey:HDR_DEST_ID];

    [dict setValue:@(self.type) forKey:HDR_TYPE];
    [dict setValue:@([content length]) forKey:HDR_CONTENT_LENGTH];
    
    self.command = [[CommandBuilder sharedClient] createCommentCommand:dict type:IN_COMMENT_CMD];
    return self.command;
}

@end
//
//  UploadPhotoRequest.m
//  spabeecore
//
//  Created by Toan Pham Thanh on 5/27/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "UploadPhotoRequest.h"
#import "EntityManager.h"

@implementation UploadPhotoRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *data = (NSMutableDictionary*)responseObject;
       
        return data;
    }
    return nil;
}

@end

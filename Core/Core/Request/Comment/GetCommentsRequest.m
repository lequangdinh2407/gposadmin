//
//  GetAddressRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "GetCommentsRequest.h"
#import "EntityManager.h"

@implementation GetCommentsRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    NSArray* commentsArr = (NSArray*)[Helpers getArrayDiffNull:[data objectForKey:RESPONSE_COMMENTS_LIST]];
    NSInteger totalComment = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_TOTAL_COMMENT]];

    NSMutableArray* comments = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < commentsArr.count; i++) {
        CommentEntity* comment = (CommentEntity*)[[EntityManager sharedClient] createItem:[commentsArr objectAtIndex:i] type:COMMENT_ENTITY];
        [comments addObject:comment];
    }
    
    BOOL hasMore = [Helpers getBoolDiffNull:[data objectForKey:RESPONSE_HAS_MORE]];
    BOOL isAppend = [Helpers getBoolDiffNull:[data objectForKey:RESPONSE_IS_APPEND]];
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:comments forKey:RESPONSE_COMMENTS_LIST];
    [dataResult setValue:@(hasMore) forKey:RESPONSE_HAS_MORE];
    [dataResult setValue:@(isAppend) forKey:RESPONSE_IS_APPEND];
    [dataResult setValue:@(totalComment) forKey:RESPONSE_TOTAL_COMMENT];

    if([data objectForKey:RESPONSE_LAST_CHILD]) {
        CommentEntity* lastComment = (CommentEntity*)[[EntityManager sharedClient] createItem:[data objectForKey:RESPONSE_LAST_CHILD] type:COMMENT_ENTITY];
        [dataResult setValue:lastComment forKey:RESPONSE_LAST_CHILD];
    }
    
    return dataResult;
}

@end

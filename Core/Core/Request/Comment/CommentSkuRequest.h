//
//  GetAddressRequest.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface CommentSkuRequest : BaseRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey skuId:(NSString*)skuId content:(NSString*)content sticker:(NSString*)sticker success:(SuccessCallback)success failure:(FailureCallback)failure;

@end
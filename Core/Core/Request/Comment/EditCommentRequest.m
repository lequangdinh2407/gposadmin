//
//  GetAddressRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "EditCommentRequest.h"
#import "EntityManager.h"

@implementation EditCommentRequest

- (instancetype)init:(NSString*)url feedPhotoId:(NSString*)feedPhotoId text:(NSString*)text sticker:(NSString*)sticker photoId:(NSString*)photoId success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if (![CoreStringUtils isEmpty:text]) {
        [params setObject:text forKey:REQUEST_TEXT];
    }
    if (![CoreStringUtils isEmpty:feedPhotoId]) {
        [params setObject:feedPhotoId forKey:REQUEST_FEED_PHOTO_ID];
    }
    if (![CoreStringUtils isEmpty:photoId]) {
        [params setObject:photoId forKey:REQUEST_PHOTO_ID];
    }
    if (![CoreStringUtils isEmpty:sticker]) {
        [params setObject:sticker forKey:REQUEST_STICKER];
    }
    self.jsonData = [JSONUtils convertToJson:params];
    self.method = PUT;
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    CommentEntity* feed = (CommentEntity*)[[EntityManager sharedClient] createItem:[data objectForKey:RESPONSE_COMMENT] type:COMMENT_ENTITY];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    
    [dataResult setValue:feed forKey:RESPONSE_COMMENT];
    return dataResult;
}

@end

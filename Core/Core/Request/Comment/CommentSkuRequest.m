//
//  GetAddressRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "CommentSkuRequest.h"
#import "EntityManager.h"

@implementation CommentSkuRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey skuId:(NSString*)skuId content:(NSString*)content sticker:(NSString*)sticker success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:sessionKey forKey:REQUEST_SESSION_KEY];
    [params setObject:skuId forKey:REQUEST_SKU_ID];
    [params setObject:content forKey:REQUEST_CONTENT];
    [params setObject:sticker forKey:REQUEST_STICKER];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    
    CommentEntity* feed = (CommentEntity*)[[EntityManager sharedClient] createItem:[data objectForKey:RESPONSE_COMMENT] type:COMMENT_ENTITY];
    NSInteger totalComment = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_TOTAL_COMMENT]];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:feed forKey:RESPONSE_COMMENT];
    [dataResult setValue:@(totalComment) forKey:RESPONSE_TOTAL_COMMENT];    
    return dataResult;
}

@end
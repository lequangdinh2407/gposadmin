//
//  GetAddressRequest.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface GetSocketCommentUrlRequest : BaseRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey success:(SuccessCallback)success failure:(FailureCallback)failure;

@end
//
//  GetAddressRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "DeleteCommentRequest.h"
#import "EntityManager.h"

@implementation DeleteCommentRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    
    NSInteger totalComment = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_TOTAL_COMMENT]];
    NSInteger childCount = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_TOTAL_CHILD]];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:@(totalComment) forKey:RESPONSE_TOTAL_COMMENT];
    [dataResult setValue:@(childCount) forKey:RESPONSE_TOTAL_CHILD];

    return dataResult;
}

@end

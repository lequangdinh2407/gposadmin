//
//  GetAddressRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "GetSocketCommentUrlRequest.h"

@implementation GetSocketCommentUrlRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:sessionKey forKey:REQUEST_SESSION_KEY];
    
    self.jsonData = [Helpers convertToJson:params];
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    NSString* url = [CoreStringUtils getStringDiffNull:[data objectForKey:RESPONSE_URL]];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:url forKey:RESPONSE_URL];
    return dataResult;
}

@end

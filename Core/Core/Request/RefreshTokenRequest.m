//
//  LoginRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "RefreshTokenRequest.h"
#import "SessionManager.h"

@implementation RefreshTokenRequest

- (instancetype)init:(NSString*)url refreshToken:(NSString*)refreshToken lon:(float)lon lat:(float)lat success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:refreshToken forKey:REQUEST_REFRESH_TOKEN];
    [params setValue:@(lon) forKey:REQUEST_LON];
    [params setValue:@(lat) forKey:REQUEST_LAT];

    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary* data = (NSDictionary*)responseObject;
        
        NSString* sessionKey = [CoreStringUtils getStringDiffNull:[data objectForKey:RESPONSE_SESSIONKEY]];
        [SessionManager sharedClient].sessionKey = sessionKey;

        NSString* refreshToken = [CoreStringUtils getStringDiffNull:[data objectForKey:RESPONSE_REFRESH_TOKEN]];
        [SessionManager sharedClient].refreshToken = refreshToken;
        
        NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
        return dataResult;
    }
    return nil;
}

@end

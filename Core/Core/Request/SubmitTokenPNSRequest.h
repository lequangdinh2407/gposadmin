//
//  SubmitTokenPNSRequest.h
//  Core
//
//  Created by ToanPham on 3/31/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"


@interface SubmitTokenPNSRequest : BaseRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey imei:(NSString*)imei token:(NSString*)token success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

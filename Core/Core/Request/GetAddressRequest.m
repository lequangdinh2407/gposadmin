//
//  GetAddressRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "GetAddressRequest.h"
#import "AFNetworking.h"
#import "RequestErrorCode.h"
#import "Log.h"
#import "CoreConstants.h"
#import "Helpers.h"
#import "RequestManager.h"
#import "AddressEntity.h"

static NSString* const TAG = @"BaseRequest";

@implementation GetAddressRequest

- (instancetype)init:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    return self;
}

- (void)addToQueue {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:self.url
       parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [Log d:0 tag:TAG message:responseObject];
              @try {
                  NSMutableDictionary * dataResult = [self parseNetworkRespone:responseObject];
                  self.success(dataResult, 0);
              }
              @catch (NSException * e) {
                  [Log d:0 tag:TAG message:@"Exception while parsing Network response!"];
                  self.failure(nil, UNKNOWN_EXCEPTION);
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [Log d:0 tag:TAG message:error];
              self.failure(nil, UNKNOWN_EXCEPTION);
          }];
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSArray* results = (NSArray*) [responseObject objectForKey:@"results"];
    NSDictionary *rsl = [results objectAtIndex:0];
    NSDictionary *geometry = [rsl objectForKey:@"geometry"];
    NSDictionary *location = [geometry objectForKey:@"location"];

    double lng = [[location objectForKey:@"lng"] doubleValue];
    double lat = [[location objectForKey:@"lat"] doubleValue];

    AddressEntity *addressEntiy = [[AddressEntity alloc] init];
    addressEntiy.lon = lng;
    addressEntiy.lat = lat;
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:addressEntiy forKey:RESPONSE_ADDRESS_ENTITY];
    
    return dataResult;
}

@end
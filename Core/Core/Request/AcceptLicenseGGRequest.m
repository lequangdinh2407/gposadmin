//
//  AcceptLicenseGGRequest.m
//  Core
//
//  Created by ToanPham on 6/9/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "AcceptLicenseGGRequest.h"

@implementation AcceptLicenseGGRequest

- (instancetype)init:(NSString*)url accessToken:(NSString*)accessToken imei:(NSString*)imei success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:accessToken forKey:REQUEST_ACCESS_TOKEN];
    [params setValue:imei forKey:REQUEST_IMEI];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

@end

//
//  CheckValidCodeRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "CheckValidCodeRequest.h"

@implementation CheckValidCodeRequest

- (instancetype)init:(NSString*)url sendInfo:(NSString*)sendInfo activeCode:(NSString*)activeCode imei:(NSString*)imei countryCode:(NSString*)countryCode success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];

    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:sendInfo forKey:REQUEST_SENDINFO];
    [params setValue:activeCode forKey:REQUEST_ACTIVE_CODE];
    [params setValue:imei forKey:REQUEST_IMEI];
    [params setValue:countryCode forKey:REQUEST_COUNTRY_CODE];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    return dataResult;
}

@end

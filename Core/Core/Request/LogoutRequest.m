//
//  LogoutRequest.m
//  Core
//
//  Created by ToanPham on 5/13/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "LogoutRequest.h"
#import "UserEntity.h"
#import "SessionManager.h"
#import "EntityManager.h"

@implementation LogoutRequest

- (instancetype)init:(NSString*)url deviceId:(NSString*)deviceId success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:deviceId forKey:REQUEST_DEVICE_ID];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    return nil;
}

@end

//
//  SendFeedbackRequest.m
//  Core
//
//  Created by ToanPham on 6/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "SendFeedbackRequest.h"

@implementation SendFeedbackRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey message:(NSString*)message success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:sessionKey forKey:REQUEST_SESSION_KEY];
    [params setValue:message forKey:REQUEST_MESSAGE];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

@end

//
//  BaseWebSocketRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 3/13/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@class StompFrame;

@interface BaseStompRequest : BaseRequest

@property (nonatomic, strong) NSString* cmd;
@property (nonatomic, strong) NSDictionary* headers;
@property (nonatomic, strong) NSDictionary* body;
@property (nonatomic, strong) NSString* receiptId;

@property (nonatomic, strong) StompFrame* frame;

@property (nonatomic) uint64_t sendTime;
@property (nonatomic) uint8_t timeOutCount;

- (instancetype)init:(NSString*)cmd headers:(NSDictionary*)headers body:(NSDictionary*)body receiptId:(NSString*)recepitId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (StompFrame*)getFrame;

@end

//
//  BaseWebSocketRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 3/13/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "SubscribeRequest.h"
#import "StompFrame.h"
#import "StompManager.h"
#import "StompBuilder.h"

#define TAG @"SubscribeRequest"

@implementation SubscribeRequest

- (instancetype)init:(NSString*)destination subscriptionId:(NSString*)subscriptionId unsubscribe:(BOOL)unsubscribe success:(SuccessCallback)success failure:(FailureCallback)failure{
    
    NSString* receiptId = [NSString stringWithFormat:@"%u", [Helpers getReqIDFormPref]];
    NSDictionary* headers = nil;
    if (!unsubscribe)
        headers = @{DESTINATION_HDR : destination, RECEIPT_HDR : receiptId, ID_HDR : subscriptionId};
    else
        headers = @{RECEIPT_HDR : receiptId, ID_HDR : subscriptionId};
    
    self = [super init:(unsubscribe ? UNSUBSCRIBE_CMD : SUBSCRIBE_CMD) headers:headers body:nil receiptId:receiptId success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    return self;
}

@end

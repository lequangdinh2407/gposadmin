//
//  BaseWebSocketRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 3/13/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "SendRequest.h"
#import "StompFrame.h"
#import "StompManager.h"
#import "StompBuilder.h"

#define TAG @"SendRequest"

@implementation SendRequest

- (instancetype)init:(NSString*)destination body:(NSDictionary*)body success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    NSString* receiptId = [NSString stringWithFormat:@"%u", [Helpers getReqIDFormPref]];
    NSDictionary* headers = @{DESTINATION_HDR : destination, RECEIPT_HDR : receiptId};
    
    self = [super init:SEND_CMD headers:headers body:body receiptId:receiptId success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    return self;
}

- (StompFrame*)getFrame {
    if (!self.frame) {
        self.frame = [[StompBuilder sharedClient] createFrame:SEND_CMD headers:self.headers body:self.body];
    }
    return self.frame;
}

@end

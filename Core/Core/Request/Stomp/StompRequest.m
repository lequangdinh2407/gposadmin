//
//  WebSocketRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 3/6/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "StompRequest.h"
#import "SessionManager.h"
#import "StompManager.h"
#import "RequestManager.h"

@implementation StompRequest

- (instancetype)init:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

- (NSString *)getURL {
    return self.url;
}

- (void)addToQueue {   
    [[StompManager sharedClient] connect:self];    
}

@end

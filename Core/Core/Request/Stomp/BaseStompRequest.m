//
//  BaseWebSocketRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 3/13/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "BaseStompRequest.h"
#import "CoreConstants.h"
#import "StompManager.h"
#import "StompBuilder.h"

#define TAG @"BaseStompRequest"

@implementation BaseStompRequest

- (instancetype)init:(NSString*)cmd headers:(NSDictionary*)headers body:(NSDictionary*)body receiptId:(NSString*)recepitId success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:nil success:success failure:failure];
    
    if (!self) {
        return nil;
    }

    self.frame = nil;
    
    self.cmd = cmd;
    self.receiptId = recepitId;
    if ([Helpers isNotNull:headers]) {
        self.headers = [NSDictionary dictionaryWithDictionary:headers];
    }
    if ([Helpers isNotNull:body]) {
        self.body = [NSDictionary dictionaryWithDictionary:body];
    }
    
    return self;
}


- (StompFrame*)getFrame {
    if (!self.frame) {
        self.frame = [[StompBuilder sharedClient] createFrame:self.cmd headers:self.headers body:self.body];
    }
    return self.frame;
}

- (void)addToQueue {
    [[StompManager sharedClient] addToQueue:self];
}

@end

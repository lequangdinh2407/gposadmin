//
//  WebSocketRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 3/6/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface StompRequest : BaseRequest

@property (nonatomic) uint8_t timeOutCount;

- (instancetype)init:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure;
- (NSString *)getURL;

@end

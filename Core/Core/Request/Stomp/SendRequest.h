//
//  BaseWebSocketRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 3/13/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseStompRequest.h"

@interface SendRequest : BaseStompRequest

- (instancetype)init:(NSString*)destination body:(NSDictionary*)body success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

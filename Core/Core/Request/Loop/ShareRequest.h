//
//  SharePromotionRequest.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/23/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface ShareRequest : BaseRequest

- (instancetype)init:(NSString*)url objectType:(int)objectType objectId:(NSString*)objectId message:(NSString*)message platformId:(NSString*)platformId facebookId:(NSString*)facebookId success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

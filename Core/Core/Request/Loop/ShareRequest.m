//
//  SharePromotionRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/23/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "ShareRequest.h"

@implementation ShareRequest

- (instancetype)init:(NSString*)url objectType:(int)objectType objectId:(NSString*)objectId message:(NSString*)message platformId:(NSString*)platformId facebookId:(NSString*)facebookId success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSDictionary* objectRef = @{REQUEST_TYPE : @(objectType), REQUEST_ID : objectId};
    [params setValue:objectRef forKey:REQUEST_OBJECT_REF];
    [params setValue:message forKey:REQUEST_FACEBOOK_MESSAGE];
    //[params setValue:platformId forKey:REQUEST_PLATFORM_ID];
    [params setValue:facebookId forKey:REQUEST_FACEBOOK_ID];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    return dataResult;
}

@end

//
//  BaseRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "BaseRequest.h"
#import "AFNetworking.h"
#import "CoreConstants.h"
#import "RequestErrorCode.h"
#import "Log.h"
#import "Helpers.h"
#import "RequestManager.h"
#import "SessionManager.h"
#import "CoreStringUtils.h"

@implementation BaseRequest

static NSString* const TAG = @"BaseRequest";
static NSInteger const MAX_RETRY_SESSION_EXPIRED = 1;
static NSInteger const MAX_RETRY_NETWORK_SLOW = 1;

- (instancetype)init:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.url = url;
    self.success = success;
    self.failure = failure;
    self.retrySessionExpired = 0;
    self.retryNetworkSlow = 0;
    self.jsonData = @"";
    self.signature = @"";
    self.timeStamp = @"";
    self.method = POST;
    
    return self;
}

- (instancetype)init:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure progress:(ProgressCallback)progress {
    self = [self init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    self.progress = progress;
    
    return self;
}

- (instancetype)initGET:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [self init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    self.jsonData = @"";
    self.method = GET;
    
    return self;
}

- (instancetype)initDELETE:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [self init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    self.jsonData = @"";
    self.method = DELETE;
    
    return self;
}

- (instancetype)init:(NSString*)url file:(NSData*)file success:(SuccessCallback)success failure:(FailureCallback)failure progress:(ProgressCallback)progress {
    self = [self init:url success:success failure:failure progress:progress];
    
    if (!self) {
        return nil;
    }
    self.isUpload = YES;
    self.mimeType = @"image/jpeg";
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    //NSString *str = [[NSString alloc] initWithData:file encoding:NSUTF8StringEncoding];
    
    //[params setObject:file forKey:@"List product image"];
    self.params = nil;
    //self.jsonData = [Helpers convertToJson:params];
    self.data = file;
    
    return self;
}

- (instancetype)init:(NSString*)url file:(NSData*)file success:(SuccessCallback)success failure:(FailureCallback)failure {
    return [self init:url file:file success:success failure:failure progress:nil];
}
    
- (NSMutableDictionary*)getParams {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSString* removedPrefixUrl = [self.url stringByReplacingOccurrencesOfString:GPOS_ADMIN_URL withString:@""];
    NSString* data = removedPrefixUrl;
    if (data.length > 10) {
        data = [self.url substringFromIndex:MAX((int)[self.url length] - 10, 0)];
    }
    
    NSString* basic = [Helpers getBasic:[RequestManager sharedClient].username andPassword:[RequestManager sharedClient].password];
    self.contentType = @"application/json";
    self.authorization = [NSString stringWithFormat:@"Basic %@", basic];
    self.token = [Helpers getToken:basic];
    self.device = @"3A-9BE37F1696E28";
    
    self.timeStamp = [Helpers getCurrentTimeStamp];
    self.signature = [Helpers encryptDataSHA256:[NSString stringWithFormat:@"%@%@",  self.timeStamp , data] key:SECRECT_KEY];
    
    if ([self.url containsString:SETTINGS_URI] && self.jsonData.length > 0 && [[self.jsonData substringToIndex:1] isEqualToString:@"["]) {
        self.arrayParams = self.jsonData;
    } else {
        params = [JSONUtils parseJsonToMap:self.jsonData];
    }
    
    [Log d:0 tag:TAG message:self.jsonData];
    return params;
}

- (void)addToPosQueue {
    
    void (^success)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [Log d:0 tag:TAG message:responseObject];
        
        if (responseObject == nil) {
            self.success(nil, 0);
            return;
        }
        
        if ([responseObject isKindOfClass:[NSString class]]) {
            self.failure(nil, UNKNOWN_EXCEPTION);
            [Helpers showToast:responseObject];
            return;
        }
        
        if ([responseObject isKindOfClass:[NSArray class]] || [responseObject isKindOfClass:[NSDictionary class]] || [responseObject isKindOfClass:[NSString class]] || [responseObject isKindOfClass:[NSNumber class]]) {
            NSMutableDictionary * dataResult = nil;
            @try {
                dataResult = [self parseNetworkRespone:responseObject];
            }
            @catch (NSException * e) {
                [Log d:0 tag:TAG message:@"Exception while parsing Network response!"];
                self.failure(nil, UNKNOWN_EXCEPTION);
                
            }
            
            /*
            if ([Helpers isNotNull:[responseObject objectForKey:RESPONSE_PAGING]]) {
                NSDictionary* paging = [Helpers getDictDiffNull:[responseObject objectForKey:RESPONSE_PAGING]];
                if ([Helpers isNotNull:[paging objectForKey:RESPONSE_NEXT]]) {
                    [dataResult setValue:@(YES) forKey:RESPONSE_HAS_MORE];
                }
            }
            */
            
            self.success(dataResult, 0);
            
        } else {
            self.failure(nil, UNKNOWN_EXCEPTION);
        }
    };
    
    void (^failure)(AFHTTPRequestOperation *operation, NSError *error) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [Log d:0 tag:TAG message:error];
        
        /*huydna: Just retry if error code is TIMEOUT*/
        if (error.code == NSURLErrorTimedOut)
        {
            self.retryNetworkSlow++;
            if (self.retryNetworkSlow <= MAX_RETRY_NETWORK_SLOW) {
                [self addToQueue];
            } else {
                self.failure(nil, UNKNOWN_EXCEPTION);
            }
        } else {
            /*Error code is not TIMEOUT, we don't need a retry*/
            
            NSHTTPURLResponse *response = operation.response;
            
            /* session is expired */
            if (response.statusCode == 401) {
                self.retrySessionExpired++;
                if (self.retrySessionExpired <= MAX_RETRY_SESSION_EXPIRED && [SessionManager sharedClient].sessionKey.length > 0) {
                    [[RequestManager sharedClient] addToWaitingQueue:self];
                    [[SessionManager sharedClient] setSessionExpired];
                } else {
                    self.failure(nil, SESSION_TIMEOUT);
                }
            } else {
                self.failure(nil, (int)response.statusCode);
            }
        }
        /*~huydna*/
    };
    
    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"url: %@", self.url]];
    self.params = [self getParams];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15; //15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    switch (self.method) {
        case POST:
            [manager POST_GPOS:self.url parameters:self.params authorization:self.authorization token:self.token device:self.device success:success failure:failure];
            break;
        case GET:
            [manager GET_GPOS:self.url parameters:self.params authorization:self.authorization token:self.token device:self.device success:success failure:failure];
            break;
        case PUT:
            if ([self.url containsString:SETTINGS_URI]) {
                [manager PUT_GPOS:self.url parameters:self.arrayParams authorization:self.authorization token:self.token device:self.device success:success failure:failure];
            } else {
                [manager PUT_GPOS:self.url parameters:self.params authorization:self.authorization token:self.token device:self.device success:success failure:failure];
            }
            break;
        case DELETE:
            [manager DELETE_GPOS:self.url parameters:self.params authorization:self.authorization token:self.token device:self.device success:success failure:failure];
            break;
        default:
            break;
    }
}

- (void)addToQueue {
    
    void (^success)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [Log d:0 tag:TAG message:responseObject];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary* response = (NSDictionary*)responseObject;
            int errorCode = (int)[[response objectForKey:RESPONSE_ERROR_CODE] integerValue];
            NSString* message = [response objectForKey:RESPONSE_MSG];
            if (message != nil && ![message isEqual:[NSNull null]] && ![CoreStringUtils isEmpty:message]) {
                [Helpers showToast:message];
            }
            
            switch (errorCode) {
                case NO_ERROR: {
                    /*huydna: Handling errors while parse network response*/
                    NSMutableDictionary * dataResult = nil;
                    @try {
                         dataResult = [self parseNetworkRespone:[responseObject objectForKey:RESPONSE_DATA]];
                    }
                    @catch (NSException * e) {
                        [Log d:0 tag:TAG message:@"Exception while parsing Network response!"];
                        self.failure(nil, UNKNOWN_EXCEPTION);
                        break;
                    }
                    if ([Helpers isNotNull:[responseObject objectForKey:RESPONSE_PAGING]]) {
                        NSDictionary* paging = [Helpers getDictDiffNull:[responseObject objectForKey:RESPONSE_PAGING]];
                        if ([Helpers isNotNull:[paging objectForKey:RESPONSE_NEXT]]) {
                            [dataResult setValue:@(YES) forKey:RESPONSE_HAS_MORE];
                        }
                    }
                    self.success(dataResult, errorCode);
                    /*~huydna*/
                    
                    break;
                }
                case SESSION_TIMEOUT:
                    self.retrySessionExpired++;
                    if (self.retrySessionExpired <= MAX_RETRY_SESSION_EXPIRED && [SessionManager sharedClient].sessionKey.length > 0) {
                        [[RequestManager sharedClient] addToWaitingQueue:self];
                        [[SessionManager sharedClient] setSessionExpired];
                    } else {
                        self.failure(nil, errorCode);
                    }
                    
                    break;
                case PASSWORD_EXPIRE: {
                    NSDictionary *data = [responseObject objectForKey:RESPONSE_DATA];
                    bool hasSessionKey = false;
                    if ([Helpers isNotNull:data])
                    {
                        NSString *sessionKey = [data objectForKey:RESPONSE_SESSIONKEY];
                        if ([Helpers isNotNull:sessionKey])
                        {
                            [SessionManager sharedClient].sessionKey = sessionKey;
                            hasSessionKey = true;
                        }
                    }
                    if (hasSessionKey == false) {
                        [Log d:0 tag:TAG message:@"PASSWORD_EXPIRE but no sessionKey returned???"];
                    }
                    self.failure(nil, errorCode);
                    
                    break;
                }
                default: {
                    NSMutableDictionary * dataResult = nil;
                    @try {
                        dataResult = [self parseNetworkRespone:[responseObject objectForKey:RESPONSE_DATA]];
                    }
                    @catch (NSException * e) {
                        [Log d:0 tag:TAG message:@"Exception while parsing Network response!"];
                        self.failure(nil, UNKNOWN_EXCEPTION);
                        break;
                    }

                    self.failure(dataResult, errorCode);
                    break;
                }
            }
        } else {
            self.failure(nil, UNKNOWN_EXCEPTION);
        }
    };
    
    
    void (^failure)(AFHTTPRequestOperation *operation, NSError *error) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [Log d:0 tag:TAG message:error];
        
        /*huydna: Just retry if error code is TIMEOUT*/
        if (error.code == NSURLErrorTimedOut)
        {
            self.retryNetworkSlow++;
            if (self.retryNetworkSlow <= MAX_RETRY_NETWORK_SLOW) {
                [self addToQueue];
            } else {
                self.failure(nil, UNKNOWN_EXCEPTION);
            }
        } else {
            /*Error code is not TIMEOUT, we don't need a retry*/
            
            NSHTTPURLResponse *response = operation.response;
            
            /* session is expired */
            if (response.statusCode == 401) {
                self.retrySessionExpired++;
                if (self.retrySessionExpired <= MAX_RETRY_SESSION_EXPIRED && [SessionManager sharedClient].sessionKey.length > 0) {
                    [[RequestManager sharedClient] addToWaitingQueue:self];
                    [[SessionManager sharedClient] setSessionExpired];
                } else {
                    self.failure(nil, SESSION_TIMEOUT);
                }
            } else {
                self.failure(nil, (int)response.statusCode);
            }
        }
        /*~huydna*/
    };
    
    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"url: %@", self.url]];
    self.params = [self getParams];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15; //15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    switch (self.method) {
        case POST:
            [manager POST:self.url parameters:self.params apiKey:API_KEY signature:self.signature timeStamp:self.timeStamp sessionKey:[SessionManager sharedClient].sessionKey success:success failure:failure];
            break;
        case GET:
            [manager GET:self.url parameters:self.params apiKey:API_KEY signature:self.signature timeStamp:self.timeStamp sessionKey:[SessionManager sharedClient].sessionKey success:success failure:failure];
           
            break;
        case PUT:
            [manager PUT:self.url parameters:self.params apiKey:API_KEY signature:self.signature timeStamp:self.timeStamp sessionKey:[SessionManager sharedClient].sessionKey success:success failure:failure];
            break;
        case DELETE:
            [manager DELETE:self.url parameters:self.params apiKey:API_KEY signature:self.signature timeStamp:self.timeStamp sessionKey:[SessionManager sharedClient].sessionKey success:success failure:failure];
            break;
        default:
            break;
    }   
}

- (void)addToUploadQueue {
    self.params = [self getParams];
    
    void (^success)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject) {
        [Log d:0 tag:TAG message:responseObject];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary* response = (NSDictionary*)responseObject;
            int errorCode = (int)[[response objectForKey:RESPONSE_ERROR_CODE] integerValue];
            switch (errorCode) {
                case WAITING_APPROVAL:
                    
                case NO_ERROR: {
                    /*huydna: Handling errors while parse network response*/
                    NSMutableDictionary * dataResult = nil;
                    @try {
                        dataResult = [self parseNetworkRespone:[responseObject objectForKey:RESPONSE_DATA]];
                    }
                    @catch (NSException * e) {
                        [Log d:0 tag:TAG message:@"Exception while parsing Network response!"];
                        self.failure(nil, UNKNOWN_EXCEPTION);
                        break;
                    }
                    self.success(dataResult, errorCode);
                    /*~huydna*/
                    
                    break;
                }
                case SESSION_TIMEOUT:
                    self.retrySessionExpired++;
                    if (self.retrySessionExpired <= MAX_RETRY_SESSION_EXPIRED && [SessionManager sharedClient].sessionKey.length > 0) {
                        [[RequestManager sharedClient] addToWaitingQueue:self];
                        [[SessionManager sharedClient] setSessionExpired];
                    } else {
                        self.failure(nil, errorCode);
                    }
                    
                    break;
                    
                case PASSWORD_EXPIRE: {
                    NSDictionary *data = [responseObject objectForKey:RESPONSE_DATA];
                    bool hasSessionKey = false;
                    if ([Helpers isNotNull:data])
                    {
                        NSString *sessionKey = [data objectForKey:RESPONSE_SESSIONKEY];
                        if ([Helpers isNotNull:sessionKey])
                        {
                            [SessionManager sharedClient].sessionKey = sessionKey;
                            hasSessionKey = true;
                        }
                    }
                    if (hasSessionKey == false) {
                        [Log d:0 tag:TAG message:@"PASSWORD_EXPIRE but no sessionKey returned???"];
                    }
                    self.failure(nil, errorCode);
                    break;
                }
                default:
                    self.failure(nil, errorCode);
                    break;
            }
        } else {
            self.failure(nil, UNKNOWN_EXCEPTION);
        }
    };
    
    void (^failure)(AFHTTPRequestOperation *operation, NSError *error) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        [Log d:0 tag:TAG message:error];
        
        if (error.code == NSURLErrorTimedOut)
        {
            self.retryNetworkSlow++;
            if (self.retryNetworkSlow <= MAX_RETRY_NETWORK_SLOW) {
                [self addToQueue];
            } else {
                self.failure(nil, UNKNOWN_EXCEPTION);
            }
        } else {
            /*Error code is not TIMEOUT, we don't need a retry*/
            
            NSHTTPURLResponse *response = operation.response;
            
            /* session is expired */
            if (response.statusCode == 401) {
                self.retrySessionExpired++;
                if (self.retrySessionExpired <= MAX_RETRY_SESSION_EXPIRED && [SessionManager sharedClient].sessionKey.length > 0) {
                    [[RequestManager sharedClient] addToWaitingQueue:self];
                    [[SessionManager sharedClient] setSessionExpired];
                } else {
                    self.failure(nil, SESSION_TIMEOUT);
                }
            } else {
                self.failure(nil, (int)response.statusCode);
            }
        }
    };
    
    void (^constructingBodyWithBlock)(id<AFMultipartFormData> formData) = ^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:self.data
                                    name:@"file"
                                fileName:@"Upload" mimeType:self.mimeType];
        
    };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 1500000; //15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"url: %@", self.url]];

    switch (self.method) {
        case PUT:{
            //[manager PUT:self.url parameters:self.params apiKey:API_KEY signature:self.signature timeStamp:self.timeStamp sessionKey:[SessionManager sharedClient].sessionKey constructingBodyWithBlock:constructingBodyWithBlock success:success failure:failure];
        }
            break;
        case POST:{
            //[manager POST:self.url parameters:self.params apiKey:API_KEY signature:self.signature timeStamp:self.timeStamp sessionKey:[SessionManager sharedClient].sessionKey constructingBodyWithBlock:constructingBodyWithBlock success:success failure:failure];
        }
            break;
            
        default:{
            //[manager POST:self.url parameters:self.params apiKey:API_KEY signature:self.signature timeStamp:self.timeStamp sessionKey:[SessionManager sharedClient].sessionKey constructingBodyWithBlock:constructingBodyWithBlock success:success failure:failure];
        }
            break;
    }
}

- (void)addToProgressUploadQueue {
    //self.params =
    [self getParams];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 1500000; //15;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];

    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    
    NSString* method = @"POST";
    
    switch (self.method) {
        case PUT:
            method = @"PUT";
            break;
        case POST:
            method = @"POST";
            break;
        default:
            break;

    }
    
    

    NSString *fileName = [NSString stringWithFormat:@"received_%08X.jpg", arc4random()];
    
    /*
    NSMutableURLRequest *request = [serializer requestWithMethod:method URLString:self.url parameters:self.params error:nil];
    */
    
    
    NSMutableURLRequest *request =
    [serializer multipartFormRequestWithMethod:method URLString:self.url
                                    parameters:self.params
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                         [formData appendPartWithFileData:self.data
                                                     name:@"List product image"
                                                 fileName:fileName mimeType:self.mimeType];
                     } error:nil];
    
    [request setValue:self.authorization forHTTPHeaderField:@"Authorization"];
    [request setValue:self.token forHTTPHeaderField:@"Token"];
    [request setValue:self.device forHTTPHeaderField:@"Device"];
    
    
    
//    NSMutableData *body = [NSMutableData data];
//
//    NSString *boundary = [NSString stringWithFormat:@"Boundary+%08X%08X", arc4random(), arc4random()];
//
//
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
//    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
//
//    // file
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"Content-Disposition: form-data; name=\"List product image\"; filename=\"abc.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[NSData dataWithData:self.data]];
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//
//    // close form
//    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//
//    [request setHTTPBody:body];
    

    AFHTTPRequestOperation *operation =
    [manager HTTPRequestOperationWithRequest:request
        success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [Log d:0 tag:TAG message:responseObject];
        
            
        NSString* message = responseObject;
       
            
        NSString *uploadedUrl = [NSString stringWithFormat:@"%@_%@", [RequestManager sharedClient].storeId, fileName];
        
        NSMutableDictionary * dataResult = [[NSMutableDictionary alloc] init];;
        [dataResult setValue:uploadedUrl forKey:RESPONSE_DATA];
        self.success(dataResult, 0);
            
//        if ([responseObject isKindOfClass:[NSDictionary class]]) {
//            NSDictionary* response = (NSDictionary*)responseObject;
//            int errorCode = (int)[[response objectForKey:RESPONSE_ERROR_CODE] integerValue];
//            switch (errorCode) {
//                case WAITING_APPROVAL:
//
//                case NO_ERROR: {
//                    /*huydna: Handling errors while parse network response*/
//                    NSMutableDictionary * dataResult = nil;
//                    @try {
//                        dataResult = [self parseNetworkRespone:[responseObject objectForKey:RESPONSE_DATA]];
//                    }
//                    @catch (NSException * e) {
//                        [Log d:0 tag:TAG message:@"Exception while parsing Network response!"];
//                        self.failure(nil, UNKNOWN_EXCEPTION);
//                        break;
//                    }
//                    self.success(dataResult, errorCode);
//                    /*~huydna*/
//
//                    break;
//                }
//                case SESSION_TIMEOUT:
//                    self.retrySessionExpired++;
//                    if (self.retrySessionExpired <= MAX_RETRY_SESSION_EXPIRED && [SessionManager sharedClient].sessionKey.length > 0) {
//                        [[RequestManager sharedClient] addToWaitingQueue:self];
//                        [[SessionManager sharedClient] setSessionExpired];
//                    } else {
//                        self.failure(nil, errorCode);
//                    }
//
//                    break;
//
//                case PASSWORD_EXPIRE: {
//                    NSDictionary *data = [responseObject objectForKey:RESPONSE_DATA];
//                    bool hasSessionKey = false;
//                    if ([Helpers isNotNull:data])
//                    {
//                        NSString *sessionKey = [data objectForKey:RESPONSE_SESSIONKEY];
//                        if ([Helpers isNotNull:sessionKey])
//                        {
//                            [SessionManager sharedClient].sessionKey = sessionKey;
//                            hasSessionKey = true;
//                        }
//                    }
//                    if (hasSessionKey == false) {
//                        [Log d:0 tag:TAG message:@"PASSWORD_EXPIRE but no sessionKey returned???"];
//                    }
//                    self.failure(nil, errorCode);
//                    break;
//                }
//                default:
//                    self.failure(nil, errorCode);
//                    break;
//            }
//        } else {
//            self.failure(nil, UNKNOWN_EXCEPTION);
//        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Log d:0 tag:TAG message:error];
        
        if (error.code == NSURLErrorTimedOut)
        {
            self.retryNetworkSlow++;
            if (self.retryNetworkSlow <= MAX_RETRY_NETWORK_SLOW) {
                [self addToQueue];
            } else {
                self.failure(nil, UNKNOWN_EXCEPTION);
            }
        } else {
            /*Error code is not TIMEOUT, we don't need a retry*/
            
            NSHTTPURLResponse *response = operation.response;
            
            /* session is expired */
            if (response.statusCode == 401) {
                self.retrySessionExpired++;
                if (self.retrySessionExpired <= MAX_RETRY_SESSION_EXPIRED && [SessionManager sharedClient].sessionKey.length > 0) {
                    [[RequestManager sharedClient] addToWaitingQueue:self];
                    [[SessionManager sharedClient] setSessionExpired];
                } else {
                    self.failure(nil, SESSION_TIMEOUT);
                }
            } else {
                self.failure(nil, (int)response.statusCode);
            }
        }
    }];
    
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                        long long totalBytesWritten,
                                        long long totalBytesExpectedToWrite) {
        self.progress(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);        
    }];
    
    [Log d:0 tag:TAG message:[NSString stringWithFormat:@"url: %@", self.url]];

    // 5. Begin!
    [operation start];    
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    return nil;
}

@end

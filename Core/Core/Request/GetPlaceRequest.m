//
//  GetPlaceRequest.m
//  Core
//
//  Created by Dragon on 3/6/17.
//  Copyright © 2017 ToanPham. All rights reserved.
//

#import "GetPlaceRequest.h"
#import "AFNetworking.h"
#import "RequestErrorCode.h"
#import "Log.h"
#import "CoreConstants.h"
#import "Helpers.h"
#import "RequestManager.h"
#import "AddressEntity.h"

static NSString* const TAG = @"BaseRequest";

@implementation GetPlaceRequest

- (instancetype)init:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    return self;
}

- (void)addToQueue {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:self.url
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             [Log d:0 tag:TAG message:responseObject];
             @try {
                 NSMutableDictionary * dataResult = [self parseNetworkRespone:responseObject];
                 self.success(dataResult, 0);
             }
             @catch (NSException * e) {
                 [Log d:0 tag:TAG message:@"Exception while parsing Network response!"];
                 self.failure(nil, UNKNOWN_EXCEPTION);
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [Log d:0 tag:TAG message:error];
             self.failure(nil, UNKNOWN_EXCEPTION);
         }];
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSMutableDictionary* result = [responseObject objectForKey:@"result"];
    NSDictionary *geometry = [result objectForKey:@"geometry"];
    NSDictionary *location = [geometry objectForKey:@"location"];
    
    AddressEntity *addressEntiy = [[AddressEntity alloc] init];
    addressEntiy.lon = [Helpers getDoubleDiffNull:[location objectForKey:@"lng"]];;
    addressEntiy.lat = [Helpers getDoubleDiffNull:[location objectForKey:@"lat"]];;
    addressEntiy.formatedAddress = [CoreStringUtils getStringDiffNull:[result objectForKey:@"formatted_address"]];
    
    NSArray *addressComponents = [result objectForKey:@"address_components"];
    for (int i= 0; i < addressComponents.count; i++) {
        NSDictionary* addressComponent = (NSDictionary*)[addressComponents objectAtIndex:i];
        NSArray *types = [addressComponent objectForKey:@"types"];
    
        if ([[types objectAtIndex:0] isEqualToString:@"administrative_area_level_1"]) {
            addressEntiy.state = [addressComponent objectForKey:@"long_name"];
        } else if ([[types objectAtIndex:0] isEqualToString:@"country"]) {
            addressEntiy.country = [addressComponent objectForKey:@"long_name"];
        } else if ([[types objectAtIndex:0] isEqualToString:@"locality"]) {
            addressEntiy.city = [addressComponent objectForKey:@"long_name"];
        } else if ([[types objectAtIndex:0] isEqualToString:@"postal_code"]) {
            addressEntiy.zipCode = [addressComponent objectForKey:@"long_name"];
        }
    }
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:addressEntiy forKey:RESPONSE_ADDRESS_ENTITY];
    
    return dataResult;
}

@end

//
//  SubmitRatingRequest.h
//  Core
//
//  Created by Dragon on 3/18/17.
//  Copyright © 2017 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

@interface SubmitRatingRequest : BaseRequest

- (instancetype)init:(NSString*)url lanscapingId:(NSString*)lanscapingId comment:(NSString*)comment point:(double)point userId:(NSString*)userId success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

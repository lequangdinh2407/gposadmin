//
//  SubmitRatingRequest.m
//  Core
//
//  Created by Dragon on 3/18/17.
//  Copyright © 2017 ToanPham. All rights reserved.
//

#import "SubmitRatingRequest.h"

@implementation SubmitRatingRequest

- (instancetype)init:(NSString*)url lanscapingId:(NSString*)lanscapingId comment:(NSString*)comment point:(double)point userId:(NSString*)userId success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    
    if (![CoreStringUtils isEmpty:comment])
        [params setObject:comment forKey:REQUEST_COMMENT];
    [params setObject:userId forKey:REQUEST_USER_ID];
    [params setObject:@(point) forKey:REQUEST_POINT];
    
    NSDictionary* objectRef = @{REQUEST_TYPE : @(9), REQUEST_ID : lanscapingId};
    [params setObject:objectRef forKey:REQUEST_OBJECT_REF];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        
    }
    return nil;
}


@end

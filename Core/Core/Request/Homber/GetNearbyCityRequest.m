//
//  GetGardenAddressRequest.m
//  Core
//
//  Created by Dragon on 3/31/17.
//  Copyright © 2017 ToanPham. All rights reserved.
//

#import "GetNearbyCityRequest.h"
#import "EntityManager.h"

@implementation GetNearbyCityRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    if ([responseObject isKindOfClass:[NSArray class]]) {
        NSArray *data = (NSArray*)responseObject;
        NSMutableArray* cities = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < data.count; i++) {
            CityEntity* city = (CityEntity*)[[EntityManager sharedClient] createItem:data[i] type:CITY_ENTITY];
            [cities addObject:city];
        }
        
        NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
        [dataResult setValue:cities forKey:RESPONSE_DATA];
        return dataResult;
    }
    return nil;
}

@end

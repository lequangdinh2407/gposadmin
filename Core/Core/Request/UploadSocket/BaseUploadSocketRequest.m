//
//  BaseUploadSocketRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 4/29/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "BaseUploadSocketRequest.h"
#import "RequestManager.h"
#import "CoreConstants.h"

#define TAG @"BaseUploadSocketRequest"

@implementation BaseUploadSocketRequest


- (instancetype)init:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:nil success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    self.command = nil;
    
    if (params != nil && [params isKindOfClass:[NSDictionary class]])
    {
        self.jsonData = [JSONUtils convertToJson:params];
//        [Log d:0 tag:TAG message:[NSString stringWithFormat:@"content=%@", self.jsonData]];
    }
    
    return self;
}


- (SocketCommand*)getCommand {
    return self.command;
}

- (void)addToQueue {
    [[UploadSocketManager sharedClient] addToQueue:self];
}

- (NSMutableDictionary*)receiveResponse:(SocketCommand*)responseCommand {
    return nil;
}

@end
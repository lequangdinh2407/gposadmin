//
//  UploadFileRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 5/2/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseUploadSocketRequest.h"

@interface UploadFileRequest : BaseUploadSocketRequest

@property (nonatomic) uint64_t uploadID;
@property (nonatomic) uint32_t partID;
@property (nonatomic, strong) NSData* data;

- (instancetype)init:(uint64_t)uploadID partID:(uint32_t)partID data:(NSData*)data success:(SuccessCallback)success failure:(FailureCallback)failure;

@end
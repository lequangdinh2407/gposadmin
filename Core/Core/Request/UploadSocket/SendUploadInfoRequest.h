//
//  SendUploadInfoRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 5/2/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseUploadSocketRequest.h"

@interface SendUploadInfoRequest : BaseUploadSocketRequest

- (instancetype)init:(uint16_t)totalPart size:(uint64_t)size type:(uint8_t)type success:(SuccessCallback)success failure:(FailureCallback)failure;

@end
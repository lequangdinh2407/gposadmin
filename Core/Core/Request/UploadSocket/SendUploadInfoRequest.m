//
//  SendUploadInfoRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 5/2/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "SendUploadInfoRequest.h"
#import "CommandBuilder.h"

@implementation SendUploadInfoRequest

- (instancetype)init:(uint16_t)totalPart size:(uint64_t)size type:(uint8_t)type success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:@(totalPart) forKey:REQUEST_TOTAL];
    [params setValue:@(size) forKey:REQUEST_SIZE];
    [params setValue:@(type) forKey:REQUEST_TYPE];
    
    self = [super init:params success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary *response = (NSDictionary*)responseObject;
        uint64_t idUpload = [Helpers getULongLongDiffNull:[response objectForKey:RESPONSE_UPLOADFILE_ID]];
        NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
        [result setValue:@(idUpload) forKey:RESPONSE_UPLOADFILE_ID];
        return result;
    }
    return nil;
}

- (SocketCommand*)getCommand {
    if (self.command != nil)
        return self.command;
    
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    NSData *content = [self.jsonData dataUsingEncoding:NSUTF8StringEncoding];
    
    [dict setValue:@(SEND_UPLOAD_INFO_CMD) forKey:HDR_CMD];
    [dict setValue:@([Helpers getReqIDFormPref]) forKey:HDR_REQ_ID];
    [dict setValue:@([content length]) forKey:HDR_CONTENT_LENGTH];
    [dict setValue:content forKey:HDR_CONTENT];
    
    self.command = [[CommandBuilder sharedClient] createCommand:dict type:SEND_UPLOAD_INFO_CMD];
    return self.command;
}

@end
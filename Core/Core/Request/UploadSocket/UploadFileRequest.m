//
//  UploadFileRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 5/2/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "UploadFileRequest.h"
#import "CommandBuilder.h"
#import "UploadFileCommand.h"

@implementation UploadFileRequest

- (instancetype)init:(uint64_t)uploadID partID:(uint32_t)partID data:(NSData*)data success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];

    self = [super init:params success:success failure:failure];
    
    self.uploadID = uploadID;
    self.partID = partID;
    self.data = data;
    
    if (!self) {
        return nil;
    }
    
    return self;
}

- (NSMutableDictionary*)receiveResponse:(SocketCommand*)responseCommand {
    UploadFileCommand* uploadCommand = (UploadFileCommand*)responseCommand;
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setValue:@(uploadCommand.uploadID) forKey:RESPONSE_UPLOADFILE_ID];
    [result setValue:@(uploadCommand.partID) forKey:RESPONSE_UPLOADFILE_PART];
    [result setValue:uploadCommand.url forKey:RESPONSE_UPLOADFILE_URL];
    
    return result;
}

- (SocketCommand*)getCommand {
    if (self.command != nil)
        return self.command;
    
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    
    unsigned long length = 8 + 4 + self.data.length;
    
    uint8_t* data = malloc(8 + 4 + self.data.length);
    uint8_t* head = data;
    
    *(uint64_t*)head = CFSwapInt64HostToBig(self.uploadID);
    head += sizeof(self.uploadID);
    
    *(uint32_t*)head = CFSwapInt32HostToBig(self.partID);
    head += sizeof(self.partID);
    
    memcpy(head, [self.data bytes], self.data.length);
    
    NSData* content = [[NSData alloc] initWithBytes:data length:length];
    
    free(data);
    
    [dict setValue:@(UPLOAD_FILE_CMD) forKey:HDR_CMD];
    [dict setValue:@([Helpers getReqIDFormPref]) forKey:HDR_REQ_ID];
    [dict setValue:@(length) forKey:HDR_CONTENT_LENGTH];
    [dict setValue:content forKey:HDR_CONTENT];
    
    self.command = [[CommandBuilder sharedClient] createCommand:dict type:UPLOAD_FILE_CMD];
    return self.command;
}

@end
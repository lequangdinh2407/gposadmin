//
//  UploadSocketRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 4/29/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "UploadSocketRequest.h"
#import "UploadSocketManager.h"

@implementation UploadSocketRequest

- (void)addToQueue {
    [[UploadSocketManager sharedClient] connect:self];
}

@end
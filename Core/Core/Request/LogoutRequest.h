//
//  LogoutRequest.h
//  Core
//
//  Created by ToanPham on 5/13/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface LogoutRequest : BaseRequest

- (instancetype)init:(NSString*)url deviceId:(NSString*)deviceId success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

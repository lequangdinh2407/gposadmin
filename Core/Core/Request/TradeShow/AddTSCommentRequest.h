//
//  GetAddressRequest.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface AddTSCommentRequest : BaseRequest

- (instancetype)init:(NSString*)url objectType:(int)objectType objectId:(NSString*)objectId feedPhotoId:(NSString*)feedPhotoId text:(NSString*)text sticker:(NSString*)sticker photoId:(NSString*)photoId success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

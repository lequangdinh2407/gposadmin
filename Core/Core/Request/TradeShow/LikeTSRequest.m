
//
//  GetNewsRequest.m
//  Core
//
//  Created by ToanPham on 1/19/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "LikeTSRequest.h"
#import "Helpers.h"

@implementation LikeTSRequest

- (instancetype)init:(NSString*)url objectType:(int)objectType objectId:(NSString*)objectId photoId:(NSString*)photoId type:(int)type success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    NSDictionary* objectRef = @{REQUEST_TYPE : @(objectType), REQUEST_ID : objectId};
    [params setObject:objectRef forKey:REQUEST_OBJECT_REF];
    [params setObject:@(type) forKey:REQUEST_TYPE];
    if (![CoreStringUtils isEmpty:photoId]) {
        [params setObject:photoId forKey:REQUEST_PHOTO_ID];
    }
    
    self.jsonData = [Helpers convertToJson:params];
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary *data = (NSDictionary*)responseObject;
        int totalLike = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_TOTAL_LIKES]];
        NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
        [dataResult setValue:@(totalLike) forKey:RESPONSE_TOTAL_LIKE];
        return dataResult;
    }
    return nil;
}

@end

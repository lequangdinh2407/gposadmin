//
//  GetBannerRequest.h
//  Core
//
//  Created by ToanPham on 1/19/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface RatingTSRequest : BaseRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey objectType:(int)objectType objectId:(NSString*)objectId point:(float)point success:(SuccessCallback)success failure:(FailureCallback)failure;
- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey objectType:(int)objectType objectId:(NSString*)objectId success:(SuccessCallback)success failure:(FailureCallback)failure;

@end
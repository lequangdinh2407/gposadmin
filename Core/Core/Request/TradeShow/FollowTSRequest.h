//
//  GetNewsRequest.h
//  Core
//
//  Created by ToanPham on 1/19/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface FollowTSRequest : BaseRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey objectType:(int)objectType objectId:(NSString*)objectId photoId:(NSString*)photoId type:(int)type success:(SuccessCallback)success failure:(FailureCallback)failure;

@end
//
//  GetAddressRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "AddTSCommentRequest.h"
#import "EntityManager.h"

@implementation AddTSCommentRequest

- (instancetype)init:(NSString*)url objectType:(int)objectType objectId:(NSString*)objectId feedPhotoId:(NSString*)feedPhotoId text:(NSString*)text sticker:(NSString*)sticker photoId:(NSString*)photoId success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSDictionary* objectRef = @{REQUEST_TYPE: @(objectType), REQUEST_ID : objectId};
    [params setObject:objectRef forKey:REQUEST_OBJECT_REF];
    if (![CoreStringUtils isEmpty:text]) {
        [params setObject:text forKey:REQUEST_TEXT];
    }
    if (![CoreStringUtils isEmpty:feedPhotoId]) {
        [params setObject:feedPhotoId forKey:REQUEST_FEED_PHOTO_ID];
    }
    if (![CoreStringUtils isEmpty:photoId]) {
        [params setObject:photoId forKey:REQUEST_PHOTO_ID];
    }
    if (![CoreStringUtils isEmpty:sticker]) {
        [params setObject:sticker forKey:REQUEST_STICKER];
    }
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    
    CommentEntity* feed = (CommentEntity*)[[EntityManager sharedClient] createItem:[data objectForKey:RESPONSE_COMMENT] type:COMMENT_ENTITY];
    
    NSInteger totalComment = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_TOTAL_COMMENT]];
    NSInteger childCount = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_TOTAL_CHILD]];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:feed forKey:RESPONSE_COMMENT];
    [dataResult setValue:@(totalComment) forKey:RESPONSE_TOTAL_COMMENT];
    [dataResult setValue:@(childCount) forKey:RESPONSE_TOTAL_CHILD];
    
    return dataResult;
}

@end

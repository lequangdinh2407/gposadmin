//
//  GetListCountriesRequest.m
//  spabeecore
//
//  Created by ToanPham on 2/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "GetListCitiesRequest.h"
#import "EntityManager.h"

@implementation GetListCitiesRequest

- (instancetype)init:(NSString*)url state:(NSString*)isoCode success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:isoCode forKey:REQUEST_STATE];
    
    self.jsonData = [JSONUtils convertToJson:params];
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    NSArray* countries = [Helpers getArrayDiffNull:[data objectForKey:RESPONSE_CITIES]];
    NSMutableArray* statesList = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < countries.count; i++) {
        CityEntity *stateEntity = (CityEntity*) [[EntityManager sharedClient] createItem:[countries objectAtIndex:i] type:CITY_ENTITY];
        [statesList addObject:stateEntity];
    }
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:statesList forKey:RESPONSE_CITIES];
    
    return dataResult;
}

@end

//
//  GetBannerRequest.m
//  Core
//
//  Created by ToanPham on 1/19/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "RatingTSRequest.h"
#import "Helpers.h"

@implementation RatingTSRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey objectType:(int)objectType objectId:(NSString*)objectId point:(float)point success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:sessionKey forKey:REQUEST_SESSION_KEY];
    [params setObject:@(objectType) forKey:REQUEST_OBJECT_TYPE];
    [params setObject:objectId forKey:REQUEST_OBJECT_ID];
    [params setObject:@(point) forKey:REQUEST_POINT];
    
    self.jsonData = [Helpers convertToJson:params];
    return self;
}

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey objectType:(int)objectType objectId:(NSString*)objectId success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:sessionKey forKey:REQUEST_SESSION_KEY];
    [params setObject:@(objectType) forKey:REQUEST_OBJECT_TYPE];
    [params setObject:objectId forKey:REQUEST_OBJECT_ID];
    
    self.jsonData = [Helpers convertToJson:params];
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary *data = (NSDictionary*)responseObject;
        
        float skuRatingPoint = [Helpers getDoubleDiffNull:[data objectForKey:RESPONSE_RATING_POINT]];
        int totalRating = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_TOTAL_RATING]];
        
        NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
        [dataResult setValue:@(skuRatingPoint) forKey:RESPONSE_RATING_POINT];
        [dataResult setValue:@(totalRating) forKey:RESPONSE_TOTAL_RATING];
        
        return dataResult;
    }
    return nil;   
}

@end
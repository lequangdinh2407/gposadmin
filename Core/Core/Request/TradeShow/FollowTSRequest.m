
//
//  GetNewsRequest.m
//  Core
//
//  Created by ToanPham on 1/19/16.
//  Copyright © 2016 ToanPham. All rights reserved.
//

#import "FollowTSRequest.h"
#import "Helpers.h"

@implementation FollowTSRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey objectType:(int)objectType objectId:(NSString*)objectId photoId:(NSString*)photoId type:(int)type success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    
    [params setObject:@(type) forKey:REQUEST_TYPE];
    
    self.jsonData = [Helpers convertToJson:params];
    self.method = PUT;
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary *data = (NSDictionary*)responseObject;
        
        int totalFollow = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_TOTAL_FOLLOW]];
        
        NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
        [dataResult setValue:@(totalFollow) forKey:RESPONSE_TOTAL_FOLLOW];
        
        return dataResult;
    }
    return nil;
}

@end

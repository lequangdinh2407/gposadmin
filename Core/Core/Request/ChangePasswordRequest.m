//
//  HTTPChangePasswordRequest.m
//  Core
//
//  Created by ToanPham on 5/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "ChangePasswordRequest.h"

@implementation ChangePasswordRequest

- (instancetype)init:(NSString*)url Id:(int)Id curPass:(NSString*)curPass newPass:(NSString*)newPass rePass:(NSString*)rePass type:(uint8_t)type success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:@(Id) forKey:@"id"];
    [params setValue:curPass forKey:@"currentPassword"];
    [params setValue:newPass forKey:@"password"];
    [params setValue:rePass forKey:@"confirmPassword"];
    [params setValue:@(type) forKey:REQUEST_TYPE];
    
    self.jsonData = [JSONUtils convertToJson:params];
    self.method = PUT;
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    return nil;
}

@end

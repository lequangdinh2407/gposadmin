//
//  LoginGGRequest.m
//  Core
//
//  Created by ToanPham on 6/9/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "LoginGGRequest.h"
#import "UserEntity.h"
#import "SessionManager.h"
#import "EntityManager.h"

@implementation LoginGGRequest

- (instancetype)init:(NSString*)url token:(NSString*)token success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:token forKey:REQUEST_TOKEN];
    [params setValue:@(3) forKey:REQUEST_TYPE];
    self.jsonData = [JSONUtils convertToJson:params];
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary* data = (NSDictionary*)responseObject;
        
        UserEntity *userEntity = (UserEntity*) [[EntityManager sharedClient] createItem:[data objectForKey:RESPONSE_PROFILE] type:USER_ENTITY];
        [SessionManager sharedClient].userEntity = userEntity;
        
        SocialProfileEntity* socialProfileEntity = (SocialProfileEntity*) [[EntityManager sharedClient] createItem:[data objectForKey:RESPONSE_SOCIAL_PROFILE] type:SOCIAL_PROFILE_ENTITY];
        [SessionManager sharedClient].socialProfile = socialProfileEntity;
        
        if (![CoreStringUtils isEmpty:userEntity.userId]) {
            [SessionManager sharedClient].socialProfile.userId = userEntity.userId;
        }
        
        NSString* sessionKey = [CoreStringUtils getStringDiffNull:[data objectForKey:RESPONSE_SESSIONKEY]];
        [SessionManager sharedClient].sessionKey = sessionKey;
        
        NSString* socket = [CoreStringUtils getStringDiffNull:[data objectForKey:RESPONSE_API_SOCKET]];
        [SessionManager sharedClient].socket = socket;
        
        NSString* refreshToken = [CoreStringUtils getStringDiffNull:[data objectForKey:RESPONSE_REFRESH_TOKEN]];
        [SessionManager sharedClient].refreshToken = refreshToken;
        
        [SharePrefUtils writeIntPreference:PREFKEY_LOGIN_TYPE prefValue:LOGIN_GG];
        
        NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
        return dataResult;
    }
    return nil;
}

@end

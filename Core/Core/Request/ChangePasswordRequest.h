//
//  HTTPChangePasswordRequest.h
//  Core
//
//  Created by ToanPham on 5/16/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface ChangePasswordRequest : BaseRequest

- (instancetype)init:(NSString*)url Id:(int)Id curPass:(NSString*)curPass newPass:(NSString*)newPass rePass:(NSString*)rePass type:(uint8_t)type success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

//
//  LoginRequest.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface RefreshTokenRequest : BaseRequest

- (instancetype)init:(NSString*)url refreshToken:(NSString*)refreshToken lon:(float)lon lat:(float)lat success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

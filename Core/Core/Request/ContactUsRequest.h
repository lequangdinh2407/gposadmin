//
//  SendFeedbackRequest.h
//  Core
//
//  Created by ToanPham on 6/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface ContactUsRequest : BaseRequest

- (instancetype)init:(NSString*)url params:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure;

@end
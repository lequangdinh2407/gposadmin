//
//  RequestCodeRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/23/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "RequestCodeRequest.h"

@implementation RequestCodeRequest

- (instancetype)init:(NSString*)url sendInfo:(NSString*)sendInfo imei:(NSString*)imei countryCode:(NSString*)countryCode success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:sendInfo forKey:REQUEST_SENDINFO];
    [params setValue:imei forKey:REQUEST_IMEI];
    [params setValue:countryCode forKey:REQUEST_COUNTRY_CODE];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    return dataResult;
}

@end

//
//  GetAddressRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "GetCountRelationshipRequest.h"
#import "EntityManager.h"
#import "UserEntity.h"
#import "EntityManager.h"

@implementation GetCountRelationshipRequest

/*
- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey userId:(NSString*)userId success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:sessionKey forKey:REQUEST_SESSION_KEY];
    [params setObject:userId forKey:REQUEST_USER_ID];
    self.jsonData = [JSONUtils convertToJson:params];

     
    return self;
}
 */

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    uint numFeed = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_NUMFEED]];
    uint numFollow = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_NUMFOLLOW]];
    uint numFollowed = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_NUMFOLLOWED]];
    
    double ratingPoint = [Helpers getDoubleDiffNull:[data objectForKey:RESPONSE_RATING_POINT]];
    int serveTotal = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_SERVE_TOTAL]];
    NSString* providerRegisterTime = [data objectForKey:RESPONSE_PROVIDER_REGISTER_TIME];
    
    UserEntity* user = (UserEntity*) [[EntityManager sharedClient] createItem:[data objectForKey:RESPONSE_USER] type:USER_ENTITY];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setObject:@(numFeed) forKey:RESPONSE_NUMFEED];
    [dataResult setObject:@(numFollow) forKey:RESPONSE_NUMFOLLOW];
    [dataResult setObject:@(numFollowed) forKey:RESPONSE_NUMFOLLOWED];
    
    [dataResult setObject:@(ratingPoint) forKey:RESPONSE_RATING_POINT];
    [dataResult setObject:@(serveTotal) forKey:RESPONSE_SERVE_TOTAL];
    [dataResult setObject:providerRegisterTime forKey:RESPONSE_PROVIDER_REGISTER_TIME];
    
    [dataResult setObject:user forKey:RESPONSE_USERINFO];
    
    return dataResult;
}

@end

//
//  GetAddressRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "GetListRelationshipRequest.h"
#import "EntityManager.h"

@implementation GetListRelationshipRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    NSArray* feedsArr = (NSArray*)[Helpers getArrayDiffNull:[data objectForKey:RESPONSE_USERS]];
    BOOL hasMore = [Helpers getBoolDiffNull:[data objectForKey:RESPONSE_HAS_MORE]];
    int totalUser = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_TOTAL_USER]];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:feedsArr forKey:RESPONSE_USERS];
    [dataResult setValue:@(hasMore) forKey:RESPONSE_HAS_MORE];
    [dataResult setValue:@(totalUser) forKey:RESPONSE_TOTAL_USER];
    return dataResult;
}

@end

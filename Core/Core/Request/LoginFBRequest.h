//
//  LoginFBRequest.h
//  Core
//
//  Created by ToanPham on 6/3/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface LoginFBRequest : BaseRequest

- (instancetype)init:(NSString*)url token:(NSString*)token success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

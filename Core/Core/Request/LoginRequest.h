//
//  LoginRequest.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface LoginRequest : BaseRequest

//- (instancetype)init:(NSString*)url token:(NSString*)token type:(int)type lon:(float)lon lat:(float)lat imei:(NSString*)imei success:(SuccessCallback)success failure:(FailureCallback)failure;

- (instancetype)init:(NSString*)url name:(NSString*)name password:(NSString*)password success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

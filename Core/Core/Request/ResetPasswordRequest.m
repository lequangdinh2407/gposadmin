//
//  ResetPasswordRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/23/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "ResetPasswordRequest.h"

@implementation ResetPasswordRequest

- (instancetype)init:(NSString*)url userName:(NSString*)userName imei:(NSString*)imei success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:userName forKey:REQUEST_USERNAME];
    [params setValue:imei forKey:REQUEST_IMEI];

    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    return dataResult;
}

@end

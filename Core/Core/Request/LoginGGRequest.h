//
//  LoginGGRequest.h
//  Core
//
//  Created by ToanPham on 6/9/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface LoginGGRequest : BaseRequest

- (instancetype)init:(NSString*)url token:(NSString*)token success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

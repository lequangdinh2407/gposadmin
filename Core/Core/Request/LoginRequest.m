//
//  LoginRequest.m
//  spabees
//
//  Created by Toan Pham Thanh on 12/21/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import "LoginRequest.h"
#import "UserEntity.h"
#import "SessionManager.h"
#import "EntityManager.h"
#import "SocialProfileEntity.h"

@implementation LoginRequest

/*
- (instancetype)init:(NSString*)url token:(NSString*)token type:(int)type lon:(float)lon lat:(float)lat imei:(NSString*)imei success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:token forKey:REQUEST_TOKEN];
    [params setValue:imei forKey:REQUEST_IMEI];
    [params setValue:@(type) forKey:REQUEST_TYPE];
    [params setValue:@(lon) forKey:REQUEST_LON];
    [params setValue:@(lat) forKey:REQUEST_LAT];

    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}
*/

- (instancetype)init:(NSString*)url name:(NSString*)name password:(NSString*)password success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:name forKey:REQUEST_NAME];
    [params setValue:password forKey:REQUEST_PASSWORD];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary* data = (NSDictionary*)responseObject;
        
        UserEntity *userEntity = (UserEntity*) [[EntityManager sharedClient] createItem:[data objectForKey:RESPONSE_PROFILE] type:USER_ENTITY];
        [SessionManager sharedClient].userEntity = userEntity;
        
        SocialProfileEntity* socialProfileEntity = (SocialProfileEntity*) [[EntityManager sharedClient] createItem:[data objectForKey:RESPONSE_SOCIAL_PROFILE] type:SOCIAL_PROFILE_ENTITY];
        [SessionManager sharedClient].socialProfile = socialProfileEntity;
        
        if (![CoreStringUtils isEmpty:userEntity.userId]) {
            [SessionManager sharedClient].socialProfile.userId = userEntity.userId;
        }
        
        NSString* sessionKey = [CoreStringUtils getStringDiffNull:[data objectForKey:RESPONSE_SESSIONKEY]];
        [SessionManager sharedClient].sessionKey = sessionKey;

        NSString* socket = [CoreStringUtils getStringDiffNull:[data objectForKey:RESPONSE_API_SOCKET]];
        [SessionManager sharedClient].socket = socket;
        
        NSString* refreshToken = [CoreStringUtils getStringDiffNull:[data objectForKey:RESPONSE_REFRESH_TOKEN]];
        [SessionManager sharedClient].refreshToken = refreshToken;

        [SharePrefUtils writeIntPreference:PREFKEY_LOGIN_TYPE prefValue:LOGIN_PHONE];
        
        NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
        return dataResult;
    }
    return nil;
}

@end

//
//  CheckValidCodeRequest.h
//  spabees
//
//  Created by Toan Pham Thanh on 12/22/14.
//  Copyright (c) 2014 Toan Pham Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface CheckValidCodeRequest : BaseRequest

- (instancetype)init:(NSString*)url sendInfo:(NSString*)sendInfo activeCode:(NSString*)activeCode imei:(NSString*)imei countryCode:(NSString*)countryCode success:(SuccessCallback)success failure:(FailureCallback)failure ;

@end

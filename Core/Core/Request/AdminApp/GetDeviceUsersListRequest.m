//
//  GetDeviceUsersListRequest.m
//  Core
//
//  Created by Inglab on 21/09/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetDeviceUsersListRequest.h"
#import "EntityManager.h"

@implementation GetDeviceUsersListRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSArray* array = (NSArray*)responseObject;
    NSMutableArray* list = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < array.count; i++) {
        DeviceUserEntity* entity = (DeviceUserEntity*)[[EntityManager sharedClient] createItem:array[i] type:DEVICE_USER_ENTITY];
        
        [list addObject:entity];
    }
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:list forKey:RESPONSE_DATA];
    
    return result;
}

@end

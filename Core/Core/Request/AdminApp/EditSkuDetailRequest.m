//
//  EditSkuDetailRequest.m
//  Core
//
//  Created by Inglab on 19/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "EditSkuDetailRequest.h"

@implementation EditSkuDetailRequest

- (instancetype)init:(NSString*)url skuId:(int)skuId listProductId:(int)listProductId firstCategoryId:(int)firstCategoryId Name:(NSString*)Name DislayOrder:(int)DislayOrder Price:(double)Price SupplyCost:(double)SupplyCost Duration:(NSString*)Duration MinQuantity:(int)MinQuantity MaxQuantity:(int)MaxQuantity SaleTaxRate:(double)SaleTaxRate image:(NSString*)image success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:@(skuId) forKey:ENTITY_Id];
    [params setObject:@(listProductId) forKey:ENTITY_LIST_PRODUCT_ID];
    [params setObject:@(firstCategoryId) forKey:ENTITY_FIRST_CATEGORY_ID];
    [params setObject:Name forKey:ENTITY_NAME];
    [params setObject:@(Price) forKey:ENTITY_PRICE];
    [params setObject:@(DislayOrder) forKey:ENTITY_DISPLAY_ORDER];
    [params setObject:@(SupplyCost) forKey:ENTITY_STANDARD_COST];
    [params setObject:Duration forKey:ENTITY_DURATION];
    [params setObject:@(MinQuantity) forKey:ENTITY_MIN_QUANTITY];
    [params setObject:@(MaxQuantity) forKey:ENTITY_MAX_QUANTITY];
    [params setObject:@(true) forKey:ENTITY_ACTIVE];
    //[params setObject:@(true) forKey:ENTITY_NON_INVENTORY];
    [params setObject:@(true) forKey:ENTITY_SHOW_ON_APP];
    //[params setObject:@(12.0) forKey:ENTITY_QOH];
    [params setObject:image forKey:ENTITY_IMAGE_URI];
    //[params setObject:@(14) forKey:ENTITY_SALE_TAX_GROUP_ID];
    //[params setObject:@(15.0) forKey:ENTITY_SALE_TAX_RATE];
    //[params setObject:@"abc" forKey:ENTITY_PRINTER_NAME];
    //[params setObject:@(true) forKey:ENTITY_IS_GIFT_CARD];
    
    self.jsonData = [JSONUtils convertToJson:params];
    //[self.jsonData stringByReplacingOccurrencesOfString:@"\" withString:@""];
    
    self.method = PUT;
    return self;
    
}


@end

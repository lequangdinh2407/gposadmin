//
//  AddNewProductRequest.m
//  Core
//
//  Created by Inglab on 14/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "AddNewProductRequest.h"
#import "CoreStringUtils.h"
@implementation AddNewProductRequest

- (instancetype)init:(NSString*)url Name:(NSString*)Name isActive:(BOOL)isActive image:(NSString*)image success:(SuccessCallback)success  failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:Name forKey:ENTITY_NAME];
    
    [params setObject:@(NO) forKey:ENTITY_ACTIVE];
    if (isActive) {
        [params setObject:@(isActive) forKey:ENTITY_ACTIVE];
    }
    
    /*
    if (![CoreStringUtils isEmpty:image]) {
        [params setObject:image forKey:ENTITY_IMAGE_URI];
    } else {
        [params setObject:@"" forKey:ENTITY_IMAGE_URI];
    }
    */
    
    self.jsonData = [JSONUtils convertToJson:params];
    //self.method = POST;
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSString* listProductId = (NSString*)responseObject;
   
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:listProductId forKey:RESPONSE_DATA];
    return result;
}

@end

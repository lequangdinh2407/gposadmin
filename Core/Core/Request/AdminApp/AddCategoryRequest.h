//
//  AddCategoryRequest.h
//  Core
//
//  Created by Inglab on 14/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddCategoryRequest : BaseRequest

- (instancetype)init:(NSString*)url Name:(NSString*)Name  success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

NS_ASSUME_NONNULL_END

//
//  GetPosCategoriesRequest.m
//  Core
//
//  Created by Inglab on 20/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetPosCategoriesRequest.h"
#import "EntityManager.h"

@implementation GetPosCategoriesRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSArray* array = (NSArray*)responseObject;
    NSMutableArray* list = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < array.count; i++) {
        PosCategoryEntity* entity = (PosCategoryEntity*)[[EntityManager sharedClient] createItem:array[i] type:CATEGORY_ENTITY];
        
        [list addObject:entity];
    }
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:list forKey:RESPONSE_DATA];
    
    return result;
}


@end

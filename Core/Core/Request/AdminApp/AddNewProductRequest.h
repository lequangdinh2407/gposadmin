//
//  AddNewProductRequest.h
//  Core
//
//  Created by Inglab on 14/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddNewProductRequest : BaseRequest

- (instancetype)init:(NSString*)url Name:(NSString*)Name isActive:(BOOL)isActive image:(NSString*)image success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

NS_ASSUME_NONNULL_END

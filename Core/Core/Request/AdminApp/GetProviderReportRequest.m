//
//  GetProviderReportRequest.m
//  Core
//
//  Created by Inglab on 08/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetProviderReportRequest.h"
#import "EntityManager.h"

@implementation GetProviderReportRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    NSArray *providerReports = [data objectForKey:ENTITY_ProviderReports];
    
    NSMutableArray* list = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < providerReports.count; i++) {
        ProviderReportEntity *providerReport = (ProviderReportEntity*)[[EntityManager sharedClient] createItem:providerReports[i] type:PROVIDER_REPORT_ENTITY];
        
        [list addObject:providerReport];
    }
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:list forKey:RESPONSE_DATA];
    
    return dataResult;
}


@end

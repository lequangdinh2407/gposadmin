//
//  GetSaleReportsRequest.m
//  Core
//
//  Created by Inglab on 14/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetSaleReportsRequest.h"
#import "EntityManager.h"

@implementation GetSaleReportsRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    
    SaleReportsEntity *shop = (SaleReportsEntity*)[[EntityManager sharedClient] createItem:data type:SALE_REPORTS_ENTITY];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:shop forKey:RESPONSE_DATA];
    
    return dataResult;
}

@end

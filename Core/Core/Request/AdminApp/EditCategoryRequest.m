//
//  EditCategoryRequest.m
//  Core
//
//  Created by Inglab on 27/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "EditCategoryRequest.h"

@implementation EditCategoryRequest

- (instancetype)init:(NSString*)url categoryId:(int)categoryId Name:(NSString*)Name  Modifiers:(NSArray*)Modifiers success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:@(categoryId) forKey:ENTITY_Id];
    [params setObject:Name forKey:ENTITY_NAME];
    [params setObject:@(true) forKey:ENTITY_ACTIVE];
    
    if (Modifiers != nil) {
      [params setObject:Modifiers forKey:ENTITY_MODIFIERS];
    }
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    self.method = PUT;
    return self;
    
}

@end

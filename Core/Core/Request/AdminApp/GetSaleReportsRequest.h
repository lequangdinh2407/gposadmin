//
//  GetSaleReportsRequest.h
//  Core
//
//  Created by Inglab on 14/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetSaleReportsRequest : BaseRequest

@end

NS_ASSUME_NONNULL_END

//
//  GetInMonthSaleRequest.m
//  Core
//
//  Created by Inglab on 13/02/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "GetInMonthSaleRequest.h"
#import "EntityManager.h"

@implementation GetInMonthSaleRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSArray* array = (NSArray*)responseObject;
    NSMutableArray* list = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < array.count; i++) {
        SaleEntity* entity = (SaleEntity*)[[EntityManager sharedClient] createItem:array[i] type:SALE_ENTITY];
        
        [list addObject:entity];
    }
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:list forKey:RESPONSE_DATA];
    
    return result;
}

@end

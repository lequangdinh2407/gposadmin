//
//  GetRecentSaleRequest.m
//  Core
//
//  Created by Inglab on 13/02/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "GetRecentSaleRequest.h"
#import "EntityManager.h"

@implementation GetRecentSaleRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    
    NSArray* array = [data objectForKey:RESPONSE_SALES];
    NSMutableArray* list = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < array.count; i++) {
        SaleEntity* entity = (SaleEntity*)[[EntityManager sharedClient] createItem:array[i] type:SALE_ENTITY];
        
        [list addObject:entity];
    }
    
    int totalOrder = [Helpers getUIntDiffNull:[data objectForKey:RESPONSE_TOTAL_ORDER]];
    int totalSale = [Helpers getDoubleDiffNull:[data objectForKey:RESPONSE_TOTAL_SALE]];
    
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:list forKey:RESPONSE_SALES];
    [result setObject:@(totalOrder) forKey:RESPONSE_TOTAL_ORDER];
    [result setObject:@(totalSale) forKey:RESPONSE_TOTAL_SALE];
    
    return result;
}

@end

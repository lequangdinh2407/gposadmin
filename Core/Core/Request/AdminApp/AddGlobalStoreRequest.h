//
//  AddGlobalStoreRequest.h
//  Core
//
//  Created by Inglab on 04/01/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

@interface AddGlobalStoreRequest : BaseRequest

- (instancetype)init:(NSString*)url name:(NSString*)name shopCode:(NSString*)shopCode storeId:(NSString*)storeId password:(NSString*)password active:(BOOL)active device:(NSString*)device phone:(NSString*)phone coordinateLon:(double)coordinateLon coordinateLat:(double)coordinateLat success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

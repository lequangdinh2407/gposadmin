//
//  EditStoreOwnerRequest.m
//  Core
//
//  Created by Inglab on 12/01/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "EditStoreOwnerRequest.h"

@implementation EditStoreOwnerRequest

- (instancetype)init:(NSString*)url Id:(int)Id firstName:(NSString*)firstName lastName:(NSString*)lastName email:(NSString*)email phone:(NSString*)phone password:(NSString*)password active:(BOOL)active storeIds:(NSArray*)storeIds  success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:@(Id) forKey:@"Id"];
    [params setObject:firstName forKey:@"firstName"];
    [params setObject:lastName forKey:@"lastName"];
    [params setObject:email forKey:@"email"];
    [params setObject:phone forKey:@"phone"];
    
//    if (password != nil) {
//        [params setObject:password forKey:@"password"];
//    }
    
    if (storeIds != nil) {
        [params setObject:storeIds forKey:@"storeIds"];
    }
    
    [params setObject:@(active) forKey:@"active"];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    self.method = PUT;
    return self;
    
}

@end

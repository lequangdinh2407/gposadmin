//
//  GetSaleOrderDetailRequest.h
//  Core
//
//  Created by Inglab on 22/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetSaleOrderDetailRequest : BaseRequest

@end

NS_ASSUME_NONNULL_END

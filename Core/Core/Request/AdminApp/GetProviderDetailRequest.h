//
//  GetProviderDetailRequest.h
//  Core
//
//  Created by Inglab on 09/12/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetProviderDetailRequest : BaseRequest

@end

NS_ASSUME_NONNULL_END

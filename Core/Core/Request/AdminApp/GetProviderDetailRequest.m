//
//  GetProviderDetailRequest.m
//  Core
//
//  Created by Inglab on 09/12/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetProviderDetailRequest.h"
#import "EntityManager.h"

@implementation GetProviderDetailRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    
    ProviderEntity *entity = (ProviderEntity*)[[EntityManager sharedClient] createItem:data type:PROVIDER_ENTITY];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:entity forKey:RESPONSE_DATA];
    
    return dataResult;
}

@end

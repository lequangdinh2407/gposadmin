//
//  SaveSettingsRequest.h
//  Core
//
//  Created by Inglab on 05/08/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"
#import "SettingEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface SaveSettingsRequest : BaseRequest

- (instancetype)init:(NSString*)url list:(NSMutableArray*)list success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

NS_ASSUME_NONNULL_END

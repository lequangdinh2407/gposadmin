//
//  PostDeviceUserRequest.m
//  Core
//
//  Created by Inglab on 11/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "PostDeviceUserRequest.h"

@implementation PostDeviceUserRequest

- (instancetype)init:(NSString*)url UserName:(NSString*)UserName Email:(NSString*)Email  PassCode:(NSString*)PassCode IsCashier:(BOOL)IsCasiher IsSupervisor:(BOOL)IsSupervisor IsController:(BOOL)IsController IsManager:(BOOL)IsManager success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:UserName forKey:ENTITY_USER_NAME];
    [params setObject:Email forKey:ENTITY_Email];
    [params setObject:PassCode forKey:ENTITY_PASSCODE];
    
    NSMutableArray* roleList = [[NSMutableArray alloc] init];
    if (IsCasiher) {
        [roleList addObject:@"Cashier"];
    }
    
    if (IsSupervisor) {
         [roleList addObject:@"Supervisor"];
    }
    
    if (IsController) {
        [roleList addObject:@"Controller"];
    }
    
    if (IsManager) {
        [roleList addObject:@"Manager"];
    }
   
    [params setObject:roleList forKey:ENTITY_ROLES];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    self.method = POST;
    return self;
    
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSString* Id = (NSString*)responseObject;
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:Id forKey:RESPONSE_DATA];
    
    return result;
}
@end

//
//  EditStoreOwnerRequest.h
//  Core
//
//  Created by Inglab on 12/01/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

@interface EditStoreOwnerRequest : BaseRequest

- (instancetype)init:(NSString*)url Id:(int)Id firstName:(NSString*)firstName lastName:(NSString*)lastName email:(NSString*)email phone:(NSString*)phone password:(NSString*)password active:(BOOL)active storeIds:(NSArray*)storeIds  success:(SuccessCallback)success failure:(FailureCallback)failure;

@end



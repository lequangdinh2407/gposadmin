//
//  GetGlobalStoreListRequest.m
//  Core
//
//  Created by Inglab on 27/12/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetGlobalStoreListRequest.h"
#import "EntityManager.h"

@implementation GetGlobalStoreListRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSArray* array = (NSArray*)responseObject;
    
    NSMutableArray* list = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < array.count; i++) {
        GlobalStoreEntity* entity = (GlobalStoreEntity*)[[EntityManager sharedClient] createItem:array[i] type:GLOBAL_STORE_ENTITY];
        
        if (![CoreStringUtils isEmpty:entity.name]) {
            [list addObject:entity];
        }
    }
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:list forKey:RESPONSE_DATA];
    
    return result;
}

@end

//
//  PostDeviceUserRequest.h
//  Core
//
//  Created by Inglab on 11/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface PostDeviceUserRequest : BaseRequest

- (instancetype)init:(NSString*)url UserName:(NSString*)UserName Email:(NSString*)Email PassCode:(NSString*)PassCode IsCashier:(BOOL)IsCasiher IsSupervisor:(BOOL)IsSupervisor IsController:(BOOL)IsController IsManager:(BOOL)IsManager success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

NS_ASSUME_NONNULL_END

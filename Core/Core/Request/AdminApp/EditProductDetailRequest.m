//
//  EditProductDetailRequest.m
//  Core
//
//  Created by Inglab on 26/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "EditProductDetailRequest.h"

@implementation EditProductDetailRequest

- (instancetype)init:(NSString*)url productId:(int)productId Name:(NSString*)Name DislayOrder:(int)DislayOrder ImageUri:(NSString*)ImageUri NonInventory:(BOOL)NonInventory Customizable:(BOOL)Customizable IsGiftCard:(BOOL)IsGiftCard PrinterName:(NSString*)PrinterName Products:(NSArray*)Products Categories:(NSArray*)Categories success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:@(productId) forKey:ENTITY_Id];
    [params setObject:Name forKey:ENTITY_NAME];
    [params setObject:@(DislayOrder) forKey:ENTITY_DISPLAY_ORDER];
    [params setObject:@(NonInventory) forKey:ENTITY_NON_INVENTORY];
    [params setObject:@(Customizable) forKey:ENTITY_CUSTOMIZEABLE];
    
    if (![CoreStringUtils isEmpty:ImageUri]) {
        [params setObject:ImageUri forKey:ENTITY_IMAGE_URI];
    }else {
        [params setObject:@"" forKey:ENTITY_IMAGE_URI];
    }
    
    [params setObject:PrinterName forKey:ENTITY_PRINTER_NAME];
    [params setObject:@(IsGiftCard) forKey:ENTITY_IS_GIFT_CARD];
    [params setObject:@(true) forKey:ENTITY_ACTIVE];
    
    //if (Products != nil) {
      //  [params setObject:Products forKey:ENTITY_PRODUCTS];
    //}
    
    [params setObject:Categories forKey:ENTITY_CATEGORIES];
    self.jsonData = [JSONUtils convertToJson:params];
    
    self.method = PUT;
    return self;
    
}

@end

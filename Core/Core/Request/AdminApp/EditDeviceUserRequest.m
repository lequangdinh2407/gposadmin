//
//  EditDeviceUserRequest.m
//  Core
//
//  Created by Inglab on 15/12/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "EditDeviceUserRequest.h"

@implementation EditDeviceUserRequest

- (instancetype)init:(NSString*)url deviceUserId:(int)deviceUserId UserName:(NSString*)UserName Email:(NSString*)Email PassCode:(NSString*)PassCode IsCashier:(BOOL)IsCasiher IsSupervisor:(BOOL)IsSupervisor IsController:(BOOL)IsController IsManager:(BOOL)IsManager success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:@(deviceUserId) forKey:ENTITY_Id];
    [params setObject:UserName forKey:ENTITY_USER_NAME];
    [params setObject:Email forKey:ENTITY_Email];
    [params setObject:PassCode forKey:ENTITY_PASSCODE];
    
    NSMutableArray* roleList = [[NSMutableArray alloc] init];
    if (IsCasiher) {
        [roleList addObject:@"Cashier"];
    }
    
    if (IsSupervisor) {
        [roleList addObject:@"Supervisor"];
    }
    
    if (IsController) {
        [roleList addObject:@"Controller"];
    }
    
    if (IsManager) {
        [roleList addObject:@"Manager"];
    }
    
    [params setObject:roleList forKey:ENTITY_ROLES];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    self.method = POST;
    return self;
}

@end

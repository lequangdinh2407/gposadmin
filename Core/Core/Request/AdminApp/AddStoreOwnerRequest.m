//
//  AddStoreOwnerRequest.m
//  Core
//
//  Created by Inglab on 12/01/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "AddStoreOwnerRequest.h"

@implementation AddStoreOwnerRequest

- (instancetype)init:(NSString*)url firstName:(NSString*)firstName lastName:(NSString*)lastName email:(NSString*)email phone:(NSString*)phone  active:(BOOL)active storeIds:(NSArray*)storeIds success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:firstName forKey:@"firstName"];
    [params setObject:lastName forKey:@"lastName"];
    [params setObject:email forKey:@"email"];
    [params setObject:phone forKey:@"phone"];
    [params setObject:storeIds forKey:@"storeIds"];
    [params setObject:@(active) forKey:@"active"];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    self.method = POST;
    return self;
    
}

@end

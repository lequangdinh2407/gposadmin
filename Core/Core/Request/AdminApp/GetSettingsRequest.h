//
//  GetSettingsRequest.h
//  Core
//
//  Created by Inglab on 05/08/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetSettingsRequest : BaseRequest

@end

NS_ASSUME_NONNULL_END

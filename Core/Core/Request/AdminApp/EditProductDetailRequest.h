//
//  EditProductDetailRequest.h
//  Core
//
//  Created by Inglab on 26/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface EditProductDetailRequest : BaseRequest

- (instancetype)init:(NSString*)url productId:(int)productId Name:(NSString*)Name DislayOrder:(int)DislayOrder ImageUri:(NSString*)ImageUri NonInventory:(BOOL)NonInventory Customizable:(BOOL)Customizable IsGiftCard:(BOOL)IsGiftCard PrinterName:(NSString*)PrinterName Products:(NSArray*)Products Categories:(NSArray*)Categories success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

NS_ASSUME_NONNULL_END

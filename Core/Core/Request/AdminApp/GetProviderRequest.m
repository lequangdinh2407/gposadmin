//
//  GetProviderRequest.m
//  Core
//
//  Created by Inglab on 09/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetProviderRequest.h"
#import "EntityManager.h"

@implementation GetProviderRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSArray* array = (NSArray*)responseObject;
    NSMutableArray* list = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < array.count; i++) {
        ProviderEntity* entity = (ProviderEntity*)[[EntityManager sharedClient] createItem:array[i] type:PROVIDER_ENTITY];
        
        [list addObject:entity];
    }
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:list forKey:RESPONSE_DATA];
    
    return result;
}

@end

//
//  GetSaleTaxGroupRequest.m
//  Core
//
//  Created by Inglab on 25/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetSaleTaxGroupRequest.h"
#import "EntityManager.h"
#import "CoreStringUtils.h"

@implementation GetSaleTaxGroupRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSArray* array = (NSArray*)responseObject;
    NSMutableArray* list = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < array.count; i++) {
        SaleTaxGroupEntity *entity = (SaleTaxGroupEntity*)[[EntityManager sharedClient] createItem:array[i] type:SALE_TAX_GROUP_ENTITY];
        if (![CoreStringUtils isEmpty:entity.GroupName]) {
            [list addObject:entity];
        }
        
    }
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:list forKey:RESPONSE_DATA];
    
    return result;
}

@end

//
//  GetPosCategoriesRequest.h
//  Core
//
//  Created by Inglab on 20/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetPosCategoriesRequest :BaseRequest

@end

NS_ASSUME_NONNULL_END

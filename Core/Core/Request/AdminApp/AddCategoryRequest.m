//
//  AddCategoryRequest.m
//  Core
//
//  Created by Inglab on 14/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "AddCategoryRequest.h"

@implementation AddCategoryRequest

- (instancetype)init:(NSString*)url Name:(NSString*)Name  success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:Name forKey:ENTITY_NAME];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    //self.method = POST;
    return self;
    
}
@end

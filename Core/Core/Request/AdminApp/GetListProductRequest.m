//
//  GetListProductRequest.m
//  Core
//
//  Created by Inglab on 19/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetListProductRequest.h"
#import "EntityManager.h"

@implementation GetListProductRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    
    ProductEntity *entity = (ProductEntity*)[[EntityManager sharedClient] createItem:data type:PRODUCT_ENTITY];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:entity forKey:RESPONSE_DATA];
    
    return dataResult;
}

@end

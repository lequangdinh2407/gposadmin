//
//  AddNewSKURequest.m
//  Core
//
//  Created by Inglab on 14/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "AddNewSKURequest.h"
#import "CoreStringUtils.h"

@implementation AddNewSKURequest

- (instancetype)init:(NSString*)url Name:(NSString*)Name DislayOrder:(int)DislayOrder Price:(double)Price SupplyCost:(double)SupplyCost Duration:(NSString*)Duration MinQuantity:(int)MinQuantity MaxQuantity:(int)MaxQuantity SaleTaxRate:(double)SaleTaxRate image:(NSString*)image success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:Name forKey:ENTITY_NAME];
    [params setObject:@(Price) forKey:ENTITY_PRICE];
    [params setObject:@(DislayOrder) forKey:ENTITY_DISPLAY_ORDER];
    [params setObject:@(SupplyCost) forKey:ENTITY_STANDARD_COST];
    [params setObject:Duration forKey:ENTITY_DURATION];
    [params setObject:@(MinQuantity) forKey:ENTITY_MIN_QUANTITY];
    [params setObject:@(MaxQuantity) forKey:ENTITY_MAX_QUANTITY];
    [params setObject:@(true) forKey:ENTITY_ACTIVE];
    [params setObject:@(true) forKey:ENTITY_SHOW_ON_APP];
    
    if (![CoreStringUtils isEmpty:image]) {
        [params setObject:image forKey:ENTITY_IMAGE_URI];
    } else {
        [params setObject:@"" forKey:ENTITY_IMAGE_URI];
    }
    self.jsonData = [JSONUtils convertToJson:params];
    
    //self.method = POST;
    return self;
    
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSString* listProductId = (NSString*)responseObject;
    
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:listProductId forKey:RESPONSE_DATA];
    return result;
}

@end

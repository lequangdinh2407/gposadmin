//
//  GetPaymentReportsRequest.m
//  Core
//
//  Created by Inglab on 04/08/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetPaymentReportsRequest.h"
#import "EntityManager.h"

@implementation GetPaymentReportsRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    
    GeneralPaymentReportEntity *entity = (GeneralPaymentReportEntity*)[[EntityManager sharedClient] createItem:data type:GENERAL_PAYMENT_REPORT_ENTITY];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:entity forKey:RESPONSE_DATA];
    
    return dataResult;
}

@end

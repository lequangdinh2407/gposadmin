//
//  GetStoreOwnerListRequest.m
//  Core
//
//  Created by Inglab on 06/01/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "GetStoreOwnerListRequest.h"
#import "EntityManager.h"

@implementation GetStoreOwnerListRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSArray* array = (NSArray*)responseObject;
    NSMutableArray* list = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < array.count; i++) {
        StoreOwnerEntity* entity = (StoreOwnerEntity*)[[EntityManager sharedClient] createItem:array[i] type:STORE_OWNER_ENTITY];
        
        [list addObject:entity];
    }
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:list forKey:RESPONSE_DATA];
    
    return result;
}

@end

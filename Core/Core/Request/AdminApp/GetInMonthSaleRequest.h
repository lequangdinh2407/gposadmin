//
//  GetInMonthSaleRequest.h
//  Core
//
//  Created by Inglab on 13/02/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetInMonthSaleRequest : BaseRequest

@end

NS_ASSUME_NONNULL_END

//
//  EditSkuDetailRequest.h
//  Core
//
//  Created by Inglab on 19/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface EditSkuDetailRequest : BaseRequest

- (instancetype)init:(NSString*)url skuId:(int)skuId listProductId:(int)listProductId firstCategoryId:(int)firstCategoryId Name:(NSString*)Name DislayOrder:(int)DislayOrder Price:(double)Price SupplyCost:(double)SupplyCost Duration:(NSString*)Duration MinQuantity:(int)MinQuantity MaxQuantity:(int)MaxQuantity SaleTaxRate:(double)SaleTaxRate image:(NSString*)image success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

NS_ASSUME_NONNULL_END

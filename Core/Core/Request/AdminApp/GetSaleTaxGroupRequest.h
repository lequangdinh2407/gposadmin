//
//  GetSaleTaxGroupRequest.h
//  Core
//
//  Created by Inglab on 25/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetSaleTaxGroupRequest : BaseRequest

@end

NS_ASSUME_NONNULL_END

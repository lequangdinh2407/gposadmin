//
//  GetModifiderRequest.m
//  Core
//
//  Created by Inglab on 27/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetModifiderRequest.h"
#import "EntityManager.h"

@implementation GetModifiderRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSArray* array = (NSArray*)responseObject;
    NSMutableArray* list = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < array.count; i++) {
        ModifierEntity* entity = (ModifierEntity*)[[EntityManager sharedClient] createItem:array[i] type:MODIFIER_ENTITY];
        
        [list addObject:entity];
    }
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:list forKey:RESPONSE_DATA];
    
    return result;
}

@end

//
//  GetGlobalStoreListRequest.h
//  Core
//
//  Created by Inglab on 27/12/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetGlobalStoreListRequest : BaseRequest

@end

NS_ASSUME_NONNULL_END

//
//  GetCustomerListRequest.m
//  Core
//
//  Created by Inglab on 21/09/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetCustomerListRequest.h"
#import "EntityManager.h"

@implementation GetCustomerListRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSArray* array = (NSArray*)responseObject;
    NSMutableArray* list = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < array.count; i++) {
        CustomerEntity* entity = (CustomerEntity*)[[EntityManager sharedClient] createItem:array[i] type:CUSTOMER_ENTITY];
        
        [list addObject:entity];
    }
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:list forKey:RESPONSE_DATA];
    
    return result;
}

@end

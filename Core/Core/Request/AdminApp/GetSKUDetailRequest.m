//
//  GetSKUDetailRequest.m
//  Core
//
//  Created by Inglab on 19/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetSKUDetailRequest.h"
#import "EntityManager.h"

@implementation GetSKUDetailRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    
    ListProductEntity *entity = (ListProductEntity*)[[EntityManager sharedClient] createItem:data type:LIST_PRODUCT_ENTITY];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:entity forKey:RESPONSE_DATA];
    
    return dataResult;
}

@end

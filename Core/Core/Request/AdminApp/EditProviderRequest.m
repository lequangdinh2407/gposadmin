//
//  EditProviderRequest.m
//  Core
//
//  Created by Inglab on 19/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "EditProviderRequest.h"
#import "EntityManager.h"

@implementation EditProviderRequest

- (instancetype)init:(NSString*)url providerId:(int)providerId  Name:(NSString*)Name Phone:(NSString*)Phone Active:(BOOL)Active SignedIn:(BOOL)SignedIn LastSignedInTime:(NSString*)LastSignedInTime JobCount:(int)JobCount SaleAmount:(double)SaleAmount CommissionRate:(double)CommissionRate PassCode:(NSString*)PassCode NotificationKey:(NSString*)NotificationKey WorkHours:(double)WorkHours IsTardy:(BOOL)IsTardy WorkingDays:WorkingDays success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:@(providerId) forKey:ENTITY_Id];
    [params setObject:Name forKey:ENTITY_NAME];
    [params setObject:Phone forKey:ENTITY_PHONE];
    [params setObject:@(NO) forKey:ENTITY_ACTIVE];
    if (Active) {
        [params setObject:@(Active) forKey:ENTITY_ACTIVE];
    }
    
    //[params setObject:LastSignedInTime forKey:ENTITY_ACTIVE];
    [params setObject:@(SignedIn) forKey:ENTITY_SIGNED_IN];
    [params setObject:LastSignedInTime forKey:ENTITY_LAST_SIGNEDIN_TIME];
    [params setObject:Phone forKey:ENTITY_PHONE];
    [params setObject:@(JobCount) forKey:ENTITY_JOB_COUNT];
    //[params setObject:PassCode forKey:ENTITY_JOB_COUNT];
    
    [params setObject:@(SaleAmount) forKey:ENTITY_SALE_AMOUNT];
    [params setObject:@(CommissionRate) forKey:ENTITY_COMMISSION_RATE];
    [params setObject:PassCode forKey:ENTITY_PASSCODE];
    [params setObject:NotificationKey forKey:ENTITY_NOTIFICATION_KEY];
    [params setObject:@(WorkHours) forKey:ENTITY_WORK_HOURS];
    [params setObject:@(IsTardy) forKey:ENTITY_IS_TARDY];
    
    if (WorkingDays) {
        [params setObject:WorkingDays forKey:ENTITY_WorkingDays];
    }
    self.jsonData = [JSONUtils convertToJson:params];
    
    self.method = PUT;
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    
    return responseObject;
    
    /*
    NSDictionary *data = (NSDictionary*)responseObject;
    
    ProviderEntity *entity = (ProviderEntity*)[[EntityManager sharedClient] createItem:data type:PROVIDER_ENTITY];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:entity forKey:RESPONSE_DATA];
    
    return dataResult;
    */
    
}

@end


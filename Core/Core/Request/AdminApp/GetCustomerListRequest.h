//
//  GetCustomerListRequest.h
//  Core
//
//  Created by Inglab on 21/09/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetCustomerListRequest : BaseRequest

@end

NS_ASSUME_NONNULL_END

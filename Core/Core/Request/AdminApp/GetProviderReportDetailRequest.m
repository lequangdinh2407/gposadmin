//
//  GetProviderReportDetailRequest.m
//  Core
//
//  Created by Inglab on 16/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetProviderReportDetailRequest.h"
#import "EntityManager.h"

@implementation GetProviderReportDetailRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    
    ProviderReportEntity *entity = (ProviderReportEntity*)[[EntityManager sharedClient] createItem:data type:PROVIDER_REPORT_ENTITY];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:entity forKey:RESPONSE_DATA];
    
    return dataResult;
}


@end

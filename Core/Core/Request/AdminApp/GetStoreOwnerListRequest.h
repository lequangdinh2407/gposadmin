//
//  GetStoreOwnerListRequest.h
//  Core
//
//  Created by Inglab on 06/01/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetStoreOwnerListRequest : BaseRequest

@end

NS_ASSUME_NONNULL_END

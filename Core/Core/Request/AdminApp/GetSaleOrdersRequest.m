//
//  GetSaleOrdersRequest.m
//  Core
//
//  Created by Inglab on 22/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetSaleOrdersRequest.h"
#import "EntityManager.h"

@implementation GetSaleOrdersRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    
    SaleOrderEntity *entity = (SaleOrderEntity*)[[EntityManager sharedClient] createItem:data type:SALE_ORDER_ENTITY];
    
    NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
    [dataResult setValue:entity forKey:RESPONSE_DATA];
    
    return dataResult;
}

@end

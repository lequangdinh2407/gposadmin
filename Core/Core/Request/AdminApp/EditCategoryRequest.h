//
//  EditCategoryRequest.h
//  Core
//
//  Created by Inglab on 27/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface EditCategoryRequest : BaseRequest

- (instancetype)init:(NSString*)url categoryId:(int)categoryId Name:(NSString*)Name  Modifiers:(NSArray*)Modifiers success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

NS_ASSUME_NONNULL_END

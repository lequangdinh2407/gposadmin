//
//  GetProviderReportDetailRequest.h
//  Core
//
//  Created by Inglab on 16/11/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetProviderReportDetailRequest : BaseRequest

@end

NS_ASSUME_NONNULL_END

//
//  GetSKUDetailRequest.h
//  Core
//
//  Created by Inglab on 19/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface GetSKUDetailRequest : BaseRequest

@end

NS_ASSUME_NONNULL_END

//
//  GetCategoryDetailRequest.m
//  Core
//
//  Created by Inglab on 03/10/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "GetCategoryDetailRequest.h"
#import "EntityManager.h"

@implementation GetCategoryDetailRequest

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    NSDictionary *data = (NSDictionary*)responseObject;
    CategoryEntity* entity = (CategoryEntity*)[[EntityManager sharedClient] createItem:data type:CATEGORY_ENTITY];
  
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:entity forKey:RESPONSE_DATA];
    
    return result;
}

@end

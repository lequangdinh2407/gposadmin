//
//  SaveSettingsRequest.m
//  Core
//
//  Created by Inglab on 05/08/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "SaveSettingsRequest.h"
#import "SettingEntity.h"

@implementation SaveSettingsRequest

- (instancetype)init:(NSString*)url list:(NSMutableArray*)list success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    //NSMutableArray* settingsList = [[NSMutableArray alloc] init];
    
    /*
    for (int i = 0; i < list.count; i++) {
        SettingEntity* setting = [list objectAtIndex:i];
        
        NSMutableDictionary* _map = [[NSMutableDictionary alloc] init];
        //[_map setObject:@(setting.Id) forKey:ENTITY_Id];
        [_map setObject:setting.Name forKey:ENTITY_NAME];
        [_map setObject: @(200.0) forKey:ENTITY_VALUE];
        //[_map setObject: setting.Value forKey:ENTITY_VALUE];
        [settingsList addObject:_map];
    }
    */
    
    //NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    //[params setObject:@(Id) forKey:ENTITY_Id];

    //[
    
    self.jsonData = [JSONUtils convertToJson:list];
    
    self.method = PUT;
    
    return self;
    
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:@"" forKey:RESPONSE_DATA];
    
    return result;
}

@end

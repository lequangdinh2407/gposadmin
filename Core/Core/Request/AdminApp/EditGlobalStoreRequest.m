//
//  EditGlobalStoreRequest.m
//  Core
//
//  Created by Inglab on 04/01/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "EditGlobalStoreRequest.h"

@implementation EditGlobalStoreRequest

- (instancetype)init:(NSString*)url Id:(int)Id name:(NSString*)name shopCode:(NSString*)shopCode storeId:(NSString*)storeId password:(NSString*)password active:(BOOL)active device:(NSString*)device phone:(NSString*)phone coordinateLon:(double)coordinateLon coordinateLat:(double)coordinateLat success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:@(Id) forKey:@"id"];
    [params setObject:name forKey:@"name"];
    [params setObject:phone forKey:@"phone"];
    [params setObject:password forKey:@"password"];
    [params setObject:shopCode forKey:@"shopCode"];
    //[params setObject:storeId forKey:@"storeId"];
    [params setObject:device forKey:@"device"];
    
    NSMutableDictionary *coordinate = [[NSMutableDictionary alloc] init];
    [coordinate setObject:@(coordinateLat) forKey:@"lat"];
    [coordinate setObject:@(coordinateLon) forKey:@"lon"];
    [params setObject:coordinate forKey:@"coordinate"];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    self.method = PUT;
    return self;
    
}
@end

//
//  AddStoreOwnerRequest.h
//  Core
//
//  Created by Inglab on 12/01/2020.
//  Copyright © 2020 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

@interface AddStoreOwnerRequest : BaseRequest

- (instancetype)init:(NSString*)url firstName:(NSString*)firstName lastName:(NSString*)lastName email:(NSString*)email phone:(NSString*)phone  active:(BOOL)active storeIds:(NSArray*)storeIds  success:(SuccessCallback)success failure:(FailureCallback)failure;

@end



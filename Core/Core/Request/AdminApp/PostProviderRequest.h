//
//  PostProviderRequest.h
//  Core
//
//  Created by Inglab on 18/07/2019.
//  Copyright © 2019 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface PostProviderRequest : BaseRequest

- (instancetype)init:(NSString*)url Name:(NSString*)Name Phone:(NSString*)Phone Active:(BOOL)Active SignedIn:(BOOL)SignedIn LastSignedInTime:(NSString*)LastSignedInTime JobCount:(int)JobCount SaleAmount:(double)SaleAmount CommissionRate:(double)CommissionRate PassCode:(NSString*)PassCode NotificationKey:(NSString*)NotificationKey WorkHours:(double)WorkHours IsTardy:(BOOL)IsTardy  success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

NS_ASSUME_NONNULL_END

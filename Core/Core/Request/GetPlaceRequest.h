//
//  GetPlaceRequest.h
//  Core
//
//  Created by Dragon on 3/6/17.
//  Copyright © 2017 ToanPham. All rights reserved.
//

#import "BaseRequest.h"

@interface GetPlaceRequest : BaseRequest

- (instancetype)init:(NSString*)url success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

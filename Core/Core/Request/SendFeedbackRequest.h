//
//  SendFeedbackRequest.h
//  Core
//
//  Created by ToanPham on 6/12/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface SendFeedbackRequest : BaseRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey message:(NSString*)message success:(SuccessCallback)success failure:(FailureCallback)failure;

@end
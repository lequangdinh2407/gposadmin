//
//  WebSocketHasACKRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 3/30/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseWebSocketRequest.h"

@class SocketCommand;

@interface WebSocketHasACKRequest : BaseWebSocketRequest

- (void)receiveACK:(SocketCommand*)ackCommand;

@end
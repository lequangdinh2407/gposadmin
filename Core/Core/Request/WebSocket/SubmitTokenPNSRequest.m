//
//  SubmitTokenPNSRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 3/31/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "SubmitTokenPNSRequest.h"
#import "LuvKonectConstants.h"
#import "CommandBuilder.h"

@implementation SubmitTokenPNSRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey imei:(NSString*)imei token:(NSString*)token success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:sessionKey forKey:REQUEST_SESSION_KEY];
    [params setValue:imei forKey:REQUEST_IMEI];
    [params setValue:token forKey:REQUEST_TOKEN];
    [params setValue:REQUEST_DEVICE_ID_IOS forKey:REQUEST_DEVICE_ID];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

@end
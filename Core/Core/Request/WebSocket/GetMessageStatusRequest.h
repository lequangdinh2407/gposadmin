//
//  GetMessageStatusRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 5/13/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebSocketHasACKRequest.h"

@class SocketCommand;

@interface GetMessageStatusRequest : BaseWebSocketRequest

- (instancetype)init:(NSArray*)ids success:(SuccessCallback)success failure:(FailureCallback)failure;

@end
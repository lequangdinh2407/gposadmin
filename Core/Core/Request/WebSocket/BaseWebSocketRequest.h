//
//  BaseWebSocketRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 3/13/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"
#import "SocketCommand.h"
#import "WebSocketManager.h"

@interface BaseWebSocketRequest : BaseRequest

@property (nonatomic, strong) SocketCommand* command;
@property (nonatomic) uint64_t sendTime;
@property (nonatomic) uint8_t timeOutCount;

- (instancetype)init:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure;
- (SocketCommand*)getCommand;

@end
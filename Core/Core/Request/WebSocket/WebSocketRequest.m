//
//  WebSocketRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 3/6/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "WebSocketRequest.h"
#import "SessionManager.h"
#import "WebSocketManager.h"
#import "RequestManager.h"

@implementation WebSocketRequest

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey roomId:(NSString*)roomId success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:url success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:sessionKey forKey:REQUEST_SESSION_KEY];
    [params setValue:roomId forKey:REQUEST_ROOM_ID];
    
    self.jsonData = [JSONUtils convertToJson:params];
    
    return self;
}

- (NSString *)getURL {
    self.params = [self getParams];
    NSString *params = [self.params objectForKey:REQUEST_PARAMS];
    params = [params stringByReplacingOccurrencesOfString: @"/" withString: @"%2f"];
    params = [params stringByReplacingOccurrencesOfString: @"=" withString: @"%3d"];
    NSString *timestamp = [self.params objectForKey:REQUEST_TIMESTAMP];
    NSString* url = [NSString stringWithFormat:@"%@&%@=%@&%@=%@&%@=%@", self.url, REQUEST_APIKEY, API_KEY, REQUEST_TIMESTAMP, timestamp, REQUEST_PARAMS, params];
    url = [url stringByReplacingOccurrencesOfString: @"+" withString: @"%2b"];
    return url;
}

- (void)addToQueue {   
    [[WebSocketManager sharedClient] connect:self];    
}

@end
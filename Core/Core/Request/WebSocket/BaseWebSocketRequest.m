//
//  BaseWebSocketRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 3/13/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "BaseWebSocketRequest.h"
#import "RequestManager.h"
#import "CoreConstants.h"

#define TAG @"BaseWebSocketRequest"

@implementation BaseWebSocketRequest


- (instancetype)init:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure {
    self = [super init:nil success:success failure:failure];
    
    if (!self) {
        return nil;
    }

    self.command = nil;
    
    if (params != nil && [params isKindOfClass:[NSDictionary class]])
    {
        self.jsonData = [JSONUtils convertToJson:params];
        [Log d:0 tag:TAG message:[NSString stringWithFormat:@"content=%@", self.jsonData]];
    }
    
    return self;
}

- (SocketCommand*)getCommand {
    return self.command;
}

- (void)addToQueue {
    [[WebSocketManager sharedClient] addToQueue:self];    
}

@end

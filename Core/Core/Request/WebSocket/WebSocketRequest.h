//
//  WebSocketRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 3/6/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface WebSocketRequest : BaseRequest

@property (nonatomic) uint8_t timeOutCount;

- (instancetype)init:(NSString*)url sessionKey:(NSString*)sessionKey roomId:(NSString*)roomId success:(SuccessCallback)success failure:(FailureCallback)failure;
- (NSString *)getURL;

@end
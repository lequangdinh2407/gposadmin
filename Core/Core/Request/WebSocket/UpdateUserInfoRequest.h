//
//  UpdateUserInfoRequest.h
//  luvkonectcore
//
//  Created by ToanPham on 3/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

@interface UpdateUserInfoRequest : BaseRequest

- (instancetype)init:(NSString*)url params:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure;

@end

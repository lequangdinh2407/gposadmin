//
//  UpdateUserInfoRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 3/26/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "UpdateUserInfoRequest.h"
#import "SessionManager.h"
#import "EntityManager.h"

@implementation UpdateUserInfoRequest

- (instancetype)init:(NSString*)url params:(NSMutableDictionary*)params success:(SuccessCallback)success failure:(FailureCallback)failure {
    
    self = [super init:url success:success failure:failure];
    if (!self) {
        return nil;
    }
    
    self.jsonData = [JSONUtils convertToJson:params];
    self.method = PUT;
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary* data = (NSDictionary*)responseObject;
        
        UserEntity *userEntity = (UserEntity*) [[EntityManager sharedClient] createItem:data type:USER_ENTITY];
        
        NSMutableDictionary *dataResult = [[NSMutableDictionary alloc] init];
        if (!userEntity)
            [dataResult setValue:userEntity forKey:RESPONSE_DATA];
        return dataResult;
    }
    return nil;
}

@end

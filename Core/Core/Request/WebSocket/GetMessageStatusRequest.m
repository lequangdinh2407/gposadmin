//
//  GetMessageStatusRequest.m
//  luvkonectcore
//
//  Created by ToanPham on 5/13/15.
//  Copyright (c) 2015 ToanPham. All rights reserved.
//

#import "GetMessageStatusRequest.h"
#import "CommandBuilder.h"
#import "EntityManager.h"

#define TAG @"GetMessageStatusRequest"

@implementation GetMessageStatusRequest

- (instancetype)init:(NSArray*)ids success:(SuccessCallback)success failure:(FailureCallback)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:ids forKey:REQUEST_IDS];
    
    self = [super init:params success:success failure:failure];
    
    if (!self) {
        return nil;
    }
    
    return self;
}

- (NSMutableDictionary *)parseNetworkRespone:(id)responseObject {
    if ([responseObject isKindOfClass:[NSArray class]]) {
        NSArray *response = (NSArray*)responseObject;
        if (response.count > 0) {
            NSMutableDictionary* results = [[NSMutableDictionary alloc] init];
            [results setObject:response forKey:RESPONSE_DATA];
            return results;
        }
    }
    return nil;
}


- (SocketCommand*)getCommand {
    if (self.command != nil)
        return self.command;
    
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    NSData *content = [self.jsonData dataUsingEncoding:NSUTF8StringEncoding];
    
    [dict setValue:@(GET_MSG_STATUS_CMD) forKey:HDR_CMD];
    [dict setValue:@([Helpers getReqIDFormPref]) forKey:HDR_REQ_ID];
    [dict setValue:@([content length]) forKey:HDR_CONTENT_LENGTH];
    [dict setValue:content forKey:HDR_CONTENT];
    
    self.command = [[CommandBuilder sharedClient] createCommand:dict type:GET_MSG_STATUS_CMD];
    
    return self.command;
}

@end